<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 自定義Controller父類別(前端)
 *
 * @access	public
 * @param
 * @return
 */
class FrontEnd_Controller extends CI_Controller {
	
	/**
	 * 自定義建構方法
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct() {
		
		parent::__construct();
		
		//追加自定義func
		$this->load->database();
		$this->load->helper(array('url','dio_string','dio_message','dio_url','dio_array','dio_sys_vars','dio_sys','dio_db','is_valid'));
		$this->load->library(array('dio_pagination','cart','Cart_chenyunpaochuan','cart_log','category','Category_space'));
		
		//建立購物車物件
		$this->cls_cart = new cart();
		$this->cart_chenyunpaochuan = new Cart_chenyunpaochuan();
		
		//建立產品分類物件
		$this->cls_category = new category();
		$this->cls_category_space = new Category_space();
		
	    //設置語系
    	if( empty($_SESSION['language']) ){
    		changeLanguage('zh_tw');
    	}
    
		//設置 System 語系
		$this->config->set_item('language',$_SESSION['language']);
				
		//設置 Aplication 語系
		$this->lang->load('caseinc',$_SESSION['language']);
		
		//橫幅廣告
		$this->db->where('status' ,1);
		$this->ad_banner = $this->db->get('ad_banner')->result_array();
		
		//內頁廣告
		$this->db->where('status' ,1);
		$this->ad_content = $this->db->get('ad_content')->result_array();
		
		//優先指定meta
		$this->global_meta = array();
		/*
		if( !empty($this->meta_first()) ){
			$this->global_meta = seoMeta('first' ,$this->meta_first());
		}*/
		
		//各頁面寫入SESSION
		$this->page_record();
		
		//將分店ID寫入SESSION
		$branch_id = $this->input->get('branch_id');
		
		if( !empty($branch_id) ){
			$_SESSION['branch_id'] = $branch_id;
			
			//分店僅可使用組合商品購物流程
			if( !in_array(getUrlLast2Section(), getBranchEnabledFunc()) ){
				redirect('checkout/step1_comb_1');
			}
			
		}elseif( !empty($_SESSION['branch_id']) ){
			
			//分店僅可使用組合商品購物流程
			if( !in_array(getUrlLast2Section(), getBranchEnabledFunc()) ){
				redirect('checkout/step1_comb_1');
			}
				
		}
		
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 優先指定meta
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function meta_first(){
	
		//移除網域名
		$url = str_ireplace(base_url(), "", getCurrentFullUrl());
		$url = explode('?', $url);
		
		if( !empty($url[1])){
			$url = $url[0].'?'.getQueryStringParam($url[1]);
		}else{
			$url = $url[0];
		}
		
		$this->db->where('status' ,1);
		$this->db->where('url' ,$url);
		$query = $this->db->get('meta')->row_array();
		
		
		return $query;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 紀錄瀏覽頁面
	 *       [本次]和[上一次]瀏覽頁面
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function page_record(){
		
		$urlPage     = getUrlLast2Section();
		$urlPageAll  = empty($_SERVER['QUERY_STRING']) ?  $urlPage : $urlPage. '?' . $_SERVER['QUERY_STRING'];
		
		//上一個頁面
		if( !empty($_SESSION['page']['current'] ) && ($_SESSION['page']['current'] != $urlPageAll) ) {
		
			$_SESSION['page']['prev'] = $_SESSION['page']['current'] ;
		}
		
		//目前頁面設定
		if( in_array($urlPage, getValidController()) ){
				
			$_SESSION['page']['current'] = $urlPageAll;
		}else{
				
			$_SESSION['page']['current'] = '';
		}
		
	}

}

// --------------------------------------------------------------------

/**
 * 自定義Controller父類別(系統端)
 *
 * @access	public
 * @param
 * @return
 */
class BackEnd_Controller extends CI_Controller {

	/**
	 * 自定義建構方法
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct() {

		parent::__construct();
		
		$this->load->helper(array('dio_array','dio_sys_vars','dio_url','dio_db','dio_sys','dio_format','dio_business'));
		$this->load->library(array('Category','Category_space'));
		
		//建立產品分類物件
		$this->cls_category = new category();
		$this->cls_category_space = new Category_space();
		
		
		//設置語系
		//if( empty($_SESSION['language']) ){
			changeLanguage('zh_tw');
		//}
		
		//設置 System 語系
		$this->config->set_item('language',$_SESSION['language']);
		
		//設置 Aplication 語系
		$this->lang->load('caseinc',$_SESSION['language']);
	}

}

// --------------------------------------------------------------------

/**
 * 自定義Controller父類別(店家端)
 *
 * @access	public
 * @param
 * @return
 */
class Storend_Controller extends CI_Controller {

	/**
	 * 自定義建構方法
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct() {

		parent::__construct();

		$this->load->helper(array('dio_array','dio_sys_vars','dio_url','dio_db','dio_sys','dio_format'));
		$this->load->library(array('Category','Category_space'));

		//建立產品分類物件
		$this->cls_category = new category();
		$this->cls_category_space = new Category_space();


		//設置語系
		//if( empty($_SESSION['language']) ){
		changeLanguage('zh_tw');
		//}

		//設置 System 語系
		$this->config->set_item('language',$_SESSION['language']);

		//設置 Aplication 語系
		$this->lang->load('caseinc',$_SESSION['language']);
	}

}

