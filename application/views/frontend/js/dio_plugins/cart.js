﻿//-------------------------------------------------------------        
//
// 加入購物車 
//
//-------------------------------------------------------------        
 function my_add(product_speci_id ,sku ,entity){

	 /*		
     if( is_string_empty(sku) ){
    	 sku    = $( 'input[name="sku"]' ).val();
    	 entity = $( 'select[name="entity"] option::selected' ).val();
     }else{
    	 sku    = sku;
    	 entity = $( 'select[name="entity"] option::selected' ).val();
     }*/
     
	 if( entity == 0){
		 var entity = $('input[name="entity"]').val();
	 }
     
     var url = DIO_PATH_URL + 'buy/add';
     
     $.ajax({
  	      url: url,
  		  data: {product_speci_id : product_speci_id,
  	  		                   sku: sku ,
  			                entity: entity }, 
  		  type: 'POST',
  		  dataType: 'json',
  		  success: function(errMsg){    
		          if(errMsg == 0) {
				     alert('感謝您！商品已加入您的購物車！');
			      }else{
			    	 alert(errMsg); 
			      }
		          
		          location.reload();		          
   		  }
  	});

 }	


 //--------------------------------------------------------------
 //
 // 購物車的數量異動
 //
 //-------------------------------------------------------------
 function cart_qty_low(sign ,cart_id ,o_entity ,e){
 	 
	//非數字中止繼續
	if( isNaN(e) || (e == "")){
		 e.preventDefault(); 
    }
	 
 	if(sign == '+'){
 		var entity = parseInt($('#cart_entity_'+cart_id).val()) + parseInt(o_entity);
 	}else if(sign == '-'){
 		var entity = parseInt($('#cart_entity_'+cart_id).val()) - parseInt(o_entity);
 	}else if(sign == '*'){
         var entity = parseInt($('#cart_entity_'+cart_id).val());
 	}	
 	
     var url = DIO_PATH_URL + 'buy/edit';
 	
 	$.ajax({
 	      url: url,
 		  data: { cart_id: cart_id ,entity: entity }, 
 		  type: 'POST',
 		  dataType: 'json',
 		  success: function(errMsg){ 
 			   location.reload();                 
 	      }
 	});	  
 	
 }
 

//--------------------------------------------------------------
//
//刪除單筆購物車資料
//
//--------------------------------------------------------------
function cart_del(cart_id){
	
	var entity = parseInt($('#cart_entity_' + cart_id).val());
	
	cart_qty_low('-' ,cart_id ,entity ,entity);
}