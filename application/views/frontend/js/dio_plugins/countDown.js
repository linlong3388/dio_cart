//-------------------------------------------------------------        
//
// 優惠到期日
//   須建立 id='countdown' 且傳入截止日期
//
//-------------------------------------------------------------        
 function countDown( edate ){
		 
	// Set the date we're counting down to
	 var countDownDate = new Date(edate).getTime();

	 // Update the count down every 1 second
	 var x = setInterval(function() {

	     // Get todays date and time
	     var now = new Date().getTime();
	     
	     // Find the distance between now an the count down date
	     var distance = countDownDate - now;
	     
	     // Time calculations for days, hours, minutes and seconds
	     var days = Math.floor(distance / (1000 * 60 * 60 * 24));
	     var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
	     var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
	     var seconds = Math.floor((distance % (1000 * 60)) / 1000);
	     
	     // Output the result in an element with id="demo"
	     /* 一次列印
	     document.getElementById("countdown").innerHTML = days + "天" + hours + "時"
	     + minutes + "分" + seconds + "秒截止";*/
	     
	     document.getElementById("countDown_day").innerHTML  = (days  = days) ? days : '00';
	     document.getElementById("countDown_hour").innerHTML = (hours = hours) ? hours : '00';
	     document.getElementById("countDown_min").innerHTML  = (minutes = minutes) ? minutes : '00';
	     document.getElementById("countDown_sec").innerHTML  = (seconds = seconds) ? seconds : '00';
	     
	     // If the count down is over, write some text 
	     if (distance < 0) {
	         clearInterval(x);
	         //document.getElementById("countdown").innerHTML = "EXPIRED";
		     document.getElementById("countDown_day").innerHTML  = '00';
		     document.getElementById("countDown_hour").innerHTML = '00';
		     document.getElementById("countDown_min").innerHTML  = '00';
		     document.getElementById("countDown_sec").innerHTML  = '00';
	         
	     }
	 	
	 }, 1000); 

 }	