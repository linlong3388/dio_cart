﻿/*****************************************************
/* 名稱           : is_image
 * 功能           : 判斷是否為圖檔
 ****************************************************/ 
function is_image(filename) {
	var parts = filename.split('.');
    var ext = parts[parts.length - 1];
    switch (ext.toLowerCase()) {
    case 'jpg':
    case 'gif':
    case 'bmp':
    case 'png':
        //etc
        return true;
    }
    return false;
}


/*****************************************************
/* 名稱           : is_string_empty
 * 功能           : 判斷是否為空字串
 ****************************************************/ 
function is_string_empty(str) {
    return (!str || 0 === str.length);
}


/*****************************************************
/* 名稱           : MM_openBrWindow
 * 功能           : 彈跳子視窗
 ****************************************************/ 
function MM_openBrWindow(theURL,winName,features) { //v2.0
	  window.open(theURL,winName,features);
}


/*****************************************************
/* 名稱           : def_numeric
 * 功能           : 定義輸入值必須為數值型態
 ****************************************************/ 
function def_numeric(){
	if(event.keyCode < 48 || event.keyCode > 57) if(event.keyCode != 13 ) 
		 event.returnValue = false;
}


/*****************************************************
/* 名稱           : del_confirm
 * 功能           : 使用者按下刪除後 ,跳出確認視窗 ,並做導向 
 ****************************************************/ 
function del_confirm(url ,id){
   var chk_box=confirm('確定刪除資料?');
   if (chk_box == true) {
	  location.href = url+id;
   }
}


/*****************************************************
/* 名稱           : dio_random
 * 功能           : 產生一組隨機數 
 * 備註           ：由於IE瀏覽器不支援indexOf，參考
 * http://paladinprogram.blogspot.tw/2010/07/ie-arrayindexof.html
 ****************************************************/ 
function dio_random(val){
    var a=[];
    while(a.length < val) {
        var n = Math.ceil(Math.random() * val);      
        if (a.indexOf(n)==-1) a.push(n);        
    }
      
     return a;  
}


/*****************************************************
/* 名稱           : is_null
 * 功能           : 判斷是否為null值 
 ****************************************************/
function is_null(obj){  
  if (typeof(obj) == 'undefined' || obj == null)  
    return true;  
  return false;  
}  


/*****************************************************
/* 名稱           : get_host_folder
 * 功能           : 取得主機/資料夾路徑 
 ****************************************************/
function get_host_folder(){
	
	var folder   = '';
    var pathname = location.pathname.split('/')[1];
	
	//判斷資料夾是否存在
	if( pathname != '' && pathname != 'index.php'){
		folder = location.pathname.split('/')[1] + "/";
	}
	
	var url = location.protocol + "//" + document.domain + "/"  + folder;
	return url;
}


/*****************************************************
/* 名稱           : dio_blockUI
 * 功能           : blockUI訊息提示 
 ****************************************************/
function dio_blockUI(msg){
	
   $.blockUI({ 
         message:"<b style='font-size:20px'>" + msg + "</b>" , 
         css: { 
             border: 'none', 
             padding: '15px', 
             backgroundColor: '#000', 
                              '-webkit-border-radius': '10px', 
                              '-moz-border-radius': '10px', 
             opacity: .5, 
             color: '#fff',
             height: '10%' 
           } 
   });

   setTimeout($.unblockUI, 1500); 
}


/*****************************************************
/* 名稱 : dio_getParameterByName
 * 功能 : 取得URL參數值
 ****************************************************/
function dio_getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
