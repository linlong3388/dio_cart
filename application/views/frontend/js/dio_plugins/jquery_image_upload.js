/**
 * 
 * 函數 : jquery_image_upload
 * 說明 : 圖片上傳。
 * 
 * @functionName jquery_image_upload
 * @author Dio
 * 
 */

/***********************************
/* 名稱 : base_image_delete
/* 功能 : 基本圖片刪除 
/* @param : 名稱
************************************/
function base_image_delete(name) {
   $("input[name="+name+"]").val('');	
   $('#'+name).attr('src' ,'#');
}


/***********************************
/* 名稱 : base_image_upload
/* 功能 : 基本圖片上傳 
/* @param : 名稱 ,路徑
************************************/
function base_image_upload(name) {
	
	$('#dialog').remove();
    $('body').append('<div id="dialog" style="padding: 3px 0px 0px 0px;"><div class="file-manager"></div></div>');

	$('#dialog').dialog({
		title: '圖片',
		bgiframe: false,
		width: 800,
		height: 400,
		resizable: false,
		modal: false
	});
	
	//file manager
	var path;
	
    var elf = $('.file-manager').elfinder({
 	               url : dio_elfinder_url,  // connector URL (REQUIRED) 	              
 	               uiOptions: {
                       toolbar : [ ['search'] ]
                     }, 
 	               handlers : {
 	                         select : function(event, elfinderInstance) {
 	                                   var selected = event.data.selected;
 	                                   if(selected.length > 0){
 	              	                        var file = elfinderInstance.file(selected[0]); 	              	                        
 	              	                        //path格式 ,目錄/檔名
 	              	                        path = elfinderInstance.path(selected[0]).replace(/\\/g, '/');
 	              	                        path = path.replace(/uploads\//g, '');
 	              	                        filename = file.name; //檔名
                                       }
 	                                },
 	                          dblclick: function(event, elfinderInstance) {

 	                        	           //判斷是否為圖檔 
                                          if(is_image(filename)){  
                                        	    $('#dialog').dialog('close');
 	                        	                elfinderInstance.destroy();
           	                        	        $("input[name="+name+"]").val(path); //只取檔名
	                 	                        $('#'+name).attr('src',dio_base_url + 'resources/uploads/' + path); 
		                 	                        
 	                                	        return false; // stop elfinder
 	                                      }                    
 	                           }            
 	                         }
     	       }).elfinder('instance');
	     
}


/***********************************
/* 名稱 : base_file_download
/* 功能 : 檔案下載連結 
/* @param : 名稱 ,路徑
************************************/
function base_file_download(name) {
	
	$('#dialog').remove();
    $('body').append('<div id="dialog" style="padding: 3px 0px 0px 0px;"><div class="file-manager"></div></div>');

	$('#dialog').dialog({
		title: '圖片',
		bgiframe: false,
		width: 800,
		height: 400,
		resizable: false,
		modal: false
	});
	
	//file manager
	var path;
	
    var elf = $('.file-manager').elfinder({
 	               url : 'elfinder_init',  // connector URL (REQUIRED) 	              
 	               uiOptions: {
                       toolbar : [ ['search'] ]
                     }, 
 	               handlers : {
 	                         select : function(event, elfinderInstance) {
 	                                   var selected = event.data.selected;
 	                                   if(selected.length > 0){
 	              	                        var file = elfinderInstance.file(selected[0]); 	              	                        
 	              	                        //path格式 ,目錄/檔名
 	              	                        path = elfinderInstance.path(selected[0]).replace(/\\/g, '/');
 	              	                        path = path.replace(/uploads\//g, '');
 	              	                        filename = file.name; //檔名
                                       }
 	                                },
 	                          dblclick: function(event, elfinderInstance) {
                                   	    $('#dialog').dialog('close');
                       	                elfinderInstance.destroy();
   	                        	        $("input[name="+name+"]").val(path); //只取檔名

   	                        	        return false; // stop elfinder
 	                           }            
 	                         }
     	       }).elfinder('instance');
	     
}


/* END jquery_image_upload */
