<body>
<?php $this->load->view('frontend/common/menu.tpl') ?>
<?php $this->load->view('frontend/common/banner.tpl') ?>
<?php $this->load->view('frontend/common/breadcrumb.tpl') ?>

<div class="container-fluid nopadding">
  <div class="container space-top-bm-lg">
    <div class="col-md-12">
      <div class="abCont col-md-12 space-bm">
        <div class="col-md-12 text-center sm-font space-bm"><span class=" txt-line-sm" style="border-bottom:3px solid #fcca3a;padding-bottom:10px; margin-bottom:10px；"><i class="glyphicon glyphicon-tree-deciduous"></i> 關於我們</span> </div>
         <?php echo changeContentImagePath($query['content'])?></div>
      <!-- end adCont--><!-- end serviceCont--><!-- end futureCont--> 
    </div>
  </div>
</div>

<!-- end middle -->
<?php $this->load->view('frontend/common/footer.tpl') ?>
<?php $this->load->view('frontend/common/footer_js.tpl') ?>
<script>
$(document).ready(function() { 
    $("html").niceScroll(); 
  });
  $(".navbar-nav li").eq(1).append("<i class='active'></i>");
$('.parallax-b').parallax({imageSrc: 'images/his.jpg'});
$("span.menu").click(function(){
$(".top-menu ul").slideToggle("" , function(){
});
});

/*=================================
||			WOW
==================================*/
wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  )
wow.init();
 </script>
</body>
</html>

