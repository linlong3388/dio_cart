<body>
<?php $this->load->view('frontend/common/menu.tpl') ?>
<?php $this->load->view('frontend/common/breadcrumb.tpl') ?>


<div class="news container space-top-lg">
  <div class="col-md-12 text-center sm-font space-bm"><span class=" txt-line-sm" style="border-bottom:3px solid #fcca3a;padding-bottom:10px; margin-bottom:10px；"><i class="glyphicon glyphicon-heart"></i><?php echo $this->cls_category->get_1parent()[2]['name']?></span> </div>
  <div class="row">
  
  <div class="col-md-12 space-bm-xs text-center clearfix">
      <ul class="all-ul-li-none pro-left-nav">
          <li><a href="<?php echo getUserURL('product_grain/lists?srh_category=3_*&page=1')?>">所有</a></li>
       <?php foreach ($this->cls_category->get_2parent(3) as $row) { ?>
          <li><a href="<?php echo getUserURL('product_grain/lists?srh_category=3_'.$row['category_id'].'&page=1')?>"><?php echo $row['name']?></a></li>
       <?php } ?>
      </ul>
  </div> 

  <div class="col-md-12 color-yellow-bk Margin-bm-sm padding-non">
    <div class="container reflashbox padding-non"> 
      <div class="col-xs-12 col-md-3 big_txt Margin-top-xs"><i class="glyphicon glyphicon-flash reflashTitle"></i>本月活動/剩餘：</div>
      
      <div class="col-xs-12 col-md-9 padding-non">
        <span class="reflastime col-md-9 col-xs-12 space-top-bm-xs"> 
          <span class="timebox" id="countDown_day">00</span> 天 
          <span class="timebox" id="countDown_hour">00</span> 時 
          <span class="timebox" id="countDown_min">00</span> 分 
          <span class="timebox" id="countDown_sec">00</span> 秒 
        </span>
        
        <span class="col-md-3 col-xs-12 space-top-bm-xs">
          <a class="color-white reflash-btn">活動結束將無法下標</a>
        </span>
      </div>
      
    </div>
  </div>

  <!--<div class="col-md-12 space-bm-xs text-center clearfix">
    <img src="upload/59f93c3762c1c.jpg" width="100%;">
  </div>-->

   <div class="col-md-12">
    <!--商品列表-->
     <?php foreach ($query as $row) { ?>
    <div class="col-md-3 wow fadeInDown items" data-wow-delay="0.1s"  data-wow-offset="20"> 
      <a href="<?php echo getUserURL('product_grain/view?srh_category='.$this->input->get('srh_category').'&product_id='.$row['product_id'])?>"><img src="<?php echo getDefaultImageURL($row['image'] ,255,255)?>" class=" Margin-top-bm-xs center-block hvr-grow-rotate all-border-bold"></a>
      <div class="col-md-12 icon-yellow Margin-top-bm-xs space-left" style="font-size:15px;"><?php echo $row['name']?></div>
      <div class="color-gray xs-text txt-line-xs col-md-12 padding-non Margin-bm-xs"><?php echo mb_substr( strip_tags($row['techenique']), 0, 16,"utf-8") ?>...</div>
      <div class=" xs-text txt-line-xs col-md-12 padding-non"> <span>市場售價 <span class="org-sale"><?php echo DIO_CURRENCY.addCommas($row['price_original'])?></span></span> / <span>愛心價 <span class="color-wd-red"><?php echo DIO_CURRENCY.addCommas($row['price'])?></span></span> </div>
      <a href="<?php echo getUserURL('product_grain/view?srh_category=3_*&product_id='.$row['product_id'])?>" class="btn btn-default col-md-6 cartsid Margin-top-bm-xs">查看贊助內容</a> 
      <?php if(date('Y-m-d') <= $query_category['edate']){ ?>
        <a href="javascript:my_add('<?php echo $row['product_id']?>','<?php echo $row['sku']?>','1')" class="btn btn-warning col-md-6 addcart Margin-top-bm-xs">加入贊助</a> 
      <?php } ?>
    </div>
    <?php } ?>
    
   </div> 
    
  </div>
 <div class="col-xs-12 space-top-bm-lg padding-non">
         <nav aria-label="...">
          <ul class="pagination">
   					  <!-- 首頁 --> 
				      <?php if(!$pagination['is_first']){ ?>
				             <li><a href="<?php echo $pagination['url'].'?'.preg_replace('/&?page=[^&]*/', '', getQueryStringParam()).'&page=1' ?>"><span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span></a></li>
                      <?php } ?>
                  
				      <!-- 上一頁 --> 
				      <?php if(!$pagination['is_first']){ ?>
				             <li><a href="<?php echo $pagination['url'].'?'.preg_replace('/&?page=[^&]*/', '', getQueryStringParam()).'&page='.($pagination['page']-1) ?>">&lsaquo;</a></li>
                      <?php } ?>
                       
                      <!-- 數字頁碼 -->
                      <?php for($i=$pagination['range1'] ; $i<=$pagination['range2'] ;$i++){ ?>
                              <li class="<?php echo ($this->input->get('page') == $i) ? ' active' : ''?>"><a href="<?php echo $pagination['url'].'?'.preg_replace('/&?page=[^&]*/', '', getQueryStringParam()).'&page='.$i ?>"><?php echo $i ?></a></li>  
                      <?php } ?>
                      
                      <!-- 下一頁 -->
				      <?php if(!$pagination['is_last']){ ?>
				             <li><a href="<?php echo $pagination['url'].'?'.preg_replace('/&?page=[^&]*/', '', getQueryStringParam()).'&page='.($pagination['page']+1) ?>">&rsaquo;</a></li>
                     
                      <?php } ?>
                      
                      <!-- 末頁 --> 
				      <?php if(!$pagination['is_last']){ ?>
				             <li><a href="<?php echo $pagination['url'].'?'.preg_replace('/&?page=[^&]*/', '', getQueryStringParam()).'&page='.($pagination['numPage']) ?>"><span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span></a></li>
                      <?php } ?>  
         </ul>  
           
         </nav></div>
</div>

<?php $this->load->view('frontend/common/footer.tpl') ?>
<!-- end middle --> 
<?php $this->load->view('frontend/common/footer_js.tpl') ?>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script> 
<script src="<?php echo getUserFile('js/stepper.widget.js')?>"></script>
<script src="<?php echo getUserFile('js/dio_plugins/countDown.js')?>"></script>
<script src="<?php echo getUserFile('js/dio_plugins/cart.js')?>"></script>
<script>
//--------------------------------------------------------------
//
// 倒數計時器
//
//--------------------------------------------------------------
countDown("<?php echo str_replace("-","/",$query_category['edate']).' 23:59:59'?>");

$(document).ready(function() { 
    $("html").niceScroll(); 
  });
  $(".navbar-nav li").eq(3).append("<i class='active'></i>");
$("span.menu").click(function(){
$(".top-menu ul").slideToggle("" , function(){
});
});

/*=================================
||			WOW
==================================*/
wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  )
wow.init();
 </script>
</body>
</html>
