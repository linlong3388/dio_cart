<body>
<?php $this->load->view('frontend/common/menu.tpl') ?>
<?php $this->load->view('frontend/common/breadcrumb.tpl') ?>

<div class="container-fluid nopadding">
  <div class="container space-top-bm">
    <div class="row">
      <div class="col-md-12 sm-text padding-non clearfix">
        <div class="col-md-10 col-sm-12 col-xs-12 big_txt space-top-bm"><i class="fa fa-shopping-cart"></i> / <?php echo $query['name']?></div>
      <div  class="col-md-2">
                        <div class="fb-like" data-href="http://www.ollin.com.tw/product.php?id=46" data-width="150" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
                      </div>
      </div>
      <div class="col-md-12 padding-non space-top-bm-md">
        <div class="col-md-6"><img src="<?php echo base_url(DIO_PATH_PIMG.'/'.$query['image'])?>" class="img-responsive Margin-bm-md"></div>
        <div class="col-md-6 xs-text tw-word txt-line-md clearfix">
          
          <div class="panel panel-default col-md-12 padding-non">
            <div class="panel-heading"><i class="fa fa-info-circle"></i> 商品簡介：</div>
            <div class="panel-body"><?php echo $query['techenique']?> </div>
          </div>
          
          <div class=" xs-text txt-line-xs col-md-12 padding-non">
          <span>● 需求數量： <?php echo $query['ps_stock']?> </span><br>
          <span>● 市場售價： <span class="org-sale"><?php echo DIO_CURRENCY.addCommas($query['price_original'])?></span></span> 
          / <span>愛心價： <span class="color-wd-red"><?php echo DIO_CURRENCY.addCommas($query['price'])?></span></span> </div>
          
         <?php if( date('Y-m-d') <= $query['cg_edate'] ) { ?> 
          <div class="col-md-6 space-top-bm-sm padding-non">
          
            <div class="input-group stepper-widget">
              <?php if( $query['ps_stock'] > 0 ){ ?>
                <input type="text" name="entity" class="form-control js-qty-input" value="1" onkeypress="def_numeric()">
              <?php }else{ ?>
                <input type="text" class="form-control" aria-label="..." placeholder="1" value="0" readonly>
              <?php } ?>
              
              <div class="input-group-btn">
                <button type="button" class="btn btn-default js-qty-down"><i class="glyphicon glyphicon-minus"></i></button>
                <button type="button" class="btn btn-default js-qty-up"><i class="glyphicon glyphicon-plus"></i></button>
              </div>
            </div>
            
          </div>
          
          <div class="col-md-6 space-bm-sm text-right">
             <button onclick="my_add('<?php echo $query['product_speci_id']?>','<?php echo $query['sku']?>','0')"  type="button" class="btn btn-warning btn-lg">加入贊助</button>
             <button onclick="javascript:location.href='<?php echo getUserURL('checkout/step0')?>'"  type="button" class="btn btn-danger btn-lg">立即結帳</button>
          </div>
          <?php } ?>
          
        </div>
        <div class="col-md-12 space-top-bm-sm  Margin-top-md xs-text clearfix"> 
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-bell"></i> 商品資訊</a></li>
            <li role="presentation"></li>
          </ul>
          
          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active space-top-bm-md" id="home"><?php echo changeContentImageURL($query['description'])?></div>
            <div role="tabpanel" class="tab-pane space-top-bm-md" id="profile">...</div>
          </div>
        </div>
      </div>
    </div>
        </div> 
          </div>

<script src="<?php echo getUserFile('js/dio_plugins/cart.js')?>"></script> 
<?php $this->load->view('frontend/common/footer.tpl') ?>
<?php $this->load->view('frontend/common/footer_js.tpl') ?>
<script src="<?php echo getUserFile('js/stepper.widget.js')?>"></script>
<script src="<?php echo getUserFile('js/jquery.pajinate.js')?>"></script> 
<script>
jQuery(document).on('ready', function(){
    jQuery('.stepper-widget').stepper();
});


$(document).ready(function() { 
    $("html").niceScroll(); 
  });
$(".navbar-nav li").eq(3).append("<i class='active'></i>");  
$(document).ready(function(){
				$('#paging_container9').pajinate({
					num_page_links_to_display : 3,
					items_per_page : 6,
                    wrap_around: true,
                    show_first_last: false
				});
});   



$('.parallax-a').parallax({imageSrc: 'images/p01.png'});
$("span.menu").click(function(){
$(".top-menu ul").slideToggle("" , function(){
});
});

/*=================================
||			WOW
==================================*/
wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  )
wow.init();
 </script>
</body>
</html>
