<div class="col-md-12 btn-group btn-group-justified non-padd space-top-md" role="group">
  <div class="btn-group" role="group">
    <button type="button" <?php echo (getUrlLast2Section() == 'checkout/step1') ? ' class="btn btn-success"' : ' class="btn btn-default"' ?>>購物清單</button>
  </div>   
  <div class="btn-group" role="group">
    <button type="button" <?php echo (getUrlLast2Section() == 'checkout/step2') ? ' class="btn btn-success"' : ' class="btn btn-default"' ?>>填寫收件資料</button>
  </div>
  <div class="btn-group" role="group">
    <button type="button" <?php echo (getUrlLast2Section() == 'checkout/step3') ? ' class="btn btn-success"' : ' class="btn btn-default"' ?>>填寫訂單資料</button>
  </div>
  <div class="btn-group" role="group">
    <button type="button" <?php echo (getUrlLast2Section() == 'checkout/step4') ? ' class="btn btn-success"' : ' class="btn btn-default"' ?>>訂單確認</button>
  </div>
  
</div>
