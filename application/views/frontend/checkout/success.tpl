<?php $pay_method = $this->input->get('pay_method')?>

<body>
<?php $this->load->view('frontend/common/menu.tpl') ?>
<?php $this->load->view('frontend/common/breadcrumb.tpl') ?>

<div class="container space-top-bm-lg ">
    <div class="row">
      <div class="col-md-12 sm-text clearfix"><i class="fa fa-shopping-cart"></i> / 購物結果</div>
      
      <div class="col-md-12">
        
      <div class="col-md-12 well">

         <h3>恭喜!您已完成一筆訂購單!</h3>
         <p><?php //echo $query?></p>
        
          <?php if( $pay_method == 5) {?>
		                   轉帳資訊如下 : <p>
		      <?php echo $query ?> <hr />
        
               <p>
                                        轉帳須知 : <p>
                  <b class="dio_star">★</b>若完成轉帳付款，請至 : <a href="<?php echo getUserURL('contact/view')?>">「聯絡我們」</a>，填寫您的<b>「訂單編號」</b>和<b>「轉帳後四碼」</b>，並送出訊息給管理員，以便查帳，謝謝您。<br/>
               </p>
          <?php }?>
        
        <hr />
        
         <p>
        
         <h4>詳細資料請至  <a href="<?php echo getUserURL('customer_order/lists?id='.uniqid().'&page=1')?>">「會員中心」</a> 查詢</h4>
   
          <p>
   
                         若有問題，可到 :
          <a class="dio_href" href="<?php echo getUserURL('contact/view')?>">聯絡我們</a> 
     
        <br> 
           
      </div>
      
      <div class="col-md-12">
       <button type="button" class="btn btn-primary btn-lg pull-right" onclick="location.href='<?php echo getUserURL('product/lists?srh_category=1_*&page=1')?>'">繼續購物</button>     </div>
    
    </form>
    
    </div>
  </div>
   </div>

<?php $this->load->view('frontend/common/footer.tpl') ?>
<!-- end middle --> 
<?php $this->load->view('frontend/common/footer_js.tpl') ?>
<script>
//---------------------------------------------------                        
//
// 取得地址資料
//
//---------------------------------------------------                        
function default_addr(customer_addr_id){

	var url= DIO_PATH_URL + 'ajax/getCustomerAddr';
	
	$.ajax({
	      url: url,
		  data: { customer_addr_id: customer_addr_id.value }, 
		  type: 'POST',
		  dataType: 'json',
		  success: function(info){    
                   $('input[name="last_name"]').val( info.last_name );  
                   $('input[name="email"]').val( info.email );  
                   $('input[name="mobile"]').val( info.mobile );  
                   $('input[name="phone"]').val( info.phone );  
                   $('select[name="local"]').val( info.local );
                   $('input[name="address"]').val( info.address );
                      
	               return false; 
		  }
	});	  
	
}


                        
$(document).ready(function() { 
    $("html").niceScroll(); 
  });

/*=================================
||			WOW
==================================*/
wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  )
wow.init();
 </script>
</body>
</html>
