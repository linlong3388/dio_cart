<body>
<?php $this->load->view('frontend/common/menu.tpl') ?>
<?php $this->load->view('frontend/common/breadcrumb.tpl') ?>

<div class="container space-top-bm-lg ">
    <div class="row">
      <div class="col-md-12 sm-text clearfix"><i class="fa fa-shopping-cart"></i> / 購物清單</div>
     
     <?php $this->load->view('frontend/checkout/common/menu.tpl') ?>
      
      <div class="col-md-12">

        <b style="color: red"><?php echo validation_errors(); ?></b> 
          
        <?php echo form_open( getCurrentFullUrl() , array('id' => 'step3'))?>  
      
         <p><strong><b class="dio_star">★</b>選擇付款方式：</strong><br>
            <br>
               <select name="pay_method" class="form-control">
                  <?php $pay_metnod = fieldCartDefault('pay_method' ,0); 
                        if( isCategory_3ForCart() ){ 
                        	 $payMethodAry = getPayMethod();
                        	 unset($payMethodAry[1]);
                        	 unset($payMethodAry[2]);
                        	 unset($payMethodAry[3]); ?>
                      <?php foreach ( reSortArrayMutil(3,$pay_metnod,$payMethodAry) as $key=>$val ) {?>
                          <option value="<?php echo $key?>"><?php echo $val?></option>
                      <?php } ?>
                  <?php }else{ ?> 
                      <?php foreach ( reSortArrayMutil(3,$pay_metnod,getPayMethod()) as $key=>$val ) {?>
                          <option value="<?php echo $key?>"><?php echo $val?></option>
                      <?php } ?>
                  <?php } ?>      
               </select>
         </p>
         
         <?php if( !isCategory_3ForCart() ){ ?> 
         <p><strong>優惠券代碼：</strong><br>
            <br>
            <?php if( !empty($_SESSION['sys_info']['promo']['coupon']) ){ ?>
                <input type="text" class="form-control" id="code_use" name="code_use" value="<?php echo !empty($_SESSION['my_order']['info']['code_use']) ? $_SESSION['my_order']['info']['code_use'] : set_value('code_use')?>">
            <?php }else{ ?>
                <b>目前暫無優惠券方案</b>
            <?php } ?>
         </p>
         <?php } ?>
         
         <p><strong>備註：</strong><br>
            <br>
            <textarea name="memo" rows="5" class="form-control" id="exampleInputEmail2"><?php echo !empty($_SESSION['my_order']['info']['memo']) ? $_SESSION['my_order']['info']['memo'] : set_value('memo')?></textarea>
         </p>
      
      <div class="col-md-12">
         <button type="button" onclick="formSubmit()" class="btn btn-primary btn-lg pull-right">下一步</button>
         <button type="button" class="btn btn-default btn-lg" onclick="location.href='<?php echo getUserURL('checkout/step2#dio_qty')?>'">回步驟2</button>
      </div>
    
    </form>     
    
    </div>
  </div>
   </div>

<?php $this->load->view('frontend/common/footer.tpl') ?>
<!-- end middle --> 
<?php $this->load->view('frontend/common/footer_js.tpl') ?>
<script>
//---------------------------------------------------                        
//
// 表單送出[下一步]
//
//---------------------------------------------------  
function formSubmit(){
    $('#step3').submit();
}


//---------------------------------------------------                        
//
// 取得地址資料
//
//---------------------------------------------------                        
function default_addr(customer_addr_id){

	var url= DIO_PATH_URL + 'ajax/getCustomerAddr';
	
	$.ajax({
	      url: url,
		  data: { customer_addr_id: customer_addr_id.value }, 
		  type: 'POST',
		  dataType: 'json',
		  success: function(info){    
                   $('input[name="last_name"]').val( info.last_name );  
                   $('input[name="email"]').val( info.email );  
                   $('input[name="mobile"]').val( info.mobile );  
                   $('input[name="phone"]').val( info.phone );  
                   $('select[name="local"]').val( info.local );
                   $('input[name="address"]').val( info.address );
                      
	               return false; 
		  }
	});	  
	
}


                        
$(document).ready(function() { 
    $("html").niceScroll(); 
  });

/*=================================
||			WOW
==================================*/
wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  )
wow.init();
 </script>
</body>
</html>
