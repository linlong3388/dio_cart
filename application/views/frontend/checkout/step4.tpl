<body>
<?php $this->load->view('frontend/common/menu.tpl') ?>
<?php $this->load->view('frontend/common/breadcrumb.tpl') ?>

<div class="container space-top-bm-lg ">
    <div class="row">
      <div class="col-md-12 sm-text clearfix"><i class="fa fa-shopping-cart"></i> / 購物清單</div>
      
      <?php $this->load->view('frontend/checkout/common/menu.tpl') ?>
            
      <div class="col-md-12 space-top-bm-sm">
      
        <p><strong>(一)訂購清單：</strong><br>
      
        <table width="100%" border="1" class="cart_summary">
        
          <tr>
            <th width="20%" bgcolor="#fbfbfb" scope="col">商品</th>
         	<th bgcolor="#fbfbfb" scope="col">說明</th>
         	<th bgcolor="#fbfbfb" scope="col">單價</th>
         	<th bgcolor="#fbfbfb" scope="col">數量</th>
         	<th bgcolor="#fbfbfb" scope="col">總計</th>
         </tr>
         
         <?php foreach ($_SESSION['my_order']['cart'] as $key=>$row) { ?>   
         <tr>
         	<td><img src="<?php echo base_url(DIO_PATH_PIMG.'/'.$row['image'])?>" alt="" width="50%"></td>
         	<td><p class="title"><strong><?php echo $row['name']?></strong><br>
           			SKU：<?php echo $row['sku2']?><br>
           			               <strong>
               <?php if( isCategory_3($row['category_id']) ) { 
                        $isCategory2 = true;
               	?> 
                  <b style="color:blue">( 助糧商品 )</b>
               <?php }else{ 
               	        $isCategory3 = true;;
               	?>
                   ( 一般商品 )
               <?php }?>
               </strong>
           			
           			</p></td>
         	<td><?php echo DIO_CURRENCY.addCommas($row['price'])?></td>
         	<td><?php echo $row['entity']?></td>
         	<td align="right"><?php echo DIO_CURRENCY.addCommas($row['total'])?></td>
         </tr>
       <?php } ?>   
       
       <!-- 運費 -->   
        <tr>
          	<td colspan="3" bgcolor="#fbfbfb">運費	</td>
         	<td colspan="3" align="right" bgcolor="#fbfbfb"><?php echo DIO_CURRENCY.addCommas($_SESSION['my_order']['fee']['total'])?></td>
       </tr>
       
       <!-- 優惠券 -->  
       <?php if( !empty($_SESSION['my_order']['coupon']['total']) ){ ?>
       <tr>
          	<td colspan="3" bgcolor="#fbfbfb"><?php echo $_SESSION['my_order']['coupon']['name']?></td>
         	<td colspan="3" align="right" bgcolor="#fbfbfb">-<?php echo DIO_CURRENCY.addCommas($_SESSION['my_order']['coupon']['total'])?></td>
       </tr>
       <?php } ?>       
       
       <tr>
         <td colspan="3" bgcolor="#fbfbfb">總計</td>
         <td colspan="3" align="right" bgcolor="#fbfbfb"><?php echo DIO_CURRENCY.addCommas($_SESSION['my_order']['total'])?></td>
       </tr>
       
     </table>
      
    </div>
    
    <?php if( !empty($_SESSION['my_order']['cvs']) ) { ?>
     <div class="col-md-12 non-padd">
        <p><strong>超商取貨資訊：</strong><br>
          <br>
          
       <div class="col-md-12 well">
     
        <p>超商類別 : <?php echo reSortArrayMutil(2,$_SESSION['my_order']['cvs']['cvs_logistics_sub_type'],getECPayCvsLogisticsSubType())?></p>
        <p>超商編號 : <?php echo $_SESSION['my_order']['cvs']['cvs_store_id']?></p>
        <p>超商名稱 : <?php echo $_SESSION['my_order']['cvs']['cvs_store_name']?></p>
        <p>超商地址 : <?php echo $_SESSION['my_order']['cvs']['cvs_address']?></p>
        <p>超商電話 : <?php echo $_SESSION['my_order']['cvs']['cvs_telephone']?></p>
        <p>備註 : <?php echo $_SESSION['my_order']['cvs']['cvs_extra_data']?></p>
      
       </div> 
      
     </div>
    <?php } ?> 
      
    <div class="col-md-12 non-padd">
        <p><strong>收件人資訊：</strong><br>
          <br>
          
       <div class="col-md-12 well">
     
        <p>姓名 : <?php echo $_SESSION['my_order']['info']['last_name']?></p>
        <p>電話 : <?php echo $_SESSION['my_order']['info']['phone']?></p>
        <p>手機 : <?php echo $_SESSION['my_order']['info']['mobile']?></p>
        <p>地址 : <?php echo $_SESSION['my_order']['info']['local'] .' '.$_SESSION['my_order']['info']['address']?></p>
        <p>付款方式 : <?php echo reSortArrayMutil(2,$_SESSION['my_order']['info']['pay_method'],getPayMethod()) ?></p>
        <p>備註 : <?php echo $_SESSION['my_order']['info']['memo']?></p>
     
       </div> 
      
     </div>
     

      <div class="col-md-12">
         <button type="button" class="btn btn-primary btn-lg pull-right" onclick="order_submit()">下一步</button>
         <button type="button" class="btn btn-default btn-lg" onclick="location.href='<?php echo getUserURL('checkout/step3')?>'">回步驟3</button>
        
      </div>
      
<!--/lightbox-資料處理中 -->
<div id="open-process" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
     <div class="modal-body">
       <img src="<?php echo getUserFile('images/loading.gif')?>" />
       <b style="font-size: larger"> 資料處理中，請稍後...</b> 
     </div>
     
      </div>
    </div>  
</div> 
<!--/lightbox-資料處理中 -->
      
      
  </div>
  </div>

<?php $this->load->view('frontend/common/footer.tpl') ?>
<!-- end middle --> 
<?php $this->load->view('frontend/common/footer_js.tpl') ?>
<script>
//---------------------------------------------------                        
//
// 訂單送出，避免重複下單
//
//---------------------------------------------------                        
function order_submit(){
	$('#open-process').modal({backdrop: 'static', keyboard: false});
	location.href='<?php echo getUserURL('checkout/step5')?>';
}


$(document).ready(function() { 
    $("html").niceScroll(); 
  });
  
/*=================================
||			WOW
==================================*/
wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  )
wow.init();
 </script>
</body>
</html>
