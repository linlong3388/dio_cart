<?php $isCategory2 = false;
      $isCategory3 = false; ?>
<body>
<?php $this->load->view('frontend/common/menu.tpl') ?>
<?php $this->load->view('frontend/common/breadcrumb.tpl') ?>

<div class=" container space-top-lg">
  <div class="container-fluid nopadding clearfix">
  <div class="container space-top-bm-lg ">
    <div class="row">
      <div class="col-md-12 sm-text clearfix"><i class="fa fa-shopping-cart"></i> / 購物清單</div> 
      
      <?php $this->load->view('frontend/checkout/common/menu.tpl') ?>
      
    <?php if( !empty($_SESSION['my_order']['cart']) ){ ?>  
      <div class="col-md-12 space-top-bm-sm">
        <table width="98%" border="1" cellpadding="0" cellspacing="0" class="cart_summary">
        <tr>
         <th width="20%" bgcolor="#fbfbfb" scope="col">商品</th>
         <th bgcolor="#fbfbfb" scope="col">說明</th>
         <th bgcolor="#fbfbfb" scope="col">單價</th>
         <th bgcolor="#fbfbfb" scope="col">數量</th>
         <th width="5%" bgcolor="#fbfbfb" scope="col">刪除</th>
         <th bgcolor="#fbfbfb" scope="col">總計</th>
       </tr>
      
      <?php foreach ($_SESSION['my_order']['cart'] as $key=>$row) { ?>      
       <tr>
         <td><img src="<?php echo base_url(DIO_PATH_PIMG.'/'.$row['image'])?>" alt="" width="50%"></td>
         <td><p class="title">
               <strong><?php echo $row['name']?></strong><br>
               SKU：<?php echo $row['sku2']?><br>
               <strong>
               <?php if( isCategory_3($row['category_id']) ) { 
                        $isCategory2 = true;
               	?> 
                  <b style="color:blue">( 助糧商品 )</b>
               <?php }else{ 
               	        $isCategory3 = true;;
               	?>
                   ( 一般商品 )
               <?php }?>
               </strong>
             </p>
       </td>
         <td><?php echo DIO_CURRENCY.addCommas($row['price'])?></td>
         <td>
             <input type="text" name="entity" id="cart_entity_<?php echo $key?>" class="form-control" onkeyup="cart_qty_low('*' ,'<?php echo $key?>' ,'1',this.value)" value="<?php echo $row['entity']?>" ><br>
             <div class="cart-btn-list">
               <a class="btn btn-default glyphicon glyphicon-plus" onclick="javascript:cart_qty_low('+' ,'<?php echo $key?>' ,'1','1')"></a>
               <a class="btn btn-default glyphicon glyphicon-minus" onclick="javascript:cart_qty_low('-' ,'<?php echo $key?>' ,'1','1')"></a>
             </div>
         </td>
         <td align="center"><a class="del-btn glyphicon glyphicon-trash" onclick="javascript:cart_del('<?php echo $key?>')"></a></td>
         <td align="right"><?php echo DIO_CURRENCY.addCommas($row['total'])?></td>
       </tr>
     <?php } ?>  
       
       <tr>
         <td colspan="3" align="right" bgcolor="#fbfbfb">小計</td>
         <td colspan="3" align="right" bgcolor="#fbfbfb"><?php echo DIO_CURRENCY.addCommas($this->cls_cart->count_all_total())?></td>
       </tr>
         
   </table>
   
    <?php if($isCategory2 && $isCategory3){ ?>
     <p><h3 style="color:red">*注意:一般商品與助糧商品必需分開購買!</h3></p>
    <?php } ?>
   
      </div>
      
    <?php }else{ ?>  
       <div class="col-md-12 space-top-bm-sm">
         <h3>目前尚無購物資料!</h3>
       </div>  
    <?php } ?>  
      
<!--lightboxs-->
      <div id="modal-directions" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">購物須知
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <div class="editor"><?php echo $_SESSION['sys_info']['rule_shipping_fee']?></div>
            </div>
          </div>
        </div>
      </div>      
<!--/lightbox--> 
      
      
      <div class="col-md-12">
      <?php if( !($isCategory2 && $isCategory3) ){ ?>
         <?php if( !empty($_SESSION['my_order']['cart']) ){ ?>
            <button type="button" class="btn btn-primary btn-lg pull-right" onclick="next_step()">下一步</button>
         <?php } ?>
      <?php } ?>   
         <button type="button" class="btn btn-default btn-lg" onclick="javascript:location.href='<?php echo getUserURL('product/lists?srh_category=1_2&page=1')?>'">回購物</button>
                       我已了解<a href="#" data-toggle="modal" data-target="#modal-directions"><span class="gray">購物須知</span></a>
    
       </div>
    
    </div>
  </div>
   </div>
</div>

<?php $this->load->view('frontend/common/footer.tpl') ?>
<!-- end middle --> 
<?php $this->load->view('frontend/common/footer_js.tpl') ?>
<script>
//--------------------------------------------------------------
//
// 購物車的數量異動(BUTTON)
//
//--------------------------------------------------------------
function cart_qty_low(sign ,cart_id ,o_entity){
	
	if(sign == '+'){
		var entity = parseInt($('#cart_entity_'+cart_id).val()) + parseInt(o_entity);
	}else if(sign == '-'){
		var entity = parseInt($('#cart_entity_'+cart_id).val()) - parseInt(o_entity);
	}else if(sign == '*'){
        var entity = parseInt($('#cart_entity_'+cart_id).val());
	}	
	
	var param = "?cart_id="+cart_id+"&entity="+entity;

	$('#open-process').modal('show');
	
	location.href = '<?php echo getUserURL('buy/edit')?>'+param;
}


//--------------------------------------------------------------
//
// 刪除單筆購物車資料
//
//--------------------------------------------------------------
function cart_del(cart_id){

	var entity = parseInt($('#cart_entity_'+cart_id).val());
	cart_qty_low('-' ,cart_id ,entity);
}

//--------------------------------------------------------------
//
// 下一步(購物須知驗證)
//
//--------------------------------------------------------------
function next_step(){

	location.href='<?php echo getUserURL('checkout/step2')?>' ;
	
	/* 服務條款
	if( $('#agree').is(":checked") ){
		location.href='<?php //echo getUserURL('checkout/step1')?>' ;
	}else{
		alert('<?php //echo $this->lang->line('step1_privacy_chk_ok')?>');
	}*/
	
}

$(document).ready(function() { 
    $("html").niceScroll(); 
  });

/*=================================
||			WOW
==================================*/
wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  )
wow.init();
 </script>
</body>
</html>
