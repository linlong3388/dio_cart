<body>
<?php $this->load->view('frontend/common/menu.tpl') ?>
<?php $this->load->view('frontend/common/breadcrumb.tpl') ?>

<div class="container space-top-bm-lg ">
    <div class="row">
      <div class="col-md-12 sm-text clearfix"><i class="fa fa-shopping-cart"></i> / 購物結果</div>
      
      <div class="col-md-12">
        
      <div class="col-md-12 well">

         <h3>抱歉，訂購失敗!</h3>
         <p><?php echo $query?></p>
        
         <p>
        
        <span style="font-size: 16px">   
           <h4>若有問題請洽客服人員 <?php echo DIO_PHONE?></h4>
        </span>
        
        <br>
              
      </div>
      
      <div class="col-md-12">
  <button type="button" class="btn btn-primary btn-lg pull-right" onclick="location.href='<?php echo getUserURL('index.php')?>'">回首頁</button>
  <button type="button" class="btn btn-primary btn-lg pull-right" onclick="location.href='<?php echo getUserURL('contact/view')?>'">聯絡我們</button>    </form>
    
    </div>
  </div>
   </div>

   <p>
   
<?php $this->load->view('frontend/common/footer.tpl') ?>
<!-- end middle --> 
<?php $this->load->view('frontend/common/footer_js.tpl') ?>
<script>
//---------------------------------------------------                        
//
// 取得地址資料
//
//---------------------------------------------------                        
function default_addr(customer_addr_id){

	var url= DIO_PATH_URL + 'ajax/getCustomerAddr';
	
	$.ajax({
	      url: url,
		  data: { customer_addr_id: customer_addr_id.value }, 
		  type: 'POST',
		  dataType: 'json',
		  success: function(info){    
                   $('input[name="last_name"]').val( info.last_name );  
                   $('input[name="email"]').val( info.email );  
                   $('input[name="mobile"]').val( info.mobile );  
                   $('input[name="phone"]').val( info.phone );  
                   $('select[name="local"]').val( info.local );
                   $('input[name="address"]').val( info.address );
                      
	               return false; 
		  }
	});	  
	
}


                        
$(document).ready(function() { 
    $("html").niceScroll(); 
  });

/*=================================
||			WOW
==================================*/
wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  )
wow.init();
 </script>
</body>
</html>
