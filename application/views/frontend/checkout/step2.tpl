<body>
<?php $this->load->view('frontend/common/menu.tpl') ?>
<?php $this->load->view('frontend/common/breadcrumb.tpl') ?>

<div class="container space-top-bm-lg ">
    <div class="row">
      <div class="col-md-12 sm-text clearfix"><i class="fa fa-shopping-cart"></i> / 購物清單</div>
     
     <?php $this->load->view('frontend/checkout/common/menu.tpl') ?>
      
      <div class="col-md-12">

        <p><strong>選擇送貨地址：</strong><br>
          <br>
              <select onchange="default_addr(this)" class="form-control">
               <option>請選擇:</option>
               <?php foreach ($addr as $row) { ?>
                 <option value="<?php echo $row['customer_addr_id']?>"><?php echo $row['local'] .' / '.$row['address']?></option>
               <?php }?>
             </select>
        </p>
     
       <!-- <p><i class="glyphicon glyphicon-cog"></i> 更新地址</p> -->

      <b style="color: red"><?php echo validation_errors(); ?></b> 
      
      <?php echo form_open( getCurrentFullUrl() )?>        
        
      <div class="col-md-12 well">
      
        <ul id="address_delivery">
          
          <li><b class="dio_star">★</b>購買人： 
              <input type="text" name="last_name" class="form-control" value="<?php echo !empty($_SESSION['my_order']['info']['last_name']) ? $_SESSION['my_order']['info']['last_name'] : set_value('last_name')?>">
          </li>
          
          <li>購買人Email：
              <input type="text" name="email" class="form-control" value="<?php echo !empty($_SESSION['my_order']['info']['email']) ? $_SESSION['my_order']['info']['email'] : set_value('email')?>">
          </li>
          
          <li><b class="dio_star">★</b>購買人手機： 
              <input type="text" name="mobile" class="form-control" value="<?php echo !empty($_SESSION['my_order']['info']['mobile']) ? $_SESSION['my_order']['info']['mobile'] : set_value('mobile')?>" onkeypress="def_numeric()" maxlength="10">
          </li>
          
          <li>購買人電話：
              <input type="text" name="phone" class="form-control" value="<?php echo !empty($_SESSION['my_order']['info']['phone']) ? $_SESSION['my_order']['info']['phone'] : set_value('phone')?>" maxlength="20">
          </li>
          
          <li><b class="dio_star">★</b>購買人區碼：
                 <select name="local" class="form-control">
                  <?php $local = !empty($_SESSION['my_order']['info']['local']) ? $_SESSION['my_order']['info']['local'] : set_value('local');
                        foreach ( reSortArrayMutil(3 ,$local ,getLocalCity()) as $key=>$val){ ?>
                          <option value="<?php echo $key?>"><?php echo $val?></option>
                  <?php } ?>
                 </select>
          </li>
        
          <li><b class="dio_star">★</b>購買人地址：
            <input type="text" name="address" class="form-control" value="<?php echo !empty($_SESSION['my_order']['info']['address']) ? $_SESSION['my_order']['info']['address'] : set_value('address')?>">
          </li>
          
        </ul>
         
      </div>
      
      <div class="col-md-12">
         <button type="submit" class="btn btn-primary btn-lg pull-right">下一步</button>
         <button type="button" class="btn btn-default btn-lg" onclick="location.href='<?php echo getUserURL('checkout/step1#dio_qty')?>'">回步驟1</button>
      </div>
    
    </form>
    
    </div>
  </div>
   </div>

<?php $this->load->view('frontend/common/footer.tpl') ?>
<!-- end middle --> 
<?php $this->load->view('frontend/common/footer_js.tpl') ?>
<script>
//---------------------------------------------------                        
//
// 取得地址資料
//
//---------------------------------------------------                        
function default_addr(customer_addr_id){

	var url= DIO_PATH_URL + 'ajax/getCustomerAddr';
	
	$.ajax({
	      url: url,
		  data: { customer_addr_id: customer_addr_id.value }, 
		  type: 'POST',
		  dataType: 'json',
		  success: function(info){    
                   $('input[name="last_name"]').val( info.last_name );  
                   $('input[name="email"]').val( info.email );  
                   $('input[name="mobile"]').val( info.mobile );  
                   $('input[name="phone"]').val( info.phone );  
                   $('select[name="local"]').val( info.local );
                   $('input[name="address"]').val( info.address );
                      
	               return false; 
		  }
	});	  
	
}


                        
$(document).ready(function() { 
    $("html").niceScroll(); 
  });

/*=================================
||			WOW
==================================*/
wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  )
wow.init();
 </script>
</body>
</html>
