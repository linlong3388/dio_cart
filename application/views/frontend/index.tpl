<body>
<?php $this->load->view('frontend/common/menu.tpl') ?>
<div id="carousel-example-generic" class="carousel slide nopadding" data-ride="carousel"> 
  <!-- Indicators --> 
  
  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
  <?php foreach ($ad as $row){ ?>
    <div class="item">
      <div class="bannershow" style="background:url(<?php echo base_url(DIO_PATH_PIMG.$row['image'])?>) no-repeat;background-position:center; background-size: cover;"> </div>
    </div>
  <?php } ?>  
    <!-- Controls --> 
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> 
    
  </div>
</div>
<!-- end slides --> 
<!-- middle -->

<div class="parallax-a padding-non" data-parallax="scroll" data-image-src="<?php echo getUserFile('images/b01.jpg')?>">
  <div class="container-fluid color-yellow-bk  padding-non">
    <div class="container reflashbox padding-non">
      <div class="col-xs-12 col-md-3 big_txt Margin-top-xs"><i class="glyphicon glyphicon-flash reflashTitle"></i>本月活動/剩餘：</div>
      
      <div class="col-xs-12 col-md-9 padding-non">
        <span class="reflastime col-md-10 col-xs-12 space-top-bm-xs"> 
          <span class="timebox" id="countDown_day">00</span> 天 
          <span class="timebox" id="countDown_hour">00</span> 時 
          <span class="timebox" id="countDown_min">00</span> 分 
          <span class="timebox" id="countDown_sec">00</span> 秒 
        </span>
        
        <span class="col-md-2 col-xs-12 space-top-bm-xs">
            <a class="color-white reflash-btn" href="<?php echo getUserURL('product/lists?srh_category=2_*&page=1')?>">查看活動</a>
        </span>
      </div>
      
    </div>
  </div>

  <div class="space-large container-fluid">
    <div class="container"> 
       <img src="<?php echo base_url(DIO_PATH_PIMG.$xml->blockA->img)?>" width="100%" alt=""/> 
       <img src="<?php echo base_url(DIO_PATH_PIMG.$xml->blockB->img)?>" width="100%" alt=""/></div>
    <div> </div>
  </div>
</div>

<div class="news container space-top-lg">
  <div class="col-md-12 text-center sm-font space-bm"><span class=" txt-line-sm" style="border-bottom:3px solid #fcca3a;padding-bottom:10px; margin-bottom:10px；"><i class="glyphicon glyphicon-gift"></i> 商品清單選擇</span> </div>
  
  <!--商品列表-->
  <div class="row"> 
    <?php foreach ($product as $row){ ?> 
    <div class="col-md-3 wow fadeInDown items" data-wow-delay="0.1s"  data-wow-offset="20"> 
      <a href="<?php echo getUserURL('product/view?srh_category=2_*&product_id='.$row['product_id'])?>"><img src="<?php echo base_url(DIO_PATH_PIMG.$row['image'])?>" class=" Margin-top-bm-xs center-block hvr-grow-rotate all-border-bold"></a>
         <div class="col-md-12 icon-yellow Margin-top-bm-xs space-left" style="font-size:15px;"><?php echo $row['name']?></div>
         <div class="color-gray xs-text txt-line-xs col-md-12 padding-non Margin-bm-xs"><?php echo mb_substr( strip_tags($row['techenique']), 0, 15,"utf-8") ?>... </div>
         <div class=" xs-text txt-line-xs col-md-12 padding-non"> <span>市場售價 <span class="org-sale"><?php echo DIO_CURRENCY.addCommas($row['price_original'])?></span></span> / <span>活動售價 <span class="color-wd-red"><?php echo DIO_CURRENCY.addCommas($row['price'])?></span></span> </div>
      <a href="<?php echo getUserURL('product/view?srh_category=2_*&product_id='.$row['product_id'])?>" class="btn btn-default col-md-6 cartsid Margin-top-bm-xs">查看商品內容</a>
      <a href="<?php echo getUserURL('checkout/step0')?>" class="btn btn-warning col-md-6 addcart Margin-top-bm-xs">加入購物車</a> 
    </div>
    <?php } ?>
  </div>
  
  <a href="<?php echo getUserURL('product/lists?srh_category=2_*&page=1')?>" class="center-block news-more-bk text-center space-top-sm space-bm-xs Margin-top">查看更多</a> </div>

  
  <div class="container-fluid parallax-b space-top-lg padding-non" data-parallax="scroll" data-image-src="<?php echo getUserFile('images/b02.jpg')?>">
  <div class="container">
    <div class="col-md-12 text-center sm-font space-bm"><span class=" txt-line-sm" style="border-bottom:3px solid #fcca3a;padding-bottom:10px; margin-bottom:10px；"><i class="glyphicon glyphicon-heart"></i> 助糧專區</span> </div>
    <div class=" container padding-non">
      
      <!--商品列表-->
     <?php foreach ($product_grain as $row){ ?>  
      <div class="col-md-3 wow fadeInDown items" data-wow-delay="0.1s"  data-wow-offset="20"> 
      <a href="<?php echo getUserURL('product_grain/view?srh_category=3_*&product_id='.$row['product_id'])?>"><img src="<?php echo base_url(DIO_PATH_PIMG.$row['image'])?>" class=" Margin-top-bm-xs center-block hvr-grow-rotate all-border-bold"></a>
         <div class="col-md-12 icon-yellow Margin-top-bm-xs space-left" style="font-size:15px;"><?php echo $row['name']?></div>
         <div class="color-gray xs-text txt-line-xs col-md-12 padding-non Margin-bm-xs"><?php echo mb_substr( strip_tags($row['techenique']), 0, 15,"utf-8") ?>...</div>
         <div class=" xs-text txt-line-xs col-md-12 padding-non"> <span>市場售價 <span class="org-sale"><?php echo DIO_CURRENCY.addCommas($row['price_original'])?></span></span> / <span>活動售價 <span class="color-wd-red"><?php echo DIO_CURRENCY.addCommas($row['price'])?></span></span> </div>
      <a href="<?php echo getUserURL('product_grain/view?srh_category=3_*&product_id='.$row['product_id'])?>" class="btn btn-default col-md-6 cartsid Margin-top-bm-xs">查看贊助內容</a> 
      <a href="<?php echo getUserURL('checkout/step0')?>" class="btn btn-warning col-md-6 addcart Margin-top-bm-xs">贊助商品</a> </div>
     <?php } ?>
   
    </div>
    </div>
     
    <a href="<?php echo getUserURL('product_grain/lists?srh_category=3_*&page=1')?>" class="center-block news-more-bk text-center Margin-top">查看更多</a>
</div>

<?php $this->load->view('frontend/common/footer.tpl') ?>
<!-- end middle --> 
<?php $this->load->view('frontend/common/footer_js.tpl') ?>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script> 
<script src="<?php echo getUserFile('js/stepper.widget.js')?>"></script>
<script src="<?php echo getUserFile('js/dio_plugins/countDown.js')?>"></script>
<script>
//--------------------------------------------------------------
//
// 倒數計時器
//
//--------------------------------------------------------------
countDown("<?php echo str_replace("-","/",$product_category['edate']).' 23:59:59'?>");

$(document).ready(function() { 
    $("html").niceScroll(); 
  });
  $(".navbar-nav li:first").append("<i class='active'></i>");
  $(".item:first,.littlebar:first").addClass('active');
$('.parallax-a').parallax({imageSrc: '<?php echo getUserFile('images/b01.jpg')?>'});
$('.parallax-b').parallax({imageSrc: '<?php echo getUserFile('images/b02.jpg')?>'});
$("span.menu").click(function(){
$(".top-menu ul").slideToggle("" , function(){
});
});

/*=================================
||			WOW
==================================*/
wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  )
wow.init();
 </script>
</body>
</html>
