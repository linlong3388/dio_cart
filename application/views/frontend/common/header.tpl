<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php $this->load->view('widget/meta.tpl')?>
<?php $this->load->view('widget/fb_share.tpl')?>
<meta name="robots" content="all">
<meta name="author" content="">
<meta name="copyright" content="">
<meta name="Distribution" content="Taiwan">
<meta name="revisit-after" CONTENT="1 days" >
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="<?php echo getUserFile('css/bootstrap.css')?>" rel="stylesheet" type="text/css">
<link href="<?php echo getUserFile('css/public.css')?>" rel="stylesheet" type="text/css">
<link href="<?php echo getUserFile('css/mystyle.css')?>" rel="stylesheet" type="text/css">
<link href="<?php echo getUserFile('css/animate.css')?>" rel="stylesheet" type="text/css">
<link href="<?php echo getUserFile('css/hover.css')?>" rel="stylesheet" type="text/css">
<?php if($func == "new_view" || $func == "product_view"){?>
  <link href="<?php echo getUserFile('css/paginationStyles.css')?>" rel="stylesheet" type="text/css">
<?php } ?>
<link href="<?php echo getUserFile('css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">
<link href="<?php echo getUserFile('css/dio_common.css')?>" rel="stylesheet" type="text/css">

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TD8GP6N');</script>
<!-- End Google Tag Manager --><?php $this->load->view('widget/js_vars.tpl')?>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KHKDKMB');</script>
<!-- End Google Tag Manager -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117177946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117177946-1');
</script>
<script type="application/javascript">(function(w,d,t,r,u){w[u]=w[u]||[];w[u].push({'projectId':'10000','properties':{'pixelId':'10057218'}});var s=d.createElement(t);s.src=r;s.async=true;s.onload=s.onreadystatechange=function(){var y,rs=this.readyState,c=w[u];if(rs&&rs!="complete"&&rs!="loaded"){return}try{y=YAHOO.ywa.I13N.fireBeacon;w[u]=[];w[u].push=function(p){y([p])};y(c)}catch(e){}};var scr=d.getElementsByTagName(t)[0],par=scr.parentNode;par.insertBefore(s,scr)})(window,document,"script","https://s.yimg.com/wi/ytc.js","dotq");</script>


<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1778286235543464');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=1778286235543464&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>

