<div class="container-fluid navmember">
<div class="container">
 <div style="float:right;">
 <?php if( !IsLoginCustomer() ) { ?>
     <a href="<?php echo getUserURL('sign_in/register')?>"><i class="glyphicon glyphicon-list-alt"></i> 加入會員</a> 
   │ <a href="<?php echo getUserURL('sign_in/login')?>"><i class="glyphicon glyphicon-user"></i> 會員登入</a>
 <?php }else{ ?>
     <a href="<?php echo getUserURL('customer_order/lists')?>"><i class="glyphicon glyphicon-user"></i> 會員中心</a> 
   │ <a href="<?php echo getUserURL('sign_in/logout')?>"><i class="glyphicon glyphicon-log-out"></i> 登出</a> 
 <?php } ?>
 </div>
</div>
</div>

<nav class="nav-wil padding-non">
  <div class="container">
  <a href="<?php echo getUserURL('index.php')?>" class="logo">
   <img src="<?php echo base_url(DIO_PATH_PIMG.$_SESSION['sys_info']['logo'])?>" class="img-responsive wow fadeInLeft" data-wow-offset="80" width="280px;" style="padding-top:10px;" ></a>
   <div class="top-menu text-center space-top-xs"> <span class="menu"></span>
      <ul class="nav navbar-nav header-nav">
        <li class="hvr-underline-from-center"><a href="<?php echo getUserURL('index/main')?>">首頁</a> </li>
        <li class="hvr-underline-from-center"><a href="<?php echo getUserURL('about/view')?>">關於我們</a> </li>
        <li class="hvr-underline-from-center"><a href="<?php echo getUserURL('product/lists?srh_category=2_*&page=1')?>"><?php echo $this->cls_category->get_1parent()[1]['name']?></a></li>
        <li class="hvr-underline-from-center"><a href="<?php echo getUserURL('product_grain/lists?srh_category=3_*&page=1')?>"><?php echo $this->cls_category->get_1parent()[2]['name']?></a></li>
        <li class="hvr-underline-from-center"><a href="<?php echo getUserURL('qa/view')?>">常見Q&A </a></li>
        <li class="hvr-underline-from-center"><a href="<?php echo getUserURL('contact/view')?>">聯絡我們</a></li>
        <div class="clearfix"> </div>
      </ul>
    </div>
   </div>
</nav>
<!-- Live Chat Widget powered by https://keyreply.com/chat/ -->
<!-- Advanced options: -->
<!-- data-align="left" -->
<!-- data-overlay="true" -->
<div class="fastcart">
<div class="cartword">購物車 <span class="color-gold" style=" font-size:15px;"><?php echo $this->cls_cart->count_all_entity()?></span> 件</div>
  <a href="<?php echo getUserURL('checkout/step0')?>">查看</a>
</div>



<script data-align="right" data-overlay="false" id="keyreply-script" src="//keyreply.com/chat/widget.js" data-color="#E4392B" data-apps="JTdCJTIybGluZSUyMjolMjJodHRwczovL2xpbmUubWUvUi90aS9wLyUyNTQwYm12ODg4NnYlMjIsJTIycGhvbmUlMjI6JTIyMDItODY0Ny0yMjMzJTIyLCUyMmZhY2Vib29rJTIyOiUyMjMxMTA1NDM4Mjc0MzExOCUyMiwlMjJlbWFpbCUyMjolMjJib2JpMTA2MTJAZ21haWwuY29tJTIyJTdE"></script>