<div class="container-fluid nopadding" style=" background:#f5f5f5;">
<div class="container" >

  <ol class="breadcrumb">
       <li><a href="<?php echo getUserURL('index.php')?>">首頁</a></li>
     
     <?php if( getUrlLast2Section() == 'about/view' ){ ?>
       <li class="active">關於我們</li>
     <?php } ?>
     
     <?php if( getUrlLast2Section() == 'sign_in/login' ){ ?>
       <li class="active">會員登入</li>
     <?php } ?>
     
     <?php if( getUrlLast2Section() == 'sign_in/forget' ){ ?>
       <li class="active">忘記密碼</li>
     <?php } ?>
     
     <?php if( getUrlLast2Section() == 'sign_in/register' ){ ?>
       <li class="active">會員註冊</li>
     <?php } ?>
     
     <?php if( getUrlLast2Section() == 'checkout/step1' || getUrlLast2Section() == 'checkout/step2'
     		|| getUrlLast2Section() == 'checkout/step3' || getUrlLast2Section() == 'checkout/step4'){ ?>
       <li class="active">購物清單</li>
     <?php } ?>
     
     <?php if( getUrlLast2Section() == 'contact/view' ){ ?>
       <li class="active">聯絡我們</li>
     <?php } ?>
     
     <?php if( getUrlLast2Section() == 'product/lists' ){ ?>
       <li class="active"><?php echo $this->cls_category->get_1parent()[1]['name']?></li>
     <?php } ?> 
      
     <?php if( getUrlLast2Section() == 'product/view' ){ ?>
       <li><a href="<?php echo getUserURL('product/lists?srh_category=1_2&page=1')?>"><?php echo $this->cls_category->get_1parent()[1]['name']?></a></li>
       <li class="active">詳細內容</li>    
     <?php } ?> 

     <?php if( getUrlLast2Section() == 'product_grain/lists' ){ ?>
       <li class="active"><?php echo $this->cls_category->get_1parent()[2]['name']?></li>
     <?php } ?> 
      
     <?php if( getUrlLast2Section() == 'product_grain/view' ){ ?>
       <li><a href="<?php echo getUserURL('product_grain/lists?srh_category=1_3&page=1')?>"><?php echo $this->cls_category->get_1parent()[2]['name']?></a></li>
       <li class="active">詳細內容</li>    
     <?php } ?> 
     
     <?php if( getUrlLast2Section() == 'qa/view' ){ ?>
       <li class="active">Q&A</li>
     <?php } ?>
     
     <?php if( getUrlLast2Section() == 'order/lists' ){ ?>
       <li class="active">購物清單</li>
     <?php } ?> 
      
     <?php if( getUrlLast2Section() == 'order/view' ){ ?>
       <li><a href="<?php echo getUserURL('order/lists?page=1')?>">購物清單</a></li>
       <li class="active">詳細內容</li>
     <?php } ?> 
      
     <?php if( getUrlLast2Section() == 'checkout_return/success' || getUrlLast2Section() == 'checkout_return/fail' ){ ?>
       <li class="active">訂購結果</li>
     <?php } ?>
    
     <!-- 會員中心  -->  
     <?php if( getUrlLast2Section() == 'customer_order/lists' ){ ?>
       <li>會員中心</li>
       <li class="active">購物紀錄</li>
     <?php } ?>   
      
     <?php if( getUrlLast2Section() == 'customer_order/view' ){ ?>
       <li>會員中心</li>
       <li><a href="<?php echo getUserURL('customer_order/lists')?>">購物紀錄</a></li>
       <li class="active">檢視</li>
     <?php } ?>   
     
     <?php if( getUrlLast2Section() == 'customer/info' ){ ?>
       <li>會員中心</li>
       <li class="active">基本資料</li>
     <?php } ?>   
     
     <?php if( getUrlLast2Section() == 'customer_addr/lists' ){ ?>
       <li>會員中心</li>
       <li class="active">編輯地址</li>
     <?php } ?>   
     
     <?php if( getUrlLast2Section() == 'customer_addr/add' ){ ?>
       <li>會員中心</li>
       <li><a href="<?php echo getUserURL('customer_addr/lists')?>">編輯地址</a></li>
       <li class="active">新增</li>
     <?php } ?>    
     
     <?php if( getUrlLast2Section() == 'customer_addr/edit' ){ ?>
       <li>會員中心</li>
       <li><a href="<?php echo getUserURL('customer_addr/lists')?>">編輯地址</a></li>
       <li class="active">編輯</li>
     <?php } ?>   
     
    <?php if( getUrlLast2Section() == 'customer/pass' ){ ?>
       <li>會員中心</li>
       <li class="active">修改密碼</li>
     <?php } ?>  
     
     <?php if( getUrlLast2Section() == 'customer_return/lists' ){ ?>
       <li>會員中心</li>
       <li class="active">退貨管理</li>
     <?php } ?>   
     
   </ol>
  
  </div>
</div>
