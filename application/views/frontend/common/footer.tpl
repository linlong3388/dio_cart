<div id="footer" class="padding-non">
  <div class="container space-top-bm-lg">
      
      <div class="col-md-3"> NAVIGATION 
         <a href="<?php echo getUserURL('index/main')?>">首頁</a> 
         <a href="<?php echo getUserURL('about/view')?>">關於我們</a> 
         <a href="<?php echo getUserURL('product/lists?srh_category=2_*&page=1')?>"><?php echo $this->cls_category->get_1parent()[1]['name']?></a> 
         <a href="<?php echo getUserURL('product_grain/lists?srh_category=3_*&page=1')?>"><?php echo $this->cls_category->get_1parent()[2]['name']?></a> 
         <a href="<?php echo getUserURL('qa/view')?>"> 常見Q&A </a> 
         <a href="<?php echo getUserURL('contact/view')?>">聯絡我們</a>
      </div>
      
      <div class="col-md-3"> FOLLOW US 
          <a href="https://www.facebook.com/bobilovepet/" Target="_blank">Facebook</a> 
          <!--<a href="http://www.xcellube.com/default.html?location=http://www.xcellube.com/msds/" Target="_blank">Instagram </a>--> 
      </div>
      
      <div class="col-md-3"> MY ACCOUNT 
         <a href="<?php echo getUserURL('contact/view')?>">聯絡管理</a> 
         <a href="<?php echo getUserURL('sign_in/login')?>">會員登入</a> 
      </div>
      
      <div class="col-md-3">OUR LOCATION 
         <a>E-mail：<?php echo $_SESSION['sys_info']['web_email']?></a>
         <a>連絡專線：<?php echo $_SESSION['sys_info']['web_phone']?></a>
         <a>連絡傳真：<?php echo $_SESSION['sys_info']['web_fax']?></a>  
         <a>公司地址：<?php echo $_SESSION['sys_info']['web_addr']?></a>
      </div>

  </div>
</div>
<div id="footer_final"> Copyright © 2018 All right reserved  版權所有 </div>
