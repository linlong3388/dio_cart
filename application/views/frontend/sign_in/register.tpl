<body>
<?php $this->load->view('frontend/common/menu.tpl') ?>
<?php //$this->load->view('frontend/common/banner.tpl') ?>
<?php $this->load->view('frontend/common/breadcrumb.tpl') ?>

<div class="container-fluid nopadding">
  <div class="container space-top-bm-lg">
    <div class="row">
      <div class="col-md-8 space-bm-md clearfix">
        <div class="col-md-12 sm-text space-bm-md"><i class="glyphicon glyphicon-list-alt color-gold"></i> 會員註冊</div>
        <div class="col-md-12 padding-non">
        
            <b style="color: red"><?php echo validation_errors(); ?></b>   
        
            <?php echo form_open( getCurrentFullUrl() ,array('class' => 'form-horizontal') )?>
        
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label"><b class="dio_star">★</b>會員帳號 </label>
              <div class="col-sm-10">
                <input type="text" name="username" class="form-control" id="inputEmail3" placeholder="請輸入會員帳號" value="<?php echo set_value('username')?>">
              </div>
            </div>
        
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label"><b class="dio_star">★</b>會員密碼 </label>
              <div class="col-sm-10">
                <input type="password" name="password" class="form-control" id="inputEmail3" placeholder="密碼需為6-12個字元">
              </div>
            </div>
            
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label"><b class="dio_star">★</b>確認密碼 </label>
              <div class="col-sm-10">
                <input type="password" name="passconf" class="form-control" id="inputEmail3" placeholder="請再確認一次您的密碼">
              </div>
            </div>
        
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label"><b class="dio_star">★</b>姓名 </label>
              <div class="col-sm-10">
                <input type="text" name="last_name" class="form-control" id="inputEmail3" placeholder="請輸入您的姓名" value="<?php echo set_value('last_name')?>">
              </div>
            </div>
            
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">生日</label>
              <div class="col-sm-10">
              
   <div class="col-xs-4">
    <select name="birthday_year" class="form-control">
      <?php foreach ( reSortArrayMutil(1 ,0 ,getBirthDayYear()) as $key=>$val) { ?>      
        	  <option value="<?php echo $key?>"><?php echo $val?></option>
      <?php } ?>
    </select>
  </div>
  
  <div class="col-xs-4">
    <select name="birthday_month" class="form-control">
      <?php foreach ( reSortArrayMutil(1 ,0 ,getBirthDayMonth()) as $key=>$val) { ?>
       		  <option value="<?php echo $key?>"><?php echo addZero($val)?></option>
       <?php } ?>
    </select>
  </div>
  
  <div class="col-xs-4">
    <select name="birthday_day"  class="form-control">
       <?php foreach ( reSortArrayMutil(1 ,0 ,getBirthDayDay()) as $key=>$val) { ?>
             <option value="<?php echo $key?>"><?php echo addZero($val)?></option>
       <?php } ?>
    </select>
  </div>
              </div>
            </div>
            
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label"><b class="dio_star">★</b>Email </label>
              <div class="col-sm-10">
                <input type="email" name="email" class="form-control" id="inputEmail3" placeholder="請輸入您的收件Email" value="<?php echo set_value('email')?>">
              </div>
            </div>
          
            <div class="form-group">
             <label for="inputPassword3" class="col-sm-2 control-label"><b class="dio_star">★</b>手機</label>
             <div class="col-sm-10">
                <input type="text" name="mobile" class="form-control" id="inputPassword3" placeholder="請輸入您的手機" value="<?php echo set_value('mobile')?>" onkeypress="def_numeric()" maxlength="10">
             </div>
            </div>
            
            <div class="form-group">
             <label for="inputPassword3" class="col-sm-2 control-label">安全碼驗證</label>
             <div class="col-sm-10">
                <input type="text" name="secure_code" class="form-control" id="inputPassword3" placeholder="請輸入下方圖示的數字(共四碼)" onkeypress="def_numeric()" maxlength="4">
                <br />
                <img src="<?php echo getUserURL('sign_in/captcha')?>" alt=""/>
             </div>
            </div>
            
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" id="agree" name="agree" > 我已閱讀並同意<a href="#"  data-toggle="modal" data-target="#open-member" class="black">會員隱私權政策</a>
                    </div>
              </div>
            </div>
            <div class="form-group"></div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" onclick="goRegistered()" class="btn btn-danger btn-lg">會員註冊</button>
              </div>
            </div>
          </form>
          
        <!--lightboxs-會員服務條款-->
        <div id="open-member" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">會員服務條款
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                <div class="editor"><?php echo $_SESSION['sys_info']['rule_register']?></div>
              </div>
            </div>
          </div>
        </div>
        <!--/lightbox-會員服務條款--> 
          
        </div>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('frontend/common/footer.tpl') ?>
<?php $this->load->view('frontend/common/footer_js.tpl') ?>
<script>
//--------------------------------------------------------------
//
// 預設載入
//
//--------------------------------------------------------------
$(function(){
    var errMsg = '<?php echo $is_ErrMsg ?>';
    if(errMsg == 1){
       $('#open-errMsg').modal('show');
    }
});   


//--------------------------------------------------------------
//
// 會員隱私權政策
//
//--------------------------------------------------------------
function goRegistered(){

	if( $('#agree').is(":checked") ){
		$('#open-process').modal('show');
	    $('form').submit();
	}else{
		alert('<?php echo '*請閱讀並勾選會員隱私權政策'?>');
	}
}


$(document).ready(function() { 
    $("html").niceScroll(); 
  });
  $(".navbar-nav li").eq(5).append("<i class='active'></i>");
$("span.menu").click(function(){
$(".top-menu ul").slideToggle("" , function(){
});
});

/*=================================
||			WOW
==================================*/
wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  )
wow.init();
 </script>
</body>
</html>
