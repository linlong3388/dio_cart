<body>
<?php $this->load->view('frontend/common/menu.tpl') ?>
<?php //$this->load->view('frontend/common/banner.tpl') ?>
<?php $this->load->view('frontend/common/breadcrumb.tpl') ?>

<div class="container-fluid nopadding">
  <div class="container space-top-bm-lg">
    <div class="row">
   
        <div class="col-md-6 space-bm-md clearfix">
        <div class="col-md-12 sm-text space-bm-md"><i class="glyphicon glyphicon-user color-gold"></i> 會員登入</div>
        <div class="col-md-12 padding-non">
        
        <b style="color: red"><?php echo validation_errors(); ?></b>       
        
        <!-- 登入表單 -->
<?php echo form_open( getCurrentFullUrl() ,array('class' => 'form-horizontal') )?> 
  
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">帳號</label>
    <div class="col-sm-10">
      <input type="text" name="username" class="form-control" id="inputEmail3" value="<?php echo set_value('username')?>" placeholder="請輸入帳號">
    </div>
  </div>
  
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">會員密碼</label>
    <div class="col-sm-10">
      <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="請輸入密碼">
    </div>
  </div>
  
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">安全碼驗證</label>
    <div class="col-sm-10">
      <input type="text" name="secure_code" class="form-control" placeholder="請輸入下方圖示的數字(共四碼)" onkeypress="def_numeric()" maxlength="4">
      <br />
      <img src="<?php echo getUserURL('sign_in/captcha')?>" alt=""/>
    </div>
  </div>
  
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <div class="checkbox">
        <label>
          <input type="checkbox"> 記住我
        </label>
         │ <a href="<?php echo getUserURL('sign_in/forget')?>">忘記密碼?</a>
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <div class="checkbox">
        <a href="<?php echo $fb_login_url?>"><i class="fa fa-facebook"></i> facebook 登入</a> 
  <!--  <a><i class="fa fa-google"></i> google 登入</a>-->
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-success btn-lg">會員登入</button>  
    </div>
  </div>
</form>
        </div>
        </div>
        <div class="col-md-6">
        <div class="col-md-12 sm-text space-bm-md"><i class="glyphicon glyphicon-list-alt color-gold"></i> 會員註冊</div>
         <div class="col-md-12"> <span class="en-word big-text">Join All in</span> <br>
              <a href="<?php echo getUserURL('sign_in/register')?>" class="btn btn-default btn-lg">免費加入bobipet會員</a>
         </div> 
        </div>
    
    </div>
  </div>
</div>

<?php $this->load->view('frontend/common/footer.tpl') ?>
<?php $this->load->view('frontend/common/footer_js.tpl') ?> 
<script>
$(document).ready(function() { 
    $("html").niceScroll(); 
  });
  $(".navbar-nav li").eq(5).append("<i class='active'></i>");
$("span.menu").click(function(){
$(".top-menu ul").slideToggle("" , function(){
});
});

/*=================================
||			WOW
==================================*/
wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  )
wow.init();
 </script>
</body>
</html>
