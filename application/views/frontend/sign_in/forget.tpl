<body>
<?php $this->load->view('frontend/common/menu.tpl') ?>
<?php //$this->load->view('frontend/common/banner.tpl') ?>
<?php $this->load->view('frontend/common/breadcrumb.tpl') ?>


<div class="container-fluid nopadding">
  <div class="container space-top-bm-lg">
    <div class="row">
   
        <div class="col-md-6 space-bm-md clearfix">
        <div class="col-md-12 sm-text space-bm-md"><i class="glyphicon glyphicon-envelope color-gold"></i> 忘記密碼?</div>
        <div class="col-md-12 padding-non">
        
        <b style="color: red"><?php echo validation_errors(); ?></b> 
        
<!-- 登入表單 -->
<?php echo form_open( getCurrentFullUrl() ,array('class' => 'form-horizontal') )?> 
  
   <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">帳號</label>
    <div class="col-sm-10">
      <input type="text" name="username" class="form-control" id="inputEmail3" placeholder="請輸入帳號" value="<?php echo set_value('username')?>">
    </div>
  </div> 
  
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
    <div class="col-sm-10">
      <input type="text" name="email" class="form-control" id="inputEmail3" placeholder="請輸入email" value="<?php echo set_value('email')?>">
    </div>
  </div> 
  
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label"></label>
    <div class="col-sm-10">
       <a href="<?php echo getUserURL('sign_in/login')?>">回 會員登入</a> 
    </div>
  </div> 
    
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-success btn-lg">送出</button>  
    </div>
  </div>
  
</form>

        </div>
        </div>
  
    
    </div>
  </div>
</div>


<!-- end middle -->
<?php $this->load->view('frontend/common/footer.tpl') ?>
<?php $this->load->view('frontend/common/footer_js.tpl') ?> 
<script>
$(document).ready(function() { 
    $("html").niceScroll(); 
  });
  $(".navbar-nav li").eq(5).append("<i class='active'></i>");
$("span.menu").click(function(){
$(".top-menu ul").slideToggle("" , function(){
});
});

/*=================================
||			WOW
==================================*/
wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  )
wow.init();   
</script> 
</body>
</html>
