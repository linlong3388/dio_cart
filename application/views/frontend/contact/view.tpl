<body>
<?php $this->load->view('frontend/common/menu.tpl') ?>
<?php $this->load->view('frontend/common/breadcrumb.tpl') ?>

<div class="news container space-top-lg">
  <div class="col-md-12 text-center sm-font space-bm"><span class=" txt-line-sm" style="border-bottom:3px solid #fcca3a;padding-bottom:10px; margin-bottom:10px；"><i class="glyphicon glyphicon-envelope"></i> 聯絡我們</span> </div><div class="text-center">可在下表中填下您的聯絡資料與留言，我們很樂意為大家服務。</div>
  <div class="row">


  <!--<div class="col-md-12  color-yellow-bk Margin-bm-sm">
    <div class="container reflashbox padding-non"> 
      <div class="col-xs-12 col-md-3 big_txt Margin-top-xs"><i class="glyphicon glyphicon-flash reflashTitle"></i>快閃活動 / 剩餘：</div>
      <div class="col-xs-12 col-md-9"><span class="reflastime col-md-10 col-xs-12 space-top-bm-xs"> <span class="timebox">1</span> 天 <span class="timebox">1</span> 時 <span class="timebox">1</span> 分 <span class="timebox">1</span> 秒 </span><span class="col-md-2 col-xs-12 space-top-bm-xs"><a class="color-white reflash-btn">活動結束將無法下標</a></span></div>
    </div>
  </div>-->
  

     
  </div>

  <div class="container space-top-bm">
    <div class="col-md-4 text-center color-gary space-top-lg"><i class="glyphicon glyphicon-envelope" style="font-size:20em;"></i> <br>
<span class="big-text en-word">Mail To US</span></div>
    <div class="col-md-8 space-top-bm-lg">
      
      <?php if($this->session->flashdata('msg')){ ?>				 
		   <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
      <?php } ?>       
      
      <?php echo form_open( getCurrentFullUrl() ,array('class' => 'form-horizontal') ) ?>
     
       <b style="color: red"><?php echo validation_errors(); ?></b>
      
        <div class="form-group">
          <div class="col-sm-12">
            <input type="text" class="form-control input-lg" id="inputEmail3" name="name" placeholder="您的姓名" value="<?php echo set_value('name')?>" >
          </div>
        </div>
        
        <div class="form-group">
          <div class="col-sm-12">
            <input type="email" class="form-control input-lg" id="inputEmail3" name="email" placeholder="您的信箱" value="<?php echo set_value('email')?>" >
          </div>
        </div>
        
        <div class="form-group">
          <div class="col-sm-12">
            <input type="text" class="form-control input-lg" id="inputPassword3" name="phone" placeholder="聯絡電話" value="<?php echo set_value('phone')?>" >
          </div>
        </div>
        
        <div class="form-group">
          <div class="col-sm-12">
            <textarea class="form-control" rows="5" name="content" placeholder="留言給我..."><?php echo set_value('content')?></textarea>
          </div>
        </div>
        
        <div class="form-group">
          <div class="col-sm-12">
            <button type="submit" class="btn btn-warning btn-lg" >寄送郵件</button>
          </div>
        </div>
        
      </form>
    </div>
  </div>
</div>

<?php $this->load->view('frontend/common/footer.tpl') ?>
<!-- end middle --> 
<?php $this->load->view('frontend/common/footer_js.tpl') ?>
<script>
$(document).ready(function() { 
    $("html").niceScroll(); 
  });
  $(".navbar-nav li").eq(5).append("<i class='active'></i>");
$("span.menu").click(function(){
$(".top-menu ul").slideToggle("" , function(){
});
});

/*=================================
||			WOW
==================================*/
wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  )
wow.init();
 </script>
</body>
</html>
