<body>
<?php $this->load->view('frontend/common/menu.tpl') ?>
<?php $this->load->view('frontend/common/breadcrumb.tpl') ?>

<div class="news container space-top-lg">
  <div class="col-md-12 text-center sm-font space-bm"><span class=" txt-line-sm" style="border-bottom:3px solid #fcca3a;padding-bottom:10px; margin-bottom:10px；"><i class="glyphicon glyphicon-question-sign"></i> Q&A</span> </div>
  <div class="row"><!--<div class="col-md-12  color-yellow-bk Margin-bm-sm">
    <div class="container reflashbox padding-non"> 
      <div class="col-xs-12 col-md-3 big_txt Margin-top-xs"><i class="glyphicon glyphicon-flash reflashTitle"></i>快閃活動 / 剩餘：</div>
      <div class="col-xs-12 col-md-9"><span class="reflastime col-md-10 col-xs-12 space-top-bm-xs"> <span class="timebox">1</span> 天 <span class="timebox">1</span> 時 <span class="timebox">1</span> 分 <span class="timebox">1</span> 秒 </span><span class="col-md-2 col-xs-12 space-top-bm-xs"><a class="color-white reflash-btn">活動結束將無法下標</a></span></div>
    </div>
  </div>-->
  

    <div class="col-md-12 xs-text tw-word txt-line-md space-top-bm-md"><?php echo changeContentImagePath($query['content'])?></div> 
  </div>
  
 <div class="col-xs-12 space-top-bm-lg padding-non"></div></div>

<?php $this->load->view('frontend/common/footer.tpl') ?>
<!-- end middle --> 
<?php $this->load->view('frontend/common/footer_js.tpl') ?>
<script>
$(document).ready(function() { 
    $("html").niceScroll(); 
  });
  $(".navbar-nav li").eq(4).append("<i class='active'></i>");
$("span.menu").click(function(){
$(".top-menu ul").slideToggle("" , function(){
});
});

/*=================================
||			WOW
==================================*/
wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  )
wow.init();
 </script>
</body>
</html>
