    <div class="col-md-3 space-bm-lg text-center ">
        <h1 class="en-word color-gold nopadding">Items</h1>
        <h3 class="nopadding space-bm-md">- 分類清單 -</h3>
        <ul class="all-ul-li-none pro-left-nav">
          <li style='float: left ; width: 100%;'><a style='float: left ; width: 100%;' <?php echo ($func == 'customer_order_lists' || $func == 'customer_order_view' || $func == 'customer_order_returns') ? ' class="btn active"' : ' class="btn"'?> href="<?php echo getUserURL('customer_order/lists')?>">購物記錄</a></li>
          <li style='float: left ; width: 100%;'><a style='float: left ; width: 100%;' <?php echo ($func == 'customer_info') ? ' class="btn active"' : ' class="btn"'?> href="<?php echo getUserURL('customer/info')?>">基本資料</a></li>
          <li style='float: left ; width: 100%;'><a style='float: left ; width: 100%;' <?php echo ($func == 'customer_addr_lists' || $func == 'customer_addr_edit' || $func == 'customer_addr_add') ? ' class="btn active"' : ' class="btn"'?> href="<?php echo getUserURL('customer_addr/lists')?>">編輯地址</a></li>
          <li style='float: left ; width: 100%;'><a style='float: left ; width: 100%;' <?php echo ($func == 'customer_pass') ? ' class="btn active"' : ' class="btn"'?> href="<?php echo getUserURL('customer/pass')?>">修改密碼</a></li>
          <li style='float: left ; width: 100%;'><a style='float: left ; width: 100%;' <?php echo ($func == 'customer_return_lists' || $func == 'customer_return_view') ? ' class="btn active"' : ' class="btn"'?> href="<?php echo getUserURL('customer_return/lists')?>">退貨管理</a></li>
          <li style='float: left ; width: 100%;'><a style='float: left ; width: 100%;' href="<?php echo getUserURL('sign_in/logout')?>">登出</a></li>
        </ul>
      </div> 