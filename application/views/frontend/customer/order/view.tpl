<body>
<?php $this->load->view('frontend/common/menu.tpl') ?>
<?php $this->load->view('frontend/common/breadcrumb.tpl') ?>

<div class="container-fluid nopadding">
  <div class="container space-top-bm-lg">
    <div class="row">

    <?php $this->load->view('frontend/customer/common/menu.tpl') ?>
    
      <div class="col-md-9 space-bm-md clearfix">
        <h3 class="col-md-12 space-bm-md"><i class="glyphicon glyphicon-list-alt color-gold"></i> 購物記錄 / 檢視</h3>
        
        <div class="col-md-12 padding-non">
        
       <h4><b>訂購資料 &nbsp<input type="button" class="btn btn-primary" onClick="location.href='<?php echo getUserURL('customer_order/lists')?>'" value="返回" /></b></h4>
        
        
<form class="form-horizontal">
  
<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">訂單編號</label>
   <div class="col-sm-5"><?php echo $query['order']['master']['order_show_id']?></div>
</div>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">狀態</label>
   <div class="col-sm-5"><?php echo reSortArrayMutil(2 ,$query['order']['master']['status'] ,getOrderStatus())?></div>
</div>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">收件人姓名</label>
   <div class="col-sm-5"><?php echo $query['order']['master']['name']?></div>
</div>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">收件人手機</label>
   <div class="col-sm-5"><?php echo $query['order']['master']['mobile']?></div>
</div>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">收件人電話</label>
   <div class="col-sm-5"><?php echo $query['order']['master']['phone']?></div>
</div>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
   <div class="col-sm-5"><?php echo $query['order']['master']['email']?></div>
</div>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">收件人地址</label>
   <div class="col-sm-5"><?php echo $query['order']['master']['local'].' '.$query['order']['master']['address']?></div>
</div>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">付款方式</label>
   <div class="col-sm-5"><?php echo reSortArrayMutil(2,$query['order']['master']['pay_method'],getPayMethod())?></div>
</div>

<?php if($query['order']['master']['pay_method'] == 5) {?>
<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">轉帳資訊</label>
   <div class="col-sm-5"><?php echo $query['order']['master']['pay_method_content']?></div>
</div>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">轉帳須知</label>
   <div class="col-sm-5">若完成轉帳付款，請至 : <a href="<?php echo getUserURL('contact/view')?>">「聯絡我們」</a>，填寫您的<b>「訂單編號」</b>和<b>「轉帳後四碼」</b>，並送出訊息給管理員，以便查帳，謝謝您。</div>
</div>
<?php } ?>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">總計</label>
   <div class="col-sm-5"><?php echo DIO_CURRENCY.addCommas($query['order']['master']['total'])?></div>
</div>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">備註</label>
   <div class="col-sm-5"><?php echo $query['order']['master']['memo']?></div>
</div>
  
<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">建立日期</label>
   <div class="col-sm-5"><?php echo $query['order']['master']['cdate']?></div>
</div>  

<?php if( $query['order']['master']['pay_method'] == 2 || $query['order']['master']['pay_method'] == 3) { ?>

<h4><b>超商資訊</b></h4>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">店舖編號</label>
   <div class="col-sm-5"><?php echo $query['order']['master']['cvs_store_id']?></div>
</div>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">店舖名稱</label>
   <div class="col-sm-5"><?php echo $query['order']['master']['cvs_store_name']?></div>
</div>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">店舖地址</label>
   <div class="col-sm-5"><?php echo $query['order']['master']['cvs_address']?></div>
</div>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">店舖電話</label>
   <div class="col-sm-5"><?php echo $query['order']['master']['cvs_telephone']?></div>
</div>
           
<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">額外資訊</label>
   <div class="col-sm-5"><?php echo $query['order']['master']['cvs_extra_data']?></div>
</div>                        

<?php } ?>

<?php if( $query['order']['master']['pay_method'] == 2 || $query['order']['master']['pay_method'] == 3) { ?>

<h4><b>物流資訊</b></h4>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">交易編號</label>
   <div class="col-sm-5"><?php echo $query['order']['master']['logistics_no']?></div>
</div>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">狀態訊息</label>
   <div class="col-sm-5"><?php echo $query['order']['master']['logistics_status_msg']?></div>
</div>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">狀態更新時間</label>
   <div class="col-sm-5"><?php echo $query['order']['master']['logistics_status_udate']?></div>
</div>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">物流/寄貨編號</label>
   <div class="col-sm-5"><?php echo $query['order']['master']['logistics_payment_no']?></div>
</div>
           
<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">驗證碼</label>
   <div class="col-sm-5"><?php echo $query['order']['master']['logistics_validation_no']?></div>
</div>                        

<?php } ?>


<h4><b>發票資訊</b></h4>

<?php if( empty($query['order']['master']['ticket_type']) ){ ?>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label"> 發票類型</label>
   <div class="col-sm-5"><?php echo reSortArrayMutil(2,$query['order']['master']['ticket_type'] ,getTicketType())?></div>
</div> 

<?php }else{ ?>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label"> 發票類型</label>
   <div class="col-sm-5"><?php echo reSortArrayMutil(2,$query['order']['master']['ticket_type'] ,getTicketType())?></div>
</div> 

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label"> 發票抬頭</label>
   <div class="col-sm-5"><?php echo $query['order']['master']['ticket3_title']?></div>
</div> 

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label"> 統一編號</label>
   <div class="col-sm-5"><?php echo $query['order']['master']['ticket3_unified']?></div>
</div> 
<?php } ?>
 
</form>  
       
<h4><b>訂購明細</b>
 <?php if($query['order']['master']['status'] == 1){ ?>
   <input type="button" class="btn btn-danger" onClick="orderStatusCancel('<?php echo $query['order']['master']['order_show_id']?>')" value="取消訂單" />
  <?php } ?> 
</h4>       
       
        <table class="table table-bordered" id="orderDetail">
          
          <thead>
            <tr class="gary-bk">
				<th>圖示</th>
				<th>名稱</th>
				<th>功能</th>
				<th>單價</th>
				<th>數量</th>
				<th>小計</th>
            </tr>
          </thead>
          
			<tbody>
			<?php foreach ($query['order']['detail'] as $row){ ?>
			  <tr>
				 <td>
				    <a href="<?php echo getUserURL('product/view?srh_category=1_*&product_id='.$row['product_id'])?>" target="_new"> 
					  <img width="80px" height="80px" src="<?php echo getAdminImg($row['image'])?>"></a>
				 </td>
		
				<td>
				    <a href="<?php echo getUserURL('product/view?srh_category=1_*&product_id='.$row['product_id'])?>" target="_new"> 
				       <?php echo $row['name']?></a>
				       <br />
                       <?php if( isCategory_3($row['category_multi_id']) ) { 
                               $isCategory2 = true;
               	       ?> 
                          <b style="color:blue">( 助糧商品 )</b>
                       <?php }else{ 
               	          $isCategory3 = true;;
               	        ?>
                            ( 一般商品 )
                       <?php }?>
              </td>
				
				<td>
				    <?php if( ($query['order']['master']['status'] == 2) && !isCategory_3( $row['category_multi_id'] )    ){ ?>
				      <button type="button" class="btn btn-primary" onClick="javascript:location.href='<?php echo getUserURL('customer_order/returns?order_id='.$row['order_id'].'&order_detail_id='.$row['order_detail_id'])?>'" >退換</button>
				    <?php }else{ ?>
				       --
				    <?php } ?>
				</td>
				
				<td><?php echo '$'.addCommas($row['price'])?></td>
				<td><?php echo $row['entity'] ?></td>
				<td><?php echo '$'.addCommas($row['total'])?></td>
			</tr>
			
			<?php } ?>
			
			</tbody>
			
			<tfoot>
			
			  <!-- 小計 -->
			  <tr>
			     <td colspan="5">小計:</td>
				 <td colspan="1"><?php echo DIO_CURRENCY.addCommas($query['order']['master']['sub_total'])?></td>
			  </tr>  
			
			  <!-- 運費 -->
			  <?php foreach ($query['order']['promo'] as $row){ ?> 
			  <tr>
			     <td colspan="5"><?php echo $row['name']?>:</td>
				 <td colspan="1"><?php echo DIO_CURRENCY.addCommas($row['total'])?></td>
			  </tr>
			  <?php } ?>
			  
			  <!-- 總計 -->
			  <tr>
			     <td colspan="5">總計:</td>
				 <td colspan="1"><?php echo DIO_CURRENCY.addCommas($query['order']['master']['total'])?></td>
			  </tr>                                                
			</tfoot>
          
        </table>       
       
        </div>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('frontend/common/footer.tpl') ?>
<!-- end middle --> 
<?php $this->load->view('frontend/common/footer_js.tpl') ?>
<script>
//--------------------------------------------------------------
//
// 取消訂單
//
//--------------------------------------------------------------
function orderStatusCancel(order_show_id){

    var chk_box=confirm('確定取消訂單?');
    if (chk_box == true) {
        
        $.ajax({
            type: 'post',
            data: 'order_show_id=' + order_show_id,
            url: '<?php echo getUserURL('ajax/orderStatusCancel')?>',
            success : function(json){
        	       //location.reload();              
                 } 
        });
      
    }
}  

				          
$(document).ready(function() { 
    $("html").niceScroll(); 
  });


/*=================================
||			WOW
==================================*/
wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  )
wow.init();
 </script>
</body>
</html>
