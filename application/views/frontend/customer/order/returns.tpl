    <body class="pro-return">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="wrapper ">
        
            <?php $this->load->view('frontend/common/menu.tpl') ?>
        
            <article class="boxCont bg-light proreturn-box">
              <div class="boxInner w600">
              
                  <div class="columns clearfix">
                  
                      <div class="title">
                          <h2>退換貨申請</h2>
                      </div>
                      <div class="col12-md">
                            <ol>
                                <li>
                                    退換貨申請說明：網路旗艦店客戶享有7天猶豫期之權益(注意!鑑賞期非試用期)，於鑑賞期間 內，若對產品有疑慮，我們可以為您安排辦理退貨;退貨物品需連同正本發票一起由物流公司帶回， 如有多樣商品只退部分東西，待確認退貨後會再開新發票寄送，商品必須是全完整無缺件;物品取回 後進行檢測，檢測後確認無誤，我們將協助您進行退換貨。
                                </li>
                                <li>
                                    注意：很抱歉，我們不接受以下商品之退換貨:已使用過或組裝過之商品、包裝已拆開之床墊、 寢具。
                                </li>
                                <li>
                                    商品顏色皆以實體拍攝，並盡量縮小色差，由於拍攝時的現場光線，以及使用者螢幕廠牌對顏色的顯示略有不同，請以實際商品為主，如真在意色差問題請購買前審慎思考。
                                </li>
                                <li>
                                    若您對於收到的產品不滿意希望退貨，請您詳述原因，以確保符合7天鑑賞期之資格。
                                </li>
                                <li>
                                    提醒您！提出此申請不代表訂單已符合退換貨資格，後續流程將透過客服信箱與您聯繫處理方式。
                                </li>
                                <li>
                                    退換貨範圍以實體產品為限，不包含運費、組裝費及任何形式之服務費用。
                                </li>
                            </ol>
                            
                           <span class="wrong-msg"><?php echo validation_errors(); ?></span>
                            
                           <?php echo form_open( getCurrentFullUrl() )?>

                                <label for="">
                                    <input type="text" style="display: none" name="order_detail_id" placeholder="訂單編號" value="<?php echo $query['order_detail_id']?>" />
                                                                                                訂單編號 : <?php echo $query['order_show_id']?> 
                                </label>                           
                           
                                <label  class="selectBox">
                                    <select name="reason_id">
                                        <?php $reason_id = set_value('reason_id' ,'*');
                                              foreach ( reSortArrayMutil(4 ,$reason_id ,getReasonId() ,'申請原因') as $key=>$val) { ?>
                                          <option value="<?php echo $key?>"><?php echo $val?></option>
                                        <?php } ?>
                                    </select>
                                </label>
                                
                                <label for="">
                                    <input type="text" name="name" placeholder="姓名" value="<?php echo set_value('name' ,$query['name'])?>" />
                                </label>
                              
                                <label for="">
                                    <input type="text" name="phone" placeholder="市內電話" value="<?php echo set_value('phone' ,$query['phone'])?>" />
                                </label>
                                
                                <label for="">
                                    <input type="text" name="mobile" placeholder="手機" value="<?php echo set_value('mobile' ,$query['mobile'])?>">
                                </label>
                                
                                <label for="">
                                    <input type="text" name="email" placeholder="email" value="<?php echo set_value('email' ,$query['email'])?>" />
                                </label>

                                <label for="">
                                    <select name="local">
                                       <?php foreach ( reSortArrayMutil(3 ,set_value('local' ,$query['local']) ,getLocalCity()) as $key=>$val){ ?>       
                                          <option value="<?php echo $key?>"><?php echo $val?></option>
                                       <?php } ?>
                                    </select>
                                </label>     
                                
                                <label for="">
                                    <input type="text" name="address" placeholder="地址" value="<?php echo set_value('address' ,$query['address'])?>" />
                                </label>    
                                
                                <label for=""><?php echo $query['p_name']?> ( <?php echo $query['p_sku']?> )</label>
                                
                                <label for="">
                                    <input type="text" name="entity" placeholder="退(換)貨數量" value="<?php echo set_value('entity' ,1)?>">
                                </label>
                                
                                <textarea name="reason" placeholder="原因"><?php echo set_value('reason')?></textarea>
                                <input type="submit" value="申請退貨">
                            </form>
                      </div>

                  </div>
              </div>               
            </article>
            <?php $this->load->view('frontend/common/footer.tpl') ?>

        </div>
        
        <?php $this->load->view('frontend/common/footer_js.tpl') ?>
    </body>
</html>
