<body>
<?php $this->load->view('frontend/common/menu.tpl') ?>
<?php $this->load->view('frontend/common/breadcrumb.tpl') ?>

<div class="container-fluid nopadding">
  <div class="container space-top-bm-lg">
    <div class="row">

    <?php $this->load->view('frontend/customer/common/menu.tpl') ?>
    
      <div class="col-md-9 space-bm-md clearfix">
        <h3 class="col-md-12 space-bm-md"><i class="fa fa-tags color-gold"></i> 歷史購物清單</h3>
        <div class="col-md-12">
        <table class="table table-bordered">
          
          <thead>
            <tr class="gary-bk">
              <th>訂單編號</th>
              <th>客戶名稱</th>
              <th>總額(含運費)</th>
              <th>訂單狀態</th>
              <th>訂購日期</th>
              <th>功能</th>
            </tr>
          </thead>
          
          <?php foreach ($query as $row){ ?>
             <tr>
                <th scope="row"><?php echo $row['order_show_id']?></th>
                <td valign="middle"><?php echo $row['name']?></td>
                <td valign="middle"><?php echo DIO_CURRENCY.addCommas($row['total']+$row['shipping_fee'])?></td>
                <td valign="middle"><?php echo reSortArrayMutil(2 ,$row['status'],getOrderStatus())?></td>
                <td valign="middle"><?php echo $row['cdate']?></td>
                <td><a class="btn btn-success" href="<?php echo getUserURL('customer_order/view?order_id='.$row['order_id']) ?>">
					   <i class="fa fa-pencil-square-o"></i>檢視</a> 
			    </td>		
             </tr>
            <?php } ?>
            
        </table>
        
      </div>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('frontend/common/footer.tpl') ?>
<!-- end middle --> 
<?php $this->load->view('frontend/common/footer_js.tpl') ?>
<script>
$(document).ready(function() { 
    $("html").niceScroll(); 
  });


/*=================================
||			WOW
==================================*/
wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  )
wow.init();
 </script>
</body>
</html>
