<body>
<?php $this->load->view('frontend/common/menu.tpl') ?>
<?php $this->load->view('frontend/common/breadcrumb.tpl') ?>

<div class="container-fluid nopadding">
  <div class="container space-top-bm-lg">
    <div class="row">

    <?php $this->load->view('frontend/customer/common/menu.tpl') ?>
    
      <div class="col-md-9 space-bm-md clearfix">
        <h3 class="col-md-12 space-bm-md"><i class="glyphicon glyphicon-list-alt color-gold"></i> 修改密碼</h3>
        <div class="col-md-12 padding-non">
         
        <?php if($this->session->flashdata('msg')){ ?>				 
		  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	    <?php } ?>  
         
        <b style="color: red"><?php echo validation_errors(); ?></b> 
        
         <?php echo form_open( getCurrentFullUrl() ,array('class' => 'form-horizontal') )?>
        
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label"><b class="dio_star">★</b>原密碼</label>
              <div class="col-sm-10">
                <input type="password" name="passorig" class="form-control" placeholder="請輸入您的原密碼" />
              </div>
            </div>
        
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label"><b class="dio_star">★</b>新密碼 </label>
              <div class="col-sm-10">
                <input type="password" name="password" class="form-control" placeholder="請輸入您的新密碼" />
              </div>
            </div>
                        
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label"><b class="dio_star">★</b>確認密碼</label>
              <div class="col-sm-10">
                <input type="password" name="passconf" class="form-control" id="inputEmail3" placeholder="再一次確認新密碼" />
              </div>
            </div>
           
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-danger btn-lg">送出</button>
              </div>
            </div>
          
          </form>
          
        </div>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('frontend/common/footer.tpl') ?>
<!-- end middle --> 
<?php $this->load->view('frontend/common/footer_js.tpl') ?>
<script>
$(document).ready(function() { 
    $("html").niceScroll(); 
  });


/*=================================
||			WOW
==================================*/
wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  )
wow.init();
 </script>
</body>
</html>
