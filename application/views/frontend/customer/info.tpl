<body>
<?php $this->load->view('frontend/common/menu.tpl') ?>
<?php $this->load->view('frontend/common/breadcrumb.tpl') ?>

<div class="container-fluid nopadding">
  <div class="container space-top-bm-lg">
    <div class="row">

    <?php $this->load->view('frontend/customer/common/menu.tpl') ?>
    
      <div class="col-md-9 space-bm-md clearfix">
        <h3 class="col-md-12 space-bm-md"><i class="glyphicon glyphicon-list-alt color-gold"></i> 修改會員資料</h3>
        <div class="col-md-12 padding-non">
         
        <?php if($this->session->flashdata('msg')){ ?>				 
		  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	    <?php } ?>  
         
        <b style="color: red"><?php echo validation_errors(); ?></b> 
        
         <?php echo form_open( getCurrentFullUrl() ,array('class' => 'form-horizontal') )?>
        
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">登入類型 </label>
              <div class="col-sm-10">
                <input type="text" name="log_id" class="form-control"  value="<?php echo reSortArrayMutil(2 ,$query['log_id'] ,getLoginType())?>" readonly>
              </div>
            </div>
        
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label">帳號 </label>
              <div class="col-sm-10">
                <input type="text" name="username" class="form-control" value="<?php echo $query['username']?>" readonly>
              </div>
            </div>
        
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label"><b class="dio_star">★</b>姓名 </label>
              <div class="col-sm-10">
                <input type="text" name="last_name" class="form-control" id="inputPassword3" placeholder="請輸入您的姓名" value="<?php echo isset($query['last_name']) ? $query['last_name'] : set_value('last_name')?>">
              </div>
            </div>
        
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">生日</label>
              
              <div class="col-md-4 col-xs-12">
                 <select name="birthday_year" class="form-control">
                   <?php foreach ( reSortArrayMutil(3 ,$query['birthday_year'] ,getBirthDayYear()) as $key=>$val) { ?>
        	          <option value="<?php echo $key?>"><?php echo $val?></option>
                   <?php } ?>
                 </select>
              </div>
              
              <div class="col-md-3 col-xs-12">
                <select name="birthday_month" class="form-control">
                  <?php foreach ( reSortArrayMutil(3 ,$query['birthday_month'] ,getBirthDayMonth()) as $key=>$val) { ?>
       		  		<option value="<?php echo $key?>"><?php echo addZero($val)?></option>
           		  <?php } ?>
         		</select>
              </div>
              
              <div class="col-md-3 col-xs-12">
        		 <select name="birthday_day"  class="form-control">
          			 <?php foreach ( reSortArrayMutil(3 ,$query['birthday_day'] ,getBirthDayDay()) as $key=>$val) { ?>
             		<option value="<?php echo $key?>"><?php echo addZero($val)?></option>
           			<?php } ?>
        		 </select>
             	 </div>
            	
            </div>
                        
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-2 control-label"><b class="dio_star">★</b>收件email </label>
              <div class="col-sm-10">
                <input type="email" name="email" class="form-control" id="inputEmail3" placeholder="請輸入您的收件Email" value="<?php echo !empty($query['email']) ? $query['email'] : set_value('email')?>">
              </div>
            </div>
           
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label"><b class="dio_star">★</b>手機</label>
              <div class="col-sm-10">
                <input type="text" name="mobile" class="form-control" id="inputPassword3" placeholder="請輸入您的手機" value="<?php echo !empty($query['mobile']) ? $query['mobile'] : set_value('mobile')?>">
              </div>
            </div>
           
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-danger btn-lg">送出</button>
              </div>
            </div>
          
          </form>
          
        </div>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('frontend/common/footer.tpl') ?>
<!-- end middle --> 
<?php $this->load->view('frontend/common/footer_js.tpl') ?>
<script>
$(document).ready(function() { 
    $("html").niceScroll(); 
  });


/*=================================
||			WOW
==================================*/
wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  )
wow.init();
 </script>
</body>
</html>
