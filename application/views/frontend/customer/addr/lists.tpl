<body>
<?php $this->load->view('frontend/common/menu.tpl') ?>
<?php $this->load->view('frontend/common/breadcrumb.tpl') ?>

<div class="container-fluid nopadding">
  <div class="container space-top-bm-lg">
    <div class="row">

    <?php $this->load->view('frontend/customer/common/menu.tpl') ?>
    
      <div class="col-md-9 space-bm-md clearfix">
      
        <?php if($this->session->flashdata('msg')){ ?>				 
		   <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	    <?php } ?>  
      
        <h3 class="col-md-12 space-bm-md"><i class="fa fa-tags color-gold"></i> 編輯地址</h3>
        <div class="col-md-12">
        <table class="table table-bordered">
          <caption> <input type="button" class="btn btn-primary" onClick="location.href='<?php echo getUserURL('customer_addr/add')?>'" value="新增" />
          </caption>
          
          <thead>
            <tr class="gary-bk">
			 	<th>收貨人</th>
				<th>手機</th>
				<th>地址</th>
				<th>建立日期</th>
				<th>狀態</th>
				<th style="width: 85px;">功能</th>
            </tr>
          </thead>
          
          <tbody>
			<?php foreach ($query as $row) { ?>
			  <tr>
			 	<td><?php echo $row['first_name'] . $row['last_name']?></td>
				<td><?php echo $row['mobile']?></td>
				<td><?php echo $row['local'] .' / '. $row['address']?></td>
				<td><?php echo $row['cdate']?></td>
			    <td><?php echo $row['status']== 1 ? '啟用' : "<b style='color:red'>停用</b>"?></td>
				<td><a class="myButton small dio_a" href="<?php echo getUserURL('customer_addr/edit?customer_addr_id='.$row['customer_addr_id'])?>">編輯</a>
					<a class="myButton small dio_a" href="javascript:del('<?php echo $row['customer_addr_id'] ?>')">刪除</a>
				</td>
			  </tr>
			<?php } ?>
          </tbody>
            
        </table>
      </div>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('frontend/common/footer.tpl') ?>
<!-- end middle --> 
<?php $this->load->view('frontend/common/footer_js.tpl') ?>
<script>
//-------------------------------------------------------------------
//
// 刪除資料
//
//-------------------------------------------------------------------
function del(customer_addr_id){
    var chk_box=confirm('確定刪除資料?');
    if (chk_box == true) {
          location.href = '<?php echo base_url("customer_addr/del?customer_addr_id=") ?>'+customer_addr_id;
    }
}

$(document).ready(function() { 
    $("html").niceScroll(); 
  });


/*=================================
||			WOW
==================================*/
wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  )
wow.init();
 </script>
</body>
</html>
