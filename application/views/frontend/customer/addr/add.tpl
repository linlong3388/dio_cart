<body>
<?php $this->load->view('frontend/common/menu.tpl') ?>
<?php $this->load->view('frontend/common/breadcrumb.tpl') ?>

<div class="container-fluid nopadding">
  <div class="container space-top-bm-lg">
    <div class="row">

    <?php $this->load->view('frontend/customer/common/menu.tpl') ?>
    
      <div class="col-md-9 space-bm-md clearfix">
        <h3 class="col-md-12 space-bm-md"><i class="glyphicon glyphicon-list-alt color-gold"></i> 編輯地址 / 新增</h3>
        
        <div class="col-md-12 padding-non">

         <?php if($this->session->flashdata('msg')){ ?>				 
		 <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	    <?php } ?> 
        
         <b style="color: red"><?php echo validation_errors(); ?></b> 
        
         <?php echo form_open( getCurrentFullUrl() ,array('class' => 'form-horizontal') )?>
  
<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label"><b class="dio_star">★</b>收件人</label>
     <div class="col-sm-5">
        <input type="text" name="last_name" class="form-control" id="inputPassword3" placeholder="請輸入收件人姓名" value="<?php echo set_value('last_name')?>" />
     </div>
</div>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">收件人Email</label>
     <div class="col-sm-5">
        <input type="text" name="email" class="form-control" id="inputPassword3"placeholder="請輸入收件人Email" value="<?php echo set_value('email')?>" />
     </div>
</div>

        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label"><b class="dio_star">★</b>收件人手機</label>
            <div class="col-sm-5">
              <input type="text" name="mobile" class="form-control" id="inputPassword3" placeholder="請輸入收件人手機" value="<?php echo set_value('mobile')?>" onKeyPress="def_numeric()" maxlength="10"/>
            </div>
        </div> 
        
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">收件人電話</label>
            <div class="col-sm-5">
              <input type="text" name="phone" class="form-control" id="inputPassword3" placeholder="請輸入收件人電話" value="<?php echo set_value('phone')?>" onKeyPress="def_numeric()" />
            </div>
        </div>
     
       <div class="form-group">
         <label for="inputPassword3" class="col-sm-2 control-label"><b class="dio_star">★</b>收件人地址</label>
           <div class="col-sm-10">
             <div class="row">
               <div class="col-xs-4">
                 <select name="local" class="form-control">
                  <?php foreach ( getLocalCity() as $key=>$val){ ?>
                          <option value="<?php echo $key?>"><?php echo $val?></option>
                  <?php } ?>
                 </select>
               </div>                                     
         
               <div class="col-xs-8">
                <input type="text" name="address" class="form-control" id="inputPassword3" placeholder="請輸入收件人/地址" value="<?php echo set_value('address')?>" />
               </div>
             </div>
          </div>
       </div>        
         
        <div class="form-group">
          <div class="col-sm-10">
            <div class="col-sm-10">
               <button type="submit" class="btn btn-primary">送出</button>
               <input type="button" class="btn btn-primary" onClick="location.href='<?php echo getUserURL('customer_addr/lists')?>'" value="返回" />
            </div>
          </div>
        </div>

</form>  
        
       
        </div>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('frontend/common/footer.tpl') ?>
<!-- end middle --> 
<?php $this->load->view('frontend/common/footer_js.tpl') ?>
<script>
//--------------------------------------------------------------
//
// 取消訂單
//
//--------------------------------------------------------------
function orderStatusCancel(order_show_id){

    var chk_box=confirm('確定取消訂單?');
    if (chk_box == true) {
        
        $.ajax({
            type: 'post',
            data: 'order_show_id=' + order_show_id,
            url: '<?php echo getUserURL('ajax/orderStatusCancel')?>',
            success : function(json){
        	       location.reload();              
                 } 
        });
      
    }
}  

				          
$(document).ready(function() { 
    $("html").niceScroll(); 
  });


/*=================================
||			WOW
==================================*/
wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  )
wow.init();
 </script>
</body>
</html>
