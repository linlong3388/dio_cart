<body>
<?php $this->load->view('frontend/common/menu.tpl') ?>
<?php $this->load->view('frontend/common/breadcrumb.tpl') ?>

<div class="container-fluid nopadding">
  <div class="container space-top-bm-lg">
    <div class="row">

    <?php $this->load->view('frontend/customer/common/menu.tpl') ?>
    
      <div class="col-md-9 space-bm-md clearfix">
        <h3 class="col-md-12 space-bm-md"><i class="glyphicon glyphicon-list-alt color-gold"></i> 退貨管理 / 檢視</h3>
        
        <div class="col-md-12 padding-non">
        
       <h4><b>退換資訊 &nbsp<input type="button" class="btn btn-primary" onClick="location.href='<?php echo getUserURL('customer_return/lists?id='.uniqid().'&page=1')?>'" value="返回" /></b></h4>
        
        
<form class="form-horizontal">
  
<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">訂單編號</label>
   <div class="col-sm-5">
      <a href="<?php echo getUserURL('customer_order/view?order_id='.$query['order_id'])?>" target="_blank" 
      style="color: blue; text-decoration: underline;"><?php echo get_assign_field('`order`', 'order_id', $query['order_id'], 'order_show_id')?></a>
   </div>
</div>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">退貨編號</label>
   <div class="col-sm-5"><?php echo $query['return_id']?></div>
</div>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">狀態</label>
   <div class="col-sm-5"><?php echo get_return_status( 2,$query['status'])?></div>
</div>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">姓名</label>
   <div class="col-sm-5"><?php echo $query['name']?></div>
</div>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">手機</label>
   <div class="col-sm-5"><?php echo $query['mobile']?></div>
</div>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
   <div class="col-sm-5"><?php echo $query['email']?></div>
</div>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">地址</label>
   <div class="col-sm-5"><?php echo $query['local'].' '.$query['address']?></div>
</div>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">商品名稱</label>
   <div class="col-sm-5"><?php echo $query['p_name']?> ( <?php echo $query['p_sku']?> )</div>
</div>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">退(換)貨數量</label>
   <div class="col-sm-5"><?php echo $query['entity']?></div>
</div>

<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">退(換)貨原因</label>
   <div class="col-sm-5"><?php echo $query['reason']?></div>
</div>
  
<div class="form-group">
   <label for="inputEmail3" class="col-sm-2 control-label">建立日期</label>
   <div class="col-sm-5"><?php echo $query['cdate']?></div>
</div>  
 
</form>  
       
        </div>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('frontend/common/footer.tpl') ?>
<!-- end middle --> 
<?php $this->load->view('frontend/common/footer_js.tpl') ?>
<script>
$(document).ready(function() { 
    $("html").niceScroll(); 
  });


/*=================================
||			WOW
==================================*/
wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  )
wow.init();
 </script>
</body>
</html>
