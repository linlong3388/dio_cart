<body>
<?php $this->load->view('frontend/common/menu.tpl') ?>
<?php $this->load->view('frontend/common/breadcrumb.tpl') ?>

<div class="container-fluid nopadding">
  <div class="container space-top-bm-lg">
    <div class="row">

    <?php $this->load->view('frontend/customer/common/menu.tpl') ?>
    
      <div class="col-md-9 space-bm-md clearfix">
        <h3 class="col-md-12 space-bm-md"><i class="fa fa-tags color-gold"></i> 退貨管理</h3>
        <div class="col-md-12">
        
        <?php if($this->session->flashdata('msg')){ ?>				 
		   <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	    <?php }elseif($this->session->flashdata('msg_err')){ ?>
		   <div class="alert alert-danger"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg_err')?></div>
	    <?php } ?>  
        
        <table class="table table-bordered">
          
          <thead>
            <tr class="gary-bk">
               <th>退換編號</th>
               <th>客戶姓名</th>
               <th>商品名稱</th>
               <th>狀態</th>
               <th>建立日期</th>
               <th>功能</th>
            </tr>
          </thead>
          
          <?php foreach ($query as $row){ ?>
       <tr>
         <td><?php echo $row['return_id']?></td>
         <td><?php echo $row['name']?></td>
         <td width="50%"><?php echo $row['p_name']?></td>
         <td><?php echo get_return_status( 2,$row['status'])?></td>
         <td><?php echo substr($row['cdate'],0,10)?></td>
         <td><a href="<?php echo getUserURL('customer_return/view?return_id='.$row['return_id'])?>">檢視</a></td>
        </tr>
            <?php } ?>
            
        </table>
      </div>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('frontend/common/footer.tpl') ?>
<!-- end middle --> 
<?php $this->load->view('frontend/common/footer_js.tpl') ?>
<script>
$(document).ready(function() { 
    $("html").niceScroll(); 
  });


/*=================================
||			WOW
==================================*/
wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  )
wow.init();
 </script>
</body>
</html>
