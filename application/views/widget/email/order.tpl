<html>
<head>
<title><?php echo $info['subject'] ?></title>
<style type="text/css">
table {
    border-collapse: collapse;
}

table, th, td {
    border: 1px solid black;
}
</style>
</head>
	
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">
  <p><h2>您有一筆來自【<?php echo $info['company'] ?>】的訂單通知</h2><p>
  
  <table border="1" cellpadding="1" width="100%">
     
     <tr> 
        <td width="20%">訂單狀態</td>
        <td><?php echo reSortArrayMutil(2,$info['data']['order']['master']['status'],getOrderStatus())?></td>
     </tr>
     
     <tr> 
        <td>訂單編號</td>
        <td><?php echo $info['data']['order']['master']['order_show_id']?></td>
     </tr>   
  
     <tr> 
        <td>訂購人姓名</td>
        <td><?php echo setDataToStar($info['data']['order']['master']['name'], '*', 1, 1) ?></td>
     </tr>  
  
     <tr> 
        <td>訂購人手機</td>
        <td><?php echo setDataToStar($info['data']['order']['master']['mobile'],'*****', 3, 5)?></td>
     </tr>
     
     <tr> 
        <td>訂購人電話</td>
        <td><?php echo setDataToStar($info['data']['order']['master']['phone'],'*****', 3, 5)?></td>
     </tr>

     <tr> 
        <td>訂購人Email</td>
        <td><?php echo $info['data']['order']['master']['email']?></td>
     </tr>
     
     <tr> 
        <td>訂購人地址</td>
        <td><?php echo setDataToStar($info['data']['order']['master']['local'],'***', 0, 8).' '.setDataToStar($info['data']['order']['master']['address'],'******', 0, 4)?></td>
     </tr>  
     
     <tr> 
        <td>發票類型</td>
        <td><?php if( empty($info['data']['order']['master']['ticket_type']) ){ ?>
              <?php echo reSortArrayMutil(2,$info['data']['order']['master']['ticket_type'] ,getTicketType())?>
           <?php }else{ ?>
                                      發票抬頭 : <?php echo $info['data']['order']['master']['ticket3_title']?> <br>
                                      統一編號 : <?php echo $info['data']['order']['master']['ticket3_unified']?>
           <?php } ?>
        </td>
     </tr>
     
     <tr> 
        <td>付款方式</td>
        <td><?php echo reSortArrayMutil(2,$info['data']['order']['master']['pay_method'],getPayMethod())?></td>
     </tr>

     <?php if($info['data']['order']['master']['pay_method'] == 5) {?>
       <tr> 
           <td>轉帳資訊</td>
           <td><?php echo $info['data']['order']['master']['pay_method_content']?></td>
       </tr>   
       <tr> 
           <td>轉帳須知</td>
           <td><b class="dio_star">★</b>若完成轉帳付款，請至 : <a href="<?php echo getUserURL('contact/view')?>">「聯絡我們」</a>，填寫您的<b>「訂單編號」</b>和<b>「轉帳後四碼」</b>，並送出訊息給管理員，以便查帳，謝謝您。<br/></td>
       </tr>   
       
     <?php } ?>
     
     <tr> 
        <td>備註</td>
        <td><?php echo $info['data']['order']['master']['memo']?></td>
     </tr>   
     
     <tr>
        <td>購物清單</td>
        <td>
  
                  <table class="order-list table">
                    <tr>
                      <th width="26%">商品</th>
                      <th width="30%">名稱</th>
                      <th width="14%">單價</th>
                      <th width="15%">數量</th>
                      <th width="15%">小計</th>
                    </tr>
                    
                   <?php foreach ($info['data']['order']['detail'] as $row){ ?> 
                    <tr class="alert">
                      
                      <?php if( $row['type_id'] == 1) {?>
                         <td><div class="order-img"><img src="<?php echo base_url(DIO_PATH_PIMG.'/'.$row['image'])?>" alt="商品圖" class="img-responsive" width="180px" height="150px" /></div></td>
                         <td><p class="list-title"><?php echo $row['name']?>
                      <?php }else{ ?>
                         <td><div class="order-img"><a href="<?php echo getUserURL('product/view?srh_category=1_*&product_id='.$row['product_id'])?>"><img src="<?php echo base_url(DIO_PATH_PIMG.'/'.$row['image'])?>" alt="商品圖" class="img-responsive"  width="180px" height="150px" /></a></div></td>
                         <td><p class="list-title"><a href="<?php echo getUserURL('product/view?srh_category=1_*&product_id='.$row['product_id'])?>"><?php echo $row['name']?></a>
                      <?php } ?>
                      
                         <?php if( !empty($row['box']) ){?>
                               <br/>
                         <?php foreach ($row['box'] as $key=>$val) { ?>
                                <b><?php echo $val['pname'].'x'.$val['entity']?></b>  
                         <?php } ?>
                         <?php } ?>
                         </p></td>
                      <td><p class="price"><?php echo DIO_CURRENCY.addCommas($row['price'])?></p></td>
                      <td><p class="number"><?php echo $row['entity']?></p></td>
                      <td><p class="price"><?php echo DIO_CURRENCY.addCommas($row['total'])?></p></td>
                    </tr>
                    <?php } ?>
                  
                    <!-- 運費  -->
                    <?php foreach ($info['data']['order']['promo'] as $row){ ?> 
                     <tr>
                      <td>&nbsp;</td>
                      <td><p class="list-title"><?php echo $row['name']?></p></td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td><p class="price"><?php echo $row['item_type'].DIO_CURRENCY.addCommas($row['total'])?></p></td>
                    </tr>
                    <?php } ?>
                  
                    <!-- 總額 -->
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td><p class="total-price"><?php echo DIO_CURRENCY.addCommas($info['data']['order']['master']['total'])?></p></td>
                    </tr>
                  
                  </table>
        
        <tr> 
           <td>注意事項</td>
           <td><span><b style="color:red">※因詐騙集團猖獗，【<?php echo $info['company'] ?>】絕不會以電話或簡訊等任何方式要求您提供信用卡等個人資料，或請您到ATM進行任何操作，如您接獲疑似詐騙電話請勿理會，如有疑問請洽客服或撥打168防詐騙專線，以防受騙。</b></span><br/>
			               注意事項：<br/>
			      <!-- 
			      ■兌換或訂購商品將於訂單正式成立起約7~14個工作天內寄出。<br/> -->
			      ■若有問題請聯絡客服 : <?php echo DIO_EMAIL ?>。<br/>
			      ■自105年1月1日起，生鮮食品不適用於消費者保護法第19條規定，因此不提供7天鑑賞期。除商品本身瑕疵或運送過程而造成的損毀，才可接受您的退貨申請。非商品瑕疵恕無法為您辦理退換貨，一經拆封、食用、失溫及保存不良等情形而導致商品變質，則無法處理您的退換貨申請，敬請見諒！<br/>
		          ■更多訊息查詢，請至<a href=<?php echo base_url('index.php')?> target="_new">「系統中心」</a>。</td>
        </tr>  
     
     
        </td>
     </tr>
  </table>
               
</body>
</html>