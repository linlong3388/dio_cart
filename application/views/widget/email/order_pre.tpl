<html>
<head>
<title><?php echo $info['subject'] ?></title>
</head>
	
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">
  <p><h2>您有一筆來自【<?php echo $info['company'] ?>】的預購單通知</h2><p>
  <p><h3>底下是預購單相關訊息。</h3></p>
     <table border="1" cellpadding="1" width="100%">
       
       <tr>
           <td colspan="2" align="center">商品資料</td>
           <td></td>
       </tr>
       
       <tr>
           <td>名稱</td>
           <td><?php echo $name?></td>
       </tr>
       <tr>
           <td>數量</td>
           <td><?php echo $name?></td>
       </tr>
       
       
       <tr>
           <td colspan="2" align="center">預購人資料</td>
           <td></td>
       </tr>
       
       <tr>
           <td>姓名</td>
           <td><?php echo $name?></td>
       </tr>
       <tr>
           <td>電話</td>
           <td><?php echo $phone?></td>
       </tr>
       <tr>
           <td>電子郵件</td>
           <td><?php echo $email?></td>
       </tr>
          <tr>
           <td>地址</td>
           <td><?php echo $local?><?php echo $address?></td>
       </tr>
          
      </table>
	
  <p><h3>■更多訊息查詢，請至<a href=<?php echo $info['webpage']?>/backend/login/valid target=\"_new\">「系統中心」</a>查詢。</h3></p>
     
</body>
</html>