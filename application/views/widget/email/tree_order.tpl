<html>
<head>
<title><?php echo $info['subject'] ?></title>
</head>
	
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">
  <p><h2>您有一筆來自【<?php echo $info['company'] ?>】的認養果樹 訂購單通知</h2><p>
  <p><h3>底下是 訂購單的相關訊息。</h3></p>
     <table border="1" cellpadding="1" width="100%">
       
       <tr>
           <td colspan="2" align="center">企業贊助與認養果樹 訂購單</td>          
       </tr>

       <tr>
           <td>訂單編號</td>
           <td><?php echo $tree_order_id?></td>
       </tr>
       
       <tr>
           <td>果樹編號</td>
           <td><?php echo $t_sku?></td>
       </tr>
       
       <tr>
           <td>名稱</td>
           <td><?php echo $t_name?></td>
       </tr>
       
       <tr>
           <td>價格</td>
           <td><?php echo DIO_CURRENCY.addCommas($t_price)?></td>
       </tr>
       
       
       <tr>
           <td colspan="2" align="center">聯絡人資料</td>
       </tr>
       
       <tr>
           <td>統一編號</td>
           <td><?php echo setDataToStar($unified,'****', 0, 4)?></td>
       </tr>
       <tr>
           <td>企業名稱</td>
           <td><?php echo setDataToStar($bname,'**', 0, 2)?></td>
       </tr>
       <tr>
           <td>姓名</td>
           <td><?php echo setDataToStar($name,'*', 1, 1)?></td>
       </tr>
       <tr>
           <td>電話</td>
           <td><?php echo setDataToStar($phone,'****', 0, 4)?></td>
       </tr>
       <tr>
           <td>電子郵件</td>
           <td><?php echo $email?></td>
       </tr>
       <tr>
           <td>備註事項</td>
           <td><?php echo $memo?></td>
       </tr>
   
      </table>
	
  <p><h3>■更多訊息查詢，請至<a href=<?php echo $info['webpage']?> target=\"_new\">「系統中心」</a>查詢。</h3></p>
     
</body>
</html>