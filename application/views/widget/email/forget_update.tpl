<html>
<head>
<title><?php echo $info['subject'] ?></title>
</head>

<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">
    <p><h2><?php echo $info['subject'] ?></h2><p>
    <p>請使用新的密碼登入網站會員區，如需修改密碼，請至會員專區重新設定您的密碼，並妥善保存！</p>
    <p>要是您未曾變更過您的密碼，則表示您的帳戶可能已經遭到入侵。如要取回帳戶存取權，</p>
    <p>請聯繫(04)2371-0511 玉鼎法律事務所網站管理人員。</p>
 </body>
</html>
