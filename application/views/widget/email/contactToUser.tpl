<html>
<head>
<title>您有一筆來自 【<?php echo $info['company'] ?>】的訊息</title>
</head>
	
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">
    <p><h2>來自【<?php echo $info['company'] ?>】最新訊息通知</h2><p>
    <p><h3>您好，您提問的問題已獲得回應。</h3></p>
                <table border="1" cellpadding="1" width="100%">
                    <tr>
                        <td>姓名</td>
                        <td><?php echo setDataToStar($name, '*', 1, 1) ?></td>
                    </tr>
                    <tr>
                        <td>電話</td>
                        <td><?php echo setDataToStar($phone,'*****', 3, 5) ?></td>
                    </tr>
                    <tr>
                        <td>電子郵件</td>
                        <td><?php echo $email ?></td>
                    </tr>
                    <tr>
                        <td>訊息</td>
                        <td><?php echo $content ?></td>
                    </tr>
                    <tr>
                        <td>管理員回應</td>
                        <td><?php echo $memo_admin ?></td>
                    </tr>
                </table>
	
       <p><h3> ■若有問題請聯絡客服 : <?php echo DIO_EMAIL ?>。</h3></p>
                 
</body>
</html>