<html>
<head>
<title>您有一筆來自 【<?php echo $info['company'] ?>】的訊息</title>
</head>
	
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">
    <p><h2>來自【<?php echo $info['company'] ?>】最新訊息通知</h2><p>
    <p><h3>底下是提問訊息和提問人的相關資料。</h3></p>
                <table border="1" cellpadding="1" width="100%">
                    <tr>
                        <td>姓名</td>
                        <td><?php echo $name ?></td>
                    </tr>
                    <tr>
                        <td>電話</td>
                        <td><?php echo $phone ?></td>
                    </tr>
                    <tr>
                        <td>電子郵件</td>
                        <td><?php echo $email ?></td>
                    </tr>
                    <tr>
                        <td>訊息</td>
                        <td><?php echo $content ?></td>
                    </tr>
                </table>
	
       <p><h3>■更多訊息查詢，請至<a href=<?php echo $info['webpage']?>backend/login/valid target=\"_new\">「系統中心」</a>查詢。</h3></p>
                 
</body>
</html>