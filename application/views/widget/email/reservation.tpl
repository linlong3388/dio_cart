<html>
<head>
<title><?php echo $info['subject'] ?></title>
</head>

<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">
    <p><h2><?php echo $info['subject'] ?></h2><p>
    <p>您的預約資料如下：</p>
    <p><b>預約人：</b><?php echo $info['c_name']?></p>
    <p><b>電話：</b><?php echo $info['c_phone']?></p>
    <p><b>E-mail：</b><?php echo $info['c_email']?></p>
    <p><b>聯絡地址：</b><?php echo $info['c_addr']?></p>
    
    <p><b>日期/時段：</b><span class="yellow"><?php echo substr($info['redate'],0,10)?>&nbsp;/&nbsp;<?php echo getReservationTime($info['retime'])?></span></p>
    <p><b>律師：</b><?php echo $info['l_name']?>律師</p>
    <p><b>預約地點：</b><?php echo $info['b_title']?><br>
<div class="bg_gray_light padding_8"><?php echo $info['b_phone']?>&nbsp;/&nbsp;<a href="https://www.google.com.tw/maps/place/403%E5%8F%B0%E4%B8%AD%E5%B8%82%E8%A5%BF%E5%8D%80%E4%B8%89%E6%B0%91%E8%B7%AF%E4%B8%80%E6%AE%B593%E8%99%9F/@24.1328936,120.6672224,17z/data=!3m1!4b1!4m2!3m1!1s0x34693d0631b56881:0x42bdf1fa5db7258a" target="_blank"><?php echo $info['b_addr']?></a></div></p>
    <p><b>諮詢內容：</b><?php echo $info['rc_content']?></p>
    
</body>
</html>