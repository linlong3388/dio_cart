<?php 
      $fb_default_title       = DIO_COMPANY;
      $fb_default_description = DIO_COMPANY;
      $fb_default_image       = getUserFile('img/tile.png');
?>

<meta property="og:url" content="<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"?>" />
<meta property="og:title" content="<?php echo isset($query_fb['og_title']) ? $query_fb['og_title'] : $fb_default_title?>" />
<meta property="og:description" content="<?php echo isset($query_fb['og_description']) ?  mb_substr( strip_tags($query_fb['og_description']),0,100,"utf-8") : $fb_default_description?>" />
<meta property="og:image" content="<?php echo isset($query_fb['og_image']) ? $query_fb['og_image'] : $fb_default_image?>" />
<!-- 
<meta property="og:image:width" content="80" />
<meta property="og:image:height" content="100" />
 -->
