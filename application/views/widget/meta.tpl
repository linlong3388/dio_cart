    <?php //優先指定Meta
          if( !empty($this->global_meta['first']) ) { ?>
	  <title><?php echo $this->global_meta['first']['meta_title']?></title>
	  <meta name="description" content="<?php echo $this->global_meta['first']['meta_description']?>">
	  <meta name="keywords" content="<?php echo $this->global_meta['first']['meta_keyword']?>">

    <?php //分類Meta
         }elseif( !empty($query_meta['category']) ) { ?>
	  <title><?php echo $query_meta['category']['meta_title']?></title>
	  <meta name="description" content="<?php echo $query_meta['category']['meta_description']?>">
	  <meta name="keywords" content="<?php echo $query_meta['category']['meta_keyword']?>">
    
   <?php  //內頁Meta
          }elseif( !empty($query_meta['content']) ){ ?>
	  <title><?php echo $query_meta['content']['meta_title']?></title>
	  <meta name="description" content="<?php echo $query_meta['content']['meta_description']?>">
	  <meta name="keywords" content="<?php echo $query_meta['content']['meta_keyword']?>">
	
   <?php  //其他頁Meta(for xml格式)
          }elseif( !empty($query_meta['other']) ) { ?>
	  <title><?php echo $xml->meta->title?></title>
	  <meta name="description" content="<?php echo $xml->meta->description?>">
	  <meta name="keywords" content="<?php echo $xml->meta->keyword?>">
	
   <?php }else{ ?>
	  <!-- 預設Meta -->
	  <title><?php echo $_SESSION['sys_info']['meta_title']?></title>
	  <meta name="description" content="<?php echo $_SESSION['sys_info']['meta_description'] ?>">
	  <meta name="keywords" content="<?php echo $_SESSION['sys_info']['meta_keyword'] ?>">
	<?php } ?>