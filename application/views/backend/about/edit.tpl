<body>

<style>
.fa-star {
	color: red
}
td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'about' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			
			 <?php if($this->session->flashdata('msg')){ ?>				 
		        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	         <?php } ?> 
			
			 <b style="color: red"><?php echo validation_errors(); ?></b> 			
			
			<?php echo form_open( getCurrentFullUrl() )?>
				<table>
					<tr>
						<th colspan="2">編輯資料</th>
					</tr>

					<tr style="display: none">
						<th width="20%">編號</th>
						<td><input type="text" name="about_id" value="<?php echo isset($query['about_id']) ? $query['about_id'] : set_value('about_id')?>" readonly></td>
					</tr>
					
					<!-- 
					<tr>
						<th width="20%">分類</th>
						<td><select name="content_category_id">
						      <?php //foreach ( reSortArrayMutil(3, $query['content_category_id'] ,getContentCategory()) as $key=>$val ){ ?>
						        <option value="<?php //echo $key?>"><?php //echo $val?></option>
						      <?php //} ?>
						    </select>
					</tr>	
					 -->
					
					<!-- 
					<tr>
						<th width="20%"><i class="fa fa-star"></i>標題</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="title" value="<?php //echo isset($query['title']) ? $query['title'] : set_value('title')?>"></td>
					</tr>
					 -->	
					
					<!-- 
					<tr id="hd_image">
						<th width="20%">圖片</th>
						<td><input style="display: none" class="input-medium focused" id="specification_image" name="image" value="<?php //echo isset($query['image']) ? $query['image'] : set_value('image') ;?>">
     	                    <div class="span12">
			                   <img id="image" class="img-responsive" width="300px" height="250px"  src="<?php //echo (!empty($query['image'])) ? base_url("resources/uploads/".$query['image']) : base_url("resources/uploads").'/'.set_value('image')?>" alt="圖片"/>
                                  <p>                                                       
                                     <a onclick="base_image_upload('image');">選擇圖片</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                     <a onclick="base_image_delete('image');">刪除圖片</a>
                                  </p>
		                     </div>          
						</td>
					</tr>
					 -->	

				   <!-- 
				    <tr>
						<th width="20%">排序</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="sort_order" value="<?php //echo isset($query['sort_order']) ? $query['sort_order'] : set_value('sort_order')?>" onkeypress="def_numeric()"></td>
					</tr>			
									
					<tr>
						<th width="20%"><i class="fa fa-star"></i>狀態</th>
						<td>
						    <input type="radio" name="status" value="1"
						    <?php //if( isset($query['status']) && ($query['status'] == '1') ) echo set_radio('status', '1' ,true);
						      //else echo set_radio('status', '1'); ?>>啟用 
						    <input type="radio" name="status" value="0"
							<?php //if( isset($query['status']) && ($query['status'] == '0') ) echo set_radio('status', '0' ,true);
							  //else echo set_radio('status', '0'); ?>>停用
						</td>
					</tr>
					 -->
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>內容</th>
						<td><textarea class="fullwidth dio_textarea" rows="3" name="content" placeholder="請輸入資料"><?php echo isset($query['content']) ? $query['content'] : set_value('content')?></textarea></td>
					</tr>

					<!-- 
					<tr>
						<th width="20%">建立日期</th>
						<td><input class="fullwidth" type="text" name="cdate" value="<?php //echo isset($query['cdate']) ? $query['cdate'] : set_value('cdate')?>" readonly></td>
					</tr>
					 -->
					
					<tr>
						<th colspan="2">Meta頁籤</th>
					</tr>
					
					<tr>
						<th width="20%">Meta標籤標題</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="meta_title" value="<?php echo isset($query['meta_title']) ? $query['meta_title'] : set_value('meta_title')?>">
						</td>
					</tr>
					
					<tr>
						<th width="20%">Meta標籤描述</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="meta_description" value="<?php echo isset($query['meta_description']) ? $query['meta_description'] : set_value('meta_description')?>">
						</td>
					</tr>
					
					<tr>
						<th width="20%">Meta標籤關鍵字</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="meta_keyword" value="<?php echo isset($query['meta_keyword']) ? $query['meta_keyword'] : set_value('meta_keyword')?>">
						</td>
					</tr>						

				</table>

				<div class="buttons">
					<input type="submit" value="送出" />
					<!-- <button type="button" onclick="javascript:location.href='<?php //echo getAdminURL('about/lists')?>'">返回</button> -->
				</div>

				</form>
			</div>

		</div>

	</div>

<script src="<?php echo base_url('application/views/backend/js/tinymce/tinymce.min.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_tinymce_elfinder.js')?>"></script>
	
	
<link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>" rel="stylesheet">
<link href="<?php echo base_url('application/views/backend/css/elfinder.min.css')?>" rel="stylesheet">
<link href="<?php echo base_url('application/views/backend/css/theme.css')?>" rel="stylesheet">

<script src="<?php echo base_url('application/views/backend/js/jquery-1.7.2.min.js');?>"></script>
<script src="<?php echo base_url('application/views/backend/js/jquery-ui-1.8.21.custom.min.js');?>"></script>
<script src="<?php echo base_url('application/views/backend/js/elfinder.min.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/i18n/elfinder.zh_CN.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_image_upload.js')?>"></script>

</body>
</html>
