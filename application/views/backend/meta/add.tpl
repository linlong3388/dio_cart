<body>

<style>
.fa-star {
	color: red
}
td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'meta' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			
			 <?php if($this->session->flashdata('msg')){ ?>				 
		        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	         <?php } ?> 
			
			 <b style="color: red"><?php echo validation_errors(); ?></b> 
			 
			<?php echo form_open( current_url())?>
				<table>
					<tr>
						<th colspan="2">新增資料</th>
					</tr>

					<tr>
						<th width="20%"><i class="fa fa-star"></i>頁面名稱</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="name" value="<?php echo set_value('name')?>"></td>
					</tr>
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>網址</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="url" value="<?php echo set_value('url')?>"></td>
					</tr>
					
					<tr>
						<th colspan="2">Meta頁籤</th>
					</tr>
					
					<tr>
						<th width="20%">Meta標籤標題</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="meta_title" value="<?php echo isset($query['meta_title']) ? $query['meta_title'] : set_value('meta_title')?>">
						</td>
					</tr>
					
					<tr>
						<th width="20%">Meta標籤描述</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="meta_description" value="<?php echo isset($query['meta_description']) ? $query['meta_description'] : set_value('meta_description')?>">
						</td>
					</tr>
					
					<tr>
						<th width="20%">Meta標籤關鍵字</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="meta_keyword" value="<?php echo isset($query['meta_keyword']) ? $query['meta_keyword'] : set_value('meta_keyword')?>">
						</td>
					</tr>						
					
				</table>

				<div class="buttons">
					<input type="submit" value="送出" />
					<button type="button"
						onclick="javascript:location.href='<?php echo getAdminURL('meta/lists')?>'">返回</button>
				</div>

				</form>
			</div>

		</div>

	</div>
	
</body>
</html>
