<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'admin' ,getAdminMenu())?></h2></div>

		<div id="content">

			<span style="float: right">
				<div class="actions">
					<a href="javascript:location.href='<?php echo getAdminURL('admin/add')?>'"
						class="btn btn-default btn-sm"> <i class="fa fa-pencil"></i> 新增</a>
				</div> </span>

				<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">
			
			 <?php if($this->session->flashdata('msg')){ ?>				 
		        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	         <?php } ?> 		

				<!-- (一)搜尋主體 -->

				<p></p>

				<!-- (二)列表主體 -->
				<div class="clearfix">

					<table class="dio_datatable">
						<thead>
							<tr>
								<!-- <td>編號</td> -->
								<td>群組</td>
								<td>帳號</td>
								<td>暱稱</td>
								<td>電子郵件</td>
								<td>等級</td>
								<td>狀態</td>
								<td>建立日期</td>
								<td>功能</td>
							</tr>
						</thead>

						<tbody>

						<?php foreach ($query as $row){ ?>
							<tr>
								<!-- <td class="center"><?php //echo $row['admin_id']; ?></td> -->
								<td class="center"><a href="<?php echo getAdminURL('admin_group_role/edit?admin_group_id='.$row['admin_group_id'])?>"><?php echo $row['title']; ?></td>
								<td class="center"><?php echo $row['username']; ?></td>
								<td class="center"><?php echo $row['nickname']; ?></td>
								<td class="center"><?php echo $row['email']; ?></td>
								<td class="center"><?php echo ($row['role'] == '66') ? "<b style='color:red'>最高管理員</b>" : "管理員"; ?>
								</td>
								<td class="center"><?php echo ($row['status'] == '1') ? "啟用" : "<b style='color:red'>停用</b>"  ?>
								</td>
								<td class="center"><?php echo substr($row['cdate'], 0,10) ?><b style="display: none"><?php echo substr($row['cdate'], 11) ?></td>
								<td class="center">
								   <a class="btn btn-info" href="<?php echo getAdminURL('admin/edit?admin_id='.$row['admin_id'])?>">
										<i class="icon-edit icon-white"></i> 編輯</a> 
										
							       <a class="btn btn-info" href="<?php echo getAdminURL('admin/pass?admin_id='.$row['admin_id'])?>">
									    <i class="icon-edit icon-white"></i> 密碼</a> 	
								
								   <a class="btn btn-danger" href="javascript:my_confirm(<?php echo $row['admin_id']?>);">
										<i class="icon-trash icon-white"></i> 刪除</a>
							
								</td>
							</tr>
							<?php } ?>

						</tbody>

					</table>
				</div>

			</div>


		</div>

	</div>

	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script type="text/javascript">
	 $(document).ready(function() {
        $('.dio_datatable').dataTable({
        	 'aaSorting': [[ 0, 'desc' ]] ,
          	 "language": { 
   		         url : "<?php echo base_url("application/views/backend/js/dataTables/i18n/chinese_traditional.lang.json")?>"
             },
             "iDisplayLength": 50
         });
    });
   	
    function my_confirm(admin_id){
       var chk_box=confirm('確定刪除資料?');
       if (chk_box == true) {
             location.href = '<?php echo getAdminURL("admin/del?admin_id=")?>'+admin_id;
       }                                 
    }
</script>

</body>
</html>




