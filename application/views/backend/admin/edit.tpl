<body>

<style>
.fa-star {
	color: red
}
td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'admin' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

		<?php if($this->session->flashdata('msg')){ ?>				 
		    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	     <?php } ?> 
		
		 <b style="color: red"><?php echo validation_errors()?></b>
		
			<div class="container editform">
			<?php echo form_open( getCurrentFullUrl() )?>
				<table>
					<tr>
						<th colspan="2">編輯資料</th>
					</tr>

					<tr style="display: none">
						<th width="20%">編號</th>
						<td><input type="text" name="admin_id" value="<?php echo isset($query['admin_id']) ? $query['admin_id'] : set_value('admin_id')?>" readonly></td>
					</tr>

					<tr>
						<th width="20%"><i class="fa fa-star"></i>群組</th>
						<td> 
							<select class="form-control" name="admin_group_id">
					      	  <?php $admin_group_id = isset($query['admin_group_id']) ? $query['admin_group_id'] : set_value('admin_group_id');
				          	        foreach( reSortArrayMutil(3 ,$admin_group_id ,transKeyPairArray($query_admin_group ,'admin_group_id' ,'title')) as $key=>$val) :?>    
                                      <option value="<?php echo $key?>"><?php echo $val?></option>
                              <?php endforeach; ?>					        
					        </select>
					    </td>
					</tr>
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>帳號</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="username" value="<?php echo isset($query['username']) ? $query['username'] : set_value('username')?>">
					    </td>
					</tr>
					
					<tr>
						<th width="20%">暱稱</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="nickname" value="<?php echo isset($query['nickname']) ? $query['nickname'] : set_value('nickname')?>">
					    </td>
					</tr>

					<tr>
						<th width="20%">Email</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="email" value="<?php echo isset($query['email']) ? $query['email'] : set_value('email')?>">
					    </td>
					</tr>
					
					<tr>
						<th width="20%">手機</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="mobile" value="<?php echo isset($query['mobile']) ? $query['mobile'] : set_value('mobile')?>">
						</td>
					</tr>																	

					<tr>
						<th width="20%"><i class="fa fa-star"></i>狀態</th>
						<td>
						    <input type="radio" name="status" value="1"
						    <?php if( isset($query['status']) && ($query['status'] == '1') ) echo set_radio('status', '1' ,true);
						      else echo set_radio('status', '1'); ?>>啟用 
						    <input type="radio" name="status" value="0"
							<?php if( isset($query['status']) && ($query['status'] == '0') ) echo set_radio('status', '0' ,true);
							  else echo set_radio('status', '0'); ?>>停用
						</td>
					</tr>

					<tr>
						<th width="20%">建立日期</th>
						<td><input class="fullwidth" type="text" name="cdate" value="<?php echo isset($query['cdate']) ? $query['cdate'] : set_value('cdate')?>" readonly> 
						    <b style="color: red"><?php echo form_error('cdate')?></b>
						 </td>
					</tr>

				</table>

				<div class="buttons">
					<input type="submit" value="送出" />
					<button type="button"
						onclick="javascript:location.href='<?php echo getAdminURL('admin/lists')?>'">返回</button>
				</div>

				</form>
			</div>

		</div>

	</div>

</body>
</html>
