<body>

<style>
.fa-star {
	color: red
}
td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'admin' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>
			
		<div class="container editform">

		<?php if($this->session->flashdata('msg')){ ?>				 
		    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	     <?php } ?> 
		
		 <b style="color: red"><?php echo validation_errors()?></b>		
		
	    <!-- 資料框 
	    <div style="padding-bottom:10px;">
		   <fieldset><legend>資料框:</legend>
		                帳號 : <input type="text" value="<?php //echo $query['username']?>" readonly>  /  暱稱 : <input type="text" value="<?php //echo $query['nickname']?>" readonly> 
		      /  Email : <input type="text" value="<?php //echo $query['email']?>" readonly>   
		   </fieldset>
	    </div>	-->	
		
			<?php echo form_open( getCurrentFullUrl() )?>
				<table>
					<tr>
						<th colspan="2">編輯密碼</th>
					</tr>

					<tr style="display:none">
						<th width="20%">編號</th>
						<td><input type="text" name="admin_id" value="<?php echo isset($query['admin_id']) ? $query['admin_id'] : set_value('admin_id')?>" readonly></td>
					</tr>
			
					<tr>
						<th width="20%"><i class="fa fa-star"></i>新密碼</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="password" name="password" value="<?php echo set_value('password')?>">
					    </td>
					</tr>
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>再一次確認新密碼</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="password" name="passconf" value="<?php echo set_value('passconf')?>">
					    </td>
					</tr>

				</table>

				<div class="buttons">
					<input type="submit" value="送出" />
					<!-- <button type="button" onclick="javascript:location.href='<?php //echo getAdminURL('admin/lists')?>'">返回</button> -->
				</div>

				</form>
			</div>

		</div>

	</div>

</body>
</html>
