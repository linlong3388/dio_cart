<body>

<style>
.fa-star {
	color: red
}
td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'admin' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

		 <b style="color: red"><?php echo validation_errors()?></b>
		
			<div class="container editform">
			
			<?php echo form_open( getCurrentFullUrl() )?>
				<table>
					<tr>
						<th colspan="2">新增資料</th>
					</tr>
				
				    <tr>
						<th width="20%"><i class="fa fa-star"></i>群組</th>
						<td> 
							<select class="form-control" name="admin_group_id">
					      	  <?php foreach( $query_admin_group as $row) :?>    
                                 <option value="<?php echo $row['admin_group_id']?>"><?php echo $row['title']?></option>
                              <?php endforeach; ?>					        
					        </select>
					    </td>
					</tr>
				
					<tr>
						<th width="20%"><i class="fa fa-star"></i>帳號</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="username" value="<?php echo set_value('username')?>"> 
						</td>
					</tr>
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>密碼</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="password" name="password" value=""> 
						</td>
					</tr>
					
					<tr>
						<th width="20%">暱稱</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="nickname" value="<?php echo set_value('nickname')?>"> 
						</td>
					</tr>
					
					<tr>
						<th width="20%">Email</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="email" value="<?php echo set_value('email')?>"> 
						</td>
					</tr>
					
					<tr>
						<th width="20%">手機</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="mobile" value="<?php echo set_value('mobile')?>"> 
						</td>
					</tr>
					
				</table>

				<div class="buttons">
					<input type="submit" value="送出" />
					<button type="button"
						onclick="javascript:location.href='<?php echo getAdminURL('admin/lists')?>'">返回</button>
				</div>

				</form>
			</div>

		</div>

	</div>

</body>
</html>
