<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'space_category' ,getAdminMenu())?></h2></div>

		<div id="content">

			<span style="float: right">
				<div class="actions">
					<a href="javascript:location.href='<?php echo getAdminURL('space_category_child/add?category_id='.$this->input->get('category_id'))?>'"
						class="btn btn-default btn-sm"> <i class="fa fa-pencil"></i> 新增</a>
					<a href="javascript:location.href='<?php echo getAdminURL('space_category/lists')?>'"
						class="btn btn-default btn-sm"> 返回</a>
				</div> </span>


				<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">

			   <?php if($this->session->flashdata('msg')){ ?>				 
				  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
			   <?php }elseif($this->session->flashdata('msg_err')){ ?>
				  <div class="alert alert-danger"><i class="fa fa-times-circle"></i> <?php echo $this->session->flashdata('msg_err')?></div>
			   <?php } ?>
			
			
				<!-- (一)搜尋主體 -->
				<fieldset>
					<legend>搜尋條件:</legend>
					分類項 :
					<?php echo get_assign_field('space_category', 'category_id', $this->input->get('category_id'), 'name')?>
					<br />
				</fieldset>

				<p></p>

				<!-- (二)列表主體 -->
				<div class="clearfix">
					<table class="dio_datatable">
						<thead>
							<tr>
								<td>(次級)分類級別</td>
								<td>層級</td>
								<td>狀態</td>
								<td>建立日期</td>
								<td>功能</td>
							</tr>
						</thead>

						<tbody>

						<?php foreach ($query as $row){ ?>
							<tr>
								<td><?php echo $this->cls_category_space->get_name_template( $row['path'] )?></td>
								<td><?php echo $row['level']-1 ?></td>
								<td class="center"><?php echo ($row['status'] == '1') ? "正常" : "<b style='color:red'>停用</b>"?></td>
								<td><?php echo $row['created_at']?></td>
								<td><a class="btn btn-success"
									href="<?php echo getAdminURL('space_category_child/edit?parent_category_id='.$this->input->get('category_id').'&category_id='.$row['category_id']);?>">
										<i class="fa fa-pencil-square-o"></i> 編輯</a> <a
									class="btn btn-danger"
									href="javascript:my_confirm(<?php echo $this->input->get('category_id') ?>,<?php echo $row['category_id'] ?>);">
										<i class="fa fa-trash"></i> 刪除</a>
								</td>
							</tr>
							<?php } ?>

						</tbody>
					</table>
				</div>

			</div>

		</div>

	</div>

	<script type="text/javascript">
    $(document).ready(function() {
        $('.dio_datatable').dataTable({
          	 "language": { 
   		                  url : "<?php echo base_url("application/views/backend/js/dataTables/i18n/chinese_traditional.lang.json")?>"
                         },
             "iDisplayLength": 100            
         });
    });

    function my_confirm( parent_category_id ,category_id ){

       var host = "<?php echo base_url() ?>";
       var param  = "index.php/backend/space_category_child/del?parent_category_id="+parent_category_id+"&category_id="+category_id;
        
       var chk_box=confirm('確定刪除資料?');
       if (chk_box == true) {
           location.href = host+param;
       }
    }
</script>



</body>
</html>
