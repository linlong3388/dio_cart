<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>elFinder 2.0</title>

<!-- elfinder 的專屬 css -->
<link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>" rel="stylesheet">
<link href="<?php echo base_url('application/views/backend/css/elfinder.min.css')?>" rel="stylesheet">
<link href="<?php echo base_url('application/views/backend/css/theme.css')?>" rel="stylesheet">

<!-- elfinder 的專屬 js -->
<script src="<?php echo base_url('application/views/backend/js/jquery-1.7.2.min.js');?>"></script>
<script src="<?php echo base_url('application/views/backend/js/jquery-ui-1.8.21.custom.min.js');?>"></script>
<script src="<?php echo base_url('application/views/backend/js/elfinder.min.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/i18n/elfinder.zh_CN.js')?>"></script>

</head>
<body>

<style>
  .star{ color:red }
</style>

	<div id="page-wrapper">

		<!-- /.row -->
		<div class="row">
		
		    <div>
		       <b class="star">★</b> 單一檔案大小不可超過5MB <br/>
		       <b class="star">★</b> 資料夾均以字母(a-z)、數字(0-9)、下滑線(_)組合命名，如 : product_01、logo_ad、product 等
		    </div>
		    
		    <p>		
		
			<div class="file-manager"></div>
		</div>
		<!-- /.row -->

	</div>
	<!-- /#page-wrapper -->


	<script type="text/javascript" charset="utf-8">
	$(function() {
		   var elf = $('.file-manager').elfinder({
		              url : 'init',  // connector URL (REQUIRED)
           	      lang : 'zh_CN',
           	       
		              //功能鈕
		              uiOptions : {
		            	    // toolbar configuration
		            	    toolbar : [
		            	        ['back', 'forward'],
		            	        // ['reload'],
		            	        // ['home', 'up'],
		            	        ['mkdir', 'mkfile', 'upload'],
		            	        ['open', 'download', 'getfile'],
		            	        ['info'],
		            	        ['quicklook'],
		            	        //['copy', 'cut', 'paste'],
		            	        ['rm'],
		            	        ['extract', 'archive'],
		            	        ['search'],
		            	        ['view'],
		            	        ['help']
		            	    ]},
		            	    
                  //右鍵鈕
		             contextmenu : {		            	    
		            	    cwd    : ['reload', 'back', '|' , 'info'], 
		            	    files  : ['getfile', '|','open', 'quicklook', '|', 'download', '|' , 'rm', '|', 'archive', 'extract', '|', 'info']
			              }        

	             }).elfinder('instance');

		});
   </script>

</body>

</html>
