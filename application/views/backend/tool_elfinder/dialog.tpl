<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>elFinder 2.0</title>

<!-- elfinder 的專屬 css -->
<link
	href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css');?>"
	rel="stylesheet">
<link
	href="<?php echo base_url('application/views/backend/css/elfinder.min.css');?>"
	rel="stylesheet">
<link
	href="<?php echo base_url('application/views/backend/css/theme.css');?>"
	rel="stylesheet">

<!-- elfinder 的專屬 js -->
<script
	src="<?php echo base_url('application/views/backend/js/jquery-1.7.2.min.js');?>"></script>
<script
	src="<?php echo base_url('application/views/backend/js/jquery-ui-1.8.21.custom.min.js');?>"></script>
<script
	src="<?php echo base_url('application/views/backend/js/elfinder.min.js');?>"></script>
<script
	src="<?php echo base_url('application/views/backend/js/i18n/elfinder.zh_CN.js')?>"></script>
</head>
<body>

	<div id="page-wrapper">

		<!-- /.row -->
		<div class="row">
			<div class="file-manager"></div>
		</div>
		<!-- /.row -->

	</div>
	<!-- /#page-wrapper -->

	<script type="text/javascript">

  var FileBrowserDialogue = {
    init: function() {
      // Here goes your code for setting your custom things onLoad.
    },
    mySubmit: function (URL) {
      // pass selected file path to TinyMCE
      parent.tinymce.activeEditor.windowManager.getParams().setUrl(URL);
    
      // force the TinyMCE dialog to refresh and fill in the image dimensions
      var t = parent.tinymce.activeEditor.windowManager.windows[0];
      t.find('#src').fire('change');

      // close popup window
      parent.tinymce.activeEditor.windowManager.close();
    }
  }


	$(function() {
		   var elf = $('.file-manager').elfinder({
		              url : 'init',  // connector URL (REQUIRED)
		              lang : 'zh_CN', 
		              //功能鈕
		              uiOptions : {
		            	    // toolbar configuration
		            	    toolbar : [
		            	        ['back', 'forward'],
		            	        // ['reload'],
		            	        // ['home', 'up'],
		            	        ['mkdir', 'mkfile', 'upload'],
		            	        ['open', 'download', 'getfile'],
		            	        ['info'],
		            	        ['quicklook'],
		            	        //['copy', 'cut', 'paste'],
		            	        ['rm'],
		            	        ['extract', 'archive'],
		            	        ['search'],
		            	        ['view'],
		            	        ['help']
		            	    ]},
		            	    
	                 //右鍵鈕
		             contextmenu : {		            	    
		            	    cwd    : ['reload', 'back', '|' , 'info'], 
		            	    files  : ['getfile', '|','open', 'quicklook', '|', 'download', '|' , 'rm', '|', 'archive', 'extract', '|', 'info']
			              },    
		             getFileCallback: function(file) { // editor callback
		                  // file.url - commandsOptions.getfile.onlyURL = false (default)
		                  // file     - commandsOptions.getfile.onlyURL = true
		                  FileBrowserDialogue.mySubmit(file.url); // pass selected file path to TinyMCE 
		                } 
	             }).elfinder('instance');
		});

  
</script>


</body>

</html>
