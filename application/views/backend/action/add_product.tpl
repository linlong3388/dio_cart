<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'add_product' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">

				<!-- (一)搜尋主體 -->
			<?php echo form_open("backend/action/lists")?>
				<fieldset>
					<legend>搜尋條件:</legend>
					<div style="float: left">
						活動分類 : <input type="text"
							value="<?php echo get_assign_field('ct_action', 'action_id', $this->input->get('action_id'), 'name')?>"
							readonly> <input type="text" name="action_id"
							style="display: none"
							value="<?php echo $this->input->get('action_id')?>" readonly>
						<button type="button" onclick="multi_category_func_id()">加入商品</button>
					</div>
					<button type="button"
						onclick="javascript:location.href='<?php echo getAdminURL('action/lists')?>'">返回</button>
				</fieldset>
				</form>

				<p></p>

				<!-- (二)列表主體 -->
				<div class="clearfix">
					<table class="dio_datatable">
						<thead>
							<tr>
								<td><input id="selectAll" type="checkbox" />
								</td>
								<td>商品名稱</td>
								<td>價格</td>
								<td>商品狀態</td>
								<td>發佈時間</td>
							</tr>
						</thead>

						<tbody>
						<?php foreach ($query as $row) { ?>
							<tr>
								<td><input type="checkbox" /> <input type="hidden"
									value="<?php echo $row['product_id']?>" /></td>
								<td><a
									href="<?php echo base_url("index.php/backend/action/edit?product_id=".$row['product_id'])?>"
									target="_new"><?php echo $row['name']?>
								</a>
								</td>
								<td><?php echo DIO_CURRENCY.addCommas($row['price'])?>
								</td>
								<td><?php echo get_common_status($row['status'])?>
								</td>
								<td><?php echo substr($row['cdate'], 0,10) ?><b style="display: none"><?php echo substr($row['cdate'], 11) ?>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>

			</div>


		</div>

	</div>

<script>

$(document).ready(function() {
    $('.dio_datatable').dataTable({
      	 "language": { 
		         url : "<?php echo base_url("application/views/backend/js/dataTables/i18n/chinese_traditional.lang.json")?>"
         },
         "iDisplayLength": 50
     });
});

//-------------------------------------------------------------
// 全選&全不選
//-------------------------------------------------------------
$(function() {
	$('#selectAll').click(function (e) {
	    $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
	});
});


//-------------------------------------------------------------
// 賣場管別分類 / 異動  
//-------------------------------------------------------------
function multi_category_func_id(){
	  var aryList = new Array();
	  
	  $('table tbody').find('input[type="checkbox"]:checked').each(function (i) {
	 	   aryList.push($(this).next().val());
	  });

	  if(aryList.length <= 0){
	     alert('您尚未勾選項目!');
	  }else{
	     var chk_box=confirm('確定更改[活動分類]?');
		 if (chk_box == true) {
		   	location.href = '<?php echo base_url("index.php/backend/action/add_product_upd?action_id=".$this->input->get('action_id')."&aryList=")?>'+aryList;
		 }	      
	  }
	  
} 


//-------------------------------------------------------------
// 刪除  
//-------------------------------------------------------------
function my_confirm(vendor_category_id){
	   var chk_box=confirm('確定刪除?');
	   if (chk_box == true) {
	         location.href = '<?php echo base_url("index.php/vendor/center/category_delete2?vendor_category_id=")?>'+vendor_category_id;
	   }
}
</script>


</body>
</html>
