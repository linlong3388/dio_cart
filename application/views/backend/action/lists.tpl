<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'action' ,getAdminMenu())?></h2></div>

		<div id="content">
            
            <!-- 
			<span style="float: right">
				<div class="actions">
			      <a href="javascript:location.href='<?php //echo getAdminURL('action/add')?>'" class="btn btn-default btn-sm"> 
					<i class="fa fa-pencil"></i> 新增</a> 
				</div> 
		    </span>
		     -->

				<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">

				<!-- (一)搜尋主體 -->

				<p></p>

				<!-- (二)列表主體 -->
				<div class="clearfix">

					<table class="dio_datatable">
						<thead>
							<tr>
								<td>活動名稱</td>
								<td>狀態</td>
								<td>建立日期</td>
								<!-- <td>排序</td>  -->
								<td>功能</td>
							</tr>
						</thead>

						<tbody>

						<?php foreach ($query as $row){ ?>
							<tr>
								<td class="center"><?php echo $row['name']?> <a
									href="<?php echo getAdminURL('action/view_product?action_id='.$row['action_id'])?>"><?php echo '(' .$row['count']. ')'?>
								</a></td>
								<td class="center"><?php echo ($row['status'] == '1') ? "正常" : "<b style='color:red'>停用</b>"?>
								</td>
								<td class="center"><?php echo substr($row['cdate'], 0,10) ?><b style="display: none"><?php echo substr($row['cdate'], 11) ?>
								</td>
								<td class="center"><a class="btn"
									href="<?php echo getAdminURL('action/add_product?action_id='.$row['action_id'])?>">
										<i class="fa fa-plus"></i> 加入商品</a> <a class="btn"
									href="<?php echo getAdminURL('action/edit?action_id='.$row['action_id'])?>">
										<i class="fa fa-pencil-square-o"></i> 編輯</a> 
								 <!-- <a class="btn" href="javascript:my_confirm(<?php //echo $row['action_id']?>);">
										<i class="fa fa-trash"></i> 刪除</a> -->
								 </td>
							</tr>
							<?php } ?>

						</tbody>

					</table>
				</div>

			</div>


		</div>

	</div>

	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script type="text/javascript">
	 $(document).ready(function() {
        $('.dio_datatable').dataTable({
          	 "language": { 
   		         url : "<?php echo base_url("application/views/backend/js/dataTables/i18n/chinese_traditional.lang.json")?>"
             },
             "iDisplayLength": 50
         });
    });
   	
    function my_confirm(action_id){
       var chk_box=confirm('確定刪除資料?');
       if (chk_box == true) {
             location.href = '<?php echo base_url("index.php/backend/action/del?action_id=") ?>'+action_id;
       }
    }
</script>

</body>
</html>




