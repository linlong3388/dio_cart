<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'category_promo_product' ,getAdminMenu())?></h2></div>
	
		<div id="content">
		
			<span style="float: right">
				<div class="actions">
					<a href="javascript:location.href='<?php echo getAdminURL('category_promo/lists')?>'"
						class="btn btn-default btn-sm"> <i class="fa fa-mail-reply"></i> 返回</a>
				</div> 
			</span>

			<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">
			
			<h4>標題 : <?php echo $query['name']?></h4>
			
			<p>

			<h4>活動期間 : <?php echo $query['sdate']?> - <?php echo $query['edate']?></h4>
			
			 <?php if($this->session->flashdata('msg')){ ?>				 
		        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	         <?php } ?> 			
			
			  <!-- (一)搜尋主體 -->
              <?php echo form_open(getCurrentFullUrl()  ,array('method'=>'get'))?>
	             <fieldset>
					<legend>快捷功能:</legend>
					<input type="button" onclick="multi_select()" value="更新商品" />
				</fieldset>	
	        	</form>
	        	 
				<p></p>

				<!-- (二)列表主體 -->
				<div class="clearfix">

					<table class="dio_datatable">
						<thead>
							<tr>
							    <td><input type="checkbox" id="selectAll" onclick="selectAll()" /></td>
								<td>圖示</td>
								<td>商品名稱</td>
								<td>狀態</td>
								<td>排序</td>
							</tr>
						</thead>

						<tbody>

						<?php foreach ($product as $row){ ?>
							<tr>
								<td>
							      <input type="checkbox" <?php echo ($row['isSelect'] == 1) ? ' checked' : ''?> />
								  <input style="display: none" type="text" value="<?php echo $row['product_id'] ?>" />
								</td>
							    <td class="center"><img src="<?php echo base_url(DIO_PATH_PIMG.'/'.$row['image'])?>" width="120px" height="100px"/></td>
								<td class="center"><?php echo $row['name']?></td>
								<td class="center"><?php echo ($row['status'] == '1') ? "正常" : "<b style='color:red'>停用</b>"?></td>
								<td class="center"><?php echo $row['sort_order']?></td>
							</tr>
							<?php } ?>

						</tbody>

					</table>
				</div>

			</div>


		</div>

	</div>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script type="text/javascript">

//-------------------------------------------------------------
//
// 全選&全不選
//
//-------------------------------------------------------------
$('#selectAll').click(function (e) {
    $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
});

	
//-------------------------------------------------------------
//多筆[確認訂單]& 多筆[取消]  
//-------------------------------------------------------------
function multi_select(){
 var aryList = new Array();

 $('table tbody').find('input[type="checkbox"]:checked').each(function (i) {
	   aryList.push($(this).next().val());
 });

 if(aryList.length <= 0){
    alert('至少要有一筆商品資料!');
 }else{
    var chk_box=confirm('確定加入已選取商品?');
	if (chk_box == true) {
	     location.href = '<?php echo getAdminURL("category_promo_product/multi_select?category_promo_id={$category_promo_id}&aryList=")?>' + aryList;
	}	      
 }   
} 
</script>

</body>
</html>




