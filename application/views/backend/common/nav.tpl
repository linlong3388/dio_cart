<div id="header" class="clearfix">
	<h1 class="logo">
		<a href="<?php echo base_url('backend/dashboard/main')?>"
			style="display: initial; background: none;"><?php echo DIO_WEBNAME ?></a>
	</h1>
	<ul id="nav">
	
	    <!-- 主控台  -->
		<li><a href="<?php echo base_url('backend/dashboard/main')?>"><?php echo reSortArrayMutil(2,'dashboard' ,getAdminMenu())?></a></li>

		
		<!-- 關於我們  
		<?php //if( isAdminRoleTbName('story_img') || isAdminRoleTbName('story') || isAdminRoleTbName('certification') || isAdminRoleTbName('awards')
				 // || isAdminRoleTbName('plan') || isAdminRoleTbName('branch') || isAdminRoleTbName('download') ) { ?>
		<li><a href="javascript:;"><?php //echo reSortArrayMutil(2,'about' ,getAdminMenuTop())?><i class="fa fa-angle-down"></i></a>
			<ul>
			    
			    <?php //if( isAdminRoleTbName('story_img') || isAdminRoleTbName('story')) { ?>
			    <li class=" dropdown-submenu"><a href="#">
				   <i class="fa fa-file"></i>品牌故事</a>
				     <ul class="dropdown-menu" style="background-color:#55616f">
						 <?php //if( isAdminRoleTbName('story_img') ){ ?>
						   <li>
							  <a href="<?php //echo getAdminURL('story_img/lists')?>"><?php //echo reSortArrayMutil(2,'story_img' ,getAdminMenu())?></a>
						   </li>
						 <?php //} ?>
						
						 <?php //if( isAdminRoleTbName('story') ){ ?>
						   <li>
							  <a href="<?php //echo getAdminURL('story/lists')?>"><?php //echo reSortArrayMutil(2,'story' ,getAdminMenu())?></a>
						   </li>							
						<?php //} ?>
					</ul>
				</li>
				<?php //} ?>
				
				<?php //if( isAdminRoleTbName('certification') ) { ?>
				 <li class=" dropdown-submenu"><a href="#">
				   <i class="fa fa-file"></i>食在安心</a>
				     <ul class="dropdown-menu" style="background-color:#55616f">
						<?php //if( isAdminRoleTbName('certification') ){ ?>
						   <li class="">
							  <a href="<?php //echo getAdminURL('certification/edit?certification_id=1')?>"><?php //echo reSortArrayMutil(2,'certification' ,getAdminMenu())?></a>
						   </li>
						<?php //} ?>
						<?php //if( isAdminRoleTbName('certification_img') ){ ?>
				   		   <li class=" ">
							   <a href="<?php //echo getAdminURL('certification_img/lists')?>"><?php //echo reSortArrayMutil(2,'certification_img' ,getAdminMenu())?></a>
						   </li>
						<?php //} ?>							
					</ul>
				</li>
			    <?php //} ?>
			
			    <?php //if( isAdminRoleTbName('awards') ) { ?>
			      <li><a href="<?php //echo getAdminURL('awards/lists')?>">
				      <i class="fa fa-file"></i><?php //echo reSortArrayMutil(2,'awards' ,getAdminMenu())?></a>
				  </li>
				<?php //} ?>
				
				<?php //if( isAdminRoleTbName('plan') ) { ?>
			      <li><a href="<?php //echo getAdminURL('plan/edit')?>">
				      <i class="fa fa-file"></i><?php //echo reSortArrayMutil(2,'plan' ,getAdminMenu())?></a>
				  </li>
				<?php //} ?>
			
			    <?php //if( isAdminRoleTbName('branch') ) { ?>
				  <li><a href="<?php //echo getAdminURL('branch/lists')?>">
				      <i class="fa fa-file"></i><?php //echo reSortArrayMutil(2,'branch' ,getAdminMenu())?></a>
				  </li>	
				<?php //} ?>
				
				<?php //if( isAdminRoleTbName('download') ) { ?>
				  <li><a href="<?php //echo getAdminURL('download/lists')?>">
				      <i class="fa fa-file"></i><?php //echo reSortArrayMutil(2,'download' ,getAdminMenu())?></a>
				  </li>		
				<?php //} ?>
			</ul>
	    </li>
	    <?php //} ?>
	  -->
	  
	    <!-- 網站內容 -->
	    <?php if( isAdminRoleTbName('index') || isAdminRoleTbName('news') || isAdminRoleTbName('article') 
	    	   || isAdminRoleTbName('qa') || isAdminRoleTbName('contact')) { ?>
		<li><a href="javascript:;"><?php echo reSortArrayMutil(2,'webContent' ,getAdminMenuTop())?><i class="fa fa-angle-down"></i></a>
			<ul>
			    <?php if( isAdminRoleTbName('index') ) { ?>
			      <li><a href="<?php echo getAdminURL('index/edit')?>">
				      <i class="fa fa-file"></i><?php echo reSortArrayMutil(2,'index' ,getAdminMenu())?></a>
				  </li>
				<?php } ?>
			
			    <!-- 
			    <?php //if( isAdminRoleTbName('news') ) { ?>
			      <li><a href="<?php //echo getAdminURL('news/lists')?>">
				      <i class="fa fa-file"></i><?php //echo reSortArrayMutil(2,'news' ,getAdminMenu())?></a>
				  </li>
				<?php //} ?>
				 -->
				
				<li><!-- <a href="<?php //echo getAdminURL('about/lists')?>"> -->
			   	    <a href="<?php echo getAdminURL('about/edit?about_id=1')?>">
				    <i class="fa fa-file"></i><?php echo reSortArrayMutil(2,'about' ,getAdminMenu())?></a>
				</li>
				
			    <!--
			    <li class=" dropdown-submenu"><a href="<?php //echo getAdminURL('news/lists')?>">
				   <i class="fa fa-file"></i>最新訊息</a>
				     <ul class="dropdown-menu" style="background-color:#55616f">
						<li class=" ">
							<a href="<?php //echo getAdminURL('news_category/lists')?>"><?php //echo reSortArrayMutil(2,'news_category' ,getAdminMenu())?></a>
						</li>
						<li class=" ">
							<a href="<?php //echo getAdminURL('news/lists')?>"><?php //echo reSortArrayMutil(2,'news' ,getAdminMenu())?></a>
						</li>							
					</ul>
				</li> 
				
				<li><a href="<?php //echo getAdminURL('branch/lists')?>">
				    <i class="fa fa-file"></i><?php //echo reSortArrayMutil(2,'branch' ,getAdminMenu())?></a>
				</li>				
				
				<?php //if( isAdminRoleTbName('article') ) { ?>
			      <li><a href="<?php //echo getAdminURL('article/lists')?>">
				      <i class="fa fa-file"></i><?php //echo reSortArrayMutil(2,'article' ,getAdminMenu())?></a>
				  </li>				
				<?php //} ?>
			   
			   <li class=" dropdown-submenu"><a href="<?php //echo getAdminURL('knowledge/lists')?>">
				   <i class="fa fa-file"></i>知識專欄</a>
				     <ul class="dropdown-menu" style="background-color:#55616f">
						<li class=" ">
							<a href="<?php //echo getAdminURL('knowledge_category/lists')?>"><?php //echo reSortArrayMutil(2,'knowledge_category' ,getAdminMenu())?></a>
						</li>
						<li class=" ">
							<a href="<?php //echo getAdminURL('knowledge/lists')?>"><?php //echo reSortArrayMutil(2,'knowledge' ,getAdminMenu())?></a>
						</li>							
					</ul>				   
				</li>
				 -->
				<?php if( isAdminRoleTbName('qa') ) { ?>
				  <li><a href="<?php echo getAdminURL('qa/edit?qa_id=1')?>">
				      <!-- <a href="<?php //echo getAdminURL('qa/lists')?>"> -->
				      <i class="fa fa-file"></i><?php echo reSortArrayMutil(2,'qa' ,getAdminMenu())?></a>
				  </li>
				<?php } ?>
				
				<!-- 
				<li class=" dropdown-submenu"><a href="#">
				    <i class="fa fa-file"></i>常見問題</a>
				      <ul class="dropdown-menu" style="background-color:#55616f">
						<li class=" ">
							<a href="<?php //echo getAdminURL('qa_category/lists')?>"><?php //echo reSortArrayMutil(2,'qa_category' ,getAdminMenu())?></a>
						</li>
						<li class=" ">
							<a href="<?php //echo getAdminURL('qa/lists')?>"><?php //echo reSortArrayMutil(2,'qa' ,getAdminMenu())?></a>
						</li>							
					</ul>
				</li>
				
				<li class=" dropdown-submenu"><a href="#">
				    <i class="fa fa-file"></i>空間案例</a>
				      <ul class="dropdown-menu" style="background-color:#55616f">
						<li class=" ">
							<a href="<?php //echo getAdminURL('space_category/lists')?>"><?php //echo reSortArrayMutil(2,'space_category' ,getAdminMenu())?></a>
						</li>
						<li class=" ">
							<a href="<?php //echo getAdminURL('space/lists')?>"><?php //echo reSortArrayMutil(2,'space' ,getAdminMenu())?></a>
						</li>							
					</ul>
				</li> -->
                
                <?php if( isAdminRoleTbName('contact') ) { ?>
				   <li><a href="<?php echo getAdminURL('contact/lists')?>">
				       <i class="fa fa-file"></i><?php echo reSortArrayMutil(2,'contact' ,getAdminMenu())?></a>
				   </li>				
				<?php } ?>
				
			</ul>
		</li>
		<?php } ?> 
		
		
		<!-- 會員管理  -->
		<?php if( isAdminRoleTbName('customer') ) { ?>
		<li><a href="javascript:;"><?php echo reSortArrayMutil(2,'customer' ,getAdminMenuTop())?><i class="fa fa-angle-down"></i>
		</a>
			<ul>
			   <?php if( isAdminRoleTbName('customer') ) { ?>
				  <li><a href="<?php echo getAdminURL('customer/lists?page=1')?>">
				      <i class="fa fa-file"></i><?php echo reSortArrayMutil(2,'customer' ,getAdminMenu())?></a>
				  </li>
			   <?php } ?>	
			</ul>
		</li>
		<?php } ?>
		
		
		<!-- 商品管理  -->
		<?php if( isAdminRoleTbName('product_category') || isAdminRoleTbName('product') || isAdminRoleTbName('product_box')
				 || isAdminRoleTbName('brand') ) { ?>
		<li><a href="javascript:;"><?php echo reSortArrayMutil(2,'product' ,getAdminMenuTop())?><i class="fa fa-angle-down"></i>
		</a>
			<ul>
			    <?php if( isAdminRoleTbName('product_category') ) { ?>
			      <li><a href="<?php echo getAdminURL('product_category/lists?page=1')?>">
			          <i class="fa fa-file"></i>商品分類</a>
				  </li>
				<?php } ?>
				<!-- 
				<li><a href="<?php //echo getAdminURL('action/lists')?>">
				     <i class="fa fa-file"></i>活動分類</a>
				</li>
				 -->
				 
				<!--  
				<li><a href="<?php //echo getAdminURL('product_property/lists')?>">
				    <i class="fa fa-file"></i><?php //echo reSortArrayMutil(2,'product_property' ,getAdminMenu())?></a>
				</li>
				 -->
				<?php if( isAdminRoleTbName('product') ) { ?>
				   <li><a href="<?php echo getAdminURL('product/lists?page=1')?>">
				       <i class="fa fa-file"></i><?php echo reSortArrayMutil(2,'product' ,getAdminMenu())?></a>
				   </li>
                <?php } ?>
				
				<!-- 
				<?php //if( isAdminRoleTbName('product_box') ) { ?>
				   <li><a href="<?php //echo getAdminURL('product_box/lists?page=1')?>">
				       <i class="fa fa-file"></i><?php //echo reSortArrayMutil(2,'product_box' ,getAdminMenu())?></a>
				   </li>
				<?php //} ?>
				
				<?php //if( isAdminRoleTbName('brand') ) { ?>
                   <li><a href="<?php //echo getAdminURL('brand/lists')?>">
				       <i class="fa fa-file"></i><?php //echo reSortArrayMutil(2,'brand' ,getAdminMenu())?></a>
				   </li>
				<?php //} ?>    
				 -->
				
				<!-- 
				<li><a href="<?php //echo getAdminURL('product_qa/lists')?>">
				    <i class="fa fa-file"></i><?php //echo reSortArrayMutil(2,'product_qa' ,getAdminMenu())?></a>
				</li>			
					
				<li><a href="<?php //echo getAdminURL('product/product_import_view')?>">
				    <i class="fa fa-file"></i>批次上傳</a>
				</li>
				 -->
				  
			</ul>
		</li>
		<?php } ?>	
			
			
		<!-- 訂單管理  -->	
		<?php if( isAdminRoleTbName('order') || isAdminRoleTbName('returns')) { ?>
		<li><a href="javascript:;"><?php echo reSortArrayMutil(2,'order' ,getAdminMenuTop())?><i class="fa fa-angle-down"></i></a>
			<ul>
			   <?php if( isAdminRoleTbName('order') ) { ?>
				  <li><a href="<?php echo getAdminURL('order/lists?page=1')?>">
				      <i class="fa fa-file"></i><?php echo reSortArrayMutil(2,'order' ,getAdminMenu())?></a>
			 	  </li>
			   <?php } ?>	
				<!-- 
				<li><a href="<?php //echo getAdminURL('order_pre/lists')?>"><i class="fa fa-file"></i><?php //echo reSortArrayMutil(2,'order_pre' ,getAdminMenu())?></a>
				</li>
				 -->
				<?php if( isAdminRoleTbName('returns') ) { ?> 
				<li><a href="<?php echo getAdminURL('returns/lists')?>">
				    <i class="fa fa-file"></i><?php echo reSortArrayMutil(2,'returns' ,getAdminMenu())?></a>
				</li>				
				<?php } ?>
				<!-- 
				<li><a href="<?php //echo getAdminURL('order/order_export_list')?>">
				    <i class="fa fa-file"></i>訂單下載</a>
				</li>
				<li><a href="<?php //echo getAdminURL('order/order_import_view')?>">
				    <i class="fa fa-file"></i>訂單匯入</a>
				</li>
				 -->
			</ul>
		</li>
		<?php } ?>
		
		
		<!-- 廣告&行銷  -->
		<?php if( isAdminRoleTbName('ad_home') || isAdminRoleTbName('ad_banner') || isAdminRoleTbName('category_promo')
				  || isAdminRoleTbName('meta') ) { ?>
		<li><a href="javascript:;"><?php echo reSortArrayMutil(2,'sale' ,getAdminMenuTop())?><i class="fa fa-angle-down"></i>
		</a>
			<ul>
			    <?php if( isAdminRoleTbName('ad_home') ) { ?> 
				   <li><a href="<?php echo getAdminURL('ad_home/lists')?>">
				       <i class="fa fa-file"></i><?php echo reSortArrayMutil(2,'ad_home' ,getAdminMenu())?></a>
				   </li>
				<?php } ?>
				
				<?php if( isAdminRoleTbName('ad_banner') ) { ?>
			       <li><a href="<?php echo getAdminURL('ad_banner/lists')?>">
				       <i class="fa fa-file"></i><?php echo reSortArrayMutil(2,'ad_banner' ,getAdminMenu())?></a>
				   </li>
				<?php } ?>
				
				<!-- 
				<?php //if( isAdminRoleTbName('category_promo') ) { ?>
				   <li><a href="<?php //echo getAdminURL('category_promo/lists')?>">
				       <i class="fa fa-file"></i><?php //echo reSortArrayMutil(2,'category_promo' ,getAdminMenu())?></a>
				   </li>			
				<?php //} ?>
				 -->
				
				<!-- 
				<?php //if( isAdminRoleTbName('meta') ) { ?>
			       <li><a href="<?php //echo getAdminURL('meta/lists')?>">
		               <i class="fa fa-file"></i><?php //echo reSortArrayMutil(2 ,'meta' ,getAdminMenu())?></a>
		           </li>
				<?php //} ?>
				 -->
				
				<!-- 
			    <li><a href="<?php //echo getAdminURL('ad_content/lists')?>">
				     <i class="fa fa-file"></i><?php //echo reSortArrayMutil(2,'ad_content' ,getAdminMenu())?></a>
				</li>
				 -->
				
				<!-- 
		        <li><a href="<?php //echo getAdminURL('edm/add')?>">
				     <i class="fa fa-file"></i><?php //echo reSortArrayMutil(2,'edm' ,getAdminMenu())?></a>
				</li> -->
				
				<li><a href="<?php echo getAdminURL('promo_coupon/lists')?>">
		       	    <i class="fa fa-file"></i><?php echo reSortArrayMutil(2,'promo_coupon' ,getAdminMenu())?></a>
				</li>
				
				<!-- 								
			    <li><a href="<?php //echo getAdminURL('promo_vip/edit')?>">
		       	    <i class="fa fa-file"></i><?php //echo reSortArrayMutil(2,'promo_vip' ,getAdminMenu())?></a>
				</li>
				
				<li><a href="<?php //echo getAdminURL('promo/feedback_gold_update')?>">
		       	    <i class="fa fa-file"></i>購物金</a>
				</li>
				-->
				
			</ul>
		</li>
		<?php } ?>
		
		
		<!-- 統計報表
		<?php //if( isAdminRoleTbName('report_order') || isAdminRoleTbName('report_customer') ) { ?>
		<li><a href="javascript:;"><?php //echo reSortArrayMutil(2,'statistics' ,getAdminMenuTop())?><i class="fa fa-angle-down"></i>
		</a>
			<ul>
			    <?php //if( isAdminRoleTbName('report_order') ) { ?>
				  <li><a href="<?php //echo getAdminURL('report_order/lists')?>">
				      <i class="fa fa-file"></i><?php //echo reSortArrayMutil(2,'report_order' ,getAdminMenu())?></a>
				  </li>
				<?php //} ?>
				
				<?php //if( isAdminRoleTbName('report_customer') ) { ?>
				  <li><a href="<?php //echo getAdminURL('report_customer/lists')?>">
				      <i class="fa fa-file"></i><?php //echo reSortArrayMutil(2,'report_customer' ,getAdminMenu())?></a>
				  </li>
				<?php //} ?>
				
				<?php //if( isAdminRoleTbName('report_product') ) { ?>
				  <li><a href="<?php //echo getAdminURL('report_product/lists')?>">
				      <i class="fa fa-file"></i><?php //echo reSortArrayMutil(2,'report_product' ,getAdminMenu())?></a>
				  </li>
				<?php //} ?>
			</ul>
	   </li>		
	   <?php //} ?> 
	     --> 
	    
	   <!-- 
	   <li><a href="javascript:;">金流管理<i class="fa fa-angle-down"></i></a>
			<ul>
			    <?php //foreach (getPayMethod() as $key=>$val) { ?>
			      <li><a href="<?php //echo getAdminURL('cash/'.$key)?>"><i class="fa fa-file"></i><?php //echo $val?></a></li>
			    <?php //}?>
			</ul>
		</li>
	    -->
	   
	   
	    <!-- 系統管理 -->
	    <?php if( isAdminRoleTbName('system') || isAdminRoleTbName('fee') || isAdminRoleTbName('admin_group')
	    		|| isAdminRoleTbName('admin') || isAdminRoleTbName('tool_elfinder')) { ?>
		<li><a href="javascript:;"><?php echo reSortArrayMutil(2,'system' ,getAdminMenuTop())?><i class="fa fa-angle-down"></i></a>
			<ul>
				<!-- <li><a href="<?php //echo getAdminURL('notice/lists')?>"><i class="fa fa-file"></i><?php //echo reSortArrayMutil(2,'notice' ,getAdminMenu())?></a></li> -->
				<?php if( isAdminRoleTbName('system') ) { ?>
				    <li><a href="<?php echo getAdminURL('system/main')?>">
				        <i class="fa fa-file"></i><?php echo reSortArrayMutil(2,'system' ,getAdminMenu())?></a>
				    </li>
				<?php } ?>
				
				<!-- 
				<?php //if( isAdminRoleTbName('fee') || isAdminRoleTbName('fee')) { ?>
				<li class=" dropdown-submenu"><a href="#">
				   <i class="fa fa-file"></i>運費設定</a>
				     <ul class="dropdown-menu" style="background-color:#55616f">
						<?php //if( isAdminRoleTbName('fee') ) { ?>
					   	   <li>
					   	      <a href="<?php //echo getAdminURL('fee/edit')?>"><?php //echo reSortArrayMutil(2,'fee' ,getAdminMenu())?></a>
						   </li>	
						<?php //} ?>
				        
				        <?php //if( isAdminRoleTbName('fee_cod') ) { ?>
					   	   <li>
							  <a href="<?php //echo getAdminURL('fee_cod/edit')?>"><?php //echo reSortArrayMutil(2,'fee_cod' ,getAdminMenu())?></a>
						   </li>
						<?php //} ?>
					</ul>
				</li>
				<?php //} ?>
			     -->
			    	
				<!-- <li><a href="<?php //echo getAdminURL('system/backup')?>"><i class="fa fa-file"></i>資料庫備份</a></li>
				<?php //if( isAdminRoleTbName('admin_group') || isAdminRoleTbName('admin')) { ?>
				<li class=" dropdown-submenu"><a href="#">
				   <i class="fa fa-file"></i>管理員設定</a>
				     <ul class="dropdown-menu" style="background-color:#55616f">
						<?php //if( isAdminRoleTbName('admin_group') ) { ?>
					   	   <li>
							  <a href="<?php //echo getAdminURL('admin_group/lists')?>"><?php //echo reSortArrayMutil(2,'admin_group' ,getAdminMenu())?></a>
						   </li>	
						<?php //} ?>
				        
				        <?php //if( isAdminRoleTbName('admin') ) { ?>
					   	   <li>
							  <a href="<?php //echo getAdminURL('admin/lists')?>"><?php //echo reSortArrayMutil(2,'admin' ,getAdminMenu())?></a>
						   </li>
						<?php //} ?>   
					</ul>
				</li>
				<?php //} ?>   -->
				
				<?php if( isAdminRoleTbName('admin') ) { ?>
				   <li>
					  <a href="<?php echo getAdminURL('admin/pass?admin_id=2')?>">
				      <i class="fa fa-file"></i><?php echo reSortArrayMutil(2,'admin' ,getAdminMenu())?></a>
				   </li>
				<?php } ?>   
				
				<?php if( isAdminRoleTbName('tool_elfinder') ) { ?>
				   <li>
				      <a href="<?php echo getAdminURL('tool_elfinder/index')?>">
				      <i class="fa fa-file"></i><?php echo reSortArrayMutil(2,'tool_elfinder' ,getAdminMenu())?></a>
				   </li>
			    <?php } ?>
			    
			    <!-- 
			    <?php //if( isAdminRoleTbName('barcode') ) { ?>
				   <li>
				      <a href="<?php //echo getAdminURL('barcode/index')?>">
				      <i class="fa fa-file"></i><?php //echo reSortArrayMutil(2,'barcode' ,getAdminMenu())?></a>
				   </li>
			    <?php //} ?>
			     -->
			</ul>
		</li>
		<?php } ?>
		
	</ul>
	<div class="welcome">
		歡迎
		<?php echo $_SESSION['admin_info']['username'] ?>
		︱ <a href="<?php echo getAdminURL('logout/main')?>">登出</a>
	</div>
</div>
