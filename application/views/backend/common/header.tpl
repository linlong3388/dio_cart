<!DOCTYPE html>
<html lang="zh-Hant">
<head>
<meta charset="utf-8" />
<title><?php echo DIO_META_TITLE?> 管理後台</title>
<meta name="description" content="<?php echo DIO_META_DESCRIPTION?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta http-equiv="Content-type" content="text/html; charset=utf-8">

<link rel="shortcut icon" href="<?php echo base_url('ctmall.png')?>" type="image/png" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('application/views/backend/css/normalize.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('application/views/backend/css/admin.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('application/views/backend/css/font-awesome.min.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('application/views/backend/css/dataTables.bootstrap.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('application/views/backend/css/components-rounded.css')?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('application/views/backend/css/dio_common.css')?>">

<script src="<?php echo base_url('application/views/backend/js/jquery.1.10.2.min.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/jquery.mousewheel.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/jquery.easing.1.3.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/selectivizr-min.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/dataTables/jquery.dataTables.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/dio_plugins/common.js')?>"></script>

</head>

<!-- 引入JS全域變數 -->
<?php $this->load->view("backend/common/js_vars.tpl")?>
