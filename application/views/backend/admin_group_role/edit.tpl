<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'admin_group_role' ,getAdminMenu())?></h2></div>

		<div id="content">

		    <!-- 
			<span style="float: right">
				<div class="actions">
					<a href="javascript:location.href='<?php //echo getAdminURL('admin/add')?>'" class="btn btn-default btn-sm"> <i class="fa fa-pencil"></i> 新增</a>
				</div> 
			</span>
             -->
			
				<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">
			
			 <?php if($this->session->flashdata('msg')){ ?>				 
		        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	         <?php } ?> 		

	    <!-- 資料框 -->
	    <div style="padding-bottom:10px;">
		   <fieldset><legend>資料框:</legend>
		                群組名稱 : <input type="text" value="<?php echo $query['title']?>" readonly>  
		      <button type="button" onclick="javascript:location.href='<?php echo getAdminURL('admin_group/lists')?>'">返回</button>  
		   </fieldset>
	    </div>
				<p></p>

		<!-- 快捷鍵 -->
	    <div style="padding-bottom:10px;">
		   <fieldset><legend>快捷鍵:</legend>
	         <input type="button" class="btn btn-info" onclick="formSubmit()" value="變更權限" />
		   </fieldset>
	    </div>
				<p></p>
				
				<!-- (二)列表主體 -->
				<?php echo form_open( getCurrentFullUrl() )?>
				
				<input style="display:none" type="text" name="admin_group_id" value="<?php echo $query['admin_group_id']?>" />
				
				<div class="clearfix">

					<table>
						<thead>
							<tr> 
							    <td><input type="checkbox" id="selectAll" /></td>
								<td><b>功能名稱</b></td>
							</tr>
						</thead>

						<tbody>

						<?php foreach ($query_role as $key=>$val){ 
						        $isSelect = explode('|', $val); ?>
							<tr>
							   <td>
							        <input type="checkbox" name="<?php echo $key ?>" <?php echo ($isSelect[1] == 1) ? ' checked' : ''?>  value="<?php echo $key ?>" />
								</td> 
								<td class="center"><?php echo $isSelect[0]?></td>
							</tr>
						 <?php } ?>
						 
						</tbody>

					</table>
				</div>

				</form>
				
			</div>


		</div>

	</div>

<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script type="text/javascript">
//-------------------------------------------------------------
//
// 表單送出
//
//-------------------------------------------------------------
function formSubmit(){

    var chk_box=confirm('確定變更權限資料?');
    if (chk_box == true) {
    	$("form").submit();	
    }
}


//-------------------------------------------------------------
//
// 全選&全不選
//
//-------------------------------------------------------------
$('#selectAll').click(function (e) {
    $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
});

</script>

</body>
</html>




