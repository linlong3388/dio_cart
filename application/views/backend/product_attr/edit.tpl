<body>

<style>
.img-responsive {
	width: 48px;
	height: 23px;
	border: 1px solid #999;
}

.img-responsive2 {
	width: 290px;
	height: 190px;
	border: 1px solid #999;
}

.page_wrap--data_area--main_buyer--table--ul li {
	width: 200px;
	height: 200px;
}

.page_wrap--data_area--main_buyer--table--ul li input[type="file"] {
	margin-top: 20px;
}

input[name="vendor_id"] {
	background-color: rgb(228, 228, 225);
}

.page_wrap--data_area--main_buyer--table tr th {
	text-align: center;
}

.page_wrap--data_area--main_buyer--table--ul li {
	width: 200px;
}

b {
	color: red;
}

.fa-star {
	color: red
}

select {
	width: auto
}
td {
    text-align: left;
}
</style>

	<div id="container">
		<div id="content">

			<div class="container editform">
		    
	        <b style="color: red"><?php echo validation_errors()?></b>
	        
			<?php echo form_open( getAdminURL('product_attr/edit') ,array('enctype' => 'multipart/form-data'))?>
		               
				<table>
					<tr>
						<th colspan="2">編輯屬性資料</th>
					</tr>
				
					<tr style="display: none">
						<th width="20%"><i class="fa fa-star"></i>商品編號</th>
						<td><input type="text" name="product_id" 
							value="<?php echo isset($query['product_id']) ? $query['product_id'] : set_value('product_id') ?>" readonly/>
						</td>
					</tr>
					
					<tr style="display: none">
						<th width="20%"><i class="fa fa-star"></i>商品屬性編號</th>
						<td><input type="text" name="product_attr_id" 
							value="<?php echo isset($query['product_attr_id']) ? $query['product_attr_id'] : set_value('product_attr_id') ?>" readonly/>
						</td>
					</tr>
					
   					<tr>
						<th width="20%">圖片</th>
						<td><input style="display: none" class="input-medium focused" id="specification_image" name="image" value="<?php echo isset($query['image']) ? $query['image'] : set_value('image') ;?>">
     	                    <img id="image" width="150px" height="150px" src="<?php echo base_url('resources/uploads/'.$query['image']) ?>" alt="圖片"/>
     	                          <p>                                                       
                                     <a onclick="base_image_upload('image');">選擇圖片</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                     <a onclick="base_image_delete('image');">刪除圖片</a>
                                  </p>
		                </td>
					</tr>
		
				    <tr>
						<th width="20%"><i class="fa fa-star"></i>排序</th>
						<td><input type="text" name="sort_order" 
							value="<?php echo isset($query['sort_order']) ? $query['sort_order'] : set_value('sort_order') ?>" />
						</td>
					</tr>
		
				</table>

				 <!-- 
				<div class="buttons">
					<input type="submit" value="送出" />
					<button type="button" onclick="javascript:close_iframe();">關閉</button>
				</div>                               
                    -->
				
				</form>
			</div>

		</div>

	</div>

	<link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('application/views/backend/css/elfinder.min.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('application/views/backend/css/theme.css')?>" rel="stylesheet">

	<script src="<?php echo base_url('application/views/backend/js/jquery-1.7.2.min.js');?>"></script>
	<script src="<?php echo base_url('application/views/backend/js/jquery-ui-1.8.21.custom.min.js');?>"></script>
	<script src="<?php echo base_url('application/views/backend/js/elfinder.min.js')?>"></script>
	<script src="<?php echo base_url('application/views/backend/js/i18n/elfinder.zh_CN.js')?>"></script>
	<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_image_upload.js')?>"></script>

<script>
//-------------------------------------------------------------
//
// 依[屬性分類]取得商品資料
//
//-------------------------------------------------------------
function get_product_property(property_category_id){
	
	var url = '<?php echo base_url("index.php/backend/product_attr/get_property")?>' + '?property_category_id=' + property_category_id;
	
	$.getJSON(url , function(data) {
	   $('select[name="property_id"]').empty();
	  
       $.each(data, function(i) {
		   $('select[name="property_id"]').append("<option value="+data[i].property_id+">"+data[i].name+"</option>");
		});
		
	});
	
}
</script>
	
</body>
</html>
