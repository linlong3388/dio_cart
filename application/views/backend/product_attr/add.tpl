<body>

<style>
.fa-star {
	color: red
}
td {
    text-align: left;
}
</style>

	<div id="container">

	<div id="content">

	 		<div class="container editform">
	 		
				<?php echo form_open( getAdminURL('product_attr/add') ,array('enctype' => 'multipart/form-data'))?>
		               
				<table>
					<tr>
						<th colspan="2">新增屬性資料</th>
					</tr>
					
					<tr style="display: none">
						<th><i class="fa fa-star"></i>商品編號</th>
						<td><input type="text" name="product_id" value="<?php echo $this->input->get('product_id') ?>" readonly/>
						</td>
					</tr>	
			
					<tr>
						<th><i class="fa fa-star"></i>圖片 :</th>
						<td><input style="display: none" class="input-medium focused" id="specification_image" name="image"
							value="<?php echo set_value('image')?>"> 
							<img id="image" width="150px" height="150px" src="<?php echo base_url("resources/uploads").'/'.set_value('image')?>"
							alt="圖片" /> 
							<br>
							<a onclick="base_image_upload('image');">選擇圖片</a>&nbsp;&nbsp;|&nbsp;&nbsp;
							<a onclick="base_image_delete('image');">刪除圖片</a></td>
					</tr>
				
				</table>

				   <input style="display: none" id="submit_product_attr_add" type="button" onclick="add_attr()" >  
				</form>
	
			</div>

		</div>

	</div>
	
	<link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('application/views/backend/css/elfinder.min.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('application/views/backend/css/theme.css')?>"rel="stylesheet">

	<script src="<?php echo base_url('application/views/backend/js/jquery-1.7.2.min.js')?>"></script>
	<script	src="<?php echo base_url('application/views/backend/js/jquery-ui-1.8.21.custom.min.js');?>"></script>
	<script src="<?php echo base_url('application/views/backend/js/elfinder.min.js')?>"></script>
	<script src="<?php echo base_url('application/views/backend/js/i18n/elfinder.zh_CN.js')?>"></script>
	<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_image_upload.js')?>"></script>
	
	

<script>
//-------------------------------------------------------------
//
// 預設載入頁面 : 頁面所有元素載入完畢後，才觸發
//
//-------------------------------------------------------------
/*
jQuery(window).load(function() {
	get_product_property( $('select[name="property_category_id"] option:selected').val() );	
});
*/


//-------------------------------------------------------------
//
// add_attr
// 商品屬性 / 新增 
//
//-------------------------------------------------------------
function add_attr(){

	 //參數設定
	 var url = '<?php echo getAdminURL('product_attr/add')?>';

     $.post(url ,{data: $('form').serialize()} ,function(err_msg){ 
         //參數驗證
        if(!is_string_empty(err_msg)){
        	alert(err_msg);      	  
            return false;
        }else{            
            location.reload();
        }    
    }); 
        
}

  
//-------------------------------------------------------------
//
// 依[屬性分類]取得商品資料
//
//-------------------------------------------------------------
function get_product_property(property_category_id){
	
	var url = '<?php echo base_url("index.php/backend/product_attr/get_property")?>' + '?property_category_id=' + property_category_id;
	
	$.getJSON(url , function(data) {
	   $('select[name="property_id"]').empty();
	  
       $.each(data, function(i) {
		   $('select[name="property_id"]').append("<option value="+data[i].property_id+">"+data[i].name+"</option>");
		});
		
	});
	
}

</script>
	
</body>
</html>
