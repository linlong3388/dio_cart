<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'product_box' ,getAdminMenu())?></h2></div>

		<div id="content">

			<span style="float: right">
				<div class="actions">
					<a href="javascript:location.href='<?php echo getAdminURL('product_box/add')?>';" class="btn btn-default btn-sm"> 
					<i class="fa fa-pencil"></i> 新增</a>
				</div> 
		    </span>

			<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">
			
			<?php if($this->session->flashdata('msg')){ ?>				 
				  <div class="alert alert-success"><i class="fa fa-check-circle"></i><?php echo $this->session->flashdata('msg')?></div>
			<?php }elseif($this->session->flashdata('msg_err')){ ?>
				  <div class="alert alert-danger"><i class="fa fa-times-circle"></i> <?php echo $this->session->flashdata('msg_err')?></div>
			<?php } ?>			

				<div class="clearfix">
					<table class="dio_datatable">

						<thead>
							<tr>
								<td>圖示</td>
								<!-- <td>編號</td>
								<td>sku</td> -->
								<td>禮盒名稱</td>
								<td>單位</td>
								<td>價格</td>
								<td>排序</td>
								<td>商品狀態</td>
								<td>建立日期</td>
								<td>功能</td>
							</tr>
						</thead>

						<tbody>
						<?php foreach ($query as $row) { ?>
							<tr>
								<td><img width="50px" height="50px" src="<?php echo base_url('resources/uploads/'.$row['image'])?>"></td>
								<!-- <td><?php //echo $row['product_id']?></td>
								<td><?php //echo $row['sku']?></td> -->
								<td><?php echo mb_substr($row['name'],0,20,"utf-8")?></td>
								<td><?php echo reSortArrayMutil(2,$row['unit'],getProductType1Unit())?></td>
								<td><?php echo DIO_CURRENCY.$row['price']?></td>
								<td class="center"><?php echo $row['sort_order']?></td>
								<td><?php echo ($row['status'] == '1') ? "正常" : "<b style='color:red'>停用</b>"?></td>
								<td><?php echo substr($row['cdate'], 0,10) ?><b style="display: none"><?php echo substr($row['cdate'], 11) ?></td>
								<td><a class="btn btn-success" href="<?php echo getAdminURL('product_box/edit?product_id='.$row['product_id'])?>">
										<i class="fa fa-pencil-square-o"></i> 編輯</a> 
									<a class="btn btn-danger" href="javascript:my_confirm(<?php echo $row['product_id']?>);">
										<i class="fa fa-trash"></i> 刪除</a>
								</td>
							</tr>
							<?php } ?>

						</tbody>
					</table>

				</div>
			</div>

		</div>
	</div>

	<link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>" rel="stylesheet">
	<script src="<?php echo base_url('application/views/backend/js/jquery-ui-1.11.1.min.js')?>"></script>
	<script src="<?php echo base_url('application/views//backend/js/dio_plugins/jquery_fields_date.js')?>"></script>

<script type="text/javascript">
	 $(document).ready(function() {
	        $('.dio_datatable').dataTable({
	        	 'aaSorting': [[ 4, 'desc' ]] ,
	          	 "language": { 
	   		         url : "<?php echo base_url("application/views/backend/js/dataTables/i18n/chinese_traditional.lang.json")?>"
	             },
	             "iDisplayLength": 50
	         });
	    });	
	
    function my_confirm(product_id){
       var chk_box=confirm('確定刪除資料?');
       if (chk_box == true) {
             location.href = '<?php echo base_url("index.php/backend/product_box/del?product_id=") ?>'+product_id;
       }
    }
    </script>

</body>
</html>
