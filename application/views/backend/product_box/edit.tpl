<body>

<style>
.img-responsive {
	width: 48px;
	height: 23px;
	border: 1px solid #999;
}

.img-responsive2 {
	width: 290px;
	height: 190px;
	border: 1px solid #999;
}

.page_wrap--data_area--main_buyer--table--ul li {
	width: 200px;
	height: 200px;
}

.page_wrap--data_area--main_buyer--table--ul li input[type="file"] {
	margin-top: 20px;
}

input[name="vendor_id"] {
	background-color: rgb(228, 228, 225);
}

.page_wrap--data_area--main_buyer--table tr th {
	text-align: center;
}

.page_wrap--data_area--main_buyer--table--ul li {
	width: 200px;
}

.fa-star {
	color: red
}

select {
	width: auto
}

td {
    text-align: left;
}

dio_select_title{
    font-weight: bold;
}
</style>


	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'product_box' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			
			 <?php if($this->session->flashdata('msg')){ ?>				 
		       <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	        <?php } ?>		
	        
	        <b style="color: red"><?php echo validation_errors(); ?></b>
	        
			<?php echo form_open( current_url()."?product_id=".$query['product_id'] ,array( 'enctype' => 'multipart/form-data'))?>
				<table>
					<tr>
						<th colspan="2">編輯資料</th>
					</tr>
					
					<tr style="display: none">
						<th width="20%"><i class="fa fa-star"></i>商品編號</th>
						<td><input type="text" name="product_id" 
							value="<?php echo isset($query['product_id']) ? $query['product_id'] : set_value('product_id') ?>" readonly/>
						</td>
					</tr>

					<tr>
						<th width="20%">sku</th>
						<td><input type="text" name="sku" 
							value="<?php echo isset($query['sku']) ? $query['sku'] : set_value('sku') ?>" readonly/>
						</td>
					</tr>
					
					<tr>
						<th><i class="fa fa-star"></i>國際條碼</th>
						<td><input style="width: 60%;"  placeholder="請輸入資料" type="text" name="barcode" value="<?php echo isset($query['barcode']) ? $query['barcode'] : set_value('barcode')?>"  onkeypress="def_numeric()" maxlength="13">
						    <p> 
						    <img width="110px" height="25px" src="<?php echo base_url('resources/barcode/'.$query['barcode'] .'.jpg') ?>" alt="圖片"/>
						</td>
					</tr>
					
					<tr>
						<th><i class="fa fa-star"></i>運費類型</th>
						<td><select name="fee_category_id">
					          <?php foreach ( reSortArrayMutil(3 ,$query['fee_category_id'] ,getFeeCategory()) as $key=>$val) { ?>
 							     <option value="<?php echo $key?>"><?php echo $val?></option>
							  <?php } ?>
							</select> 
						</td>
					</tr>
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>禮盒名稱</th>
						<td><input style="width: 60%;"  placeholder="請輸入資料" type="text" name="name" value="<?php echo isset($query['name']) ? $query['name'] : set_value('name')?>">
						</td>
					</tr>
					
					<tr>
						<th>單位</th>
						<td><select name="unit">
						      <?php foreach ( reSortArrayMutil(3,$query['unit'],getProductType1Unit()) as $key=>$val) { ?>
						         <option value="<?php echo $key?>"><?php echo $val?></option>
						      <?php } ?>
						    </select>
						</td>
					</tr>

					<tr>
						<th><i class="fa fa-star"></i>價格</th>
						<td><input type="text" name="price" value="<?php echo isset($query['price']) ? $query['price'] : set_value('price')?>" onkeypress="def_numeric()" maxlength="12" />元
						</td>
					</tr>
					
   					<tr>
						<th width="20%">主封面圖<br>(640x640)</th>
						<td><input style="display: none" class="input-medium focused" id="specification_image" name="image" value="<?php echo isset($query['image']) ? $query['image'] : set_value('image') ;?>">
     	                    <img id="image" width="150px" height="150px" src="<?php echo base_url('resources/uploads/'.$query['image'])?>" alt="圖片"/>
     	                          <p>                                                       
                                     <a onclick="base_image_upload('image');">選擇圖片</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                     <a onclick="base_image_delete('image');">刪除圖片</a>
                                  </p>
		                </td>
					</tr>
				    				
					<tr>
						<th colspan="2">封面圖</th>
					</tr>
					
					<tr>
						<th>封面圖/編輯</th>
						<td>
							<button type="button" class="btn btn-primary"
								onclick="dlg_product_attr_ins('<?php echo $query['product_id']?>','<?php echo $query['sku']?>')">新增</button>
							<p>
							
							<table id="tb_product_speci" class="dio_datatable">
								<thead>
									<tr>
										<th>圖片</th>
										<th>排序</th>
										<th>功能</th>
									</tr>
								</thead>

								<tbody>
								<?php $index = 0;
								foreach ( $query_attr as $row) { ?>
									<tr>
										<td align="center"><img src="<?php echo base_url(DIO_PATH_PIMG.$row['image'])?>" width="150px" height="150px"/></td>
										<td align="center"><?php echo $row['sort_order']?></td>
										<td align="center">
									  	   <button type="button" class="btn btn-primary"
												onclick="javascript:dlg_product_attr_edit('<?php echo $row['product_attr_id']?>')">編輯</button>
											&nbsp;|&nbsp;
											<button type="button" class="btn btn-danger"
												onclick="javascript:dlg_product_attr_del(<?php echo $row['product_attr_id'] ?>);">刪除</button>
										</td>
									</tr>
									<?php
									$index++;
								} ?>
								</tbody>
							</table></td>
					</tr>
					
					
					<tr>
						<th colspan="2">其他訊息</th>
					</tr>
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>排序</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text"
							name="sort_order"
							value="<?php echo isset($query['sort_order']) ? $query['sort_order'] : set_value('sort_order')?>" onkeypress="def_numeric()" ></td>
					</tr>
					
					<tr>
						<th><i class="fa fa-star"></i>狀態</th>
						<td><input type="radio" name="status" value="1"
						<?php if( isset($query['status']) && ($query['status'] == '1') ) echo set_radio('status', '1' ,true);
						else echo set_radio('status', '1'); ?>>啟用 <input type="radio"
							name="status" value="0"
							<?php if( isset($query['status']) && ($query['status'] == '0') ) echo set_radio('status', '0' ,true);
							else echo set_radio('status', '0'); ?>>停用</td>
					</tr>
					
					<!-- 
					<tr>
						<th colspan="2">Meta頁籤</th>
					</tr>
					
					<tr>
						<th width="20%">Meta標籤標題</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="meta_title" value="<?php echo isset($query['meta_title']) ? $query['meta_title'] : set_value('meta_title')?>">
						</td>
					</tr>
					
					<tr>
						<th width="20%">Meta標籤描述</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="meta_description" value="<?php echo isset($query['meta_description']) ? $query['meta_description'] : set_value('meta_description')?>">
						</td>
					</tr>
					
					<tr>
						<th width="20%">Meta標籤關鍵字</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="meta_keyword" value="<?php echo isset($query['meta_keyword']) ? $query['meta_keyword'] : set_value('meta_keyword')?>">
						</td>
					</tr>
					 -->					

				</table>

				<div class="buttons">
					<input type="submit" value="送出" />
					<button type="button"
						onclick="javascript:location.href='<?php echo getAdminURL('product_box/lists')?>'">返回</button>
				</div>

				</form>
			</div>

		</div>

	</div>
	
	<link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('application/views/backend/css/elfinder.min.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('application/views/backend/css/theme.css')?>" rel="stylesheet">

	<script src="<?php echo base_url('application/views/backend/js/jquery-1.7.2.min.js');?>"></script>
	<script src="<?php echo base_url('application/views/backend/js/jquery-ui-1.8.21.custom.min.js');?>"></script>
	<script src="<?php echo base_url('application/views/backend/js/elfinder.min.js')?>"></script>
	<script src="<?php echo base_url('application/views/backend/js/i18n/elfinder.zh_CN.js')?>"></script>
	<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_image_upload.js')?>"></script>
	<script src="<?php echo base_url('application/views/backend/js/tinymce/tinymce.min.js')?>"></script>
	<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_tinymce_elfinder.js')?>"></script>
	<script src="<?php echo base_url('application/views/backend/js/jquery.blockUI.min.js')?>"></script>
	
<script type="text/javascript"> 
	
/***********************************
/* 名稱 : 
/* 功能 : 文字編輯器的圖片上傳
************************************/
$('#category_func').change(function(){

    var category_func_id = $(this).val();
	 
	var url = "<?php echo getAdminURL('ajax/category_func_select') ?>" + '?category_func_id=' + category_func_id;

	$.getJSON(url , function(data) {
		   $('select[name="category_func_id"]').empty();
		   $.each(data, function(i) {
			   $('select[name="category_func_id"]').append("<option value="+data[i].category_func_id+">"+data[i].name+"</option>");
	    });
	}); 
});


//單筆刪除
function my_confirm(product_speci_id){
  var chk_box=confirm('確定刪除資料?');
  if (chk_box == true) {

	   //參數設定
	   var url = '<?php echo base_url("index.php/backend/product_speci/del?product_speci_id=") ?>'+product_speci_id;
	   
	   //post資料
	   $.post(url ,function(){
		 	 $('<div>處理中 ,請稍候...</div>').dialog();
            location.reload();
	   });
  }
}


//-----------------------------------------------------------------
//
// 商品屬性 / 新增 
//
//-----------------------------------------------------------------
function dlg_product_speci_ins(product_id ,sku){
	
	   $('<div></div>').append($('<iframe id="iframe_product_speci_ins" width="100%" height="100%" frameborder="0" scrolling="no" style="min-width: 95%;height:100%;"/>')
				.attr("src", "<?php echo getAdminURL('product_speci/add?product_id=')?>"+product_id+'&sku='+sku))
		        .dialog({'title' : "新增(商品)屬性",
		                 'width' : "600" ,
		                 'height' : "430" ,
		                 buttons: [{
		                    	     text: "確認",
		                             click: function() {
		                               $('#iframe_product_speci_ins').contents().find('#dio_form1').click();
		                                $(this).dialog( "close" );
		                                
		                                $('<div>處理中 ,請稍候...</div>').dialog();

		                                parent.location.reload();
			                         }
		                           },
		                           {
		                	         text: "取消", 
		       	                     click: function(){
		       		                   $(this).dialog( "close" );
		                             }
		                          }]
		  });
  }


//-----------------------------------------------------------------
//
// 商品屬性 / 編輯 
//
//-----------------------------------------------------------------
  function dlg_product_speci_edit(sku2){
		
   $('<div></div>').append($('<iframe id="iframe_product_speci_edit" width="100%" height="100%" frameborder="0" scrolling="no" style="min-width: 95%;height:100%;"/>')
		.attr("src", "<?php echo getAdminURL('product_speci/edit?sku2=')?>"+sku2))
        .dialog({'title' : "編輯(商品)屬性",
                 'width' : "600" ,
                 'height' : "430" ,
                 buttons: [{
                    	     text: "確認",
                             click: function() {
                               $('#iframe_product_speci_edit').contents().find('form').submit();
                               $(this).dialog( "close" );
                              
                               $('<div>處理中 ,請稍候...</div>').dialog();
                               parent.location.reload();
                             }
                           },
                           {
                	         text: "取消", 
       	                     click: function(){
       		                   $(this).dialog( "close" );
                             }
                          }]
  });

 }           


//-----------------------------------------------------------------
//
// 商品屬性 / 刪除 
//
//-----------------------------------------------------------------  
function dlg_product_speci_del(product_speci_id){
   var chk_box=confirm('確定刪除資料?');
     if (chk_box == true) {
   	   //參數設定
   	   var url = '<?php echo base_url("index.php/backend/product_speci/del?product_speci_id=") ?>'+product_speci_id;
    	   
   	   //post資料
   	   $.post(url ,function(){
	 	   $('<div>處理中 ,請稍候...</div>').dialog();
           location.reload();
        });
     }
}


//-----------------------------------------------------------------
//
// 功能屬性 / 新增 
//
//-----------------------------------------------------------------
function dlg_product_attr_ins(product_id ,sku){
	
	   $('<div></div>').append($('<iframe id="iframe_product_attr_ins" width="100%" height="100%" frameborder="0" scrolling="no" style="min-width: 95%;height:100%;"/>')
				.attr("src", "<?php echo getAdminURL('product_attr/add?product_id=')?>"+product_id+'&sku='+sku))
		        .dialog({'title' : "新增/產品縮圖",
		                 'width' : "860" ,
		                 'height' : "420" ,
		                 buttons: [{
		                    	     text: "確認",
		                             click: function() {
		                               $('#iframe_product_attr_ins').contents().find('#submit_product_attr_add').click();
		                               $(this).dialog( "close" );

		                               $('<div>處理中 ,請稍候...</div>').dialog();
		                               
		                               parent.location.reload();
		                             }
		                           },
		                           {
		                	         text: "取消", 
		       	                     click: function(){
		       		                   $(this).dialog( "close" );
		                             }
		                          }]
		  });
  }

  
//-----------------------------------------------------------------
//
// 功能屬性 / 編輯 
//
//-----------------------------------------------------------------
function dlg_product_attr_edit(product_attr_id){
	
	   $('<div></div>').append($('<iframe id="iframe_product_attr_edit" width="100%" height="100%" frameborder="0" scrolling="no" style="min-width: 95%;height:100%;"/>')
				.attr("src", "<?php echo getAdminURL('product_attr/edit?product_attr_id=')?>"+product_attr_id))
		        .dialog({'title' : "編輯/產品縮圖",
	                     'width' : "860" ,
	                     'height' : "550" ,
		                 buttons: [{
		                    	     text: "確認",
		                             click: function() {
		                               $('#iframe_product_attr_edit').contents().find('form').submit();
		                               $(this).dialog( "close" );

		                               $('<div>處理中 ,請稍候...</div>').dialog();
		                               
		                               parent.location.reload();
		                             }
		                           },
		                           {
		                	         text: "取消", 
		       	                     click: function(){
		       		                   $(this).dialog( "close" );
		                             }
		                          }]
		  });
  }

//-----------------------------------------------------------------
//
// 功能屬性 / 刪除 
//
//-----------------------------------------------------------------  
function dlg_product_attr_del(product_attr_id){
   var chk_box=confirm('確定刪除資料?');
     if (chk_box == true) {
   	   //參數設定
   	   var url = '<?php echo base_url("index.php/backend/product_attr/del?product_attr_id=") ?>'+product_attr_id;
    	   
   	   //post資料
   	   $.post(url ,function(){
	 	   $('<div>處理中 ,請稍候...</div>').dialog();
           location.reload();
        });
     }
} 
 

</script>


</body>
</html>
