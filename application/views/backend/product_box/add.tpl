<body>

<style>
.img-responsive {
	width: 200px;
	height: 200px;
	border: 1px solid #999;
}

.page_wrap--data_area--main_buyer--table--ul li {
	width: 200px;
	height: 200px;
}

.page_wrap--data_area--main_buyer--table--ul li input[type="file"] {
	margin-top: 20px;
}

.fa-star {
	color: red
}

td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'product_box' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			
			 <b style="color: red"><?php echo validation_errors(); ?></b>
			
			<?php echo form_open( getCurrentFullUrl() ,array( 'enctype' => 'multipart/form-data'))?>
				<table>
					<tr>
						<th colspan="2">新增禮盒</th>
					</tr>
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>國際條碼</th>
						<td><input placeholder="請輸入資料" type="text" name="barcode" value="<?php echo set_value('barcode')?>"  onkeypress="def_numeric()" maxlength="13"></td>
					</tr>
					
					<tr>
						<th><i class="fa fa-star"></i>運費類型</th>
						<td><select name="fee_category_id">
					          <?php foreach ( getFeeCategory() as $key=>$val) { ?>
 							     <option value="<?php echo $key?>"><?php echo $val?></option>
							  <?php } ?>
							</select> 
						</td>
					</tr>

					<tr>
						<th width="20%"><i class="fa fa-star"></i>禮盒名稱</th>
						<td><input placeholder="請輸入資料" type="text" name="name" value="<?php echo set_value('name')?>"></td>
					</tr>
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>單位</th>
						<td><select name="unit">
					          <?php foreach (getProductType1Unit() as $key=>$val){ ?>
 							     <option value="<?php echo $key?>"><?php echo $val?></option>
							  <?php } ?>
							</select> 
					    </td>
					</tr>

					<tr>
						<th width="20%"><i class="fa fa-star"></i>價格</th>
						<td><input placeholder="請輸入資料" type="text" name="price" value="<?php echo set_value('price')?>" onkeypress="def_numeric()" maxlength="12">元
						</td>
					</tr>
					
				   <tr>
					   <th width="20%">主封面圖<br>(640x640)</th>
					   <td>
					       <input style="display: none" class="input-medium focused" name="image" value="<?php echo isset($query['image']) ? $query['image'] : set_value('image') ;?>">
     	                   <img id="image" class="img-responsive" src="<?php echo (!empty($query['image'])) ? base_url("resources/uploads/".$query['image']) : base_url("resources/uploads").'/'.set_value('image')?>" alt="圖片"/>
                              <p>                                                       
                                 <a onclick="base_image_upload('image');">選擇圖片</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                 <a onclick="base_image_delete('image');">刪除圖片</a>
                              </p>
		               </td>
				   </tr>
				
				</table>

				<div class="buttons">
					<!-- <input type="submit" value="下一步" />  -->
					<input type="submit" value="送出" />
					<button type="button"
						onclick="javascript:location.href='<?php echo getAdminURL('product_box/lists?page=1')?>'">返回</button>
				</div>

				</form>
			</div>

		</div>

	</div>
	
    <link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('application/views/backend/css/elfinder.min.css')?>" rel="stylesheet">
    
    <script src="<?php echo base_url('application/views/backend/js/jquery-1.7.2.min.js');?>"></script>
    <script src="<?php echo base_url('application/views/backend/js/jquery-ui-1.8.21.custom.min.js');?>"></script>
    <script src="<?php echo base_url('application/views/backend/js/elfinder.min.js')?>"></script>
    <script src="<?php echo base_url('application/views/backend/js/i18n/elfinder.zh_CN.js')?>"></script>
	<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_image_upload.js')?>"></script>
	<script src="<?php echo base_url('application/views/backend/js/tinymce/tinymce.min.js')?>"></script>
	<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_tinymce_elfinder.js')?>"></script>

</body>
</html>
