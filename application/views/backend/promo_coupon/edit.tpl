<body>

<style>
.fa-star {
	color: red
}
td {
    text-align: left;
}
#enable_category {
   width: 250px; 
   height: 200px; 
}
#enable_product {
   width: 250px; 
   height: 200px; 
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'promo_coupon' ,getAdminMenu())?></h2></div>

		<div id="content">
		
		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			
			 <?php if($this->session->flashdata('msg')){ ?>				 
		        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	         <?php } ?> 
			
			 <b style="color: red"><?php echo validation_errors(); ?></b> 
			
			<?php echo form_open( getCurrentFullUrl() )?>
				
				<table>
					<tr>
						<th colspan="2">編輯資料</th>
					</tr>

				   <tr>
						<th width="20%">代碼</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="code"
							value="<?php echo $query['code']?>" readonly /> 
						</td>
					</tr>					
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>名稱</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="name"
							value="<?php echo isset($query['name']) ? $query['name'] : set_value('name')?>"> 
					    </td>
					</tr>
                   
					<tr>
						<th width="20%"><i class="fa fa-star"></i>類型</th>
						<td>
						   	<select class="fullwidth" name="code_type">
						      <?php foreach ( reSortArrayMutil(3,$query['code_type'],getPromoCouponCodeType()) as $key=>$val) { ?>
						        <option value="<?php echo $key?>"><?php echo $val?></option>
							  <?php }?>
						    </select>
						</td>
					</tr>

					<tr>
						<th><i class="fa fa-star"></i>折扣</th>
						<td><input type="text" name="code_val" value="<?php echo isset($query['code_val']) ? $query['code_val'] : set_value('code_val') ?>"
							onkeypress="def_numeric()" maxlength="12" />
						</td>
					</tr>
					
					<!-- 
					<tr>
						<th>限定分類<br>
							<span style="color: red">"新增分類"請選擇下拉選單<br>
"刪除分類"請在該分類項目上點兩下</span></th>
						<td>
						  <select onchange="sltToTextarea('enable_category',this.value)">
                             <?php //foreach ($query_product_category as $row){ ?>
                                <option value="<?php //echo $row['category_id']?>|<?php //echo $row['name']?>"><?php //echo $row['name']?></option>
                             <?php //} ?>
                          </select>
						  
						  <br />
						  
						  <select name="enable_category[]" id="enable_category" multiple="multiple" ondblclick="delTextarea('enable_category',this.value)"></select> 
						
						</td>
					</tr>
					
					<tr>
						<th>限定商品<br>
							<span style="color: red">"新增分類"請選擇下拉選單<br>
"刪除分類"請在該分類項目上點兩下</span></th>
						<td>
						  <select onchange="sltToTextarea('enable_product',this.value)">
                             <?php //foreach ($query_product as $row){ ?>
                                <option value="<?php //echo $row['product_id']?>|<?php //echo $row['name']?>"><?php //echo $row['name']?></option>
                             <?php //} ?>
                          </select>
						  
						  <br />
						  
						  <select name="enable_product[]" id="enable_product" multiple="multiple" ondblclick="delTextarea('enable_product',this.value)"></select> 
						
						</td>
					</tr>
					 -->
					
					<tr>
						<th><i class="fa fa-star"></i>購物總金額</th>
						<td><input type="text" name="total" value="<?php echo isset($query['total']) ? $query['total'] : set_value('total')?>"
							onkeypress="def_numeric()" maxlength="12" />元 
						</td>
					</tr>
					
					<tr>
						<th width="20%">使用總數</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="use_total"
							value="<?php echo isset($query['use_total']) ? $query['use_total'] : set_value('use_total')?>" /> 
						</td>
					</tr>
					
					<tr>
						<th width="20%">已使用次數</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="use_count"
							value="<?php echo $query_coupon?>" readonly /> 
						</td>
					</tr>
					
					<tr>
						<th width="20%">會員可使用次數</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="use_count_customer"
							value="<?php echo isset($query['use_count_customer']) ? $query['use_count_customer'] : set_value('use_count_customer')?>" /> 
						</td>
					</tr>	
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>活動開始日期</th>
						<td><input class="fullwidth dio_date" type="text" name="sdate"
							value="<?php echo isset($query['sdate']) ? $query['sdate'] : set_value('sdate')?>"> 
						</td>
					</tr>

					<tr>
						<th width="20%"><i class="fa fa-star"></i>活動結束日期</th>
						<td><input class="fullwidth dio_date" type="text" name="edate"
							value="<?php echo isset($query['edate']) ? $query['edate'] : set_value('edate')?>"> 
						</td>
					</tr>

					<!-- 
					<tr>
						<th width="20%"><i class="fa fa-star"></i>使用有效日期</th>
						<td><input class="fullwidth dio_date" type="text" name="udate"
							value="<?php //echo isset($query['udate']) ? $query['udate'] : set_value('udate')?>"> 
						</td>
					</tr>
					 -->				
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>狀態</th>
						<td><input type="radio" name="status" value="1"
						    <?php echo ($query['status'] == '1') ? ' checked' : '' ?>> 啟用
						    <input type="radio" name="status" value="0"
							<?php echo ($query['status'] == '0') ? ' checked' : '' ?>> 停用
					    </td>
					</tr>				
				
				</table>

				<div class="buttons">
					<input type="submit" value="送出" />
					<button type="button" onclick="javascript:location.href='<?php echo getAdminURL('promo_coupon/lists')?>'">返回</button>
				</div>

				</form>
			</div>

		</div>

	</div>

<script type="text/javascript">
//--------------------------------------------------------------------------------
//
// 預設載入
//
//--------------------------------------------------------------------------------
$(document).ready(function() {

  var is_enable_category = '<?php echo $query['is_enable_category']?>';
  var is_enable_product = '<?php echo $query['is_enable_product']?>';

  //取得限定分類資料
  if(is_enable_category == '1'){
	  getEnableCategoryData();
  }	  	

  //取得限定商品資料
  if(is_enable_product == '1'){
	  getEnableProductData();
  }	 
	
  //表單送出
  $( "form" ).submit(function( event ) {
        $('#enable_category option').prop('selected', true);
        $('#enable_product option').prop('selected', true);
   });
   
});


//--------------------------------------------------------------------------------
//
// 取得限定分類資料
//
//--------------------------------------------------------------------------------
function getEnableCategoryData(){
	
	var data = '<?php echo $query["enable_category"]?>';

	data = data.split(",");
	
	$.each(data, function(key, val) {
		sltToTextarea('enable_category' ,val);
	});
}


//--------------------------------------------------------------------------------
//
// 取得限定商品資料
//
//--------------------------------------------------------------------------------
function getEnableProductData(){
	
	var data = '<?php echo $query["enable_product"]?>';

	data = data.split(",");
	
	$.each(data, function(key, val) {
		sltToTextarea('enable_product' ,val);
	});
}
</script>


<link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>" rel="stylesheet">

<script src="<?php echo base_url('application/views/backend/js/jquery-1.7.2.min.js');?>"></script>
<script src="<?php echo base_url('application/views/backend/js/jquery-ui-1.8.21.custom.min.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_fields_date.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_fields_sltMulti.js')?>"></script>


</body>
</html>

