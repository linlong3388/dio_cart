<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'promo_coupon' ,getAdminMenu())?></h2></div>

		<div id="content">

			<span style="float: right">
				<div class="actions">
					<a href="javascript:location.href='<?php echo getAdminURL('promo_coupon/add')?>'"
						class="btn btn-default btn-sm"> <i class="fa fa-pencil"></i> 新增</a>
				</div> </span>

				<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">
			
			 <?php if($this->session->flashdata('msg')){ ?>				 
		        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	         <?php } ?> 

				<!-- (一)搜尋主體 -->

				<p></p>

				<!-- (二)列表主體 -->
				<div class="clearfix">
					<table class="dio_datatable">
						<thead>
							<tr>
							    <td>名稱</td>
							    <td>代碼</td>
								<td>類型</td>
								<td>折扣</td>
								<td>開始日期</td>
								<td>結束日期</td>
								<td>狀態</td>
								<td>功能</td>
							</tr>
						</thead>

						<tbody>
						<?php foreach ($query as $row){ ?>
							<tr>
							    <td class="center"><?php echo $row['name']?></td>
								<td class="center"><?php echo $row['code']?></td>
								<td class="center"><?php echo reSortArrayMutil(2,$row['code_type'],getPromoCouponCodeType())?></td>
								<td class="center"><?php echo $row['code_val']?></td>
								<td class="center"><?php echo $row['sdate']?></td>
								<td class="center"><?php echo $row['edate']?></td>
								<td class="center"><?php echo ($row['status'] == 1) ? "啟用" : "<b style='color:red'>停用</b>"?></td>
								<td class="center">
								  <a class="btn btn-info" href="<?php echo getAdminURL('promo_coupon/edit?promo_coupon_id='.$row['promo_coupon_id'])?>">
								 	 <i class="fa fa-pencil-square-o"></i> 編輯</a> 
								  <a class="btn btn-danger" href="javascript:my_confirm(<?php echo $row['promo_coupon_id']?>);">
								     <i class="fa fa-trash"></i> 刪除</a>
								</td>
							</tr>
							<?php } ?>
						</tbody>

					</table>
				</div>

			</div>

		</div>

	</div>

	<script type="text/javascript">
    $(document).ready(function() {
        $('.dio_datatable').dataTable({
          	 "language": { 
   		         url : "<?php echo base_url("application/views/backend/js/dataTables/i18n/chinese_traditional.lang.json")?>"
   		     },
   		     "order": [[ 4, "desc" ]]
         });
    });
   
    function my_confirm(promo_coupon_id){
       var chk_box=confirm('確定刪除資料?');
       if (chk_box == true) {
             location.href = '<?php echo base_url("index.php/backend/promo_coupon/del?promo_coupon_id=") ?>'+promo_coupon_id;
       }
    }
</script>

</body>
</html>

