<body>

<style>
.fa-star {
	color: red
}
td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'promo' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			
		    <?php if($this->session->flashdata('msg')){ ?>				 
		        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	        <?php } ?>		
			
			<b style="color: red"><?php echo validation_errors()?></b>	
			
			<?php echo form_open( getCurrentFullUrl() )?>
				<table>
					<tr>
						<th colspan="2">編輯資料</th>
					</tr>
					
					<tr>
						<th><i class="fa fa-star"></i>總金額</th>
						<td><input type="text" name="total" value="<?php echo isset($query['total']) ? $query['total'] : set_value('total')?>"
							onkeypress="def_numeric()" maxlength="12" />元 
						</td>
					</tr>
										
					<tr>
						<th><i class="fa fa-star"></i>回饋金額</th>
						<td><input type="text" name="money" value="<?php echo isset($query['money']) ? $query['money'] : set_value('money')?>"
							onkeypress="def_numeric()" maxlength="12" />元 
						</td>
					</tr>
					
					<tr>
						<th>回饋金額/上限</th>
						<td><input type="text" name="money_limit" value="<?php echo isset($query['money_limit']) ? $query['money_limit'] : set_value('money_limit')?>"
							onkeypress="def_numeric()" maxlength="12" />元 
						</td>
					</tr>					
							
					<tr>
						<th width="20%"><i class="fa fa-star"></i>狀態</th>
						<td><input type="radio" name="status" value="1"
						    <?php echo ($query['status'] == '1') ? ' checked' : '' ?>> 啟用
						    <input type="radio" name="status" value="0"
							<?php echo ($query['status'] == '0') ? ' checked' : '' ?>> 停用
					    </td>
					</tr>				
				
				</table>

				<div class="buttons">
					<input type="submit" value="送出" />
				</div>

				</form>
			</div>

		</div>

	</div>

</body>
</html>

