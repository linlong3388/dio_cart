<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'story_img' ,getAdminMenu())?></h2></div>
	
		<div id="content">

			<span style="float: right">
				<div class="actions">
					<a href="javascript:location.href='<?php echo getAdminURL('story_img/add')?>'"
						class="btn btn-default btn-sm"> <i class="fa fa-pencil"></i> 新增</a>
				</div> </span>

				<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">

			 <?php if($this->session->flashdata('msg')){ ?>				 
		        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	         <?php } ?> 			
			
				<!-- (一)搜尋主體 -->

				<p></p>

				<!-- (二)列表主體 -->
				<div class="clearfix">

					<table class="dio_datatable">
						<thead>
							<tr>
								   <!-- <th>標題</th> -->
									<th>圖片</th>
									<th>說明</th>
							   		<th>狀態</th>
							   		<td>排序</td>									
									<th>功能</th>
							</tr>
						</thead>

						<tbody>

						<?php foreach ($query as $row){ ?>
								<tr>
								     <!-- <td class="center"><?php //echo $row['title'] ?></td>	-->						
									<td class="center" width="20%"><img src="<?php echo base_url('resources/uploads/'.$row['image'])?>" height="80px" width="100px"></td>	
									<td class="center"><?php echo $row['image_alt'] ?></td>
									<td class="center"><?php echo ($row['status'] == 1) ? '啟用' : "<b style='color:red'>停用</b>"?></td>							
									<td class="center"><?php echo $row['sort_order']?></td>
									<td class="center">
									    <a class="btn btn-info" href="<?php echo getAdminURL('story_img/edit?common_picture_id='.$row['common_picture_id'])?>">
									       <i class="icon-view icon-white"></i>編輯</a> 
									    <a class="btn btn-danger" href="javascript:my_confirm(<?php echo $row['common_picture_id']?>);">
									       <i class="icon-trash icon-white"></i>刪除</a>
									</td>
								</tr>
							<?php } ?>

						</tbody>

					</table>
				</div>

			</div>


		</div>

	</div>

	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script type="text/javascript">
	 $(document).ready(function() {
        $('.dio_datatable').dataTable({
        	 'aaSorting': [[ 3, 'desc' ]] ,
          	 "language": { 
   		         url : "<?php echo base_url("application/views/backend/js/dataTables/i18n/chinese_traditional.lang.json")?>"
             },
             "iDisplayLength": 50
         });
    });
   	
    function my_confirm(common_picture_id){
       var chk_box=confirm('確定刪除資料?');
       if (chk_box == true) {
             location.href = '<?php echo base_url("index.php/backend/story_img/del?common_picture_id=") ?>' + common_picture_id;
       }
    }
</script>

</body>
</html>




