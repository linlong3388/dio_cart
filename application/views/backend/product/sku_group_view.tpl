<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'product' ,getAdminMenu())?></h2></div>

		<div id="content">
		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">

				<!-- (一)搜尋主體 -->
				<fieldset>
					<legend>搜尋條件:</legend>
					sku群組編號 :
					<?php echo $this->input->get('sku')?>
					<br />
					<button type="button"
						onclick="javascript:location.href='<?php echo getAdminURL('product/sku_group_list')?>'">返回</button>
				</fieldset>

				<p></p>

				<!-- (二)列表主體 -->
				<div class="clearfix">
					<table class="dio_datatable">
						<thead>
							<tr>
								<td>sku編號</td>
								<td>商品名稱</td>
								<td>日期</td>
							</tr>
						</thead>

						<tbody>

						<?php foreach ($query as $row){ ?>
							<tr>
								<td><?php echo $row['sku']?>
								</td>
								<td><?php echo $row['name']?>
								</td>
								<td><?php echo substr($row['cdate'], 0,10) ?><b style="display: none"><?php echo substr($row['cdate'], 11) ?></td>
							</tr>
							<?php } ?>

						</tbody>
					</table>
				</div>

			</div>

		</div>

	</div>

	<script type="text/javascript">
    $(document).ready(function() {
        $('.dio_datatable').dataTable({
          	 "language": { 
   		                  url : "<?php echo base_url("application/views/backend/js/dataTables/i18n/chinese_traditional.lang.json")?>"
                         },
             "iDisplayLength": 100            
         });
    });

    function my_confirm( parent_category_id ,category_id ){

       var host = "<?php echo base_url() ?>";
       var param  = "index.php/backend/product_category_child/del?parent_category_id="+parent_category_id+"&category_id="+category_id;
        
       var chk_box=confirm('確定刪除資料?');
       if (chk_box == true) {
           location.href = host+param;
       }
    }
</script>



</body>
</html>
