<body>

	<style>
.img-responsive {
	width: 48px;
	height: 23px;
	border: 1px solid #999;
}

input[name="product_id"] {
	background-color: rgb(228, 228, 225);
}

.fa-star {
	color: red
}

th {
	padding: 20px
}

select {
	width: auto
}

td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'product' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			<?php echo form_open( getCurrentFullUrl() ) ?>
				<table>
					<tr>
						<th colspan="2">新增 Step2 / 屬性編輯</th>
					</tr>

					<tr style="display: none">
						<th><i class="fa fa-star"></i>商品編號 :</th>
						<td><input type="text" name="product_id" value="<?php echo $query['product_id'] ?>"/></td>
					</tr>

					<tr>
						<th>sku :</th>
						<td><input type="text" name="sku" style="width: 30%" value="<?php echo $query['sku'] ?>" readonly /></td>
					</tr>

					<tr>
						<th><i class="fa fa-star"></i>屬性/尺寸 :</th>
						<td><select name="property_id_size">
						      <?php foreach ( transKeyPairArray(getPropertyCategoryItem(0) ,'property_id' ,'name')  as $key=>$val) { ?>
						         <option value="<?php echo $key?>"><?php echo $val?></option>
						      <?php } ?>
						    </select>
				    	 </td>
					</tr>
					
					<tr>
						<th><i class="fa fa-star"></i>屬性/顏色 :</th>
						<td><select name="property_id_color">
						      <?php foreach ( transKeyPairArray(getPropertyCategoryItem(1) ,'property_id' ,'name') as $key=>$val) { ?>
						         <option value="<?php echo $key?>"><?php echo $val?></option>
						      <?php } ?>
						    </select>
						</td>
					</tr>
					
					<tr>
						<th><i class="fa fa-star"></i>子sku :</th>
						<td><input type="text" name="sku2" style="width: 30%" value="<?php echo set_value('sku2')?>"/></td>
					</tr>
					
					<tr>
						<th>規格 :</th>
						<td><input type="text" name="speci" style="width: 30%" value="<?php echo set_value('speci')?>"/></td>
					</tr>

					<tr>
						<th><i class="fa fa-star"></i>庫存量 :</th>
						<td><input type="text" name="stock" style="width: 30%" value="<?php echo set_value('stock')?>" onkeypress="def_numeric()" /></td>
					</tr>

					<!-- 
					<tr>
						<th>主圖 :</th>
						<td><input style="display: none" class="input-medium focused" id="specification_image" name="image" value="<?php //echo set_value('image')?>">
							<img id="image" style="width: 290px; height: 190px" class="img-responsive" src="<?php //echo base_url("resources/uploads").'/'.set_value('image')?>" alt="圖片" /> 
							<br>
							<a onclick="base_image_upload('image');">選擇圖片</a>&nbsp;&nbsp;|&nbsp;&nbsp;
							<a onclick="base_image_delete('image');">刪除圖片</a>
						</td>
					</tr>
					 -->

					<tr>
						<th></th>
						<td>
							<button type="button" onclick="add_speci()">新增項目</button>
						</td>
					</tr>

					<tr>
						<th width="20%">屬性列表</th>
						<td>
							<table class="dio_datatable" style="width: 60%">
								<thead>
									<tr>
									    <!-- <th>主圖</th>  -->
										<th>sku</th>
										<th>子sku</th>
										<th>屬性/尺寸</th>
										<th>屬性/顏色</th>
										<th>庫存</th>
										<th>功能</th>
									</tr>
								</thead>

								<tbody>
								<?php foreach ( $query_speci as $row) { ?>
									<tr>
									    <!-- <td align="center"><img width="100px" height="100px" src="<?php //echo base_url('resources/uploads/'.$row['image'])?>"></td>  -->
										<td align="center"><?php echo $row['sku']?></td>
										<td align="center"><?php echo $row['sku2']?></td>
										<td align="center"><?php echo $row['prs_name']?></td>
										<td align="center"><?php echo $row['prc_name']?></td>
										<td align="center"><?php echo $row['stock']?></td>
										<td align="center">
											<button type="button" class="btn btn-danger"
												onclick="javascript:del_specification(<?php echo $row['product_speci_id'] ?>);">刪除</button>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table></td>
					</tr>

				</table>

				<div class="buttons">
					<input type="submit" value="完成" />
					<button type="button"
						onclick="javascript:location.href='<?php echo getAdminURL('product/lists')?>'">返回</button>
				</div>
			</form>

			</div>

		</div>

	</div>

	<link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>"
		rel="stylesheet">
	<link href="<?php echo base_url('application/views/backend/css/elfinder.min.css')?>"
		rel="stylesheet">
	<link href="<?php echo base_url('application/views/backend/css/theme.css')?>"
		rel="stylesheet">

	<script src="<?php echo base_url('application/views/backend/js/jquery-1.7.2.min.js')?>"></script>
	<script 
		src="<?php echo base_url('application/views/backend/js/jquery-ui-1.8.21.custom.min.js');?>"></script>
	<script
		src="<?php echo base_url('application/views/backend/js/elfinder.min.js')?>"></script>
	<script
		src="<?php echo base_url('application/views/backend/js/i18n/elfinder.zh_CN.js')?>"></script>
	<script
		src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_image_upload.js')?>"></script>
	
	<script type="text/javascript">
/***********************************
/* 名稱 : add_speci
/* 功能 : 商品屬性 / 新增 
************************************/
function add_speci(){

	 //參數設定
	 var url = '<?php echo getAdminURL('product_speci/add')?>';

     $.post(url ,{data: $('form').serialize()} ,function(err_msg){ 
         //參數驗證
        if(!is_string_empty(err_msg)){
        	alert(err_msg);      	  
            return false;
        }else{
        	$('<div>處理中 ,請稍候...</div>').dialog();
            location.reload();
        }    
    }); 
        
  }


/***********************************
/* 名稱 : del_specification
/* 功能 : 商品屬性 / 刪除 
************************************/
 function del_specification(product_speci_id){
   var chk_box=confirm('確定刪除資料?');
   if (chk_box == true) {

	   //參數設定
	   var url = '<?php echo base_url("index.php/backend/product_speci/del?product_speci_id=") ?>'+product_speci_id;
	   
	   //post資料
	   $.post(url ,function(){
		 	 $('<div>處理中 ,請稍候...</div>').dialog();
             location.reload();
	   });
   }
 }

</script>

</body>
</html>
