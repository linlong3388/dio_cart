<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'product_category' ,getAdminMenu())?></h2></div>

		<div id="content">
		    <!-- 
         	<span style="float: right">
				<div class="actions">
					<a href="javascript:location.href='<?php //echo getAdminURL('product_category/add')?>'" class="btn btn-default btn-sm"> <i class="fa fa-pencil"></i> 新增</a>
				</div>
			</span>
             -->
            
			<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">
			
			   <?php if($this->session->flashdata('msg')){ ?>				 
				  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
			   <?php }elseif($this->session->flashdata('msg_err')){ ?>
				  <div class="alert alert-danger"><i class="fa fa-times-circle"></i> <?php echo $this->session->flashdata('msg_err')?></div>
			   <?php } ?>		
		 
				<!-- (一)搜尋主體 -->
				<p></p>

				<!-- (二)列表主體 -->
				<div class="clearfix">

					<table class="dio_datatable">
						<thead>
							<tr>
								<td>(頂級)分類級別</td>
								<td>(次級)分類數</td>
								<!--  <td>排序</td>
								<td>狀態</td>
								<td>商品總數</td>
								<td>建立日期</td> -->
								<td>功能</td>
							</tr>
						</thead>

						<tbody>

						<?php foreach ($query as $row){ ?>
							<tr>
								<td class="center"><?php echo empty($row['path']) ? "<b style='color:blue'>" . $row['name'] . "</b>" : $this->cls_category->get_name($row['path'])?></td>
								<td class="center dio_href">
								  <?php if( $row['category_id'] > 1){ ?>
								   <a href="<?php echo getAdminURL('product_category_child/lists?category_id='.$row['category_id'])?>" class="btn btn-success"><?php echo $row['count']?></a>
								 <?php } ?>
								</td>
								<!--
								<td class="center"><?php //echo $row['sort_order']?></td>
								<td class="center"><?php //echo ($row['status'] == '1') ? "正常" : "<b style='color:red'>停用</b>"?></td>
								<td class="center"><?php //echo $row['pt_total']?></td> 
								<td class="center"><?php //echo substr($row['created_at'],0,10)?></td> -->
								<td class="center">
								    <a class="btn btn-success"
									href="<?php echo getAdminURL('product_category/edit?category_id='.$row['category_id'])?>">
										<i class="fa fa-pencil-square-o"></i> 編輯</a> 
								    
								    <!-- 
								     <?php //if( $row['category_id'] > 1){ ?>
								      <a class="btn btn-danger" href="javascript:my_confirm(<?php //echo $row['category_id']?>);">
								          <i class="fa fa-trash"></i> 刪除</a>
								     <?php //} ?>  -->   
								</td>
							</tr>
							<?php } ?>

						</tbody>

					</table>
					
				</div>

			</div>

		</div>

	</div>

	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script type="text/javascript">
	 /*
	 $(document).ready(function() {
        $('.dio_datatable').dataTable({
          	 "language": { 
   		         url : "<?php //echo base_url("application/views/backend/js/dataTables/i18n/chinese_traditional.lang.json")?>"
             },
             "iDisplayLength": 50
         });
    });*/

   	/*
    function my_confirm(category_id){
       var chk_box=confirm('確定刪除資料?');
       if (chk_box == true) {
             location.href = '<?php //echo base_url("index.php/backend/product_category/del?category_id=") ?>'+category_id;
       }
    }*/
</script>

</body>
</html>




