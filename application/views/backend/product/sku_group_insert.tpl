<body>

	<style>
.fa-star {
	color: red
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'product' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			<?php echo form_open("backend/product/sku_group_insert" ,array( 'enctype' => 'multipart/form-data'))?>
				<table>
					<tr>
						<th colspan="2">新增資料</th>
					</tr>

					<tr>
						<th width="20%"><i class="fa fa-star"></i>商品類別</th>
						<td><select name="category_id">
						<?php foreach ($this->cls_category->get_last_parent() as $row) { ?>
								<option value="<?php echo $row['category_id']?>">
									<span><?php echo $this->cls_category->get_name($row['path'])?>
									</span>
								</option>
								<?php }?>
						</select></td>
					</tr>

					<tr>
						<th width="20%"><i class="fa fa-star"></i>sku群組代號</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text"
							name="sku"
							value="<?php echo set_value('sku')?>"> <b
							style="color: red"><?php echo form_error('sku')?>
						</b></td>
					</tr>
				</table>

				<div class="buttons">
					<input type="submit" value="送出" />
					<button type="button"
						onclick="javascript:location.href='<?php echo getAdminURL('product/sku_group_list')?>'">返回</button>
				</div>

				</form>
			</div>

		</div>

	</div>

	<link
		href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css');?>"
		rel="stylesheet">
	<link
		href="<?php echo base_url('application/views/backend/css/theme.css');?>"
		rel="stylesheet">
	<link
		href="<?php echo base_url('application/views/backend/css/jquery.cleditor.css');?>"
		rel="stylesheet">

	<script
		src="<?php echo base_url('application/views/backend/js/jquery-ui-1.11.1.min.js');?>"></script>
	<script
		src="<?php echo base_url('application/views/backend/js/jquery.cleditor.js');?>"></script>
	<script
		src="<?php echo base_url('application/views/backend/js/jquery.cleditor.extimage.js');?>"></script>

	<script type="text/javascript">
/***********************************
/* 名稱 : readURL
/* 功能 : 新圖片上傳
************************************/
function readURL(input) {	

	if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result)
                .width(200)
                .height(200);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
</script>

</body>
</html>
