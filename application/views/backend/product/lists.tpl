<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'product' ,getAdminMenu())?></h2></div>

		<div id="content">

			<span style="float: right">
				<div class="actions">
					<a href="javascript:location.href='<?php echo getAdminURL('product/product_add_step1')?>';" class="btn btn-default btn-sm"> 
					<i class="fa fa-pencil"></i> 新增</a>
				</div> 
		    </span>

			<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">
			
			<?php if($this->session->flashdata('msg')){ ?>				 
				  <div class="alert alert-success"><i class="fa fa-check-circle"></i><?php echo $this->session->flashdata('msg')?></div>
			<?php }elseif($this->session->flashdata('msg_err')){ ?>
				  <div class="alert alert-danger"><i class="fa fa-times-circle"></i> <?php echo $this->session->flashdata('msg_err')?></div>
			<?php } ?>			

			<!-- (一)搜尋主體 -->
			<?php echo form_open(current_url() ,array('method'=>'get'))?>
				<fieldset>
					<legend>搜尋條件:</legend>
					
					<input style="display:none" type="text" name="page" value="1"> 
					
					選擇分類: 
					<?php $srh_category_id = $this->input->get('srh_category_id');?>
					<select name="srh_category_id">
					
					  <?php if(isset($srh_category_id ) && is_numeric($srh_category_id) ){ ?>
                         <option value="<?php echo $srh_category_id?>"><?php echo get_assign_field('category', 'category_id', $srh_category_id, 'name')?></option>
                      <?php }else{ ?>
					     <option value="*">不拘</option>
				 	  <?php } ?>
				 	  
				 	  <?php foreach ( $this->cls_category->get_1parent() as $row) { ?>
					     <optgroup label="<?php echo $row['name']?>">
					  <?php foreach ( $this->cls_category->get_2parent($row['category_id']) as $row2) { ?>
 				         <option value="<?php echo $row2['category_id']?>"><?php echo $row2['name']?></option>
					  <?php } ?>
					     </optgroup>
                      <?php } ?>
                      
                      <?php if( isset($srh_category_id ) && is_numeric($srh_category_id) ){ ?>
                         <option value="*">不拘</option>
				 	  <?php } ?>	
				 	  
					</select>
					 
					<!--  
					商品編號 : 
					  <input type="text" name="srh_product_id" value="<?php //echo $this->input->get('srh_product_id')?>"> 
					 -->
					sku(%) :
					  <input type="text" name="srh_sku" value="<?php echo $this->input->get('srh_sku')?>"> 
					 
					商品名稱(%) :
					  <input type="text" name="srh_product_name" value="<?php echo $this->input->get('srh_product_name')?>"> 
					
					<p>
					
					<!-- 
					廣告分類: 
					<select name="srh_ad_id">
					  <?php //$srh_ad_id = $this->input->get('srh_ad_id');
					        //foreach ( reSortArrayMutil(3,$srh_ad_id ,getAdProductCategory()) as $key=>$val){ ?>
                        <option value="<?php //echo $key?>"><?php //echo $val?></option> 
                      <?php //} ?>
					</select>
					 -->
					
					上架期間: 
					  <input type="text" name="srh_cdate1" class="dio_date" value="<?php echo $this->input->get('srh_cdate1')?>">
				 	 ~<input type="text" name="srh_cdate2" class="dio_date" value="<?php echo $this->input->get('srh_cdate2')?>">

				 	狀態: 
					<select name="srh_status" style="width: 10%">
						<?php foreach ($slt_status as $key=>$val) { ?>
						   <option value="<?php echo $key?>"><?php echo $val?></option>
						<?php } ?>
					</select> 
				 	 
					<p>	
					
					庫存(以下): 
					<input type="text" name="srh_stock" value="<?php echo $this->input->get('srh_stock')?>" onkeypress="def_numeric()"> 
										
					排序 : 
					<select name="srh_sort">
					<?php
					foreach (reSortArray($this->input->get('srh_sort') ,$slt_sort_by) as $key=>$val) { ?>
						<option value="<?php echo $key?>">
						<?php echo $val?>
						</option>
						<?php } ?>
					</select> 
					
					<input type="radio" name="srh_sort_type" value="DESC" <?php echo $this->input->get('srh_sort_type') == 'DESC' ? " checked" : "" ?> />降冪
					<input type="radio" name="srh_sort_type" value="ASC" <?php echo $this->input->get('srh_sort_type') == 'ASC' ? " checked" : "" ?> />升冪			
					
					<input type="submit" value="查詢">
				</fieldset>
				</form>

				<p>
				
			    <!-- 快捷功能 
			   <?php //echo form_open(getCurrentFullUrl()  ,array('method'=>'get'))?>
				  <fieldset>
					<legend>快捷功能:</legend>
					<input type="radio" name="btn_enabled" value="1" checked />啟用
					<input type="radio" name="btn_enabled" value="0" />停用			
					
					<input type="button" class="btn btn-info" onclick="multi_comb()" value="組合" />
				  </fieldset>
				</form>	

				<p> -->
				
				<b style="color: red"><?php echo form_error('srh_cdate1')?></b> 
				<b style="color: red"><?php echo form_error('srh_cdate2')?></b>

				<!-- (二)列表主體 -->
				<div class="clearfix">
					<table class="dio_datatable">

						<thead>
							<tr>
								<!-- <td>分類</td> -->
								<td><input type="checkbox" id="selectAll" /></td>
								<td>圖示</td>
								<!-- <td>編號</td> -->
								<td>sku</td>
								<td>商品名稱</td>
								<td>運費類型</td>
								<td>價格(台幣)</td>
								<td>庫存量</td>
								<td>顯示在首頁</td>
								<!-- <td>可否組合</td> -->
								<td>排序</td>
								<td>商品狀態</td>
								<td>建立日期</td>
								<td>功能</td>
							</tr>
						</thead>

						<tbody>
						<?php foreach ($query as $row) { ?>
							<tr>
								<!-- <td><?php //echo $this->cls_category->get_name($row['path'])?></td> --> 
								<!-- <td><img width="50px" height="50px" src="<?php //echo base_url('resources/uploads/'.$row['image'])?>"></td> -->
								<td><?php if($row['status'] == 1){ ?>
							          <input type="checkbox" />
								    <?php } ?>
								      <input style="display: none" type="text" value="<?php echo $row['product_id']?>" />
								</td>
								<td><img width="50px" height="50px" src="<?php echo base_url('resources/uploads/'.$row['image'])?>"></td>
								<!-- <td><?php //echo $row['product_id']?></td>  -->
								<td><?php echo $row['sku']?></td>
								<td><?php echo mb_substr($row['name'],0,20,"utf-8")?></td>
								<td><?php echo reSortArrayMutil(2,$row['fee_category_id'] ,getFeeCategory()) ?></td>
								<td><?php echo DIO_CURRENCY.addCommas($row['price'])?></td>
								<td><?php echo ($row['stock'] <= 0) ? "<b style='color:red'>".$row['stock']."</b>" : $row['stock']?></td>
								<td><?php echo ($row['is_show_home'] == '1') ? "是" : "否"?></td>
								<!-- 
								<td><?php //if($row['is_comb'] == '1'){ ?>
								      <a href="<?php //echo getAdminURL('product_comb/lists?product_id='.$row['product_id'])?>" class="btn btn-success">是</a>
								    <?php //}else{ ?>
								      <b>否</b> 
								    <?php //} ?>
							    </td>  -->
								<td class="center"><?php echo $row['sort_order']?></td>
								<td><?php echo ($row['status'] == '1') ? "上架" : "<b style='color:red'>下架</b>"?></td>
								<td><?php echo substr($row['cdate'], 0,10) ?><b style="display: none"><?php echo substr($row['cdate'], 11) ?></td>
								<td><a class="btn btn-success" href="<?php echo getAdminURL('product/edit?product_id='.$row['product_id'])?>">
										<i class="fa fa-pencil-square-o"></i> 編輯</a> 
									<a class="btn btn-danger" href="javascript:my_confirm(<?php echo $row['product_id']?>);">
										<i class="fa fa-trash"></i> 刪除</a>
								</td>
							</tr>
							<?php } ?>

						</tbody>
					</table>

					<div class="pageNum">
						<span>顯示第<?php echo $page_startEnd['page_start']?> 至 <?php echo $page_startEnd['page_end']?>
							項結果，共 <?php echo $query_group_by[0]['count']?> 項 </span> <span
							style="float: right;"><?php echo $pages->display_pages()?>
						</span>
					</div>

				</div>
			</div>

		</div>
	</div>

	<link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>" rel="stylesheet">
	<script src="<?php echo base_url('application/views/backend/js/jquery-ui-1.11.1.min.js')?>"></script>
	<script src="<?php echo base_url('application/views//backend/js/dio_plugins/jquery_fields_date.js')?>"></script>

<script type="text/javascript">
//-------------------------------------------------------------
//
// 全選&全不選
//
//-------------------------------------------------------------
$('#selectAll').click(function (e) {
    $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
});


//-------------------------------------------------------------
//
// 多筆[可否組合] 
//
//-------------------------------------------------------------
function multi_comb(){
	
   var aryList = new Array();
   var enabled = $('input[name=btn_enabled]:checked').val() ;
  
   $('table tbody').find('input[type="checkbox"]:checked').each(function (i) {

	   var product_id = $(this).next().val();
	   
	   aryList.push(product_id);
   });

   if(aryList.length <= 0){
       alert('您尚未勾選項目!');
   }else{
    //var chk_box=confirm('確定改變訂單狀態為已出貨?');
	  //if (chk_box == true) {
	  var url     = '?enabled=' +　enabled + '&aryList=' + aryList;
	  location.href = '<?php echo getAdminURL("product/multi_comb")?>' + url;
	  //}	      
   }
      
}

function my_confirm(product_id){
       var chk_box=confirm('確定刪除資料?');
       if (chk_box == true) {
             location.href = '<?php echo base_url("index.php/backend/product/del?product_id=") ?>'+product_id;
       }
    }
</script>

</body>
</html>
