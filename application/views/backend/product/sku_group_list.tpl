<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'product' ,getAdminMenu())?></h2></div>

		<div id="content">
		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">

				<!-- (一)搜尋主體 -->
				<fieldset>
					<legend>搜尋條件:</legend>
					<button type="button" class="btn btn-primary"
						onclick="javascript:location.href='<?php echo getAdminURL('product/sku_group_insert')?>'">新增</button>
				</fieldset>

				<p></p>

				<!-- (二)列表主體 -->
				<div class="clearfix">
					<table class="dio_datatable">
						<thead>
							<tr>
								<td>分類</td>
								<td>sku群組編號</td>
								<td>狀態</td>
								<td>建檔日期</td>
								<td>功能</td>
							</tr>
						</thead>

						<tbody>
						<?php foreach ($query as $row){ ?>
							<tr>
								<td><?php $path = get_assign_field('category', 'category_id', $row['category_id'], 'path');
								echo $this->cls_category->get_name($path)?></td>
								<td class="center"><?php echo $row['sku']?>
								</td>
								<td class="center"><?php echo ($row['status'] == 1) ? "啟用" : "<b style='color:red'>停用</b>" ; ?>
								</td>
								<td class="center"><?php echo substr($row['cdate'], 0,10) ?><b style="display: none"><?php echo substr($row['cdate'], 11) ?></td>
								<td class="center"><a class="btn btn-info"
									href="<?php echo getAdminURL('product/sku_group_view?sku='.$row['sku'])?>">
										<i class="fa fa-pencil-square-o"></i> 檢視產品</a> <a
									class="btn btn-info"
									href="<?php echo getAdminURL('product/sku_group_update?sku='.$row['sku'])?>">
										<i class="fa fa-pencil-square-o"></i> 編輯</a> <a
									class="btn btn-danger"
									href="javascript:my_confirm(<?php echo $row['sku']?>);">
										<i class="fa fa-trash"></i> 刪除</a>
								</td>
							</tr>
							<?php } ?>
						</tbody>

					</table>
				</div>

			</div>

		</div>

	</div>

	<script type="text/javascript">
    $(document).ready(function() {
        $('.dio_datatable').dataTable({
          	 "language": { 
   		         url : "<?php echo base_url("application/views/backend/js/dataTables/i18n/chinese_traditional.lang.json")?>"
   		     },
   		     "order": [[ 3, "desc" ]]
         });
    });
   
    function my_confirm(sku){
       var chk_box=confirm('確定刪除資料?');
       if (chk_box == true) {
             location.href = '<?php echo base_url("index.php/backend/product/sku_group_delete?sku=") ?>'+sku;
       }
    }
</script>

</body>
</html>

