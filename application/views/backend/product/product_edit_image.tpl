<body>

	<style>
input[name="product_id"] {
	background-color: rgb(228, 228, 225);
}

.fa-star {
	color: red
}

th {
	padding: 20px
}

select {
	width: auto
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'product' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
				<table>

				<?php echo form_open("backend/product/product_edit_image")?>

					<tr>
						<th colspan="2">屬性圖片編輯</th>
					</tr>

					<tr style="display: none">
						<th>商品屬性編號 :</th>
						<td><input type="text" name="product_speci_id"
							value="<?php echo $query['product_speci_id']?>" readonly />
						</td>
					</tr>

					<tr>
						<th>sku :</th>
						<td><input type="text" name="sku"
							value="<?php echo $query['sku']?>" readonly />
						</td>
					</tr>

					<tr>
						<th><i class="fa fa-star"></i>圖片<br>(640x640)</th>
						<td><input style="display: none" class="input-medium focused"
							id="specification_image" name="image"
							value="<?php echo set_value('image')?>"> <img id="image"
							width="300px" height="250px"
							src="<?php echo base_url("resources/uploads").'/'.set_value('image')?>"
							alt="圖片" /> <a onclick="base_image_upload('image');">選擇圖片</a>&nbsp;&nbsp;|&nbsp;&nbsp;
							<a onclick="base_image_delete('image');">刪除圖片</a></td>
					</tr>

					<tr>
						<th>圖片說明 :</th>
						<td><input type="text" name="image_alt" />
						</td>
					</tr>

					<tr>
						<th></th>
						<td>
							<button type="button" onclick="add_specification_img()">新增圖片</button>
							<!-- <input type="submit" value="新增圖片" />  -->
							<button type="button" onclick="javascript:window.close();">離開</button>
						</td>
					</tr>

					</form>

					<tr>
						<th width="20%">材質編輯</th>
						<td>
							<table class="dio_datatable" style="width: 60%">
								<thead>
									<tr>
										<th>圖片</th>
										<th>圖片說明</th>
										<!-- <th>狀態</th> -->
										<th>功能</th>
									</tr>
								</thead>

								<tbody>
								<?php foreach ($query_image as $row) { ?>
									<tr>
										<td align="center"><img style="width: 180px; height: 150px"
											src="<?php echo base_url('resources/uploads/'.$row['image'])?>">
										</td>
										<td align="center"><?php echo $row['image_alt']?>
										</td>
										<!-- <td align="center"><?php //echo $row['status']?></td> -->
										<td align="center">
											<button type="button" class="btn btn-danger"
												onclick="javascript:del_specification_img(<?php echo $row['product_speci_image_id']?>);">刪除</button>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table></td>
					</tr>

				</table>

			</div>

		</div>

	</div>

	<link
		href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>"
		rel="stylesheet">
	<link
		href="<?php echo base_url('application/views/backend/css/elfinder.min.css')?>"
		rel="stylesheet">
	<link
		href="<?php echo base_url('application/views/backend/css/theme.css')?>"
		rel="stylesheet">

	<script
		src="<?php echo base_url('application/views/backend/js/jquery-1.7.2.min.js')?>"></script>
	<script
		src="<?php echo base_url('application/views/backend/js/jquery-ui-1.8.21.custom.min.js');?>"></script>
	<script
		src="<?php echo base_url('application/views/backend/js/elfinder.min.js')?>"></script>
	<script
		src="<?php echo base_url('application/views/backend/js/i18n/elfinder.zh_CN.js')?>"></script>
	<script
		src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_image_upload.js')?>"></script>
	<script
		src="<?php echo base_url('application/views/backend/js/jquery.blockUI.min.js')?>"></script>

	<script type="text/javascript">
/***********************************
/* 名稱 : add_specification_img
/* 功能 : 商品屬性 / 新增 
************************************/
function add_specification_img(){

	 //參數設定
	 var url = '<?php echo getAdminURL('product/product_edit_image_add')?>';

     $.post(url ,{data: $('form').serialize()} ,function(err_msg){ 
         //參數驗證
        if(!is_string_empty(err_msg)){
      	  $.blockUI({ 
  	   	         message:"<em style='font-size:25px'>"+err_msg+"</em>" , 
  	   	         baseZ: 2000,
    	   	     css: { 
  	                    border: 'none', 
  	                    padding: '20px', 
  	                    backgroundColor: '#000', 
  	                                     '-webkit-border-radius': '10px', 
  	                                     '-moz-border-radius': '10px', 
  	                    opacity: .5, 
  	                    color: '#FFFFFF',
  	                    height: '10%' 
  	                  } 
  	   	      });

      	  setTimeout($.unblockUI, 1500);  
      	  
          return false;
        }else{
            $.blockUI({ 
   	   	         message:"<em style='font-size:25px'>處理中 ,請稍候...</em>" , 
   	   	         baseZ: 2000,
     	   	     css: { 
   	                    border: 'none', 
   	                    padding: '20px', 
   	                    backgroundColor: '#000', 
   	                                     '-webkit-border-radius': '10px', 
   	                                     '-moz-border-radius': '10px', 
   	                    opacity: .5, 
   	                    color: '#FFFFFF',
   	                    height: '10%' 
   	                  } 
   	   	      });
 	   	      
            location.reload();
        }    
    }); 
        
  }


/***********************************
/* 名稱 : del_specification_img
/* 功能 : 商品屬性 / 刪除 
************************************/
 function del_specification_img(product_speci_image_id){
   var chk_box=confirm('確定刪除資料?');
   if (chk_box == true) {

	   //參數設定
	   var url = '<?php echo base_url("index.php/backend/product/product_edit_image_del?product_speci_image_id=") ?>'+product_speci_image_id;
	   
	   //post資料
	   $.post(url ,function(){
           $.blockUI({ 
 	   	       message:"<em style='font-size:25px'>處理中 ,請稍候...</em>" , 
 	   	       baseZ: 2000,
   	   	       css: { 
 	                    border: 'none', 
 	                    padding: '20px', 
 	                    backgroundColor: '#000', 
 	                                     '-webkit-border-radius': '10px', 
 	                                     '-moz-border-radius': '10px', 
 	                    opacity: .5, 
 	                    color: '#FFFFFF',
 	                    height: '10%' 
 	                  } 
 	   	    });
	   	      
            location.reload();
	   });
   }
 }

</script>

</body>
</html>
