<body>

	<div id="wrapper">

	<?php $this->load->view("backend/common/navbar_logo.tpl"); ?>

		<div id="page_wrap">
			<div class="page_wrap--member_area"></div>
		</div>

		<?php $this->load->view("backend/common/navbar_side.tpl"); ?>

		<div class="page_wrap--member_area_content">

			<div class="panel panel-default">
				<div class="panel-heading" style="background-color: #066;">
					<span style="color: #fff">訂單異動失敗</span>
				</div>

				<div class="panel-body">
					<p>
						抱歉! 中信編號 <b style="color: blue"><?php echo $this->input->get('order_detail_id')?>
						</b> 的訂單狀態異動失敗!
					</p>
					<p>如果是[已取消]狀態的設定，請確定該筆訂單的訂購日期是否已過時。</p>
					<p>(每日晚上 20:00 以前才可進行取消當日的訂單，超過此時間，請改由人工作業，謝謝您)</p>
					<p>
						<a
							href="<?php echo getAdminURL('product/order_list')?>">[回
							待處理訂單 ]</a>
					</p>
				</div>

			</div>

		</div>


	</div>

	<?php //$this->load->view("backend/common/footer.tpl"); ?>
</body>
</html>

