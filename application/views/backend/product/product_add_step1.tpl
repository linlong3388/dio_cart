<body>

<style>
.img-responsive {
	width: 200px;
	height: 200px;
	border: 1px solid #999;
}

.page_wrap--data_area--main_buyer--table--ul li {
	width: 200px;
	height: 200px;
}

.page_wrap--data_area--main_buyer--table--ul li input[type="file"] {
	margin-top: 20px;
}

.fa-star {
	color: red
}

td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'product' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			
			 <b style="color: red"><?php echo validation_errors(); ?></b>
			
			<?php echo form_open( getCurrentFullUrl() ,array( 'enctype' => 'multipart/form-data'))?>
				<table>
					<tr>
						<th colspan="2">新增 Step1 / 基本資料</th>
					</tr>

					<tr>
						<th width="20%"><i class="fa fa-star"></i>編號(sku)</th>
						<td><input placeholder="請輸入資料" type="text" name="sku" value="<?php echo set_value('sku')?>"></td>
					</tr>
					
					<tr>
						<th><i class="fa fa-star"></i>商品分類</th>
						<td><?php foreach ( $this->cls_category->get_1parent() as $row) { ?>
							         <b><?php echo $row['name']?></b><br />  
							<?php foreach ( transKeyPairArray($this->cls_category->get_2parent($row['category_id']) ,'category_id' ,'name') as $key=>$val) {  ?>
						             <input type="checkbox" name="category_multi_id[]" value="<?php echo $key?>" <?php echo set_checkbox("category_multi_id[]", $key)?> /> <?php echo $val?>
							<?php  } ?>
						            <hr/>
						    <?php } ?>
						 </td>
					</tr>	
					
					<tr>
						<th><i class="fa fa-star"></i>運費類型</th>
						<td><select name="fee_category_id">
					          <?php foreach ( getFeeCategory() as $key=>$val) { ?>
 							     <option value="<?php echo $key?>"><?php echo $val?></option>
							  <?php } ?>
							</select> 
						</td>
					</tr>
					
					<!-- 
					<tr>
						<th width="20%"><i class="fa fa-star"></i>國際條碼</th>
						<td><input placeholder="請輸入資料" type="text" name="barcode" value="<?php //echo set_value('barcode')?>" onkeypress="def_numeric()" maxlength="13"></td>
					</tr>
					 -->
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>商品名稱</th>
						<td><input placeholder="請輸入資料" type="text" name="name" value="<?php echo set_value('name')?>"></td>
					</tr>
					
					<tr>
						<th width="20%">建議售價</th>
						<td><input placeholder="請輸入資料" type="text" name="price_original" value="<?php echo set_value('price_original')?>" onkeypress="def_numeric()" maxlength="12">元
						</td>
					</tr>
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>活動售價</th>
						<td><input placeholder="請輸入資料" type="text" name="price" value="<?php echo set_value('price')?>" onkeypress="def_numeric()" maxlength="12">元
						</td>
					</tr>

					<tr>
						<th width="20%"><i class="fa fa-star"></i>庫存</th>
						<td><input placeholder="請輸入資料" type="text" name="stock" value="<?php echo set_value('stock')?>" onkeypress="def_numeric()" maxlength="12">
						</td>
					</tr>					
					
				   <tr>
					   <th width="20%">圖片<br>(640x640)</th>
					   <td>
					       <input style="display: none" class="input-medium focused" name="image" value="<?php echo isset($query['image']) ? $query['image'] : set_value('image') ;?>">
     	                   <img id="image" class="img-responsive" src="<?php echo (!empty($query['image'])) ? base_url("resources/uploads/".$query['image']) : base_url("resources/uploads").'/'.set_value('image')?>" alt="圖片"/>
                              <p>                                                       
                                 <a onclick="base_image_upload('image');">選擇圖片</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                 <a onclick="base_image_delete('image');">刪除圖片</a>
                              </p>
		               </td>
				   </tr>
				   
				   <!-- 
				   <tr>
						<th width="20%">關鍵字</th>
						<td><input placeholder="請輸入資料" type="text" name="keyword" value="<?php //echo set_value('keyword')?>" />
						</td>
					</tr>	
				    -->
				
				    <!-- 
					<tr>
						<th width="20%">特色簡述</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="brief" value="<?php //echo set_value('brief')?>">
						</td>
					</tr>
					 -->

				    <!-- 
					<tr>
						<th width="20%">促銷訊息</th>
						<td><input placeholder="請輸入資料" type="text" name="promo_msg"
							value="<?php //echo set_value('promo_msg')?>">
						</td>
					</tr>
					 -->

				   	<tr>
						<th width="20%">簡介</th>
						<td><textarea class="fullwidth dio_textarea" rows="3" name="techenique"><?php echo isset($query['techenique']) ? $query['techenique'] : set_value('techenique')?></textarea>
						</td>
					</tr>
				   
				    <!-- 
				    <tr>
						<th width="20%">說明</th>
						<td><textarea class="fullwidth" rows="3" name="specification"><?php //echo isset($query['specification']) ? $query['specification'] : set_value('specification')?></textarea>
						</td>
					</tr>
				   
				    <tr>
						<th width="20%">備註</th>
						<td><textarea class="fullwidth" rows="3" name="notice"><?php //echo isset($query['notice']) ? $query['notice'] : set_value('notice')?></textarea>
						</td>
					</tr>
				     -->
				   
					<tr>
						<th width="20%">詳細資訊</th>
						<td><textarea class="fullwidth dio_textarea" rows="3" name="description"><?php echo isset($query['description']) ? $query['description'] : set_value('description')?></textarea>
						</td>
					</tr>
                    

					<!-- 
					<tr>
						<th width="20%">技術</th>
						<td><textarea class="fullwidth dio_textarea" rows="3" name="techenique">
						<?php //echo isset($query['techenique']) ? $query['techenique'] : set_value('techenique')?>
							</textarea></td>
					</tr>
					 -->

					<tr>
						<th colspan="2">Meta頁籤</th>
					</tr>
					
					<tr>
						<th width="20%">Meta標籤標題</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="meta_title" value="<?php echo isset($query['meta_title']) ? $query['meta_title'] : set_value('meta_title')?>">
						</td>
					</tr>
					
					<tr>
						<th width="20%">Meta標籤描述</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="meta_description" value="<?php echo isset($query['meta_description']) ? $query['meta_description'] : set_value('meta_description')?>">
						</td>
					</tr>
					
					<tr>
						<th width="20%">Meta標籤關鍵字</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="meta_keyword" value="<?php echo isset($query['meta_keyword']) ? $query['meta_keyword'] : set_value('meta_keyword')?>">
						</td>
					</tr>	
						
				</table>

				<div class="buttons">
					<!-- <input type="submit" value="下一步" />  -->
					<input type="submit" value="送出" />
					<button type="button"
						onclick="javascript:location.href='<?php echo getAdminURL('product/lists?page=1')?>'">返回</button>
				</div>

				</form>
			</div>

		</div>

	</div>
	
    <link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('application/views/backend/css/elfinder.min.css')?>" rel="stylesheet">
    
    <script src="<?php echo base_url('application/views/backend/js/jquery-1.7.2.min.js');?>"></script>
    <script src="<?php echo base_url('application/views/backend/js/jquery-ui-1.8.21.custom.min.js');?>"></script>
    <script src="<?php echo base_url('application/views/backend/js/elfinder.min.js')?>"></script>
    <script src="<?php echo base_url('application/views/backend/js/i18n/elfinder.zh_CN.js')?>"></script>
	<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_image_upload.js')?>"></script>
	<script src="<?php echo base_url('application/views/backend/js/tinymce/tinymce.min.js')?>"></script>
	<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_tinymce_elfinder.js')?>"></script>

</body>
</html>
