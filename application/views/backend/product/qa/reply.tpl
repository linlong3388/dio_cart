<body>

<style>
.fa-star {
	color: red
}
td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'product_qa' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">

			<?php echo form_open( current_url().'?'.$_SERVER["QUERY_STRING"])?>

				<table>
					<tr>
						<th colspan="2">問題與解答 / 回覆 (<i class="fa fa-star"></i> 為必填欄位)</th>
					</tr>

					<tr>
						<th>問題編號</th>
						<td><input type="text" name="product_question_id"
							value="<?php echo isset($query_question['product_question_id']) ? $query_question['product_question_id'] : set_value('product_question_id')?>"
							readonly /> <input style="display: none" type="text"
							name="customer_id"
							value="<?php echo isset($query_question['customer_id']) ? $query_question['customer_id'] : set_value('customer_id')?>">
						</td>
					</tr>

					<tr>
						<th>提問內容</th>
						<td>
						    <textarea rows="10" class="fullwidth" name="question" style="background-color: rgb(228, 228, 225);"><?php echo isset($query_question['question']) ?  $query_question['question'] : set_value('question')?></textarea>
						</td>
					</tr>

					<tr>
						<th><i class="fa fa-star"></i>回覆</th>
						<td>
						   <textarea rows="10" class="fullwidth" name="re_question"><?php echo isset($query_question['re_question']) ?  $query_question['re_question'] : set_value('re_question')?></textarea> 
						   <b style="color: red"><?php echo form_error('re_question')?></b> 
						   <br /><b style="color: red">(*限500個中文字 )</b>
						</td>
					</tr>

				</table>
				<div class="buttons">
					<input class="center_btn" type="submit" value="送出">
					<button type="button" onclick="javascript:location.href='<?php echo getAdminURL('product_qa/lists')?>'">返回</button>
				</div>
				</form>

			</div>

		</div>

	</div>

</body>
</html>
