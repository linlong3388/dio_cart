<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'product_qa' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">
		
				<p>

				<!-- (二)列表主體 -->
				<div class="clearfix">
					<table class="dio_datatable">
						<thead>
							<tr>
								<td>編號</td>
								<td>商品名稱</td>
								<td>問題</td>
								<td>狀態</td>
								<td>建立日期</td>
								<td>功能</td>
							</tr>
						</thead>

						<tbody>
						<?php foreach ($query as $row) { ?>
							<tr>
								<td><?php echo $row['product_question_id'] ?>
								</td>
								<td><a href="<?php echo getAdminURL('product/edit?product_id='.$row['product_id'])?>"
									target="_blank"><?php echo get_assign_field('product', 'product_id', $row['product_id'] ,'name')?>
								</a></td>
								<td><?php echo mb_substr(strip_tags($row['question']) , 0 ,20 ,"UTF-8")?>...</td>
								<td><?php echo ($row['status'] == '1') ?  "<b style='color:blue'>未回覆</b>" : "已回覆" ?>
								</td>
								<td class="small"><?php echo substr($row['cdate'], 0,10) ?><b style="display: none"><?php echo substr($row['cdate'], 11) ?></td>
								<td><a class="btn"
									href="<?php echo getAdminURL('product_qa/reply?product_question_id='.$row['product_question_id'])?>"><i
										class="fa fa-comment"></i> 回覆</a></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>

				</div>

			</div>

		</div>

	</div>

<script>
$(function() {
    $('.dio_datatable').dataTable({
      	 "language": { 
		         url : "<?php echo base_url("application/views/backend/js/dataTables/i18n/chinese_traditional.lang.json")?>" 
         },
         "order": [[ 4, "desc" ]]  
     });
});
</script>

</body>
</html>
