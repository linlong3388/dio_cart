<body>

	<style>
.fa-star {
	color: red
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'product' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			<?php echo form_open("backend/product/sku_group_update" ,array( 'enctype' => 'multipart/form-data'))?>
				<table>
					<tr>
						<th colspan="2">編輯資料</th>
					</tr>

					<tr>
						<th><i class="fa fa-star"></i>商品類別</th>
						<td><?php if( isset($query['category_id']) ){
							$category_path = $this->cls_category->get_name( get_assign_field('category', 'category_id', $query['category_id'], 'path'));
						}else{
							$category_path = set_value('category_id');
						}
						?> <select name="category_id">
								<option value="<?php echo $query['category_id']?>">
								<?php echo $category_path ?>
								</option>
								<?php foreach ($this->cls_category->get_all() as $row) {
									if($row['category_id'] != $query['category_id']){
						   	?>
								<option value="<?php echo $row['category_id']?>">
								<?php echo $this->cls_category->get_name( get_assign_field('category', 'category_id', $row['category_id'], 'path'))?>
								</option>
								<?php   }
								}
								?>
						</select> <input type="text" name="product_id"
							style="display: none"
							value="<?php echo isset($query['product_id']) ? $query['product_id'] : set_value('product_id') ?>"
							readonly /> <input type="text" name="vendor_id"
							style="display: none"
							value="<?php echo isset($query['vendor_id']) ? $query['vendor_id'] : set_value('vendor_id') ?>"
							readonly /></td>
					</tr>

					<tr>
						<th width="20%"><i class="fa fa-star"></i>sku群組代號</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text"
							name="sku"
							value="<?php echo isset($query['sku']) ? $query['sku'] : set_value('sku')?>">
							<b style="color: red"><?php echo form_error('sku')?> </b>
						</td>
					</tr>

				</table>

				<div class="buttons">
					<input style="display: none" type="text" name="sku_group_id"
						value="<?php echo isset($query['sku_group_id']) ? $query['sku_group_id'] : set_value('sku_group_id')?>">
					<input type="submit" value="送出" />
					<button type="button"
						onclick="javascript:location.href='<?php echo getAdminURL('product/sku_group_list')?>'">返回</button>
				</div>

				</form>
			</div>

		</div>

	</div>


	<link
		href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css');?>"
		rel="stylesheet">
	<link
		href="<?php echo base_url('application/views/backend/css/theme.css');?>"
		rel="stylesheet">
	<link
		href="<?php echo base_url('application/views/backend/css/jquery.cleditor.css');?>"
		rel="stylesheet">

	<script
		src="<?php echo base_url('application/views/backend/js/jquery-ui-1.11.1.min.js');?>"></script>
	<script
		src="<?php echo base_url('application/views/backend/js/jquery.cleditor.js');?>"></script>
	<script
		src="<?php echo base_url('application/views/backend/js/jquery.cleditor.extimage.js');?>"></script>


	<script type="text/javascript">
/***********************************
/* 名稱 : readURL
/* 功能 : 新圖片上傳
************************************/
function readURL(input) {	

	if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result)
                .width(200)
                .height(200);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
</script>

</body>
</html>
