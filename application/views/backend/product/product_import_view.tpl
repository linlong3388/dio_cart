<body>

<style>
.fa-star {
	color: red
}
td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl'); ?>

	<div id="title"><h2><?php echo reSortArrayMutil(2,'product' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl'); ?>

			<div class="container">

			<?php echo form_open("backend/product/product_import_csv" ,array( 'enctype' => 'multipart/form-data'))?>

				<table border="1" class="page_wrap--data_area--main_buyer--table">
					<tr>
						<th colspan="2">批次上傳</th>
					</tr>

					<tr>
						<th><i class="fa fa-star"></i>CSV檔案匯入</th>
						<td><input type="file" name="csv" /><b style="color: red">*一次上傳筆數不得超過500筆</b>
						</td>
					</tr>

					<tr>
						<th></th>
						<td><input class="btn btn-primary" type="submit" value="確定" /> 
						    <!-- 
						     [ <a href="<?php //echo base_url("resources/product_format.xlsx")?>">說明範例</a>
						     | <a href="<?php //echo base_url("resources/product_format.csv")?>">表格下載</a> ]
						     -->
						 </td>
					</tr>
				</table>

				</form>

			</div>

		</div>

		<?php $this->load->view('backend/common/footer.tpl'); ?>

	</div>

</body>
</html>
