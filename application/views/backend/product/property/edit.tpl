<body>

<style>
.fa-star {
	color: red
}
td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'product_property' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			
			<?php if($this->session->flashdata('msg')){ ?>				 
		       <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	        <?php } ?>		
	        
	        <b style="color: red"><?php echo validation_errors(); ?></b>			
			
			<?php echo form_open( getCurrentFullUrl() )?>
				<table>
					<tr>
						<th colspan="2">編輯資料</th>
					</tr>

					<tr style="display: none">
						<th width="20%">編號</th>
						<td><input type="text" name="property_id"
							value="<?php echo isset($query['property_id']) ? $query['property_id'] : set_value('property_id')?>"
							readonly></td>
					</tr>
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>屬性分類</th>
						<td>
					        <select class="form-control" name="property_category_id">
					        <?php foreach ( reSortArrayMutil(3,$query['property_category_id'],getPropertyCategory()) as $key=>$val) { ?>
					   	        <option value="<?php echo $key?>"><?php echo $val?></option>
					        <?php } ?>
					        </select>
					</tr>
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>名稱</th>
						<td><i class="fa fa-star"></i>(中)<input style="width: 60%;"  placeholder="請輸入資料" type="text" name="name" value="<?php echo isset($query['name']) ? $query['name'] : set_value('name')?>">
						    <p>
						    (英)<input style="width: 60%;"  placeholder="請輸入資料" type="text" name="name_en" value="<?php echo isset($query['name_en']) ? $query['name_en'] : set_value('name_en')?>">
						    <p>
						    (日)<input style="width: 60%;"  placeholder="請輸入資料" type="text" name="name_jp" value="<?php echo isset($query['name_jp']) ? $query['name_jp'] : set_value('name_jp')?>">
						</td>
					</tr>

					<!-- 
				   <tr>
					   <th width="20%"><i class="fa fa-star"></i>排序</th>
					   <td><input class="fullwidth" placeholder="請輸入資料" type="text" name="sort_order" 
					        value="<?php echo isset($query['sort_order']) ? $query['sort_order'] : set_value('sort_order')?>">
					   </td>
				   </tr>
				    -->

					<tr>
						<th width="20%"><i class="fa fa-star"></i>商品狀態</th>
						<td><input type="radio" name="status" value="1"
						<?php if( isset($query['status']) && ($query['status'] == '1') ) echo set_radio('status', '1' ,true);
						else echo set_radio('status', '1'); ?>>啟用 <input type="radio"
							name="status" value="0"
							<?php if( isset($query['status']) && ($query['status'] == '0') ) echo set_radio('status', '0' ,true);
							else echo set_radio('status', '0'); ?>>停用</td>
					</tr>

					<tr>
						<th width="20%">建立日期</th>
						<td><input class="fullwidth" type="text" name="cdate"
							value="<?php echo isset($query['cdate']) ? $query['cdate'] : set_value('cdate')?>"
							readonly> 
						</td>
					</tr>

				</table>

				<div class="buttons">
					<input type="submit" value="送出" />
					<button type="button"
						onclick="javascript:location.href='<?php echo getAdminURL('product_property/lists')?>'">返回</button>
				</div>

				</form>
			</div>

		</div>

	</div>

</body>
</html>
