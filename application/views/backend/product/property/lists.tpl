<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'product_property' ,getAdminMenu())?></h2></div>

		<div id="content">

			<span style="float: right">
				<div class="actions">
					<a href="javascript:location.href='<?php echo getAdminURL('product_property/add')?>'"
						class="btn btn-default btn-sm"> <i class="fa fa-pencil"></i> 新增</a>
				</div> </span>

				<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">
			
			  <?php if($this->session->flashdata('msg')){ ?>				 
		        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	         <?php } ?> 	

				<!-- (一)搜尋主體 -->

				<p></p>

				<!-- (二)列表主體 -->
				<div class="clearfix">

					<table class="dio_datatable">
						<thead>
							<tr>
								<td>屬性分類</td>
								<td>名稱</td>
								<td>狀態</td>
								<td>建立日期</td>
								<!-- <td>排序</td>  -->
								<td>功能</td>
							</tr>
						</thead>

						<tbody>

						<?php foreach ($query as $row){ ?>
							<tr>
								<td class="center"><?php echo reSortArrayMutil(2,$row['property_category_id'],getPropertyCategory())?>
								</td>
								<td class="center"><?php echo $row['name']?>
								</td>
								<td class="center"><?php echo ($row['status'] == '1') ? "正常" : "<b style='color:red'>停用</b>"?>
								</td>
								<td class="center"><?php echo substr($row['cdate'], 0,10) ?><b style="display: none"><?php echo substr($row['cdate'], 11) ?></td>
								<td class="center"><a class="btn btn-info"
									href="<?php echo getAdminURL('product_property/edit?property_id='.$row['property_id'])?>">
										<i class="fa fa-pencil-square-o"></i> 編輯</a> <a
									class="btn btn-danger"
									href="javascript:my_confirm(<?php echo $row['property_id']?>);">
										<i class="fa fa-trash"></i> 刪除</a></td>
							</tr>
							<?php } ?>

						</tbody>

					</table>
				</div>

			</div>


		</div>

	</div>

	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script type="text/javascript">
	 $(document).ready(function() {
        $('.dio_datatable').dataTable({
          	 "language": { 
   		         url : "<?php echo base_url("application/views/backend/js/dataTables/i18n/chinese_traditional.lang.json")?>"
             },
             "iDisplayLength": 50
         });
    });
   	
    function my_confirm(property_id){
       var chk_box=confirm('確定刪除資料?');
       if (chk_box == true) {
             location.href = '<?php echo base_url("index.php/backend/product_property/del?property_id=") ?>'+property_id;
       }
    }
</script>

</body>
</html>




