<body>

<style>
.fa-star {
	color: red
}
td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'product_property' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			<?php echo form_open( current_url())?>
				<table>
					<tr>
						<th colspan="2">新增資料</th>
					</tr>

					<tr>
						<th width="20%"><i class="fa fa-star"></i>屬性分類</th>
						<td><select class="form-control" name="property_category_id">
		                      <?php foreach( getPropertyCategory() as $key=>$val) :?>    
                                  <option value="<?php echo $key?>"><?php echo $val?></option>
                               <?php endforeach; ?>
	                         </select>
	                    </td>
					</tr>

					<tr>
						<th width="20%"><i class="fa fa-star"></i>分類名稱</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text"
							name="name" value="<?php echo set_value('name')?>"> <b
							style="color: red"><?php echo form_error('name')?> </b></td>
					</tr>
				</table>

				<div class="buttons">
					<input type="submit" value="送出" />
					<button type="button"
						onclick="javascript:location.href='<?php echo getAdminURL('product_property/lists')?>'">返回</button>
				</div>

				</form>
			</div>

		</div>

	</div>

</body>
</html>
