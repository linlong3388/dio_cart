<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'product_property' ,getAdminMenu())?></h2></div>

		<div id="content">
		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">

				<!-- (一)搜尋主體 -->
				<fieldset>
					<legend>搜尋條件:</legend>
					分類項 :
					<?php echo get_assign_field('property', 'property_id', $this->input->get('property_id'), 'name')?>
					<br />
					<button type="button"
						onclick="javascript:location.href='<?php echo getAdminURL('product/property_2insert?property_id='.$this->input->get('property_id') )?>'">新增</button>
					<button type="button"
						onclick="javascript:location.href='<?php echo getAdminURL('product_property/lists')?>'">返回</button>
				</fieldset>

				<p></p>

				<!-- (二)列表主體 -->
				<div class="clearfix">
					<table class="dio_datatable">
						<thead>
							<tr>
								<td>(次級)分類級別</td>
								<td>層級</td>
								<td>新增日期</td>
								<td>功能</td>
							</tr>
						</thead>

						<tbody>

						<?php foreach ($query as $row){ ?>
							<tr>
								<td><?php echo $cls_property->get_name_template( $row['path'] )?>
								</td>
								<td><?php echo $row['level']-1 ?>
								</td>
								<td><?php echo $row['created_at']?>
								</td>
								<td><a class="btn btn-info"
									href="<?php echo getAdminURL('product/property_2update?parent_property_id='.$this->input->get('property_id').'&property_id='.$row['property_id']);?>">
										<i class="fa fa-pencil-square-o"></i> 編輯</a> <a
									class="btn btn-danger"
									href="javascript:my_confirm(<?php echo $this->input->get('property_id') ?>,<?php echo $row['property_id'] ?>);">
										<i class="fa fa-trash"></i> 刪除</a>
								</td>
							</tr>
							<?php } ?>

						</tbody>
					</table>
				</div>

			</div>

		</div>

	</div>

	<script type="text/javascript">
    $(document).ready(function() {
        $('.dio_datatable').dataTable({
          	 "language": { 
   		                  url : "<?php echo base_url("application/views/backend/js/dataTables/i18n/chinese_traditional.lang.json")?>"
                         },
             "iDisplayLength": 100            
         });
    });

    function my_confirm( parent_property_id ,property_id ){

       var host = "<?php echo base_url() ?>";
       var param  = "index.php/backend/product/property_2delete?parent_property_id="+parent_property_id+"&property_id="+property_id;
        
       var chk_box=confirm('確定刪除資料?');
       if (chk_box == true) {
           location.href = host+param;
       }
    }
</script>



</body>
</html>
