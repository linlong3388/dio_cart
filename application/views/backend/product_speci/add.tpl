<body>

<style>
.fa-star {
	color: red
}
td {
    text-align: left;
}
</style>

	<div id="container">

	<div id="content">

	 		<div class="container editform">

	 		<b style="color: red"><?php echo validation_errors(); ?></b>
	 		
				<?php echo form_open( getAdminURL('product_speci/add') ,array('enctype' => 'multipart/form-data'))?>
		           
				<table>
					<tr>
						<th colspan="2">新增屬性資料</th>
					</tr>
					
					<tr style="display: none">
						<th width="20%"><i class="fa fa-star"></i>商品編號</th>
						<td><input type="text" name="product_id" value="<?php echo $this->input->get('product_id') ?>" readonly/>
						</td>
					</tr>					

					<tr style="display: none">
						<th width="20%"><i class="fa fa-star"></i>sku</th>
						<td><input type="text" name="sku" value="<?php echo $this->input->get('sku') ?>" readonly/>
						</td>
					</tr>					
										 
					<tr>
						<th width="20%"><i class="fa fa-star"></i>sku子項</th>
						<td><input type="text" name="sku2" value="<?php echo set_value('sku2') ?>" /></td>
					</tr>

					<tr>
						<th><i class="fa fa-star"></i>屬性/尺寸</th>
						<td><select name="property_id_size">
						      <?php foreach ( transKeyPairArray(getPropertyCategoryItem(0) ,'property_id' ,'name')  as $key=>$val) { ?>
						         <option value="<?php echo $key?>"><?php echo $val?></option>
						      <?php } ?>
						    </select>
						</td>
					</tr>
					
					<tr>
						<th><i class="fa fa-star"></i>屬性/顏色</th>
						<td><select name="property_id_color">
						      <?php foreach ( transKeyPairArray(getPropertyCategoryItem(1) ,'property_id' ,'name') as $key=>$val) { ?>
						         <option value="<?php echo $key?>"><?php echo $val?></option>
						      <?php } ?>
						    </select>
						</td>
					</tr>
				
					<tr>
						<th><i class="fa fa-star"></i>庫存</th>
						<td><input type="text" name="stock" value="<?php echo set_value('stock')?>" onkeypress="def_numeric()" maxlength="12" />
						</td>
					</tr>

					<!-- 
   					<tr>
						<th width="20%">圖片</th>
						<td><input style="display: none" class="input-medium focused" id="specification_image" name="image" value="<?php //echo isset($query['image']) ? $query['image'] : set_value('image') ;?>">
     	                    <img id="image" width="150px" height="150px" src="<?php //echo base_url('resources/uploads/'.$query['image']) ?>" alt="圖片"/>
     	                          <p>                                                       
                                     <a onclick="base_image_upload('image');">選擇圖片</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                     <a onclick="base_image_delete('image');">刪除圖片</a>
                                  </p>
		                </td>
					</tr>
		             -->
				</table>
                           
                     <input style="display: none" id="dio_form1" type="button" onclick="add_speci()" >      
				</form>
	
			</div>

		</div>

	</div>

<script type="text/javascript">
/***********************************
/* 名稱 : add_speci
/* 功能 : 商品屬性 / 新增 
************************************/
function add_speci(){

	 //參數設定
	 var url = '<?php echo getAdminURL('product_speci/add')?>';

     $.post(url ,{data: $('form').serialize()} ,function(err_msg){ 
         //參數驗證
        if(!is_string_empty(err_msg)){
        	alert(err_msg);      	  
            return false;
        }else{            
            location.reload();
        }    
    }); 
        
  }
</script>	
	
</body>
</html>
