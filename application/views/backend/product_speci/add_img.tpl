<body>

	<style>
input[name="product_id"] {
	background-color: rgb(228, 228, 225);
}

.fa-star {
	color: red
}

th {
	padding: 20px
}

select {
	width: auto
}
td {
    text-align: left;
}
</style>

	<div id="container">

	<?php //$this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'product_speci' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php //$this->load->view('backend/common/breadcrumb.tpl')?>
     	
			<div class="container editform">
			
			 <?php if($this->session->flashdata('msg')){ ?>				 
		        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	         <?php } ?> 			
			
			<b style="color: red"><?php echo validation_errors(); ?></b>
			
			<table>

				<?php echo form_open( getCurrentFullUrl() )?>

					<tr>
						<th colspan="2">屬性圖片編輯</th>
					</tr>

					<tr style="display:none">
						<th>屬性編號 :</th>
						<td><input type="text" name="product_speci_id" value="<?php echo $query['product_speci_id']?>" readonly /></td>
					</tr>

					<tr>
						<th>sku子項 :</th>
						<td><input type="text" name="sku2" value="<?php echo $query['sku2']?>" readonly /></td>
					</tr>

					<tr>
						<th><i class="fa fa-star"></i>圖片 :</th>
						<td><input style="display: none" class="input-medium focused"
							id="specification_image" name="image"
							value="<?php echo set_value('image')?>"> <img id="image"
							width="300px" height="250px"
							src="<?php echo base_url("resources/uploads").'/'.set_value('image')?>"
							alt="圖片" /> <a onclick="base_image_upload('image');">選擇圖片</a>&nbsp;&nbsp;|&nbsp;&nbsp;
							<a onclick="base_image_delete('image');">刪除圖片</a></td>
					</tr>

					<tr>
						<th>圖片說明 :</th>
						<td><input type="text" name="image_alt" />
						</td>
					</tr>

					<tr>
						<th></th>
						<td>
							<!-- <button type="button" onclick="add()">新增圖片</button>  -->
							<input type="submit" value="新增圖片" />  
							<button type="button" onclick="exit()">離開</button>
						</td>
					</tr>

					</form>

					<tr>
						<th width="20%">圖片列表</th>
						<td>
							<table class="dio_datatable" style="width: 60% ;">
								<thead>
									<tr>
										<th>圖片</th>
										<th>圖片說明</th>
										<!-- <th>狀態</th> -->
										<th>功能</th>
									</tr>
								</thead>

								<tbody>
								<?php foreach ($query_list as $row) { ?>
									<tr>
										<td style="text-align: center;"><img style="width: 180px; height: 150px"
											src="<?php echo base_url('resources/uploads/'.$row['image'])?>">
										</td>
										<td><?php echo $row['image_alt']?></td>
										<!-- <td align="center"><?php //echo $row['status']?></td> -->
										<td><button type="button" class="btn btn-danger" onclick="javascript:del(<?php echo $row['product_speci_image_id']?>);">刪除</button>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table></td>
					</tr>

				</table>

			</div>

		</div>

	</div>

	<link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('application/views/backend/css/elfinder.min.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('application/views/backend/css/theme.css')?>"rel="stylesheet">

	<script src="<?php echo base_url('application/views/backend/js/jquery-1.7.2.min.js')?>"></script>
	<script	src="<?php echo base_url('application/views/backend/js/jquery-ui-1.8.21.custom.min.js');?>"></script>
	<script src="<?php echo base_url('application/views/backend/js/elfinder.min.js')?>"></script>
	<script src="<?php echo base_url('application/views/backend/js/i18n/elfinder.zh_CN.js')?>"></script>
	<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_image_upload.js')?>"></script>
	
<script type="text/javascript">

//-----------------------------------------------------------------
//
// 離開 
//
//-----------------------------------------------------------------
function exit(){
  opener.location.reload(); 
  window.close(); 
}


//-----------------------------------------------------------------
//
// 圖片新增 
//
//-----------------------------------------------------------------
/*
function add(){
	 $('form').submit();
	 $('<div>處理中 ,請稍候...</div>').dialog();
}*/


//-----------------------------------------------------------------
//
// 圖片刪除
//
//-----------------------------------------------------------------
 function del(product_speci_image_id){
   var chk_box=confirm('確定刪除資料?');
   if (chk_box == true) {

	   $('<div>處理中 ,請稍候...</div>').dialog();
	   
	   var url = '<?php echo base_url("index.php/backend/product/product_edit_image_del?product_speci_image_id=") ?>'+product_speci_image_id;
	   $.post(url ,function(){		      
            location.reload();
	   });
   }
 }

</script>

</body>
</html>
