<body>
	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'dashboard' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl'); ?>

			<!-- 
			<?php if($query_notice['notice']){ ?>
			<div class="note">
				系統公告：<?php echo $query_notice['notice'] ?>
			</div>
			<?php } ?>
             -->

			<div class="events clearfix">

				<a class="item profile" href="#"> <i class="fa fa-user"></i> <span
					class="name"><?php echo $_SESSION['admin_info']['username']?>
				</span> <span class="info">上次登入：<?php echo $_SESSION['admin_info']['log_date']?>
				</span> <span class="info">IP 位址：<?php echo $this->input->ip_address()?>
				</span> </a> 
				
				
				<!-- 訂單管理  -->	
		        <?php if( isAdminRoleTbName('order') ) { ?>	
				<a class="item blue" href="<?php echo getAdminURL('order/lists?srh_status=1')?>">
					<i class="fa fa-credit-card"></i> 
					<span class="number"><?php echo $query_order_count_1?></span> 
					<span class="info">待處理訂單</span> 
				</a> 
				
				<a class="item red" href="<?php echo getAdminURL('order/lists?srh_status=3')?>">
					<i class="fa fa-truck"></i> 
					<span class="number"><?php echo $query_order_count_3?></span> 
					<span class="info">取消訂單</span> 
				</a> 
				<?php } ?>

				
				<!-- 商品管理  -->
		        <?php if( isAdminRoleTbName('product')  ) { ?>
				<a class="item yellow" href="<?php echo getAdminURL('product/lists')?>"> 
				   <i class="fa fa-comments"></i> 
				   <span class="number"><?php echo $query_product ?></span> 
				   <span class="info">商品數量</span> 
				</a>
				<?php } ?>
				
				
				<!-- 聯絡我們  -->
		        <?php if( isAdminRoleTbName('contact')  ) { ?>
				<a class="item green" href="<?php echo getAdminURL('contact/lists')?>"> 
				   <i class="fa fa-comments"></i> 
				   <span class="number"><?php echo $query_contact?></span> 
				   <span class="info">聯絡我們</span> 
				</a>
				<?php } ?>
				
			</div>


			<!-- 最新訂單列表 -->
			<?php if( isAdminRoleTbName('order') ) { ?>
			<div style="margin: 1% auto">
				<b>最新10筆訂單：</b>
				<table class="dio_datatable">

					<thead>
						<tr>
								<td>訂單編號</td>
								<td>訂單來源</td>
								<td>訂購人</td>
								<!-- <td>配送方式</td> -->
								<td>收件人</td>
								<td>總額</td>
								<td>訂單狀態</td>
								<!-- <td>預購單</td> -->
								<td>訂購日期</td>
							<td>功能</td>
						</tr>
					</thead>

					<tbody>

					<?php foreach ($query_order_new as $row){ ?>
						<tr>
								<td><?php echo $row['order_show_id']?></td>
								<td><?php echo empty($row['b_title']) ? '網頁下單' : $row['b_title']?></td>
								<td><?php echo $row['o_name']?></td>
								<!-- 
								<td><?php //echo mb_substr( reSortArrayMutil(2,$row['trans_method'],getTransMethod()), 0, 4,"utf-8")?></td>
								 -->
								<td><?php echo empty($row['name']) ? '--' : $row['name']?></td>
								<td><?php echo DIO_CURRENCY.addCommas($row['total'])?></td>
								<td><?php echo reSortArrayMutil(2 ,$row['status'],getOrderStatus())?></td>
								<!-- <td><?php //echo empty($row['order_type']) ? '否' : "<b style='color:red'>是</b>"?></td> -->
								<td><?php echo substr($row['cdate'], 0,10) ?><b style="display: none"><?php echo substr($row['cdate'], 11) ?></td>
						    	<td align="center"><a class="btn btn-info"
								href="<?php echo getAdminURL('order/view?order_id='.$row['order_id'])?>">
									<i class="fa fa-pencil-square-o"></i> 檢視</a></td>
						</tr>
						<?php } ?>

					</tbody>

				</table>
			</div>
			<?php } ?>

		</div>

		<?php $this->load->view('backend/common/footer.tpl'); ?>

	</div>

	<script>
	/*
    $(document).ready(function() {
        $('.dio_datatable').dataTable({
          	 "language": { 
   		         url : "<?php //echo base_url("application/views/backend/js/dataTables/i18n/chinese_traditional.lang.json")?>"
             },
             "order": [[ 0, "desc" ]]
         });
    });*/
    </script>
</body>
</html>
