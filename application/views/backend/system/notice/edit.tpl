<body>

<style>
.fa-star {
	color: red
}
td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'notice' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			<?php echo form_open( current_url())?>
				<table>
					<tr>
						<th colspan="2">編輯資料</th>
					</tr>

					<tr>
						<th width="20%">編號</th>
						<td><input type="text" name="notice_id" value="<?php echo isset($query['notice_id']) ? $query['notice_id'] : set_value('notice_id')?>" readonly></td>
					</tr>
			
					<tr>
						<th width="20%"><i class="fa fa-star"></i>內容</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="content" value="<?php echo isset($query['content']) ? $query['content'] : set_value('content')?>">
							<b style="color: red"><?php echo form_error('content')?></b>
					    </td>
					</tr>

					<tr>
						<th width="20%"><i class="fa fa-star"></i>商品狀態</th>
						<td>
						    <input type="radio" name="status" value="1"
						    <?php if( isset($query['status']) && ($query['status'] == '1') ) echo set_radio('status', '1' ,true);
						      else echo set_radio('status', '1'); ?>>啟用 
						    <input type="radio" name="status" value="0"
							<?php if( isset($query['status']) && ($query['status'] == '0') ) echo set_radio('status', '0' ,true);
							  else echo set_radio('status', '0'); ?>>停用
						</td>
					</tr>

					<tr>
						<th width="20%">建立日期</th>
						<td><input class="fullwidth" type="text" name="cdate" value="<?php echo isset($query['cdate']) ? $query['cdate'] : set_value('cdate')?>" readonly> 
						    <b style="color: red"><?php echo form_error('cdate')?></b>
						 </td>
					</tr>

				</table>

				<div class="buttons">
					<input type="submit" value="送出" />
					<button type="button"
						onclick="javascript:location.href='<?php echo getAdminURL('notice/lists')?>'">返回</button>
				</div>

				</form>
			</div>

		</div>

	</div>

</body>
</html>
