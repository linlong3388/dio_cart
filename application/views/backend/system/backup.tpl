<body>

<style>
.fa-star {
	color: red
}
td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'news' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>
		
			<div class="container editform">

	   <?php if($this->session->flashdata('msg')){ ?>				 
		 <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	    <?php } ?> 			
			
			<?php echo form_open( getCurrentFullUrl())?>
				<table>

					<tr>
						<th colspan="2">資料庫備份</th>
					</tr>

					<tr>
						<th>資料庫下載</th>
						<td><input type="submit" name="download_db" value="確定" /></td>
					</tr>
				</table>

				</form>
			</div>

		</div>

	</div>

	<script
		src="<?php echo base_url('application/views/frontend/js/dio_plugins/jquery_fields_city.js');?>"></script>

</body>
</html>

