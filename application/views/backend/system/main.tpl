<body>

<style>
.fa-star {
	color: red
}
td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'system' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>
		
			<div class="container editform">

	   <?php if($this->session->flashdata('msg')){ ?>				 
		 <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	    <?php } ?> 			
			
			<b style="color: red"><?php echo validation_errors(); ?></b>
			
			<?php echo form_open( current_url() )?>
				<table>

					<tr>
						<th colspan="2">系統設定</th>
					</tr>

					<tr>
						<th><i class="fa fa-star"></i>系統維護中</th>
						<td>
							<input type="radio" name="is_enabled" value="1" <?php if( isset($query['is_enabled']) && ($query['is_enabled'] == '1') ) echo set_radio('is_enabled', '1' ,true);
								else echo set_radio('is_enabled', '1'); ?>> 啟用
							<input type="radio" name="is_enabled" value="0" <?php if( isset($query['is_enabled']) && ($query['is_enabled'] == '0') ) echo set_radio('is_enabled', '0' ,true);
								else echo set_radio('is_enabled', '0'); ?>> 停用
						</td>
					</tr>
					
					<tr>
						<th><i class="fa fa-star"></i>預計到貨時段</th>
						<td>
							<input type="radio" name="is_time_arrival" value="1" <?php if( isset($query['is_time_arrival']) && ($query['is_time_arrival'] == '1') ) echo set_radio('is_time_arrival', '1' ,true);
								else echo set_radio('is_time_arrival', '1'); ?>> 啟用
							<input type="radio" name="is_time_arrival" value="0" <?php if( isset($query['is_time_arrival']) && ($query['is_time_arrival'] == '0') ) echo set_radio('is_time_arrival', '0' ,true);
								else echo set_radio('is_time_arrival', '0'); ?>> 停用
						</td>
					</tr>
					
					<tr>
						<th>網站名稱</th>
						<td>
							<input type="text" name="web_name" value="<?php echo isset($query['web_name']) ? $query['web_name'] : set_value('web_name') ?>" /> 
						</td>
					</tr>
					
					<tr>
						<th>網址</th>
						<td>
							<input type="text" name="web_url" value="<?php echo isset($query['web_url']) ? $query['web_url'] : set_value('web_url') ?>" /> 
						</td>
					</tr>
					
					<tr>
						<th>管理員信箱</th>
						<td>
							<input type="text" name="web_email" value="<?php echo isset($query['web_email']) ? $query['web_email'] : set_value('web_email') ?>" /> 
						</td>
					</tr>
					
					<tr>
						<th>電話</th>
						<td>
							<input type="text" name="web_phone" value="<?php echo isset($query['web_phone']) ? $query['web_phone'] : set_value('web_phone') ?>" /> 
						</td>
					</tr>
					
					<tr>
						<th>傳真</th>
						<td>
							<input type="text" name="web_fax" value="<?php echo isset($query['web_fax']) ? $query['web_fax'] : set_value('web_fax') ?>" /> 
						</td>
					</tr>
					
					<tr>
						<th>地址</th>
						<td>
							<input type="text" name="web_addr" value="<?php echo isset($query['web_addr']) ? $query['web_addr'] : set_value('web_addr') ?>" /> 
						</td>
					</tr>
					
					<tr>
						<th>運費設定</th>
						<td>
							<input type="text" name="shipping_fee" value="<?php echo isset($query['shipping_fee']) ? $query['shipping_fee'] : set_value('shipping_fee') ?>" onkeypress="def_numeric()"> 
						    免運額 : <input type="text" name="shipping_fee_full" value="<?php echo isset($query['shipping_fee_full']) ? $query['shipping_fee_full'] : set_value('shipping_fee_full') ?>" onkeypress="def_numeric()" > 
						</td>
					</tr>
					
					<tr id="hd_image">
						<th width="20%">Logo<br><b style="color:blue"><?php echo reSortArrayMutil(2 ,'logo' ,getRecommendSize())?></b></th>
						<td><input style="display: none" class="input-medium focused" id="specification_logo" name="logo" value="<?php echo isset($query['logo']) ? $query['logo'] : set_value('logo') ;?>">
     	                    <div class="span12">
			                   <img id="logo" class="img-responsive" width="275px" height="80px"  src="<?php echo (!empty($query['logo'])) ? base_url("resources/uploads/".$query['logo']) : base_url("resources/uploads").'/'.set_value('logo')?>" alt="圖片"/>
                                  <p>                                                       
                                     <a onclick="base_image_upload('logo');">選擇圖片</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                     <a onclick="base_image_delete('logo');">刪除圖片</a>
                                  </p>
		                     </div>          
						</td>
					</tr>
					
					<!--  
					<tr id="hd_image">
						<th width="20%">Favicon<br><b style="color:blue"><?php //echo reSortArrayMutil(2 ,'favicon' ,getRecommendSize())?></b></th>
						<td><input style="display: none" class="input-medium focused" id="specification_favicon" name="favicon" value="<?php //echo isset($query['favicon']) ? $query['favicon'] : set_value('favicon') ;?>">
     	                    <div class="span12">
			                   <img id="favicon" class="img-responsive" width="32px" height="32px"  src="<?php //echo (!empty($query['favicon'])) ? base_url("resources/uploads/".$query['favicon']) : base_url("resources/uploads").'/'.set_value('favicon')?>" alt="圖片"/>
                                  <p>                                                       
                                     <a onclick="base_image_upload('favicon');">選擇圖片</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                     <a onclick="base_image_delete('favicon');">刪除圖片</a>
                                  </p>
		                     </div>          
						</td>
					</tr>
					-->
					
					<!-- 
					<tr>
						<th width="20%">客製化流程</th>
						<td><textarea class="fullwidth dio_textarea" name="rule_flow" placeholder="請輸入資料"><?php //echo isset($query['rule_flow']) ? $query['rule_flow'] : set_value('rule_flow')?></textarea></td>
					</tr>
					 -->
			
			        <tr>
						<th width="20%">會員服務條款</th>
						<td><textarea class="fullwidth dio_textarea" name="rule_register" placeholder="請輸入資料"><?php echo isset($query['rule_register']) ? $query['rule_register'] : set_value('rule_register')?></textarea></td>
					</tr>		
					
					<tr>
						<th width="20%">購物須知</th>
						<td><textarea class="fullwidth dio_textarea" name="rule_shipping_fee" placeholder="請輸入資料"><?php echo isset($query['rule_shipping_fee']) ? $query['rule_shipping_fee'] : set_value('rule_shipping_fee')?></textarea></td>
					</tr>				

					<!-- 
					<tr>
						<th width="20%">優惠券須知</th>
						<td><textarea class="fullwidth dio_textarea" name="rule_coupon" placeholder="請輸入資料"><?php //echo isset($query['rule_coupon']) ? $query['rule_coupon'] : set_value('rule_coupon')?></textarea></td>
					</tr>
					 -->				
					 
					<tr>
						<th width="20%">付款方式(ATM)</th>
						<td><textarea class="fullwidth dio_textarea" name="pay_method_atm" placeholder="請輸入資料"><?php echo isset($query['pay_method_atm']) ? $query['pay_method_atm'] : set_value('pay_method_atm')?></textarea></td>
					</tr>
					
					<!-- 
					<tr>
						<th width="20%">付款方式(貨到付款)</th>
						<td><textarea class="fullwidth" name="pay_method_cod" placeholder="請輸入資料"><?php //echo isset($query['pay_method_cod']) ? $query['pay_method_cod'] : set_value('pay_method_cod')?></textarea></td>
					</tr>					
					 
					-->
					
					<!-- 
					<tr>
						<th width="20%">運送服務</th>
				        <td><textarea class="fullwidth dio_textarea" name="trans_method1" placeholder="請輸入資料"><?php //echo isset($query['trans_method1']) ? $query['trans_method1'] : set_value('trans_method1')?></textarea></td>
					</tr>	
					
					<tr>
						<th width="20%">門市店取</th>
				        <td><textarea class="fullwidth dio_textarea" name="trans_method2" placeholder="請輸入資料"><?php //echo isset($query['trans_method2']) ? $query['trans_method2'] : set_value('trans_method2')?></textarea></td>
					</tr>
					 -->					

					<!-- 
					<tr>
						<th width="20%">自訂程式碼(GA)<br/><i class="fa fa-star">請由系統人員定義</i></th>
						<td><textarea class="fullwidth dio_textarea" name="ga" placeholder="請輸入資料"><?php //echo isset($query['ga']) ? $query['ga'] : set_value('ga')?></textarea></td>
					</tr>
					 -->					
					
					<!-- 
					<tr>
						<th width="20%">原始碼自訂header<br/><i class="fa fa-star">請由系統人員定義</i></th>
						<td><textarea class="fullwidth" name="content_header" rows="10" placeholder="請輸入資料"><?php //echo isset($query['content_header']) ? $query['content_header'] : set_value('content_header')?></textarea></td>
					</tr>
					
					<tr>
						<th width="20%">原始碼自訂body<br/><i class="fa fa-star">請由系統人員定義</i></th>
						<td><textarea class="fullwidth" name="content_body" rows="10" placeholder="請輸入資料"><?php //echo isset($query['content_body']) ? $query['content_body'] : set_value('content_body')?></textarea></td>
					</tr>
					 -->
					
					<tr>
						<th colspan="2">超商取貨設定</th>
					</tr>
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>寄件人姓名</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="cvs_name" value="<?php echo isset($query['cvs_name']) ? $query['cvs_name'] : set_value('cvs_name')?>">
						</td>
					</tr>
					
					<tr>
						<th width="20%">寄件人電話<br><b style="color:blue">僅允許數字；特殊符號僅限()-#</b></th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="cvs_phone" value="<?php echo isset($query['cvs_phone']) ? $query['cvs_phone'] : set_value('cvs_phone')?>">
						</td>
					</tr>
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>寄件人手機<br><b style="color:blue">僅允許數字</b></th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="cvs_mobile" value="<?php echo isset($query['cvs_mobile']) ? $query['cvs_mobile'] : set_value('cvs_mobile')?>">
						</td>
					</tr>
					
					<tr>
						<th width="20%">寄件人備註</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="cvs_memo" value="<?php echo isset($query['cvs_memo']) ? $query['cvs_memo'] : set_value('cvs_memo')?>">
						</td>
					</tr>
					
					
					<tr>
						<th colspan="2">Meta頁籤(系統預設)</th>
					</tr>
					
					<tr>
						<th width="20%">Meta標籤標題</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="meta_title" value="<?php echo isset($query['meta_title']) ? $query['meta_title'] : set_value('meta_title')?>">
						</td>
					</tr>
					
					<tr>
						<th width="20%">Meta標籤描述</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="meta_description" value="<?php echo isset($query['meta_description']) ? $query['meta_description'] : set_value('meta_description')?>">
						</td>
					</tr>
					
					<tr>
						<th width="20%">Meta標籤關鍵字</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="meta_keyword" value="<?php echo isset($query['meta_keyword']) ? $query['meta_keyword'] : set_value('meta_keyword')?>">
						</td>
					</tr>				
					
					<tr>
						<th>最後更新日期</th>
						<td><input class="fullwidth" type="text" name="udate" value="<?php echo isset($query['udate']) ? $query['udate'] : set_value('udate')?>" readonly></td>
					</tr>

				</table>

				<div class="buttons">
					<input type="submit" value="送出" />
				</div>

				</form>
			</div>

		</div>

	</div>

<script src="<?php echo base_url('application/views/backend/js/tinymce/tinymce.min.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_tinymce_elfinder.js')?>"></script>

<link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>" rel="stylesheet">
<link href="<?php echo base_url('application/views/backend/css/elfinder.min.css')?>" rel="stylesheet">
<link href="<?php echo base_url('application/views/backend/css/theme.css')?>" rel="stylesheet">

<script src="<?php echo base_url('application/views/backend/js/jquery-1.7.2.min.js');?>"></script>
<script src="<?php echo base_url('application/views/backend/js/jquery-ui-1.8.21.custom.min.js');?>"></script>
<script src="<?php echo base_url('application/views/backend/js/elfinder.min.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/i18n/elfinder.zh_CN.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_image_upload.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_fields_date.js')?>"></script>

</body>
</html>

