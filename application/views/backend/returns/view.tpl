<body>

<style>
.fa-star {
	color: red
}

td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'returns' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			<?php echo form_open( getCurrentFullUrl() )?>
				<table>
					<tr>
						<th colspan="2">收件人資料</th>
					</tr>

					<tr>
						<th width="20%">退貨編號</th>
						<td><input type="text" name="return_id"
							value="<?php echo isset($query['return_id']) ? $query['return_id'] : set_value('return_id') ?>"
							readonly /></td>
					</tr>

					<tr>
						<th width="20%">訂單編號</th>
						<td><a class="btn btn-success" 
	                        href="<?php echo getAdminURL('order/view?order_id='.$query['order_id'])?>"
	                        target="_blank"><?php echo get_assign_field('`order`', 'order_id', $query['order_id'], 'order_show_id')?></a>
	                    </td>
					</tr>

					<tr>
						<th width="20%">狀態</th>
						<td><select name="status" style="width: 15%">
						<?php foreach (get_return_status(3 ,$query['status']) as $key=>$val) { ?>
								<option value="<?php echo $key?>"><?php echo $val?></option>
						<?php } ?>
						</select> <input type="submit" class="btn btn-primary"
							value="變更狀態"></td>
					</tr>

					<tr>
						<th>姓名</th>
						<td><?php echo $query['name']?></td>
					</tr>

					<!-- 
					<tr>
						<th>手機</th>
						<td><?php //echo $query['mobile']?></td>
					</tr>
					 -->

					<tr>
						<th>手機或電話</th>
						<td><?php echo $query['phone']?></td>
					</tr>

					<tr>
						<th>Email</th>
						<td><?php echo $query['email']?></td>
					</tr>

					<tr>
						<th>地址</th>
						<td><?php echo $query['local'] .' / '. $query['address']?></td>
					</tr>

					<tr>
						<th>數量</th>
						<td><?php echo $query['entity']?></td>
					</tr>

					<tr>
						<th>售價</th>
						<td><?php echo DIO_CURRENCY.addCommas($query['price'])?></td>
					</tr>

					<tr>
						<th>小計</th>
						<td><?php echo DIO_CURRENCY.addCommas($query['total'])?></td>
					</tr>

					<tr>
						<th>建立日期</th>
						<td><?php echo $query['cdate']?></td>
					</tr>

					<tr>
						<th colspan="2">商品資料</th>
					</tr>

					<tr>
						<th>商品編號</th>
						<td><a class="btn btn-success"
							href="<?php echo getAdminURL('product/edit?product_id='.$query['product_id'])?>"
							target="_blank"> <?php echo $query['product_id']?> </a></td>
					</tr>

					<tr>
						<th>商品名稱</th>
						<td><?php echo $query['p_name']?></td>
					</tr>
					
				    <tr>
						<th>型號</th>
						<td><?php echo $query['sku']?></td>
					</tr>
					
					<tr>
						<th colspan="2">退貨原因</th>
					</tr>

					<!-- 
					<tr>
						<th>退貨類型</th>
						<td><?php //echo reSortArrayMutil(2,$query['reason_id'] ,getReasonId())?></td>
					</tr>
					 -->				

				    <tr>
						<th>退貨原因</th>
						<td><?php echo $query['reason']?></td>
					</tr>					
					
					
					<tr>
						<th colspan="2">系統管理</th>
					</tr>	
					
					<tr>
						<th>管理人員備註</th>
						<td><textarea name="memo_admin" rows="10" cols="100" class="fullwidth"><?php echo isset($query['memo_admin']) ? $query['memo_admin'] : set_value('memo_admin')?></textarea></td>
					</tr>				
					
				</table>

				<div class="buttons">
				     <input type="submit" value="送出" />
				    <button type="button" onclick="javascript:location.href='<?php echo getAdminURL('returns/lists')?>'">返回</button> 
				</div>

				</form>
			</div>

		</div>

	</div>

</body>
</html>
