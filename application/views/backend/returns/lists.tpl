<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
    <div id="title"><h2><?php echo reSortArrayMutil(2,'returns' ,getAdminMenu())?></h2></div>

		<div id="content">
		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">

  			<?php if($this->session->flashdata('msg')){ ?>				 
				  <div class="alert alert-success"><i class="fa fa-check-circle"></i><?php echo $this->session->flashdata('msg')?></div>
			<?php }elseif($this->session->flashdata('msg_err')){ ?>
				  <div class="alert alert-danger"><i class="fa fa-times-circle"></i> <?php echo $this->session->flashdata('msg_err')?></div>
			<?php } ?>				
			
				<!-- (一)搜尋主體 -->
		
				<!-- (二)列表主體 -->
				
				<div class="clearfix">
					<table class="dio_datatable">
						<thead>
							<tr>
								<td>退換編號</td>
								<!-- <td>訂單編號</td>  -->
								<td>收件人</td>
								<td>商品名稱</td>
								<td>狀態</td>
								<td>建立日期</td>
								<td>功能</td>
							</tr>
						</thead>

						<tbody>
						<?php foreach ($query as $row) { ?>
							<tr>
								<td><?php echo $row['return_id']?></td>
								<!-- <td><?php //echo $row['order_id']?></td>  -->
								<td><?php echo $row['name']?></td>
								<td><?php echo $row['p_name']?></td>
								<td><?php echo get_return_status( 2,$row['status'])?></td>
								<td><?php echo substr($row['cdate'], 0,10) ?><b style="display: none"><?php echo substr($row['cdate'], 11) ?></td>
								<td><a class="btn btn-success"
									href="<?php echo getAdminURL('returns/view?return_id='.$row['return_id'])?>">
										<i class="fa fa-pencil-square-o"></i>檢視</a></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>

			</div>

		</div>
	</div>

	<link
		href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css');?>"
		rel="stylesheet">
	<script
		src="<?php echo base_url('application/views/backend/js/jquery-ui-1.11.1.min.js');?>"></script>
	<script
		src="<?php echo base_url('application/views//backend/js/dio_plugins/jquery_fields_date.js');?>"></script>

	<script>
    $(document).ready(function() {
        $('.dio_datatable').dataTable({
          	 "language": { 
   		         url : "<?php echo base_url("application/views/backend/js/dataTables/i18n/chinese_traditional.lang.json")?>" ,
   		      "dom": '<"top"i>rt<"bottom"flp><"clear">'
             },
             "iDisplayLength": 100,
             "order": [[ 0, "desc" ]]  
         });
    });
</script>

</body>
</html>
