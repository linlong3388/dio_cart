/*******************************************************************
 * 
 * 定義class為'dio_edu'就能自動啟用 select的學歷資料。
 * 
 * @functionName jquery_fields_sltMulti
 * @author Dio
 * 
 *******************************************************************/

//----------------------------------------------------------
//
// 將選擇資料，放進 select multiple
//
//----------------------------------------------------------
function sltToTextarea(id,param){
	
	if( $('#'+id).has('option').length > 0 ) {

		//取得select容器(物件)資料
		var sltData = $.map($('#'+id+' option') ,function(option) {
			return option.value;
		});

		//走訪物件資料
		for(key in sltData){

			//如果有重複資料
            if(param.split("|")[0] == sltData[key]){
               return false;
            }        
		}
	}

	$('#'+id).append("<option value=" + param.split("|")[0] + ">" + param.split("|")[1] + "</option>");

	return true;
}


//----------------------------------------------------------
//
// 單筆刪除 textArea 資料
//
//----------------------------------------------------------
function delTextarea(id,param){
	$('#'+id+' option').filter("[value=" + param + "]").remove();
	
	return true;
}


