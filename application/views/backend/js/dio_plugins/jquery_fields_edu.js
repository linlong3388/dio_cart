/**
 * 
 * 函數 : jQuery擴充 - 學歷資料
 * 說明 : 定義class為'dio_edu'就能自動啟用 select的學歷資料。
 * 
 * @functionName jquery_fields_edu
 * @author Dio
 * 
 */

$(function(){
	$('.dio_edu').append(
			'<option value="博士">博士</option>',
			'<option value="碩士">碩士</option>',
			'<option value="大學">大學</option>',
			'<option value="四技">四技</option>',
			'<option value="二技">二技</option>',
			'<option value="二專">二專</option>',
			'<option value="三專">三專</option>',
			'<option value="五專">五專</option>',
			'<option value="高中">高中</option>',
			'<option value="高職">高職</option>',
			'<option value="國中(含)以下">國中(含)以下</option>'
			);
}); 

/* END jquery_fields_edu */
