$(function(){
     tinymce.init({ //selector:'textarea',
    	            selector:'.dio_textarea',  
    	            plugins:  ['advlist autolink lists link image charmap print preview anchor',
                                  'searchreplace visualblocks code fullscreen',
                                  'insertdatetime media table contextmenu paste code',
                                  'textcolor colorpicker'],
	                 toolbar: 'insertfile undo redo | styleselect | bold italic | forecolor backcolor |'+
	                          'alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |'+
	                          'link image | fontsizeselect',
	                 height : "350",           
	                 language: 'zh_TW', 
	                 fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
	                 
	                 //串接elfinder檔案管理器 
	                 file_browser_callback: elFinderBrowser
	             });
});


/***********************************
/* 名稱 : jquery_tinymce_elfinder
/* 功能 : 基本圖片上傳 
/* @param : 名稱 ,路徑
************************************/
function elFinderBrowser (field_name, url, type, win) {
	  tinymce.activeEditor.windowManager.open({
	    file: dio_elfinder_url_dialog,
	    title: 'elFinder 2.0',
	    width: 900,  
	    height: 450,
	    resizable: 'yes'
	  }, {
	    setUrl: function (url) {
	      win.document.getElementById(field_name).value = url;
	    }
	  });

	  return false;
} 


/* END jquery_image_upload */
