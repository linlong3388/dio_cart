/**
 * 
 * 函數 : jQuery擴充 - 文字編輯器裡的圖片上傳
 * 說明 : 
 * @functionName jquery_image_upload_textarea
 * @author Dio
 */

$(function(){

	/***********************************
	/* 名稱 : 預設
	/* 功能 : 文字編輯器的圖片上傳
	************************************/
	$("textarea").cleditor({
	  	 width:   800 , 
	     height:  500 
	});

	var defalut_txtImg_path;
	var defalut_txtImg_filename;

	//file manager
	 var elf = $('.file-manager').elfinder({
	 	               url : 'elfinder_init',  // connector URL (REQUIRED) 	              
	 	               handlers : {
	 	                         select : function(event, elfinderInstance) {
	                                      var selected = event.data.selected;
	                                     
	                                    if(selected.length > 0){
	 	                                	    var file = elfinderInstance.file(selected[0]);
	 	                                	    defalut_txtImg_filename = file.name; //檔名
	 	                                	    defalut_txtImg_path = get_host_folder() +'resources/' + elfinderInstance.path(selected[0]).replace(/\\/g, '/'); //將反斜線反轉
	 	                                	    
	                                            $('#dio_cleditor_url').val(defalut_txtImg_path);
	                                      }
	 	                          },
	 	                         dblclick: function(event, elfinderInstance) {
	                   	              //判斷是否為圖檔 
	                                  if(is_image(defalut_txtImg_filename)){  

	                                      $('#dio_cleditor_url').val(defalut_txtImg_path);
	                              	      $('#dio_cleditor_button').click(); 
	           	                          return false; // stop elfinder
	                                  }                    
	                            }
	 	                   },      
	 	                uiOptions: {
	 	                           toolbar : [ ['search'] ]
	 	                         }         
	     	       }).elfinder('instance');    
	

});
/* END jquery_image_upload_textarea */