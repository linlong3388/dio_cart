﻿/*****************************************************
/* 名稱           : is_image
 * 功能           : 判斷是否為圖檔
 ****************************************************/ 
function is_image(filename) {
	var parts = filename.split('.');
    var ext = parts[parts.length - 1];
    switch (ext.toLowerCase()) {
    case 'jpg':
    case 'gif':
    case 'bmp':
    case 'png':
        //etc
        return true;
    }
    return false;
}


/*****************************************************
/* 名稱           : is_file
 * 功能           : 判斷是否為檔案
 ****************************************************/ 
function is_file(filename) {
	var parts = filename.split('.');
    var ext = parts[parts.length - 1];
    switch (ext.toLowerCase()) {
    case 'jpg':
    case 'gif':
    case 'bmp':
    case 'png':
    case 'png':	
    case 'csv':
    case 'doc':
    case 'htm':
    case 'html':
    case 'jar':
    case 'pdf':
    case 'pps':
    case 'ppt':
    case 'rar':
    case 'rtf':
    case 'tgz':
    case 'txt':
    case 'xls':	
    case 'zip':	
    	//etc
        return true;
    }
    return false;
}


/*****************************************************
/* 名稱           : is_string_empty
 * 功能           : 判斷是否為空字串
 ****************************************************/ 
function is_string_empty(str) {
    return (!str || 0 === str.length);
}


/*****************************************************
/* 名稱           : MM_openBrWindow
 * 功能           : 彈跳子視窗
 ****************************************************/ 
function MM_openBrWindow(theURL,winName,features) { //v2.0
	  window.open(theURL,winName,features);
}


/*****************************************************
/* 名稱           : def_numeric
 * 功能           : 定義輸入值必須為數值型態
 ****************************************************/ 
function def_numeric(){
	if(event.keyCode < 48 || event.keyCode > 57) if(event.keyCode != 13 ) 
		 event.returnValue = false;
}


/*****************************************************
/* 名稱           : del_confirm
 * 功能           : 使用者按下刪除後 ,跳出確認視窗 ,並做導向 
 ****************************************************/ 
function del_confirm(url ,id){
   var chk_box=confirm('確定刪除資料?');
   if (chk_box == true) {
	  location.href = url+id;
   }
}


/*****************************************************
/* 名稱           : dio_random
 * 功能           : 產生一組隨機數 
 * 備註           ：由於IE瀏覽器不支援indexOf，參考
 * http://paladinprogram.blogspot.tw/2010/07/ie-arrayindexof.html
 ****************************************************/ 
function dio_random(val){
    var a=[];
    while(a.length < val) {
        var n = Math.ceil(Math.random() * val);      
        if (a.indexOf(n)==-1) a.push(n);        
    }
      
     return a;  
}


/*****************************************************
/* 名稱           : is_null
 * 功能           : 判斷是否為null值 
 ****************************************************/
function is_null(obj){  
  if (typeof(obj) == 'undefined' || obj == null)  
    return true;  
  return false;  
}  


/*****************************************************
/* 名稱           : get_host_folder
 * 功能           : 取得主機/資料夾路徑 
 ****************************************************/
function get_host_folder(){
	
	var folder   = '';
    var pathname = location.pathname.split('/')[1];
	
	//判斷資料夾是否存在
	if( pathname != '' && pathname != 'index.php'){
		folder = location.pathname.split('/')[1] + "/";
	}
	
	//本機版
	var url = location.protocol + "//" + document.domain + "/"  + folder;
	
	//線上版(根目錄下)
	//var url = location.protocol + "//" + document.domain + "/" ;
	
	return url;
}


/*************************************************************************************
/* 名稱           : removeUrlParam
 * 功能           : 移除url的特定參數值
 * 說明           : [key]參數名稱
 *                  [sourceURL] URL，可用window.location.href 取得目前的url
 ************************************************************************************/
function removeUrlParam(key, sourceURL) {
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}

