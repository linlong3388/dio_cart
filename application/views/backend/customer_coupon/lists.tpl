<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'customer' ,getAdminMenu())?></h2></div>

		<div id="content">
		
			<span style="float: right">
				<div class="actions">
				    <a href="javascript:location.href='<?php echo getAdminURL('customer/edit?customer_id='.$this->input->get("customer_id"))?>' " class="btn btn-default btn-sm"> 
					<i class="fa fa-pencil"></i> 編輯</a> 
					
					<a href="javascript:location.href='<?php echo getAdminURL('customer/lists?page=1')?>' " class="btn btn-default btn-sm"> 
					<i class="fa fa-reply"></i> 返回列表</a>             
				</div> 
		    </span>
		
		<?php $this->load->view('backend/common/breadcrumb.tpl')?>
			
			<div class="container">

			<label>會員ID : <input type="text" value="<?php echo $query['customer_id']?>" style="width:10%" readonly /> </label>
			<label>會員帳號 : <input type="text" value="<?php echo $query['username']?>" readonly /> </label>
			<label>會員姓名 : <input type="text" value="<?php echo $query['last_name']?>" readonly /> </label>
			
			<hr />
			
				<div class="clearfix">

					<table class="dio_datatable">
						<thead>
							<tr>
                               <th>序號</th>
                               <th>訂購編號</th>
                               <th>活動方案</th>
                               <th>金額</th>
							</tr>
						</thead>

						<tbody>
						<?php foreach ($query_coupon as $row) { ?>
                           <tr>
                              <td><?php echo $row['code']?></td>
                              <td><?php echo $row['order_show_id']?></td>
                              <td><?php echo $row['name']?></td>
                              <td><span class="gold"><?php echo DIO_CURRENCY.addCommas($row['total'])?></span></td>
                           </tr>
						<?php } ?>
						</tbody>

					</table>

				</div>


			</div>

		</div>

	</div>

	<link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>" rel="stylesheet">
	<script src="<?php echo base_url('application/views/backend/js/jquery-ui-1.11.1.min.js')?>"></script>
	<script src="<?php echo base_url('application/views//backend/js/dio_plugins/jquery_fields_date.js')?>"></script>

	<script type="text/javascript">

	 $(document).ready(function() {
	        $('.dio_datatable').dataTable({
	        	 'aaSorting': [[ 7, 'desc' ]] ,
	          	 "language": { 
	   		         url : "<?php echo base_url("application/views/backend/js/dataTables/i18n/chinese_traditional.lang.json")?>"
	             },
	             "iDisplayLength": 50
	         });
	    });

	
      function my_confirm(order_id ,product_speci_id){
         var chk_box=confirm('確定出貨?');
         if (chk_box == true) {
             location.href = '<?php echo base_url("index.php/backend/order/order_shipping?order_id=") ?>'+order_id;
         }
      }
    </script>

</body>
</html>
