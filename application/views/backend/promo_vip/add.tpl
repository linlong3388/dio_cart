<body>

<style>
.fa-star {
	color: red
}
td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'promo' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			
			 <?php if($this->session->flashdata('msg')){ ?>				 
		        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	         <?php } ?> 
			
			 <b style="color: red"><?php echo validation_errors(); ?></b> 
			
			
			<?php echo form_open( getCurrentFullUrl() )?>
				<table>
					<tr>
						<th colspan="2">新增資料</th>
					</tr>

					<tr>
						<th width="20%"><i class="fa fa-star"></i>名稱</th>
						<td>
						    <input class="fullwidth" placeholder="請輸入資料" type="text" name="name" value="<?php echo set_value('name')?>"> 
						</td>
					</tr>

					<tr>
						<th width="20%"><i class="fa fa-star"></i>代碼</th>
						<td>
						    <input class="fullwidth" placeholder="請輸入資料" type="text" name="code" value="<?php echo set_value('code')?>"> 
						</td>
					</tr>

					<tr>
						<th width="20%"><i class="fa fa-star"></i>類型</th>
						<td>
						    <select class="fullwidth" name="code_type">
						      <?php foreach (get_discount_coupon_status( 1,$query['code_type']) as $key=>$val) { ?>
						        <option value="<?php echo $key?>"><?php echo $val?></option>
							  <?php }?>
						    </select>
						</td>
					</tr>
					
					<tr>
					    <th width="20%"><i class="fa fa-star"></i>折扣</th>
						<td><input placeholder="請輸入資料" type="text" name="code_val" value="<?php echo set_value('code_val')?>" onkeypress="def_numeric()" maxlength="12">元  
						</td>
					</tr>
					
					<tr>
					    <th width="20%"><i class="fa fa-star"></i>總金額</th>
						<td><input placeholder="請輸入資料" type="text" name="total" value="<?php echo set_value('total')?>" onkeypress="def_numeric()" maxlength="12">元  						
						</td>
					</tr>
					
					<tr>
						<th width="20%">使用次數</th>
						<td>
						    <input class="fullwidth" placeholder="請輸入資料" type="text" name="use_count" value="<?php echo set_value('use_count')?>">
						</td>
					</tr>	
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>開始日期</th>
						<td>
						    <input class="fullwidth dio_date" placeholder="請輸入資料" type="text" name="sdate" value="<?php echo set_value('sdate')?>">
						</td>
					</tr>															

					<tr>
						<th width="20%"><i class="fa fa-star"></i>結束日期</th>
						<td>
						    <input class="fullwidth dio_date" placeholder="請輸入資料" type="text" name="edate" value="<?php echo set_value('edate')?>">
						</td>
					</tr>	

				</table>

				<div class="buttons">
					<input type="submit" value="送出" />
					<button type="button" onclick="javascript:location.href='<?php echo getAdminURL('promo/discount_coupon_lists')?>'">返回</button>
				</div>

				</form>
			</div>

		</div>

	</div>

<link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>" rel="stylesheet">

<script src="<?php echo base_url('application/views/backend/js/jquery-1.7.2.min.js');?>"></script>
<script src="<?php echo base_url('application/views/backend/js/jquery-ui-1.8.21.custom.min.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_fields_date.js')?>"></script>
 
</body>
</html>
