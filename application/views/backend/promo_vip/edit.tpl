<body>

<style>
.fa-star {
	color: red
}
td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'promo_vip' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			
			 <?php if($this->session->flashdata('msg')){ ?>				 
		        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	         <?php } ?> 
			
			 <b style="color: red"><?php echo validation_errors(); ?></b> 
			
			<?php echo form_open( getCurrentFullUrl() )?>
				<table>
					<tr>
						<th colspan="2">編輯資料</th>
					</tr>

					<tr>
						<th width="20%"><i class="fa fa-star"></i>開始日期</th>
						<td><input class="fullwidth dio_date" type="text" name="sdate"
							value="<?php echo isset($query['sdate']) ? $query['sdate'] : set_value('sdate')?>"> 
						</td>
					</tr>

					<tr>
						<th width="20%"><i class="fa fa-star"></i>結束日期</th>
						<td><input class="fullwidth dio_date" type="text" name="edate"
							value="<?php echo isset($query['edate']) ? $query['edate'] : set_value('edate')?>"> 
						</td>
					</tr>

					<tr>
						<th><i class="fa fa-star"></i>滿額金額</th>
						<td><input type="text" name="total" value="<?php echo isset($query['total']) ? $query['total'] : set_value('total')?>"
							onkeypress="def_numeric()" maxlength="12" />元 
						</td>
					</tr>			
					
					<tr>
						<th><i class="fa fa-star"></i>續約金額</th>
						<td><input type="text" name="ctotal" value="<?php echo isset($query['ctotal']) ? $query['ctotal'] : set_value('ctotal')?>"
							onkeypress="def_numeric()" maxlength="12" />元 
						</td>
					</tr>
					
					<tr>
						<th><i class="fa fa-star"></i>折扣</th>
						<td><input type="text" name="discount" value="<?php echo isset($query['discount']) ? $query['discount'] : set_value('discount') ?>"
							onkeypress="def_numeric()" maxlength="2" />%
						</td>
					</tr>
			
			        <!-- 
					<tr>
						<th width="20%"><i class="fa fa-star"></i>狀態</th>
						<td><input type="radio" name="status" value="1"
						    <?php //echo ($query['status'] == '1') ? ' checked' : '' ?>> 啟用
						    <input type="radio" name="status" value="0"
							<?php //echo ($query['status'] == '0') ? ' checked' : '' ?>> 停用
					    </td>
					</tr>
					 -->				
				
				</table>

				<div class="buttons">
					<input type="submit" value="送出" />
				</div>

				</form>
			</div>

		</div>

	</div>

	<script type="text/javascript">
    $(document).ready(function() {
        $('#dataTables-example').dataTable({
          	 "language": { 
   		         url : "<?php echo base_url("application/views/backend/js/dataTables/i18n/chinese_traditional.lang.json")?>"
             }
         });
    });

    function my_confirm(promo_vip_id){
       var chk_box=confirm('確定刪除資料?');
       if (chk_box == true) {
             location.href = '<?php echo base_url("index.php/backend/promo_vip/discount_coupon_del?promo_vip_id=") ?>'+promo_vip_id;
       }
    }
</script>


<link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>" rel="stylesheet">

<script src="<?php echo base_url('application/views/backend/js/jquery-1.7.2.min.js');?>"></script>
<script src="<?php echo base_url('application/views/backend/js/jquery-ui-1.8.21.custom.min.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_fields_date.js')?>"></script>


</body>
</html>

