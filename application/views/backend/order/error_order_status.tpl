<body>

	<style>
.fa-star {
	color: red
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'order' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
				<table>
					<tr>
						<th colspan="2">訂單異動失敗</th>
					</tr>
					<tr>
						<th colspan="2"><p>
								抱歉! 中信編號 <b style="color: blue"><?php echo $this->input->get('order_detail_id')?>
								</b> 的訂單狀態異動失敗!
							</p>
							<p>如果是[已取消]狀態的設定，請確定該筆訂單的訂購日期是否已過時。</p>
							<p>(每日晚上 20:00 以前才可進行取消當日的訂單，超過此時間，請改由人工作業，謝謝您)</p>
							<p>
								<a
									href="<?php echo getAdminURL('order/lists')?>">[回
									訂單列表 ]</a>
							</p></th>
					</tr>
				</table>

			</div>

		</div>

	</div>

</body>
</html>
