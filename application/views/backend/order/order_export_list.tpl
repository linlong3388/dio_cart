<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'order' ,getAdminMenu())?></h2></div>

		<div id="content">
		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">

				<!-- (一)搜尋主體 -->
			<?php echo form_open(current_url())?>
				<fieldset>
					<legend>搜尋條件: </legend>
					訂購日期 :<input type="text" name="srh_cdate1" class="dio_date"
						value="<?php echo !empty($srh_param['srh_cdate1']) ? $srh_param['srh_cdate1'] : ''?>">
					~<input type="text" name="srh_cdate2" class="dio_date"
						value="<?php echo !empty($srh_param['srh_cdate2']) ? $srh_param['srh_cdate2'] : ''?>">
					, 訂單狀態:<select name="srh_status">
					<?php if(!(empty($srh_param['srh_status']) && $srh_param['srh_status'] !== '0') ){ ?>
						<option value="<?php echo $srh_param['srh_status']?>">
						<?php echo get_order_status($srh_param['srh_status']) ?>
						</option>
						<?php } ?>
						<option value=''>不拘</option>
						<?php foreach (get_order_status('-1') as $key=>$val) {
							if($key != $srh_param['srh_status']){
								?>
						<option value="<?php echo $key?>">
						<?php echo $val?>
						</option>
						<?php  }
						}
						?>
					</select> <input type="submit" value="查詢"> <input type="button"
						onclick="export_cvs()" value="匯出csv">
				</fieldset>
				</form>

				<p></p>

				<b style="color: red"><?php echo form_error('srh_cdate1')?> </b> <b
					style="color: red"><?php echo form_error('srh_cdate2')?> </b>

				<!-- (二)列表主體 -->
				<div class="clearfix">
					<table class="dio_datatable">
						<thead>
							<tr>
								<td>訂單日期</td>
								<td>中信編號</td>
								<td>訂單編號</td>
								<td>收件人</td>
								<td>金額</td>
								<td>點數</td>
								<td>分期</td>
								<td>數量</td>
								<td>貨號</td>
								<!-- <td>功能</td>  -->
							</tr>
						</thead>

						<tbody>
						<?php if(isset($query)) { ?>
						<?php foreach ($query as $row) { ?>
							<tr>
								<td><?php echo substr($row['cdate'], 0,10) ?><b style="display: none"><?php echo substr($row['cdate'], 11) ?></td>
								<td><?php echo $row['order_detail_id'] ?>
								</td>
								<td><?php echo $row['order_detail_show_id'] ?>
								</td>
								<td><?php echo $row['reci_name'] ?>
								</td>
								<td><?php if($row['pay_method'] == '3' || $row['pay_method'] == '6'){ 
									echo addCommas($row['product_total']);
								}else{
									echo !empty($row['money']) ? $row['money'] : '';
								}
								?></td>
								<td><?php echo $row['bonus']?>
								</td>
								<td><?php echo $row['pay_method']?>
								</td>
								<td><?php echo $row['entity'] ?>
								</td>
								<td><?php echo $row['shipping_id'] ?>
								</td>
								<!-- 
								<td>
								   <a class="btn btn-success" href="<?php echo getAdminURL('product/order_status_edit?order_detail_id='.$row['order_detail_id']) ?>">
								     <i class="fa fa-pencil-square-o"></i>檢視</a> 
								</td>
								 -->
							</tr>
							<?php } ?>
							<?php } ?>
						</tbody>

					</table>
				</div>



			</div>
		</div>

	</div>

	<link
		href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css');?>"
		rel="stylesheet">
	<script
		src="<?php echo base_url('application/views/backend/js/jquery-ui-1.11.1.min.js');?>"></script>
	<script
		src="<?php echo base_url('application/views//backend/js/dio_plugins/jquery_fields_date.js');?>"></script>

	<script>
    $(document).ready(function() {
        $('.dio_datatable').dataTable({
        	 //"ajax": "<?php echo getAdminURL('product/order_list_ajax') ?>" ,
          	 "language": { 
   		         url : "<?php echo base_url("application/views/backend/js/dataTables/i18n/chinese_traditional.lang.json")?>" 
             },
             "iDisplayLength": 100,
             "order": [[ 0, "desc" ]]
         });
    });
    </script>

	<script type="text/javascript">
    <!--
    function export_cvs(){

       var url = '';
       var srh_cdate1 = $('input[name="srh_cdate1"]').val();
       var srh_cdate2 = $('input[name="srh_cdate2"]').val();
       var srh_status = $('select[name="srh_status"]').val();	    

       if(is_string_empty(srh_cdate1) || is_string_empty(srh_cdate2)){
            alert('*日期區間不可空白!');
            return false;
       }

       url = '<?php echo base_url("index.php/backend/product/order_export_excel") ?>' + '?srh_cdate1='+ srh_cdate1 + '&srh_cdate2=' + srh_cdate2 + '&srh_status=' + srh_status;
       location.href = url;
       
    }
    //-->
    </script>

</body>
</html>
