<body>

<style>
#orderDetail th {
	background: #45B6AF;
}

#orderDetail td {
	text-align: right;
}

td {
    text-align: left;
}

.fa-star {
	color: red
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'order' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			
			<?php if($this->session->flashdata('msg')){ ?>				 
		       <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	        <?php } ?>			
			
			<?php echo form_open("backend/order/view?".$_SERVER["QUERY_STRING"])?>
				<table>
					<tr>
						<th colspan="2">檢視資料</th>
					</tr>
					
					<tr>
						<th>訂單編號</th>
					    <input style="display: none" type="text" name="order_id" value="<?php echo $query['order_id']?>" />
						<td><?php echo $query['order_show_id']?></td>
					</tr>
					
					<tr>
						<th>訂單來源</th>
						<td><?php echo empty($query['b_title']) ? '網頁下單' : $query['b_title']?></td>
					</tr>

					<tr>
						<th>狀態</th>
						<td>
						   <select name="status" style="width: 15%">
						     <?php foreach( reSortArrayMutil(3 ,$query['status'] ,getOrderStatus()) as $key=>$val ) { ?>
							     <option value="<?php echo $key?>"><?php echo $val?></option>
						      <?php } ?>
						    </select> 
						  <input type="submit" class="btn btn-primary" value="變更狀態">
						
						</td>
					</tr>

					<!-- 
					<tr>
						<th>配送方式</th>
						<td><?php //echo reSortArrayMutil(2,$query['trans_method'],getTransMethod())?></td>
					</tr>
					 -->
					
					<?php if($query['trans_method'] == 0) { ?>
					<tr>
						<th>收件人姓名</th>
						<td><?php echo $query['name']?></td>
					</tr>

					<tr>
						<th>收件人手機</th>
						<td><?php echo $query['mobile']?></td>
					</tr>
				
					<tr>
						<th>收件人電話</th>
						<td><?php echo $query['phone']?></td>
					</tr>

					<!-- 
					<tr>
						<th>收件人Email</th>
						<td><?php //echo $query['email']?></td>
					</tr>
					 -->

					<tr>
						<th>收件人地址</th>
						<td><?php echo $query['local'].' / '.$query['address']?></td>
					</tr>
					
					<?php } ?>

					<tr>
						<th>付款方式</th>
						<td><?php echo reSortArrayMutil(2,$query['pay_method'],getPayMethod())?></td>
					</tr>
					
					<?php if($query['pay_method'] == 5){ ?>
					<tr>
						<th>付款資訊</th>
						<td><?php echo $query['pay_method_content'] ?></td>
					</tr> 
				   <?php } ?>
				
					
					<!--
					  <tr>
						  <th>匯款帳號後五碼</th>
						  <td><input type="text" name="pay_atm_no" value="<?php //echo $query['pay_atm_no']?>" maxlength="5" /></td>
					  </tr>
					-->
				
					<!-- 
					<tr>
						<th>配送地區</th>
						<td><?php //echo reSortArrayMutil(2,$query['zip'],getTransZip())?></td>
					</tr>
					 -->
				
					<tr>
						<th>總計</th>
						<td><?php echo DIO_CURRENCY.addCommas($query['total'])?></td>
					</tr>
					
					<!-- 
					<tr>
						<th>預計到貨日</th>
						<td><?php //echo ($query['date_arrival'] == '0000-00-00') ? '' : $query['date_arrival']?></td>
					</tr>
										
					<tr>
						<th>預計到貨時間</th>
						<td><?php //echo ($query['time_arrival'] == '99') ? '' : reSortArrayMutil(2 ,$query['time_arrival'] ,getTimeArrival())?></td>
					</tr>
					 -->
					
					<tr>
						<th>備註</th>
						<td><?php echo $query['memo']?></td>
					</tr>

					<tr>
						<th>建立日期</th>
						<td><?php echo $query['cdate']?></td>
					</tr>

					<tr>
						<th colspan="2">訂購人資訊</th>
					</tr>

					<tr>
						<th>帳號</th>
						<td><a href="<?php echo getAdminURL('customer/edit?customer_id='.$query['customer_id'])?>" target="_new" class="btn btn-success"><?php echo $query['username']?></a></td>
					</tr>

					<tr>
						<th>姓名</th>
						<td><?php echo $query['o_name']?></td>
					</tr>

				    <tr>
						<th>Email</th>
						<td><?php echo $query['o_email']?></td>
					</tr>
					
					<?php if( $query['pay_method'] == 2 || $query['pay_method'] == 3) { ?>
					
					<tr>
						<th colspan="2">超商資訊</th>
					</tr>
					
					<tr>
						<th>店舖編號</th>
						<td><?php echo $query['cvs_store_id']?></td>
					</tr>
					
					<tr>
						<th>店舖名稱</th>
						<td><?php echo $query['cvs_store_name']?></td>
					</tr>
					
					<tr>
						<th>店舖地址</th>
						<td><?php echo $query['cvs_address']?></td>
					</tr>
					
					<tr>
						<th>店舖電話</th>
						<td><?php echo $query['cvs_telephone']?></td>
					</tr>
					
					<tr>
						<th>額外資訊</th>
						<td><?php echo $query['cvs_extra_data']?></td>
					</tr>																														
					
					<?php } ?>
					

					<?php if( $query['pay_method'] == 2 || $query['pay_method'] == 3) { ?>
					
					<tr>
						<th colspan="2">物流資訊</th>
					</tr>
					
					<tr>
						<th>交易編號</th>
						<td><?php echo $query['logistics_no']?></td>
					</tr>
					
					<!-- 
					<tr>
						<th>狀態碼</th>
						<td><?php //echo $query['logistics_status_code']?></td>
					</tr>
					 -->
					
					<tr>
						<th>狀態訊息</th>
						<td><?php echo $query['logistics_status_msg']?></td>
					</tr>
					
					<tr>
						<th>狀態更新時間</th>
						<td><?php echo $query['logistics_status_udate']?></td>
					</tr>
					
					<tr>
						<th>寄貨編號</th>
						<td><?php echo $query['logistics_payment_no']?></td>
					</tr>																														
					
					<tr>
						<th>驗證碼</th>
						<td><?php echo $query['logistics_validation_no']?></td>
					</tr>
					
					<?php } ?>
					
					
					<!-- 
					<tr>
						<th colspan="2">物流資訊</th>
					</tr>	
					
					<tr>
						<th>貨號</th>
						<td><input type="text" name="trans_no" value="<?php //echo isset($query['trans_no']) ? $query['trans_no'] : set_value('trans_no')?>"/></td>
					</tr>	
					 -->
					 
					<tr>
						<th colspan="2">發票資訊</th>
					</tr>	
					
					<tr>
						<th>發票類型</th>
						<td><input type="text" name="ticket_type" value="<?php echo reSortArrayMutil(2,$query['ticket_type'] ,getTicketType())?>"/></td>
					</tr>	
				
					<?php if( !empty($query['ticket_type']) ){ ?>
					  <tr>
						  <th>發票抬頭</th>
						  <td><input type="text" name="ticket3_title" value="<?php echo isset($query['ticket3_title']) ? $query['ticket3_title'] : set_value('ticket3_title')?>"/></td>
					  </tr>	
					
				  	  <tr>
						  <th>統一編號</th>
						  <td><input type="text" name="ticket3_unified" value="<?php echo isset($query['ticket3_unified']) ? $query['ticket3_unified'] : set_value('ticket3_unified')?>"/></td>
					  </tr>	
					<?php } ?>
					
					<tr>
						<th>郵寄地址</th>
						<td><?php echo $query['ticket_addr']?></td>
					</tr>
					
					
					<tr>
						<th colspan="2">系統管理</th>
					</tr>	
					
					<tr>
						<th>管理人員備註</th>
						<td><textarea name="memo_admin" rows="10" class="fullwidth"><?php echo isset($query['memo_admin']) ? $query['memo_admin'] : set_value('memo_admin')?></textarea></td>
					</tr>
					
					<tr>
						<th></th>
						<td><div class="buttons">
					         <input type="submit" value="送出" />
					         <button type="button" onclick="javascript:location.href='<?php echo getAdminURL('order/lists?page=1')?>'">返回</button>
				            </div>
						</td>
					</tr>					
					
					<tr>
						<th colspan="2">訂單明細</th>
					</tr>

					<!-- 
					<tr>
						<th>幣別</th>
						<td><?php //echo DIO_CURRENCY//$query['currency']?></td>
					</tr>
					 -->
					
					<tr>
						<th>明細資料</th>
						<td><div id="show_err"></div>
							<table id="orderDetail" >
								<thead>
									<tr>
										<th>圖示</th>
										<th>名稱</th>
										<th>功能</th>
										<th>單價</th>
										<th>數量</th>
										<th>小計</th>
									</tr>
								</thead>

								<tbody>
								<?php foreach ($query_detail as $row){  ?>
									<tr>
										<td>
										   <!-- 
										   <a href="<?php echo base_url("index.php/backend/product/edit?product_id=".$row['product_id'])?>" target="_new"> 
											  <img width="50px" height="50px" src="<?php echo base_url('resources/uploads/'.$row['image'])?>">
										   </a>
										    -->
										    <a href="<?php echo base_url("index.php/backend/product/edit?product_id=".$row['product_id'])?>" target="_new"> 
											  <img width="50px" height="50px" src="<?php echo getAdminImg($row['image'])?>">
										   </a>
										</td>
										<td align="left"><a href="<?php echo base_url("index.php/backend/product/edit?product_id=".$row['product_id'])?>"
											target="_new"> <?php echo $row['name']?><br> <?php echo !empty($row['attr1']) ? $row['attr1'] : ''?> <?php echo !empty($row['attr2']) ? ' / '.$row['attr2'] : ''?> <br> <?php echo $row['sku2']?></a>
										</td>
										<td>
										    <?php if( ($query['status'] == 2) && !isCategory_3( $row['category_multi_id'] ) ){ ?>
										         <button type="button" onclick="javascript:location.href='<?php echo getAdminURL('order/order_return?order_detail_id='.$row['order_detail_id'])?>'" >退換</button>
										    <?php }else{ ?> 
										         -- 
										    <?php } ?>
										</td>										
										<td><?php echo addCommas($row['price'])?></td>
										<td><?php echo $row['entity'] ?></td>
										<td><?php echo addCommas($row['total'])?></td>
									</tr>
									<?php } ?>
								</tbody>

								<tfoot>
								  <?php foreach ($query_promo as $row){ ?>
								     <tr <?php echo ($row['item_type'] == '-') ? ' style="color:red"' : "" ?> >
										  <td colspan="4"><?php echo $row['name']?>(<?php echo $row['item_type']?>)</td>
										  <td colspan="2" ><?php echo DIO_CURRENCY.addCommas($row['total'])?></td>
									 </tr>
			                      <?php } ?>
										<td colspan="4" >總計:</td>
										<td colspan="2" ><b style="font-size: 16px"><?php echo DIO_CURRENCY.addCommas($query['total'])?></b></td>
								   </tr>
								</tfoot>
							</table>
						 </td>
					</tr>

				</table>

				</form>
			</div>

		</div>
	</div>
	
   <?php $this->load->view('backend/order/widget/dlg_detail_edit.tpl')?>	

</body>
</html>
