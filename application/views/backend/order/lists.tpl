<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'order' ,getAdminMenu())?></h2></div>

		<div id="content">
		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">

				<!-- (一)搜尋主體 -->
			<?php echo form_open(getCurrentFullUrl()  ,array('method'=>'get'))?>
				<fieldset>
					<legend>搜尋條件:</legend>
                      <input style="display:none" type="text" name="page" value="1"> 
                                                     訂單編號(%) : <input type="text" name="srh_order_show_id" value="<?php echo $this->input->get('srh_order_show_id')?>" /> 
					會員ID : <input type="text" name="srh_customer_id" value="<?php echo $this->input->get('srh_customer_id')?>" /> 
					訂購人(%) : <input type="text" name="srh_o_name" value="<?php echo $this->input->get('srh_o_name')?>" /> 
					
					<p>
					
					訂單期間 : 
					  <input type="text" name="srh_order_cdate1" class="dio_date" value="<?php echo $this->input->get('srh_order_cdate1')?>">
					 ~<input type="text" name="srh_order_cdate2" class="dio_date" value="<?php echo $this->input->get('srh_order_cdate2')?>">
					
					<p>
					
					<!-- 
					分店 : 
					<select name="srh_branch_id">
					   <?php //foreach ( reSortArrayMutil(4,$this->input->get('srh_branch_id') ,transKeyPairArray(getBranch(), 'branch_id', 'title'))  as $key=>$val) { ?>
						<option value="<?php //echo $key?>">
						<?php //echo $val?>
						</option>
						<?php //} ?>
					</select>
					 -->
					
					訂單狀態 : 
					<select name="srh_status">
					   <?php foreach (get_order_status(4,$this->input->get('srh_status')) as $key=>$val) { ?>
						<option value="<?php echo $key?>">
						<?php echo $val?>
						</option>
						<?php } ?>
					</select>

					排序 : 
					   <select name="srh_sort">
						  <?php foreach (reSortArray($this->input->get('srh_sort') ,$data_sort) as $key=>$val) { ?>
							<option value="<?php echo $key?>">
							   <?php echo $val?>
							</option>
							<?php } ?>
						</select> 
						
						<input type="radio" name="srh_sort_type" value="DESC"
						<?php echo $this->input->get('srh_sort_type') == 'DESC' ? " checked" : "" ?> />降冪
						<input type="radio" name="srh_sort_type" value="ASC"
						<?php echo $this->input->get('srh_sort_type') == 'ASC' ? " checked" : "" ?> />升冪
                     
					<input type="submit" value="查詢">
					<input type="submit" name="submit_output" value="資料匯出">
					
				</fieldset>
				
				<fieldset>
					<legend>快捷功能:</legend>
					<input type="button" onclick="multi_status_0()" value="已出貨" />
				</fieldset>	
				
				</form>

				<p></p>

				<b style="color: red"><?php echo form_error('srh_cdate1')?> </b> 
				<b style="color: red"><?php echo form_error('srh_cdate2')?> </b>

				<!-- (二)列表主體 -->
				<div class="clearfix">

					<table class="dio_datatable">
						<thead>
							<tr>
							    <th><input type="checkbox" id="selectAll" onclick="selectAll()" /></th>
								<td>訂單編號</td>
								<td>訂單來源</td>
								<td>訂購人</td>
								<!-- <td>配送方式</td> -->
								<td>收件人</td>
								<td>總額</td>
								<td>訂單狀態</td>
								<!-- <td>預購單</td> -->
								<td>訂購日期</td>
								<td>功能</td>
							</tr>
						</thead>

						<tbody>
						<?php foreach ($query as $row) { ?>
							<tr>
								<td>
							        <?php if($row['status'] == 1){ ?>
							           <input type="checkbox" />
								    <?php } ?>
								       <input style="display: none" type="text" value="<?php echo $row['order_id'] ?>" />
								</td>
								<td><?php echo $row['order_show_id']?></td>
								<td><?php echo empty($row['b_title']) ? '網頁下單' : $row['b_title']?></td>
								<td><?php echo $row['o_name']?></td>
								<!-- 
								<td><?php //echo mb_substr( reSortArrayMutil(2,$row['trans_method'],getTransMethod()), 0, 4,"utf-8")?></td>
								 -->
								<td><?php echo empty($row['name']) ? '--' : $row['name']?></td>
								<td><?php echo DIO_CURRENCY.addCommas($row['total'])?></td>
								<td><?php echo reSortArrayMutil(2 ,$row['status'],getOrderStatus())?></td>
								<!-- <td><?php //echo empty($row['order_type']) ? '否' : "<b style='color:red'>是</b>"?></td> -->
								<td><?php echo substr($row['cdate'], 0,10) ?><b style="display: none"><?php echo substr($row['cdate'], 11) ?></td>
								<td><a class="btn btn-success"
									href="<?php echo getAdminURL('order/view?order_id='.$row['order_id']) ?>">
										<i class="fa fa-pencil-square-o"></i>檢視</a> <?php if($row['status'] == '5') { ?>
									<!-- <a class="btn btn-info" href="javascript:my_confirm(<?php echo $row['order_id']?>);">  -->
									<a class="btn btn-info"
									href="<?php echo getAdminURL('order/order_shipping?'.$_SERVER["QUERY_STRING"].'&order_id='.$row['order_id']) ?>">
										<i class="fa fa-pencil-square-o"></i>出貨</a> <?php } ?></td>
							</tr>
							<?php } ?>
						</tbody>

					</table>

					<div class="pageNum">
						<span>顯示第<?php echo $page_startEnd['page_start']?> 至 <?php echo $page_startEnd['page_end']?>
							項結果，共 <?php echo $query_group_by[0]['count']?> 項 </span> <span
							style="float: right;"><?php echo $pages->display_pages()?>
						</span>
					</div>


				</div>


			</div>

		</div>

	</div>

	<link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>" rel="stylesheet">
	<script src="<?php echo base_url('application/views/backend/js/jquery-ui-1.11.1.min.js')?>"></script>
	<script src="<?php echo base_url('application/views//backend/js/dio_plugins/jquery_fields_date.js')?>"></script>

	<script type="text/javascript">
      function my_confirm(order_id ,product_speci_id){
         var chk_box=confirm('確定出貨?');
         if (chk_box == true) {
             location.href = '<?php echo base_url("index.php/backend/order/order_shipping?order_id=") ?>'+order_id;
         }
      }

      //-------------------------------------------------------------
      //
      // 全選&全不選
      //
      //-------------------------------------------------------------
      $('#selectAll').click(function (e) {
          $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
      });


      //-------------------------------------------------------------
      //
      // 多筆[確認訂單]& 多筆[取消]
      //  
      //-------------------------------------------------------------
      function multi_status_0(){
         var aryList = new Array();

         $('table tbody').find('input[type="checkbox"]:checked').each(function (i) {
      	   aryList.push($(this).next().val());
         });

         if(aryList.length <= 0){
            alert('您尚未勾選項目!');
         }else{
            var chk_box=confirm('確定改變訂單狀態為已出貨?');
      	  if (chk_box == true) {
      	      location.href = '<?php echo getAdminURL("order/multi_order_status_2?aryList=")?>' + aryList;
      	  }	      
         }   
      } 
            
    </script>

</body>
</html>
