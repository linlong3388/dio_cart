<body>

	<style>
.page_wrap--data_area--main_buyer--table tr th {
	text-align: center;
}
</style>

	<div id="wrapper">

	<?php $this->load->view("backend/common/navbar_logo.tpl"); ?>

		<div id="page_wrap">
			<div class="page_wrap--member_area"></div>
		</div>

		<?php $this->load->view("backend/common/navbar_side.tpl"); ?>

		<div class="page_wrap--member_area_content">
			<div class="panel panel-default">
				<div class="panel-heading" style="background-color: #066;">
					<span style="color: #fff">待處理訂單 / 出貨
						<button type="button" class="btn btn-primary"
							onclick="javascript:window.history.back()">返回</button> </span>
				</div>
			</div>

			<div class="panel-body">

			<?php echo form_open("backend/product/order_shipping?".$_SERVER["QUERY_STRING"]."order_detail_id=".$query['order_detail_id'])?>

				<table border="1" class="page_wrap--data_area--main_buyer--table">
					<tr>
						<th colspan="2">出貨單號編輯</th>
					</tr>

					<tr>
						<th>訂單編號</th>
						<td><input type="text" name="order_detail_show_id"
							value="<?php echo isset($query['order_detail_show_id']) ? $query['order_detail_show_id'] : set_value('order_detail_show_id') ?>"
							style="width: 20%; background-color: #eee;" readonly /> <input
							type="text" name="order_detail_id"
							value="<?php echo isset($query['order_detail_id']) ? $query['order_detail_id'] : set_value('order_detail_id') ?>"
							style="display: none" readonly />
						</td>
					</tr>

					<tr>
						<th>出貨單號</th>
						<td><input type="text" name="shipping_id"
							value="<?php echo isset($query['shipping_id']) ? $query['shipping_id'] : set_value('shipping_id') ?>"
							maxlength="20" style="width: 20%" /> <b style="color: red"><?php echo form_error('shipping_id')?>
						</b> <input type="submit" class="btn btn-primary" value="送出">
						</td>
					</tr>

				</table>

				</form>

			</div>

		</div>

	</div>

</body>
</html>
