<body>

	<style>
.page_wrap--data_area--main_buyer--table tr th {
	text-align: center;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'order' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			<?php echo form_open("backend/order/order_status_edit?".$_SERVER["QUERY_STRING"]."&order_detail_id=".$query['order_detail_id'])?>
				<table>
					<tr>
						<th colspan="2">編輯資料</th>
					</tr>

					<tr>
						<th>訂單編號</th>
						<td><?php echo $query['order_detail_show_id']?>
						</td>
					</tr>

					<tr>
						<th>狀態</th>
						<td><select name="status" style="width: 15%">
								<option value="<?php echo $query['status']?>">
								<?php echo get_order_status($query['status'])?>
								</option>
								<?php foreach (get_order_status() as $key=>$val) {
									if($query['status'] == '1'){ //已授權狀態
										$enable_status = array(0,5);
										if(in_array($key ,$enable_status) && ( $key != $query['status']) ){ ?>
								<option value="<?php echo $key?>">
								<?php echo $val?>
								</option>
								<?php
										}
									}else{
										$enable_status = array(0,2,4,5);
										if(in_array($key ,$enable_status) && ( $key != $query['status']) ){
											?>
								<option value="<?php echo $key?>">
								<?php echo $val?>
								</option>
								<?php      }
									}
								}
								?>
						</select> <input type="submit" class="btn btn-primary"
							value="變更狀態"> <?php if($query['status'] == '1'){ ?> <b
							style="color: red">*說明: 授權成功，但請款失敗! 請至中信後台確認並調整!</b> <?php } ?>
						</td>
					</tr>

					<tr>
						<th>收件人姓名</th>
						<td><?php echo $query['reci_name']?>
						</td>
					</tr>

					<tr>
						<th>收件人手機</th>
						<td><?php echo $query['reci_mobile']?>
						</td>
					</tr>

					<tr>
						<th>收件人電話</th>
						<td><?php echo $query['reci_phone']?>
						</td>
					</tr>

					<tr>
						<th>收件人Email</th>
						<td><?php echo $query['reci_email']?>
						</td>
					</tr>

					<tr>
						<th>收件人地址</th>
						<td><?php echo $query['reci_local']?> / <?php echo $query['reci_address']?>
						</td>
					</tr>

					<tr>
						<th>配送方式</th>
						<td><?php echo $query['trans_method']?>
						</td>
					</tr>

					<tr>
						<th>數量</th>
						<td><?php echo $query['entity']?>
						</td>
					</tr>

					<tr>
						<th>售價</th>
						<td><?php echo '$'.addCommas($query['product_price'])?>
						</td>
					</tr>

					<tr>
						<th>小計</th>
						<td><?php echo '$'.addCommas($query['product_total'])?>
						</td>
					</tr>

					<tr>
						<th>建立日期</th>
						<td><?php echo $query['cdate']?>
						</td>
					</tr>

					<tr>
						<th colspan="2">商品資訊</th>
					</tr>

					<tr>
						<th>商品編號</th>
						<td><a style="color: blue; text-decoration: underline;"
							href="<?php echo getAdminURL('product/edit?product_id='.$query['product_id'])?>"
							target="_blank"><?php echo sprintf('%05d',$query['product_id']) ?>
						</a></td>
					</tr>

					<tr>
						<th>商品名稱</th>
						<td><a style="color: blue; text-decoration: underline;"
							href="<?php echo getAdminURL('product/edit?product_id='.$query['product_id'])?>"
							target="_blank"><?php echo $query['product_name'] ?> </a></td>
					</tr>

					<tr>
						<th>商品型號</th>
						<td><?php echo $query['product_model']?>
						</td>
					</tr>

					<tr>
						<th>商品規格</th>
						<td><?php echo $query['product_specific']?>
						</td>
					</tr>


					<tr>
						<th colspan="2">付款方式</th>
					</tr>

					<tr>
						<th>付款方式</th>
						<td><?php //echo $query['pay_method']?>
						</td>
					</tr>

					<tr>
						<th>點</th>
						<td><?php echo !empty($pay_method_ary['bonus']) ? $pay_method_ary['bonus'] : '' ?>
						</td>
					</tr>

					<tr>
						<th>金</th>
						<td><?php echo !empty($pay_method_ary['money']) ? $pay_method_ary['money'] : '' ?>
						</td>
					</tr>

					<tr>
						<th colspan="2">發票資訊</th>
					</tr>

					<!-- 
                    <tr>
						<th>發票類型</th>
						<td><?php echo $query['ticket_type'] ?> </td>
					</tr>
                     -->

					<?php if($query['ticket_type'] == '1') { ?>
					<tr>
						<th>買受人(二聯)</th>
						<td><?php echo $query['ticket2_buyer'] ?></td>
					</tr>
					<?php }elseif ($query['ticket_type'] == '2'){ ?>
					<tr>
						<th>統一編號(三聯)</th>
						<td><?php echo $query['ticket3_unified'] ?></td>
					</tr>

					<tr>
						<th>發票抬頭(三聯)</th>
						<td><?php echo $query['ticket3_title'] ?></td>
					</tr>
					<?php } ?>

					<tr>
						<th colspan="2">訂購人資訊</th>
					</tr>

					<tr>
						<th>姓名</th>
						<td><?php echo !empty($query_customer['name']) ? $query_customer['name'] : '' ?>
						</td>
					</tr>

					<tr>
						<th>帳號(email)</th>
						<td><?php echo !empty($query_customer['email']) ? $query_customer['email'] : '' ?>
						</td>
					</tr>

					<tr>
						<th>手機</th>
						<td><?php echo !empty($query_customer['mobile']) ? $query_customer['mobile'] : '' ?>
						</td>
					</tr>

				</table>

				<div class="buttons">
					<input type="submit" value="送出" />
					<button type="button" onclick="javascript:window.history.back()">返回</button>
				</div>

				</form>
			</div>

		</div>
	</div>

</body>
</html>
