<body>

	<style>
.fa-star {
	color: red
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'order' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			<?php echo form_open("backend/product/order_shipped_edit_status")?>
				<table>
					<tr>
						<th colspan="2">出貨列表 /編輯</th>
					</tr>

					<tr>
						<th width="20%">訂單編號</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text"
							name="order_detail_id"
							value="<?php echo isset($query['order_detail_id']) ? $query['order_detail_id'] : set_value('order_detail_id')?>"
							readonly> <b style="color: red"><?php echo form_error('order_detail_id')?>
						</b></td>
					</tr>

					<tr>
						<th width="20%">出貨單號</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text"
							name="shipping_id"
							value="<?php echo isset($query['shipping_id']) ? $query['shipping_id'] : set_value('shipping_id')?>"
							readonly> <b style="color: red"><?php echo form_error('shipping_id')?>
						</b></td>
					</tr>

				</table>

				<div class="buttons">
					<input type="submit" value="送出" />
					<button type="button"
						onclick="javascript:location.href='<?php echo getAdminURL('order/order_shipped_list')?>'">返回</button>
				</div>

				</form>
			</div>

		</div>

	</div>

</body>
</html>
