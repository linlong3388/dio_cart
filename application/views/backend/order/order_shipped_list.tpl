<body>


	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'order' ,getAdminMenu())?></h2></div>

		<div id="content">
		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">

				<!-- (一)搜尋主體 -->
			<?php echo form_open("backend/order/order_shipped_list")?>
				<fieldset>
					<legend>搜尋條件:</legend>
					訂單期間 : <input type="text" name="srh_cdate1" class="dio_date"
						value="<?php echo !empty($srh_param['srh_cdate1']) ? $srh_param['srh_cdate1'] : ''?>">
					~<input type="text" name="srh_cdate2" class="dio_date"
						value="<?php echo !empty($srh_param['srh_cdate2']) ? $srh_param['srh_cdate2'] : ''?>">
					, 訂單狀態 : <select name="srh_status">
					<?php if(!(empty($srh_param['srh_status']) && $srh_param['srh_status'] !== '0') ){ ?>
						<option value="<?php echo $srh_param['srh_status']?>">
						<?php echo get_order_status($srh_param['srh_status']) ?>
						</option>
						<?php } ?>
						<option value=''>不拘</option>
						<?php foreach (get_order_status('-1') as $key=>$val) {
							//if($key != $srh_param['srh_status']){
							?>
						<option value="<?php echo $key?>">
						<?php echo $val?>
						</option>
						<?php  //}
						}
						?>
					</select> <input type="submit" value="查詢">
				</fieldset>
				</form>

				<p></p>

				<b style="color: red"><?php echo form_error('srh_cdate1')?> </b> <b
					style="color: red"><?php echo form_error('srh_cdate2')?> </b>

				<!-- (二)列表主體 -->
				<div class="clearfix">
					<table class="dio_datatable">
						<thead>
							<tr>
								<td>訂單編號</td>
								<td>出貨單號</td>
								<td>訂購日期</td>
								<td>客戶名稱</td>
								<td>商品代號</td>
								<td>商品名稱</td>
								<td>總額</td>
								<td>訂單狀態</td>
								<td>功能</td>
							</tr>
						</thead>

						<tbody>
						<?php foreach ($query as $row) { ?>
							<tr>
								<td><?php echo $row['order_detail_show_id']?>
								</td>
								<td><?php echo $row['shipping_id']?>
								</td>
								<td><?php echo substr($row['cdate'], 0,10) ?><b style="display: none"><?php echo substr($row['cdate'], 11) ?></td>
								<td><?php echo get_assign_field('order', 'order_id', $row['order_id'], 'reci_name')?>
								</td>
								<td><?php echo $row['product_id']?>
								</td>
								<td><?php echo $row['product_name']?>
								</td>
								<td><?php //echo 'NT$'.addCommas($row['total_price'])?>
								</td>
								<td><?php echo get_order_status($row['status'])?>
								</td>
								<td><a class="btn btn-success"
									href="<?php echo getAdminURL('order/order_shipped_view?order_detail_id='.$row['order_detail_id']) ?>">
										<i class="fa fa-pencil-square-o"></i>檢視</a> <a
									class="btn btn-info"
									href="<?php echo getAdminURL('order/order_shipped_edit_status?order_detail_id='.$row['order_detail_id']) ?>">
										<i class="fa fa-pencil-square-o"></i>編輯</a></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>


			</div>

		</div>

	</div>


	<link
		href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>"
		rel="stylesheet">
	<script
		src="<?php echo base_url('application/views/backend/js/jquery-ui-1.11.1.min.js')?>"></script>
	<script
		src="<?php echo base_url('application/views//backend/js/dio_plugins/jquery_fields_date.js')?>"></script>

	<script>
    $(document).ready(function() {
        $('.dio_datatable').dataTable({
          	 "language": { 
   		         url : "<?php echo base_url("application/views/backend/js/dataTables/i18n/chinese_traditional.lang.json")?>" ,
   		      "dom": '<"top"i>rt<"bottom"flp><"clear">'
             },
             "iDisplayLength": 100,
             "order": [[ 0, "desc" ]]  
         });
    });
</script>

</body>
</html>
