<div id="dlg_detail_edit" style="display: none">
	<form>
		<table>
		
		  	<tr style="display: none">
				<td><i class="fa fa-star"></i>訂單明細編號 :</td>
				<td><input type="text" name="order_detail_id" readonly /></td>
			</tr>
		
  		    <tr>
				<td>圖片 :</td>
				<td><img width="50px" height="50px" id="dio_img"></td>
			</tr>

			<tr>
				<td>名稱 :</td>
				<td><input type="text" name="name" readonly /></td>
			</tr>

			<tr>
				<td><i class="fa fa-star"></i>單價 :</td>
				<td><input type="text" name="price" onchange="dlg_detail_total()" onkeypress="def_numeric()" maxlength="10" /></td>
			</tr>
			
		    <tr>
				<td><i class="fa fa-star"></i>數量 :</td>
				<td><input type="text" name="entity" onchange="dlg_detail_total()" onkeypress="def_numeric()" maxlength="10" /></td>
			</tr>
			
			<tr>
				<td>小計 :</td>
				<td><input type="text" name="total" onkeypress="def_numeric()" maxlength="10" readonly/></td>
			</tr>

		</table>
	</form>
</div>

<link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>" rel="stylesheet">
<script src="<?php echo base_url('application/views/backend/js/jquery-1.7.2.min.js');?>"></script>
<script src="<?php echo base_url('application/views/backend/js/jquery-ui-1.8.21.custom.min.js');?>"></script>

<script>

/********************************************
/* 名稱 : dlg_detail_total
/* 功能 : 計算總額 
*********************************************/
function dlg_detail_total(){
    var price  = $('#dlg_detail_edit input[name="price"]').val();
    var entity = $('#dlg_detail_edit input[name="entity"]').val();

    $('#dlg_detail_edit input[name="total"]').val((price*entity));
}


/********************************************
/* 名稱 : dlg_detail_edit
/* 功能 : 訂單項目 / 編輯 
*********************************************/
 function dlg_detail_edit(parmas){

	var parmas_ary = parmas.split(",");
	
	$('#dlg_detail_edit input[name="order_detail_id"]').val(parmas_ary[0]);
	$('#dlg_detail_edit #dio_img').attr('src' ,parmas_ary[1]);
	$('#dlg_detail_edit input[name="name"]').val(parmas_ary[2]);  
	$('#dlg_detail_edit input[name="price"]').val(parmas_ary[3]);
	$('#dlg_detail_edit input[name="entity"]').val(parmas_ary[4]);
	$('#dlg_detail_edit input[name="total"]').val(parmas_ary[5]);  

	
    //圖片
	//$('#dlg_detail_edit input[name="imageEdit"]').val(parmas_ary[2]);
	//$('#dlg_detail_edit #imageEdit').attr('src' ,'<?php //echo base_url("resources/uploads/")?>/'+parmas_ary[4]);
    $('#dlg_detail_edit #image').attr('src' ,'<?php echo base_url("resources/uploads/")?>/'+parmas_ary[2]);
    		
    $('#dlg_detail_edit').dialog({ 
		                  'title' : "項目編輯",
		                  'width' : "600" ,
		                 'height' : "450" ,
		                  buttons: [
		                            {
		                              text: "確認",
		                              click: function() {
		                            	  var order_detail_id = parmas_ary[0];
		          		                  var url = "<?php echo getAdminURL('order/edit_detail')?>";
		                                  $.post(url , {order_detail_id: order_detail_id ,data: $('#dlg_detail_edit form').serialize()} ,function(err_msg){
		                                       //參數驗證
		                                      if(!is_string_empty(err_msg)){
		                                    	  $('#show_err').empty().append("<div class='alert alert-danger'><i class='fa fa-times-circle'></i>"+err_msg+"</div>");
		                                      }else{
		                                    	  $('#show_err').empty().append("<div class='alert alert-success'><i class='fa fa-times-circle'></i>"+'資料庫異動成功'+"</div>");
                                                  location.reload();
					                          }
			                              }); 

                                    	  $(this).dialog( "close" );
                                          return true;
		                              }
		                            },
		                            {
		                              text: "取消", 
		                              click: function(){
		                              		 $(this).dialog( "close" );
		                                   }
		                             }
		                          ]	
    
    }).prev(".ui-widget-header").css("color","#BCC2CB").css("background","#111");
 }           


</script>
