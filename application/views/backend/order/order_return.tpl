<body>

<style>
.fa-star {
	color: red
}

td {
    text-align: left;
}
</style>

<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'order' ,getAdminMenu())?></h2></div>

  <div id="content">

  	<?php $this->load->view('backend/common/breadcrumb.tpl')?>
    	
  	<div class="container editform">
  	
  			<?php if($this->session->flashdata('msg')){ ?>				 
				  <div class="alert alert-success"><i class="fa fa-check-circle"></i><?php echo $this->session->flashdata('msg')?></div>
			<?php }elseif($this->session->flashdata('msg_err')){ ?>
				  <div class="alert alert-danger"><i class="fa fa-times-circle"></i> <?php echo $this->session->flashdata('msg_err')?></div>
			<?php } ?>	
  	
  	         <b style="color: red"><?php echo validation_errors(); ?></b> 
  	
			<?php echo form_open( getCurrentFullUrl() )?>
				<table>
					<tr>
						<th colspan="2">收件人資料</th>
					</tr>
					
					<tr style="display: none">
						<td><input type="text" name="order_detail_id" value="<?php echo $query['order_detail_id']?>" readonly/></td>
						<td><input type="text" name="customer_id" value="<?php echo $query['customer_id']?>" readonly/></td>
					</tr>
					
					<tr>
						<th width="20%">訂單編號</th>
						<td><input type="text" name="order_show_id" value="<?php echo $query['order_show_id']?>" readonly/></td>
					</tr>
	
					<tr>
						<th>收件人姓名</th>
						<td><!-- <input type="text" name="last_name" value="<?php //echo $query['last_name']?>" /> --> 
						    <input type="text" name="name" value="<?php echo $query['name']?>" />
						</td>
					</tr>

					<!-- 
					<tr>
						<th>收件人手機</th>
						<td><input type="text" name="mobile" value="<?php //echo $query['mobile']?>" /></td>
					</tr>
                     -->
					
					<tr>
						<th>收件人電話</th>
						<td><input type="text" name="phone" value="<?php echo $query['phone']?>" /></td>
					</tr>

					<tr>
						<th>收件人Email</th>
						<td><input type="text" name="email" value="<?php echo $query['email']?>" /></td>
					</tr>

					<tr>
						<th>收件人地址</th>
						<td>
						  <input type="text" style="width: auto;" name="local" value="<?php echo $query['local']?>" /> /
						  <input type="text" style="width: auto;" name="address" value="<?php echo $query['address']?>" />
						</td>
					</tr>

					<tr>
						<th>退貨日期</th>
						<td><input type="text" name="cdate" value="<?php echo $query['cdate']?>" readonly/></td>
					</tr>

					<tr>
						<th colspan="2">商品資料</th>
					</tr>

					<tr>
						<th>商品編號</th>
						<td><a class="btn btn-success"
							href="<?php echo getAdminURL('product/edit?product_id='.$query['product_id'])?>"
							target="_blank"> <?php echo $query['product_id']?> </a></td>
					</tr>

					<tr>
						<th>商品名稱</th>
						<td><a class="btn btn-success"
							href="<?php echo getAdminURL('product/edit?product_id='.$query['product_id'])?>"
							target="_blank"> <?php echo $query['p_name']?></a>
					    </td>
					</tr>
					
					<tr>
						<th>型號</th>
						<td><?php echo $query['p_sku']?></td>
					</tr>
					
					<tr>
						<th><i class="fa fa-star"></i>退(換)貨數量</th>
						<td><input type="text" name="entity" onchange="order_return_total()" value="1" /></td>
					</tr>

					<tr>
						<th><i class="fa fa-star"></i>售價</th>
						<td><input type="text" name="price" onchange="order_return_total()" value="<?php echo $query['price']?>" /></td>
					</tr>

					<tr>
						<th>小計</th>
						<td><input type="text" name="total" value="<?php echo $query['price']?>" readonly/></td>
					</tr>					
					
					<tr>
						<th><i class="fa fa-star"></i>退(換)貨原因</th>
						<td><textarea name="reason" rows="10" placeholder="請輸入退(換)貨原因..." class="fullwidth"><?php echo set_value('reason')?></textarea></td>
					</tr>					
					
				</table>

				<div class="buttons">
				    <input type="submit" value="送出">
				    <button type="button" onclick="javascript:location.href='<?php echo getAdminURL('order/view?order_id='.$query['order_id'])?>'">返回</button> 
				</div>                                
				                                              
				</form>
   
			
	</div>

 </div>

</div>

<script>
/********************************************
/* 名稱 : order_return_total
/* 功能 : 計算總額 
*********************************************/
function order_return_total(){
    var price  = $('input[name="price"]').val();
    var entity = $('input[name="entity"]').val();

    $('input[name="total"]').val((price*entity));
}
</script>

</body>
</html>
