<body>

<style>
td {
    text-align: left;
}
</style>


	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'order' ,getAdminMenu())?></h2></div>

		<div id="content">
		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">

				<!-- (一)搜尋主體 -->
			<?php echo form_open("backend/product/order_import_csv" ,array( 'enctype' => 'multipart/form-data'))?>
				<table border="1" class="page_wrap--data_area--main_buyer--table">
					<tr>
						<th colspan="2">訂單匯入</th>
					</tr>

					<tr>
						<th>CVS檔案匯入</th>
						<td><input type="file" name="csv" />
						</td>
					</tr>

					<tr>
						<th></th>
						<td><input class="btn btn-primary" type="submit" value="確定" />
						</td>
					</tr>
				</table>
				</form>

				<p></p>

				<!-- (二)列表主體 -->
				<div class="clearfix"></div>

			</div>
		</div>

	</div>

</body>
</html>
