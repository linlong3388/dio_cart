<body>

	<style>
.fa-star {
	color: red
}
</style>

	<div id="container">

	<?php $this->load->view('vendor/common/nav.tpl'); ?>

	<?php $this->load->view('vendor/common/title.tpl'); ?>

		<div id="content">

		<?php $this->load->view('vendor/common/breadcrumb.tpl'); ?>

			<div class="container editform">

			<?php echo form_open("vendor/center/password"); ?>
				<table>
					<tr>
						<th>項目</th>
						<th>內容(<i class="fa fa-star"></i> 為必填欄位)</th>
					</tr>

					<tr>
						<th><i class="fa fa-star"></i>舊密碼</th>
						<td><input type="password" name="password" width="100%"
							maxlength="12" /> <b style="color: red"><?php echo form_error('password')?>
						</b></td>
					</tr>

					<tr>
						<th><i class="fa fa-star"></i>新密碼</th>
						<td><input type="password" name="passnew" width="100%"
							maxlength="12" /> <label>(* 密碼必須為6-12個字元)</label> <b
							style="color: red"><?php echo form_error('passnew')?> </b></td>
					</tr>

					<tr>
						<th><i class="fa fa-star"></i>再一次確認新密碼</th>
						<td><input type="password" name="passconf" width="100%"
							maxlength="12" /> <b style="color: red"><?php echo form_error('passconf')?>
						</b></td>
					</tr>



				</table>
				<div class="buttons">
					<input type="submit" value="送出" />
				</div>
				</form>

			</div>

		</div>

		<?php $this->load->view('vendor/common/footer.tpl'); ?>

	</div>

</body>
</html>
