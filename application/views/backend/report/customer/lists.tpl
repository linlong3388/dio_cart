<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'report_customer' ,getAdminMenu())?></h2></div>

		<div id="content">
		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">

				<!-- (一)搜尋主體 -->
			<?php echo form_open( getCurrentFullUrl() ,array('method'=>'get')) ?>
				<fieldset>
					<legend>搜尋條件:</legend>
						
					        帳號(%): 
					    <input type="text" name="srh_username" value="<?php echo $this->input->get('srh_username')?>" placeholder="帳號 模糊搜尋"> 
						
					        姓名(%):	
						<input type="text" name="srh_name" value="<?php echo $this->input->get('srh_name')?>" placeholder="姓名 模糊搜尋"> 
						
						<p>						
						
						<!-- 
						 排序 : 
						<select name="srh_sort">
						  <?php //foreach (reSortArray($this->input->get('srh_sort') ,$data_sort) as $key=>$val) { ?>
							<option value="<?php //echo $key?>"><?php //echo $val?></option>
							<?php //} ?>
						</select> 
						
						<input type="radio" name="srh_sort_type" value="DESC"
						<?php //echo $this->input->get('srh_sort_type') == 'DESC' ? " checked" : "" ?> />降冪
						<input type="radio" name="srh_sort_type" value="ASC"
						<?php //echo $this->input->get('srh_sort_type') == 'ASC' ? " checked" : "" ?> />升冪
					     -->
					
					
					<input type="submit" value="查詢">
				</fieldset>
				</form>

				<p></p>

				<b style="color: red"><?php echo form_error('srh_cdate1')?>
				</b> <b style="color: red"><?php echo form_error('srh_cdate2')?>
				</b>

				<!-- (二)列表主體 -->
				<div class="clearfix">
					<table class="dio_datatable">
						<thead>
							<tr>
							    <td>帳號</td>
								<td>姓名</td>
								<td>購買數量</td>
								<td>金額總計</td>
							</tr>
						</thead>
						<tbody>
						<?php foreach ($query as $row) { ?>
							<tr>
							    <td><a href="<?php echo getAdminURL('customer/edit?customer_id='.$row['customer_id'])?>" target="_new"><?php echo $row['username']?></a></td>
								<td><?php echo $row['name']?></td>
								<td><?php echo addCommas($row['entity'])?></td>
								<td><?php echo DIO_CURRENCY.addCommas( $row['product_total'] )?></td>
							</tr>
						<?php } ?>
						</tbody>
					</table>

					<br>
                   
                    <!-- 
					<div class="pageNum">
						<span>顯示第<?php //echo $page_startEnd['page_start']?> 至 <?php //echo $page_startEnd['page_end']?>
							項結果，共 <?php //echo $query_group_by[0]['count']?> 項 </span> <span
							style="float: right;"><?php //echo $pages->display_pages()?>
						</span>
					</div>
                     -->
					
				</div>

			</div>

		</div>

	</div>

	<link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css');?>" rel="stylesheet">
	<script src="<?php echo base_url('application/views/backend/js/jquery-ui-1.11.1.min.js');?>"></script>
	<script src="<?php echo base_url('application/views//backend/js/dio_plugins/jquery_fields_date.js');?>"></script>

<script>
$(document).ready(function() {
    $('.dio_datatable').dataTable({
      	 "language": { 
		         url : "<?php echo base_url("application/views/backend/js/dataTables/i18n/chinese_traditional.lang.json")?>"
		     },
		     "order": [[ 0, "desc" ]]
     });
});
</script>	
	
</body>
</html>
