<body>

<style>
td {
	text-align: center;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'report_order' ,getAdminMenu())?></h2></div>

		<div id="content">
		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">

				<!-- (一)搜尋主體 -->
			<?php echo form_open( getCurrentFullUrl() ,array('method'=>'get'))?>
				<fieldset>
					<legend>搜尋條件:</legend>
					訂單期間 : <input type="text" name="srh_cdate1" class="dio_date" 	value="<?php echo $this->input->get('srh_cdate1')?>">
					~<input type="text" name="srh_cdate2" class="dio_date" value="<?php echo $this->input->get('srh_cdate2')?>">
					
					<!--	
					, 依群組顯示 : 
					<select name="srh_group_date">
					<?php //if(!(empty($srh_param['srh_group_date']) && $srh_param['srh_group_date'] !== '0') ){ ?>
					    <option></option>
					<?php //} ?>
					</select> -->
					, 訂單狀態 : 
					<select name="srh_status">
					   <?php foreach (get_order_status(4,$this->input->get('srh_status')) as $key=>$val) { ?>
						  <option value="<?php echo $key?>"><?php echo $val?></option>
					   <?php } ?>
					</select> 
				  	
					<input type="submit" value="查詢">
				</fieldset>
				</form>

				<p></p>

				<b style="color: red"><?php echo form_error('srh_cdate1')?> </b> <b
					style="color: red"><?php echo form_error('srh_cdate2')?> </b>

				<!-- (二)列表主體 -->
				<div class="clearfix">
					<table class="dio_datatable">
						<thead>
							<tr>
								<td>訂單日期</td>
								<td>狀態</td>
								<td>訂單明細數量</td>
								<td>商品數量</td>
								<td>金額總計</td>
							</tr>
						</thead>
						<?php if(!empty($query)){ ?>
						<tbody>
						<?php
						//總額統計
						$total_sum = 0;
						$total_product_sum = 0;
						$total = 0;
						?>

						<?php foreach ($query as $row) { ?>
							<tr>
								<td><?php echo $row['sdate']?></td>
								<td><?php echo reSortArrayMutil(2,$row['status'],getOrderStatus())?></td>
								<td><?php $total_sum += $row['order_count'];
								          echo addCommas($row['order_count'])?></td>
								<td><?php $total_product_sum += $row['product_sum']; 
								          echo addCommas($row['product_sum'])?></td>
								<td><?php $total += $row['total']; 
								          echo addCommas($row['total'])?></td>
							</tr>
							<?php } ?>
						</tbody>

						<tfoot align="center">
							<tr>
								<td nowrap></td>
								<td nowrap>總計:</td>
								<td nowrap><b><?php echo addCommas($total_sum)?></b></td>
								<td nowrap><b><?php echo addCommas($total_product_sum)?></b></td>
								<td nowrap><b><?php echo addCommas($total)?></b></td>
							</tr>
						</tfoot>
						<?php } ?>
					</table>

				</div>

			</div>

		</div>

	</div>

	<link
		href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css');?>"
		rel="stylesheet">
	<script
		src="<?php echo base_url('application/views/backend/js/jquery-ui-1.11.1.min.js');?>"></script>
	<script
		src="<?php echo base_url('application/views//backend/js/dio_plugins/jquery_fields_date.js');?>"></script>

</body>
</html>
