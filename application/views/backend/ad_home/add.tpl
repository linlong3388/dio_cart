<body>

	<style>
.fa-star {
	color: red
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'ad_home' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			<?php echo form_open(current_url() ,array( 'enctype' => 'multipart/form-data'))?>
				<table>
					<tr>
						<th colspan="2">新增資料</th>
					</tr>

					<tr>
						<th width="20%"><i class="fa fa-star"></i>類型</th>
						<td><select name="type" class="fullwidth" onchange="youtube(this.value)">
						<?php foreach ( get_ad_home_type_data() as $key => $val) { ?>
								<option value="<?php echo $key ?>">
								<?php echo $val?>
								</option>
								<?php }?>
						</select></td>
					</tr>

					<!-- 
					<tr id="hd_title">
						<th width="20%">標題</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text"
							name="title" value="<?php echo set_value('title')?>"> <b
							style="color: red"><?php echo form_error('title')?> </b></td>
					</tr>

					<tr id="hd_description">
						<th width="20%">簡述</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text"
							name="description" value="<?php echo set_value('description')?>"> <b
							style="color: red"><?php echo form_error('description')?> </b></td>
					</tr>
					 -->

					<tr id="hd_image">
						<th width="20%">圖片</th>
						<td style="text-align: left;">
	                        <input style="display: none" class="input-medium focused" id="specification_image" name="image" value="<?php echo isset($query['image']) ? $query['image'] : set_value('image') ;?>">
   	                           <div class="span12">
   	                             <img id="image" class="img-responsive" width="300px" height="250px" src="<?php echo (!empty($query['image'])) ? base_url("resources/uploads/".$query['image']) : base_url("resources/uploads").'/'.set_value('image')?>" alt="圖片"/>
                                    <p>                                                       
                                       <a onclick="base_image_upload('image');">選擇圖片</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                       <a onclick="base_image_delete('image');">刪除圖片</a>
                                    </p>
		                       </div>          
						</td>
					</tr>
			
					<tr>
						<th width="20%">連結</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text"
							name="url" value="<?php echo set_value('url')?>"> <b
							style="color: red"><?php echo form_error('url')?> </b></td>
					</tr>

				</table>

				<div class="buttons">
					<input type="submit" value="送出" />
					<button type="button"
						onclick="javascript:location.href='<?php echo getAdminURL('ad_home/lists')?>'">返回</button>
				</div>

				</form>
			</div>

		</div>

	</div>

<link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>" rel="stylesheet">
<link href="<?php echo base_url('application/views/backend/css/elfinder.min.css')?>" rel="stylesheet">
<link href="<?php echo base_url('application/views/backend/css/theme.css')?>" rel="stylesheet">

<script src="<?php echo base_url('application/views/backend/js/jquery-1.7.2.min.js');?>"></script>
<script src="<?php echo base_url('application/views/backend/js/jquery-ui-1.8.21.custom.min.js');?>"></script>
<script src="<?php echo base_url('application/views/backend/js/elfinder.min.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/i18n/elfinder.zh_CN.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_image_upload.js')?>"></script>

<script>
//-------------------------------------------------------------        
//
// 使用影片 
//
//-------------------------------------------------------------   
/*
function youtube(type){

   if(type == 'c'){
	   $('#hd_title').hide('slow');
	   $('#hd_description').hide('slow');
       $('#hd_image').hide('slow');
   }else{
	  $('#hd_title').show('slow');
	  $('#hd_description').show('slow');
	  $('#hd_image').show('slow');
   }	      
}*/	 	 
</script>


</body>
</html>
