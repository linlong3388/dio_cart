<body>

<style>
.fa-star {
	color: red
}
td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'fee' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			
			 <?php if($this->session->flashdata('msg')){ ?>				 
		        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	         <?php } ?> 
			
			 <b style="color: red"><?php echo validation_errors(); ?></b> 			
			
			<?php echo form_open( getCurrentFullUrl() )?>
				<table>
					<tr>
						<th colspan="2">編輯資料</th>
					</tr>

					<tr>
						<th width="20%"><i class="fa fa-star"></i>常溫運費</th>
						<td><input placeholder="請輸入資料" type="text" name="shipping_fee" value="<?php echo isset($query['shipping_fee']) ? $query['shipping_fee'] : set_value('shipping_fee')?>" onkeypress="def_numeric()">元</td>
					</tr>					
					
					<!-- 
					<tr>
						<th width="20%"><i class="fa fa-star"></i>冷藏運費</th>
						<td><input placeholder="請輸入資料" type="text" name="shipping_fee_refri" value="<?php //echo isset($query['shipping_fee_refri']) ? $query['shipping_fee_refri'] : set_value('shipping_fee_refri')?>" onkeypress="def_numeric()">元</td>
					</tr>	
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>冷凍運費</th>
						<td><input placeholder="請輸入資料" type="text" name="shipping_fee_cool" value="<?php //echo isset($query['shipping_fee_cool']) ? $query['shipping_fee_cool'] : set_value('shipping_fee_cool')?>" onkeypress="def_numeric()">元</td>
					</tr>	
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>離島運費</th>
						<td><input placeholder="請輸入資料" type="text" name="shipping_fee_island" value="<?php //echo isset($query['shipping_fee_island']) ? $query['shipping_fee_island'] : set_value('shipping_fee_island')?>" onkeypress="def_numeric()">元</td>
					</tr>	
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>免運額(折扣後)</th>
						<td><input placeholder="請輸入資料" type="text" name="shipping_fee_full" value="<?php //echo isset($query['shipping_fee_full']) ? $query['shipping_fee_full'] : set_value('shipping_fee_full')?>" onkeypress="def_numeric()">元</td>
					</tr>	
					 -->
					
				</table>

				<div class="buttons">
					<input type="submit" value="送出" />
				</div>

				</form>
			</div>

		</div>

	</div>

<script src="<?php echo base_url('application/views/backend/js/tinymce/tinymce.min.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_tinymce_elfinder.js')?>"></script>
	
	
</body>
</html>
