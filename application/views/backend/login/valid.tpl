<body id="login">
	<div class="logo">
		<img src="<?php echo base_url(DIO_PATH_PIMG.$_SESSION['sys_info']['logo'])?>" width="350px" height="101px" alt="logo">
	</div>
	<div class="content">
		<div class="signin">
		<?php echo form_open(current_url() ,array('name' => 'form1'))?>
			<h3>管理員登入</h3>
			
			<div class="form-group">
				<input type="text" name="username" placeholder="請輸入帳號" value="<?php echo set_value('username')?>" /> <b><?php echo form_error('username')?>
				</b>
			</div>
			
			<div class="form-group">
				<input type="password" name="password" placeholder="請輸入密碼" /> <b><?php echo form_error('password')?>
				</b>
			</div>
			
			<div class="form-group">
				<input type="text" name="secure_code"  maxlength="4" placeholder="驗證碼" /> 
				<b><?php echo form_error('secure_code')?></b>
			</div>
			
			<div class="form-group">
			<img id="captchaImg" src="<?php echo getAdminURL('login/captcha')?>" />
				<button type="button" class="btn-sm btn-primary" onclick="changeImg()"> <span class="fa fa-refresh fa-fw" aria-hidden="true"></span> </button>
			</div>
			
			<div class="form-group">
				<input type="submit" value="登入" /> <input type="checkbox"
					name="checkbox2" value="" /> 保持登入(公用電腦請勿勾選) <input type="hidden"
					name="login_type" value="login" />
			</div>
			</form>
		</div>
	</div>

	<div class="copyright">
	<?php echo DIO_LOGIN_FOOTER?>
	</div>

<script>
//-------------------------------------------------------------
//
// 改變驗證碼 Ajax
//
//-------------------------------------------------------------
function changeImg(){
  var captcha = '<?php echo getAdminURL('login/captcha')?>'; 
     $('#captchaImg').attr('src' ,captcha);	
}      
</script>	
	
</body>
</html>
