<body>

<style>
.fa-star {
	color: red
}
td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'contact' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>
	
	<div class="container editform">
	
		<?php if($this->session->flashdata('msg')){ ?>				 
	        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
        <?php } ?> 
			
		<b style="color: red"><?php echo validation_errors(); ?></b>
		
		<?php echo form_open( getCurrentFullUrl() )?>
		
				<table>
					<tr>
						<th colspan="2">檢視資料</th>
					</tr>

					<tr>
						<th width="20%">編號</th>
						<td><input type="text" value="<?php echo $query['contact_id'] ?>"></td>
					</tr>
							
					<!--  			
					<tr>
						<th width="20%">服務門市</th>
						<td><input type="text" value="<?php //echo $query['b_title'] ?>"></td>
					</tr>			
					-->
					
					<!-- 						    
					<tr>
						<th width="20%">聯絡事項</th>
						<td>
						    <input type="text" value="<?php //echo reSortArrayMutil(2,$query['contact_category_id'],getContactCategory())?>" >
					     </td>
					</tr>
					 -->						    
					
					<!-- 
					<tr>
						<th width="20%">標題</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="title" value="<?php //echo isset($query['title']) ? $query['title'] : set_value('title')?>"></td>
					</tr>					
	                --> 

					<tr>
						<th width="20%">聯絡人</th>
						<td><input class="fullwidth" type="text" value="<?php echo $query['name'] ?>"></td>
					</tr>					
					
					<tr>
						<th width="20%">連絡電話</th>
						<td><input class="fullwidth" type="text" value="<?php echo $query['phone']?>"></td>
					</tr>					
					
					<tr>
						<th width="20%">Email</th>
						<td><input class="fullwidth" type="text" value="<?php echo $query['email']?>"></td>
					</tr>					
					
					<!-- 
					<tr>
						<th width="20%">圖片</th>
						<td><img alt="" src="<?php //echo base_url('resources/user/'.$query['image']) ?>"></td>
					</tr>
					 -->					
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>狀態</th>
						<td>
						    <input type="radio" name="status" value="1"
						    <?php if( isset($query['status']) && ($query['status'] == '1') ) echo set_radio('status', '1' ,true);
						      else echo set_radio('status', '1'); ?>>已處理 
						    <input type="radio" name="status" value="0"
							<?php if( isset($query['status']) && ($query['status'] == '0') ) echo set_radio('status', '0' ,true);
							  else echo set_radio('status', '0'); ?>>未處理
						</td>
					</tr>	
				
					
					<tr>
						<th width="20%">內容</th>
						<td><textarea class="fullwidth" rows="10" readonly ><?php echo $query['content']?></textarea></td>
					</tr>
					
					<tr>
						<th width="20%">管理員備註</th>
						<td><textarea class="fullwidth" rows="10" name="memo_admin"><?php echo isset($query['memo_admin']) ? $query['memo_admin'] : set_value('memo_admin')?></textarea></td>
					</tr>
				
					<tr>
						<th width="20%">建立日期</th>
						<td><input class="fullwidth" type="text" value="<?php echo $query['cdate']?>"></td>
					</tr>

					<tr>
				       <th width="20%"></th>
					   <td><button type="submit">送出</button> 
					       <button type="button" onclick="javascript:location.href='<?php echo getAdminURL('contact/lists')?>'">返回</button></td>	
					</tr>
			
				</table>
				
			</form>
    
         </div>

      </div>

	</div>
	
	<script>
      $('input').attr('readonly', true);
	</script>
	
		
</body>
</html>
