<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'branch' ,getAdminMenu())?></h2></div>

		<div id="content">

			<span style="float: right">
				<div class="actions">
					<a href="javascript:location.href='<?php echo getAdminURL('branch/add')?>'"
						class="btn btn-default btn-sm"> <i class="fa fa-pencil"></i> 新增</a>
				</div> </span>

				<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">

			 <?php if($this->session->flashdata('msg')){ ?>				 
		        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	         <?php } ?> 			
			
				<!-- (一)搜尋主體 -->

				<p></p>

				<!-- (二)列表主體 -->
				<div class="clearfix">

					<table class="dio_datatable">
						<thead>
							<tr>
								<!-- <th>分類</th> -->
								<th>服務據點</th>
								<th>區碼</th>
								<th>地址</th>
								<th>排序</th>
								<th>建立日期</th>
								<th>功能</th>
							</tr>
						</thead>

						<tbody>

							<?php foreach ($query as $row){ ?>
								<tr>
									<!-- <td class="center"><?php //echo reSortArrayMutil(2,$row['branch_category_id'],getBranchCategory())?></td> -->
									<td class="center"><?php echo $row['title']?></td>
									<td class="center"><?php echo $row['local']?></td>
									<td class="center"><?php echo $row['addr']?></td>
									<td class="center"><?php echo $row['sort_order']?></td>
									<td class="center"><?php echo substr($row['cdate'], 0,10) ?><b style="display: none"><?php echo substr($row['cdate'], 11) ?></td>
									<td class="center">
									    <a class="btn btn-info" href="<?php echo getAdminURL('branch/pass?branch_id='.$row['branch_id'])?>">
										   <i class="icon-view icon-white"></i> 管理員</a>
									    <a class="btn btn-info" href="<?php echo getAdminURL('branch/edit?branch_id='.$row['branch_id'])?>">
									       <i class="icon-view icon-white"></i> 編輯</a> 
									    <a class="btn btn-danger" href="javascript:my_confirm(<?php echo $row['branch_id']?>);">
									       <i class="icon-trash icon-white"></i> 刪除</a>
									</td>
								</tr>
								<?php } ?>

						</tbody>

					</table>
				</div>

			</div>


		</div>

	</div>

	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script type="text/javascript">
	 $(document).ready(function() {
        $('.dio_datatable').dataTable({
        	 'aaSorting': [[ 4, 'desc' ]] ,
          	 "language": { 
   		         url : "<?php echo base_url("application/views/backend/js/dataTables/i18n/chinese_traditional.lang.json")?>"
             },
             "iDisplayLength": 50
         });
    });
  
    function my_confirm(branch_id){
        var chk_box=confirm('確定刪除資料?');
        if (chk_box == true) {
            location.href = '<?php echo base_url("index.php/backend/branch/del?branch_id=") ?>'+branch_id;
        }
     }
</script>

</body>
</html>

