<body>

<style>
.fa-star {
	color: red
}
td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'branch' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			
			 <?php if($this->session->flashdata('msg')){ ?>				 
		        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	         <?php } ?> 
			
			 <b style="color: red"><?php echo validation_errors(); ?></b> 			
			
			<?php echo form_open( getCurrentFullUrl() )?>
				<table>
					<tr>
						<th colspan="2">編輯資料</th>
					</tr>
									
					<!-- 					
				    <tr>
						<th width="20%"><i class="fa fa-star"></i>類別</th>
						<td><select class="form-control" name="branch_category_id">
		                      <?php //foreach( reSortArrayMutil(3,$query['branch_category_id'],getBranchCategory()) as $key=>$val) :?>    
                                  <option value="<?php //echo $key?>"><?php //echo $val?></option>
                               <?php //endforeach; ?>
	                         </select>
	                    </td>
					</tr>
					 -->					
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>據點名稱</th>
						<td><input placeholder="請輸入資料" type="text" name="title" value="<?php echo isset($query['title']) ? $query['title'] : set_value('title')?>">
						</td>
					</tr>					
			
					<tr>
						<th width="20%"><i class="fa fa-star"></i>電話</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="phone" value="<?php echo isset($query['phone']) ? $query['phone'] : set_value('phone')?>"></td>
					</tr>					

					<tr>
						<th width="20%">手機</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="mobile" value="<?php echo isset($query['mobile']) ? $query['mobile'] : set_value('mobile')?>"></td>
					</tr>		
					
					<tr>
						<th width="20%">傳真</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="fax" value="<?php echo isset($query['fax']) ? $query['fax'] : set_value('fax')?>"></td>
					</tr>	
					
					<tr>
						<th width="20%">營業時間</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="action_time" value="<?php echo isset($query['action_time']) ? $query['action_time'] : set_value('action_time')?>">
						    <b>(*多行請用 ','逗號區格)</b>
						</td>
					</tr>					
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>地址</th>
						<td>
						   <select name="local" class="form-control">
                              <?php foreach ( reSortArrayMutil(3 ,$query['local'] ,getLocalCity()) as $key=>$val){ ?>
                                  <option value="<?php echo $key?>"><?php echo $val?></option>
                              <?php } ?>
                           </select>
                           
						   <input placeholder="請輸入資料" type="text" name="addr" value="<?php echo isset($query['addr']) ? $query['addr'] : set_value('addr')?>">
                        </td>
					</tr>					
							
					<tr>
						<th width="20%"><i class="fa fa-star"></i>管理員帳號</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="username" value="<?php echo isset($query['username']) ? $query['username'] : set_value('username')?>"></td>
					</tr>		
								
					<!-- 			
					<tr>
						<th width="20%">相關連結</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="link" value="<?php //echo isset($query['link']) ? $query['link'] : set_value('link')?>"></td>
					</tr>
					 -->					
					
					<tr id="hd_image">
						<th width="20%">圖片<br>(640x480)</th>
						<td><input style="display: none" class="input-medium focused" id="specification_image" name="image" value="<?php echo isset($query['image']) ? $query['image'] : set_value('image') ;?>">
     	                    <div class="span12">
			                   <img id="image" class="img-responsive" width="300px" height="250px"  src="<?php echo (!empty($query['image'])) ? base_url("resources/uploads/".$query['image']) : base_url("resources/uploads").'/'.set_value('image')?>" alt="圖片"/>
                                  <p>                                                       
                                     <a onclick="base_image_upload('image');">選擇圖片</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                     <a onclick="base_image_delete('image');">刪除圖片</a>
                                  </p>
		                     </div>          
						</td>
					</tr>
					
					<!-- 
					<tr>
						<th width="20%">分店介紹</th>
						<td><textarea class="fullwidth dio_textarea" rows="10" name="content" placeholder="請輸入資料"><?php //echo isset($query['content']) ? $query['content'] : set_value('content')?></textarea></td>
					</tr>
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>緯度</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="latitude" value="<?php //echo isset($query['latitude']) ? $query['latitude'] : set_value('latitude')?>"></td>
					</tr>					
		
					<tr>
						<th width="20%"><i class="fa fa-star"></i>經度</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="longitude" value="<?php //echo isset($query['longitude']) ? $query['longitude'] : set_value('longitude')?>"></td>
					</tr>
					 -->
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>排序</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="sort_order"
							value="<?php echo isset($query['sort_order']) ? $query['sort_order'] : set_value('sort_order')?>">
							</td>
					</tr>
					
					<!-- 
				    <tr>
						<th colspan="2">Meta頁籤</th>
					</tr>
					
					<tr>
						<th width="20%">Meta標籤標題</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="meta_title" value="<?php //echo isset($query['meta_title']) ? $query['meta_title'] : set_value('meta_title')?>">
						</td>
					</tr>
					
					<tr>
						<th width="20%">Meta標籤描述</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="meta_description" value="<?php //echo isset($query['meta_description']) ? $query['meta_description'] : set_value('meta_description')?>">
						</td>
					</tr>
					
					<tr>
						<th width="20%">Meta標籤關鍵字</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="meta_keyword" value="<?php //echo isset($query['meta_keyword']) ? $query['meta_keyword'] : set_value('meta_keyword')?>">
						</td>
					</tr>
					 -->	
					
				</table>

				<div class="buttons">
					<input type="submit" value="送出" />
					<button type="button"
						onclick="javascript:location.href='<?php echo getAdminURL('branch/lists')?>'">返回</button>
				</div>

				</form>
			</div>

		</div>

	</div>
		
<script src="<?php echo base_url('application/views/backend/js/tinymce/tinymce.min.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_tinymce_elfinder.js')?>"></script>
		
<link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>" rel="stylesheet">
<link href="<?php echo base_url('application/views/backend/css/elfinder.min.css')?>" rel="stylesheet">
<link href="<?php echo base_url('application/views/backend/css/theme.css')?>" rel="stylesheet">

<script src="<?php echo base_url('application/views/backend/js/jquery-1.7.2.min.js');?>"></script>
<script src="<?php echo base_url('application/views/backend/js/jquery-ui-1.8.21.custom.min.js');?>"></script>
<script src="<?php echo base_url('application/views/backend/js/elfinder.min.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/i18n/elfinder.zh_CN.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_image_upload.js')?>"></script>

<!-- 
<script src="<?php //echo base_url('application/views/backend/js/tinymce/tinymce.min.js')?>"></script>
<script src="<?php //echo base_url('application/views/backend/js/dio_plugins/jquery_tinymce_elfinder.js')?>"></script>
 -->
</body>
</html>
