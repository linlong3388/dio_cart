<body>
	<div id="container">

	<?php $this->load->view('vendor/common/nav.tpl'); ?>

	<?php $this->load->view('vendor/common/title.tpl'); ?>

		<div id="content">

		<?php $this->load->view('vendor/common/breadcrumb.tpl'); ?>

			<div class="container editform">

			<?php echo form_open("vendor/center/setstore" ,array( 'enctype' => 'multipart/form-data'))?>
				<table>
					<tr>
						<th>項目</th>
						<th>內容</th>
					</tr>

					<tr>
						<th>統一編號</th>
						<td><input type="text" name="unified_number" width="100%"
							value="<?php echo isset($query['unified_number']) ? $query['unified_number'] : set_value('unified_number')?>"
							readonly /></td>
					</tr>

					<!-- 公司名稱請控制長度，不應超過20個字元 -->
					<tr>
						<th>公司名稱</th>
						<td><input type="text" name="name"
							value="<?php echo isset($query) ? $query['name'] : set_value('name') ?>"
							readonly /><br /> <input type="text" name="vendor_id"
							value="<?php echo isset($query) ? $query['vendor_id'] : set_value('vendor_id') ?>"
							style="display: none"> <span><?php echo form_error('name') ?> </span>
						</td>
					</tr>

					<!-- 店家名稱請控制長度，不應超過20個字元 -->
					<tr>
						<th><b style="color: red">*</b>店家名稱</th>
						<td><input class="fullwidth" type="text" name="name2"
							value="<?php echo isset($query) ? $query['name2'] : set_value('name2') ?>" /><br />
							<b style="color: red"><?php echo form_error('name2') ?> </b></td>
					</tr>

					<tr>
						<th><b style="color: red">*</b>所在地址</th>
						<td><select name="local" class="dio_city" style="width: 200px;">
								<option
									value="<?php echo isset($query['local']) ? $query['local'] : set_value('local') ;?>">
									<?php echo isset($query['local']) ? $query['local'] : set_value('local') ;?>
								</option>
						</select> <input class="fullwidth" type="text" name="address"
							value="<?php echo isset($query['address']) ? $query['address'] : set_value('address') ;?>">
							<b style="color: red"><?php echo form_error('address')?> </b></td>
					</tr>

					<tr>
						<th><b style="color: red">*</b>連絡電話</th>
						<td><input class="fullwidth" type="text" name="phone" width="100%"
							value="<?php echo isset($query['phone']) ? $query['phone'] : set_value('phone') ?>"
							onkeypress="def_numeric()" /> <b style="color: red"><?php echo form_error('phone')?>
						</b></td>
					</tr>

					<tr>
						<th>客服電話</th>
						<td><input class="fullwidth" type="text" name="service_phone"
							width="100%"
							value="<?php echo isset($query['service_phone']) ? $query['service_phone'] : set_value('service_phone') ?>"
							onkeypress="def_numeric()" /> <b style="color: red"><?php echo form_error('service_phone')?>
						</b></td>
					</tr>

					<tr>
						<th>傳真</th>
						<td><input class="fullwidth" type="text" name="fax" width="100%"
							value="<?php echo isset($query['fax']) ? $query['fax'] : set_value('fax') ?>"
							onkeypress="def_numeric()" /> <b style="color: red"><?php echo form_error('fax')?>
						</b></td>
					</tr>

					<tr>
						<th><b style="color: red">*</b>電子郵件</th>
						<td><input class="fullwidth" type="text" name="email" width="100%"
							value="<?php echo isset($query['email']) ? $query['email'] : set_value('email') ?>" />
							<b style="color: red"><?php echo form_error('email')?> </b></td>
					</tr>

					<tr>
						<th>服務時間</th>
						<td><input class="fullwidth" type="text" name="service_time"
							width="100%"
							value="<?php echo isset($query['service_time']) ? $query['service_time'] : set_value('service_time') ?>" />
							<b style="color: red"><?php echo form_error('service_time')?> </b>
						</td>
					</tr>

					<tr>
						<th>店鋪LOGO<br /> (寬x高:200x70)</th>
						<td>
							<!-- <div class="store_logo">  -->
							<div>
								<img id="blah"
									src="<?php echo !empty($query['logo']) ? base_url('resources/uploads/vendor/'.$query['logo']) : '#'; ?>"
									alt="your image" width="200px" height="70px" /> <input
									style="display: block" type="file" name="logo"
									value="<?php echo isset($query['logo']) ? $query['logo'] : set_value('logo') ;?>"
									onchange="readURL(this)" /> <input style="display: none"
									name="logo"
									value="<?php echo isset($query['logo']) ? $query['logo'] : set_value('logo') ;?>"
									width="200px" height="70px" />
							</div></td>
					</tr>

					<!-- 
						<tr>
							<th>圖片</th>
							<td>
								<div><img align="middle" id="blah1"
									src="<?php echo !empty($query['image1']) ? base_url('resources/uploads/vendor/'.$query['image1']) : '#'; ?>"
									alt="your image" width="200px" height="200px" /> <input
									type="file" name="image1"
									value="<?php echo isset($query['image1']) ? $query['image1'] : set_value('image1') ;?>"
									onchange="readURL_A(this)" /> <input style="display: none"
									name="image1"
									value="<?php echo isset($query['image1']) ? $query['image1'] : set_value('image1') ;?>" />
								</div>

								<div><img align="middle" id="blah2"
									src="<?php echo !empty($query['image2']) ? base_url('resources/uploads/vendor/'.$query['image2']) : '#'; ?>"
									alt="your image" width="200px" height="200px" /> <input
									type="file" name="image2"
									value="<?php echo isset($query['image2']) ? $query['image2'] : set_value('image2') ;?>"
									onchange="readURL_B(this)" /> <input style="display: none"
									name="image2"
									value="<?php echo isset($query['image2']) ? $query['image2'] : set_value('image2') ;?>" />
								</div>

								<div><img align="middle" id="blah3"
									src="<?php echo !empty($query['image3']) ? base_url('resources/uploads/vendor/'.$query['image3']) : '#'; ?>"
									alt="your image" width="200px" height="200px" /> <input
									type="file" name="image3"
									value="<?php echo isset($query['image3']) ? $query['image3'] : set_value('image3') ;?>"
									onchange="readURL_C(this)" /> <input style="display: none"
									name="image3"
									value="<?php echo isset($query['image3']) ? $query['image3'] : set_value('image3') ;?>" />
								</div>
								<span>(*建議700x700 以上 且圖檔不可超過 1MB)</span>
							</td>
						</tr>
						 -->

					<tr>
						<th>店家簡介</th>
						<td><textarea style="width: 800px" rows="10" cols="50"
								name="profile">
								<?php echo isset($query['profile']) ? $query['profile'] : set_value('profile') ?>
							</textarea> <b style="color: red"><?php echo form_error('profile')?>
						</b></td>
					</tr>

					<tr>
						<th>店鋪橫幅廣告<br /> (寬x高:960x530)</th>
						<td><textarea rows="20" cols="50" name="banner">
						<?php echo isset( $query['banner'] ) ? $query['banner'] : set_value('banner') ?>
							</textarea> <!-- 
								<div>
									<img id="blah1"
										src="<?php echo !empty($query['banner']) ? base_url('resources/uploads/vendor/'.$query['banner']) : '#'; ?>"
										alt="your image" width="500px" height="250px" /> <input
										style="display: block" type="file" name="banner"
										value="<?php echo isset($query['banner']) ? $query['banner'] : set_value('banner') ;?>"
										onchange="readURL_A(this)" /> <input style="display: none"
										name="banner"
										value="<?php echo isset($query['banner']) ? $query['banner'] : set_value('banner') ;?>"
										width="500px" height="250px" />
								</div>
								 --></td>
					</tr>

					<tr>
						<th>基本條款</th>
						<td><textarea style="width: 800px" rows="10" cols="50"
								name="basic_terms">
								<?php echo isset($query['basic_terms']) ? $query['basic_terms'] : set_value('basic_terms') ?>
							</textarea> <b style="color: red"><?php echo form_error('basic_terms')?>
						</b></td>
					</tr>

					<tr>
						<th>購物說明</th>
						<td><textarea style="width: 800px" rows="10" cols="50"
								name="shopping_guide">
								<?php echo isset($query['shopping_guide']) ? $query['shopping_guide'] : set_value('shopping_guide') ?>
							</textarea> <b style="color: red"><?php echo form_error('shopping_guide')?>
						</b></td>
					</tr>


				</table>
				<div class="buttons">
					<input type="submit" value="送出" />
				</div>
				</form>

			</div>

		</div>

		<?php $this->load->view('vendor/common/footer.tpl'); ?>

	</div>

	<!-- elfinder 的專屬 css -->
	<link
		href="<?php echo base_url('application/views/vendor/css/jquery-ui-1.8.21.custom.css');?>"
		rel="stylesheet">
	<link
		href="<?php echo base_url('application/views/vendor/css/theme.css');?>"
		rel="stylesheet">
	<link
		href="<?php echo base_url('application/views/vendor/css/jquery.cleditor.css');?>"
		rel="stylesheet">

	<!-- elfinder 的專屬 js -->
	<script
		src="<?php echo base_url('application/views/vendor/js/jquery-ui-1.11.1.min.js');?>"></script>
	<script
		src="<?php echo base_url('application/views/vendor/js/jquery.cleditor.js');?>"></script>
	<script
		src="<?php echo base_url('application/views/vendor/js/jquery.cleditor.extimage.js');?>"></script>

	<script type="text/javascript">
/***********************************
/* 名稱 : readURL
/* 功能 : 新圖片上傳
************************************/
function readURL(input) {	

	if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result)
                .width(200)
                .height(70);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

/***********************************
/* 名稱 : readURL_A
/* 功能 : 新圖片上傳A
************************************/
function readURL_A(input) {	

	if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah1')
                .attr('src', e.target.result)
                .width(500)
                .height(250);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
</script>

	<script type="text/javascript">
/***********************************
/* 名稱 : 
/* 功能 : 文字編輯器的圖片上傳
************************************/
<!--
   $(function(){
        //$('#product_speci_edit').hide();
	    $("textarea[name=banner] ,textarea[name=basic_terms] ,textarea[name=shopping_guide]").cleditor({
		  	 width:   800 , 
	         height:  500 
	    });
	    $.cleditor.buttons.image.uploadUrl = '<?php echo base_url('index.php/vendor/center/cleditor_image_upload');?>';  
   });
-->
</script>

</body>
</html>
