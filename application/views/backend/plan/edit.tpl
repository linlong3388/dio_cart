<body>

<style>
.fa-star {
	color: red
}
td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'plan' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			
			 <?php if($this->session->flashdata('msg')){ ?>				 
		        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	         <?php } ?> 
			
			 <b style="color: red"><?php echo validation_errors(); ?></b> 			
			
			<?php echo form_open( getCurrentFullUrl() )?>
				<table>
					<tr>
						<th colspan="2">編輯資料</th>
					</tr>
					
					<tr><th width="20%">標題</th>
						<td><input style="width: 60%;" placeholder="請輸入資料" type="text" name="blockA_title" value="<?php echo isset($xml->blockA->title) ? $xml->blockA->title : set_value('blockA_title')?>"></td>
					</tr>	

					<!--  
					<tr><th width="20%">短標</th>
						<td><input style="width: 60%;" placeholder="請輸入資料" type="text" name="blockA_title_sub" value="<?php //echo isset($xml->blockA->title_sub) ? $xml->blockA->title_sub : set_value('blockA_title_sub')?>"></td>
					</tr> -->
					
					<tr>
						<th width="20%">內容</th>
						<td><textarea class="fullwidth dio_textarea" rows="3" name="blockA_content" placeholder="請輸入資料"><?php echo isset($xml->blockA->content) ? $xml->blockA->content : set_value('blockA_content')?></textarea></td>
					</tr>					
		
					
					<tr>
						<th colspan="2">Meta頁籤</th>
					</tr>
					
					<tr>
						<th width="20%">Meta標籤標題</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="meta_title" value="<?php echo isset($xml->meta->title) ? $xml->meta->title : set_value('meta_title')?>">
						</td>
					</tr>
					
					<tr>
						<th width="20%">Meta標籤描述</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="meta_description" value="<?php echo isset($xml->meta->description) ? $xml->meta->description : set_value('meta_description')?>">
						</td>
					</tr>
					
					<tr>
						<th width="20%">Meta標籤關鍵字</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="meta_keyword" value="<?php echo isset($xml->meta->keyword) ? $xml->meta->keyword : set_value('meta_keyword')?>">
						</td>
					</tr>						

				</table>

				<div class="buttons">
					<input type="submit" value="送出" />
					<!-- 
					  <button type="button" onclick="javascript:location.href='<?php //echo getAdminURL('news/lists')?>'">返回</button>
				     -->
				</div>

				</form>
			</div>

		</div>

	</div>

<script src="<?php echo base_url('application/views/backend/js/tinymce/tinymce.min.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_tinymce_elfinder.js')?>"></script>

<link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>" rel="stylesheet">
<link href="<?php echo base_url('application/views/backend/css/elfinder.min.css')?>" rel="stylesheet">
<link href="<?php echo base_url('application/views/backend/css/theme.css')?>" rel="stylesheet">

<script src="<?php echo base_url('application/views/backend/js/jquery-1.7.2.min.js');?>"></script>
<script src="<?php echo base_url('application/views/backend/js/jquery-ui-1.8.21.custom.min.js');?>"></script>
<script src="<?php echo base_url('application/views/backend/js/elfinder.min.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/i18n/elfinder.zh_CN.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_image_upload.js')?>"></script>

</body>
</html>
