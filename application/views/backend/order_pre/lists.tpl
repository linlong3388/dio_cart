<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'order_pre' ,getAdminMenu())?></h2></div>

		<div id="content">
		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">

				<!-- (一)搜尋主體 -->
			<?php echo form_open(current_url()  ,array('method'=>'get'))?>
				<fieldset>
					<legend>搜尋條件:</legend>
                      <input style="display:none" type="text" name="page" value="1"> 
					訂單期間 : 
					  <input type="text" name="srh_order_pre_cdate1" class="dio_date" value="<?php echo $this->input->get('srh_order_pre_cdate1')?>">
					 ~<input type="text" name="srh_order_pre_cdate2" class="dio_date" value="<?php echo $this->input->get('srh_order_pre_cdate2')?>">
				     
					<input type="submit" value="查詢">
				</fieldset>
				</form>

				<p></p>

				<b style="color: red"><?php echo form_error('srh_cdate1')?> </b> <b
					style="color: red"><?php echo form_error('srh_cdate2')?> </b>

				<!-- (二)列表主體 -->
				<div class="clearfix">

					<table class="dio_datatable">
						<thead>
							<tr>
								<td>預購單編號</td>
								<td>品名</td>
								<td>型號</td>
								<td>訂購人姓名</td>
								<td>是否已通知</td>
								<td>狀態</td>
								<td>建立日期</td>
								<td>功能</td>
							</tr>
						</thead>

						<tbody>
						<?php foreach ($query as $row) { ?>
							<tr>
								<td><?php echo $row['order_pre_id']?></td>
								<td><?php echo $row['pname']?></td>
								<td><?php echo $row['psku']?></td>
								<td><?php echo $row['name']?></td>
								<td><?php echo reSortArrayMutil(2,$row['is_notice'],getOrderPreIsNotice())?></td>
								<td><?php echo reSortArrayMutil(2,$row['status'],getOrderPreStatus())?></td>
								<td><?php echo substr($row['cdate'], 0,10) ?><b style="display: none"><?php echo substr($row['cdate'], 11) ?></td>
								<td><a class="btn btn-success"
									href="<?php echo getAdminURL('order_pre/view?order_pre_id='.$row['order_pre_id']) ?>">
										<i class="fa fa-pencil-square-o"></i>檢視</a></td>
							</tr>
							<?php } ?>
						</tbody>

					</table>

					<div class="pageNum">
						<span>顯示第<?php echo $page_startEnd['page_start']?> 至 <?php echo $page_startEnd['page_end']?>
							項結果，共 <?php echo $query_group_by[0]['count']?> 項 </span> <span
							style="float: right;"><?php echo $pages->display_pages()?>
						</span>
					</div>


				</div>


			</div>

		</div>

	</div>

	<link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>" rel="stylesheet">
	<script src="<?php echo base_url('application/views/backend/js/jquery-ui-1.11.1.min.js')?>"></script>
	<script src="<?php echo base_url('application/views//backend/js/dio_plugins/jquery_fields_date.js')?>"></script>

	<script type="text/javascript">
      function my_confirm(order_pre_id ,product_speci_id){
         var chk_box=confirm('確定出貨?');
         if (chk_box == true) {
             location.href = '<?php echo base_url("index.php/backend/order_pre/order_pre_shipping?order_pre_id=") ?>'+order_pre_id;
         }
      }
    </script>

</body>
</html>
