<body>

<style>
#order_preDetail th {
	background: #45B6AF;
}

#order_preDetail td {
	text-align: right;
}

td {
    text-align: left;
}

.fa-star {
	color: red
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'order_pre' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			
			<?php if($this->session->flashdata('msg')){ ?>				 
		       <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	        <?php } ?>			
			
			<?php echo form_open( getCurrentFullUrl())?>
				<table>
					<tr>
						<th colspan="2">檢視資料</th>
					</tr>
					
					<tr>
						<th>預購單編號</th>
					    <td><?php echo $query['order_pre_id']?></td>
					</tr>

					<tr>
						<th>品名</th>
						<td><a href="<?php echo getAdminURL('product/edit?product_id='.$query['product_id'])?>" target="_new"><?php echo $query['pname']?></a></td>
					</tr> 
					
					<tr>
						<th>型號 / 規格</th>
						<td><?php echo $query['psku']?> <?php echo !empty($query['attr1']) ? ' / '.$query['attr1'] : ''?> <?php echo !empty($query['attr2']) ? ' / '.$query['attr2'] : ''?></td>
					</tr>
					
				    <tr>
						<th>數量</th>
						<td><?php echo $query['entity']?></td>
					</tr>
					
					
					<tr>
						<th colspan="2">訂購人資訊</th>
					</tr>
					
				    <tr>
						<th>會員帳號</th>
						<td>
						  <?php if(!empty($query['customer_id'])) {  ?>
						   <a href="<?php echo getAdminURL('customer/edit?customer_id='.$query['customer_id'])?>" target="_new"><?php echo get_assign_field('customer', 'customer_id', $query['customer_id'], 'username')?></a>
						 <?php } ?> 
						</td>
					</tr>
					
					<tr>
						<th>收件人姓名</th>
						<td><?php echo $query['name']?></td>
					</tr>
					
					<tr>
						<th>收件人電話</th>
						<td><?php echo $query['phone']?></td>
					</tr>

					<tr>
						<th>收件人Email</th>
						<td><?php echo $query['email']?></td>
					</tr>

					<tr>
						<th>收件人地址</th>
						<td><?php echo $query['local'].' / '.$query['address']?></td>
					</tr>
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>是否通知</th>
						<td>
						    <input type="radio" name="is_notice" value="1"
						    <?php if( isset($query['is_notice']) && ($query['is_notice'] == '1') ) echo set_radio('is_notice', '1' ,true);
						      else echo set_radio('is_notice', '1'); ?>>已通知 
						    <input type="radio" name="is_notice" value="0"
							<?php if( isset($query['is_notice']) && ($query['is_notice'] == '0') ) echo set_radio('is_notice', '0' ,true);
							  else echo set_radio('is_notice', '0'); ?>>未通知
						</td>
					</tr>	
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>狀態</th>
						<td>
						    <input type="radio" name="status" value="2"
						    <?php if( isset($query['status']) && ($query['status'] == '2') ) echo set_radio('status', '2' ,true);
						      else echo set_radio('status', '2'); ?>>已處理 
						    <input type="radio" name="status" value="1"
						    <?php if( isset($query['status']) && ($query['status'] == '1') ) echo set_radio('status', '1' ,true);
						      else echo set_radio('status', '1'); ?>>處理中 
						    <input type="radio" name="status" value="0"
							<?php if( isset($query['status']) && ($query['status'] == '0') ) echo set_radio('status', '0' ,true);
							  else echo set_radio('status', '0'); ?>>未處理
						</td>
					</tr>	
					
					<tr>
						<th>備註</th>
						<td><?php echo $query['memo']?></td>
					</tr>

					<tr>
						<th>建立日期</th>
						<td><?php echo $query['cdate']?></td>
					</tr>
					
					<tr>
						<th colspan="2">系統管理</th>
					</tr>	
					
					<tr>
						<th>管理人員備註</th>
						<td><textarea name="memo_admin" rows="10" cols="100"><?php echo isset($query['memo_admin']) ? $query['memo_admin'] : set_value('memo_admin')?></textarea></td>
					</tr>
				
					<tr>
						<th></th>
						<td><div class="buttons">
					         <input type="submit" value="送出" />
					         <button type="button" onclick="javascript:location.href='<?php echo getAdminURL('order_pre/lists?page=1')?>'">返回</button>
				            </div>
						</td>
					</tr>					
				

				</table>

				</form>
			</div>

		</div>
	</div>
	
</body>
</html>
