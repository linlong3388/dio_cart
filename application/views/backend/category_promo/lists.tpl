<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'category_promo' ,getAdminMenu())?></h2></div>
	
		<div id="content">

			<span style="float: right">
				<div class="actions">
					<a href="javascript:location.href='<?php echo getAdminURL('category_promo/add')?>'"
						class="btn btn-default btn-sm"> <i class="fa fa-pencil"></i> 新增</a>
				</div> </span>

				<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">
			
			 <?php if($this->session->flashdata('msg')){ ?>				 
		        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	         <?php } ?> 			
			
				<!-- (一)搜尋主體 -->

				<p></p>

				<!-- (二)列表主體 -->
				<div class="clearfix">

					<table class="dio_datatable">
						<thead>
							<tr>
								<!-- <td>編號</td> -->
								<!-- <th>分類</th> -->  
								<td>圖示</td>
								<td>標題</td>
								<td>活動日期(起)</td>
								<td>活動日期(迄)</td>
								<td>折扣(%)</td>
								<td>可購數量(%)</td>
								<td>預設活動</td>
								<td>排序</td>
								<td>功能</td>
							</tr>
						</thead>

						<tbody>

						<?php foreach ($query as $row){ ?>
							<tr>
							    <!-- <td class="center"><?php //echo $row['category_promo_id']?></td> -->
								<!-- <td class="center"><?php //echo reSortArrayMutil(2,$row['category_promo_category_id'] ,transKeyPairArray(getcategory_promoCategory() ,'category_promo_category_id' ,'name'))?></td> -->
								<td class="center"><img src="<?php echo base_url(DIO_PATH_PIMG.'/'.$row['image'])?>" width="auto" height="100px"/></td>
								<td class="center"><?php echo $row['name']?></td>
								<td class="center"><?php echo substr($row['sdate'],0,10)?></td>
								<td class="center"><?php echo substr($row['edate'],0,10)?></td>
								<td class="center"><?php echo $row['rate_price']?></td>
								<td class="center"><?php echo $row['rate_entity']?></td>
								<td class="center"><?php echo ($row['status'] == '1') ? "啟用" : "<b style='color:red'>停用</b>"?></td>
								<td class="center"><?php echo $row['sort_order']?></td>
								<td class="center">
								   <a class="btn btn-info" href="<?php echo getAdminURL('category_promo_product/lists?category_promo_id='.$row['category_promo_id'])?>">
										 商品 (<?php echo $row['count']?>)</a>
								   <a class="btn btn-info" href="<?php echo getAdminURL('category_promo/edit?category_promo_id='.$row['category_promo_id'])?>">
										<i class="fa fa-pencil-square-o"></i> 編輯</a> 
								   <a class="btn btn-danger" href="javascript:my_confirm(<?php echo $row['category_promo_id']?>);">
										<i class="fa fa-trash"></i> 刪除</a>
								</td>
							</tr>
							<?php } ?>

						</tbody>

					</table>
				</div>

			</div>


		</div>

	</div>

	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script type="text/javascript">
	 $(document).ready(function() {
        $('.dio_datatable').dataTable({
        	 'aaSorting': [[ 7, 'desc' ]] ,
          	 "language": { 
   		         url : "<?php echo base_url("application/views/backend/js/dataTables/i18n/chinese_traditional.lang.json")?>"
             },
             "iDisplayLength": 50
         });
    });
   	
    function my_confirm(category_promo_id){
       var chk_box=confirm('確定刪除資料?');
       if (chk_box == true) {
             location.href = '<?php echo base_url("index.php/backend/category_promo/del?category_promo_id=") ?>'+category_promo_id;
       }
    }
</script>

</body>
</html>




