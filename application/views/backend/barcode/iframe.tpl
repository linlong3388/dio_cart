<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>elFinder 2.0</title>

<!-- elfinder 的專屬 css -->
<link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>" rel="stylesheet">
<link href="<?php echo base_url('application/views/backend/css/elfinder.min.css')?>" rel="stylesheet">
<link href="<?php echo base_url('application/views/backend/css/theme.css')?>" rel="stylesheet">

<!-- elfinder 的專屬 js -->
<script src="<?php echo base_url('application/views/backend/js/jquery-1.7.2.min.js');?>"></script>
<script src="<?php echo base_url('application/views/backend/js/jquery-ui-1.8.21.custom.min.js');?>"></script>
<script src="<?php echo base_url('application/views/backend/js/elfinder.min.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/i18n/elfinder.zh_CN.js')?>"></script>

</head>
<body>

<style>
  .star{ color:red }
</style>

	<div id="page-wrapper">

		<!-- /.row -->
		<div class="row">
		
		    <div>
		       <b class="star">★</b> 此為商品國際條碼欄位所產生的對應條碼圖檔<br/>
		    </div>
		    
		    <p>		
		
			<div class="file-manager"></div>
		</div>
		<!-- /.row -->

	</div>
	<!-- /#page-wrapper -->


	<script type="text/javascript" charset="utf-8">
	$(function() {
		   var elf = $('.file-manager').elfinder({
		              url : 'init',  // connector URL (REQUIRED)
           	      lang : 'zh_CN',
           	       
		              //功能鈕
		              uiOptions : {
		            	    // toolbar configuration
		            	    toolbar : [
		            	        ['back', 'forward'],
		            	        ['info'],
		            	        ['quicklook'],
		            	        ['rm'],
		            	        ['extract', 'archive'],
		            	        ['view'],
		            	        ['help']
		            	    ]},

		             defaultView : 'list',
		            	    
                     //右鍵鈕
		             contextmenu : {		            	    
		            	    cwd    : ['reload', 'back', '|' , 'info'], 
		            	    files  : ['getfile', '|','open', 'quicklook', '|', 'download', '|' , 'rm', '|', 'archive', 'extract', '|', 'info']
			              }        
		            	    
	             }).elfinder('instance');

		});
   </script>

</body>

</html>
