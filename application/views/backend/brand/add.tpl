<body>

<style>
.fa-star {
	color: red
}
td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'brand' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			
			 <?php if($this->session->flashdata('msg')){ ?>				 
		        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	         <?php } ?> 
			
			 <b style="color: red"><?php echo validation_errors(); ?></b> 
			 
			<?php echo form_open( current_url())?>
				<table>
					<tr>
						<th colspan="2">新增資料</th>
					</tr>
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>標題</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="title" value="<?php echo set_value('title')?>"></td>
					</tr>

					<tr id="hd_image">
						<th width="20%"><i class="fa fa-star"></i>logo<br>(400x160)</th>
						<td style="text-align: left;">
	                        <input style="display: none" class="input-medium focused" id="specification_logo" name="logo" value="<?php echo isset($query['logo']) ? $query['logo'] : set_value('logo') ;?>">
   	                           <div class="span12">
   	                             <img id="logo" class="img-responsive" width="200px" height="180px" src="<?php echo (!empty($query['logo'])) ? base_url("resources/uploads/".$query['logo']) : base_url("resources/uploads").'/'.set_value('logo')?>" alt="圖片"/>
                                    <p>                                                       
                                       <a onclick="base_image_upload('logo');">選擇圖片</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                       <a onclick="base_image_delete('logo');">刪除圖片</a>
                                    </p>
		                       </div>          
						</td>
					</tr>
					
					<!-- 
					<tr id="hd_image">
						<th width="20%">橫幅</th>
						<td style="text-align: left;">
	                        <input style="display: none" class="input-medium focused" id="specification_image" name="image" value="<?php //echo isset($query['image']) ? $query['image'] : set_value('image') ;?>">
   	                           <div class="span12">
   	                             <img id="image" class="img-responsive" width="300px" height="250px" src="<?php //echo (!empty($query['image'])) ? base_url("resources/uploads/".$query['image']) : base_url("resources/uploads").'/'.set_value('image')?>" alt="圖片"/>
                                    <p>                                                       
                                       <a onclick="base_image_upload('image');">選擇圖片</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                       <a onclick="base_image_delete('image');">刪除圖片</a>
                                    </p>
		                       </div>          
						</td>
					</tr>
					 -->
					<!-- 
					<tr>
						<th width="20%"><i class="fa fa-star"></i>內容</th>
						<td><textarea class="fullwidth dio_textarea" rows="3" name="content" placeholder="請輸入資料"><?php //echo set_value('content')?></textarea></td>
					</tr>
					 -->
					
				</table>

				<div class="buttons">
					<input type="submit" value="送出" />
					<button type="button"
						onclick="javascript:location.href='<?php echo getAdminURL('brand/lists')?>'">返回</button>
				</div>

				</form>
			</div>

		</div>

	</div>

<link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>" rel="stylesheet">
<link href="<?php echo base_url('application/views/backend/css/elfinder.min.css')?>" rel="stylesheet">
<link href="<?php echo base_url('application/views/backend/css/theme.css')?>" rel="stylesheet">

<script src="<?php echo base_url('application/views/backend/js/jquery-1.7.2.min.js');?>"></script>
<script src="<?php echo base_url('application/views/backend/js/jquery-ui-1.8.21.custom.min.js');?>"></script>
<script src="<?php echo base_url('application/views/backend/js/elfinder.min.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/i18n/elfinder.zh_CN.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_image_upload.js')?>"></script>

<script src="<?php echo base_url('application/views/backend/js/tinymce/tinymce.min.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_tinymce_elfinder.js')?>"></script>



</body>
</html>
