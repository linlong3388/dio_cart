<body>

<style>
.fa-star {
	color: red
}
td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'index' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			
			 <?php if($this->session->flashdata('msg')){ ?>				 
		        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	         <?php } ?> 
			
			 <b style="color: red"><?php echo validation_errors(); ?></b> 			
			
			<?php echo form_open( getCurrentFullUrl() )?>
				<table>
					<tr>
						<th colspan="2">編輯資料/底部區塊</th>
					</tr>
									
					<tr><th width="20%">連結(1)</th>
						<td><input style="width: 60%;" placeholder="請輸入資料" type="text" name="blockA_url" value="<?php echo isset($xml->blockA->url) ? $xml->blockA->url : set_value('blockA_url')?>"></td>
					</tr>						
									
					<tr><th width="20%">圖片(1)</th>
						<td><input style="display: none" class="input-medium focused" id="specification_image1" name="blockA_img" value="<?php echo isset($xml->blockA->img) ? $xml->blockA->img : set_value('blockA->img') ;?>">
     	                    <div class="span12">
			                   <img id="blockA_img" class="img-responsive" width="300px" height="250px"  src="<?php echo ( !empty($xml->blockA->img) ) ? base_url("resources/uploads/".$xml->blockA->img) : base_url("resources/uploads").'/'.set_value('blockA_img')?>" alt="圖片"/>
                                  <p>                                                       
                                     <a onclick="base_image_upload('blockA_img');">選擇圖片</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                     <a onclick="base_image_delete('blockA_img');">刪除圖片</a>
                                  </p>
		                     </div>          
						</td>
					</tr>
					
					<tr><th width="20%">連結(2)</th>
						<td><input style="width: 60%;" placeholder="請輸入資料" type="text" name="blockB_url" value="<?php echo isset($xml->blockB->url) ? $xml->blockB->url : set_value('blockB_url')?>"></td>
					</tr>
					
					<!-- 
					<tr><th width="20%">標題(2)</th>
						<td><input style="width: 60%;" placeholder="請輸入資料" type="text" name="blockB_title" value="<?php //echo isset($xml->blockB->title) ? $xml->blockB->title : set_value('blockB_title')?>"></td>
					</tr>	

					<tr><th width="20%">簡述(2)</th>
						<td><input style="width: 60%;" placeholder="請輸入資料" type="text" name="blockB_title_sub" value="<?php //echo isset($xml->blockB->title_sub) ? $xml->blockB->title_sub : set_value('blockB_title_sub')?>"></td>
					</tr>
					 -->
					
					<tr><th width="20%">圖片(2)</th>
						<td><input style="display: none" class="input-medium focused" id="specification_image1" name="blockB_img" value="<?php echo isset($xml->blockB->img) ? $xml->blockB->img : set_value('blockB->img') ;?>">
     	                    <div class="span12">
			                   <img id="blockB_img" class="img-responsive" width="300px" height="250px"  src="<?php echo ( !empty($xml->blockB->img) ) ? base_url("resources/uploads/".$xml->blockB->img) : base_url("resources/uploads").'/'.set_value('blockB_img')?>" alt="圖片"/>
                                  <p>                                                       
                                     <a onclick="base_image_upload('blockB_img');">選擇圖片</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                     <a onclick="base_image_delete('blockB_img');">刪除圖片</a>
                                  </p>
		                     </div>          
						</td>
					</tr>
					 
					
					<!-- 
					<tr><th width="20%">連結(3)</th>
						<td><input style="width: 60%;" placeholder="請輸入資料" type="text" name="blockC_url" value="<?php //echo isset($xml->blockC->url) ? $xml->blockC->url : set_value('blockC_url')?>"></td>
					</tr>
					
					<tr><th width="20%">標題(3)</th>
						<td><input style="width: 60%;" placeholder="請輸入資料" type="text" name="blockC_title" value="<?php //echo isset($xml->blockC->title) ? $xml->blockC->title : set_value('blockC_title')?>"></td>
					</tr>	

					<tr><th width="20%">簡述(3)</th>
						<td><input style="width: 60%;" placeholder="請輸入資料" type="text" name="blockC_title_sub" value="<?php //echo isset($xml->blockC->title_sub) ? $xml->blockC->title_sub : set_value('blockC_title_sub')?>"></td>
					</tr>
					
					<tr><th width="20%">圖片(3)<br>(最大1000x1000)</th>
						<td><input style="display: none" class="input-medium focused" id="specification_image1" name="blockC_img" value="<?php //echo isset($xml->blockC->img) ? $xml->blockC->img : set_value('blockC->img') ;?>">
     	                    <div class="span12">
			                   <img id="blockC_img" class="img-responsive" width="300px" height="250px"  src="<?php //echo ( !empty($xml->blockC->img) ) ? base_url("resources/uploads/".$xml->blockC->img) : base_url("resources/uploads").'/'.set_value('blockC_img')?>" alt="圖片"/>
                                  <p>                                                       
                                     <a onclick="base_image_upload('blockC_img');">選擇圖片</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                     <a onclick="base_image_delete('blockC_img');">刪除圖片</a>
                                  </p>
		                     </div>          
						</td>
					</tr>
					 -->
					
					<!-- 
					<tr>
						<th width="20%">內容</th>
						<td><textarea class="fullwidth dio_textarea" rows="3" name="blockA_content" placeholder="請輸入資料"><?php //echo isset($xml->blockA->content) ? $xml->blockA->content : set_value('blockA_content')?></textarea></td>
					</tr>
					 -->					
		
					
					<tr>
						<th colspan="2">Meta頁籤</th>
					</tr>
					
					<tr>
						<th width="20%">Meta標籤標題</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="meta_title" value="<?php echo isset($xml->meta->title) ? $xml->meta->title : set_value('meta_title')?>">
						</td>
					</tr>
					
					<tr>
						<th width="20%">Meta標籤描述</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="meta_description" value="<?php echo isset($xml->meta->description) ? $xml->meta->description : set_value('meta_description')?>">
						</td>
					</tr>
					
					<tr>
						<th width="20%">Meta標籤關鍵字</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="meta_keyword" value="<?php echo isset($xml->meta->keyword) ? $xml->meta->keyword : set_value('meta_keyword')?>">
						</td>
					</tr>						

				</table>

				<div class="buttons">
					<input type="submit" value="送出" />
					<button type="button"
						onclick="javascript:location.href='<?php echo getAdminURL('news/lists')?>'">返回</button>
				</div>

				</form>
			</div>

		</div>

	</div>

<script src="<?php echo base_url('application/views/backend/js/tinymce/tinymce.min.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_tinymce_elfinder.js')?>"></script>

<link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>" rel="stylesheet">
<link href="<?php echo base_url('application/views/backend/css/elfinder.min.css')?>" rel="stylesheet">
<link href="<?php echo base_url('application/views/backend/css/theme.css')?>" rel="stylesheet">

<script src="<?php echo base_url('application/views/backend/js/jquery-1.7.2.min.js');?>"></script>
<script src="<?php echo base_url('application/views/backend/js/jquery-ui-1.8.21.custom.min.js');?>"></script>
<script src="<?php echo base_url('application/views/backend/js/elfinder.min.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/i18n/elfinder.zh_CN.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_image_upload.js')?>"></script>

</body>
</html>
