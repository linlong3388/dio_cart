<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'product_comb' ,getAdminMenu())?></h2></div>

		<div id="content">

			<span style="float: right">
				<div class="actions">
					<a href="javascript:location.href='<?php echo getAdminURL('product_comb/add?product_id='.$product_id)?>';" class="btn btn-default btn-sm"> 
					<i class="fa fa-pencil"></i> 新增</a>
					
					<a href="javascript:location.href='<?php echo getAdminURL('product/lists')?>';" class="btn btn-default btn-sm"> 
					<i class="fa fa-mail-reply"></i> 返回</a>
				</div> 
		    </span>

			<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">
			
			<?php if($this->session->flashdata('msg')){ ?>				 
				  <div class="alert alert-success"><i class="fa fa-check-circle"></i><?php echo $this->session->flashdata('msg')?></div>
			<?php }elseif($this->session->flashdata('msg_err')){ ?>
				  <div class="alert alert-danger"><i class="fa fa-times-circle"></i> <?php echo $this->session->flashdata('msg_err')?></div>
			<?php } ?>			

				<div class="clearfix">
					<table class="dio_datatable">

						<thead>
							<tr>
								<td>圖示</td>
								<td>禮盒類型</td>
								<td>每組/入</td>
								<!-- <td>價格</td> -->
								<td>排序</td>
								<td>商品狀態</td>
								<td>建立日期</td>
								<td>功能</td>
							</tr>
						</thead>

						<tbody>
						<?php foreach ($query as $row) { ?>
							<tr>
								<td><img width="50px" height="50px" src="<?php echo base_url('resources/uploads/'.$row['image'])?>"></td>
								<td><?php echo reSortArrayMutil(2,$row['unit'],getProductType1Unit())?></td>
								<td class="center"><?php echo $row['unit_pre']?> 組 <?php echo $row['entity']?> 入</td>
							    <!-- <td class="center"><?php //echo DIO_CURRENCY.$row['price']?></td> -->
								<td class="center"><?php echo $row['sort_order']?></td>
								<td><?php echo ($row['status'] == '1') ? "正常" : "<b style='color:red'>停用</b>"?></td>
								<td><?php echo substr($row['cdate'], 0,10) ?><b style="display: none"><?php echo substr($row['cdate'], 11) ?></td>
								<td><a class="btn btn-success" href="<?php echo getAdminURL('product_comb/edit?product_id='.$row['product_id'].'&product_comb_id='.$row['product_comb_id'])?>">
										<i class="fa fa-pencil-square-o"></i> 編輯</a> 
									<a class="btn btn-danger" href="javascript:my_confirm('<?php echo $row['product_id']?>','<?php echo $row['product_comb_id']?>');">
										<i class="fa fa-trash"></i> 刪除</a>
								</td>
							</tr>
							<?php } ?>

						</tbody>
					</table>

				</div>
			</div>

		</div>
	</div>

	<link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>" rel="stylesheet">
	<script src="<?php echo base_url('application/views/backend/js/jquery-ui-1.11.1.min.js')?>"></script>
	<script src="<?php echo base_url('application/views//backend/js/dio_plugins/jquery_fields_date.js')?>"></script>

<script type="text/javascript">
	 $(document).ready(function() {
	        $('.dio_datatable').dataTable({
	        	 'aaSorting': [[ 0, 'desc' ]] ,
	          	 "language": { 
	   		         url : "<?php echo base_url("application/views/backend/js/dataTables/i18n/chinese_traditional.lang.json")?>"
	             },
	             "iDisplayLength": 50
	         });
	    });	
	
    function my_confirm(  product_id ,product_comb_id ){
       var chk_box=confirm('確定刪除資料?');
       if (chk_box == true) {
             location.href = '<?php echo base_url("index.php/backend/product_comb/del?product_id=") ?>' + product_id + '&product_comb_id=' + product_comb_id;
       }
    }
    </script>

</body>
</html>
