<body>

<style>
.img-responsive {
	width: 200px;
	height: 200px;
	border: 1px solid #999;
}

.page_wrap--data_area--main_buyer--table--ul li {
	width: 200px;
	height: 200px;
}

.page_wrap--data_area--main_buyer--table--ul li input[type="file"] {
	margin-top: 20px;
}

.fa-star {
	color: red
}

td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'product_comb' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			
			 <b style="color: red"><?php echo validation_errors(); ?></b>
			
			<?php echo form_open( getCurrentFullUrl() ,array( 'enctype' => 'multipart/form-data'))?>
				<table>
					<tr>
						<th colspan="2">新增組圖</th>
					</tr>
					
					<tr style="display: none">
						<th width="20%">商品ID</th>
						<td><input type="text" name="product_id" value="<?php echo $product_id?>"></td>
					</tr>
					
					<tr>
						<th><i class="fa fa-star"></i>禮盒類型</th>
						<td><select name="unit">
					          <?php foreach ( getProductType1Unit() as $key=>$val) { ?>
 							     <option value="<?php echo $key?>"><?php echo $val?></option>
							  <?php } ?>
							</select> 
						</td>
					</tr>

					<tr>
						<th width="20%"><i class="fa fa-star"></i>組</th>
						<td><input placeholder="請輸入數字" type="text" name="unit_pre" value="<?php echo set_value('unit_pre')?>" onkeypress="def_numeric()"></td>
					</tr>
				   
					<tr>
						<th width="20%"><i class="fa fa-star"></i>入</th>
						<td><input placeholder="請輸入數字" type="text" name="entity" value="<?php echo set_value('entity')?>" onkeypress="def_numeric()"></td>
					</tr>
					
					<!-- 
					<tr>
						<th width="20%"><i class="fa fa-star"></i>價格</th>
						<td><input placeholder="請輸入數字" type="text" name="price" value="<?php //echo set_value('price')?>" onkeypress="def_numeric()"></td>
					</tr>
					 -->
				   
				   <tr>
					   <th width="20%"><i class="fa fa-star"></i>圖片<br><span style="color: red">12入：240x628<br>8入：240x410</span></th>
					   <td>
					       <input style="display: none" class="input-medium focused" name="image" value="<?php echo isset($query['image']) ? $query['image'] : set_value('image') ;?>">
     	                   <img id="image" class="img-responsive" src="<?php echo (!empty($query['image'])) ? base_url("resources/uploads/".$query['image']) : base_url("resources/uploads").'/'.set_value('image')?>" alt="圖片"/>
                              <p>                                                       
                                 <a onclick="base_image_upload('image');">選擇圖片</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                 <a onclick="base_image_delete('image');">刪除圖片</a>
                              </p>
		               </td>
				   </tr>
				   
				   <!-- 
				   <tr>
						<th width="20%">圖片說明</th>
						<td><input placeholder="請輸入資料" type="text" name="image_alt" value="<?php //echo set_value('image_alt')?>"></td>
					</tr>
				  -->
				
				</table>

				<div class="buttons">
					<!-- <input type="submit" value="下一步" />  -->
					<input type="submit" value="送出" />
					<button type="button"
						onclick="javascript:location.href='<?php echo getAdminURL('product_comb/lists?product_id='.$product_id)?>'">返回</button>
				</div>

				</form>
			</div>

		</div>

	</div>
	
    <link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('application/views/backend/css/elfinder.min.css')?>" rel="stylesheet">
    
    <script src="<?php echo base_url('application/views/backend/js/jquery-1.7.2.min.js');?>"></script>
    <script src="<?php echo base_url('application/views/backend/js/jquery-ui-1.8.21.custom.min.js');?>"></script>
    <script src="<?php echo base_url('application/views/backend/js/elfinder.min.js')?>"></script>
    <script src="<?php echo base_url('application/views/backend/js/i18n/elfinder.zh_CN.js')?>"></script>
	<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_image_upload.js')?>"></script>
	<script src="<?php echo base_url('application/views/backend/js/tinymce/tinymce.min.js')?>"></script>
	<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_tinymce_elfinder.js')?>"></script>

</body>
</html>
