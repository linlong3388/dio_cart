<body>

<style>
.fa-star {
	color: red
}
td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'content' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			
			 <?php if($this->session->flashdata('msg')){ ?>				 
		        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	         <?php } ?> 
			
			 <b style="color: red"><?php echo validation_errors(); ?></b> 
			 
			<?php echo form_open( current_url())?>
				<table>
					<tr>
						<th colspan="2">新增資料</th>
					</tr>
					
					<!-- 
				    <tr>
						<th width="20%"><i class="fa fa-star"></i>分類</th>
						<td>
						   <select name="content_category_id">
						    <?php //foreach (getContentCategory() as $key=>$val){ ?>
						      <option value="<?php //echo $key?>"><?php //echo $val?></option>
						    <?php //} ?>
						    </select>
					</tr>
					 -->
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>標題</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text" name="title" value="<?php echo set_value('title')?>"></td>
					</tr>
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>內容</th>
						<td><textarea class="fullwidth dio_textarea" rows="3" name="content" placeholder="請輸入資料"><?php echo set_value('content')?></textarea></td>
					</tr>
				</table>

				<div class="buttons">
					<input type="submit" value="送出" />
					<button type="button"
						onclick="javascript:location.href='<?php echo getAdminURL('content/lists')?>'">返回</button>
				</div>

				</form>
			</div>

		</div>

	</div>
	
<script src="<?php echo base_url('application/views/backend/js/tinymce/tinymce.min.js')?>"></script>
<script src="<?php echo base_url('application/views/backend/js/dio_plugins/jquery_tinymce_elfinder.js')?>"></script>
	

</body>
</html>
