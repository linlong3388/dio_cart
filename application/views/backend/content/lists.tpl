<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'content' ,getAdminMenu())?></h2></div>

		<div id="content">

			<span style="float: right;">
				<div class="actions">
					<a href="javascript:location.href='<?php echo getAdminURL('content/add')?>'" class="btn btn-default btn-sm"> <i class="fa fa-pencil"></i> 新增</a>
				</div> 
			</span>
			
			<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">

			 <?php if($this->session->flashdata('msg')){ ?>				 
		        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	         <?php } ?> 			
			
				<!-- (一)搜尋主體 -->

				<p></p>

				<!-- (二)列表主體 -->
				<div class="clearfix">

					<table class="dio_datatable">
						<thead>
							<tr>
							   <!--  <td>分類</td>  -->
								<td>標題</td>
								<td>內容</td>
								<td>建立日期</td>
								<!-- <td>排序</td>  -->
								<td>功能</td>
							</tr>
						</thead>

						<tbody>

						<?php foreach ($query as $row){ ?>
							<tr>
							    <!-- <td class="center"><?php //echo reSortArrayMutil(2 ,$row['content_category_id'] ,getContentCategory())?></td>  -->
							    <td class="center"><?php echo $row['title']?></td>
								<td class="center"><?php echo mb_substr(strip_tags($row['content']) , 0 ,50 ,"UTF-8")."..." ;?></td>
								<td class="center"><?php echo substr($row['cdate'], 0,10) ?><b style="display: none"><?php echo substr($row['cdate'], 11) ?></td>
								<td class="center">
								   <a class="btn btn-info" href="<?php echo getAdminURL('content/edit?content_id='.$row['content_id'])?>">
										<i class="fa fa-pencil-square-o"></i> 編輯</a> 
								   <a class="btn btn-danger" href="javascript:my_confirm(<?php echo $row['content_id']?>);">
										<i class="fa fa-trash"></i> 刪除</a>
								</td>
							</tr>
							<?php } ?>

						</tbody>

					</table>
				</div>

			</div>


		</div>

	</div>

	<!-- Page-Level Demo Scripts - Tables - Use for reference -->
	<script type="text/javascript">
	 $(document).ready(function() {
        $('.dio_datatable').dataTable({
        	 'aaSorting': [[ 0, 'desc' ]] ,
          	 "language": { 
   		         url : "<?php echo base_url("application/views/backend/js/dataTables/i18n/chinese_traditional.lang.json")?>"
             },
             "iDisplayLength": 50
         });
    });
   	
    function my_confirm(content_id){
       var chk_box=confirm('確定刪除資料?');
       if (chk_box == true) {
             location.href = '<?php echo base_url("index.php/backend/content/del?content_id=") ?>'+content_id;
       }
    }
</script>

</body>
</html>




