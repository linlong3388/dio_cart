<body>

<style>
.fa-star {
	color: red;
}
td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'customer' ,getAdminMenu())?></h2></div>

		<div id="content">
		
			<span style="float: right">
				<div class="actions">
				    <!-- 
				    <a href="javascript:location.href='<?php //echo getAdminURL('customer_coupon/lists?customer_id='.$this->input->get("customer_id"))?>' " class="btn btn-default btn-sm"> 
					<i class="fa fa-pencil"></i> 優惠券</a>           
					 -->
					
					<a href="javascript:location.href='<?php echo getAdminURL('customer/lists?page=1')?>' " class="btn btn-default btn-sm"> 
					<i class="fa fa-reply"></i> 返回列表</a>             
				</div> 
		    </span>

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>
		
			<div class="container editform">

			<?php if($this->session->flashdata('msg')){ ?>				 
			    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
		    <?php } ?>			
		    
			 <b style="color: red"><?php echo validation_errors(); ?></b> 		    
		 
			<?php echo form_open(current_url().'?'.$_SERVER["QUERY_STRING"])?>
				<table>
					<tr>
						<th colspan="2">編輯資料</th>
					</tr>

					<tr style="display: none">
						<th width="20%">會員編號</th>
						<td><input  type="text"
							name="customer_id"
							value="<?php echo isset($query['customer_id']) ? $query['customer_id'] : set_value('customer_id')?>"
							readonly>
						</td>
					</tr>
					
					<tr>
						<th width="20%">登入類型</th>
						<td><?php echo reSortArrayMutil(2 ,$query['log_id'] ,getLoginType())?></td>
					</tr>
                	
					<tr>
						<th width="20%">帳號</th>
						<td><input  type="text" name="username"
							value="<?php echo isset($query['username']) ? $query['username'] : set_value('username')?>"
							readonly> 
						</td>
					</tr>
					
					<tr>
						<th width="20%">Email</th>
						<td><input  type="text" name="email"
							value="<?php echo isset($query['email']) ? $query['email'] : set_value('email')?>"
							readonly> 
						</td>
					</tr>

					<tr>
						<th width="20%">姓名</th>
						<td><!-- 
						     <input  type="text" name="first_name" value="<?php //echo isset($query['first_name']) ? $query['first_name'] : set_value('first_name')?>" readonly> 
						     -->
						    <input  type="text" name="last_name" value="<?php echo isset($query['last_name']) ? $query['last_name'] : set_value('last_name')?>" readonly> 
						</td>
					</tr>
					
					<tr>
						<th width="20%">生日</th>
						<td>年 <input type="text" name="birthday_year"
							value="<?php echo isset($query['birthday_year']) ? $query['birthday_year'] : set_value('birthday_year')?>"
							readonly>  
							月 <input type="text" name="birthday_month"
							value="<?php echo isset($query['birthday_month']) ? addZero($query['birthday_month']) : set_value('birthday_month')?>"
							readonly>  
							日 <input type="text" name="birthday_day"
							value="<?php echo isset($query['birthday_day']) ? addZero($query['birthday_day']) : set_value('birthday_day')?>"
							readonly> </td>
					</tr>
				
					<!--  
					<tr>
						<th width="20%">推薦人</th>
						<td><input class="fullwidth"  type="text"
							name="recommend_name"
							value="<?php //echo isset($query['recommend_name']) ? $query['recommend_name'] : set_value('recommend_name')?>"
							readonly> 
						</td>
					</tr>
                     -->
					
					<!-- 
					<tr>
						<th width="20%">手機</th>
						<td><input  type="text" name="mobile"
							value="<?php //echo isset($query['mobile']) ? $query['mobile'] : set_value('mobile')?>"
							readonly> 
						</td>
					</tr>
					 -->
					
					<tr>
						<th width="20%">電話</th>
						<td><input  type="text" name="phone"
							value="<?php echo isset($query['phone']) ? $query['phone'] : set_value('phone')?>" readonly> 
						</td>
					</tr>

					<tr>
						<th width="20%">地址</th>
						<td><input type="text" name="local"
							value="<?php echo isset($query['local']) ? $query['local'] : set_value('local')?>" readonly> 
						
						    <input type="text" name="address"
							value="<?php echo isset($query['address']) ? $query['address'] : set_value('address')?>" readonly> 
						
						</td>
					</tr>

					<!-- 
					<tr>
						<th width="20%">會員等級</th>
						<td><?php //if( $query['level'] == '0' ) {	?>
						   	   <b>一般會員</b>
						   <?php //}elseif( ($query['level'] == '1') || ($query['level'] == '2') ){ ?>
						       <b>VIP會員 (生效日期：<?php //echo substr($query['vip_sdate'],0,10)?>至<?php //echo substr($query['vip_edate'],0,10)?>)</b>
						   <?php //} ?>	
						    <input type="radio" name="level" value="1" <?php //echo ($query['level'] == '1') ? ' checked' : '' ?>> 一般會員 
						    <input type="radio" name="level" value="2" <?php //echo ($query['level'] == '2') ? ' checked' : '' ?>> VIP會員
						</td>
					</tr>
					 -->
					
					<tr>
						<th width="20%"><i class="fa fa-star"></i>會員狀態</th>
						<td><input type="radio" name="status" value="1"
						<?php echo ($query['status'] == '1') ? ' checked' : '' ?>> 啟用 <input
							type="radio" name="status" value="0"
							<?php echo ($query['status'] == '0') ? ' checked' : '' ?>> 停用</td>
					</tr>

					<tr>
						<th width="20%">管理員備註</th>
						<td><textarea class="fullwidth" rows="10" name="memo_admin"><?php echo isset($query['memo_admin']) ? $query['memo_admin'] : set_value('memo_admin')?></textarea></td>
					</tr>
					
					<tr>
						<th width="20%">建立日期</th>
						<td><input type="text" name="cdate"
							value="<?php echo isset($query['cdate']) ? $query['cdate'] : set_value('cdate') ;?>"
							readonly> 
						
						</td>
					</tr>

					<tr>
						<th width="20%">最後更新日期</th>
						<td><input type="text" name="udate"
							value="<?php echo isset($query['udate']) ? $query['udate'] : set_value('udate') ;?>"
							readonly> 
						
						</td>
					</tr>
					
					<tr>
						<th colspan="2">其他資訊</th>
					</tr>
					
					<tr>
						<th>常用地址</th>
						<td>
						
							<table id="tb_product_speci" class="dio_datatable">
								<thead>
									<tr>
										<th>收件人</th>
										<th>電話</th>
										<th>地址</th>
										<th>排序</th>
									</tr>
								</thead>

								<tbody>
								<?php foreach ( $query_addr as $row) { ?>
									<tr>
									    <td align="center"><?php echo $row['last_name']?></td>
									    <td align="center"><?php echo $row['phone']?></td>
									    <td align="center"><?php echo $row['local'] .' / '. $row['address']?></td>
										<td align="center"><?php echo $row['sort_order']?></td>
									</tr>
							   <?php } ?>
								</tbody>
							</table>
								
						</td>
					</tr>
					
					<!-- 
					<tr>
						<th>超商門市</th>
						<td>
						
							<table id="tb_product_speci" class="dio_datatable">
								<thead>
									<tr>
				                       <th>店號</th>
                                       <th>門市名稱</th>
                                       <th>地址</th>
                                       <th>排序</th>
									</tr>
								</thead>

								<tbody>
								<?php //foreach ( $query_store as $row) { ?>
								  <tr>
                                      <td><?php //echo $row['sno']?></td>
                                      <td><?php //echo $row['name']?></td>
                                      <td><?php //echo $row['local'] .' / '. $row['address']?></td>
                                      <td><?php //echo $row['sort_order']?></td>
                            	 </tr>
							   <?php //} ?>
								</tbody>
							</table>
								
						</td>
					</tr>
					 -->

					<!-- 
					<tr>
						<th width="20%">是否訂閱電子報</th>
						<td><?php //echo $query['is_edm'] == 1 ? '是' : '否' ?></td>
					</tr>
					 -->

				</table>

				<div class="buttons">
					<input type="submit" value="送出" /> 
					<button type="button"
						onclick="javascript:location.href='<?php echo getAdminURL('customer/lists?page=1')?>'">返回</button>
				</div>

				</form>
			</div>

		</div>

	</div>

	<script type="text/javascript">
   
    function my_confirm(customer_id){
       var chk_box=confirm('確定刪除資料?');
       if (chk_box == true) {
             location.href = '<?php echo base_url("index.php/backend/customer/del?customer_id=") ?>'+customer_id;
       }
    }
</script>

</body>
</html>

