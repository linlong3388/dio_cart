<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'customer' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">
		
				<!-- (一)搜尋主體 -->
			<?php echo form_open( getCurrentFullUrl()  ,array('method'=>'get'))?>
				<fieldset>
					<legend>搜尋條件:</legend>
                      <input style="display:none" type="text" name="page" value="1"> 
					<!-- 會員編號 : 
					  <input type="text" name="srh_customer_id" value="<?php //echo $this->input->get('srh_customer_id')?>"> 
					 -->
					帳號: 
					  <input type="text" name="srh_username" value="<?php echo $this->input->get('srh_username')?>">
					email(%): 
					  <input type="text" name="srh_email" value="<?php echo $this->input->get('srh_email')?>">
				    <!--      
				           姓(%): 
				      <input type="text" style="width: 10%" name="srh_first_name" value="<?php echo $this->input->get('srh_first_name')?>"> 
				     -->      
				           姓名(%): 
				      <input type="text" style="width: 10%" name="srh_last_name" value="<?php echo $this->input->get('srh_last_name')?>"> 
				    
				           狀態 : 
				      <select name="srh_status" style="width: 10%">
						<?php foreach (srh_customer_list($this->input->get('srh_status')) as $key=>$val) { ?>
						<option value="<?php echo $key?>"><?php echo $val?></option>
						<?php } ?>
					  </select> 
				      
				    <p>    
                     
					建立日期: 
					   <input type="text" name="srh_cdate" class="dio_date" value="<?php echo $this->input->get('srh_cdate')?>">

					排序 : 
					<select name="srh_sort">
					  <?php foreach (reSortArray($this->input->get('srh_sort') ,$data_sort) as $key=>$val) { ?>
						  <option value="<?php echo $key?>"><?php echo $val?></option>
					  <?php } ?>
					</select> 
					
					<input type="radio" name="srh_sort_type" value="DESC" <?php echo $this->input->get('srh_sort_type') == 'DESC' ? " checked" : "" ?> />降冪
					<input type="radio" name="srh_sort_type" value="ASC" <?php echo $this->input->get('srh_sort_type') == 'ASC' ? " checked" : "" ?> />升冪
					
					<input style="display:none" type="text" name="srh_page" value="<?php echo $this->input->get('page')?>"> 
					<input style="display:none" type="text" name="srh_page_per" value="20">
					
					<input type="submit" value="查詢">
                    <input type="submit" name="submit_output" value="資料匯出">
					
				</fieldset>
				</form>

				<p></p>

				<b style="color: red"><?php echo form_error('srh_cdate1')?> </b> <b
					style="color: red"><?php echo form_error('srh_cdate2')?> </b>

				<!-- (二)列表主體 -->
				<div class="clearfix">

					<table class="dio_datatable">
						<thead>
							<tr>
							    <td>登入類型</td>
								<!-- <td>會員ID</td> -->
								<td>帳號</td> 
								<td>email</td>  
								<td>姓名</td>
								<!-- <td>會員等級</td> -->
								<td>會員狀態</td>
								<td>建立日期</td>
								<td>功能</td>
							</tr>
						</thead>
				
						<tbody>

						<?php foreach ($query as $row){ ?>
							<tr>
								<td class="center"><?php echo reSortArrayMutil(2 ,$row['log_id'] ,getLoginType())?></td>
								<!-- <td class="center"><?php //echo $row['customer_id']?></td> -->
								<td class="center"><?php echo $row['username']?></td>
								<td class="center"><?php echo $row['email']?></td>
								<td class="center"><?php echo $row['last_name']?></td>
								<!-- <td class="center"><?php //echo reSortArrayMutil(2,$row['level'],getCustomerLevel())?></td> -->
								<td class="center"><?php echo ($row['status'] == '1') ? "啟用" : "<b style='color :red'>停用</b>"; ?></td>
								<td class="center"><?php echo substr($row['cdate'], 0,10) ?><b style="display: none"><?php echo substr($row['cdate'], 11) ?></td>
								<td class="center">
								  <!-- 
								  <a class="btn btn-success" href="<?php //echo getAdminURL('customer_coupon/lists?customer_id='.$row['customer_id'])?>" target="_blank" />
										<i class="fa fa-eye"></i> 優惠券</a> 
								  <a class="btn btn-success" href="<?php //echo getAdminURL('order/lists?srh_customer_id='.$row['customer_id'])?>" target="_blank" />
										<i class="fa fa-eye"></i> 訂單紀錄</a> 
								   -->
								  <a class="btn btn-success" href="<?php echo getAdminURL('customer/edit?customer_id='.$row['customer_id'].'&'.$_SERVER["QUERY_STRING"])?>">
										<i class="fa fa-pencil-square-o"></i> 編輯</a> 
								  <a class="btn btn-success" href="<?php echo getAdminURL('customer/pass?customer_id='.$row['customer_id'].'&'.$_SERVER['QUERY_STRING'])?>">
										<i class="fa fa-pencil-square-o"></i> 密碼</a> 
									<!-- 	 
								  <a class="btn btn-danger" href="javascript:my_confirm(<?php echo $row['customer_id']?>);">
										<i class="fa fa-trash"></i> 刪除</a>
								    --></td>
							</tr>
							<?php } ?>

						</tbody>
					</table>

					<div class="pageNum">
						<span>顯示第<?php echo $page_startEnd['page_start']?> 至 <?php echo $page_startEnd['page_end']?>
							項結果，共 <?php echo $query_group_by[0]['count']?> 項 </span> <span
							style="float: right;"><?php echo $pages->display_pages()?>
						</span>
					</div>

				</div>

			</div>


		</div>

		<?php $this->load->view('backend/common/footer.tpl'); ?>
	</div>

	<link href="<?php echo base_url('application/views/backend/css/jquery-ui-1.8.21.custom.css')?>" rel="stylesheet">
	<script src="<?php echo base_url('application/views/backend/js/jquery-ui-1.11.1.min.js')?>"></script>
	<script src="<?php echo base_url('application/views//backend/js/dio_plugins/jquery_fields_date.js')?>"></script>
	<script type="text/javascript">
    function my_confirm(customer_id){
       var chk_box=confirm('確定刪除資料?');
       if (chk_box == true) {
             location.href = '<?php echo base_url("index.php/backend/customer/del?customer_id=")?>'+customer_id;
       }
    }
</script>

</body>
</html>
