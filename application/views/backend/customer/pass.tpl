<body>

<style>
.fa-star {
	color: red
}
td {
    text-align: left;
}
</style>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'customer' ,getAdminMenu())?></h2></div>

		<div id="content">

		<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<div class="container editform">
			
			<?php if($this->session->flashdata('msg')){ ?>				 
			    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
		    <?php } ?>			
		    
			<?php echo form_open(current_url().'?'.$_SERVER['QUERY_STRING'])?>
				<table>
					<tr>
						<th colspan="2">編輯資料</th>
					</tr>

					<tr>
						<th width="20%">會員帳號</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="text"
							name="email"
							value="<?php echo isset($query['email']) ? $query['email'] : set_value('email')?>"
							readonly></td>
					</tr>

					<tr>
						<th width="20%"><i class="fa fa-star"></i>更改密碼</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="password"
							name="password" value=""> <b style="color: red"><?php echo form_error('password')?>
						</b></td>
					</tr>

					<tr>
						<th width="20%"><i class="fa fa-star"></i>密碼確認</th>
						<td><input class="fullwidth" placeholder="請輸入資料" type="password"
							name="passconf" value=""> <b style="color: red"><?php echo form_error('passconf')?>
						</b></td>
					</tr>

				</table>

				<div class="buttons">
					<input type="submit" value="送出" />
					<button type="button"
						onclick="javascript:location.href='<?php echo getAdminURL('customer/lists?page=1')?>'">返回</button>
				</div>

				</form>
			</div>

		</div>

	</div>

</body>
</html>
