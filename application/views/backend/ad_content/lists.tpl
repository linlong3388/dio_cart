<body>

	<div id="container">

	<?php $this->load->view('backend/common/nav.tpl')?>
	<div id="title"><h2><?php echo reSortArrayMutil(2,'ad_content' ,getAdminMenu())?></h2></div>
	
		<div id="content">

		    <span style="float: right">
				<div class="actions">
					<a href="javascript:location.href='<?php echo getAdminURL('ad_content/add')?>'"
						class="btn btn-default btn-sm"> <i class="fa fa-pencil"></i> 新增</a>
				</div> 
			</span>
           
			<?php $this->load->view('backend/common/breadcrumb.tpl')?>

			<!-- 內容主體分兩部分:(一)搜尋主體 (二)列表主體 -->
			<div class="container">
			
			 <?php if($this->session->flashdata('msg')){ ?>				 
		        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('msg')?></div>
	         <?php } ?> 			

				<!-- (一)搜尋主體 -->

				<p></p>

				<!-- (二)列表主體 -->
				<div class="clearfix">
					<table class="dio_datatable">
						<thead>
							<tr>
								<td>圖片</td>
								<td>類型</td>
								<td>狀態</td>
								<td>建檔日期</td>
								<td>功能</td>
							</tr>
						</thead>

						<tbody>
						<?php foreach ($query as $row){ ?>
							<tr>
								<td class="center"><img src="<?php echo base_url('resources/uploads/'.$row['image']); ?>" height="150px" width="200px" /></td>
								<td class="center"><?php echo reSortArrayMutil(2,$row['type'],getAdContent()) ?></td>
								<td class="center"><?php echo ($row['status'] == 1) ? "啟用" : "<b style='color:red'>停用</b>" ; ?></td>
								<td class="center"><?php echo substr($row['cdate'], 0,10) ?><b style="display: none"><?php echo substr($row['cdate'], 11) ?></td>
								<td class="center"><a class="btn btn-info"
									href="<?php echo getAdminURL('ad_content/edit?ad_content_id='.$row['ad_content_id']);?>">
										<i class="fa fa-pencil-square-o"></i> 編輯</a> <a
									class="btn btn-danger"
									href="javascript:my_confirm(<?php echo $row['ad_content_id']?>);">
										<i class="fa fa-trash"></i> 刪除</a>
								</td>
							</tr>
							<?php } ?>
						</tbody>

					</table>
				</div>

			</div>

		</div>

	</div>

	<script type="text/javascript">
    $(document).ready(function() {
        $('.dio_datatable').dataTable({
          	 "language": { 
   		         url : "<?php echo base_url("application/views/backend/js/dataTables/i18n/chinese_traditional.lang.json")?>"
   		     },
   		     "order": [[ 3, "desc" ]]
         });
    });
   
    function my_confirm(ad_content_id){
       var chk_box=confirm('確定刪除資料?');
       if (chk_box == true) {
             location.href = '<?php echo base_url("index.php/backend/ad_content/del?ad_content_id=") ?>'+ad_content_id;
       }
    }
</script>

</body>
</html>

