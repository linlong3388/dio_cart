<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*****************************************************
/* 名稱           : setSystemToSession
 * 功能           : 將系統參數設置為 $_SESSION 變數
 * 參數           : 
 ****************************************************/
function setSystemToSession(){
	
   if( !empty($_SESSION['sys_info']) )
	  return true;
	
   $CI =& get_instance();
   $_SESSION['sys_info'] = $CI->db->get_where('system', array('system_id' => '1') )->row_array();
}


/*****************************************************
/* 名稱           : setPromoFeedbackGold
 * 功能           : 將購物金設置為 $_SESSION 變數
 * 參數           :
 ****************************************************/
function setPromoFeedbackGold(){

	if( !empty($_SESSION['sys_info']['promo']['feedback_gold']) )
		return true;
	
	$CI =& get_instance();
	
	$CI->db->where('feedback_gold_id' ,1);
	$CI->db->where('status' ,1);
	$_SESSION['sys_info']['promo']['feedback_gold'] = $CI->db->get('promo_feedback_gold')->row_array();
}


/*****************************************************
/* 名稱           : setPromoVIP
 * 功能           : 將VIP條件設置為 $_SESSION 變數
 * 參數           :
 ****************************************************/
function setPromoVIP(){

	if( !empty($_SESSION['sys_info']['promo']['vip']) )
		return true;
	
	$CI =& get_instance();

	$CI->db->where('promo_vip_id' ,1);
	$CI->db->where('status' ,1);
	$CI->db->where('sdate <= ' ,date('Y-m-d'));
	$CI->db->where('edate >= ' ,date('Y-m-d'));
	
	$_SESSION['sys_info']['promo']['vip'] = $CI->db->get('promo_vip')->row_array();
}


/*****************************************************
/* 名稱           : setPromoCoupon
 * 功能           : 將折價券條件設置為 $_SESSION 變數，隨機挑一
 *           筆可用折價券，結帳步驟會再依輸入條件，reSet 
 *           一次。
 * 參數           :
 ****************************************************/
function setPromoCoupon(){
	
	if( !empty($_SESSION['sys_info']['promo']['coupon']) )
		return true;
	
	$CI =& get_instance();

	//$CI->db->where('promo_coupon_id' ,1);
	$CI->db->where('status' ,1);
	$CI->db->where('sdate <= ' ,date('Y-m-d'));
	$CI->db->where('edate >= ' ,date('Y-m-d'));
	$CI->db->limit(1);
	
	$query = $CI->db->get('promo_coupon');
		
	if( $query->num_rows() > 0 ){
		$_SESSION['sys_info']['promo']['coupon'] = $query->row_array();
	
		//取得折價券使用總數
		$CI->db->where('code' ,$_SESSION['sys_info']['promo']['coupon']['code']);
		$_SESSION['sys_info']['promo']['coupon']['use_count'] = $CI->db->count_all_results('customer_coupon');
	}
}


/*****************************************************
/* 名稱           : is_enabled
 * 功能           : 系統維護中
 * 參數           : t/f
 ****************************************************/
function is_enabled(){
	
	if( empty($_SESSION['sys_info']['is_enabled']) ){
		
	   $pos = strpos($_SERVER["REQUEST_URI"], 'backend');	
	   
	   if ($pos === false) {
	   	   header('Location:'.base_url('comingSoon/index.html'));
           exit; 
	   }   
	// $myip = $_SERVER['REMOTE_ADDR'];
    // if ($myip != "118.163.94.241") {
    //     header('Location:'.'http://fbline.com.tw/comingSoon/index.html'); 
    //     exit; 
    // }    
	}
}


/* End of file setting */