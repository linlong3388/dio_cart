<?php
/**
 * 方法 : 英文語系
 * @author Dio
 *
 */


//清單 menu
$lang['menu_lang']          =  "Language";
$lang['menu_branch']        =  "Stors";
$lang['menu_welcome']       =  "Welcome";
$lang['menu_login']         =  "Login/Register";
$lang['menu_menu']          =  "Menu";
$lang['menu_home']          =  "Home";
$lang['menu_news']          =  "News";
$lang['menu_new_arrival']   =  "New arrival";
$lang['menu_best_sellers']  =  "Best Sellers";
$lang['menu_brands']        =  "Brands";
$lang['menu_products']      =  "Products";
$lang['menu_outlet']        =  "OUTLET";
$lang['menu_faq']           =  "FAQ";
$lang['menu_contact']       =  "Customer Service";
$lang['menu_search']        =  "Product Search...";
$lang['menu_item']          =  "Item";


//Stors(branch)
$lang['branch_title']       =  "Stors";
$lang['branch_all']         =  "All";
$lang['branch_select']      =  "Please Choose";
$lang['branch_store']       =  "Store";
$lang['branch_tel']         =  "Tel";
$lang['branch_detail']      =  "Detail";
$lang['branch_addr']        =  "Address";



//麵包屑(breadcrumb)
$lang['breadcrumb_news']     =  "News";
$lang['breadcrumb_pview']    =  "View Product";
$lang['breadcrumb_buy']      =  "Shopping list";
$lang['breadcrumb_order_info']    =  "Fill Order Information";
$lang['breadcrumb_order_confrim'] =  "Order Confirmation";
$lang['breadcrumb_success']  =  "Shopping Success";
$lang['breadcrumb_fail']     =  "Shopping fail";
$lang['breadcrumb_brands']   =  "Brands";
$lang['breadcrumb_products'] =  "Products";
$lang['breadcrumb_psearch']  =  "Product Search";
$lang['breadcrumb_branch']   =  "Stors";
$lang['breadcrumb_about']    =  "About Us";
$lang['breadcrumb_contact']  =  "Customer Service";
$lang['breadcrumb_faq']      =  "FAQ";
$lang['breadcrumb_info']     =  "Member Profile";
$lang['breadcrumb_osearch']  =  "Orders";
$lang['breadcrumb_coupon']   =  "Shopping Gold";
$lang['breadcrumb_login']    =  "Login/Register";


//登入(login)
$lang['login_title']         =  "Members";
$lang['login_login']         =  "Login";
$lang['login_username']      =  "Account";
$lang['login_pass']          =  "Password";
$lang['login_forget']        =  "Forget Password?";
$lang['login_name']          =  "Name";
$lang['login_tel']           =  "Tel";
$lang['login_email']         =  "E-mail";
$lang['login_pass_confrim']  =  "Confirm Password";
$lang['login_code']          =  "Codes";
$lang['login_edm']           =  "Newsletter Subscription";
$lang['login_edm_yes']       =  "Subscription";
$lang['login_edm_no']        =  "Not subscribe";
$lang['login_agree']         =  "I have read and agree";
$lang['login_provision']     =  "Membership Terms of Service";
$lang['login_register']      =  "I want to register";
$lang['login_memo']          =  "Please enter your registered account and registration of E-mail, the system will send the password to your E-mail!";
$lang['login_confrim']       =  "Confrim";
$lang['login_privacy']       =  "<h3>Privacy and Terms</h3>
              <div class=\"editor\"> This site is to provide a secure transaction service's website, so our personal privacy of website users, absolutely respected and protected. The following content will let you know how we collect, use and protect the personal information you provide. <br>
                <br>
                <strong>Collect personal information policy</strong> <br>
                To confirm that you conduct trading services performed at the site and you can easily get the information on this site, we will ask you to provide, including Name, Contact Tel, e-mail, contacts, address ... and other personal information.<br>
                <br>
                <strong>The use of policy to collect personal data</strong><br>
                You entered on the site information, this website will be used only for transactions related to the job.<br>
                <br>
                <strong>Share personal information with third parties Policy</strong><br>
                1. without your prior consent, personal data collected on this website will not sell, exchange, or lease any of your personal information to other organizations or individuals, or used for purposes other than the purpose.
                 2. If this site in order to provide other services or preferential rights, and the need to provide the service or offer third party when sharing user data (with other industry players to jointly launch activities or special) will be provided at the event fully demonstrates that you can freely choose whether to accept the specific services or promotions.<br>
                <br>
                <strong>Cookies Use</strong><br>
                Site in order to enhance user convenience and data entry accuracy, and provide better service, all data fields on the site will use cookies to store the way you've entered data. So this site will under this policy principles, write and read Cookies in your browser. If you are unwilling to accept the Cookie is written, you can set the privacy level in the browser function key that you are using for the high, you can refuse Cookie writing, but may lead to some website functions can not be performed properly. <br>
                <br>
                <strong>The amendment safeguards the privacy rights</strong><br>
                Privacy terms of this site will respond to technological trends, revise relevant laws and regulations or other environmental changes and other factors make the appropriate modifications to ensure the implementation of user privacy conception. The revised terms will be adjusted at any time, immediately published in this site.</div> ";


//表尾(footer)
$lang['footer_about']         =  "About us";
$lang['footer_brands']        =  "Brands";
$lang['footer_branch']        =  "Stors";
$lang['footer_contact']       =  "Customer Service";
$lang['footer_follow']        =  "FOLLOW US";
$lang['footer_faq']           =  "FAQ";
//$lang['footer_quality']       =  "Quality Test";
//$lang['footer_description']   =  "Warranty";
//$lang['footer_qa']            =  "Q&A";
$lang['footer_addr']          =  "No.94, Minzu Rd., Central Dist., Taichung City 400, Taiwan (R.O.C.)";


//News(news)
$lang['news_title']           =  "News";
$lang['news_all']             =  "All";
$lang['news_back']            =  "Return";


//商品(product)&購物燈箱(product_lightBox)
$lang['product_title1']          =  "New arrival";
$lang['product_title2']          =  "Best Sellers";
$lang['product_title3']          =  "OUTLET";
$lang['product_sku']             =  "Model";
$lang['product_price']           =  "Price";
$lang['product_price_promo']     =  "VIP Price";
$lang['product_size']            =  "Size";
$lang['product_color']           =  "Color";
$lang['product_entity']          =  "Quantity";
$lang['product_addCart']         =  "add to Cart";
$lang['product_ask']             =  "Inquire this product";
$lang['product_detail']          =  "Product Detail";
$lang['product_description']     =  "Feature";
$lang['product_speci']           =  "Specification";
$lang['product_already_addCart']  =  "Has joined the shopping cart!";
$lang['product_continue']        =  "Continue";
$lang['product_checkout']        =  "Checkout";
$lang['product_back']            =  "Return";
$lang['product_name']            =  "Name";
$lang['product_tel']             =  "Tel";
$lang['product_email']           =  "E-mail";
$lang['product_message']         =  "Leave a message";
$lang['product_code']            =  "Codes";
$lang['product_memo1']           =  "Fields are required in order to quickly process your customer problem.";
$lang['product_memo2']           =  "‧E-mail Be sure to fill in a single mailbox and useful in order to get back to you.";
$lang['product_view']            =  "QUICKVIEW";
$lang['product_result']          =  "Product Search Results";
$lang['product_rltSuccess1']     =  "Search";
$lang['product_rltSuccess2']     =  "The results are as follows";
$lang['product_rltfail1']        =  "Your search";
$lang['product_rltfail2']        =  "No results matching.";


//Brands
$lang['brand_title1']            =  "Brands";
$lang['quick_view']              =  "QUICKVIEW";


//結帳
$lang['step1_title']             =  "Shopping list";
$lang['step1_noData']            =  "Your shopping list any products Oh no!";
$lang['step1_cancel']            =  "Cancel";
$lang['step1_pname']             =  "Product Name";
$lang['step1_price']             =  "Price";
$lang['step1_entity']            =  "Quantity";
$lang['step1_sum']               =  "Subtotal";
$lang['step1_privacy_chk']       =  "I understand <span class=\"gray\">Shopping Notes";
$lang['step1_total']             =  "total";
$lang['step1_continue']          =  "Continue";
$lang['step1_next']              =  "Next";
$lang['step1_privacy_title']    =  "Shopping Notes";
$lang['step1_privacy']     =  "<ul>
<li> Distribution Region and Shipment: The Island (Free transport), Islands (shipping will be $ 150 million), foreign direct delivery (due to shipping the actual shipping freight, freight because of the different regions and shipping methods such as shipping, air transport ... and different. we will receive your order will be quoted to you and contact you) </ li>
                  <li> a shipping method to Hsinchu, the main cargo. </li>
                  <li> The Company Shopping Remittance Account: First Bank - North Taichung Branch Bank Code: 007 Account #: 403-10-051261 username: US Leather Co., Ltd. crown. </li>
                  <li> Goods Drawing color due to the relationship between PC screen is slightly different, if the color of guests very seriously, and then make sure that under the orders and customer service after purchase Ruoyin color problems return, return the goods freight need to be borne by consumers themselves. </li>
                  <li> If the reason for the return, for example, defective product itself, or to the wrong merchandise, please contact customer service within seven days, after confirmation we will have someone contact the government to receive a replacement, part of the freight borne by the US crown here . </li>
                  <li> If consumers are not satisfied non-commodity goods defective ask the customer, please contact customer service within seven days and staff will be contacted for confirmation before items returned to the Company, return the goods cost borne by consumers themselves, we receive the goods after confirming the correct product defect damage, customer service staff will handle your return procedure. </li>
                  <li> If you receive the goods are not satisfied, within seven days from the date of arrival (including holidays) for the appreciation of, only Appreciation Appreciation period allowed outdoor landing try. If you want to keep the primary commodity please return, complete without accessories for missing parts, Do not scratch, wear, holding the sleeve, the integrity of the packaging material. Please keep the original carton can not be sure to clean the complete damage, if more than this period is deemed to have agreed to purchase. </li>
                  <li> contains only product warranty on manufacturing defects in the main structure, for improper use arising from wear, tear (break) damage is not covered under warranty crack, fire, and other strong chemicals in different ways. </li>              </ul>";
$lang['step1_privacy_chk_ok']    =  "*Check shopping instructions!";


//結帳(checkout/step2)
$lang['step2_title']             =  "Fill Order Information";
$lang['step2_otitle']            =  "Purchaser";
$lang['step2_name']              =  "Name";
$lang['step2_tel']               =  "Tel";
$lang['step2_email']             =  "E-mail";
$lang['step2_paymethod']         =  "payment method";
$lang['step2_transmethod']       =  "Shipping Method";
$lang['step2_coupon']            =  "Volume discount number";
$lang['step2_promo']             =  "Learn cart gold";
$lang['step2_dtitle']            =  "Addressee";
$lang['step2_the_same']          =  "The Same Purchaser";
$lang['step2_dlocal']            =  "Delivery Area";
$lang['step2_memo_1']            =  "Note";
$lang['step2_memo_2']            =  " <div class=\"red\">Overseas direct delivery: Due to the actual freight shipping freight, shipping and delivery method because of the different regions (sea, air, etc ...) varies.
                     We will receive your order will be quoted to you and contact you.</div>";
$lang['step2_addr']              =  "Address";
$lang['step2_memo']              =  "Remark";
$lang['step2_pre']               =  "Previous";
$lang['step2_next']              =  "Next";
$lang['step2_promo_memo']        =  "Shopping Gold Description";
$lang['step2_memo_3']            =  "Date activities in full, giving a volume discount, members can order the next consumer deductible.
                                        <br>
		                            Ruoyin Order Cancel, which led to the return does not meet the conditions for the activities of gold shopping, shopping will lose the original order obtained gold. ";


//結帳(checkout/step3)
$lang['step3_title']             =  "Order Confirmation";
$lang['step3_image']             =  "Image";
$lang['step3_pname']             =  "Product Name";
$lang['step3_price']             =  "Price";
$lang['step3_entity']            =  "Quantity";
$lang['step3_sum']               =  "Subtotal";
$lang['step3_trans']             =  "Shipment";
$lang['step3_coupon']            =  "Coupons redeemed";
$lang['step3_vip']               =  "VIP Member Benefits";
$lang['step3_total']             =  "total";
$lang['step3_oname']             =  "Purchaser";
$lang['step3_dname']             =  "Addressee";
$lang['step3_tel']               =  "Tel";
$lang['step3_email']             =  "E-mail";
$lang['step3_addr']              =  "Address";
$lang['step3_ototal']            =  "order amount";
$lang['step3_paymethod']         =  "payment method";
$lang['step3_transmethod']       =  "Shipping Method";
$lang['step3_memo']              =  "Remark";
$lang['step3_pre']               =  "Previous";
$lang['step3_confrim']           =  "Confirm";


//結帳(checkout/step6_fail)
$lang['step6_fail_title']        =  "Sorry, shopping failed!";
$lang['step6_fail_memo_1']       =  "If you have questions, please contact customer service";
$lang['step6_fail_contact']      =  "Customer Service";
$lang['step6_fail_return']       =  "Return";


//結帳(checkout/step6_success)
$lang['step6_success_title']     =  "Shopping Success";
$lang['step6_success_msg']       =  "Congratulations on your successful order";
$lang['step6_success_content']   =  "You have completed the order procedure, thank you for your order.";
$lang['step6_success_status']    =  "Check Order Status";
$lang['step6_success_customer']  =  "Members";
$lang['step6_success_order_qry'] =  "Orders";
$lang['step6_success_coupon']    =  "The order obtained gold discount shopping ID: Go to Members";
$lang['step6_success_coupon_get'] =  "Receive coupons";
$lang['step6_success_return']     =  "Return";


//Customer Service(contact)
$lang['contact_title']       =  "Customer Service";
$lang['contact_name']        =  "Name";
$lang['contact_tel']         =  "Tel";
$lang['contact_email']       =  "E-mail";
$lang['contact_content']     =  "Contact Matters";
$lang['contact_upload']      =  "Image Load";
$lang['contact_msg']         =  "Leave a message";
$lang['contact_code']        =  "Codes";
$lang['contact_memo_1']      =  "Fields are required in order to quickly process your customer problem.<br>
                ‧E-mail sure to fill out a single mailbox and useful in order to get back to you.<br>
                ‧For service issues, please indicate the box you buy hard (soft) and me to buy time in order to facilitate customer service staff to make Detail explanation. ";
$lang['contact_submit']      =  "Confirm";
$lang['contact_memo_2']      =  "Dear Customer Hello<br>
              Case Inc. Web Customer Service Department here for you<br>
              If you have any questions <br>
               Welcome to contact us!";
$lang['contact_follow_us']   =  "FOLLOW US";
$lang['contact_city']        =  "Taichung store";
$lang['contact_addr']        =  "No.94, Minzu Rd., Central Dist., Taichung City 400, Taiwan (R.O.C.)";
$lang['contact_fax']         =  "FAX";


//關於我們(content)
$lang['content_title']       =  "About Us";


//會員資料(customer_info)
$lang['customer_info_title']       =  "Members";
$lang['customer_info_edit']        =  "Member Information Edit";
$lang['customer_info_level']       =  "Level";
$lang['customer_info_vip_how']     =  "How to Upgrade VIP Membership?";
$lang['customer_info_vip_date']    =  "VIP Member Effective Date";
$lang['customer_info_to']          =  "To";
$lang['customer_info_username']    =  "Account";
$lang['customer_info_name']        =  "Name";
$lang['customer_info_tel']         =  "Tel";
$lang['customer_info_email']       =  "E-mail";
$lang['customer_info_pass']        =  "Password";
$lang['customer_info_pass_confrim'] =  "Confrim Password";
$lang['customer_info_edm']         =  "Newsletter Subscription";
$lang['customer_info_is_edm']      =  "Whether Subscribe";
$lang['customer_info_confrim']     =  "Confrim";
$lang['customer_info_vip_up']      =  "Upgrade VIP Members Help";
$lang['customer_info_vip_memo']    =  "Full capacity within a year ".DIO_CURRENCY.addCommas( $_SESSION['sys_info']['promo']['vip']['total'] )." dollar, That become a VIP member, shopping enjoy VIP Member".$_SESSION['sys_info']['promo']['vip']['discount']."% Offer!<br>
                                       Full capacity within a year ".DIO_CURRENCY.addCommas( $_SESSION['sys_info']['promo']['vip']['ctotal'] )." dollar, Renewable VIP membership.";



//會員訂單(customer_order)
$lang['customer_order_title']      =  "Members";
$lang['customer_order_qry']        =  "Order Tracking";
$lang['customer_order_no']         =  "Order Number";
$lang['customer_order_odate']      =  "Order Date";
$lang['customer_order_money']      =  "Total";
$lang['customer_order_paymethod']  =  "Payment Method";
$lang['customer_order_transmethod']  =  "Shipping Method";
$lang['customer_order_transzip']  =  "Shipping area";
$lang['customer_order_ostatus']    =  "Order Status";
$lang['customer_order_detail']     =  "Detail";
$lang['customer_order_qry_view']   =  "Order Tracking/View";
$lang['customer_order_return']     =  "Return";
$lang['customer_order_info']       =  "Order Info";
$lang['customer_order_oname']      =  "Purchaser";
$lang['customer_order_dname']      =  "Addressee";
$lang['customer_order_tel']        =  "Tel";
$lang['customer_order_email']      =  "E-mail";
$lang['customer_order_addr']       =  "Address";
$lang['customer_order_omoney']     =  "Amount";
$lang['customer_order_memo']       =  "Memo";
$lang['customer_order_coupon_no']  =  "Coupon No.";
$lang['customer_order_status']     =  "Status";
$lang['customer_order_tno']        =  "Num.";
$lang['customer_order_arrTime']    =  "Time of arrival";
$lang['customer_order_station']    =  "Station";
$lang['customer_order_errMsg']     =  "Error Message";
$lang['customer_order_image']      =  "Image";
$lang['customer_order_pname']      =  "Product Name";
$lang['customer_order_entity']     =  "Entity";
$lang['customer_order_price']      =  "Price";
$lang['customer_order_subTotal']   =  "Sub Total";
$lang['customer_order_total']      =  "Total";


//會員購物金(customer_coupon)
$lang['customer_coupon_title']      =  "Members";
$lang['customer_coupon_coupon']     =  "Shopping Gold";
$lang['customer_coupon_orderNo']    =  "Order No.";
$lang['customer_coupon_action']     =  "Programme of activities";
$lang['customer_coupon_couponNo']   =  "Coupon No.";
$lang['customer_coupon_money']      =  "Total";
$lang['customer_coupon_sdate']      =  "Effective Date";
$lang['customer_coupon_edate']      =  "Expiry Date";
$lang['customer_coupon_status']     =  "Use Status";


//會員分類清單(customer_menu)
$lang['customer_menu_info']         =  "Member Info";
$lang['customer_menu_orderQry']     =  "Order Tracking";;
$lang['customer_menu_coupon']       =  "Shopping Gold";
$lang['customer_menu_logout']       =  "Logout";


/* End of file caseinc_lang.php */