<?php
/**
 * 方法 : 中文語系
 * @author Dio
 *
 */

//清單 menu
$lang['menu_lang']          =  "語系";
$lang['menu_branch']        =  "銷售據點";
$lang['menu_welcome']       =  "歡迎";
$lang['menu_login']         =  "登入/註冊";
$lang['menu_menu']          =  "選單";
$lang['menu_home']          =  "首頁";
$lang['menu_news']          =  "最新消息";
$lang['menu_new_arrival']   =  "新品上市";
$lang['menu_best_sellers']  =  "熱賣商品";
$lang['menu_brands']        =  "品牌總覽";
$lang['menu_products']      =  "商品專區";
$lang['menu_outlet']        =  "OUTLET";
$lang['menu_faq']           =  "FAQ";
$lang['menu_contact']       =  "聯絡我們";
$lang['menu_login']         =  "登入/註冊";
$lang['menu_search']        =  "商品搜尋...";
$lang['menu_item']          =  "項目";


//銷售據點(branch)
$lang['branch_title']       =  "銷售據點";
$lang['branch_all']         =  "全部";
$lang['branch_select']      =  "請選擇";
$lang['branch_store']       =  "店名";
$lang['branch_tel']         =  "電話";
$lang['branch_detail']      =  "詳細";
$lang['branch_addr']        =  "地址";


//麵包屑(breadcrumb)
$lang['breadcrumb_news']     =  "最新消息";
$lang['breadcrumb_pview']    =  "檢視商品";
$lang['breadcrumb_buy']      =  "購物清單";
$lang['breadcrumb_order_info']    =  "填寫訂單資訊";
$lang['breadcrumb_order_confrim'] =  "訂單確認";
$lang['breadcrumb_success']  =  "購物成功";
$lang['breadcrumb_fail']     =  "購物失敗";
$lang['breadcrumb_brands']   =  "品牌總覽";
$lang['breadcrumb_products'] =  "商品專區";
$lang['breadcrumb_psearch']  =  "商品搜尋";
$lang['breadcrumb_branch']   =  "銷售據點";
$lang['breadcrumb_about']    =  "關於我們";
$lang['breadcrumb_contact']  =  "聯絡我們";
$lang['breadcrumb_faq']      =  "FAQ";
$lang['breadcrumb_info']     =  "會員資料";
$lang['breadcrumb_osearch']  =  "訂單查詢";
$lang['breadcrumb_coupon']   =  "購物金";
$lang['breadcrumb_login']    =  "登入/註冊";


//登入(login)
$lang['login_title']         =  "會員專區";
$lang['login_login']         =  "登入";
$lang['login_username']      =  "帳號";
$lang['login_pass']          =  "密碼";
$lang['login_forget']        =  "忘記密碼?";
$lang['login_name']          =  "姓名";
$lang['login_tel']           =  "電話";
$lang['login_email']         =  "E-mail";
$lang['login_pass_confrim']  =  "確認密碼";
$lang['login_code']          =  "驗證碼";
$lang['login_edm']           =  "電子報訂閱";
$lang['login_edm_yes']       =  "訂閱";
$lang['login_edm_no']        =  "不訂閱";
$lang['login_agree']         =  "我已閱讀並同意";
$lang['login_provision']     =  "會員服務條款";
$lang['login_register']      =  "我要註冊";
$lang['login_memo']          =  "請輸入您的註冊帳號及註冊時的E-mail，系統將發送密碼到您E-mail！";
$lang['login_confrim']       =  "確定";
$lang['login_privacy']       =  "<h3>隱私與條款</h3>
              <div class=\"editor\"> 本網站是一個提供安全交易服務的網站，所以我們對網站使用者的個人的隱私權，絕對尊重並予以保護。以下的內容將讓您瞭解我們如何蒐集、應用及保護您所提供的個人資訊。 <br>
                <br>
                <strong>個人資料之蒐集政策</strong> <br>
                為確認您在本站所進行的交易服務行為以及方便您取得本網站資訊，我們將會請您提供包括姓名、聯絡電話、電子信箱、聯絡住址…等個人資料。<br>
                <br>
                <strong>蒐集個人資料之運用政策</strong><br>
                您在網站上所輸入的資料，本網站將僅用於交易相關作業。<br>
                <br>
                <strong>與第三者共用個人資料之政策</strong><br>
                1.在未得到您的同意之前，本網站所蒐集的個人資料絕不會任意出售、交換、或出租任何您的個人資料給其他團體或個人，或用於上述目的以外之其他用途。
                2.若本網站為了提供其他服務或優惠權益，需要與提供該服務或優惠第三者共用使用者資料時（與其他業者共同推出活動或特惠），會在活動時提供充分說明，您可以自由選擇是否接受該項特定服務或優惠。 <br>
                <br>
                <strong>Cookies之運用</strong><br>
                本站為了提昇使用者輸入資料的方便性及正確性，以及提供更好的服務，網站上各資料欄位會採用cookies方式來儲存您輸入過的資料。所以本站會在本政策原則之下，在您瀏覽器中寫入並讀取Cookies。若您不願接受Cookie的寫入，您可在您使用的瀏覽器功能項中設定隱私權等級為高，即可拒絕Cookie的寫入，但可能會導至網站某些功能無法正常執行 。 <br>
                <br>
                <strong>有關隱私權保障條款之修正權利</strong><br>
                本網站的隱私權保護條款，將因應科技發展趨勢、相關法規之修訂或其他環境變遷等因素做適當之修改，以落實保障使用者隱私權之立意。修改過的條款將隨時調整，立即刊登於本站中。</div>
  ";


//表尾(footer)
$lang['footer_about']         =  "關於我們";
$lang['footer_brands']        =  "品牌簡介";
$lang['footer_branch']        =  "銷售據點";
$lang['footer_contact']       =  "聯絡我們";
$lang['footer_follow']        =  "關注我們";
$lang['footer_faq']           =  "FAQ";
//$lang['footer_quality']       =  "品質測試";
//$lang['footer_description']   =  "保固說明";
//$lang['footer_qa']            =  "常見問題";
$lang['footer_addr']          =  "台中市中區民族路94號";


//最新消息(news)
$lang['news_title']           =  "最新消息";
$lang['news_all']             =  "全部";
$lang['news_back']            =  "返回";


//商品(product)&購物燈箱(product_lightBox)
$lang['product_title1']          =  "新品上市";
$lang['product_title2']          =  "熱賣商品";
$lang['product_title3']          =  "OUTLET";
$lang['product_sku']             =  "型號";
$lang['product_price']           =  "建議售價";
$lang['product_price_promo']     =  "優惠價";
$lang['product_size']            =  "尺寸";
$lang['product_color']           =  "顏色";
$lang['product_entity']          =  "數量";
$lang['product_addCart']         =  "加入購物車";
$lang['product_ask']             =  "詢問此商品";
$lang['product_detail']          =  "商品詳細";
$lang['product_description']     =  "產品特色";
$lang['product_speci']           =  "產品規格";
$lang['product_already_addCart']  =  "商品已加入購物車！";
$lang['product_continue']        =  "繼續購物";
$lang['product_checkout']        =  "我要結帳";
$lang['product_back']            =  "返回";
$lang['product_name']            =  "姓名";
$lang['product_tel']             =  "電話";
$lang['product_email']           =  "E-mail";
$lang['product_message']         =  "留言";
$lang['product_code']            =  "驗證碼";
$lang['product_memo1']           =  "欄位皆必填，以便客服人員迅速處理您的問題。";
$lang['product_memo2']           =  "‧E-mail務必填寫單一個且有用的信箱，以便於回覆您。";
$lang['product_view']            =  "QUICKVIEW";
$lang['product_result']          =  "商品搜尋結果";
$lang['product_rltSuccess1']     =  "搜尋";
$lang['product_rltSuccess2']     =  "結果如下";
$lang['product_rltfail1']        =  "您的搜尋";
$lang['product_rltfail2']        =  "找不到符合的搜尋結果。";
  

//品牌總覽
$lang['brand_title1']            =  "品牌總覽";
$lang['quick_view']              =  "QUICKVIEW";


//結帳
$lang['step1_title']             =  "購物清單";
$lang['step1_noData']            =  "您的購物清單中尚無任何商品喔!";
$lang['step1_cancel']            =  "取消";
$lang['step1_pname']             =  "產品名稱";
$lang['step1_price']             =  "單價";
$lang['step1_entity']            =  "數量";
$lang['step1_sum']               =  "小計";
$lang['step1_privacy_chk']       =  "我已了解<span class=\"gray\">購物須知";
$lang['step1_total']             =  "總計";
$lang['step1_continue']          =  "繼續購物";
$lang['step1_next']              =  "下一步";
$lang['step1_privacy_title']    =  "購物須知";
$lang['step1_privacy']     =  "<ul>
                  <li> 配送地區與運費：本島(免運)、離島(運費一律$150元)、海外直送(因運費以實際寄送運費計算，運費又因不同區域與寄送方式如海運、空運等…而異。我們將在收到您的訂單後會另行報價給您並與您連絡) </li>
                  <li>送貨方式以新竹貨運為主。</li>
                  <li>本公司購物匯款帳號：第一銀行-北台中分行   銀行代號：007   帳號：403-10-051261   戶名：美冠皮件有限公司。</li>
                  <li>商品圖檔顏色會因個人電腦螢幕的關係略有不同，若很重視色差的客人，請與客服人員確認後再下訂單， 購買後若因色彩問題而退換貨，商品寄回運費需由消費者自行負擔。</li>
                  <li>若退貨原因是，例如商品本身瑕疵、或是商品寄錯，請在七天之內與客服人員聯絡，聯絡確認後我們會有專人到府收換貨，運費的部分由美冠這邊負擔。</li>
                  <li>若是消費者不滿意商品非商品瑕疵問顧客，請在七天之內與客服人員聯絡，聯絡確認後再將物品寄回本公司，貨物寄回費用由消費者自行負擔，我們收到商品後確認商品瑕疵損壞無誤，客服人員將會為您辦理退換貨手續。 </li>
                  <li>若收到商品不滿意，到貨日起七日內(含假日)為鑑賞期，鑑賞期內僅供鑑賞，不可戶外落地試用，若欲退貨請保持主商品、配件之完整無缺件，請勿刮傷、磨損，保持外盒、包材之完整性。原廠外盒請保持清潔完整務必不可毀損，若超過此期限則視為同意購買。 </li>
                  <li>保固範圍僅包含產品在生產製造上的主體結構瑕疵，對於不當使用所產生的磨損、撕(破)裂、火源、強烈化學藥劑及其他不同方式造成損壞則不再保固範圍內。 </li>
                </ul>";
$lang['step1_privacy_chk_ok']    =  "*請勾選購物須知!";


//結帳(checkout/step2)
$lang['step2_title']             =  "填寫訂單資訊";
$lang['step2_otitle']            =  "訂購人";
$lang['step2_name']              =  "姓名";
$lang['step2_tel']               =  "電話";
$lang['step2_email']             =  "E-mail";
$lang['step2_paymethod']         =  "付款方式";
$lang['step2_transmethod']       =  "運送方式";
$lang['step2_coupon']            =  "折價卷序號";
$lang['step2_promo']             =  "了解購物金";
$lang['step2_dtitle']            =  "收件人";
$lang['step2_the_same']          =  "同訂購人";
$lang['step2_dlocal']            =  "配送地區";
$lang['step2_memo_1']            =  "註";
$lang['step2_memo_2']            =  " <div class=\"red\">海外直送：因運費以實際寄送運費計算，運費又因不同區域與寄送方式（海運、空運等…）而異。
                    我們將在收到您的訂單後會另行報價給您並與您連絡。</div>";
$lang['step2_addr']              =  "地址";
$lang['step2_memo']              =  "備註";
$lang['step2_pre']               =  "上一步";
$lang['step2_next']              =  "下一步";
$lang['step2_promo_memo']        =  "購物金說明";
$lang['step2_memo_3']            =  "活動日期內滿額，贈送折價卷，會員可於下一次的消費訂單中抵扣。
                                       <br>
                                                                                                   若因訂單取消、退貨而導致不滿足購物金活動條件，將喪失原訂單可獲得購物金。 ";


//結帳(checkout/step3)
$lang['step3_title']             =  "訂單確認";
$lang['step3_image']             =  "產品圖";
$lang['step3_pname']             =  "產品名稱";
$lang['step3_price']             =  "單價";
$lang['step3_entity']            =  "數量";
$lang['step3_sum']               =  "小計";
$lang['step3_trans']             =  "運費";
$lang['step3_coupon']            =  "折價券折抵";
$lang['step3_vip']               =  "VIP會員優惠";
$lang['step3_total']             =  "總計";
$lang['step3_oname']             =  "訂購人";
$lang['step3_dname']             =  "收件人";
$lang['step3_tel']               =  "電話";
$lang['step3_email']             =  "E-mail";
$lang['step3_addr']              =  "地址";
$lang['step3_ototal']            =  "訂單金額";
$lang['step3_paymethod']         =  "付款方式";
$lang['step3_transmethod']       =  "運送方式";
$lang['step3_memo']              =  "備註";
$lang['step3_pre']               =  "上一步";
$lang['step3_confrim']           =  "確認送出";


//結帳(checkout/step6_fail)
$lang['step6_fail_title']        =  "抱歉，購物失敗!";
$lang['step6_fail_memo_1']       =  "如有問題請洽客服人員";
$lang['step6_fail_contact']      =  "聯絡我們";
$lang['step6_fail_return']       =  "返回商品頁";


//結帳(checkout/step6_success)
$lang['step6_success_title']     =  "購物成功";
$lang['step6_success_msg']       =  "恭喜您～訂購成功！";
$lang['step6_success_content']   =  "您已完成訂購手續，感謝您的訂購。";
$lang['step6_success_status']    =  "查詢訂單狀態";
$lang['step6_success_customer']  =  "會員專區";
$lang['step6_success_order_qry'] =  "訂單查詢";
$lang['step6_success_coupon']    =  "本次訂單可獲得購物金折價序號：請至會員專區";
$lang['step6_success_coupon_get'] =  "領取折價券";
$lang['step6_success_return']     =  "返回商品頁";


//聯絡我們(contact)
$lang['contact_title']       =  "聯絡我們";
$lang['contact_name']        =  "姓名";
$lang['contact_tel']         =  "電話";
$lang['contact_email']       =  "E-mail";
$lang['contact_content']     =  "聯絡事項";
$lang['contact_upload']      =  "圖片上傳";
$lang['contact_msg']         =  "留言";
$lang['contact_code']        =  "驗證碼";
$lang['contact_memo_1']      =  "欄位皆必填，以便客服人員迅速處理您的問題。<br>
                ‧E-mail務必填寫單一個且有用的信箱，以便於回覆您。<br>
                ‧有關維修問題，請註明您購買的箱子為硬(軟)箱以及購買時間，以方便客服人員為您作詳細解說 。 ";
$lang['contact_submit']      =  "送出";
$lang['contact_memo_2']      =  "親愛的顧客您好<br>
              美冠皮件網路客服部在此為您服務<br>
              如果您有任何疑問<br>
              歡迎與我們聯繫 ！";
$lang['contact_follow_us']   =  "關注我們";
$lang['contact_city']        =  "台中門市";
$lang['contact_addr']        =  "台中市中區民族路94號";
$lang['contact_fax']         =  "傳真";


//關於我們(content)
$lang['content_title']       =  "關於我們";


//會員資料(customer_info)
$lang['customer_info_title']       =  "會員專區";
$lang['customer_info_edit']        =  "會員資料修改";
$lang['customer_info_level']       =  "級別";
$lang['customer_info_vip_how']     =  "如何升級VIP會員?";
$lang['customer_info_vip_date']    =  "VIP會員生效日期";
$lang['customer_info_to']          =  "至";
$lang['customer_info_username']    =  "帳號";
$lang['customer_info_name']        =  "姓名";
$lang['customer_info_tel']         =  "電話";
$lang['customer_info_email']       =  "E-mail";
$lang['customer_info_pass']        =  "密碼";
$lang['customer_info_pass_confrim'] =  "確認密碼";
$lang['customer_info_edm']         =  "電子報訂閱";
$lang['customer_info_is_edm']      =  "是否訂閱";
$lang['customer_info_confrim']     =  "確認";
$lang['customer_info_vip_up']      =  "升級VIP會員說明";

$total    = isset($_SESSION['sys_info']) ? DIO_CURRENCY.$_SESSION['sys_info']['promo']['vip']['total'] : 0;
$ctotal   = isset($_SESSION['sys_info']) ? DIO_CURRENCY.$_SESSION['sys_info']['promo']['vip']['ctotal'] : 0;
$discount = isset($_SESSION['sys_info']) ? DIO_CURRENCY.$_SESSION['sys_info']['promo']['vip']['discount'] : 0;

$lang['customer_info_vip_memo']    =  "一年內滿額 {$total} 元即成為VIP會員，VIP會員購物可享 {$discount} %優惠！<br>
                                                                                                        一年內購滿 {$ctotal} 元，可續VIP會員資格。";


//會員訂單(customer_order)
$lang['customer_order_title']      =  "會員專區";
$lang['customer_order_qry']        =  "訂單查詢";
$lang['customer_order_no']         =  "訂單編號";
$lang['customer_order_odate']       =  "訂購日期";
$lang['customer_order_money']      =  "金額";
$lang['customer_order_paymethod']  =  "付款方式";
$lang['customer_order_transmethod']  =  "運送方式";
$lang['customer_order_transzip']  =  "運送區域";
$lang['customer_order_ostatus']    =  "訂單狀態";
$lang['customer_order_detail']     =  "詳細內容";
$lang['customer_order_qry_view']   =  "訂單查詢/檢視";
$lang['customer_order_return']     =  "返回";
$lang['customer_order_info']       =  "訂單資訊";
$lang['customer_order_oname']      =  "訂購人";
$lang['customer_order_dname']      =  "收件人";
$lang['customer_order_tel']        =  "電話";
$lang['customer_order_email']      =  "E-mail";
$lang['customer_order_addr']       =  "地址";
$lang['customer_order_omoney']     =  "訂單金額";
$lang['customer_order_memo']       =  "備註";
$lang['customer_order_coupon_no']  =  "折價券序號";
$lang['customer_order_status']     =  "狀態";
$lang['customer_order_tno']        =  "貨號";
$lang['customer_order_arrTime']    =  "到達時間";
$lang['customer_order_station']    =  "站點";
$lang['customer_order_errMsg']     =  "錯誤訊息";
$lang['customer_order_image']      =  "產品圖";
$lang['customer_order_pname']      =  "產品名稱";
$lang['customer_order_entity']     =  "數量";
$lang['customer_order_price']      =  "單價";
$lang['customer_order_subTotal']   =  "金額";
$lang['customer_order_total']      =  "總計";


//會員購物金(customer_coupon)
$lang['customer_coupon_title']      =  "會員專區";
$lang['customer_coupon_coupon']     =  "購物金";
$lang['customer_coupon_orderNo']    =  "訂單編號";
$lang['customer_coupon_action']     =  "活動方案";
$lang['customer_coupon_couponNo']   =  "折價卷序號";
$lang['customer_coupon_money']      =  "金額";
$lang['customer_coupon_sdate']      =  "生效日";
$lang['customer_coupon_edate']      =  "到期日";
$lang['customer_coupon_status']     =  "使用狀態";


//會員分類清單(customer_menu)
$lang['customer_menu_info']         =  "會員資料";
$lang['customer_menu_orderQry']     =  "訂單查詢";
$lang['customer_menu_coupon']       =  "購物金";
$lang['customer_menu_logout']       =  "登出";


/* End of file caseinc_lang.php */