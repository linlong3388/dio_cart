<?php
/**
 * 方法 : 日文言語
 * @author Dio
 *
 */

//清單 menu
$lang['menu_lang']          =  "言語";
$lang['menu_branch']        =  "販売拠点";
$lang['menu_welcome']       =  "歓迎";
$lang['menu_login']         =  "ログイン/登録";
$lang['menu_menu']          =  "メニュー";
$lang['menu_home']          =  "ホーム";
$lang['menu_news']          =  "ニュース";
$lang['menu_new_arrival']   =  "新発売";
$lang['menu_best_sellers']  =  "人気商品";
$lang['menu_brands']        =  "ブランド一覧";
$lang['menu_products']      =  "製品情報";
$lang['menu_outlet']        =  "OUTLET";
$lang['menu_faq']           =  "FAQ";
$lang['menu_contact']       =  "お問い合わせ";
$lang['menu_login']         =  "ログイン/登録";
$lang['menu_search']        =  "製品検索...";
$lang['menu_item']          =  "プロジェクト";


//販売拠点(branch)
$lang['branch_title']       =  "販売拠点";
$lang['branch_all']         =  "完全な";
$lang['branch_select']      =  "選択してください";
$lang['branch_store']       =  "店名";
$lang['branch_tel']         =  "電話";
$lang['branch_detail']      =  "詳しいです";
$lang['branch_addr']        =  "アドレス";


//麵包屑(breadcrumb)
$lang['breadcrumb_news']     =  "ニュース";
$lang['breadcrumb_pview']    =  "ビュー製品";
$lang['breadcrumb_buy']      =  "買い物リスト";
$lang['breadcrumb_order_info']    =  "注文情報を記入";
$lang['breadcrumb_order_confrim'] =  "訂單確認します";
$lang['breadcrumb_success']  =  "ショッピング成功";
$lang['breadcrumb_fail']     =  "ショッピングは失敗します";
$lang['breadcrumb_brands']   =  "ブランド一覧";
$lang['breadcrumb_products'] =  "製品情報";
$lang['breadcrumb_psearch']  =  "製品検索";
$lang['breadcrumb_branch']   =  "販売拠点";
$lang['breadcrumb_about']    =  "私たちについて";
$lang['breadcrumb_contact']  =  "お問い合わせ";
$lang['breadcrumb_faq']      =  "FAQ";
$lang['breadcrumb_info']     =  "メンバープロフィール";
$lang['breadcrumb_osearch']  =  "商品追跡";
$lang['breadcrumb_coupon']   =  "ショッピングゴールド";
$lang['breadcrumb_login']    =  "ログイン/登録";


//サインイン(login)
$lang['login_title']         =  "メンバーエリア";
$lang['login_login']         =  "サインイン";
$lang['login_username']      =  "アカウント番号";
$lang['login_pass']          =  "パスワード";
$lang['login_forget']        =  "忘記パスワード?";
$lang['login_name']          =  "フルネーム";
$lang['login_tel']           =  "電話";
$lang['login_email']         =  "E-mail";
$lang['login_pass_confrim']  =  "確認しますパスワード";
$lang['login_code']          =  "コード";
$lang['login_edm']           =  "ニュースレターの購読";
$lang['login_edm_yes']       =  "サブスクリプション";
$lang['login_edm_no']        =  "不サブスクリプション";
$lang['login_agree']         =  "私は読んで同意しています";
$lang['login_provision']     =  "サービスの会員規約";
$lang['login_register']      =  "私は、登録したいです";
$lang['login_memo']          =  "Eメールのご登録アカウントと登録を入力してください、システムはあなたにパスワードを送信します E-mail！";
$lang['login_confrim']       =  "決定します";
$lang['login_privacy']       =  "<h3>プライバシーと利用規約</h3>
              <div class=\"editor\"> このサイトでは絶対に尊敬され、保護、安全なトランザクション・サービスのウェブサイトので、ウェブサイトのユーザーの私たちの個人のプライバシーを提供することにあります。以下の内容は、あなたが当社が収集方法を知っている使用して、提供された個人情報を保護します。 <br>
                <br>
                <strong>個人情報のポリシーを収集</strong> <br>
                あなたがサイトで実行取引サービスを行い、あなたは簡単にこのサイト上の情報を得ることができることを確認しますするために、我々は、アドレス...府ヒカル猫1森を含む連絡先の電話番号、電子メール、連絡先、およびその他の個人情報を提供するように求められます。<br>
                <br>
                <strong>個人的なデータを収集するためのポリシーを使用します</strong><br>
               あなたは、このウェブサイトは求人に関連するトランザクションのためにのみ使用されます、サイトの情報を入力しました。<br>
                <br>
                <strong>第三者ポリシーと個人情報を共有します</strong><br>
                1.あなたの事前の同意なしに、本ウェブサイト上で収集された個人データは、交換を販売、またはリース個人情報のいずれかを他の組織や個人に、または目的以外の目的に使用しません。
                2.他のサービスや優先権を提供するため、サービスを提供したり（共同活動を起動したり、特殊なために他の業界プレーヤー）ユーザーデータを共有するときに、サードパーティを提供するために必要としているこのサイトは完全にイベントで提供される場合は、自由にすることができますことを実証しています特定のサービスやプロモーションを受け入れるかどうかを選択します。<br>
                <br>
                <strong>クッキーの使用</strong><br>
                ユーザーの利便性とデータ入力精度を高め、あなたがデータを入力した方法を保存する際にクッキーを使用するより良いサービス、サイト上のすべてのデータフィールドを提供するためにサイト。だから、このサイトでは、このポリシーの原則の下で、お使いのブラウザでクッキーを読み書きします。あなたはクッキーが書き込まれる望まない場合は、あなたが高いために使用しているブラウザ機能キーにプライバシーレベルを設定することができ、あなたはクッキーの書き込みを拒否することができますが、いくつかのウェブサイトの機能につながる可能性があり、適切に実行することはできません。 <br>
                <br>
                <strong>改正は、プライバシーの権利を保護します</strong><br>
                このサイトのプライバシー条項は、技術動向への対応法令やその他の環境の変化を修正し、他の要因は、ユーザーのプライバシーの概念の実施を確保するために適切な変更を行います。改訂された用語は、すぐにこのサイトで公開され、随時調整されます。</div> ";


//表尾(footer)
$lang['footer_about']         =  "ブランド紹介";
$lang['footer_brands']        =  "ブランドプロフィール";
$lang['footer_branch']        =  "販売拠点";
$lang['footer_contact']       =  "お問い合わせ";
$lang['footer_follow']        =  "私たちに従ってください";
$lang['footer_faq']           =  "FAQ";
//$lang['footer_quality']       =  "品質管理テスト";
//$lang['footer_description']   =  "製品保証について";
//$lang['footer_qa']            =  "常見問題";
$lang['footer_addr']          =  "台中市中区民族路94号";


//ニュース(news)
$lang['news_title']           =  "ニュース";
$lang['news_all']             =  "完全な";
$lang['news_back']            =  "リターン";


//商品(product)&購物燈箱(product_lightBox)
$lang['product_title1']          =  "新発売";
$lang['product_title2']          =  "人気商品";
$lang['product_title3']          =  "OUTLET";
$lang['product_sku']             =  "モデル";
$lang['product_price']           =  "推奨価格";
$lang['product_price_promo']     =  "VIP価格";
$lang['product_size']            =  "サイズ";
$lang['product_color']           =  "カラー";
$lang['product_entity']          =  "数量";
$lang['product_addCart']         =  "ショッピングカートに追加します";
$lang['product_ask']             =  "商品のお問い合わせ";
$lang['product_detail']          =  "製品の詳しいです";
$lang['product_description']     =  "特長";
$lang['product_speci']           =  "製品仕様";
$lang['product_already_addCart']  =  "商品已ショッピングカートに追加します！";
$lang['product_continue']        =  "買い物を続けます";
$lang['product_checkout']        =  "私がチェックアウトしたいです";
$lang['product_back']            =  "リターン";
$lang['product_name']            =  "フルネーム";
$lang['product_tel']             =  "電話";
$lang['product_email']           =  "E-mail";
$lang['product_message']         =  "メッセージ";
$lang['product_code']            =  "コード";
$lang['product_memo1']           =  "フィールドはすぐにあなたの顧客の問題を処理するために必要とされます。";
$lang['product_memo2']           =  "‧E-mailあなたに戻って取得するために、単一のメールボックスと便利な記入してください。";
$lang['product_view']            =  "QUICKVIEW";
$lang['product_result']          =  "製品検索結果";
$lang['product_rltSuccess1']     =  "検索";
$lang['product_rltSuccess2']     =  "その結果は以下の通りであります";
$lang['product_rltfail1']        =  "検索";
$lang['product_rltfail2']        =  "結果なし一致しません。";


//ブランド一覧
$lang['brand_title1']            =  "ブランド一覧";
$lang['quick_view']              =  "QUICKVIEW";


//結帳
$lang['step1_title']             =  "買い物リスト";
$lang['step1_noData']            =  "あなたの買い物リストの任意の製品ああ、いや！";
$lang['step1_cancel']            =  "キャンセル";
$lang['step1_pname']             =  "製品名";
$lang['step1_price']             =  "単価";
$lang['step1_entity']            =  "数量";
$lang['step1_sum']               =  "小計";
$lang['step1_privacy_chk']       =  "私は理解して<span class=\"gray\">ショッピングノート";
$lang['step1_total']             =  "合計";
$lang['step1_continue']          =  "買い物を続けます";
$lang['step1_next']              =  "下一步";
$lang['step1_privacy_title']    =  "ショッピングノート";
$lang['step1_privacy']     =  "<ul>
                  <li> 配達エリアと出荷：島（無料輸送）、島（送料は$150百万になります）、理由など海運、航空輸送、などのさまざまな地域と流通方法の原因で実際の出荷貨物を出荷する外国直接送達（、貨物は...私たちを変化させます。ご注文の領収書はあなたに引用されるだろうし、あなたに連絡をした後）</li>
                  <li>新竹ベースの貨物流通方法。</li>
                  <li>当社ショッピング送金口座：第一銀行 - ノース台中支店銀行コード：007口座番号：403-10-051261ユーザー名：USレザー株式会社クラウン。</li>
                  <li>その後によるPCの画面との間の関係に色を描画財非常に真剣にゲストの色ならば、若干異なっており、注文と顧客サービスの下で購入Ruoyin色の問題は、消費者が必要な商品の返品送料を返した後ことを確認しますしてください彼らは負担しています。</li>
                  <li>返品理由の場合、例えば、欠陥製品自体、または間違った商品に、確認しますした後に我々は、交換、ここでは米国の王冠が負担貨物の一部を受け取るために、誰かが政府に連絡しなければなりません、7日以内にカスタマーサービスまでお問い合わせください。</li>
                  <li>消費者がいない場合は満足非コモディティ商品が顧客に尋ねる不良、7日とスタッフ以内にカスタマーサービスまでご連絡ください商品が当社に返却する前に、消費者自身が負担商品代を返す確認しますのためご連絡させていただきます、私たちが商品を受け取った後、商品をチェック損傷、正しい欠陥は、顧客サービスのスタッフがあなたの返品手続きを処理します。</li>
                  <li>あなたが商品を受け取った場合は屋外の着陸トライ許される唯一の感謝感謝期間、鑑賞用（祝日を含む）到着日から7日以内に、満足していない。あなたは一次産品を維持したい場合は、戻り不足している部品の付属品なしで完全ではなく、傷を行ってください損傷、摩耗や破れ、スリーブ、包装材料の完全性を維持します。この期間以上を購入することに合意したものとみなされた場合、完全な損傷を清掃してくださいすることができないオリジナルの段ボール箱を保管してください。 </li>
                  <li>保証は涙（ブレーク）損傷は保証亀裂、火災、およびさまざまな方法で他の強い化学物質の下で覆われていない、摩耗に起因する不適切な使用のために、欠陥の主な構造の製造にのみ製品が含まれています。 </li>
                </ul>";
$lang['step1_privacy_chk_ok']    =  "*ショッピング条件をチェック!";


//結帳(checkout/step2)
$lang['step2_title']             =  "注文情報を記入";
$lang['step2_otitle']            =  "購入者";
$lang['step2_name']              =  "フルネーム";
$lang['step2_tel']               =  "電話";
$lang['step2_email']             =  "E-mail";
$lang['step2_paymethod']         =  "支払い";
$lang['step2_transmethod']       =  "流通方法";
$lang['step2_coupon']            =  "折價卷序號";
$lang['step2_promo']             =  "了解ショッピングゴールド";
$lang['step2_dtitle']            =  "名宛人";
$lang['step2_the_same']          =  "同購入者";
$lang['step2_dlocal']            =  "配送地區";
$lang['step2_memo_1']            =  "註";
$lang['step2_memo_2']            =  " <div class=\"red\">海外直送：により、実際の貨物海運貨物、出荷および流通方法にあるため、異なる領域の（海、空気、等...）が変化します。
                    私達はあなたの順序はあなたに引用符で囲まれます受信し、ご連絡させていただきます。</div>";
$lang['step2_addr']              =  "アドレス";
$lang['step2_memo']              =  "発言";
$lang['step2_pre']               =  "前";
$lang['step2_next']              =  "下一步";
$lang['step2_promo_memo']        =  "ショッピングゴールド說明";
$lang['step2_memo_3']            =  "フルの日付活動は、ボリュームディスカウントを与え、メンバーは次の消費者控除を注文することができます。
                                       <br>
                                                                                                   Ruoyinは、注文をキャンセルし、ショッピングが金を得た元の順序を失うことになる、金のショッピングの活動のための条件を満たしていない結果を返します。";


//結帳(checkout/step3)
$lang['step3_title']             =  "訂單確認します";
$lang['step3_image']             =  "ピック";
$lang['step3_pname']             =  "製品名";
$lang['step3_price']             =  "単価";
$lang['step3_entity']            =  "数量";
$lang['step3_sum']               =  "小計";
$lang['step3_trans']             =  "出荷";
$lang['step3_coupon']            =  "償還クーポン";
$lang['step3_vip']               =  "VIPメンバーの特典";
$lang['step3_total']             =  "合計";
$lang['step3_oname']             =  "購入者";
$lang['step3_dname']             =  "名宛人";
$lang['step3_tel']               =  "電話";
$lang['step3_email']             =  "E-mail";
$lang['step3_addr']              =  "アドレス";
$lang['step3_ototal']            =  "注文マネー";
$lang['step3_paymethod']         =  "支払い";
$lang['step3_transmethod']       =  "流通方法";
$lang['step3_memo']              =  "発言";
$lang['step3_pre']               =  "前";
$lang['step3_confrim']           =  "確認します";


//結帳(checkout/step6_fail)
$lang['step6_fail_title']        =  "申し訳ありませんが、お買い物に失敗しました！";
$lang['step6_fail_memo_1']       =  "ご質問がある場合は、カスタマーサービスまでご連絡ください";
$lang['step6_fail_contact']      =  "お問い合わせ";
$lang['step6_fail_return']       =  "リターン";


//結帳(checkout/step6_success)
$lang['step6_success_title']     =  "ショッピング成功";
$lang['step6_success_msg']       =  "あなたの成功オーダーおめでとう〜！";
$lang['step6_success_content']   =  "ご注文手続きを完了した、ご注文をお願いいたします。";
$lang['step6_success_status']    =  "注文ステータスの確認";
$lang['step6_success_customer']  =  "メンバーエリア";
$lang['step6_success_order_qry'] =  "商品追跡";
$lang['step6_success_coupon']    =  "順序は、金割引のショッピングIDを取得した：メンバーエリアに移動します";
$lang['step6_success_coupon_get'] =  "クーポンを受け取ります";
$lang['step6_success_return']     =  "リターン";


//お問い合わせ(contact)
$lang['contact_title']       =  "お問い合わせ";
$lang['contact_name']        =  "フルネーム";
$lang['contact_tel']         =  "電話";
$lang['contact_email']       =  "E-mail";
$lang['contact_content']     =  "お問い合わせ事項";
$lang['contact_upload']      =  "写真をアップロード";
$lang['contact_msg']         =  "伝言を残します";
$lang['contact_code']        =  "コード";
$lang['contact_memo_1']      =  "フィールドはすぐにあなたの顧客の問題を処理するために必要とされます。<br>
                ‧E-mail あなたに戻って取得するために、単一のメールボックスと便利な記入してください。<br>
                ‧サービスの問題については、あなたがハード（ソフト）を購入ボックスを明記してください、私はあなたの詳しいですな説明のための顧客サービスのスタッフを容易にするために時間を買うために。";
$lang['contact_submit']      =  "送出";
$lang['contact_memo_2']      =  "お客様各位こんにちは<br>
              米国の王冠レザーウェブの顧客サービス部門は<BR>提供するためにここにあります
              ご質問があれば<br>
              私達に連絡する歓迎！";
$lang['contact_follow_us']   =  "私たちに従ってください";
$lang['contact_city']        =  "台中店";
$lang['contact_addr']        =  "台中市中区民族路94号";
$lang['contact_fax']         =  "ファックス";


//關於我們(content)
$lang['content_title']       =  "私たちについて";


//會員資料(customer_info)
$lang['customer_info_title']       =  "メンバー";
$lang['customer_info_edit']        =  "会員情報の変更";
$lang['customer_info_level']       =  "レベル";
$lang['customer_info_vip_how']     =  "VIPメンバーシップをアップグレードする方法?";
$lang['customer_info_vip_date']    =  "VIPメンバー発効日";
$lang['customer_info_to']          =  "へ";
$lang['customer_info_username']    =  "アカウント番号";
$lang['customer_info_name']        =  "フルネーム";
$lang['customer_info_tel']         =  "電話";
$lang['customer_info_email']       =  "E-mail";
$lang['customer_info_pass']        =  "パスワード";
$lang['customer_info_pass_confrim'] =  "パスワードを確認";
$lang['customer_info_edm']         =  "ニュースレターの購読";
$lang['customer_info_is_edm']      =  "購読するかどうか";
$lang['customer_info_confrim']     =  "確認します";
$lang['customer_info_vip_up']      =  "VIPメンバーのヘルプをアップグレード";
$lang['customer_info_vip_memo']    =  "年内に全容量 ".DIO_CURRENCY.addCommas( $_SESSION['sys_info']['promo']['vip']['total'] )." 元 ,VIP会員となり、VIP会員は、ショッピングを楽しみます".$_SESSION['sys_info']['promo']['vip']['discount']."%優先的な！<br>
                                                                                                        年内に全容量 ".DIO_CURRENCY.addCommas( $_SESSION['sys_info']['promo']['vip']['ctotal'] )." 元,再生可能VIP会員。";


//會員訂單(customer_order)
$lang['customer_order_title']      =  "メンバー";
$lang['customer_order_qry']        =  "商品追跡";
$lang['customer_order_no']         =  "注文番号";
$lang['customer_order_odate']      =  "注文日";
$lang['customer_order_money']      =  "マネー";
$lang['customer_order_paymethod']  =  "支払い";
$lang['customer_order_ostatus']    =  "発送状況";
$lang['customer_order_detail']     =  "細部";
$lang['customer_order_qry_view']   =  "商品追跡/ビュー";
$lang['customer_order_return']     =  "リターン";
$lang['customer_order_info']       =  "注文情報";
$lang['customer_order_oname']      =  "購入者";
$lang['customer_order_dname']      =  "名宛人";
$lang['customer_order_tel']        =  "電話";
$lang['customer_order_email']      =  "E-mail";
$lang['customer_order_addr']       =  "アドレス";
$lang['customer_order_omoney']     =  "訂單マネー";
$lang['customer_order_memo']       =  "発言";
$lang['customer_order_coupon_no']  =  "クーポン番号";
$lang['customer_order_status']     =  "状態";
$lang['customer_order_tno']        =  "貨號";
$lang['customer_order_arrTime']    =  "到着時刻";
$lang['customer_order_station']    =  "サイト";
$lang['customer_order_errMsg']     =  "エラーメッセージ";
$lang['customer_order_image']      =  "ピック";
$lang['customer_order_pname']      =  "製品名";
$lang['customer_order_entity']     =  "数量";
$lang['customer_order_price']      =  "単価";
$lang['customer_order_subTotal']   =  "マネー";
$lang['customer_order_total']      =  "合計";


//會員ショッピングゴールド(customer_coupon)
$lang['customer_coupon_title']      =  "メンバー";
$lang['customer_coupon_coupon']     =  "ショッピングゴールド";
$lang['customer_coupon_orderNo']    =  "注文番号";
$lang['customer_coupon_action']     =  "活動のプログラム";
$lang['customer_coupon_couponNo']   =  "折價卷序號";
$lang['customer_coupon_money']      =  "マネー";
$lang['customer_coupon_sdate']      =  "発効日";
$lang['customer_coupon_edate']      =  "満期日";
$lang['customer_coupon_status']     =  "状態を使用してください";


//會員分類清單(customer_menu)
$lang['customer_menu_info']         =  "メンバープロフィール";
$lang['customer_menu_orderQry']     =  "商品追跡";
$lang['customer_menu_coupon']       =  "ショッピングゴールド";
$lang['customer_menu_logout']       =  "サインアウト";


/* End of file caseinc_lang.php */