<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 列表模型  Product_model 
 *    針對產品主檔(product)的列表進行資料映射。
 *
 * @modelName Product_model
 * @author	Dio
 */
class Product_model extends CI_Model {
	
	// --------------------------------------------------------------------
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
    public function __construct(){
    	
       $this->p   = 'product'; 
       $this->ps  = 'product_speci'; 
       $this->pq  = 'product_question';
       $this->cpr = 'product_reviews'; 
       $this->pm  = 'product_memo'; 
       
       $this->c   = 'customer';
       $this->cg  = 'category';
       $this->pr  = 'property';
       
       $this->b   = 'brand';
    }    
   
	// --------------------------------------------------------------------

	/**
	 * 方法: 搜尋條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function search($srh_data){
		
		//串接SQL語句
		//$sql_where = " WHERE {$this->p}.status = 1 AND (SELECT SUM({$this->ps}.stock) > 0 ) ";
		$sql_where = " WHERE {$this->p}.status = 1 AND type_id = 0 ";
		
			  
		//依[類別] / 商品分類搜尋(複選)
		if( isset($srh_data['srh_category_id_2']) && is_numeric($srh_data['srh_category_id_2']) ){
			//次級分類的所有商品
			$sql_where .= " AND ( {$this->p}.category_multi_id REGEXP '(^|,)(".$srh_data['srh_category_id_2'].")(,|$)' )" ;
		
		}else{
			//頂級分類的所有商品
			$category_id_ary = array();
			
			$this->db->select('category_id');
			$this->db->where('parent_id' ,$srh_data['srh_category_id_1']);
			$this->db->where('level' ,3);
			$category_id_ary = $this->db->get('category')->result_array();
				
			$sql_where .= " AND ( {$this->p}.category_multi_id REGEXP '(^|,)(".$category_id_ary[0]['category_id'].")(,|$)'" ;
			
			foreach ($category_id_ary as $key=>$row){
				if($key > 0){
					$sql_where .= " OR {$this->p}.category_multi_id REGEXP '(^|,)(".$row['category_id'].")(,|$)' " ;
				}
			}
			
			$sql_where .= " ) " ;
			
		}
		
		//依商品名稱
		if( !empty($srh_data['srh_data']) ){
			$sql_where .= " AND {$this->p}.name LIKE '%".$srh_data['srh_data']."%'";
			$sql_where .= " OR {$this->p}.sku LIKE '%".$srh_data['srh_data']."%'";
		}
		
		//依關鍵字
		if( !empty($srh_data['srh_keyword']) ){
			$sql_where .= " AND ( {$this->p}.keyword LIKE '%".$srh_data['srh_keyword']."%' ";
			$sql_where .= " AND {$this->p}.product_id <> '".$srh_data['srh_product_id']."' )";
		}
		
		 //依[價格區間]
		 if( !empty($srh_data['srh_price1']) && !empty($srh_data['srh_price2'])  ){
			$sql_where .= " AND {$this->p}.price >= '".$srh_data['srh_price1']."'
			AND {$this->p}.price <= '".$srh_data['srh_price2']."'" ;
		 }
		 
		 //依廣告分類
		 if( !empty($srh_data['srh_ad_id']) ){
		 	$sql_where .= " AND {$this->p}.ad_id = {$srh_data['srh_ad_id']}";
		 }
		 
		 //依[是否顯示在首頁]
		 if( !empty($srh_data['srh_is_show_home']) ){
		 	$sql_where .= " AND {$this->p}.is_show_home = {$srh_data['srh_is_show_home']}";
		 }

		 //用GROUP BY過濾子表重複
		 //$sql_where .= " GROUP BY {$this->p}.product_id";
		 
		 return $sql_where;
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法: 排序條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function sort($srh_data){
	
		$sql_sort = " ORDER BY `sort_order` DESC";

		//串接SQL語句ORDER BY
		if( !empty($srh_data['srh_sort']) ){

			switch ($srh_data['srh_sort']) {
				
				case 'news':
					$sql_sort = " ORDER BY `cdate` DESC";
				break;
				
				case 'price_high':
					$sql_sort = " ORDER BY `price` DESC";
				break;
				
				case 'price_low':
					$sql_sort = " ORDER BY `price` ASC";
				break;
							
				case 'char_low': 
					$sql_sort = " ORDER BY `name` DESC";
				break;						
			}
		
		}
		
		return $sql_sort;
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法: 分頁條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function page($srh_data){
			
		$sql_limit = "";
		
		if( isset($srh_data['srh_limit1']) && isset($srh_data['srh_limit2'])){
			$sql_limit = " LIMIT {$srh_data['srh_limit1']} ,{$srh_data['srh_limit2']}";			
		}
		
		return $sql_limit;
	}
		
	// --------------------------------------------------------------------

	/**
	 * 方法: 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists($srh_data=array()){
	
		$sql_where = $this->search($srh_data);
		$sql_limit = $this->page($srh_data);
		$sql_sort  = $this->sort($srh_data);

		$sql = "SELECT {$this->p}.*
		              ,{$this->pm}.techenique
		              ,{$this->ps}.sku2
		              ,{$this->cg}.parent_id
		              ,{$this->cg}.meta_title
		              ,{$this->cg}.meta_description
		              ,{$this->cg}.meta_keyword
	               FROM {$this->p}
		          LEFT JOIN {$this->pm} ON {$this->p}.product_id = {$this->pm}.product_id
                  LEFT JOIN {$this->cg} ON {$this->p}.category_id = {$this->cg}.category_id
                  LEFT JOIN {$this->ps} ON {$this->p}.product_id = {$this->ps}.product_id
                  {$sql_where} 
                  GROUP BY {$this->p}.product_id
                  {$sql_sort} {$sql_limit} ";
                  
		$query = $this->db->query($sql)->result_array();
				
		if( !empty($query) ){
		   	return $query;
		}else{
		   	return array();
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法: 列表 / 總計
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists_group_by($srh_data){

		$sql_where = $this->search($srh_data);

		$sql = "SELECT COUNT(DISTINCT {$this->p}.product_id) as count
		          FROM {$this->p}
		        LEFT JOIN {$this->ps} ON {$this->p}.product_id = {$this->ps}.product_id 
                LEFT JOIN {$this->cg} ON {$this->p}.category_id = {$this->cg}.category_id     
               {$sql_where}";
               
		$query = $this->db->query($sql)->row_array();
		
		if( !empty($query['count']) ){
		   return $query['count'];
		        
		}else{
		   
		   return 0;
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 檢視
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function view($category_id ,$product_id){
		
		$sql = "SELECT {$this->p}.*
		,{$this->ps}.product_speci_id
		,{$this->b}.title as brand_id_name
		,{$this->pm}.description
		,{$this->pm}.specification
		,{$this->pm}.notice
		,{$this->pm}.techenique
		,SUM({$this->ps}.stock) as ps_stock
		,{$this->cg}.edate as cg_edate
		FROM {$this->p}
		LEFT JOIN {$this->cg} ON {$this->cg}.category_id = ?
		LEFT JOIN {$this->pm} ON {$this->p}.product_id   = {$this->pm}.product_id
		LEFT JOIN {$this->ps} ON {$this->p}.product_id   = {$this->ps}.product_id
		LEFT JOIN {$this->b}  ON {$this->p}.brand_id     = {$this->b}.brand_id
		WHERE {$this->p}.product_id = ?
		GROUP BY({$this->p}.product_id)";
			
		$query = $this->db->query($sql,array($category_id ,$product_id))->row_array();
			
		if( !empty($query) ){
	    	return $query;
	    }
		
	    return array();
	}
	
		// --------------------------------------------------------------------
	
			/**
		 * 方法: 檢視 / 明細(屬性)
		 *
		 * @access	public
		 * @param
		 * @return
		 */
		 public function view_detail($param){
	
		 	$param_key;
		 	$param_val;
	
		 	foreach($param as $key=>$val){
				$param_key = $key;
				$param_val = $val;
			}
	
			$sql = "SELECT {$this->ps}.*
		 	,prc.name as prc_name ,prs.name as prs_name
		 	FROM {$this->ps}
		 	LEFT JOIN {$this->pr} prc ON {$this->ps}.property_id_color = prc.property_id
		 	LEFT JOIN {$this->pr} prs ON {$this->ps}.property_id_size = prs.property_id
		 	WHERE {$this->ps}.{$param_key} = ? ";
	
		 	$query = $this->db->query($sql,array($param_val))->result_array();
		 		
		 	if( !empty($query) ){
				return $query;
		 	}else{
				return array();
		 	}
    }
	
    // --------------------------------------------------------------------
    
    /**
     * 方法: 基本資料
     *       傳入參數為資料欄位的可用參數
     *
     * @access	public
     * @param
     * @return
     */
    public function info($param){
    
    	$param_key;
    	$param_val;
    
    	foreach($param as $key=>$val){
    		$param_key = $key;
    		$param_val = $val;
    	}
    	 
    	$sql = "SELECT {$this->ps}.*
    	,{$this->p}.category_multi_id
    	,{$this->p}.name
    	,{$this->p}.price
    	,{$this->p}.image as p_image
    	,prc.name as prc_name
    	,prs.name as prs_name
    	FROM {$this->ps}
    	LEFT JOIN {$this->p} ON {$this->ps}.sku = {$this->p}.sku
    	LEFT JOIN {$this->pr} prc ON {$this->ps}.property_id_color = prc.property_id
    	LEFT JOIN {$this->pr} prs ON {$this->ps}.property_id_size = prs.property_id
    	WHERE {$this->ps}.{$param_key} = ? ";
    	 
    	$query = $this->db->query($sql ,array($param_val))->row_array();
    	 
    	if( !empty($query) ){
    		return $query;
    	}else{
    		return array();
    	}
    }

}

/* End of file Product_model */