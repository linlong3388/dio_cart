<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 模型 - checkout_return 
 * @modelName checkout_return_model
 * @author	Dio
 */
class checkout_return_model extends CI_Model {
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 建立訂單資料
	 *
	 * @access	public
	 * @param	$data 為 SESSION 資料，分[客戶資料]和[訂單資料]
	 * @return  order_id
	 */
	public function addOrder($data){
		
		//訂單(主表)
		$data_master = array(
				'order_show_id' => g_order_id() ,
				'customer_id'   => $data['info']['customer_id'] ,
				'vendor_id'     => $data['info']['vendor_id'] ,
				'name'          => $data['info']['name'] ,
				'mobile'        => $data['info']['mobile'] ,
				'phone'         => $data['info']['phone'] ,
				'email'         => $data['info']['email'] ,
				'pay'           => $data['info']['pay'] ,    
				//'trans_method'  => $data['info']['trans_method'] ,  
				//'ticket_type'   => $data['info']['ticket_type'] ,
				//'ticket3_title' => $data['info']['ticket3_title'] ,
				//'ticket3_unified' => $data['info']['ticket3_unified'] ,
				'local'         => $data['info']['local'] ,
				'address'       => $data['info']['address'] ,
				//'shipping_fee'  => $_SESSION['sys_info']['shipping_fee'] , 
				'memo'          => $data['info']['memo'] ,
				'status'        => 4 ,
				'cdate'         => date('Y-m-d H:i:s')
		);
		
		$this->db->insert('order' ,$data_master);
		
		$order_id = $this->db->insert_id();
		
		//訂單(副表)
		foreach ($data['cart'] as $row) {
			
			$data_detail = array(
				'order_id'           => $order_id ,
				'product_id'         => $row['product_id'] ,
				'attr'               => $row['attr'] ,
				'entity'             => $row['entity'] ,
				'price'              => !empty($row['price_promo']) ? $row['price_promo'] : $row['price'] ,
				'total'              => $row['total'] ,
			    'status'             => 1,
				//'status'             => !IsStockEnough($row['sku2'] ,$row['entity']) ? 1 : 0 ,	
				'cdate'              => $row['cdate']
			);
			
			$this->db->insert('order_detail' , $data_detail);
		}
			
		return $order_id;		
	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 更新訂單資料 / 超商取貨的物流資訊
	 *
	 * @access	public
	 * @param	
	 * @return  
	 */
	public function updateOrderLogisticsCvs( $param ){

		$data = array(
				'logistics_status_code'  => $param['RtnCode'] ,
				'logistics_status_msg'   => $param['RtnMsg'] ,
				'logistics_status_udate' => $param['UpdateStatusDate'] ,
		);

		$this->db->where('order_show_id' ,$param['MerchantTradeNo']);
		$query = $this->db->get('order')->row_array();
		
		if( empty($query['logistics_no']) ){
			$data['logistics_no'] = $param['AllPayLogisticsID'];
		}

		if( empty($query['logistics_payment_no']) ){
			$data['logistics_payment_no'] = $param['CVSPaymentNo'];
		}
		
		if( empty($query['logistics_validation_no']) ){
			$data['logistics_validation_no'] = $param['CVSValidationNo'];
		}
		
		$this->db->limit(1);
		$this->db->where('order_show_id' ,$param['MerchantTradeNo']);
		$this->db->update('order' ,$data);
	}
	
}

/* End of file vendor_checkout_return_model */