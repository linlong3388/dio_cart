<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 列表模型sign_in_model 
 *
 * @author	Dio
 */
class sign_in_model extends CI_Model {
	
	// --------------------------------------------------------------------
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
    public function __construct(){}    
   
	// --------------------------------------------------------------------

	/**
	 * 方法: 取得會員資料 (依帳/密)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getCustomerInfo($username ,$password){
		
		$this->db->where('username' ,$username);
		$this->db->where('password' ,MD5($password));
		$this->db->where('status' ,1);
		
		$query = $this->db->get('customer')->row_array();
		
	    return $query;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 取得會員資料 (依會員ID)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getCustomerInfoByCustomerId( $customer_id ){
	
		$this->db->where('customer_id' ,$customer_id);
		$this->db->where('status' ,1);
	
		$query = $this->db->get('customer')->row_array();
	
		return $query;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得會員資料 / 依社群ID
	 *
	 * @access	public
	 * @param
	 * @return
	 */
    public function getCustomerInfoBySociety($username) {
			
    	$this->db->where('log_id >' ,0); //不包含[未註冊][一般會員]
		$this->db->where('username' ,$username);
	
		return $this->db->get('customer')->row_array();
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 取得會員折價券資料 
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getCustomerCoupon($customer_id){
	
		$this->db->where('customer_id' ,$customer_id);
	
		$query = $this->db->get('customer_coupon')->result_array();
	
		return $query;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 驗證會員資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function isCustomerInfo($username ,$password){
	
		$this->db->where('username' ,$username);
		$this->db->where('password' ,MD5($password));
		$this->db->where('status' ,1);
	
		$query = $this->db->get('customer');
	
		return ($query->num_rows() <= 0 ) ? false : true ;
	}
	
}

/* End of file sign_in_model */