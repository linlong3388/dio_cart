<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 模型 - return_model 退(換)貨模型
 * 說明 : 針對退(換)貨主檔(return)進行資料映射。
 *
 * @modelName return_model
 * @author	Dio
 */
class Return_model extends CI_Model {
	
	// --------------------------------------------------------------------
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		 
		$this->r   = 'return';
		$this->rd  = 'order_detail';
		$this->p   = 'product';
	}	
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 搜尋條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_condition($srh_data){
	
		//串接SQL語句
		$sql_where = " WHERE `{$this->r}`.return_id <> '' ";
	
		//依[會員編號]
		if( !empty($srh_data['srh_customer_id']) ){
			$sql_where .= " AND `{$this->r}`.customer_id = '".$srh_data['srh_customer_id']."'";
		}
	
		return $sql_where;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 排序條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_sort_condition($srh_data){
	
		$sql_sort = "";
	
		//串接SQL語句ORDER BY
		if( !(empty($srh_data['srh_sort'])) && !(empty($srh_data['srh_sort_type'])) ){
			$sql_sort = " ORDER BY  ".$srh_data['srh_sort']." ".$srh_data['srh_sort_type'];
		}else{
			$sql_sort = " ORDER BY `{$this->r}`.return_id DESC";
		}
	
		return $sql_sort;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 分頁條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_page_condition($srh_data){
	
		$sql_limit = " LIMIT {$srh_data['limit1']},{$srh_data['limit2']}";
	
		return $sql_limit;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_list($srh_data){
			
		//搜尋條件
		$sql_where = $this->shr_condition($srh_data);
		$sql_limit = $this->shr_page_condition($srh_data);
		$sql_sort  = $this->shr_sort_condition($srh_data);
		
		$sql = "SELECT `{$this->r}`.* ,{$this->p}.name as p_name ,{$this->rd}.order_id as order_id
		         FROM `{$this->r}`
		           LEFT JOIN {$this->rd} ON `{$this->r}`.order_detail_id = {$this->rd}.order_detail_id
	        	   LEFT JOIN {$this->p} ON `{$this->rd}`.product_id = {$this->p}.product_id
		         {$sql_where} {$sql_sort} {$sql_limit} ";
		
		$query = $this->db->query($sql)->result_array();
	
		if( !empty($query) ){
			return $query;
		}else{
			return array();
		}
	}	
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 列表(總計)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_list_total($srh_data){
	
		//搜尋條件
		$sql_where = $this->shr_condition($srh_data);
	
		$sql = "SELECT COUNT({$this->r}.return_id) as total FROM `{$this->r}` {$sql_where}";
	
		$query = $this->db->query($sql)->row_array();
	
		if( !empty($query['total']) ){
			return $query['total'];
		}else{
			return 0;
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 明細
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_detail($customer_id ,$return_id){
			
		$sql = "SELECT `{$this->r}`.*
		         ,{$this->rd}.product_id
		         ,{$this->rd}.order_id
		         ,{$this->p}.sku
      		     ,{$this->p}.name as p_name
      		     ,{$this->p}.category_id as p_category_id 
		       FROM `{$this->r}`
		         LEFT JOIN {$this->rd} ON `{$this->r}`.order_detail_id = {$this->rd}.order_detail_id
		         LEFT JOIN {$this->p} ON `{$this->rd}`.product_id = {$this->p}.product_id
		       WHERE `{$this->r}`.return_id = {$return_id}
		       AND `{$this->r}`.customer_id = {$customer_id}";
	
		$query = $this->db->query($sql)->row_array();
	
		if( !empty($query) ){
		   return $query;
		}else{
		   return array();
	    }
	}

}

/* End of file return_model */