<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 模型 - 會員中心的功能
 * @modelName Member_model
 * @author	Dio
 */
class member_model extends CI_Model {

	// --------------------------------------------------------------------

	/**
	 * 方法: 會員資料
	 *
	 * @access	public
	 * @param $_SESSION
	 * @return
	 */
	public function get_info($customer_id){

		$sql = "SELECT c.* ,cdr.first_name ,cdr.last_name ,cdr.mobile ,cdr.email as cdr_email  ,cdr.conutry ,cdr.city ,cdr.zip ,cdr.local ,cdr.address
	              FROM `customer` c 
		        LEFT JOIN `customer_addr` cdr ON c.customer_id = cdr.customer_id
		        WHERE c.customer_id = {$customer_id}
		        AND cdr.status='1' LIMIT 1";

		$query = $this->db->query($sql)->row_array();
			
		if( !empty($query) ){
			return $query;
		}else{
			return array();
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法: 更新 / 設置上一次登入時間
	 *
	 * @access	public
	 * @param $_SESSION
	 * @return
	 */
	public function set_login_time(){

		$this->db->where('customer_id' ,$_SESSION['customer_info']['customer_id']);
		$this->db->update('customer' ,array( 'pre_date' => $_SESSION['customer_info']['log_date'] ,'log_date' => date('Y-m-d H:i:s') ));
			
		$_SESSION['customer_info']['pre_date'] = $_SESSION['customer_info']['log_date'];
	}

	// --------------------------------------------------------------------

	/**
	 * 方法: 取得 30 天內的交易紀錄
	 *
	 * 說明: 輸入/出 值階為SESSION
	 * @access	public
	 * @param  $_SESSION
	 * @return	int
	 */
	public function get_30order(){

		$sql = "SELECT COUNT(order_id) as total FROM `order`
    	          WHERE `customer_id` = '".$_SESSION['customer_info']['customer_id']."' 
    	          AND SUBSTR(cdate,1,10) BETWEEN '".date('Y-m-d',strtotime('-30 day'))."' AND '".date('Y-m-d')."' ";                                    

		$result = $this->db->query($sql)->row_array() ;
			
		return $result['total'];
	}

	// --------------------------------------------------------------------

	/**
	 * 方法: 取得 30 天內的問答紀錄
	 *
	 * 說明: 輸入/出 值階為SESSION
	 * @access	public
	 * @param  $_SESSION
	 * @return	int
	 */
	public function get_30question(){

		$sql = "SELECT COUNT(question_id) as total FROM `ct_question`
    	          WHERE `customer_id` = '".$_SESSION['customer_info']['customer_id']."' 
    	          AND SUBSTR(cdate,1,10) BETWEEN '".date('Y-m-d',strtotime('-30 day'))."' AND '".date('Y-m-d')."' ";                                    

		$result = $this->db->query($sql)->row_array() ;
			
		return $result['total'];
	}


}


/* End of file member_model */