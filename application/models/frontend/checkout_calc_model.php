<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 模型 - 結帳流程 / 計算
 * @modelName checkout_calc_model
 * 
 * 處理影響購物車總計的相關金額項目(如:運費，各種促銷活動...等)。
 *   注意: 1) 使用前提會員需為登入狀態，因為以登入SESSION為前提
 *         2) 為了跨頁面考量，採用SESSION記錄，而不考慮 return
 *       
 * @author	Dio
 */
class checkout_calc_model extends CI_Model {
	
	// --------------------------------------------------------------------
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		 
		$this->load->library(array('promo/Coupon'));
		
		$this->Coupon = new Coupon();
	}
	
	
    // --------------------------------------------------------------------
	//
	// 運費共有四種: 1)依溫度
	//              2)依訂單滿額  
	//              3)依商品滿額(待確認)
	//              4)依地區
	//
	// --------------------------------------------------------------------
	
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 運費規則
	 * 
	 * @access	public
	 * @param  $_SESSION['my_order']['cart']
	 * @return $_SESSION['my_order']['fee']
	 */
	public function getFee($cart){
		
		$CI =& get_instance();
		$CI->load->model("frontend/checkout_fee_model" ,"checkout_fee");
		
		$CI->checkout_fee->getFee();
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 取得 貨到付款(手續費)
	 *         
	 * @access	public
	 * @param  
	 * @return 
	 */
	public function getFeeCod( $trans_method ){
		
		$total = 0;
		
		switch ( $trans_method ) {
			//貨到付款
			case 1:
			    $total = $_SESSION['sys_info']['fee_cod'];
			break;
		}

		if( $total <= 0 )
			return false;		
		
		$_SESSION['my_order']['fee_cod']['item_type'] = '+';
		$_SESSION['my_order']['fee_cod']['item_id']   =  0;
		$_SESSION['my_order']['fee_cod']['func']      = 'fee_cod';
		$_SESSION['my_order']['fee_cod']['name']      = '貨到付款(手續費)';
		$_SESSION['my_order']['fee_cod']['total']     = $total;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 取得 折價券
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getCoupon(){
		
		if( empty($_SESSION['my_order']['info']['code_use']) ){
            unset($_SESSION['my_order']['coupon']);
			return false;
		}	
				
		$total = 0;
		
		//指定會員所屬折價券
		//$query = $this->Coupon->getByCodeUse($_SESSION['my_order']['info']['code_use']);
		
		if($_SESSION['sys_info']['promo']['coupon']['code_type'] == 1){

			$total = $_SESSION['sys_info']['promo']['coupon']['code_val'];
		}else{
			$total_cart = $this->cls_cart->count_all_total();
			$total      = round($total_cart*($_SESSION['sys_info']['promo']['coupon']['code_val']/100));
		}

		//助糧無法使用折價券
		if( isCategory_3ForCart() ){
			$total = 0;
		}
		
		$_SESSION['my_order']['coupon']['item_type'] = '-';
		$_SESSION['my_order']['coupon']['item_id']   =  0;
		$_SESSION['my_order']['coupon']['func']      = 'coupon';
		$_SESSION['my_order']['coupon']['name']      = '優惠券';
		//$_SESSION['my_order']['coupon']['total']     = $query['total'];
		$_SESSION['my_order']['coupon']['total']     = $total;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 取得購物車總計
	 *         含所有折扣，運費...等
	 *
	 * @access	public
	 * @param  
	 * @return $_SESSION['my_order']['total']
	 */
	public function getTotal(){

		$total = $this->cls_cart->count_all_total();
		
		//運費
		if( !empty($_SESSION['my_order']['fee']) ){
				$total += $_SESSION['my_order']['fee']['total'];
		}

		//手續費
		/*
		if( !empty($_SESSION['my_order']['fee_cod']['total']) ){
			$total += $_SESSION['my_order']['fee_cod']['total'];
		}*/		
		
		//折價券
		if( !empty($_SESSION['my_order']['coupon']['total']) ){
			$total -= $_SESSION['my_order']['coupon']['total'];
		}
		
		$_SESSION['my_order']['total'] = $total;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 計算購買數
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function calc_entity($entity){
	
		$_SESSION['my_order']['entity'] = $entity;
	}

}


/* End of file checkout_calc_model */