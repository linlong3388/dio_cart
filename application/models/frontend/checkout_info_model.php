<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 模型 - 結帳流程 / 資訊
 * @modelName checkout_info_model
 * @author	Dio
 */
class checkout_info_model extends CI_Model {
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 取得會員資料
	 *
	 * @access	public
	 * @param $_SESSION
	 * @return
	 */
	public function getCustomerInfo($customer_id){
	
		$sql = "SELECT c.* ,cdr.first_name ,cdr.last_name ,cdr.mobile ,cdr.email as cdr_email  ,cdr.conutry ,cdr.city ,cdr.zip ,cdr.local ,cdr.address
		          FROM `customer` c
		        LEFT JOIN `customer_addr` cdr ON c.customer_id = cdr.customer_id
		        WHERE c.customer_id = {$customer_id}
		        AND cdr.status='1' LIMIT 1";
	
		$query = $this->db->query($sql)->row_array();
			
		if( !empty($query) ){
			return $query;
		}else{
			return array();
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得會員地址資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getCustomerAddr($customer_id){
	
		$this->db->where('customer_id' ,$customer_id);
		$this->db->where('status' ,1);
	    $this->db->order_by('sort_order' ,'DESC');
		 
		return $this->db->get('customer_addr')->result_array();
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得會員常用超商資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getStoreInfo($customer_id){
	
		$this->db->where('customer_id' ,$customer_id);
		$this->db->where('status' ,1);
	
		return $this->db->get('customer_store')->result_array();
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 驗證訂單商品明細的庫存量
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function isStock(){
	
		/* 暫不使用
		foreach ($this->cls_cart->select_all() as $row){
			if( !IsStockEnough($row['sku'] ,$row['entity']) ){
				dioRedirect("checkout/step0","抱歉，商品型號: {$row['sku']} / {$row['name']} 的庫存不足!");
			}
		}*/
	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得盒裝商品
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getProductBoxList(){
	
		$data = array();
	
		$this->db->where('status' ,1);
		$this->db->where('type_id' ,1);
		$this->db->order_by('sort_order' ,'DESC');
		$query_box = $this->db->get('product')->result_array();
	
		foreach ($query_box as $key=>$row){
	
			$data[$key] = $row;
			 
			$this->db->where('product_id' ,$row['product_id']);
			$this->db->order_by('sort_order' ,'DESC');
			$query = $this->db->get('product_attr')->result_array();
			
			$data[$key]['images'] = $query;
		}
	
		return $data;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得商品&組圖
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getProductBoxCombList($unit){

		$data = array();
		
		$sql = "SELECT p.* 
				   FROM `product` p
		          WHERE  p.status = 1
				    AND p.type_id = 0 
				    AND p.is_comb = 1
				    AND (SELECT DISTINCT `unit` FROM `product_comb` 
				           WHERE product_id = p.product_id
				           AND unit='".$unit."')";
		
		$query_box = $this->db->query($sql)->result_array();
		
		foreach ($query_box as $key=>$row){
		
			$data[$key] = $row;
		
			$this->db->where('product_id' ,$row['product_id']);
			$this->db->where('unit' ,$unit);
			$this->db->order_by('entity' ,'ASC');
			$query = $this->db->get('product_comb')->result_array();
		
			if( !empty($query) ){
		    	foreach ($query as $key2=>$row2){
			    	$data[$key]['comb'][$key2] = $row2;
				    $data[$key]['comb'][$key2]['isCheck'] = 0;
			    }
			}
			
		}
		
		return $data;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 依商品ID取得禮盒資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getProductInfoByTypeId1( $product_id ){
		
		$product_id = explode('|', $product_id);
		
		$this->db->where('product_id' ,$product_id[1]);
		$query = $this->db->get('product')->row_array();
		
		return $query;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得[發票寄送地址]資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getTicketAddr( $param ){
	
		$addr = "";
		
		if($param['ticket_addr_id'] == 0){
			$addr = $param['o_local'].' '.$param['o_address'];
		}else{
			$addr = $param['local'].' '.$param['address'];
		}
	
		return $addr;
	}
	
	// --------------------------------------------------------------------
	 
	/**
	 * 方法 : 組合預覽[上一步]導向
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function step1Comb2Redirect(){
		 
		$url = getUserURL('checkout/step1_comb_1');
		
		//頁面導向
		if($_SESSION['page']['prev'] == 'checkout/step1'){
			$url = getUserURL('checkout/step1');
		}
		
		return $url;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得超商資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getCvsStoreInfo(){
	
		//串接金流(CI無法引入抽象類別，所以需獨立引入)
		require_once(APPPATH.'libraries/cash_flow/Cash_flow.php');
		$this->Dio_ECPay_Logistics_UNIMART = Cash_flow::getInstance('ECPay/Dio_ECPay_Logistics_UNIMART');
		
		echo "交易處理中，請勿關閉視窗...";
		
		$ECPayCvsMap = $this->Dio_ECPay_Logistics_UNIMART->setParamsMap($_SESSION['my_order']['info']);
		
		$str = <<<EOD
                 $ECPayCvsMap
                 <script>
                    document.getElementById("ECPayForm").submit();
                 </script>
EOD;
				
		echo $str;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 設置會員已選取的超商資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function setCustomerSelectStoreInfo( $param ){
		
		if( empty($param) ){
			unset( $_SESSION['my_order']['cvs'] );
		    return ;
		}
			
		$_SESSION['my_order']['cvs']['cvs_no']                 = $param['MerchantTradeNo'];
		$_SESSION['my_order']['cvs']['cvs_logistics_sub_type'] = $param['LogisticsSubType'];
		$_SESSION['my_order']['cvs']['cvs_store_id']           = $param['CVSStoreID'];
		$_SESSION['my_order']['cvs']['cvs_store_name']         = $param['CVSStoreName'];
		$_SESSION['my_order']['cvs']['cvs_address']            = $param['CVSAddress'];
		$_SESSION['my_order']['cvs']['cvs_telephone']          = $param['CVSTelephone'];
		$_SESSION['my_order']['cvs']['cvs_extra_data']         = $param['ExtraData'];
	}
		
}


/* End of file checkout_info_model */