<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 模型 - 結帳流程 / 驗證
 * @modelName checkout_valid_model
 * 
 * @author	Dio
 */
class checkout_valid_model extends CI_Model {
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 驗證超商取貨資料驗證
	 *        綠界
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function isEcpayCvsDataValid( $param ){
	
		$errMsg = "";
				
		if( $param['pay_method'] == '2' || $param['pay_method'] == '3'){
			
			//------------------------------------------------------
			//
			// 寄件者驗證
			//
			//------------------------------------------------------
			
			//寄件者姓名 : 字元限制為 10 個字元(最多 5 個中文字、10 個英文字)
			if( empty($_SESSION['sys_info']['cvs_name']) ){
				$errMsg = "*寄件者姓名不可空白!";
			}
			
			if( mb_strlen( $_SESSION['sys_info']['cvs_name'] ) > 5 ){
				$errMsg = "*依超商取貨規定，寄件者姓名必須 小於 五個字!";
			}
			
			//寄件者電話 : 允許數字+特殊符號；特殊符號僅限()-#
			if( !empty($_SESSION['sys_info']['cvs_phone']) ){
			   if( !preg_match('/^\(?\d{2}\)?\-?\d{2,6}\-?\d{2,6}(#\d{1,6}){0,1}$/', $_SESSION['sys_info']['cvs_phone']) ){
				   $errMsg = "*依超商取貨規定，寄件者電話號碼格式不符，格式如 : 02-22111111 !";
			   }
			}
			
			//寄件者手機 : 只允許數字、 10 碼、 09 開頭
			if( empty( $_SESSION['sys_info']['cvs_mobile'] ) ){
				$errMsg = "*寄件者手機不可空白!";
			}
				
			if( !is_numeric( $_SESSION['sys_info']['cvs_mobile'] ) ){
				$errMsg = '*寄件者手機必須為數字!';
			}
			
			if( strlen( $_SESSION['sys_info']['cvs_mobile'] ) != 10 ) {
				$errMsg = '*寄件者手機必須為10碼!';
			}
				
			if(  substr($_SESSION['sys_info']['cvs_mobile'] ,0,2) != '09' ){
				$errMsg = '*寄件者手機前兩碼必須為"09"開頭!';
			}
			
			//------------------------------------------------------
            //
            // 收件者驗證
            //
			//------------------------------------------------------
			
			//收件者姓名 : 字元限制為 10 個字元(最多 5 個中文字、10 個英文字)
			if( empty($param['last_name']) ){
				$errMsg = "*姓名不可空白!";
			}
						
			if( mb_strlen( $param['last_name'] ) > 5 ){
				$errMsg = "*依超商取貨規定，姓名必須 小於 五個字!";
			}
						
			//收件者電話 : 允許數字+特殊符號；特殊符號僅限()-#
			if( !empty($param['phone']) ){
			   if( !preg_match('/^\(?\d{2}\)?\-?\d{2,6}\-?\d{2,6}(#\d{1,6}){0,1}$/', $param['phone']) ){
				   $errMsg = "*依超商取貨規定，寄件者電話號碼格式不符，格式如 : 02-22111111 !";
			   }
			}
			
			//收件者Email : 標準Email格式，最大長度 100
			if( !empty($param['email']) ){
				
				if( strlen($param['email']) > 100 ) {
					$errMsg = '*寄件者Email長度不可超過100個字元!';
				}
				
				if( !preg_match('/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9]{2,4}$/', $param['email']) ){
					$errMsg = "*寄件者Email格式不符，格式如 : test@gmail.com !";
				}
			}
			
			//收件者手機 : 只允許數字、 10 碼、 09 開頭
			if( empty($param['mobile']) ){
				$errMsg = "*手機不可空白!";
			}
			
			if( !is_numeric($param['mobile']) ){
				$errMsg = '*手機必須為數字!';
			}
						
			if( strlen($param['mobile']) != 10 ) {
				$errMsg = '*手機必須為10碼!';
			}
			
			if(  substr($param['mobile'] ,0,2) != '09' ){
				$errMsg = '*手機前兩碼必須為"09"開頭!';
			}
			
			if( $this->cart->count_all_total() > 20000 ){
				$errMsg = '*依超商取貨規定，購物金額不得超過 20,000元! 請改以其他付款方式!';
			}
			
		}
		
		return $errMsg;
	}
	
}


/* End of file checkout_valid_model */