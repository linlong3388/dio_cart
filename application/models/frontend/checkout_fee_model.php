<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 模型 - 結帳流程 / 運費
 * 	  運費規則
 *  
 *         ●本島常溫一律160元，冷藏、冷凍一律 210元，不分盒數
 *         ●離島一律增加110元
 *         ●折扣後（如有）滿3000元免運，不分本島離島皆享有優惠，但限同一個溫層
 *         ●單筆訂單不同溫層，分開計算滿額免運與分開配送運費，個別如果未達3000元，運費3個都算
 *         ●後台需有可以修改運費的欄位，方便日後調整
 *      
 *         2018/01/10 遺漏 
 *         
 *         ●親臨門市取貨運費為0
 *         ●指定商品(滿額/數量/訂購時間)免運：兩者(滿額免運+指定商品免運)條件相衝突時，以”指定商品”為優先；
 *          兩者合併訂單時，運費分別都要計算
 *          補充說明：運費追加指定商品免運，在同一溫層下，僅限單買該商品，可享有免運，若訂單還有其他商品，
 *          
 *          則運費需額外計算 :
 *            如 : 果乾單買 600 (免運)
 *                果乾 600 + 太陽餅 10 盒 (需算太陽餅的運費)
 *         
 * @author	Dio
 */
class checkout_fee_model extends CI_Model {
	
	// --------------------------------------------------------------------
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		
		$this->fee       = 0;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 *  計算運費 / 基本運費
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function calcFeeBase(){}
	
	// --------------------------------------------------------------------
	
	/**
	 *  計算運費 / 追加離島運費
	 *
	 * @access	public
	 * @param
	 * @return 
	 */
	public function calcFeeIsland(){}
	
	// --------------------------------------------------------------------
	
	/**
	 *  計算運費 / 是否滿額免運
	 *
	 * @access	public
	 * @param
	 * @return int
	 */
	public function calcFeefull(){
		
		 if( empty($_SESSION['sys_info']['shipping_fee']) || empty($_SESSION['sys_info']['shipping_fee_full']) ){
		 	return false;
		 }
		 
		 //助糧商品免運
	     if( isCategory_3ForCart() ){
		    return false;	
		 }		
		 
		 $total_cart = $this->cls_cart->count_all_total();
		 	
		 //未滿額需運費 
		 if( $total_cart < $_SESSION['sys_info']['shipping_fee_full'] ){
		 	$this->fee = $_SESSION['sys_info']['shipping_fee'];
		 }	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 *  取得運費
	 *
	 * @access	public
	 * @param
	 * @return boolean
	 */
	public function getFee(){
		
		//計算基本運費
		$this->calcFeeBase();

		//計算離島運費
		$this->calcFeeIsland();
		
		//計算滿額免運
	    $this->calcFeefull();
	    	    
	    //寫入SESSION
	    $_SESSION['my_order']['fee']['item_type'] = '+';
	    $_SESSION['my_order']['fee']['func']      = 'fee';
	    $_SESSION['my_order']['fee']['item_id']   = 0;
	    $_SESSION['my_order']['fee']['name']      = '運費';
	    $_SESSION['my_order']['fee']['total']     = $this->fee;
	}

	
}


/* End of file checkout_fee_model */