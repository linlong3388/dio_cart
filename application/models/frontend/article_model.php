<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 列表模型 Article_model 
 *    針對產品主檔(article)的列表進行資料映射。
 *
 * @modelName Article_model
 * @author	Dio
 */
class Article_model extends CI_Model {
	
	// --------------------------------------------------------------------
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
    public function __construct(){
    	
       $this->a   = 'article'; 
       $this->ac  = 'article_category';
    }    
   
	// --------------------------------------------------------------------

	/**
	 * 方法: 搜尋條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function search($srh_data){
		
		//串接SQL語句
		$sql_where = " WHERE {$this->a}.status = 1 ";
		
		//依[類別] / 商品分類搜尋(次分類)
		/*
		 if( isset($srh_data['srh_category_id_2']) && is_numeric($srh_data['srh_category_id_2']) ){
		    $sql_where .= " AND {$this->a}.article_category_id = {$this->db->escape($srh_data['srh_category_id_2'])}" ;
		 }*/                      
		
		//依商品名稱
		if( !empty($srh_data['title']) ){
			$sql_where .= " AND {$this->a}.title LIKE '%".$srh_data['title']."%'";
		}
		
		//依是否顯示首頁
		if( !empty($srh_data['is_show_home']) ){
			$sql_where .= " AND {$this->a}.is_show_home = 1 ";
		}
		 
	    return $sql_where;
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法: 排序條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function sort($srh_data){

		$sql_sort = " ORDER BY `cdate` DESC";

		//串接SQL語句ORDER BY
		/*
		if( !(empty($srh_data['srh_sort'])) ){

			if($srh_data['srh_sort'] == 'price'){
			   $sql_sort = " ORDER BY  ".$srh_data['srh_sort']." ASC";
			}else{
			   $sql_sort = " ORDER BY  ".$srh_data['srh_sort']." DESC";
			}
		}*/

		return $sql_sort;
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法: 分頁條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function page($srh_data){
			
		$sql_limit = "";
		
		if( isset($srh_data['srh_limit1']) && isset($srh_data['srh_limit2'])){
			$sql_limit = " LIMIT {$srh_data['srh_limit1']} ,{$srh_data['srh_limit2']}";			
		}
		
		return $sql_limit;
	}
		
	// --------------------------------------------------------------------

	/**
	 * 方法: 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists($srh_data){
	
		$sql_where = $this->search($srh_data);
		$sql_limit = $this->page($srh_data);
		$sql_sort =  $this->sort($srh_data);

		$sql = "SELECT {$this->a}.* 
		              ,{$this->ac}.name as category_name
		         FROM {$this->a}
		        LEFT JOIN {$this->ac} ON {$this->a}.article_category_id = {$this->ac}.article_category_id
                  {$sql_where} {$sql_sort} {$sql_limit} ";
                  
		$query = $this->db->query($sql)->result_array();
		
		if( !empty($query) ){
		   	return $query;
		}else{
		   	return array();
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法: 列表 / 總計
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists_group_by($srh_data){

		$sql_where = $this->search($srh_data);

		$sql = "SELECT COUNT({$this->a}.article_id) as count
		          FROM {$this->a}
               {$sql_where}";
               
		$query = $this->db->query($sql)->row_array();
		
		if( !empty($query['count']) ){
		   return $query['count'];
		}else{
		   return 0;
		}
	}

}

/* End of file article_model */