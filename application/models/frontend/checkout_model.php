<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 模型 - 結帳 (for 孟聰版)
 *           將購物車SESSION資料，如[訂單],[運費],[優惠券],[手續費]...等，依序
 *           寫入資料庫。
 * 
 * @modelName Checkout_model
 * @author	Dio
 */
class Checkout_model extends CI_Model {
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 建立訂單資料
	 *
	 * @access	public
	 * @param	$data 為 SESSION 資料，分[客戶資料]和[訂單資料]
	 * @return  order_id
	 */
	public function addOrder($data){
		
		//訂單(主表)
		$data_master = array(
				'order_show_id' => g_order_id() ,
				'customer_id'   => $data['info']['customer_id'] ,
				'branch_id'     => empty($_SESSION['branch_id']) ? 0 : $_SESSION['branch_id'] ,
				'name'          => $data['info']['last_name'] ,
				'mobile'        => $data['info']['mobile'] ,
				'phone'         => $data['info']['phone'] ,
				'email'         => $data['info']['email'] ,
				'pay_method'    => $data['info']['pay_method'] , 
				'pay_method_content' => $_SESSION['sys_info']['pay_method_atm'] ,
				//'trans_method'  => $data['info']['trans_method'] ,  
				//'ticket_type'   => $data['info']['ticket_type'] ,
				//'ticket3_title' => $data['info']['ticket3_title'] ,
				//'ticket3_unified' => $data['info']['ticket3_unified'] ,
				//'zip'           => $data['info']['reci_zip'] ,
				'local'         => $data['info']['local'] ,
				'address'       => $data['info']['address'] ,
				//'shipping_fee'  => $_SESSION['sys_info']['shipping_fee'] , 
				//'date_arrival'  =>  $data['info']['date_arrival'] ,
				//'time_arrival'  => ( $_SESSION['sys_info']['is_time_arrival'] == 1 ) ? $data['info']['time_arrival'] : 99 ,
				'promo_coupon_code_use' => $data['info']['code_use'] ,
				'memo'          =>  strip_tags($data['info']['memo']) ,
				'status'        => 0 ,
				'cdate'         => date('Y-m-d H:i:s') ,
							
				//'ticket_addr_id'  => $data['info']['ticket_addr_id'] ,
				//'ticket_addr'     => $data['info']['ticket_addr'] ,
		);
		
		//超商取貨欄位
		if( !empty($data['cvs']) ){
		   $data_master['cvs_no']          = $data['cvs']['cvs_no'] ;
		   $data_master['cvs_logistics_sub_type']  = $data['cvs']['cvs_logistics_sub_type'] ;
		   $data_master['cvs_store_id']    = $data['cvs']['cvs_store_id'] ;
		   $data_master['cvs_store_name']  = $data['cvs']['cvs_store_name'] ;
		   $data_master['cvs_address']     = $data['cvs']['cvs_address'] ;
		   $data_master['cvs_telephone']   = $data['cvs']['cvs_telephone'] ;
		   $data_master['cvs_extra_data']  = $data['cvs']['cvs_extra_data'] ;
		}
		
		$this->db->insert('order' ,$data_master);
		
		$order_id = $this->db->insert_id();
		
		//訂單(副表)
		foreach ($data['cart'] as $row) {
			
			$data_detail = array(
				'order_id'           => $order_id ,
				'product_id'         => $row['product_id'] ,
				'entity'             => $row['entity'] ,
				'price'              => !empty($row['price_promo']) ? $row['price_promo'] : $row['price'] ,
				'total'              => $row['total'] ,
			    'status'             => 1,
				//'status'             => !IsStockEnough($row['sku2'] ,$row['entity']) ? 1 : 0 ,	
				'cdate'              => $row['cdate']
			);
			
			$this->db->insert('order_detail' , $data_detail);
		}
			
		return $order_id;	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 建立運費資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function addFee($param){
		
		$data = array(
					'order_id'  => $param['order_id'] ,
					'item_type' => $param['fee']['item_type'] ,
					'func'      => $param['fee']['func'] ,
					'item_id'   => $param['fee']['item_id'] ,
					'name'      => $param['fee']['name'] ,
					'total'     => $param['fee']['total'] ,
					'cdate'     => date('Y-m-d H:i:s')
		);
	
		$this->db->insert('order_promo' ,$data);
	}	
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 建立折價券資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function addCoupon($param){
	
		if( empty($param['coupon']) )
			return false;
				
		$data = array(
					'order_id'  => $param['order_id'] ,
					'item_type' => $param['coupon']['item_type'] ,
					'func'      => $param['coupon']['func'] ,
					'item_id'   => $param['coupon']['item_id'] ,
					'name'      => $param['coupon']['name'] ,
					'total'     => $param['coupon']['total'] ,
					'cdate'     => date('Y-m-d H:i:s')
		    	);
	
		$this->db->insert('order_promo' ,$data);
	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 建立折貨到付款/手續費資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function addFeeCod($param){
	
		if( empty($param['fee_cod']) )
			return false;
		
		$data = array(
					'order_id'  => $param['order_id'] ,
					'item_type' => $param['fee_cod']['item_type'] ,
					'func'      => $param['fee_cod']['func'] ,
					'item_id'   => $param['fee_cod']['item_id'] ,
					'name'      => $param['fee_cod']['name'] ,
					'total'     => $param['fee_cod']['total'] ,
					'cdate'     => date('Y-m-d H:i:s')
			);
	
		$this->db->insert('order_promo' ,$data);
	}
	
}


/* End of file vendor_Checkout_model */