<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 模型 - 結帳 (for 菊芬版)
 *           將購物車SESSION資料，如[訂單],[運費],[優惠券],[手續費]...等，依序
 *           寫入資料庫。
 * 
 * @modelName Checkout_model
 * @author	Dio
 */
class Checkout_model extends CI_Model {
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 建立訂單資料
	 *
	 * @access	public
	 * @param	$data 為 SESSION 資料，分[客戶資料]和[訂單資料]
	 * @return  order_id
	 */
	public function addOrder($data){
		
		//訂單(主表)
		$data_master = array(
				'order_show_id' => g_order_id() ,
				'customer_id'   => $data['info']['customer_id'] ,
				'branch_id'     => empty($_SESSION['branch_id']) ? 0 : $_SESSION['branch_id'] ,
				'name'          => $data['info']['name'] ,
				'phone'         => $data['info']['phone'] ,
				'email'         => $data['info']['email'] ,
				'pay_method'    => $data['info']['pay_method'] ,    
				'trans_method'  => $data['info']['trans_method'] ,  
				'ticket_type'   => $data['info']['ticket_type'] ,
				'ticket3_title' => $data['info']['ticket3_title'] ,
				'ticket3_unified' => $data['info']['ticket3_unified'] ,
				'zip'           => $data['info']['reci_zip'] ,
				'local'         => $data['info']['local'] ,
				'address'       => $data['info']['address'] ,
				//'shipping_fee'  => $_SESSION['sys_info']['shipping_fee'] , 
				'date_arrival'  =>  $data['info']['date_arrival'] ,
				'time_arrival'  => ( $_SESSION['sys_info']['is_time_arrival'] == 1 ) ? $data['info']['time_arrival'] : 99 ,
				'memo'          =>  strip_tags($data['info']['memo']) ,
				'status'        => 0 ,
				'cdate'         => date('Y-m-d H:i:s') ,
				
				'o_name'          => $data['info']['o_name'] ,
				'o_phone'         => $data['info']['o_phone'] ,
				'o_email'         => $data['info']['o_email'] ,
				'o_local'         => $data['info']['o_local'] ,
				'o_address'       => $data['info']['o_address'] ,
				
				'ticket_addr_id'  => $data['info']['ticket_addr_id'] ,
				'ticket_addr'     => $data['info']['ticket_addr'] ,
		);
		
		$this->db->insert('order' ,$data_master);
		
		$order_id = $this->db->insert_id();
		
		//訂單(副表)
		foreach ($data['cart'] as $row) {
			
			$product_id = $row['product_id'];
			
			if($row['type_id'] == 1){
				$product_id = explode('|', $row['product_id'])[1];
			}
			
			$data_detail = array(
				'order_id'           => $order_id ,
				'product_id'         => $product_id ,
				'product_box_id'     => $row['product_id'] ,
				'type_id'            => $row['type_id'] ,
				'attr1'              => $row['attr1'] ,
				'attr2'              => $row['attr2'] ,
				'entity'             => $row['entity'] ,
				'price'              => !empty($row['price_promo']) ? $row['price_promo'] : $row['price'] ,
				'total'              => $row['total'] ,
			    'status'             => 1,
				//'status'             => !IsStockEnough($row['sku2'] ,$row['entity']) ? 1 : 0 ,	
				'cdate'              => $row['cdate']
			);
			
			$this->db->insert('order_detail' , $data_detail);
			
		      //盒裝(副表)
			if($row['type_id'] == 1){ 
		      
				foreach ($row['box'] as $row2){
				
			         $data_box = array(
				                'order_id'    => $order_id ,
				              'product_id'    => $row['product_id'] ,
			     	      'product_box_id'    => $row2['product_id'] ,
			             'product_comb_id'    => $row2['product_comb_id'] ,
				                   'price'    => $row2['price'] ,
			     		        'unit_pre'    => $row2['unit_pre'] ,
			     		          'entity'    => $row2['entity'] ,
				               'total_sub'    => $row2['total_sub'] ,
			     		           'image'    => $row2['image'] ,
			     	      	      'status'    => 1,
		 	                       'cdate'    => $row['cdate']
			         );
					
			         $this->db->insert('order_box' , $data_box);
			    }
		        
			} 
		     
		}
			
		return $order_id;	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 建立運費資料 / 依溫度
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function addFee($data){
		
		foreach ($data['fee'] as $row){
	
			$data = array(
					'order_id'  => $data['order_id'] ,
					'item_type' => $row['item_type'] ,
					'func'      => $row['func'] ,
					'item_id'   => $row['item_id'] ,
					'name'      => $row['name'] ,
					'total'     => $row['total'] ,
					'cdate'     => date('Y-m-d H:i:s')
			);
	
			$this->db->insert('order_promo' ,$data);
		}
	
	}	
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 建立折價券資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function addCoupon($param){
	
		if( empty($param['coupon']) )
			return false;
				
		$data = array(
					'order_id'  => $param['order_id'] ,
					'item_type' => $param['coupon']['item_type'] ,
					'func'      => $param['coupon']['func'] ,
					'item_id'   => $param['coupon']['item_id'] ,
					'name'      => $param['coupon']['name'] ,
					'total'     => $param['coupon']['total'] ,
					'cdate'     => date('Y-m-d H:i:s')
		    	);
	
		$this->db->insert('order_promo' ,$data);
	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 建立折貨到付款/手續費資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function addFeeCod($param){
	
		if( empty($param['fee_cod']) )
			return false;
		
		$data = array(
					'order_id'  => $param['order_id'] ,
					'item_type' => $param['fee_cod']['item_type'] ,
					'func'      => $param['fee_cod']['func'] ,
					'item_id'   => $param['fee_cod']['item_id'] ,
					'name'      => $param['fee_cod']['name'] ,
					'total'     => $param['fee_cod']['total'] ,
					'cdate'     => date('Y-m-d H:i:s')
			);
	
		$this->db->insert('order_promo' ,$data);
	}
	
}


/* End of file vendor_Checkout_model */