<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 模型 - 結帳流程 / 折價券
 *         因涉及SESSION['my_order']的參數值，所以僅限購物流程專用
 * 
 * @author	Dio
 */
class checkout_coupon_model extends CI_Model {
	
	// --------------------------------------------------------------------
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){}
	
	// --------------------------------------------------------------------
	
	/**
	 *  判斷購物車商品的分類，是否屬限定分類
	 *
	 * @access	public
	 * @param
	 * @return boolean
	 */
	public function isEnableCategory(){
			
		$enable_category   = explode(',' ,$_SESSION['sys_info']['promo']['coupon']['enable_category']);
		
        foreach ($this->cart->select_all() as $row){
        	
        	//將[商品分類 ]轉為陣列
        	$category_multi_id = explode(',' ,$row['category_multi_id']);
        	
        	foreach ($category_multi_id as $key=>$val){
        		
        		if( in_array($val, $enable_category) )
        		   return true;
        	}	
        	
        }
		
        return false;	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 *  判斷購物車商品，是否屬限定商品
	 *
	 * @access	public
	 * @param
	 * @return boolean
	 */
	public function isEnableProduct(){
		
		$enable_product = explode(',' ,$_SESSION['sys_info']['promo']['coupon']['enable_product']);
		
		foreach ($this->cart->select_all() as $row){
			 
			if( in_array($row['product_id'], $enable_product) )
			   return true;
		}
		
		return false;
	}

	// --------------------------------------------------------------------
	
	/**
	 *  判斷折價券編號是否可用
	 *
	 * @access	public
	 * @param
	 * @return boolean
	 */
	public function isCouponCode($code){
		
	   if( $code !=  $_SESSION['sys_info']['promo']['coupon']['code'])
	 	  return false;

	   return true;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 *  判斷折價券是否已達發放上限
	 *
	 * @access	public
	 * @param
	 * @return boolean
	 */
	public function isCouponUseTotal(){
	
	   if( $_SESSION['sys_info']['promo']['coupon']['use_total'] <= $_SESSION['sys_info']['promo']['coupon']['use_count'])
	   	  return false;
	   
	   return true;
	}

	// --------------------------------------------------------------------
	
	/**
	 *  判斷折價券是否滿額
	 *
	 * @access	public
	 * @param
	 * @return boolean
	 */
	public function isCouponTotal(){
	
		if( $this->cls_cart->count_all_total() <  $_SESSION['sys_info']['promo']['coupon']['total'])
			return false;

		return true;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 *  驗證折價券是否為限定分類&商品
	 *
	 * @access	public
	 * @param
	 * @return boolean
	 */
	public function isCouponUseCategoryAndProduct(){
		
		//不限定分類&商品
		if( empty($_SESSION['sys_info']['promo']['coupon']['enable_category']) 
			&& empty($_SESSION['sys_info']['promo']['coupon']['enable_product']) ) {

			    return true;
		}
			
		//限定分類
		if( !empty($_SESSION['sys_info']['promo']['coupon']['enable_category']) ){

			if( $this->isEnableCategory() )
			    return true;	
		}
		
		//限定商品
		if( !empty($_SESSION['sys_info']['promo']['coupon']['enable_product']) ){
		
			if( $this->isEnableProduct() )
				return true;
		}
		
		return false;
		
	}
	
	// --------------------------------------------------------------------
	
	/**
	 *  判斷會員是否可以使用 折價券
	 *
	 * @access	public
	 * @param
	 * @return boolean
	 */
	public function isCouponUseCountCustomer(){
		
		if( empty($_SESSION['customer_info']['coupon']) )
			return true;
		
		$count = 0;
		
		foreach($_SESSION['customer_info']['coupon'] as $row){
			
			if( $_SESSION['sys_info']['promo']['coupon']['code'] == $row['code']){
				
				$count++;
			}
		}
		
		if( $_SESSION['sys_info']['promo']['coupon']['use_count_customer'] >= $count)
			return true;
		
		return false;
	}
	
	// ------------------------------------------------------------------------
	
	/**
	 * reSetPromoCoupon
	 *
	 * 重設折價券條件設置為 $_SESSION 變數
	 *
	 * @access	public
	 * @return	array
	 */
  	public function reSetPromoCoupon( $code ){
  		
			//$this->db->where('promo_coupon_id' ,1);
			$this->db->where('status' ,1);
			$this->db->where('sdate <= ' ,date('Y-m-d'));
			$this->db->where('edate >= ' ,date('Y-m-d'));
			$this->db->where('code' ,$code);
			$this->db->limit(1);
	
			$query = $this->db->get('promo_coupon');
			
			if( $query->num_rows() > 0 ){
				$_SESSION['sys_info']['promo']['coupon'] = $query->row_array();
				
				//取得折價券使用總數
				$this->db->where('code' ,$_SESSION['sys_info']['promo']['coupon']['code']);
				$_SESSION['sys_info']['promo']['coupon']['use_count'] = $this->db->count_all_results('customer_coupon');
			}	
	}
	
}


/* End of file checkout_coupon_model */