<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 模型 - Order_model 訂單模型
 * 說明 : 針對訂單主檔(order)進行資料映射。
 *
 * @modelName Order_model
 * @author	Dio
 */
class Order_model extends CI_Model {

	// --------------------------------------------------------------------
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		 
		$this->o   = 'order';
		$this->od  = 'order_detail';
		$this->op  = 'order_promo';
		 
		$this->c   = 'customer';
		$this->p   = 'product';
		 
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法: 訂單 /主表(單筆)
	 *         
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_order($param){
		
		$param_key;
		$param_val;
		
		foreach($param as $key=>$val){
			$param_key = $key;
			$param_val = $val;
		}

		$sql = "SELECT * , (SELECT SUM(entity) FROM order_detail od WHERE od.order_id = o.order_id  GROUP BY od.order_id ) as entity
                         , ((SELECT SUM(total) FROM order_detail od WHERE od.order_id = o.order_id  GROUP BY od.order_id ) 
                            + (SELECT SUM(total) FROM `order_promo` WHERE order_id = o.order_id AND item_type = '+') 
                            - (SELECT SUM(total) FROM `order_promo` WHERE order_id = o.order_id AND item_type = '-')) as total
                  FROM `order` o 
                  WHERE o.{$param_key} = ? 
		       ORDER BY o.order_id DESC"; 
			
		$query = $this->db->query($sql ,array($param_val))->row_array();
		
		if( !empty($query) ){
			return $query;
		}else{
			return array();
		}

	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法: 訂單 /主表(多筆)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_order_list($srh_data){
		
		//搜尋條件
		$sql_where = $this->shr_order_condition($srh_data);
		$sql_limit = $this->shr_page_condition($srh_data);
		$sql_sort  = $this->shr_sort_condition($srh_data);
		
		$sql = "SELECT * , (SELECT SUM(entity) FROM {$this->od} WHERE {$this->od}.order_id = {$this->o}.order_id  GROUP BY {$this->od}.order_id ) as entity
		, (IFNULL((SELECT SUM(total) FROM {$this->od} WHERE {$this->od}.order_id = {$this->o}.order_id  GROUP BY {$this->od}.order_id ),0)
		+ IFNULL((SELECT SUM(total) FROM {$this->op} WHERE order_id = {$this->o}.order_id AND item_type = '+'),0)
		- IFNULL((SELECT SUM(total) FROM {$this->op} WHERE order_id = {$this->o}.order_id AND item_type = '-'),0) ) as total
		FROM `{$this->o}` {$sql_where} {$sql_sort} {$sql_limit}";
		
		$query = $this->db->query($sql)->result_array();
		
		if( !empty($query) ){
		   return $query;
		}else{
		   return array();
		}
		
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 訂單 /主表(多筆)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_order_list_total($srh_data){
		
		//搜尋條件
		$sql_where = $this->shr_order_condition($srh_data);
		
		$sql = "SELECT COUNT({$this->o}.order_id) as total FROM `{$this->o}` {$sql_where}";
		
		$query = $this->db->query($sql)->row_array();

		if( !empty($query['total']) ){
		   return $query['total'];
		}else{
		   return 0;
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 搜尋條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_order_condition($srh_data){
	
		//串接SQL語句
		$sql_where = " WHERE `{$this->o}`.order_id <> '' ";
	
		//依訂單編號
		if( !empty($srh_data['srh_order_show_id']) ){
			$sql_where .= " AND {$this->o}.order_show_id LIKE '%".$srh_data['srh_order_show_id']."%'";
		}
	
		//依[訂購日期]
		if( !empty($srh_data['srh_order_cdate1']) && !empty($srh_data['srh_order_cdate2'])  ){
			$sql_where .= "AND SUBSTR(`{$this->o}`.cdate, 1 ,10) >= '".$srh_data['srh_order_cdate1']."'
			AND SUBSTR(`{$this->o}`.cdate, 1 ,10) <= '".$srh_data['srh_order_cdate2']."'" ;
		}
	
		//依[狀態]
		if( isset($srh_data['srh_status']) && is_numeric($srh_data['srh_status'])){
			$sql_where .= " AND `{$this->o}`.status = '".$srh_data['srh_status']."'";
		}
	
		//依[會員編號]
		if( !empty($srh_data['srh_customer_id']) ){
			$sql_where .= " AND `{$this->o}`.customer_id = '".$srh_data['srh_customer_id']."'";
		}
		
		return $sql_where;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 排序條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_sort_condition($srh_data){
	
		$sql_sort = "";
	
		//串接SQL語句ORDER BY
		if( !(empty($srh_data['srh_sort'])) && !(empty($srh_data['srh_sort_type'])) ){
			$sql_sort = " ORDER BY  ".$srh_data['srh_sort']." ".$srh_data['srh_sort_type'];
		}else{
			$sql_sort = " ORDER BY `{$this->o}`.order_id DESC";
		}
	
		return $sql_sort;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 分頁條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_page_condition($srh_data){
	
		$sql_limit = " LIMIT {$srh_data['limit1']},{$srh_data['limit2']}";
	
		return $sql_limit;
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法: 訂單 /明細(多筆)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_order_detail($param){
		
		$param_key;
		$param_val;
		
		foreach($param as $key=>$val){
			$param_key = $key;
			$param_val = $val;
		}
			
		$sql = "SELECT od.* ,p.image ,p.name ,p.category_id ,p.branch ,ps.sku2 
	              FROM `order_detail` od 
	            LEFT JOIN `order` o ON od.order_id = o.order_id  
		        LEFT JOIN `product` p ON od.product_id = p.product_id
		        LEFT JOIN `product_speci` ps ON od.product_speci_id = ps.product_speci_id 
		        WHERE o.{$param_key} = ? ";
		
		$query = $this->db->query($sql ,array($param_val))->result_array();

		if( !empty($query) ){
			return $query;
		}else{
			return array();
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法: 訂單 /主表(單筆)
	 *        訂單JOIN會員 資料表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_order_customer($customer_id,$order_id){
			
		$sql = "SELECT o.* ,CONCAT(c.birthday_year,'/',c.birthday_month,'/',c.birthday_day) as birthday ,c.phone
	                       ,c.cdate as mbr_cdate 
	                       ,(SELECT SUM(entity) FROM order_detail od WHERE od.order_id = o.order_id  GROUP BY od.order_id ) as entity
		                   ,(IFNULL((SELECT SUM(total) FROM order_detail od WHERE od.order_id = o.order_id  GROUP BY od.order_id ),0)
		                   + IFNULL((SELECT SUM(total) FROM `order_promo` WHERE order_id = o.order_id AND item_type = '+'),0)
		                   - IFNULL((SELECT SUM(total) FROM `order_promo` WHERE order_id = o.order_id AND item_type = '-'),0)) as total		         
		        FROM `order` o 
		        LEFT JOIN `customer` c ON o.customer_id = c.customer_id
		        WHERE  o.customer_id = {$customer_id} 
		        AND o.order_id = {$order_id}";
		
		$query = $this->db->query($sql)->row_array();
		
		if( !empty($query) ){
			return $query;
		}else{
			return array();
		}
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法: 訂單資料(單筆)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_order_detail_info($customer_id,$order_detail_id){
			
		$sql = "SELECT *
		         ,(SELECT category_id FROM product WHERE order_detail.product_id = product.product_id) as p_category_id
		         ,(SELECT name FROM product WHERE order_detail.product_id = product.product_id) as p_name
		         ,(SELECT sku FROM product WHERE order_detail.product_id = product.product_id) as p_sku
		       FROM `order`
		       LEFT JOIN order_detail ON `order`.order_id = order_detail.order_id
		       WHERE `order`.customer_id = {$customer_id} 
		       AND order_detail.order_detail_id = {$order_detail_id}";
	
		$query = $this->db->query($sql)->row_array();
			
		if( !empty($query) ){
		   return $query;
	    }else{
	       return array();
	    }
	}

}


/* End of file Order_model */