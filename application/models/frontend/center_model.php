<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 模型 - Center 的功能
 * @modelName Center_model
 * @author	Dio
 */
class Center_model extends CI_Model {

	// --------------------------------------------------------------------

	/**
	 * 方法: 會員資料&地址
	 *
	 * @access	public
	 * @param $_SESSION
	 * @return
	 */
	public function get_info($customer_id){

		$sql = "SELECT c.* ,cdr.first_name ,cdr.last_name ,cdr.mobile ,cdr.email as cdr_email  ,cdr.conutry ,cdr.city ,cdr.zip ,cdr.local ,cdr.address
	              FROM `customer` c 
		        LEFT JOIN `customer_addr` cdr ON c.customer_id = cdr.customer_id
		        WHERE c.customer_id = {$customer_id}
		        AND cdr.status='1' LIMIT 1";

		$query = $this->db->query($sql)->row_array();
			
		if( !empty($query) ){
			return $query;
		}else{
			return array();
		}
	}

}


/* End of file Center_model */