<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 模型 - Category_promo 
 * @modelName Category_promo_model
 * @author	Dio
 */
class Category_promo_model extends CI_Model {

	// --------------------------------------------------------------------
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
    public function __construct(){
    	
    	$this->cp   = 'category_promo';
    	$this->cpp  = 'category_promo_product';
    	$this->p    = 'product';
    }  	
	
	// --------------------------------------------------------------------

	/**
	 * 方法: 取得所屬商品 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getProductList(){

		$sql = "SELECT $this->cp.* 
		             ,(SELECT COUNT(product_id) FROM $this->cpp 
		                 WHERE $this->cp.category_promo_id = $this->cpp.category_promo_id) as count  
		        FROM $this->cp";
                
        $query = $this->db->query($sql)->result_array();

        return $query;
	}


}

/* End of file Category_promo_model */