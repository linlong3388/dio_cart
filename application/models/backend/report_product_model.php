<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 模型 - 統計報表 / 商品
 * @modelName report_product_model
 * @author	Dio
 */
class report_product_model extends CI_Model {

	// --------------------------------------------------------------------
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
    public function __construct(){
       
       $this->o   = 'order';
       $this->od  = 'order_detail';
    	
       $this->p   = 'product';
    }  	
	
	// --------------------------------------------------------------------

	/**
	 * 方法: 搜尋條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_condition($srh_data){

		//串接SQL語句
		$sql_where = " WHERE {$this->p}.product_id > 0 ";

		//依訂購日期
		if( !empty($srh_data['srh_cdate1']) && !empty($srh_data['srh_cdate2'])  ){
			$sql_where .= "AND SUBSTR({$this->od}.cdate, 1 ,10) >= '".$srh_data['srh_cdate1']."'
    	                   AND SUBSTR({$this->od}.cdate, 1 ,10) <= '".$srh_data['srh_cdate2']."'" ;
		}		

		//依[商品編號]
		/*
		if( !(empty($srh_data['srh_product_id']) && $srh_data['srh_product_id'] !== '0') ){
			$sql_where .= " AND {$this->p}.product_id = '".LTRIM($srh_data['srh_product_id'],'0')."' ";
		}*/

		//依[sku]
		if( !(empty($srh_data['srh_sku']) ) ){
			$sql_where .= " AND {$this->p}.sku LIKE '%".$srh_data['srh_sku']."%' ";
		}		
		
		//依[商品名稱]
		if( !(empty($srh_data['srh_name']) ) ){
			$sql_where .= " AND {$this->p}.name LIKE '%".$srh_data['srh_name']."%' ";
		}

		return $sql_where;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法: 排序條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_sort_condition($srh_data){

		$sql_sort = "";
		
		//串接SQL語句ORDER BY
		if( !(empty($srh_data['srh_sort'])) && !(empty($srh_data['srh_sort_type'])) ){
			$sql_sort = " ORDER BY  ".$srh_data['srh_sort']." ".$srh_data['srh_sort_type'];
		}else{
			//$sql_sort = " ORDER BY `{$this->o}`.order_id DESC";
		}
		
		return $sql_sort;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法: 群組計算
	 *
	 * 回傳值: [搜尋參數][資料]
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_group_by($srh_data){

		$sql_where = $this->shr_condition($srh_data);

		$sql = "SELECT COUNT({$this->p}.sku) as count
                  FROM {$this->p} 
                LEFT JOIN {$this->od} ON {$this->p}.product_id = {$this->od}.product_id  
		         {$sql_where}
                 ORDER BY SUBSTR({$this->p}.cdate, 1 ,10) DESC ";           

        $query = $this->db->query($sql)->result_array();
        
        if( !empty($query) ){
          	return $query;
        }else{
           	return array();
        }

	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 分頁條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_page_condition($srh_data){
	
		$sql_limit = " LIMIT 0,".$srh_data['srh_page_per'];
	
		//串接SQL語句Limit
		if( !(empty($srh_data['limit_page'])) ){
			$sql_limit = " LIMIT ".(($srh_data['limit_page']-1)*$srh_data['srh_page_per'])." ,".$srh_data['srh_page_per'];
		}
	
		return $sql_limit;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 統計
	 *
	 * 回傳值: [搜尋參數][資料]
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_product($srh_data){
	
		$sql_where = $this->shr_condition($srh_data);
		$sql_limit = $this->shr_page_condition($srh_data);
		$sql_sort  = $this->shr_sort_condition($srh_data);
	
		$sql = "SELECT {$this->p}.product_id 
		              ,{$this->p}.sku 
		              ,IFNULL(NULLIF(SUM({$this->od}.entity), '' ), 0)  as entity 
		              ,IFNULL(NULLIF(SUM({$this->od}.total), '' ), 0)  as product_total
		              ,{$this->p}.name
		        FROM {$this->p}
		       
		       LEFT JOIN {$this->od} ON {$this->p}.product_id = {$this->od}.product_id
               {$sql_where}
		       GROUP BY {$this->p}.sku
		        {$sql_sort}{$sql_limit}";
		 
		$query = $this->db->query($sql)->result_array();
		
		if( !empty($query) ){
		   return $query;
	    }else{
	       return array();
	    }
	
	}

}

/* End of file report_product_model */