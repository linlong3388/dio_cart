<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 模型 - 處理[活動]的模型
 * @modelName Action_model
 * @author	Dio
 */
class Action_model extends CI_Model {

	// --------------------------------------------------------------------

	/**
	 * 方法: 活動產品
	 * 說明: 該活動未加入的商品
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_action_product_list($action_id){

		$sql = "SELECT * FROM product
		          WHERE product_id NOT IN (SELECT product_id FROM ct_action_to_product WHERE action_id = {$action_id})";

		$query = $this->db->query($sql)->result_array();
			
		if( !empty($query) ){
			return $query;
		}else{
			return array();
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法: 活動產品
	 * 說明: 該活動已加入的商品
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_action_product_list_has($action_id){

		$sql = "SELECT * FROM `ct_action_to_product` atp
	    	      LEFT JOIN `product` p ON atp.product_id = p.product_id
		        WHERE atp.action_id = {$action_id}";

		$query = $this->db->query($sql)->result_array();
			
		if( !empty($query) ){
			return $query;
		}else{
			return array();
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法: 活動列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_action_list(){

		$sql = "SELECT a.action_id ,a.name ,COUNT(atp.action_id) as count ,a.status ,a.cdate
		          FROM `ct_action` a 
	    	      LEFT JOIN `ct_action_to_product` atp ON a.action_id = atp.action_id
		        GROUP BY a.action_id";

		$query = $this->db->query($sql)->result_array();
			
		if( !empty($query) ){
			return $query;
		}else{
			return array();
		}
	}

}


/* End of file Action_model */