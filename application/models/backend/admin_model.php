<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 模型 - 處理[管理員]的模型
 * @modelName Admin_model
 * @author	Dio
 */
class Admin_model extends CI_Model {

	// --------------------------------------------------------------------

	/**
	 * 方法: 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getAdminLists(){

		$sql = "SELECT a.* 
				      ,ag.title
		          FROM `admin` a 
	    	      LEFT JOIN `admin_group` ag ON ag.admin_group_id = a.admin_group_id
				WHERE a.role = 1
		        GROUP BY a.cdate DESC";

		$query = $this->db->query($sql)->result_array();
			
		return $query;
	}

}


/* End of file Admin_model */