<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 處理[產品]的模型
 *
 * @modelName Product_model
 * @author	Dio
 */
class Product_model extends CI_Model {
	
	// --------------------------------------------------------------------
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
    public function __construct(){

    	$this->c    = 'category';
    	$this->p    = 'product';
    	$this->ps   = 'product_speci';
    	$this->pm   = 'product_memo';
    	$this->pr   = 'property';
    	$this->pa   = 'product_attr';
    }  

	// --------------------------------------------------------------------

	/**
	 * 方法: 商品搜尋 / 搜尋條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_product_condition($srh_data){

		//串接SQL語句
		$sql_where = " WHERE {$this->p}.status <> 9 && {$this->p}.type_id=0 ";

		//依商品編號
		if( !empty($srh_data['srh_product_id']) ){
			$sql_where .= " AND {$this->p}.product_id = '".$srh_data['srh_product_id']."'";
		}

		//依sku
		if( !empty($srh_data['srh_sku']) ){
			$sql_where .= " AND {$this->p}.sku LIKE '%".$srh_data['srh_sku']."%'";
		}
		
		//依商品名稱
		if( !empty($srh_data['srh_product_name']) ){
			$sql_where .= " AND {$this->p}.name LIKE '%".$srh_data['srh_product_name']."%'";
		}

		//依分類
		/*
		if( isset($srh_data['srh_category_id']) && is_numeric($srh_data['srh_category_id']) ){
			$sql_where .= " AND {$this->p}.category_id = '".$srh_data['srh_category_id']."'";
		}*/
		if( isset($srh_data['srh_category_id']) && is_numeric($srh_data['srh_category_id']) ){
			$sql_where .= " AND {$this->p}.category_multi_id REGEXP '[[:<:]]".$srh_data['srh_category_id']."[[:>:]]'";
		}                                                            
		

		//依上架日期
		if( !empty($srh_data['srh_cdate1']) && !empty($srh_data['srh_cdate2'])  ){
			$sql_where .= "AND SUBSTR({$this->p}.cdate, 1 ,10) >= '".$srh_data['srh_cdate1']."'
    	                     AND SUBSTR({$this->p}.cdate, 1 ,10) <= '".$srh_data['srh_cdate2']."'" ;
		}

		//依商品狀態
		if( isset($srh_data['srh_status']) && is_numeric($srh_data['srh_status']) ){
		    $sql_where .= " AND {$this->p}.status IN (".$srh_data['srh_status'].")";
		}
			
		//依庫存查詢
		/*
		if( isset($srh_data['srh_stock']) && is_numeric($srh_data['srh_stock']) ){
		    $sql_where .= " AND {$this->ps}.stock <= '".$srh_data['srh_stock']."'";
		} */      
		
		//依廣告分類
		if( !empty($srh_data['srh_ad_id']) ){
			$sql_where .= " AND {$this->p}.ad_id = '".$srh_data['srh_ad_id']."'";
		}

		//依群組分類
		if( isset($srh_data['srh_group_id']) && is_numeric($srh_data['srh_group_id']) ){
			$sql_where .= " AND {$this->p}.group_id REGEXP ',?[".$srh_data['srh_group_id']."],?'";
		}
		
		//依品牌分類
		if( isset($srh_data['srh_brand_id']) && is_numeric($srh_data['srh_brand_id']) ){
			$sql_where .= " AND {$this->p}.brand_id = '".$srh_data['srh_brand_id']."'";
		}
		
		return $sql_where;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 群組條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_group_condition($srh_data){
	
		$sql_group = "";
	
		//依庫存查詢
		if( isset($srh_data['srh_stock']) && is_numeric($srh_data['srh_stock']) ){
		    $sql_group .= " HAVING SUM({$this->ps}.stock) <= '".$srh_data['srh_stock']."'";
		} 
	
		return $sql_group;
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法: 分頁條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_page_condition($srh_data){

		$sql_limit = " LIMIT 0,".$srh_data['srh_page_per'];

		//串接SQL語句Limit
		if( !(empty($srh_data['srh_limit_page'])) ){
			$sql_limit = " LIMIT ".(($srh_data['srh_limit_page']-1)*$srh_data['srh_page_per'])." ,".$srh_data['srh_page_per'];
		}

		return $sql_limit;
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法: 排序條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_sort_condition($srh_data){

		$sql_sort = "";

		//串接SQL語句ORDER BY
		if( !(empty($srh_data['srh_sort'])) && !(empty($srh_data['srh_sort_type'])) ){
			$sql_sort = " ORDER BY  ".$srh_data['srh_sort']." ".$srh_data['srh_sort_type'];
		}else{
			//$sql_sort = " ORDER BY {$this->p}.product_id DESC";
			$sql_sort = " ORDER BY {$this->p}.cdate DESC";
		}
		
		return $sql_sort;
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法: 商品搜尋
	 *
	 * 回傳值: [搜尋參數][資料]
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_product($srh_data){
		
		$sql_where = $this->shr_product_condition($srh_data);
		$sql_limit = $this->shr_page_condition($srh_data);
		$sql_group = $this->shr_group_condition($srh_data);
		$sql_sort  = $this->shr_sort_condition($srh_data);
		
		/* 庫存量為子項加總*/
			$sql = "SELECT {$this->p}.* 
			          ,SUM({$this->ps}.stock) as stock
			          ,{$this->c}.path
		             FROM {$this->p}
		            LEFT JOIN {$this->ps} ON {$this->ps}.sku = {$this->p}.sku
		            LEFT JOIN {$this->c} ON {$this->c}.category_id = {$this->p}.category_id
               {$sql_where} 
                GROUP BY {$this->ps}.product_id
               {$sql_group}
               {$sql_sort} {$sql_limit}";       
	  
        $query = $this->db->query($sql)->result_array();
        
        if( !empty($query) ){
           	return $query;
        }else{
           	return array();
        }
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法: 商品搜尋 / 群組計算
	 *
	 * 回傳值: [搜尋參數][資料]
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_product_group_by($srh_data){

		$sql_where = $this->shr_product_condition($srh_data);

		$sql = "SELECT COUNT(DISTINCT {$this->p}.product_id) as count
		          FROM {$this->p}
		        LEFT JOIN {$this->ps} ON {$this->p}.product_id = {$this->ps}.product_id 
		        {$sql_where}";

		        $query = $this->db->query($sql)->result_array();

		        if( !empty($query) ){
		        	return $query;
		        }else{
		        	return array();
		        }

	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法: 產品子項 / 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_product_speci_list($product_id){

		$sql = "SELECT {$this->ps}.* 
		         ,prs.name as prs_name 
		         ,prc.name as prc_name 
		         ,(SELECT COUNT(product_speci_image_id) FROM product_speci_image WHERE product_speci_id = {$this->ps}.product_speci_id ) as img_count
		        FROM {$this->ps}
		         LEFT JOIN {$this->pr} prs ON {$this->ps}.property_id_size  = prs.property_id
		         LEFT JOIN {$this->pr} prc ON {$this->ps}.property_id_color = prc.property_id
		        WHERE {$this->ps}.product_id = {$product_id}";
		
		$query = $this->db->query($sql)->result_array();
			
		if( !empty($query) ){
			return $query;
		}else{
			return array();
		}
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法: 產品子項 / 單筆
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_product_speci_info($product_speci_id){
	
		$sql = "SELECT {$this->ps}.*
		FROM {$this->ps}
		WHERE {$this->ps}.product_speci_id = {$product_speci_id}";
			
		$query = $this->db->query($sql)->row_array();
			
		if( !empty($query) ){
			return $query;
		}else{
			return array();
		}
	}
		
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 產品屬性 / 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_product_attr_list($product_id){
	
		$sql = "SELECT {$this->pa}.* ,pr.property_category_id as pr_property_category_id ,pr.name as pr_name
		        FROM {$this->pa}
		          LEFT JOIN {$this->pr} pr ON {$this->pa}.property_id  = pr.property_id
		        WHERE {$this->pa}.product_id = {$product_id}";
		
		$query = $this->db->query($sql)->result_array();
			
		if( !empty($query) ){
		  return $query;
	    }else{
	      return array();
	    }
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 產品屬性 / 單筆
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_product_attr_info($product_attr_id){

		$sql = "SELECT {$this->pa}.* ,pr.property_category_id as pr_property_category_id ,pr.name as pr_name
		        FROM {$this->pa}
		           LEFT JOIN {$this->pr} pr ON {$this->pa}.property_id  = pr.property_id
		        WHERE {$this->pa}.product_attr_id = {$product_attr_id}";
			
		$query = $this->db->query($sql)->row_array();
			
		if( !empty($query) ){
			return $query;
		}else{
			return array();
		}
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法: 產品描述
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_product_memo_view($product_id){

		$sql = "SELECT {$this->p}.* 
		               ,(SELECT {$this->ps}.stock FROM {$this->ps} WHERE {$this->ps}.product_id = {$this->p}.product_id) as ps_stock
		               ,{$this->pm}.description 
		               ,{$this->pm}.specification 
		               ,{$this->pm}.notice
		               ,{$this->pm}.techenique
		          FROM {$this->p} 
		        LEFT JOIN {$this->pm} ON {$this->p}.product_id = {$this->pm}.product_id
		        WHERE {$this->p}.product_id = {$product_id}";

		$query = $this->db->query($sql)->row_array();
			
		if( !empty($query) ){
			return $query;
		}else{
			return array();
		}
	}
	
}


/* End of file Product_model */