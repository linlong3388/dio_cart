<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 模型 - 統計報表 / 商品
 * @modelName report_customer_model
 * @author	Dio
 */
class report_customer_model extends CI_Model {

	// --------------------------------------------------------------------
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
    public function __construct(){
       
       $this->o   = 'order';
       $this->od  = 'order_detail';
    	
       $this->p   = 'product';
       $this->ps  = 'product_speci';
       
       $this->c   = 'customer';
    }  	
	
	// --------------------------------------------------------------------

	/**
	 * 方法: 搜尋條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_condition($srh_data){

		//串接SQL語句
		$sql_where = " WHERE {$this->c}.customer_id <> '' ";
			
		//依[商品編號]
		/*
		if( !(empty($srh_data['srh_product_id']) && $srh_data['srh_product_id'] !== '0') ){
			$sql_where .= " AND {$this->p}.product_id = '".LTRIM($srh_data['srh_product_id'],'0')."' ";
		}*/

		//依[帳號]
		if( !(empty($srh_data['srh_username']) ) ){
			$sql_where .= " AND {$this->c}.username LIKE '%".$srh_data['srh_username']."%' ";
		}		
		
		//依[商品名稱]
		if( !(empty($srh_data['srh_name']) ) ){
			$sql_where .= " AND CONCAT({$this->c}.first_name ,{$this->c}.last_name) LIKE '%".$srh_data['srh_name']."%' ";
		}                  

		return $sql_where;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法: 排序條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_sort_condition($srh_data){

		$sql_sort = "";
		
		//串接SQL語句ORDER BY
		if( !(empty($srh_data['srh_sort'])) && !(empty($srh_data['srh_sort_type'])) ){
			$sql_sort = " ORDER BY  ".$srh_data['srh_sort']." ".$srh_data['srh_sort_type'];
		}else{
			//$sql_sort = " ORDER BY `{$this->o}`.order_id DESC";
		}
		
		return $sql_sort;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法: 群組計算
	 *
	 * 回傳值: [搜尋參數][資料]
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_group_by($srh_data){

		$sql_where = $this->shr_condition($srh_data);

		$sql = "SELECT COUNT({$this->ps}.sku) as count
                  FROM {$this->ps} 
                LEFT JOIN {$this->od} ON {$this->ps}.product_speci_id = {$this->od}.product_speci_id  
		        LEFT JOIN {$this->p} ON {$this->p}.product_id = {$this->ps}.product_id
		         {$sql_where}
                 ORDER BY SUBSTR({$this->ps}.cdate, 1 ,10) DESC ";           

        $query = $this->db->query($sql)->result_array();
        
        if( !empty($query) ){
          	return $query;
        }else{
           	return array();
        }

	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 分頁條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_page_condition($srh_data){
	
		$sql_limit = " LIMIT 0,".$srh_data['srh_page_per'];
	
		//串接SQL語句Limit
		if( !(empty($srh_data['limit_page'])) ){
			$sql_limit = " LIMIT ".(($srh_data['limit_page']-1)*$srh_data['srh_page_per'])." ,".$srh_data['srh_page_per'];
		}
	
		return $sql_limit;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 統計
	 *
	 * 回傳值: [搜尋參數][資料]
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_customer($srh_data){
	
		$sql_where = $this->shr_condition($srh_data);
		//$sql_limit = $this->shr_page_condition($srh_data);
		//$sql_sort  = $this->shr_sort_condition($srh_data);
	
		$sql = "SELECT {$this->c}.customer_id
		              ,{$this->c}.username 
		              ,{$this->c}.last_name as name  
		              ,SUM({$this->od}.entity) as entity 
		              ,SUM({$this->od}.total) as product_total
		        FROM {$this->c}
		       LEFT JOIN {$this->od} ON {$this->c}.customer_id = ( SELECT MAX({$this->o}.customer_id) 
		                                                             FROM `{$this->o}` 
		                                                           WHERE {$this->o}.order_id = {$this->od}.order_id)
               {$sql_where}
		       GROUP BY {$this->c}.customer_id";                                          
		     
		$query = $this->db->query($sql)->result_array();
		
		if( !empty($query) ){
		   return $query;
	    }else{
	       return array();
	    }
	
	}

}

/* End of file report_customer_model */