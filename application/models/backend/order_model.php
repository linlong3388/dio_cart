<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 處理[訂單]的模型
 *
 * @modelName Order_model
 * @author	Dio
 */
class Order_model extends CI_Model {
	
	// --------------------------------------------------------------------
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
    public function __construct(){

       $this->b   = 'branch';
    	
       $this->c   = 'customer';
    	
       $this->o   = 'order'; 
       $this->od  = 'order_detail'; 
       $this->odi = 'order_promo';
       
       $this->p   = 'product';
       
    }   
  
	// --------------------------------------------------------------------

	/**
	 * 方法: 搜尋條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_order_condition($srh_data){

		//串接SQL語句
		$sql_where = " WHERE `{$this->o}`.order_id <> '' ";

		//依訂單編號
		if( !empty($srh_data['srh_order_show_id']) ){
			$sql_where .= " AND {$this->o}.order_show_id LIKE '%".$srh_data['srh_order_show_id']."%'";
		}

		//依會員ID
		if( !empty($srh_data['srh_customer_id']) ){
			$sql_where .= " AND {$this->o}.customer_id LIKE '%".$srh_data['srh_customer_id']."%'";
		}
		
		//依訂購人
		if( !empty($srh_data['srh_o_name']) ){
			$sql_where .= " AND {$this->o}.o_name LIKE '%".$srh_data['srh_o_name']."%'";
		}
		
		//依分店查詢
		if( !empty($srh_data['srh_branch_id']) && is_numeric($srh_data['srh_branch_id']) ){
			$sql_where .= " AND {$this->o}.branch_id = '".$srh_data['srh_branch_id']."'";
		}
		
		//依[訂購日期]
		if( !empty($srh_data['srh_order_cdate1']) && !empty($srh_data['srh_order_cdate2'])  ){
			$sql_where .= "AND SUBSTR(`{$this->o}`.cdate, 1 ,10) >= '".$srh_data['srh_order_cdate1']."'
    	                  AND SUBSTR(`{$this->o}`.cdate, 1 ,10) <= '".$srh_data['srh_order_cdate2']."'" ;
		}

		//依[狀態]
		if( isset($srh_data['srh_status']) && is_numeric($srh_data['srh_status'])){
			$sql_where .= " AND `{$this->o}`.status = '".$srh_data['srh_status']."'";
		}

		return $sql_where;
	}
    
	
	// --------------------------------------------------------------------

	/**
	 * 方法: 排序條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_sort_condition($srh_data){

		$sql_sort = "";

		//串接SQL語句ORDER BY
		if( !(empty($srh_data['srh_sort'])) && !(empty($srh_data['srh_sort_type'])) ){
			$sql_sort = " ORDER BY  ".$srh_data['srh_sort']." ".$srh_data['srh_sort_type'];
		}else{
			$sql_sort = " ORDER BY `{$this->o}`.order_id DESC";
		}		

		return $sql_sort;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法: 分頁條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_page_condition($srh_data){

		$sql_limit = " LIMIT 0,".$srh_data['srh_page_per'];

		//串接SQL語句Limit
		if( !(empty($srh_data['srh_limit_page'])) && !(empty($srh_data['srh_page_per'])) ){
			$sql_limit = " LIMIT ".(($srh_data['srh_limit_page']-1)*$srh_data['srh_page_per'])." ,".$srh_data['srh_page_per'];
		}

		return $sql_limit;
	}
	
    
   	/*
	 * ----------------------------------------------------------------------------------
	 *
	 *  底下為各式資料表的SQL語句
	 *
	 * ----------------------------------------------------------------------------------
	 *
	 *
	 */
	
	// --------------------------------------------------------------------

	/**
	 * 方法: 訂單列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_order($srh_data){

		//搜尋條件
		$sql_where = $this->shr_order_condition($srh_data);
		$sql_limit = $this->shr_page_condition($srh_data);
		$sql_sort = $this->shr_sort_condition($srh_data);

		$sql = "SELECT `{$this->o}`.* 
		              ,{$this->c}.last_name as o_name
		              ,{$this->b}.title as b_title
		              ,(SELECT SUM(entity) FROM {$this->od} WHERE {$this->od}.order_id = `{$this->o}`.order_id  GROUP BY {$this->od}.order_id ) as entity
		              , IFNULL((SELECT SUM(total) FROM {$this->od} WHERE {$this->od}.order_id = `{$this->o}`.order_id  GROUP BY {$this->od}.order_id ),0) as ototal
		              ,(IFNULL((SELECT SUM(total) FROM {$this->od} WHERE {$this->od}.order_id = `{$this->o}`.order_id  GROUP BY {$this->od}.order_id ),0)
		                 + IFNULL((SELECT SUM(total) FROM {$this->odi} WHERE order_id = `{$this->o}`.order_id AND item_type = '+'),0)
		                 - IFNULL((SELECT SUM(total) FROM {$this->odi} WHERE order_id = `{$this->o}`.order_id AND item_type = '-'),0) ) as total             
		              ,(SELECT COUNT(order_detail_id) FROM {$this->od} WHERE {$this->od}.order_id = `{$this->o}`.order_id AND {$this->od}.status = 1 ) as order_type
		         FROM `{$this->o}` 
		         LEFT JOIN `{$this->b}` ON {$this->b}.branch_id = {$this->o}.branch_id 
		         LEFT JOIN `{$this->c}` ON {$this->c}.customer_id = {$this->o}.customer_id 
		{$sql_where} {$sql_sort} {$sql_limit}";
		
		$query = $this->db->query($sql)->result_array();
	
		if( !empty($query) ){
		  	return $query;
		}else{
		   	return array();
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法: 訂單資料(單筆)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_order_info($order_id){
		
		$sql = "SELECT `{$this->o}`.*
		                 ,{$this->c}.username as username
		                 ,{$this->c}.last_name as o_name
		                 ,{$this->c}.email as o_email
		                 ,{$this->c}.phone as o_phone
		                 ,{$this->c}.mobile as o_mobile
		                 ,{$this->b}.title as b_title
		                 ,(SELECT SUM(entity) FROM {$this->od} WHERE {$this->od}.order_id = `{$this->o}`.order_id  GROUP BY {$this->od}.order_id ) as entity
		                 ,(IFNULL((SELECT SUM(total) FROM {$this->od} WHERE {$this->od}.order_id = `{$this->o}`.order_id  GROUP BY {$this->od}.order_id ),0)
		                 + IFNULL((SELECT SUM(total) FROM {$this->odi} WHERE order_id = `{$this->o}`.order_id AND item_type = '+'),0)
		                 - IFNULL((SELECT SUM(total) FROM {$this->odi} WHERE order_id = `{$this->o}`.order_id AND item_type = '-'),0)) as total             
		          FROM `{$this->o}`
		        LEFT JOIN {$this->c} ON `{$this->o}`.customer_id = {$this->c}.customer_id
		        LEFT JOIN {$this->b} ON `{$this->o}`.branch_id   = {$this->b}.branch_id
		        WHERE `{$this->o}`.order_id = ?";
			
		$query = $this->db->query($sql ,array($order_id))->row_array();
		
		if( !empty($query) ){
		    return $query;
		}else{
		    return array();
		}
	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 訂單明細(多筆)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_order_detail($order_id){
			
		$sql = "SELECT od.* 
				       ,p.image ,p.name ,p.category_multi_id ,ps.sku2
		          FROM `order_detail` od
		        LEFT JOIN `order` o ON od.order_id = o.order_id
		        LEFT JOIN `product` p ON od.product_id = p.product_id
		        LEFT JOIN `product_speci` ps ON od.product_speci_id = ps.product_speci_id
		          WHERE o.order_id = '".$order_id."'";
		
		$query = $this->db->query($sql)->result_array();
				
		if( !empty($query) ){
    		return $query;
		}else{
	    	return array();
		}
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法: 訂單資料(單筆)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_order_detail_info($order_detail_id){
			
		$sql = "SELECT * 
		           ,(SELECT name FROM {$this->p} WHERE {$this->od}.product_id = {$this->p}.product_id) as p_name
		           ,(SELECT sku FROM {$this->p} WHERE {$this->od}.product_id = {$this->p}.product_id) as p_sku
		           FROM `{$this->o}`
		         LEFT JOIN {$this->od} ON `{$this->o}`.order_id = {$this->od}.order_id
		        WHERE {$this->od}.order_detail_id = {$order_detail_id}";
		
		$query = $this->db->query($sql)->row_array();
			
		if( !empty($query) ){
		  return $query;
	    }else{
		  return array();
	    }
	}

	// --------------------------------------------------------------------

	/**
	 * 方法: 訂單搜尋 / 群組計算
	 *
	 * 回傳值: [搜尋參數][資料]
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_order_group_by($srh_data){

		$sql_where = $this->shr_order_condition($srh_data);

		$sql = "SELECT COUNT(order_id) as count FROM `{$this->o}`
		{$sql_where}";
			
		$query = $this->db->query($sql)->result_array();

		if( !empty($query) ){
			return $query;
		}else{
			return array();
		}

	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法: 匯出訂單 / CSV格式
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function get_csv($srh_data){

		$sql_where = " WHERE {$this->od}.order_detail_id <> '' ";

		//依訂購日期
		if( !empty($srh_data['srh_cdate1']) && !empty($srh_data['srh_cdate2'])  ){
			$sql_where .= "AND SUBSTR({$this->od}.cdate, 1 ,10) >= '".$srh_data['srh_cdate1']."'
    	                  AND SUBSTR({$this->od}.cdate, 1 ,10) <= '".$srh_data['srh_cdate2']."'" ;
		}

		//依狀態
		if( !(empty($srh_data['srh_status']) && $srh_data['srh_status'] !== '0') ){
			$sql_where .= " AND {$this->od}.status = '".$srh_data['srh_status']."'";
		}
			
		$sql = "SELECT {$this->od}.cdate ,{$this->od}.order_detail_id ,{$this->od}.order_detail_show_id ,`{$this->o}`.reci_name ,{$this->od}.product_id ,{$this->od}.product_name ,{$this->od}.Auth_OffsetAmt ,{$this->od}.product_total ,{$this->od}.entity ,{$this->od}.shipping_id
		               ,`{$this->o}`.reci_mobile ,`{$this->o}`.reci_phone ,`{$this->o}`.reci_local ,`{$this->o}`.reci_address ,`{$this->o}`.ticket_type ,`{$this->o}`.ticket2_buyer ,`{$this->o}`.ticket3_unified ,`{$this->o}`.ticket3_title ,`{$this->o}`.reci_email,{$this->od}.Auth_AuthCode
		           FROM `order` o 
    	           LEFT JOIN {$this->od}
    	           ON `{$this->o}`.order_id = {$this->od}.order_id
    	           {$sql_where}";

    	$query = $this->db->query($sql)->result_array();
    	            
    	if( !empty($query) ){
    	  	return $query;
    	}else{
    	   	return array();
    	}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 匯出訂單 / excel格式
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function output_xls($srh_data){
	
		//搜尋條件
		$sql_where = $this->shr_order_condition($srh_data);
		//$sql_limit = $this->shr_page_condition($srh_data);
		$sql_sort = $this->shr_sort_condition($srh_data);
	
		$sql = "SELECT `{$this->o}`.*
		    	,{$this->c}.last_name as c_name
		        ,{$this->c}.username as c_username
		        ,{$this->c}.level as c_level
		        ,{$this->c}.customer_id as c_customer_id
	         	,((SELECT SUM(total) FROM {$this->od} WHERE {$this->od}.order_id = `{$this->o}`.order_id  GROUP BY {$this->od}.order_id )
		         + IFNULL((SELECT SUM(total) FROM {$this->odi} WHERE order_id = `{$this->o}`.order_id AND item_type = '+'),0)
		         - IFNULL((SELECT SUM(total) FROM {$this->odi} WHERE order_id = `{$this->o}`.order_id AND item_type = '-'),0) ) as total
		 
		        FROM `{$this->o}`
		          LEFT JOIN {$this->c} ON `{$this->o}`.customer_id = {$this->c}.customer_id
		        {$sql_where} {$sql_sort} ";
			
		$query = $this->db->query($sql)->result_array();

		if( !empty($query) ){
		      return $query;
	    }
	    
   	    return array();
	}

}


/* End of file Order_model */