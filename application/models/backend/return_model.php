<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 說明 : 處理[退貨單]資料。
 *
 * @modelName Return_model
 * @author	Dio
 */
class Return_model extends CI_Model {
	
	// --------------------------------------------------------------------
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
    public function __construct(){
    	
       $this->r   = 'return';	
       $this->od  = 'order_detail';
       $this->p   = 'product';
    }   

	// --------------------------------------------------------------------

	/**
	 * 方法: 商品退貨 /列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_return_list(){
			
		$sql = "SELECT `{$this->r}`.* ,{$this->p}.name as p_name ,{$this->od}.order_id as order_id
	              FROM `{$this->r}`
		        LEFT JOIN {$this->od} ON `{$this->r}`.order_detail_id = {$this->od}.order_detail_id
		        LEFT JOIN {$this->p} ON `{$this->od}`.product_id = {$this->p}.product_id";

		$query = $this->db->query($sql)->result_array();

		if( !empty($query) ){
			return $query;
		}else{
			return array();
		}
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法: 商品退貨 /明細
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_return_detail($return_id){
			
		$sql = "SELECT `{$this->r}`.* 
		              ,{$this->od}.product_id 
		              ,{$this->od}.order_id 
		              ,{$this->p}.sku 
		              ,{$this->p}.name as p_name
		          FROM `{$this->r}` 
		        LEFT JOIN {$this->od} ON `{$this->r}`.order_detail_id = {$this->od}.order_detail_id
		        LEFT JOIN {$this->p} ON `{$this->od}`.product_id = {$this->p}.product_id
		        WHERE `{$this->r}`.return_id = {$return_id}";
		
		$query = $this->db->query($sql)->row_array();
		
		if( !empty($query) ){
			return $query;
		}else{
			return array();
		}
	}

}

/* End of file Return_model */