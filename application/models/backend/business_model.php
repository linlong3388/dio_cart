<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 模型 - 處理[管理員]的模型
 * @modelName Business_model
 * @author	Dio
 */
class Business_model extends CI_Model {

	// --------------------------------------------------------------------

	/**
	 * 方法: 取得管理員群組權限
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getAdminGroupRoleLists($admin_id){

		$sql = "SELECT agr.*
		          FROM `admin_group_role` agr 
	    	      LEFT JOIN `admin` a ON agr.admin_group_id = a.admin_group_id
				WHERE a.admin_id = {$admin_id}";

		$query = $this->db->query($sql)->result_array();
	
		return $query;
	}

}


/* End of file Business_model */