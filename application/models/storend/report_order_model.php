<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 模型 - 統計報表 / 訂單
 * @modelName report_order_model
 * @author	Dio
 */
class report_order_model extends CI_Model {

	// --------------------------------------------------------------------
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
    public function __construct(){
       
       $this->o   = 'order';
       $this->od  = 'order_detail';
    	
       $this->p   = 'product';
       $this->ps  = 'product_speci';
    }  	
    
    // --------------------------------------------------------------------
    
    /**
     * 方法: 搜尋條件
     *
     * @access	public
     * @param
     * @return
     */
    public function shr_condition($srh_data){
    
    	//串接SQL語句
    	$sql_where = " WHERE {$this->od}.order_detail_id <> '' ";
    
    	//依訂購日期
    	if( !empty($srh_data['srh_cdate1']) && !empty($srh_data['srh_cdate2'])  ){
    		$sql_where .= "AND SUBSTR(`{$this->o}`.cdate, 1 ,10) >= '".$srh_data['srh_cdate1']."'
    		               AND SUBSTR(`{$this->o}`.cdate, 1 ,10) <= '".$srh_data['srh_cdate2']."'" ;
    	}else{
    		//沒有填入日期，預設不顯示資料
    		$sql_where .= "AND SUBSTR(`{$this->o}`.cdate, 1 ,10) = '1997-01' "; 
    	}
    	
    	//依[狀態]
    	if( isset($srh_data['srh_status']) && is_numeric($srh_data['srh_status'])){
    		$sql_where .= " AND `{$this->o}`.status = '".$srh_data['srh_status']."'";
    	}
    
    	return $sql_where;
    }
    
    // --------------------------------------------------------------------
    
    /**
     * 方法: 搜尋條件2
     *         依年、月、週、日 搜尋
     *
     * @access	public
     * @param
     * @return
     */
    public function shr_condition2($srh_data){
    
    	//$sql_where['group_by'] = " GROUP BY SUBSTR(`{$this->o}`.cdate, 1 ,4)";
    	$sql_where['group_by'] = " GROUP BY SUBSTR(`{$this->o}`.cdate, 1 ,10)";
    	   
    	$sql_where['sdate'] = " SUBSTR(`{$this->o}`.cdate, 1 ,10) as sdate ";
    	$sql_where['edate'] = " SUBSTR(`{$this->o}`.cdate, 1 ,10) as edate ";
    
    	//依群組
    	if( !empty($srh_data['srh_group_date'])){
    
    		switch ($srh_data['srh_group_date']) {
    			case 'year':
    				$sql_where['sdate'] = " SUBSTR(`{$this->o}`.cdate, 1 ,4) as sdate ";
    				$sql_where['edate'] = " SUBSTR(`{$this->o}`.cdate, 1 ,4) as edate ";
    				$sql_where['group_by'] = " GROUP BY YEAR(`{$this->o}`.cdate) ";
    				break;
    
    			case 'month':
    				$sql_where['sdate'] = " SUBSTR(`{$this->o}`.cdate, 1 ,7) as sdate ";
    				$sql_where['edate'] = " SUBSTR(`{$this->o}`.cdate, 1 ,7) as edate ";
    				$sql_where['group_by'] = " GROUP BY MONTH(`{$this->o}`.cdate) ";
    				break;
    
    			case 'week':
    				$sql_where['sdate'] = " date_format(date_sub(`{$this->o}`.cdate, interval date_format(`{$this->o}`.cdate, '%w') day), '%Y-%m-%d') as sdate ";
    				$sql_where['edate'] = " date_format(date_add(`{$this->o}`.cdate, interval (6-date_format(`{$this->o}`.cdate, '%w')) day),  '%Y-%m-%d') as edate ";
    				$sql_where['group_by'] = " GROUP BY date_format(`{$this->o}`.cdate, '%X%V') ";
    				break;
    
    			case 'day':
    				$sql_where['group_by'] = " GROUP BY DAY(`{$this->o}`.cdate) ";
    				break;
    		}
    
    	}
    
    	return $sql_where;
    }
    
    // --------------------------------------------------------------------
    
    /**
     * 方法: 分頁條件
     *
     * @access	public
     * @param
     * @return
     */
    public function shr_page_condition($srh_data){
    
    	$sql_limit = " LIMIT 0,".$srh_data['srh_page_per'];
    
    	//串接SQL語句Limit
    	if( !(empty($srh_data['limit_page'])) ){
    		$sql_limit = " LIMIT ".(($srh_data['limit_page']-1)*$srh_data['srh_page_per'])." ,".$srh_data['srh_page_per'];
    	}
    
    	return $sql_limit;
    }   
    
    // --------------------------------------------------------------------
    
    /**
     * 方法: 統計
     *
     * 回傳值: [搜尋參數][資料]
     *
     * @access	public
     * @param
     * @return
     */
    public function shr_order($srh_data){
    
    	$sql_where = $this->shr_condition($srh_data);
    	$sql_where_ary = $this->shr_condition2($srh_data);
    	$sql_limit = $this->shr_page_condition($srh_data);
    
    	$sql = "SELECT {$sql_where_ary['sdate']} ,
    	               {$sql_where_ary['edate']} ,
    	               COUNT(`{$this->o}`.order_id) as order_count ,
    	               SUM({$this->od}.entity) as product_sum ,
    	               SUM({$this->od}.total) as total
    	        FROM `{$this->o}`
    	          LEFT JOIN {$this->od} ON `{$this->o}`.order_id = {$this->od}.order_id
    	          {$sql_where}
    	          {$sql_where_ary['group_by']}
        	    ORDER BY SUBSTR(`{$this->o}`.cdate, 1 ,10) DESC";
    
    	$query = $this->db->query($sql)->result_array();
      
    	if( !empty($query) ){
    	   return $query;
        }else{
           return array();
        }
    
    }

}

/* End of file Product_model */