<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 模型 - Dashboard_model  / 首頁
 *
 * @modelName Dashboard_model
 * @author	Dio
 */
class Dashboard_model extends CI_Model {

	// --------------------------------------------------------------------

	/**
	 * 方法: 取得訂單總計資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function get_order_status_group(){

		$sql = "SELECT status ,COUNT(status) as count FROM `order` GROUP BY status";
		$query = $this->db->query($sql)->result_array();

		if( !empty($query) ){
			 
			return $query;
		}else{
			return array();
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法: 取得商品總計資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function get_product_group(){

		$sql = "SELECT status ,COUNT(status) as count FROM `order` GROUP BY status";
		$query = $this->db->query($sql)->result_array();

		if( !empty($query) ){
			 
			return $query;
		}else{
			return array();
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法: 取得最近10筆訂單資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function get_order(){

		$sql = "SELECT * , (SELECT SUM(total) FROM order_detail od WHERE od.order_id = o.order_id  GROUP BY od.order_id ) as total
		          FROM `order` o 
    	        ORDER BY order_id DESC       
    	        LIMIT 10";		
			
		$query = $this->db->query($sql)->result_array();
			
		if( !empty($query) ){
			return $query;
		}else{
			return array();
		}
	}


}


/* End of file Dashboard_model */