<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 模型 - Statistics 各式統計報表
 * @modelName Statistics_model
 * @author	Dio
 */
class Statistics_model extends CI_Model {

	// --------------------------------------------------------------------
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
    public function __construct(){
    	
       $this->o   = 'order';	
       $this->od  = 'order_detail';
       
       $this->p  = 'product';
       $this->pd  = 'product_speci';
    }  	
	
	// --------------------------------------------------------------------

	/**
	 * 方法: 商品銷售統計
	 *
	 * 回傳值: [搜尋參數][資料]
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_sales_statistics($srh_data){

		$sql_where = $this->shr_sales_statistics_condition($srh_data);
		$sql_where_ary = $this->shr_sales_statistics_condition2($srh_data);
		$sql_limit = $this->shr_page_condition($srh_data);

		$sql = "SELECT {$sql_where_ary['sdate']} ,
		{$sql_where_ary['edate']} ,
                       COUNT(`{$this->o}`.order_id) as order_count ,
                       SUM({$this->od}.entity) as product_sum ,
                       SUM({$this->od}.total) as total  
               FROM `{$this->o}`
               LEFT JOIN {$this->od} ON `{$this->o}`.order_id = {$this->od}.order_id
               {$sql_where}
               {$sql_where_ary['group_by']}
               ORDER BY SUBSTR(`{$this->o}`.cdate, 1 ,10) DESC
    	       ";
                
               $query = $this->db->query($sql)->result_array();

               if( !empty($query) ){
               	return $query;
               }else{
               	return array();
               }

	}

	// --------------------------------------------------------------------

	/**
	 * 方法: 商品銷售統計 / 搜尋條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_sales_statistics_condition($srh_data){

		//串接SQL語句
		$sql_where = " WHERE {$this->od}.order_detail_id <> '' ";

		//依訂購日期
		if( !empty($srh_data['srh_cdate1']) && !empty($srh_data['srh_cdate2'])  ){
			$sql_where .= "AND SUBSTR(`{$this->o}`.cdate, 1 ,10) >= '".$srh_data['srh_cdate1']."'
    	                     AND SUBSTR(`{$this->o}`.cdate, 1 ,10) <= '".$srh_data['srh_cdate2']."'" ;
		}else{
			$sql_where .= "AND SUBSTR(`{$this->o}`.cdate, 1 ,10) = '1997-01' "; //沒有填入日期，預設不顯示資料
		}

		//依[狀態]
		if( !(empty($srh_data['srh_status']) && $srh_data['srh_status'] !== '0') ){
			$sql_where .= " AND `{$this->o}`.status IN (".$srh_data['srh_status'].")";
		}

		return $sql_where;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法: 商品銷售統計 / 搜尋條件2
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_sales_statistics_condition2($srh_data){

		$sql_where['group_by'] = " GROUP BY SUBSTR(`{$this->o}`.cdate, 1 ,4)";
		$sql_where['sdate'] = " SUBSTR(`{$this->o}`.cdate, 1 ,10) as sdate ";
		$sql_where['edate'] = " SUBSTR(`{$this->o}`.cdate, 1 ,10) as edate ";

		//依群組
		if( !empty($srh_data['srh_group_date'])){
				
			switch ($srh_data['srh_group_date']) {
				case 'year':
					$sql_where['sdate'] = " SUBSTR(`{$this->o}`.cdate, 1 ,4) as sdate ";
					$sql_where['edate'] = " SUBSTR(`{$this->o}`.cdate, 1 ,4) as edate ";
					$sql_where['group_by'] = " GROUP BY YEAR(`{$this->o}`.cdate) ";
					break;

				case 'month':
					$sql_where['sdate'] = " SUBSTR(`{$this->o}`.cdate, 1 ,7) as sdate ";
					$sql_where['edate'] = " SUBSTR(`{$this->o}`.cdate, 1 ,7) as edate ";
					$sql_where['group_by'] = " GROUP BY MONTH(`{$this->o}`.cdate) ";
					break;

				case 'week':
					$sql_where['sdate'] = " date_format(date_sub(`{$this->o}`.cdate, interval date_format(`{$this->o}`.cdate, '%w') day), '%Y-%m-%d') as sdate ";
					$sql_where['edate'] = " date_format(date_add(`{$this->o}`.cdate, interval (6-date_format(`{$this->o}`.cdate, '%w')) day),  '%Y-%m-%d') as edate ";
					$sql_where['group_by'] = " GROUP BY date_format(`{$this->o}`.cdate, '%X%V') ";
					break;

				case 'day':
					$sql_where['group_by'] = " GROUP BY DAY(`{$this->o}`.cdate) ";
					break;
			}
				
		}

		return $sql_where;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法: 商品銷售統計 / 群組計算
	 *
	 * 回傳值: [搜尋參數][資料]
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_sales_statistics_group_by($srh_data){

		$sql_where = $this->shr_sales_statistics_condition($srh_data);

		$sql = "SELECT COUNT(`{$this->o}`.order_id) as count
		          FROM `{$this->o}`
    	        LEFT JOIN {$this->od} ON `{$this->o}`.order_id = {$this->od}.order_id
    	        {$sql_where} ";

    	        $query = $this->db->query($sql)->result_array();

    	        if( !empty($query) ){
    	        	return $query;
    	        }else{
    	        	return array();
    	        }
	}

	// --------------------------------------------------------------------

	/**
	 * 方法: 分頁條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_page_condition($srh_data){

		$sql_limit = " LIMIT 0,".$srh_data['srh_page_per'];

		//串接SQL語句Limit
		if( !(empty($srh_data['limit_page'])) ){
			$sql_limit = " LIMIT ".(($srh_data['limit_page']-1)*$srh_data['srh_page_per'])." ,".$srh_data['srh_page_per'];
		}

		return $sql_limit;
	}

	/*
	 * ----------------------------------------------------------------------------------
	 *
	 *  底下為商品購買統計 :
	 *
	 * ----------------------------------------------------------------------------------
	 *
	 *  shr_buy_statistics 商品購買統計
	 *  shr_buy_statistics_condition 商品購買統計 / 搜尋條件
	 *  shr_buy_statistics_group_by 商品購買統計 / 群組計算
	 *  shr_buy_statistics_sort_condition 商品購買統計 / 排序條件
	 */

	// --------------------------------------------------------------------

	/**
	 * 方法: 商品購買統計 / 搜尋條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_buy_statistics_condition($srh_data){

		//串接SQL語句
		$sql_where = " WHERE {$this->pd}.product_speci_id <> '' ";

		//依訂購日期
		if( !empty($srh_data['srh_cdate1']) && !empty($srh_data['srh_cdate2'])  ){
			$sql_where .= "AND SUBSTR({$this->p}.cdate, 1 ,10) >= '".$srh_data['srh_cdate1']."'
    	                     AND SUBSTR({$this->p}.cdate, 1 ,10) <= '".$srh_data['srh_cdate2']."'" ;
		}

		//依[商品編號]
		if( !(empty($srh_data['srh_product_id']) && $srh_data['srh_product_id'] !== '0') ){
			$sql_where .= " AND {$this->p}.product_id = '".LTRIM($srh_data['srh_product_id'],'0')."' ";
		}

		//依[商品名稱]
		if( !(empty($srh_data['srh_name']) && $srh_data['srh_name'] !== '0') ){
			$sql_where .= " AND {$this->p}.name LIKE '%".$srh_data['srh_name']."%' ";
		}

		return $sql_where;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法: 商品購買統計 / 排序條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_buy_statistics_sort_condition($srh_data){

		$sql_sort = "";

		//串接SQL語句ORDER BY
		if( !(empty($srh_data['srh_sort'])) ){
			$sql_sort = " ORDER BY  ".$srh_data['srh_sort']." ".$srh_data['srh_sort_type'];
		}

		return $sql_sort;
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法: 商品購買統計
	 *
	 * 回傳值: [搜尋參數][資料]
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_buy_statistics($srh_data){
		
			$sql_where = $this->shr_buy_statistics_condition($srh_data);
		$sql_limit = $this->shr_page_condition($srh_data);
		$sql_sort = $this->shr_buy_statistics_sort_condition($srh_data);

		$sql = "SELECT {$this->pd}.product_id ,{$this->pd}.sku ,{$this->od}.product_speci_id ,{$this->od}.product_color ,SUM({$this->od}.entity) as entity ,SUM({$this->od}.total) as product_total
                    ,(SELECT name FROM {$this->p} WHERE {$this->p}.product_id = {$this->pd}.product_id) as name  
		           FROM {$this->pd}
                 LEFT JOIN {$this->od} ON {$this->pd}.product_speci_id = {$this->od}.product_speci_id
                 {$sql_where}
               GROUP BY {$this->pd}.sku 
               {$sql_sort}{$sql_limit}";
               
               $query = $this->db->query($sql)->result_array();

               if( !empty($query) ){
               	return $query;
               }else{
               	return array();
               }

	}

	// --------------------------------------------------------------------

	/**
	 * 方法: 商品購買統計 / 群組計算
	 *
	 * 回傳值: [搜尋參數][資料]
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_buy_statistics_group_by($srh_data){

		$sql_where = $this->shr_buy_statistics_condition($srh_data);

		$sql = "SELECT COUNT({$this->pd}.sku) as count
                  FROM {$this->pd} {$sql_where}
                 ORDER BY SUBSTR({$this->pd}.cdate, 1 ,10) DESC ";           

                  $query = $this->db->query($sql)->result_array();

                    if( !empty($query) ){
                    	return $query;
                    }else{
                    	return array();
                    }

	}

}

/* End of file Statistics_model */