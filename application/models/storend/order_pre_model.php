<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 處理[訂單]的模型
 *
 * @modelName Order_pre_model
 * @author	Dio
 */
class Order_pre_model extends CI_Model {
	
	// --------------------------------------------------------------------
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
    public function __construct(){
    	
       $this->op  = 'order_pre'; 
       $this->c   = 'customer';
       $this->p   = 'product';
    }   
  
	// --------------------------------------------------------------------

	/**
	 * 方法: 搜尋條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_order_condition($srh_data){

		//串接SQL語句
		$sql_where = " WHERE `{$this->op}`.order_pre_id <> '' ";
		
		//依[訂購日期]
		if( !empty($srh_data['srh_order_pre_cdate1']) && !empty($srh_data['srh_order_pre_cdate2'])  ){
			$sql_where .= "AND SUBSTR(`{$this->op}`.cdate, 1 ,10) >= '".$srh_data['srh_order_pre_cdate1']."'
    	                  AND SUBSTR(`{$this->op}`.cdate, 1 ,10) <= '".$srh_data['srh_order_pre_cdate2']."'" ;
		}

		//依[狀態]
		if( isset($srh_data['srh_status']) && is_numeric($srh_data['srh_status'])){
			$sql_where .= " AND `{$this->op}`.status = '".$srh_data['srh_status']."'";
		}

		return $sql_where;
	}
    
	
	// --------------------------------------------------------------------

	/**
	 * 方法: 排序條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_sort_condition($srh_data){

		$sql_sort = "";

		//串接SQL語句ORDER BY
		if( !(empty($srh_data['srh_sort'])) && !(empty($srh_data['srh_sort_type'])) ){
			$sql_sort = " ORDER BY  ".$srh_data['srh_sort']." ".$srh_data['srh_sort_type'];
		}else{
			$sql_sort = " ORDER BY `{$this->op}`.order_pre_id DESC";
		}		

		return $sql_sort;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法: 分頁條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_page_condition($srh_data){

		$sql_limit = " LIMIT 0,".$srh_data['srh_page_per'];

		//串接SQL語句Limit
		if( !(empty($srh_data['srh_limit_page'])) && !(empty($srh_data['srh_page_per'])) ){
			$sql_limit = " LIMIT ".(($srh_data['srh_limit_page']-1)*$srh_data['srh_page_per'])." ,".$srh_data['srh_page_per'];
		}

		return $sql_limit;
	}
	
    // --------------------------------------------------------------------

	/**
	 * 方法: 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_order_pre($srh_data){

		//搜尋條件
		$sql_where = $this->shr_order_condition($srh_data);
		$sql_limit = "";//$this->shr_page_condition($srh_data);
		$sql_sort  = $this->shr_sort_condition($srh_data);
		
		$sql = "SELECT {$this->op}.*
          		      ,{$this->p}.name as pname
          		      ,{$this->p}.sku as psku
	         	FROM {$this->op}
		         LEFT JOIN {$this->p} ON {$this->op}.product_id = {$this->p}.product_id
	             {$sql_where} {$sql_sort} {$sql_limit}";
		
		$query = $this->db->query($sql)->result_array();

		if( !empty($query) ){
		  	return $query;
		}else{
		   	return array();
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 檢視
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_order_pre_info($order_pre_id){
			
		$sql = "SELECT {$this->op}.*
          		      ,{$this->p}.name as pname
          		      ,{$this->p}.sku as psku
	         	FROM {$this->op}
		         LEFT JOIN {$this->p} ON {$this->op}.product_id = {$this->p}.product_id
		        WHERE {$this->op}.order_pre_id = {$order_pre_id}";
		
		$query = $this->db->query($sql)->row_array();
			
		if( !empty($query) ){
		  return $query;
	    }else{
		  return array();
	    }
	}

	// --------------------------------------------------------------------

	/**
	 * 方法: 訂單搜尋 / 群組計算
	 *
	 * 回傳值: [搜尋參數][資料]
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_order_pre_group_by($srh_data){

		$sql_where = "" ; //$this->shr_order_condition($srh_data);

		$sql = "SELECT COUNT(order_pre_id) as count FROM `{$this->op}` {$sql_where}";
			
		$query = $this->db->query($sql)->result_array();

		if( !empty($query) ){
			return $query;
		}else{
			return array();
		}

	}
	
}

/* End of file Order_pre_model */