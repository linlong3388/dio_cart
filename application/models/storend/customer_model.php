<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 模型 - 處理[會員]的模型
 * @modelName Customer_model
 * @author	Dio
 */
class Customer_model extends CI_Model {

    // --------------------------------------------------------------------
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
    public function __construct(){
    	
       $this->c   = 'customer';	
    }   
	
	// --------------------------------------------------------------------

	/**
	 * 方法: 搜尋條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_customer_condition($srh_data){

		//串接SQL語句
		$sql_where = " WHERE {$this->c}.customer_id <> '' ";

		//依會員編號
		if( !empty($srh_data['srh_customer_id']) ){
			$sql_where .= " AND {$this->c}.customer_id = '".$srh_data['srh_customer_id']."'";
		}

		//依 Email
		if( !empty($srh_data['srh_email']) ){
			$sql_where .= " AND {$this->c}.email LIKE '%{$srh_data['srh_email']}%' ";
		}

		//依 姓
		if( !empty($srh_data['srh_first_name']) ){
			$sql_where .= " AND {$this->c}.first_name LIKE '%{$srh_data['srh_first_name']}%' ";
		}

		//依 名
		if( !empty($srh_data['srh_last_name']) ){
			$sql_where .= " AND {$this->c}.last_name LIKE '%{$srh_data['srh_last_name']}%' ";
		}

		//依建立日期
		if( !empty($srh_data['srh_cdate']) ){
			$sql_where .= "AND SUBSTR({$this->c}.cdate, 1 ,10) = '".$srh_data['srh_cdate']."'" ;
		}

		//依會員狀態
		if( isset($srh_data['srh_status']) && $srh_data['srh_status'] >= 0 ){
		    $sql_where .= " AND {$this->c}.status  IN (".$srh_data['srh_status'].")";
		}

		return $sql_where;
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法: 分頁條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_page_condition($srh_data){

		$sql_limit = " LIMIT 0,".$srh_data['srh_page_per'];

		//串接SQL語句Limit
		if( !(empty($srh_data['srh_page'])) && !(empty($srh_data['srh_page_per'])) ){
			$sql_limit = " LIMIT ".(($srh_data['srh_page']-1)*$srh_data['srh_page_per'])." ,".$srh_data['srh_page_per'];
		}

		return $sql_limit;
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法: 排序條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_sort_condition($srh_data){

		$sql_sort = "";

		//串接SQL語句ORDER BY
		if( !(empty($srh_data['srh_sort'])) && !(empty($srh_data['srh_sort_type'])) ){
			$sql_sort = " ORDER BY  ".$srh_data['srh_sort']." ".$srh_data['srh_sort_type'];
		}else{
			$sql_sort = " ORDER BY {$this->c}.customer_id DESC";
		}

		return $sql_sort;
	}
	
	/*
	 * ----------------------------------------------------------------------------------
	 *
	 *  底下為SQL語句
	 *
	 * ----------------------------------------------------------------------------------
	 *
	 *
	 */
	
	// --------------------------------------------------------------------

	/**
	 * 方法: 搜尋列表
	 *
	 * 回傳值: [搜尋參數][資料]
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_customer($srh_data){

		$sql_where = $this->shr_customer_condition($srh_data);
		$sql_limit = $this->shr_page_condition($srh_data);
		$sql_sort = $this->shr_sort_condition($srh_data);

		$sql = "SELECT * FROM {$this->c}
		        {$sql_where} {$sql_sort} {$sql_limit}";
		        
		$query = $this->db->query($sql)->result_array();

		if( !empty($query) ){
			return $query;
		}else{
			return array();
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法: 群組計算
	 *
	 * 回傳值: [搜尋參數][資料]
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_customer_group_by($srh_data){

		$sql_where = $this->shr_customer_condition($srh_data);

		$sql = "SELECT COUNT(DISTINCT {$this->c}.customer_id) as count
		          FROM {$this->c} {$sql_where}";

		$query = $this->db->query($sql)->result_array();

		if( !empty($query) ){
		  	return $query;
		}else{
		   	return array();
		}
	}

}

/* End of file Customer_model_new */