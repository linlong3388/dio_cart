<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 類別功能 : 購物車記錄類別。
 * 說明 : 將階段性的購物記錄寫入資料表，以利會員下次登入時撈取資料。
 *
 * @controllerName Cart_log
 * @author Dio
 *
 */
class Cart_log{

	private $serializeData;

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		$this->CI =& get_instance();
	}

	// --------------------------------------------------------------------
	 
	/**
	 * 方法 : setCartData
	 * 說明 : 將訂購紀錄給序列化後，並寫入會員的Cart欄位。
	 *
	 * @access	public
	 * @param	array
	 * @return	t/f
	 */
	public function setCartData($data){

		//序列化陣列資料
		$this->serializeData = serialize($data);

		$this->CI->db->where('customer_id' ,$_SESSION['customer_info']['customer_id']);
		$this->CI->db->update('customer' ,array('cart' => $this->serializeData ));

		return ($this->CI->db->affected_rows() != 1) ? false : true;
	}

	// --------------------------------------------------------------------
	 
	/**
	 * 方法 : getCartData
	 * 說明 : 將資料表(會員的Cart欄位)取出，並解開序列化為陣列。
	 *
	 * @access	public
	 * @param
	 * @return	array
	 */
	public function getCartData(){

		$this->CI->db->where('customer_id' ,$_SESSION['customer_info']['customer_id']);
		$this->CI->db->select('cart');
		$query = $this->CI->db->get('customer')->row_array();

		if($query['cart']){

			//解序列化為陣列資料
			$this->serializeData = unserialize($query['cart']);

			return $this->serializeData;
		}else{
			return array();
		}
	}

}


/* End of file Cart_log.php */