<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 類別功能 : 搜尋類別。
 *
 * @controllerName Dio_search
 * @author Dio
 *
 */
class Dio_search{

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		$this->CI =& get_instance();
	}

	// --------------------------------------------------------------------
	 
	/**
	 * 方法 : [前台]搜尋方法
	 * 說明 : 商品全文檢索
	 *
	 * @access	public
	 * @param	$data
	 * @return
	 */
	public function search($searchText){

		//WHERE語句
		$str_where = " WHERE status = '1' ";

		//搜尋
		if( !empty($searchText) ){
			$str_where .= " AND (`name` LIKE '%{$searchText}%')";
		}

		$sql = "SELECT * FROM `product` {$str_where} ";
	  
		$result = $this->CI->db->query($sql)->result_array();

		return $result;
	}

}


/* End of file Dio_search.php */