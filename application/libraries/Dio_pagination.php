<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 * 類別功能 : 分頁類別。
 * 說明 : 
 *      Step1 : 先用get_limit_range()取得limit值。 
 *      Step2 : 將limit資料帶入料表的搜尋條件。 
 *      Step3 : 取得導覽器 pagination()。 
 * 
 * @controllerName dio_pagination
 * @author Dio
 * 
 */
class Dio_pagination{
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *  
	 * @access	public
	 * @param	
	 * @return	
	 */
	public function __construct(){
	  
		$this->page   = 1 ;  //url get目前頁碼
	}
	
	// --------------------------------------------------------------------
   
	/**
	 * 方法 : 依$page取得LIMIT的範圍
	 * 說明 : 搭配資料庫的LIMIT以便資料同步輸出。
	 * 
	 * @access	public
	 * @param	[頁碼]                   
	 * @return	[limit起始值]
	 */
	public function get_limit($page ,$limit2){
		
		$this->page = $page;
			
		$this->limit1 = 0 ;       //limit搜尋範圍1
		$this->limit2 = $limit2 ; //limit搜尋範圍2
		
	    //取得url的get  
        if($this->page >= 1){
            $this->limit1  = ($page-1)*$this->limit2;
        }
        
        return array( 'limit1' => $this->limit1 ,
                      'limit2' => $this->limit2 );		
       
	}
	
	// --------------------------------------------------------------------
   
	/**
	 * 方法 : 取得url路徑
	 * 說明 : 如:/hr4567/index.php/frontend/center/message?page=5
	 * 
	 * @access	private
	 * @param	                   
	 * @return	
	 */
	private function get_url(){
		       
        $url = $_SERVER["REQUEST_URI"];
        $url = parse_url($url);
        $url = $url['path'];	

        return $url;
	}
	
	// --------------------------------------------------------------------
   
	/**
	 * 方法 : 分頁方法
	 *
	 * @access	public
	 * @param	[總筆數] ,[頁碼]                   
	 * @return	
	 */
	public function pagination($total ,$viewPage){
		
		//參數設定
		$limit1        = $this->limit1 ;        //limit搜尋範圍1
		$limit2        = $this->limit2 ;        //limit搜尋範圍2
		$viewPage      = $viewPage ;            //可視分頁數
		$viewPageNum   = ceil( $viewPage/2);    //可視分頁開始移動值
		$numPage       = ceil( $total/$limit2); //總頁數
		$is_first      = false;
		$is_last       = false;

		//移動可視範圍
		$range1 = 0;
		$range2 = 0;
		
		//邊界判斷
		if($viewPage < $numPage){
			
			$range1 = ($this->page > $viewPageNum) ? $this->page-($viewPageNum-1) : 1;
		    $range2 = $range1+($viewPage-1);
		    $range2 = ($range2 >= $numPage) ? $numPage : $range2;
			
		    //last determine
		    if($range2 >= $numPage){
		        $is_last = true;
 		        $range1 = $range2-($viewPage-1);
		    }
		    
		    if($range1 <= 1){
		     	$is_first = true;
		    }
		    
		}else{
			
		    $is_last = true;
		    $is_first = true;
			$range1 = 1;
			$range2 = $numPage;
		}
		
		 //分頁參數設置
        $pagination = array(
		                     'url'               => $this->get_url() , //取得url路徑
		                     'page'              => $this->page ,      //目前頁碼
		                     'limit1'            => $limit1 ,          //limit/起始值
		                     'limit2'            => $limit2 ,          //limit/結束值(每頁頁數)
                             'numPage'           => $numPage,          //總頁數
                             'total'             => $total,            //總筆數
                             'range1'            => $range1,           //可視頁碼範圍/起始值
                             'range2'            => $range2,           //可視頁碼範圍/結束值
                             'is_first'          => $is_first ,        //是否為[首頁]
                             'is_last'           => $is_last    //是否為[末頁]
                           );
                      
        return $pagination;   			
	} 
	
}


/* End of file dio_pagination.php */