<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 類別功能 : 登入 Facebook 類別。
 *
 * @controllerName dio_fb
 * @author Dio
 *
 */
class Dio_fb{

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		
		$this->CI =& get_instance();
		$this->CI->load->helper(array('url'));
		
		require_once 'autoload.php';
		
		//基本配置
		$this->fb = new Facebook\Facebook([
				'app_id'     => '2010343035845330',
				'app_secret' => 'f19475e3ee41aede2c474cd4ba344217',
				'default_graph_version' => 'v2.11',
		]);
		
		//設定返回網址
		$this->redirect_uri = site_url('sign_in/login_fb');		
		
		//登入網址
		$this->login_url = "" ;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 登入
	 *
	 * @access	public
	 * @param	
	 * @return
	 */
	public function login(){

		$helper = $this->fb->getRedirectLoginHelper();

		//step1 取得權杖
		try {
		 	$accessToken = $helper->getAccessToken();
		
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			// When Graph returns an error
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
		
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
			// When validation fails or other local issues
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}
		
		//step2 取得fb個資
		if (isset($accessToken)) {
			// Logged in!
			$_SESSION['facebook_access_token'] = (string) $accessToken;
		
			try {
				$response = $this->fb->get('/me?fields=id,name',$_SESSION['facebook_access_token']);
			
			} catch(Facebook\Exceptions\FacebookResponseException $e) {
				echo 'Graph returned an error: ' . $e->getMessage();
				exit;
			
			} catch(Facebook\Exceptions\FacebookSDKException $e) {
				echo 'Facebook SDK returned an error: ' . $e->getMessage();
				exit;
			}
		
			$user = $response->getGraphUser();
		
			return $user;
		}
		
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得登入網址
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function get_login_url(){
		
		$helper = $this->fb->getRedirectLoginHelper();
		
		$permissions = ['email', 'user_likes']; // optional
		
		$this->login_url = $helper->getLoginUrl($this->redirect_uri, $permissions);
		
		return $this->login_url;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 登出
	 *
	 * @access	public
	 * @param
	 * @return
	 */
    public function logout(){

    	unset($_SESSION['facebook_access_token']);
    }
}


/* End of file dio_fb */