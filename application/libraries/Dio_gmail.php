<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 類別功能 : 透過Gmail@主機發送email類別。
 *           (採用 phpMailer 的方式)
 *          
 * 備註 : 資料表目前需獨立設定
 *
 * @controllerName Dio_gmail
 * @author Dio
 *
 */
class Dio_gmail{
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		
		$this->CI =& get_instance();
		$this->CI->load->helper(array('url','dio_format'));
		
		require_once 'PHPMailer/PHPMailerAutoload.php';
		
		$this->mail = new PHPMailer;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 發送 Email 
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function send_email($data){
		
		$this->mail->IsSMTP(); 
		$this->mail->CharSet = 'UTF-8';
		$this->mail->IsHTML(true);
		$this->mail->SMTPDebug = 0;
		$this->mail->Debugoutput = 'html';
		$this->mail->Host = 'smtp.gmail.com';
		$this->mail->Port = 587;
		$this->mail->SMTPSecure = 'tls';
		$this->mail->SMTPAuth = true;
		//測試站
		$this->mail->Username = "froghip69@gmail.com";
		$this->mail->Password = "fishsaut";
		
		$this->mail->setFrom($data['info']['from'], $data['info']['subject']);

		$this->mail->From     = DIO_EMAIL;   //設定寄件者信箱
		$this->mail->FromName = DIO_WEBNAME; //設定寄件者姓名
		
		//PHP5.6之後的版本，預設會驗證 peer 及 peer name，所以要關閉
		$this->mail->SMTPOptions = array (
				'ssl' => array (
						'verify_peer' => false,
						'verify_peer_name' => false,
						'allow_self_signed' => true
				)
		);
		
		//管理員是否接收
		//if( !empty($data['info']['to_admin']) ){
		$this->mail->AddBCC(DIO_EMAIL, $data['info']['subject']);
		//$this->mail->AddBCC('sclalan168@gmail.com', $data['info']['subject']);
		//}
		$this->mail->AddBCC($data['info']['to'], $data['info']['subject']);
		//$this->mail->addAddress($data['info']['to'], $data['info']['subject']);
		$this->mail->Subject = $data['info']['subject'];
		$this->mail->Body = $data['info']['message'];
		
		if (!$this->mail->send()) {
			//echo "Mailer Error: " . $this->mail->ErrorInfo;
			return false;
		} else {
		    return true;
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 電子報
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edm($data){
		
		$data['info']['from']     = DIO_EMAIL;
		$data['info']['to']       = $data['email'];
		$data['info']['company']  = DIO_COMPANY;
		$data['info']['webpage']  = DIO_WEBPAGE;
		$data['info']['subject']  = $data['title'];
		$data['info']['message']  = $data['content'];
		$data['info']['template'] = $this->CI->load->view('widget/email/edm.tpl',$data,true);
	
		return $this->send_email($data);
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 聯絡我們
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function contact($data){
			
		$data['info']['from']    = DIO_EMAIL;
		$data['info']['to']      = DIO_EMAIL;
		$data['info']['company'] = DIO_COMPANY;
		$data['info']['webpage'] = DIO_WEBPAGE;
		$data['info']['subject'] = "您有一筆來自 【" .$data['info']['company']. "】的訊息" ;
		$data['info']['message'] = $this->CI->load->view('widget/email/contact.tpl',$data,true);
		
		return $this->send_email($data);
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 聯絡我們To會員
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function contactToUser($data){
			
		$data['info']['from']    = DIO_EMAIL;
		$data['info']['to']      = $data['email'];
		$data['info']['company'] = DIO_COMPANY;
		$data['info']['webpage'] = DIO_WEBPAGE;
		$data['info']['subject'] = "您有一筆來自 【" .$data['info']['company']. "】的訊息" ;
		$data['info']['message'] = $this->CI->load->view('widget/email/contactToUser.tpl',$data,true);
	
		return $this->send_email($data);
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 註冊成功
	 *
	 * @access	public
	 * @param	
	 * @return	
	 */
    public function join($data){

    	$data['info']['from']    = DIO_EMAIL;
    	$data['info']['to']      = $data['email'];
    	$data['info']['company'] = DIO_COMPANY;
    	$data['info']['webpage'] = DIO_WEBPAGE;
    	$data['info']['subject'] = "恭喜您註冊成功，歡迎加入 【" .$data['info']['company']. "】" ;
    	$data['info']['message'] = $this->CI->load->view('widget/email/join.tpl',$data,true);
    	
    	return $this->send_email($data);
    } 
    
    // --------------------------------------------------------------------

	/**
	 * 方法 : 忘記密碼
	 *
	 * @access	public
	 * @param	
	 * @return	
	 */
    public function forget($param){
    
    	$data['info']['new_password'] = uniqid();
    	
    	$data['info']['from']    = DIO_EMAIL;
    	$data['info']['to']      = $param['email'];
    	$data['info']['company'] = DIO_COMPANY;
    	$data['info']['webpage'] = DIO_WEBPAGE;
    	$data['info']['subject'] = "【" .$data['info']['company']. "】忘記密碼通知" ;
    	$data['info']['message'] = $this->CI->load->view('widget/email/forget.tpl',$data,true);
    	
      	if ($this->send_email($data)){
      	    //更新會員密碼  
      	    if( !empty($data['info']['to']) ){
      	       $this->CI->db->where('username' ,$param['username']);
      	       $this->CI->db->where('TRIM(email)',$data['info']['to']);
      	       $this->CI->db->limit(1);
      	       $this->CI->db->update('customer',array('password' => MD5($data['info']['new_password']) ));
      	    }
      	    return true;  
      	}else{
      	    return false;  
      	}       	
        
    }

    // --------------------------------------------------------------------
    
    /**
     * 方法 : 更新密碼
     *
     * @access	public
     * @param
     * @return
     */
    public function forget_update($data){
    	 
    	$data['info']['new_password'] = uniqid();
    	 
    	$data['info']['from']    = DIO_EMAIL;
    	$data['info']['to']      = $data['email'];
    	$data['info']['company'] = DIO_COMPANY;
    	$data['info']['webpage'] = DIO_WEBPAGE;
    	$data['info']['subject'] = "【" .$data['info']['company']. "】更新密碼通知" ;
    	$data['info']['message'] = $this->CI->load->view('widget/email/forget_update.tpl',$data,true);
    	 
    	if ($this->send_email($data)){
    		return true;
    	}else{
    		return false;
    	}
    
    }
   
   // --------------------------------------------------------------------

	/**
	 * 方法 : 訂單訂購成功發送 Email 
	 *
	 * @access	public
	 * @param	
	 * @return	
	 */
    public function order($data){
    	
    	$data['info']['from']     = DIO_EMAIL;
    	$data['info']['to_admin'] = DIO_EMAIL;
    	$data['info']['to']      = !empty($data['order']['master']['email']) ? $data['order']['master']['email'] : "";
    	$data['info']['company'] = DIO_COMPANY;
    	$data['info']['webpage'] = DIO_WEBPAGE;
    	$data['info']['subject'] = "您有一筆來自 【" .$data['info']['company']. "】的訊息" ;
    	$data['info']['data']    = $data;
    	$data['info']['message'] = $this->CI->load->view('widget/email/order.tpl',$data,true);
    	
    	return $this->send_email($data);
    }
    
    // --------------------------------------------------------------------
    
    /**
     * 方法 : 認養成功發送 Email
     *
     * @access	public
     * @param
     * @return
     */
    public function tree_order($data){

    	$data['info']['from']     = DIO_EMAIL;
    	$data['info']['to_admin'] = DIO_EMAIL;
    	$data['info']['to']      = !empty($data['email']) ? $data['email'] : "";
    	$data['info']['company'] = DIO_COMPANY;
    	$data['info']['webpage'] = DIO_WEBPAGE;
    	$data['info']['subject'] = "您有一筆來自 【" .$data['info']['company']. "】的訊息" ;
    	$data['info']['data']    = $data;
    	$data['info']['message'] = $this->CI->load->view('widget/email/tree_order.tpl',$data,true);
    
    	return $this->send_email($data);
    }

 }
 

/* End of file email.php */