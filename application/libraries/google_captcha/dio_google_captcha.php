<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 類別功能 :  Google 類別 / 驗證
 *
 * @controllerName dio_google_captcha
 * @author Dio
 *
 */
class dio_google_captcha{

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		
		$this->CI =& get_instance();
		
		//由於CI的建構函數不包含傳入值，所以和Google的API衝突，需獨立引入
		require_once(APPPATH.'libraries/google_captcha/autoload.php');
		
		//主機端的密鑰
		$this->secret  = DIO_GOOGLE_SECRET;
 	}                          
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 驗證captcha欄位
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function valid( $g_recaptcha_response ){
		
		$recaptcha = new \ReCaptcha\ReCaptcha($this->secret);
		
		//確認驗證碼與 IP
		$resp = $recaptcha->verify($g_recaptcha_response, $_SERVER['REMOTE_ADDR']);
			
		$var = var_export($g_recaptcha_response, true);
		
		$msg_err = "";
		
		if ($resp->isSuccess()) {
			return $msg_err;
		}else{
			foreach ($resp->getErrorCodes() as $code) {
				$msg_err .= $code ;
			}
			
			return $msg_err;
		}
	}

}


/* End of file dio_google_captcha */