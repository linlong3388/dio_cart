<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 類別功能 : 商品 / 推薦方式。
 *           (尚未完成，等有空的時後再規劃  2017/07/21)
 *  
 * @controllerName product_recommend
 * @author Dio
 *
 */
class Product_recommend{

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		$this->CI =& get_instance();
	}

	// --------------------------------------------------------------------
	 
	/**
	 * 方法 : 隨機
	 *
	 * @access	public
	 * @param	
	 * @return
	 */
	public function rand(){

		$srh_data = array(
				'srh_sort' => 'rand' ,
				'srh_limit1' => 0 ,
				'srh_limit2' => 4 ,
		);
	
		return $this->product_lists->lists($srh_data);
	}

}


/* End of file Product_recommend.php */