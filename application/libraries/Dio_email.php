<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 類別功能 : 發送email類別。
 *
 * 備註 : 資料表目前需獨立設定
 *
 * @controllerName dio_email
 * @author Dio
 *
 */
class Dio_email{

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		$this->CI =& get_instance();
		$this->CI->load->library(array('email'));
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 註冊成功發送 Email
	 *
	 * @access	public
	 * @param	$user_email、$user_password
	 * @return
	 */
	public function join($user_email ,$user_password){
		
		$webName = "恭喜您註冊成功，歡迎加入 " . DIO_WEBNAME;
		
		$to      = $user_email; //會員email
		$subject = "=?UTF-8?B?".base64_encode($webName)."?="; //主題
		 
		//內容
		$message = <<<EOD
<html>
<head>
<title>{$webName}</title>
</head>

<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">
    <p><h2>{$webName}</h2><p>
    <p>當您收到此信件，代表您已經註冊成功!!</p>
    <p><b>您的密碼為:</b><b style="color:red">$user_password</b></p>
    <p>感謝您的加入，祝您購物愉快!!</p>
</body>
</html>
				
EOD;
		
		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= "From: ".DIO_WEBNAME."<".DIO_EMAIL.">" . "\r\n";
		
		if( mail($to, $subject, $message, $headers) ){
			return true;
		}else{
			return false;
		}

	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 聯絡我們
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function contact($data){
	
		$webName = "您有一筆來自 【" .DIO_WEBNAME. "】的訊息";
		
		$to      = DIO_EMAIL;   //系統email
		$subject = "=?UTF-8?B?".base64_encode($webName)."?="; //主題
		$dio_webpage = DIO_WEBPAGE;
		
		
		//內容
		$message = <<<EOD
	
<html>
<head>
<title>{$webName}</title>
</head>
	
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">
    <p><h2>{$webName}</h2><p>
    <p><h3>底下是提問訊息和提問人的相關資料。</h3></p>
                <table border="1" cellpadding="1" width="100%">
                    <tr>
                        <td>姓名</td>
                        <td>{$data['name']}</td>
                    </tr>
                    <tr>
                        <td>手機</td>
                        <td>{$data['phone']}</td>
                    </tr>
                    <tr>
                        <td>電子郵件</td>
                        <td>{$data['email']}</td>
                    </tr>
                    <tr>
                        <td>訊息</td>
                        <td>{$data['content']}</td>
                    </tr>
                </table>
	
       <p><h3>■更多訊息查詢，請至<a href={$dio_webpage}/backend/login/valid target=\"_new\">「系統中心」</a>查詢。</h3></p>
                 
</body>
</html>
	
EOD;
		
		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= "From: ".DIO_WEBNAME."<".DIO_EMAIL.">" . "\r\n";
		
		if( mail($to, $subject, $message, $headers) ){
			return true;
		}else{
			return false;
		}
	
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 忘記密碼發送 Email
	 *
	 * @access	public
	 * @param	$user_email
	 * @return
	 */
	public function forget($user_email){
		 
		$webName = DIO_WEBNAME . " 密碼變更通知";
		
		$new_password = uniqid();    //新密碼
		$to           = $user_email; //會員email
		$subject = "=?UTF-8?B?".base64_encode($webName)."?="; //主題
		
		//內容
		$message = <<<EOD
<html>
<head>
<title>{$webName}</title>
</head>

<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">
    <p><h2>{$webName}</h2><p>
    <p> 建議您在使用新密碼登入後，立即變更此密碼，並妥善保存!!</p>
    <p><b>您的新密碼為:</b><b style="color:red">$new_password</b></p>
    <p>祝您購物愉快!!</p>
</body>
</html>
				
EOD;

		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= "From: ".DIO_WEBNAME."<".DIO_EMAIL.">" . "\r\n";

		if( mail($to, $subject, $message, $headers) ){
			//更新會員密碼
			$this->CI->db->where('TRIM(email)',$user_email);
			$this->CI->db->update('customer',array('password' => md5($new_password) ));
			 
			return true;
		}else{
			return false;
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 訂單訂購成功發送 Email
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function order($user_email ,$data){
		
		$webName = DIO_WEBNAME . " 訂單通知";
		
		$to      = $user_email; //會員email
		$subject = "=?UTF-8?B?".base64_encode($webName)."?="; //主題
		$dio_webpage = DIO_WEBPAGE;
		 
		//表格外的參數
		//$order_id         = $data['query_master']['order_id'];
		$order_show_id    = $data['query_master']['order_show_id'];
		$order_date       = substr($data['query_master']['cdate'] ,0,10);
		$order_status     = reSortArrayMutil(2,$data['query_master']['status'],getOrderStatus());
		//$order_pay_method = '固定運費';
		 
		//內容
		$data_msg   = '';
		$data_promo = '';
		 
		$entity = 0;
		
		foreach ( $data['query_detail'] as $row) {
			$data_msg .= "<tr>".chr(13);
			$data_msg .= "<td>{$row['sku2']}</td>".chr(13);
			$data_msg .= "<td>{$row['name']}</td>".chr(13);
			$data_msg .= "<td>{$row['entity']}</td>".chr(13);
			$data_msg .= "<td>".DIO_CURRENCY.addCommas($row['price'])."</td>".chr(13);
			$data_msg .= "<td>".DIO_CURRENCY.addCommas($row['total'])."</td>".chr(13);
			$data_msg .= "</tr>".chr(13);

			$entity = $entity + $row['entity'];
		}
		 
		//促銷
		foreach ( $data['query_promo'] as $row){
			$data_promo .= "<tr>".chr(13);
			$data_promo .= "<td>{$row['name']}({$row['item_type']})</td>".chr(13);
			$data_promo .= "<td>&nbsp;</td>".chr(13);
			$data_promo .= "<td>&nbsp;</td>".chr(13);
			$data_promo .= "<td>&nbsp;</td>".chr(13);		
			$data_promo .= "<td>".DIO_CURRENCY.addCommas($row['total'])."</td>".chr(13);
			$data_promo .= "</tr>".chr(13);
		}
		
		$content = "<table border='1'>
                      <tr>
                         <td>商品型號</td>
                         <td>名稱</td> 
                         <td>數量</td>
                         <td>單價</td>
                         <td>金額</td>                         
                      </tr>
                      $data_msg
                      $data_promo   
                      <tr>
			        	<td colspan=\"5\" align=\"right\">
					                   總共<span>{$entity}</span>件商品  , 消費總計<span>".DIO_CURRENCY.addCommas($data['query_master']['total'])."</span>
				        </td>
				      </tr>
			
				      <tr>
				        <td colspan=\"5\" style=\"font-size: 14px; line-height: 22px;\">
					       ■更多訂單資訊查詢，請至<a href=\"{$dio_webpage}/frontend/sign_in/login\" target=\"_new\">「會員中心」</a>查詢。<br/>
				   		</td>
			        </tr>
			        
			        </tr>      
                    </table>";   	
                       
                      $message = <<<EOD
    
<html>
<head>
	<style type="text/css">
		table{
			border-collapse: collapse;
			width: 100%;
		}
		th{	padding-left: 10px;
			font-weight: bold;
			line-height: 30px;
			text-align: left;
			border: 1px solid #D7D4D4;
		}
		td{
			padding-left: 10px;
			border: 1px solid #D7D4D4;
		}
		th{
			background-color: #F0F0F0;
		}
		span{
			color: #F00;
		}
	</style>
</head>
	
<body>
	<img src="http://www.fbline.com.tw/application/views/frontend/images/ct88logov2.jpg">
	<p>感謝您對本公司商品的喜愛，我們將盡快處理你的訂單。</p>
	<table border="1">
		<tr>
			<th>訂單內容</th>
		</tr>
		<tr>
			<td>
				<b>訂單編號:</b>$order_show_id<br/>
				<b>購買日期:</b>$order_date<br/>	
				<b>狀態:</b>$order_status<br/>	
			</td>			
		</tr>
	</table>
	<br/>
	$content
</body>
</html>
    	
				
EOD;
                      
	// Always set content-type when sending HTML email
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= "From: ".DIO_WEBNAME."<".DIO_EMAIL.">" . "\r\n";

	if( @mail($to, $subject, $message, $headers) ){
		return true;
	}else{
		return false;
	}

	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 寄發 EDM
	 *
	 * @access	public
	 * @param	
	 * @return
	 */
	public function edm($data_email){
		
	  $to      = $data_email['email_to']; //會員email
	  $subject = "=?UTF-8?B?".base64_encode( $data_email['title'] )."?="; //主題
		
	  //內容
	  $message = <<<EOD
<html>
<head>
<title>{$data_email['title']}</title>
</head>
<body>{$data_email['content']}</body>
</html>
	
EOD;
		
	   // Always set content-type when sending HTML email
	   $headers = "MIME-Version: 1.0" . "\r\n";
	   $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	   $headers .= "From: " .DIO_WEBNAME. "<" .DIO_EMAIL. ">" . "\r\n";
	
	   foreach ($to as $row){
	   	  if( mail($to, $subject, $message, $headers) ){
	   		//return true;
	   	  }else{
	   		return false;
	   	  }
	   }
	
	}

}


/* End of file email.php */