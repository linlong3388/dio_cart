<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 類別功能 : 購物車(Cart)。
 *
 * 說明 : 用 SESSION 暫存購物資訊。
 * 格式 : 資料傳入格式為陣列，產品編號(product_id)、名稱(name)、數量(entity)、價格(price)、小計(total)。
 *
 * @controllerName cart
 * @author Dio
 *
 */
class Cart{

	/**
	 * 方法 : 加入項目
	 * 說明 : 若項目相同進行累加，否則新增一筆項目。
	 *
	 * @access	public
	 * @param	$data
	 * @return
	 */
	public function add( $data ){
			
		if(isset($_SESSION['my_order']['cart'])){

			//--------------------------------------------------
			//
			// 重設陣列索引
			//
			// 基於效能考量，[刪除]/[編輯]需要依此索引進行異動，可
			// 免除陣列走訪的運算開銷。
			//
			//--------------------------------------------------
			$_SESSION['my_order']['cart'] = array_values($_SESSION['my_order']['cart']);

			$temp_product_id = array();
			//$temp_product_speci_id = array();

			for($i=0 ; $i < COUNT($_SESSION['my_order']['cart']) ; $i++){
					
				array_push($temp_product_id, $_SESSION['my_order']['cart'][$i]['product_id']);
				//array_push($temp_product_speci_id, $_SESSION['my_order']['cart'][$i]['product_speci_id']);
					
				//如果商品相同
				//if( ($data['product_id'] == $_SESSION['my_order']['cart'][$i]['product_id']) && ($data['product_speci_id'] == $_SESSION['my_order']['cart'][$i]['product_speci_id']) ){
				 if( ($data['product_id'] == $_SESSION['my_order']['cart'][$i]['product_id']) ){
				
					//原有商品累加
					$entity = $_SESSION['my_order']['cart'][$i]['entity'] + $data['entity'];
					$total = $_SESSION['my_order']['cart'][$i]['price'] * $entity;

					//複寫SESSION
					$_SESSION['my_order']['cart'][$i]['entity'] = ($entity < 0 ) ? 0 : $entity ;
					$_SESSION['my_order']['cart'][$i]['total'] = ($total < 0 ) ? 0 : $total ;

				}
			}

			//新增一筆商品
			if( !in_array($data['product_id'], $temp_product_id) ){
				$_SESSION['my_order']['cart'][] = $data;

			}/*else{
				if( !in_array($data['product_speci_id'], $temp_product_speci_id)){
					$_SESSION['my_order']['cart'][] = $data;
				}
			}*/


		}else{
			$_SESSION['my_order']['cart'][] = $data;
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 選取所有項目
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function select_all(){

		if(isset($_SESSION['my_order']['cart'])){
			return  $_SESSION['my_order']['cart'];
		}else{
			return false;
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 選取單筆項目
	 *
	 * @access	public
	 * @param	$data
	 * @return
	 */

	public function select($id){

		if(isset($_SESSION['my_order']['cart'])){
			return $_SESSION['my_order']['cart'][$id];
		}else{
			return false;
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 選取單筆項目(依商品ID)
	 *
	 * @access	public
	 * @param	$data
	 * @return
	 */
	public function selectByProductId( $product_id ){
	
		foreach ($_SESSION['my_order']['cart'] as $key=>$row){
			if($row['product_id'] == $product_id){
				$this->select($key);
			}
		}
	
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 計算所有項目(總數)
	 *
	 * @access	public
	 * @param	$data
	 * @return
	 */
	public function count_all_entity(){

		$count = 0;

		if(isset($_SESSION['my_order']['cart'])){
			foreach ($_SESSION['my_order']['cart'] as $row) {
				$count = $count + $row['entity'];
			}
			return $count;
		}else{
			return 0;
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 計算所有項目(總額)
	 *
	 * @access	public
	 * @param	$data
	 * @return
	 */
	public function count_all_total(){

		$count = 0;

		if(isset($_SESSION['my_order']['cart'])){
			foreach ($_SESSION['my_order']['cart'] as $row) {
				$count = $count + $row['total'];
			}
			return $count;
		}else{
			return 0;
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯項目
	 *
	 * @access	public
	 * @param	$data
	 * @return
	 */
	public function edit($id ,$entity){
		
		if(isset($_SESSION['my_order']['cart'])){

			if($entity <= 0){
				$this->del_id($id);
			}else{
				//原有商品累加
				$total = $_SESSION['my_order']['cart'][$id]['price'] * $entity;

				//複寫SESSION
			    $_SESSION['my_order']['cart'][$id]['entity'] = ($entity < 0 ) ? 0 : $entity ;
			    $_SESSION['my_order']['cart'][$id]['total'] = ($total < 0 ) ? 0 : $total ;
			}
			
			return true;
		}else{
			return false;
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 編輯項目(依商品ID)
	 *
	 * @access	public
	 * @param	$data
	 * @return
	 */
	public function editByProductId($product_id ,$entity){
		
		foreach ($_SESSION['my_order']['cart'] as $key=>$row){
			if($row['product_id'] == $product_id){
				return $this->edit($key, $entity);
			}
		}
		
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯項目(含金額)
	 *
	 * @access	public
	 * @param	$data
	 * @return
	 */
	public function edit2($id ,$entity ,$price){

		if(isset($_SESSION['my_order']['cart'])){

			if($entity <= 0){
				$this->delete($id);
			}else{
				//更改金額
				$_SESSION['my_order']['cart'][$id]['price'] = $price;

				//原有商品累加
				$total = $_SESSION['my_order']['cart'][$id]['price'] * $entity;

				//複寫SESSION
			 $_SESSION['my_order']['cart'][$id]['entity'] = ($entity < 0 ) ? 0 : $entity ;
			 $_SESSION['my_order']['cart'][$id]['total'] = ($total < 0 ) ? 0 : $total ;
			}
		}else{
			return false;
		}
	}


	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯付款方法
	 *
	 * @access	public
	 * @param	$data
	 * @return
	 */
	public function edit_pay_method($id ,$pay_method){

		if(isset($_SESSION['my_order']['cart'])){

			//複寫SESSION
			$_SESSION['my_order']['cart'][$id]['pay_method'] = $pay_method ;
				
		}else{
			return false;
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 刪除項目(單筆)
	 * 說明 : 依陣列ID，效能較高
	 * @access	public
	 * @param
	 * @return
	 */
	public function del_id( $id ){
		
		if(isset($_SESSION['my_order']['cart'])){
			unset($_SESSION['my_order']['cart'][$id]);
			
			// 重設陣列索引
			$_SESSION['my_order']['cart'] = array_values($_SESSION['my_order']['cart']);

			return true;
		}else{
			return false;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 刪除項目(單筆)
	 * 說明 : 依sku值，系統清晰
	 * @access	public
	 * @param
	 * @return
	 */
	public function del_sku( $sku2 ){
		
		if(isset($_SESSION['my_order']['cart'])){
         	
			foreach ($_SESSION['my_order']['cart'] as $key=>$val) {
				if($val['sku2'] == $sku2){
					unset($_SESSION['my_order']['cart'][$key]);
				}
			}
			
			// 重設陣列索引
			$_SESSION['my_order']['cart'] = array_values($_SESSION['my_order']['cart']);
			
			return true;
		}else{
			return false;
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 刪除項目(多筆)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function delete_multi( $aryList ){

		$aryList = explode(',', $aryList);

		if(isset($_SESSION['my_order']['cart'])){
			foreach ($aryList as $key=>$val){
				unset($_SESSION['my_order']['cart'][$val]);
			}
			// 重設陣列索引
			$_SESSION['my_order']['cart'] = array_values($_SESSION['my_order']['cart']);
		}else{
			return false;
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 判斷商品ID是否存在購物車
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function isProductIDExistCart( $product_id ){

		if(isset($_SESSION['my_order']['cart'])){

			foreach ($_SESSION['my_order']['cart'] as $row) {
				if($row['product_id'] == $product_id ) 
					return true;
			}
		}
			
		return false;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 依商品ID取得該筆購物車資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getCartInfoByProductID( $product_id ){
	
		if(isset($_SESSION['my_order']['cart'])){
	
			foreach ($_SESSION['my_order']['cart'] as $key=>$val) {
				if($val['product_id'] == $product_id )
					return $_SESSION['my_order']['cart'][$key];
			}
		}
			
		return false;
	}


	//********************************************************************************************************
	//
	// 以下為訂單的方法
	//
	//********************************************************************************************************
	/**
	 * 方法 : 建立訂單
	 *
	 * @access	public
	 * @param	$data
	 * @return
	 */
	public function add_order($data){
		$_SESSION['my_order']['info'] = $data;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 選取所有訂單資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function select_all_order(){

		if(isset($_SESSION['my_order'])){
			return  $_SESSION['my_order'];
		}else{
			return false;
		}
	}


	//********************************************************************************************************
	//
	// 以下為收藏的方法
	//
	//********************************************************************************************************
	/**
	 * 方法 : 計算所有收藏(總數)
	 *
	 * @access	public
	 * @param	$data
	 * @return
	 */
	public function count_all_entity_for_favorites(){

		$count = 0;

		if(isset($_SESSION['customer_info']['customer_id'])){
			$this->CI =& get_instance();

			$this->CI->db->where('customer_id' ,$_SESSION['customer_info']['customer_id']);
			$count = $this->CI->db->count_all_results('customer_favorites');
		}

		return $count;
	}


}


/* End of file cart.php */