<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 類別功能 : 商品批次上傳類別(店家端)。
 *

 * @controllerName Product_import_csv
 * @author Dio
 *
 */
class Product_import_csv{

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		$this->CI =& get_instance();
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 商品批次上傳
	 * 說明 : 欄位格式
	 *       *商品分類代號  |	*商品名稱 | 型號 | *價格 | 詳細描述 | (主)圖片 | *商品ID1 | *商品ID2 | *商品ID3 | *商品ID4 | *狀態
	 *            [0]          [1]     [2]    [3]     [4]        [5]      [6]         [7]       [8]        [9]      [10]
	 *         *子項名稱      |  *庫存量
	 *            [11]        [12]
	 * @access	public
	 * @param
	 * @return
	 */
	public function import($file_path){

		$i=0;
		$product_id = 0;

		//----------------------------------------------------------------------------------
		// 資料寫入
		//----------------------------------------------------------------------------------
		$handle = fopen($file_path,"r");

		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				
			if(!empty($data[0]) && !empty($data[1]) && !empty($data[3]) && !(empty($data[10]) && $data[10] !== '0') && !empty($data[11]) && !empty($data[12]) && (!empty($data[6]) || !empty($data[7]) || !empty($data[8]) || !empty($data[9])) ){
				 
				if($i>0){ //忽略抬頭

					//圖檔不可為空值，否則會刪除 /folder 資料夾
					$image = empty($data[5]) ? 'fbLine_empty.png' : $data[5];
					 
					$master_data = array(
                                          'vendor_id' => 0 ,         //店家編號
                                        'category_id' => $data[0] ,  //商品分類ID	 
                                   'category_func_id' => 0 ,         //功能分類	
                                           'brand_id' => 0 ,         //品牌ID 
                                               'name' => mb_convert_encoding($data[1], "UTF-8", "Big5") , //商品名稱
                                              'model' => mb_convert_encoding($data[2], "UTF-8", "Big5") ,  //型號
                                              'brief' => '' ,        //規格簡述	  
                                          'promo_msg' => '' ,        //促銷訊息
                                              'price' => $data[3] ,  //價格      
                                        'price_promo' => 0 ,         //促銷價格 	
                                           'checkout' => 'bonus,staging_3,staging_6' , //結帳方式 
                                       'bonus_redeem' => 0 ,         // 紅利兌換 
                                           'delivery' => 0 ,         //配送方式
                                          'order_max' => 0 ,         //最大購買量
                                               'memo' => 0 ,         //說明	
                                        'description' => mb_convert_encoding($data[4], "UTF-8", "Big5"), //詳細描述	
                             'related_specifications' => 0 ,         //相關規格
                                           'returned' => 0 ,         //退換貨須知
                                          'stock_min' => 0 ,         //最低庫存 
			                               	  'image' => $image,          //主圖
			    	                         'image1' => 'fbLine_empty.png', //圖片1
			    	                         'image2' => 'fbLine_empty.png', //圖片2
			    	                         'image3' => 'fbLine_empty.png', //圖片3
	                                            'ID1' => mb_convert_encoding($data[6], "UTF-8", "Big5"),  //商品ID1
			    	                            'ID2' => mb_convert_encoding($data[7], "UTF-8", "Big5"),  //商品ID2
			    	                            'ID3' => mb_convert_encoding($data[8], "UTF-8", "Big5"),  //商品ID3
			    	                            'ID4' => mb_convert_encoding($data[9], "UTF-8", "Big5"),  //商品ID4
			                               	 'status' => $data[10] ,  //上架
			    	                          'cdate' => date('Y-m-d H:i:s') , //建立日期
			                                  'udate' => date('Y-m-d H:i:s')   //更新日期
					);

					$this->CI->db->insert('product' ,$master_data);
					 
					//更新全域變數
					$product_id = $this->CI->db->insert_id();
			   
					//更新副表
					$detail_data = array(
			                       'product_id' => $product_id,   //商品ID	
			                             'name' => mb_convert_encoding($data[11], "UTF-8", "Big5"), //子項名稱	
			       	                'inventory' => $data[12] ,    //庫存量	
			                            'cdate' => date('Y-m-d H:i:s')//建立日期	
					);

					$this->CI->db->insert('product_speci' ,$detail_data);
				}

				$i=1;
					
			}
		}

	}


}


/* End of file Product_import_csv.php */