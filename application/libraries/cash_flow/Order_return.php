<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 類別功能 : 結帳完成，一般來說泛指結束金流後續該處理的事宜。
 * 說明:     需完成的任務如下 :
 *	          -- 異動訂單狀態為成功(1)
 *	          -- 返回完成訊息成功或失敗
 *            -- 寄發EMAIL
 *            -- 清除會員購買暫存記錄SESSION
 *
 * @controllerName Order_return
 * @author Dio
 *
 */
class Order_return{

	private $order_id;

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){

		$this->CI =& get_instance();

		$this->CI->load->library(array('Order_status','Dio_gmail'));
		$this->CI->load->model("frontend/order_model","order");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : setParams
	 * 說明 : 設置參數。
	 *
	 * @access	public
	 * @param	[order_id]
	 * @return
	 */
	public function setParams($order_id){

		$this->order_id = $order_id;
	}

	// --------------------------------------------------------------------
	 
	/**
	 * 方法 : updateStatus
	 * 說明 : 異動訂單狀態為成功(1)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function updateStatus(){

		//設置參數
		$status = 1;

		$order_status = new Order_status();
			
		return $order_status->change_status($this->order_id, $status);
	}

	// --------------------------------------------------------------------
	 
	/**
	 * 方法 : sendMail
	 * 說明 : 寄發EMAIL
	 *
	 * @access	public
	 * @param	[order_id]
	 * @return
	 */
	public function sendMail(){

		//主單資料
		$order = $this->CI->order->shr_order_customer($this->order_id);

		//明細資料
		$order_detail = $this->CI->order->shr_order_detail($this->order_id);

		//更新發送狀態，避免重複發送
		if($order['is_email_order'] == 0){
			 
			$order_email = new Dio_gmail();
			$order_email->order($order ,$order_detail);
			 
			$this->CI->db->where('order_id' ,$this->order_id);
			$this->CI->db->update('order' ,array('is_email_order' => '1'));
		}

	}

	// --------------------------------------------------------------------
	 
	/**
	 * 方法 : clearOrderInfo
	 * 說明 : 清除會員購買暫存記錄SESSION
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function clearOrderInfo(){

		unset($_SESSION['my_order']);
	}

}


/* End of file Order_return.php */