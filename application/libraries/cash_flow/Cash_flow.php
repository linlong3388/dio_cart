<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 類別功能 : 金流的抽象父類別。
 * 說明 : 金流常用的共同方法獨立出來做成一個父階類別，並採用抽象方法，以確保子階都能統一實作所有方法
 *       ，可視為金流子類別的設計藍圖。
 *
 * @controllerName cash_flow
 * @author Dio
 *
 */

//引入金流子類別
require_once(APPPATH.'libraries/cash_flow/First_Bank/First_Bank_Atm.php');

require_once(APPPATH.'libraries/cash_flow/Fisc/Fisc_Credit.php');

require_once(APPPATH.'libraries/cash_flow/Allpay/Dio_Allpay_Atm.php');
require_once(APPPATH.'libraries/cash_flow/Allpay/Dio_Allpay_Credit.php');

require_once(APPPATH.'libraries/cash_flow/No_Flow/No_Flow_Atm.php');
require_once(APPPATH.'libraries/cash_flow/No_Flow/No_Flow_Cod.php');

require_once(APPPATH.'libraries/cash_flow/Ecpg/Ecpg_Credit.php');

require_once(APPPATH.'libraries/cash_flow/Chb/Chb_Atm_Uni.php');

require_once(APPPATH.'libraries/cash_flow/ECPay/Dio_ECPay_Credit.php');
require_once(APPPATH.'libraries/cash_flow/ECPay/Dio_ECPay_Cvs.php');
require_once(APPPATH.'libraries/cash_flow/ECPay/Dio_ECPay_Logistics_UNIMART.php');

require_once(APPPATH.'libraries/cash_flow/Dio_Process.php');

abstract class Cash_flow {

	//-----------------------------------------------------------------------------
	//
	// 傳遞至遠端主機的必填參數，共分為兩部分[固定屬性]和[非固定屬性]。
	// 固定屬性 : 會在子類的建構方法(__construct())進行設置
	// 非固定屬性 : 會在子類的setParams方法 進行設置
	//
	//-----------------------------------------------------------------------------
	protected $param;


	//-----------------------------------------------------------------------------
	//
	// 在類別初始化時，設置[固定屬性]值，如:主機URL、商家編號、密碼...等。
	//
	//-----------------------------------------------------------------------------
	abstract protected function __construct();


	//-----------------------------------------------------------------------------
	//
	// 設置[非固定屬性]值，如:訂單編號、訂購金額、數量...等。
	// 傳入值 : 陣列
	//
	//-----------------------------------------------------------------------------
	abstract protected function setParams(array $param);


	//-----------------------------------------------------------------------------
	//
	// 取得所有傳遞至遠端主機的參數，可以隨時列印出來，確定傳遞參數是否正確。
	//
	//-----------------------------------------------------------------------------
	abstract protected function getParams();
	 

	//-----------------------------------------------------------------------------
	//
	// 傳送資料到遠端主機。
	//
	//-----------------------------------------------------------------------------
	abstract protected function dataTransport();


	//-----------------------------------------------------------------------------
	//
	// 金流返回  : 檢查返回值是否正確(第一次)。
	// 傳入值 : 陣列
	// 返回型態 : T/F 或 array
	//
	//-----------------------------------------------------------------------------
	abstract protected function is_checkReturn($param);

	
	//-----------------------------------------------------------------------------
	//
	// 切換各種金流的方法。
	//
	//-----------------------------------------------------------------------------
	static function getInstance($cashName){
		
		switch ($cashName) {

			case 'First_Bank/First_Bank_Atm':
				return new First_Bank_Atm();
				break;
				
			case 'Fisc/Fisc_Credit':
				return new Fisc_Credit();
				break;
				
			case 'Allpay/Dio_Allpay_Atm':
				return new Dio_Allpay_Atm();
				break;
					
			case 'Allpay/Dio_Allpay_Credit':
				return new Dio_Allpay_Credit();
				break;
				
			case 'No_Flow/No_Flow_Cod':
				return new No_Flow_Cod();
				break;	
				
			case 'No_Flow/No_Flow_Atm':
				return new No_Flow_Atm();
				break;
				
			case 'Ecpg/Ecpg_Credit':
				return new Ecpg_Credit();
				break;
				
			case 'ECPay/Dio_ECPay_Credit':
				return new Dio_ECPay_Credit();
				break;
				
			case 'ECPay/Dio_ECPay_Cvs':
				return new Dio_ECPay_Cvs();
				break;				
				
			case 'ECPay/Dio_ECPay_Logistics_UNIMART':
				return new Dio_ECPay_Logistics_UNIMART();
				break;
				
			case 'Chb/Chb_Atm_Uni':
				return new Chb_Atm_Uni();
				break;
				
			case 'Dio_Process':
				return new Dio_Process();
				break;
		}
	}

}


/* End of file Cash_flow.php */