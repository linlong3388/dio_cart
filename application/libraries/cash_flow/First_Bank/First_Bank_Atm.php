<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 第一銀行 ATM 串接
 *     1) 需 ATM 讀卡機
 *     2) 測試環境一律返回失敗，需由對方幫忙測試返回成功
 */
class First_Bank_Atm extends Cash_flow{

	protected $param;

	/**
	 * 建構方法 : 成員和物件初始化
	 * 說明           : 設置[固定屬性]值
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		
		$this->CI =& get_instance();
		$this->CI->load->helper(array('url','dio_url'));
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : setParams
	 * 說明 : 設置串接參數。
	 * 傳入參數 : $param['info']
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function setParams(array $param){

		//$this->param['url']         = 'https://teatm.firstbank.com.tw/acq/cardpay'; //測試
		$this->param['url']         = 'https://eatm.firstbank.com.tw/acq/cardpay'; //正式
		$this->param['KEY']         = 'D2J2F2ST5ACAJRF9E3AMMZXM';

		$this->param['SendSeqNo']   = $param['order_show_id'];
		$this->param['MID']         = 'T00128920121';
		$this->param['FunCode']     = '';
		$this->param['UserData']    = DIO_COMPANY;
		$this->param['ONO']         = $param['order_show_id'];
		//$this->param['InAccountNo'] = '04850002818'; //測試
		$this->param['InAccountNo'] = '40310051261'; //正式
		$this->param['Amount']      = $param['total'];
		$this->param['MAC']         = $this->getMacValue();
		$this->param['RsURL']       = getUserURL('checkout/step5');
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : getParams
	 * 說明 : 取得轉帳資料。
	 *
	 * @access	public
	 * @param
	 * @return array
	 */
	public function getParams(){

		return $this->param;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : dataTransport
	 * 說明 : 傳送資料到遠端主機。
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function dataTransport(){
	
$str = <<<EOD
<?xml version="1.0" encoding="Big5"?>
<CardPayRq>
<SendSeqNo>{$this->param['SendSeqNo']}</SendSeqNo>
<MID>{$this->param['MID']}</MID>
<FunCode>{$this->param['FunCode']}</FunCode>
<UserData>{$this->param['UserData']}</UserData>
<ONO>{$this->param['ONO']}</ONO>
<InAccountNo>{$this->param['InAccountNo']}</InAccountNo>
<Amount>{$this->param['Amount']}</Amount>
<MAC>{$this->param['MAC']}</MAC>
<RsURL>{$this->param['RsURL']}</RsURL>
</CardPayRq>  	
EOD;

		$data = base64_encode($str);
		
		$str2 = <<<EOD2

		<b>資料處理中，請稍候...</b>
		
		<form style="display:none" id="first_bank_form" method="POST" action="{$this->param['url']}">
		<textarea rows="20" cols="100" name="CardPayRq">{$data}</textarea>
		 <br/>
		 <input type="submit" value="post"/>
		</form>
		
		<script>
           document.getElementById("first_bank_form").submit();
        </script>
EOD2;
		
		echo $str2;		
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : getMacValue
	 * 說明 : 產生檢查碼
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getMacValue()
	{
	   $iv         = "00000000";
       $text       = $this->param['SendSeqNo'].$this->param['MID'].$this->param['ONO'].$this->param['InAccountNo'].$this->param['Amount'];
       $sha1_text  = sha1($text).$iv;
       $sha1_text  = pack('H*',$sha1_text);
       $crypt_text = mcrypt_encrypt(MCRYPT_tripledes, $this->param['KEY'], $sha1_text, MCRYPT_MODE_CBC, $iv);
       bin2hex($crypt_text);

       return base64_encode($crypt_text);
	}
	 
	// --------------------------------------------------------------------

	/**
	 * 方法 : is_checkReturn
	 * 說明 : 檢查返回值是否正確(第一次)。
	 *
	 * @access	public
	 * @param	  t/f
	 * @return    t/f
	 */
	public function is_checkReturn($param){

		$CardPayRs = base64_decode($param);
		
		$xml = simplexml_load_string($CardPayRs);
		$xml_RC = (string)$xml->RC ;
		
		if($xml_RC == '0'){
			return true;
		}else{
			return false;
		}
	}
	
}


/* End of file First_Bank_Atm.php */