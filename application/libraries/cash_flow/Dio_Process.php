<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *  將金流共用的部分抽離出來獨立處理，可供處理的工作事項如下 :
 *     更新訂單狀態為1
 *     更新促銷活動狀態為1
 *     寄送Email
 *	        客制商務邏輯
 *     將金流訊息回寫到資料庫
 *     撈取這次的訂單資料
 *  
 * @controllerName Dio_Process
 * @author Dio
 *
 */
class Dio_Process{

	private $order_id;

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){

		$this->CI =& get_instance();

		$this->CI->load->library(array('Order_status','Dio_gmail','promo/Point','promo/Coupon'));
		$this->CI->load->model("frontend/order_model","order");
		
		//發送Email
		$this->dio_email = new Dio_gmail();
		
		//取得折價券
		$this->coupon    = new coupon();
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : updateOrderStatus
	 * 說明 : 更新訂單狀態。
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function updateOrderStatus($order_show_id ,$status = 1){
	
		$this->CI->db->where('order_show_id' ,$order_show_id);
		$this->CI->db->update('order' ,array('status' => $status));
	}
	
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : BusinessLogic
	 * 說明 : 商務邏輯。
	 *
	 *    昇級VIP會員等級
	 *    取得折價券
	 *    改變折價券狀態
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function BusinessLogic(array $param){

		//判斷並寫入新的折價券
		if( $this->coupon->is_coupon() ){
			$this->coupon->write($param['order_id']);
		}
		
		//改變已使用的折價券狀態
		if( !empty($param['code_use']) ){
			$this->coupon->statusByField(array('code_use' => $param['code_use']) ,0);
		}
		
		//取得還原金
		/*
		$point = new Point();
		$point->write($param['order_id']);
		*/
		
		/*
		$this->CI->load->library(array('promo/VIP'));
		$this->CI->load->library(array('promo/Coupon'));
	
		//VIP會員昇級
		$vip = new VIP();
		if($vip->is_up( $param['total']) ){
			$vip->up();
		}
	    */
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : saveReturnParams
	 * 說明 : 將返回參數回寫資料庫。
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function saveReturnParams(array $param){
	 
		/*
		if( $this->is_order_id_dupli($param['order_show_id']) ){
			$this->CI->db->insert('order_cash_return_info' ,$param);
		}
		
		$this->CI->db->where('order_id' ,$param['order_id']);
		$this->CI->db->update('order' ,array('pay_method_content' => $param['pay_method_content']));
		*/
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : sendEmail
	 * 說明 : 寄送Email。
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function sendEmail(array $param){
	
		if( $param['order']['master']['is_email_order'] == 0){
	
			$this->dio_email->order( $param );
		
			//更新發送狀態，避免重複發送
			$this->CI->db->where('order_show_id' ,$param['order']['master']['order_show_id']);
			$this->CI->db->update('order' ,array('is_email_order' => '1'));
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : updateReturnStock
	 * 說明 : 更新庫存量。
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function updateReturnStock(array $param){
		  
		foreach ($param['order']['detail'] as $row){
				
			$this->CI->db->set('stock', "`stock` - {$row['entity']}" ,false);
			$this->CI->db->where('product_id' ,$row['product_id']);
			$this->CI->db->update('product_speci');
		}
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : cleanSession
	 * 說明 : 清空購物車的 SESSION 資料。
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function cleanSession(){
		
		unset($_SESSION['page']);
		unset($_SESSION['my_order']);
	}
	

}


/* End of file Dio_Process.php */