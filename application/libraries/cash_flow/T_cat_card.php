<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 類別功能 : 黑貓宅急便 (信用卡)類別。
 * 說明 : T_cat_card 的金流設置，使用步驟和方式為:
 *         Step1 : 修改 __construct 建構函數的參數配置。
 *         Step2 : 呼叫 setParams()方法，並填入必填參數。
 *         Step3 : 呼叫 dataTransport()方法，傳送資料。
 *         Step4 : 完成。
 *
 * 官網串接資料
 *   https://#
 *
 * @controllerName T_cat_card
 * @author Dio
 *
 */
class T_cat_card extends Cash_flow{

	protected $param;

	/**
	 * 建構方法 : 成員和物件初始化
	 * 說明           : 設置[固定屬性]值
	 *               url = 交易網址(必填)
               hash_base = (必填)
	 *           link_id = (必填)
	 *     
	 * 每建構一套新系統，都要這裡重新確認參數資料是否正確，如契客代號
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		$this->param['url']           = 'https://4128888card.com.tw/cocs/client_order_append.php';
		$this->param['hash_base']     = '0uuxPlBCPFlwv0GU';
		$this->param['link_id']       = 'BP70RNEhuKsb';		                                 
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : setParams
	 * 說明 : 設置[非固定屬性]值，如:訂單編號、訂購金額、數量...等。:
	 *       cust_order_no     = 契客訂單號碼
	 *       order_amount      = 金額
	 *       order_detail      = 訂單明細
	 *       limit_product_id  = 限定產品別(繳款類型)
	 *       connect_interface = 連線操作介面(一般/刷卡介面)
	 *       send_time         = 傳送時間
	 *       chk               = 檢核碼
	 *       return_type       = 回傳方式
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function setParams(array $param){
		
		$send_time = date('Y-m-d H:i:s');
		$order_amount = intval(round($param['total']));
		$chk = md5($this->param['hash_base'].'$'.$order_amount.'$'.$send_time);
		
		$this->param['cust_order_no']      = $param['order_show_id'];
		$this->param['order_amount']       = $order_amount;
		$this->param['order_detail']       = $param['order_detail']; 
		$this->param['limit_product_id']   = 'esun.normal';
		$this->param['connect_interface']  = '1';
		$this->param['send_time']          = $send_time;
		$this->param['chk']                = $chk;
		$this->param['return_type']        = 'redirect';
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : getParams
	 * 說明 : 取得所有傳遞至遠端主機的參數，可以隨時列印出來，確定傳遞參數是否正確。
	 *
	 * @access	public
	 * @param
	 * @return array
	 */
	public function getParams(){

		return $this->param;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : dataTransport
	 * 說明 : 傳送資料到遠端主機，採用隱藏欄位並搭配JS進行資料POST傳遞。
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function dataTransport(){

		$str = <<<EOD
                 <form id='T_cat_card_form' action="{$this->param['url']}" method="post">
                    <input type="hidden" name="link_id" value="{$this->param['link_id']}"/>
                    <input type="hidden" name="cust_order_no" value="{$this->param['cust_order_no']}"/>
                    <input type="hidden" name="order_amount" value="{$this->param['order_amount']}"/>
                    <input type="hidden" name="order_detail" value="{$this->param['order_detail']}"/>
                    <input type="hidden" name="limit_product_id" value="{$this->param['limit_product_id']}"/>
                    <input type="hidden" name="connect_interface" value="{$this->param['connect_interface']}"/>
               		<input type="hidden" name="send_time" value="{$this->param['send_time']}"/>
             		<input type="hidden" name="chk" value="{$this->param['chk']}"/>
             		<input type="hidden" name="return_type" value="{$this->param['return_type']}"/>
                    <input style="display:none" type="submit"/>
                 </form> 

                 <script>
                    document.getElementById("T_cat_card_form").submit();
                 </script>
EOD;
		
		echo $str;
	}
	 
	// --------------------------------------------------------------------

	/**
	 * 方法 : is_checkReturn
	 * 說明 : 檢查返回值是否正確(第一次)。
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function is_checkReturn($param){
		//驗證成功
		if($param['sn_id'] != "00000000")
		return true;
		else
		return false;
	}

}


/* End of file T_cat_card.php */