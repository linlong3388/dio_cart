<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 類別功能 : 黑貓宅急便 (ATM)類別。
 * 說明 : T_cat_atm 的金流設置，使用步驟和方式為:
 *         Step1 : 修改 __construct 建構函數的參數配置。
 *         Step2 : 呼叫 setParams()方法，並填入必填參數。
 *         Step3 : 呼叫 dataTransport()方法，傳送資料。
 *         Step4 : 完成。
 *
 * 官網串接資料
 *   https://#
 *
 * @controllerName T_cat_atm
 * @author Dio
 *
 */
class T_cat_atm extends Cash_flow{

	protected $param;

	/**
	 * 建構方法 : 成員和物件初始化
	 * 說明           : 設置[固定屬性]值
	 *               url = 交易網址(必填)
	 *               cmd = 標頭(必填)
	 *           cust_id = 契客代號(必填)
	 *     cust_password = 契客登入密碼(必填)
	 *     
	 * 每建構一套新系統，都要這裡重新確認參數資料是否正確，如契客代號
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		$this->param['url']           = 'https://www.ccat.com.tw/cvs/ap_interface.php';
		$this->param['cmd']           = 'cvs_order_regiater';
		$this->param['cust_id']       = 'CV2482668402';
		$this->param['cust_password'] = 'CV2482668402OLI';
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : setParams
	 * 說明 : 設置[非固定屬性]值，如:訂單編號、訂購金額、數量...等。:
	 *       cust_order_number  = 契客訂單號碼(必填)
	 *       order_amount       = 繳款單金額(必填)
	 *       expire_date        = 繳款逾期時間(必填)
	 *       payer_name         = 繳款人姓名
	 *       payer_postcode     = 繳款人地址郵遞區號
	 *       payer_address      = 繳款人地址
	 *       payer_mobile       = 繳款人行動電話(必填)
	 *       payer_email        = 繳款人電子信箱(必填)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function setParams(array $param){
		$this->param['cust_order_number']  = $param['order_show_id'];
		$this->param['order_amount']       = intval(round($param['total']));
		$this->param['expire_date']        = date("Y-m-d H:i:s", strtotime("+3 day")); 
		$this->param['payer_name']         = $param['cname'];
		$this->param['payer_postcode']     = substr($param['clocal'] ,0,3);
		$this->param['payer_address']      = $param['clocal'].'/'.$param['caddress'];
		$this->param['payer_mobile']       = $param['cmobile'];
		$this->param['payer_email']        = $param['cemail'];
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : getParams
	 * 說明 : 取得所有傳遞至遠端主機的參數，可以隨時列印出來，確定傳遞參數是否正確。
	 *
	 * @access	public
	 * @param
	 * @return array
	 */
	public function getParams(){

		return $this->param;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : dataTransport
	 * 說明 : 傳送資料到遠端主機，採用隱藏欄位並搭配JS進行資料POST傳遞。
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function dataTransport(){

		$str = <<<EOD
                 <form id='T_cat_atm_form' action="{$this->param['url']}" method="post">
                    <input type="hidden" name="cmd" value="{$this->param['cmd']}"/>
                    <input type="hidden" name="cust_id" value="{$this->param['cust_id']}"/>
                    <input type="hidden" name="cust_password" value="{$this->param['cust_password']}"/>
                    <input type="hidden" name="cust_order_number" value="{$this->param['cust_order_number']}"/>
                    <input type="hidden" name="order_amount" value="{$this->param['order_amount']}"/>
                    <input type="hidden" name="expire_date" value="{$this->param['expire_date']}"/>
                    <input type="hidden" name="payer_name" value="{$this->param['payer_name']}"/>
                    <input type="hidden" name="payer_postcode" value="{$this->param['payer_postcode']}"/>
               		<input type="hidden" name="payer_address" value="{$this->param['payer_address']}"/>
             		<input type="hidden" name="payer_mobile" value="{$this->param['payer_mobile']}"/>
             		<input type="hidden" name="payer_email" value="{$this->param['payer_email']}"/>
                    <input style="display:none" type="submit"/>
                 </form> 

                 <script>
                    document.getElementById("T_cat_atm_form").submit();
                 </script>
EOD;
		
		echo $str;
	}
	 
	// --------------------------------------------------------------------

	/**
	 * 方法 : is_checkReturn
	 * 說明 : 檢查返回值是否正確(第一次)。
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function is_checkReturn($param){
		//驗證成功
		if($param['sn_id'] != "00000000")
		return true;
		else
		return false;
	}

}


/* End of file T_cat_atm.php */