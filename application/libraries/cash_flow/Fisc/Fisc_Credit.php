<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 財金公司 信用卡 串接
 *     測試環境只會返回成功狀態。
 *     正式環境會由該平台所提供的介面，進行驗證，一般來說，必須交易成功才會返回。
 *      
 *  官網 http://www.fisc.com.tw/
 */
class Fisc_Credit extends Cash_flow{

	protected $param;

	/**
	 * 建構方法 : 成員和物件初始化
	 * 說明           : 設置[固定屬性]值
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		
		$this->CI =& get_instance();
		$this->CI->load->helper(array('url','dio_url'));
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : setParams
	 * 說明 : 設置串接參數。
	 * 傳入參數 : $param['info']
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function setParams(array $param){

		$this->param['url']          = 'https://www.focas-test.fisc.com.tw/FOCAS_WEBPOS/online/'; //交易網址(測試)
		//$this->param['url']          = 'https://www.focas.fisc.com.tw/FOCAS_WEBPOS/online/'; //交易網址(正式)
		$this->param['merID']        = '53985083';        //特店網站之代碼
		$this->param['MerchantID']   = '009539850839001'; //特店代號
		$this->param['TerminalID']   = '90011801';        //機台代號
		$this->param['MerchantName'] = DIO_COMPANY;       //公司名稱
		//$this->param['customize']  =  //客製化授權頁
		$this->param['lidm']         = $param['order']['master']['order_show_id']; //交易訂單編號
		$this->param['purchAmt']     = $param['order']['master']['total'];         //授權之總金額
		$this->param['CurrencyNote'] = DIO_COMPANY;             //註記說明文字
		$this->param['AutoCap']      = 1;                       //是否轉入請款檔作業(1表示自動轉入請款單)
		$this->param['AuthResURL']   = getUserURL('checkout_return/fisc_Credit'); //回傳授權結果
		$this->param['PayType']      = 0;                       //交易類別(一般(0)、分期(1)、紅利(2))
		
		//$this->param['PeriodNum']    =      //分期交易之期數(PayType為分期必填)
		//$this->param['BonusActionCode']  = //紅利交易活動代碼(PayType為紅利必填)
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : getParams
	 * 說明 : 取得轉帳資料。
	 *
	 * @access	public
	 * @param
	 * @return array
	 */
	public function getParams(){

		return $this->param;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : dataTransport
	 * 說明 : 傳送資料到遠端主機。
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function dataTransport(){
	
		$str = <<<EOD
        <form id='fisc_form' action="{$this->param['url']}" method="post" accept-charset="big5">
          <input type='hidden' name='merID' value="{$this->param['merID']}"/>		
		  <input type='hidden' name='MerchantID' value="{$this->param['MerchantID']}"/>
		  <input type='hidden' name='TerminalID' value="{$this->param['TerminalID']}"/>
		  <input type='hidden' name='MerchantName' value="{$this->param['MerchantName']}"/>
		  <input type='hidden' name='lidm' value="{$this->param['lidm']}"/>
		  <input type='hidden' name='purchAmt' value="{$this->param['purchAmt']}"/>
		  <input type='hidden' name='CurrencyNote' value="{$this->param['CurrencyNote']}"/>
		  <input type='hidden' name='AutoCap' value="{$this->param['AutoCap']}"/>
		  <input type='hidden' name='AuthResURL' value="{$this->param['AuthResURL']}"/>
		  <input type='hidden' name='PayType' value="{$this->param['PayType']}"/>
 	      <input style="display:none" type="submit"/>         		
        </form>
		
          <script>
            document.getElementById("fisc_form").submit();
          </script>
EOD;
		
		echo $str;	
	}
	 
	// --------------------------------------------------------------------

	/**
	 * 方法 : is_checkReturn
	 * 說明 : 檢查返回值是否正確(第一次)。
	 *
	 * @access	public
	 * @param	  t/f
	 * @return    t/f
	 */
	public function is_checkReturn($param){
		
		if($param['status'] == 0 && !empty($param['authCode']) && !empty($param['xid'])){
			return true;
		}else{
			return false;
		}
		
	}
	
}


/* End of file Fisc_Credit.php */