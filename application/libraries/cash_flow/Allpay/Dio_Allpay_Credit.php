<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 類別功能 : 歐付寶/信用卡。
 * 說明 : allpay 的金流設置，使用步驟和方式為:
 *         Step1 : 修改 __construct 建構函數的參數配置。
 *         Step2 : 呼叫 setParams()方法，並填入必填參數。
 *         Step3 : 呼叫 dataTransport()方法，傳送資料。
 *         Step4 : 完成。
 *
 * 範例 :
 *   $allpay = new Allpay();
 *   $allpay->setParams(array('order_id' => '1',
 *                             'rv_name' => '皇大熊',
 *                            'rv_email' => 'linlong3388@gmail.com',
 *                            'rv_mobil' => '0923656954',
 *                           'rv_amount' => '110'
 *                     ));
 *	 $allpay->dataTransport();
 *
 * 官網串接資料
 *   https://#
 *
 * @controllerName Allpay_Credit
 * @author Dio
 *
 */
class Dio_Allpay_Credit extends Cash_flow{

	protected $param;

	/**
	 * 建構方法 : 成員和物件初始化
	 * 說明           : 設置[固定屬性]值
	 *
	 * 每建構一套新系統，都要這裡重新確認參數資料是否正確，如業主帳號
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		
		//載入歐付寶(AllInOne)模組
		require_once('Payment.Integration.php');
		
		$this->CI =& get_instance();
		
		$this->param['ServiceURL'] = 'http://payment-stage.allpay.com.tw/Cashier/AioCheckOut';
		$this->param['HashKey']    = '5294y06JbISpM5x9';
		$this->param['HashIV']     = 'v77hoKGq4kWxNNIS';
		$this->param['MerchantID'] = '2000132';
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : setParams
	 * 說明 : 設置[非固定屬性]值，如:訂單編號、訂購金額、數量...等
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function setParams(array $param){

		try
		{
			$oPayment = new AllInOne();
			
			/* 服務參數 */
			$oPayment->ServiceURL = $this->param['ServiceURL'];
			$oPayment->HashKey    = $this->param['HashKey'];
			$oPayment->HashIV     = $this->param['HashIV'];
			$oPayment->MerchantID = $this->param['MerchantID'];
						
			/* 基本參數 */
			$oPayment->Send['ReturnURL']         = getUserURL('checkout_return/pay_allpay_credit');
			$oPayment->Send['ClientBackURL']     = getUserURL('index/main');
			$oPayment->Send['OrderResultURL']    = getUserURL('checkout_return/pay_allpay_credit');
			$oPayment->Send['MerchantTradeNo']   = $param['order']['master']['order_show_id'];
			$oPayment->Send['MerchantTradeDate'] = date('Y/m/d H:i:s');
			$oPayment->Send['TotalAmount']       = (int) $param['order']['master']['total'];
			$oPayment->Send['TradeDesc']         = empty($param['order']['master']['memo']) ? '0' : $param['order']['master']['memo'];
			$oPayment->Send['ChoosePayment']     = PaymentMethod::Credit;
			$oPayment->Send['Remark']            = "";
			$oPayment->Send['ChooseSubPayment']  = PaymentMethodItem::None;
			$oPayment->Send['NeedExtraPaidInfo'] = ExtraPaymentInfo::No;
			$oPayment->Send['DeviceSource']      = DeviceType::PC;
			$oPayment->Send['UseRedeem']         = "N";
			
			// 加入選購商品資料。
			foreach ($param['order']['detail'] as $row) {
				array_push($oPayment->Send['Items'], array(
						'Name'     => $row['name'],
						'Price'    => (int)$row['price'],
						'Currency' => DIO_CURRENCY,
						'Quantity' => (int) $row['entity'],
						'URL'      => getUserURL('product/view?product_id='.$row['product_id']) 
					 ));	
			}                          
				
			/* ATM 延伸參數 */
			//$oPayment->SendExtend['ExpireDate']     = (int) "<<您允許的繳費有效天數>>";
			//$oPayment->SendExtend['PaymentInfoURL'] = "http://killbuy.jp/index.php/frontend/checkout/check_cashAtm_return";
			
			/* 產生訂單 */
			$oPayment->CheckOut();
			
			/* 產生訂單 Html Code 的方法 */
			$szHtml = $oPayment->CheckOutString();
		}
		
		catch (Exception $e)
		{
			// 例外錯誤處理。
			throw $e;
		}
		
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : getParams
	 * 說明 : 取得所有傳遞至遠端主機的參數，可以隨時列印出來，確定傳遞參數是否正確。
	 *
	 * @access	public
	 * @param
	 * @return array
	 */
	public function getParams(){
	
		return $this->param;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : dataTransport
	 * 說明 : 傳送資料到遠端主機，採用隱藏欄位並搭配JS進行資料POST傳遞。
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function dataTransport(){
		
		$html_input = "交易處理中，請勿關閉視窗...";
		
		foreach ($this->param as $key => $val) {
			$html_input .= "<input type='hidden' name='" . $key . "' value='" . $val . "'><BR>";
		}           
			
		$str = <<<EOD
                 <form id='allpay_form' action="{$this->gateway_url}" method="post">
                      $html_input
                    <input style="display:none" type="submit"/>
                 </form> 

                 <script>
                    document.getElementById("allpay_form").submit();
                 </script>
EOD;

		echo $str;
	}
	 
	// --------------------------------------------------------------------

	/**
	 * 方法 : is_checkReturn
	 * 說明 : 檢查返回值是否正確。
	 *    
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function is_checkReturn($param){
	
		try
		{
			$oPayment = new AllInOne();
			
			$oPayment->HashKey    = $this->param['HashKey'];
			$oPayment->HashIV     = $this->param['HashIV'];
			$oPayment->MerchantID = $this->param['MerchantID'];
			
			$arFeedback = $oPayment->CheckOutFeedback();
			
			$reData = array(); 
			
			// 檢核與變更訂單狀態 
			if (sizeof($arFeedback) > 0) {
				foreach ($arFeedback as $key => $value) {
					switch ($key)
					{
						case "MerchantID":           $reData['szMerchantID']           = $value; break;
                        case "MerchantTradeNo":      $reData['szMerchantTradeNo']      = $value; break;
                        case "PaymentDate":          $reData['szPaymentDate']          = $value; break;
                        case "PaymentType":          $reData['szPaymentType']          = $value; break;
                        case "PaymentTypeChargeFee": $reData['szPaymentTypeChargeFee'] = $value; break;    
                        case "RtnCode":              $reData['szRtnCode']              = $value; break;
                        case "RtnMsg":               $reData['szRtnMsg']               = $value; break;
                        case "SimulatePaid":         $reData['szSimulatePaid']         = $value; break;
                        case "TradeAmt":             $reData['szTradeAmt']             = $value; break;
                        case "TradeDate":            $reData['szTradeDate']            = $value; break;
                        case "TradeNo":              $reData['szTradeNo']              = $value; break;
                        case "PayAmt":               $reData['szPayAmt']               = $value; break;
                        case "RedeemAmt":            $reData['szRedeemAmt']            = $value; break;
                        default: break;
					}
				}
				
		       return $reData;		
				
			} else {
			   return array();
			}
		}
		catch (Exception $e)
		{
			// 例外錯誤處理。
			return array();
		}
		
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : _getMacValue
	 * 說明 : 產生檢查碼
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function _getMacValue($hash_key, $hash_iv, $form_array)
	{
		$encode_str = "HashKey=" . $hash_key;
		foreach ($form_array as $key => $value)
		{
			$encode_str .= "&" . $key . "=" . $value;
		}
		$encode_str .= "&HashIV=" . $hash_iv;
		$encode_str = strtolower(urlencode($encode_str));
		$encode_str = $this->_replaceChar($encode_str);
	
		return md5($encode_str);
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : _replaceChar
	 * 說明 : 特殊字元置換
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function _replaceChar($value)
	{
		$search_list = array('%2d', '%5f', '%2e', '%21', '%2a', '%28', '%29');
		$replace_list = array('-', '_', '.', '!', '*', '(', ')');
		$value = str_replace($search_list, $replace_list ,$value);
	
		return $value;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : natcaseksort
	 * 說明 : 解決php>=5.4 的版本中，ksort函數多了兩個 SORT_NATURAL 和 SORT_FLAG_CASE 這兩個 sort_flags
	 *       如果這樣的函數
     *       ksort($arr, SORT_NATURAL | SORT_FLAG_CASE);
     *       用在php<5.4 環境中，就會出現這樣的錯誤：
     *       PHP Notice: Use of undefined constant SORT_NATURAL - assumed 'SORT_NATURAL' in ..
     *       PHP Notice: Use of undefined constant SORT_FLAG_CASE - assumed 'SORT_FLAG_CASE' in .
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function natcaseksort( &$array ) 
	{
		uksort( $array, 'strnatcasecmp' );
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 金流返回單號驗證
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function is_order_id_dupli($order_show_id){
		
		$this->CI->db->where('order_show_id' ,$order_show_id);
		$query = $this->CI->db->get('order_cash_return_info');
			
		if( $query->num_rows() <= 0 ){
			return true;
		}else{
			return false;
		}
	}
}


/* End of file Ezship.php */