<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 類別功能 : ezship 類別。
 * 說明 : ezship 的金流設置，使用步驟和方式為:
 *         Step1 : 修改 __construct 建構函數的參數配置。
 *         Step2 : 呼叫 setParams()方法，並填入必填參數。
 *         Step3 : 呼叫 dataTransport()方法，傳送資料。
 *         Step4 : 完成。
 *
 * 範例 :
 *   $ezship = new Ezship();
 *   $ezship->setParams(array('order_id' => '1',
 *                             'rv_name' => '皇大熊',
 *                            'rv_email' => 'linlong3388@gmail.com',
 *                            'rv_mobil' => '0923656954',
 *                           'rv_amount' => '110'
 *                     ));
 *	 $ezship->dataTransport();
 *
 * 官網串接資料
 *   https://www.ezship.com.tw/staticpage/ezship_join_weborder_introdution_02_3.jsp
 *
 * @controllerName ezship
 * @author Dio
 *
 */
class Ezship extends Cash_flow{

	protected $param;

	/**
	 * 建構方法 : 成員和物件初始化
	 * 說明           : 設置[固定屬性]值
	 *               url = 交易網址(必填)
	 *             su_id = 業主帳號(必填)
	 *             rturl = 回傳路徑及程式名稱(選填)
	 *           webtemp = 網站所需額外判別資料(選填)
	 *
	 * 每建構一套新系統，都要這裡重新確認參數資料是否正確，如業主帳號
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		$this->param['url']       = 'https://www.ezship.com.tw/emap/rv_request_web.jsp';
		$this->param['su_id']     = 'info@glaubetw.com';
		$this->param['rturl']     = '';
		$this->param['webtemp']   = '';
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : setParams
	 * 說明 : 設置[非固定屬性]值，如:訂單編號、訂購金額、數量...等。:
	 *       order_id  = 訂單編號(必填)
	 *       rv_name   = 取件人姓名(必填)
	 *       rv_email  = 取件人電子郵件(必填)
	 *       rv_mobil  = 取件人行動電話(必填)
	 *       rv_amount = 交易金額(必填)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function setParams(array $param){
		$this->param['order_id']  = $param['order_id'];
		$this->param['rv_name']   = $param['rv_name'];
		$this->param['rv_email']  = $param['rv_email'];
		$this->param['rv_mobil']  = $param['rv_mobil'];
		$this->param['rv_amount'] = intval(round($param['rv_amount']));
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : getParams
	 * 說明 : 取得所有傳遞至遠端主機的參數，可以隨時列印出來，確定傳遞參數是否正確。
	 *
	 * @access	public
	 * @param
	 * @return array
	 */
	public function getParams(){

		return $this->param;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : dataTransport
	 * 說明 : 傳送資料到遠端主機，採用隱藏欄位並搭配JS進行資料POST傳遞。
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function dataTransport(){

		$str = <<<EOD
                 <form id='ezship_form' action="{$this->param['url']}" method="post">
                    <input type="hidden" name="su_id" value="{$this->param['su_id']}"/>
                    <input type="hidden" name="rturl" value="{$this->param['rturl']}"/>
                    <input type="hidden" name="webtemp" value="{$this->param['webtemp']}"/>
                    <input type="hidden" name="order_id" value="{$this->param['order_id']}"/>
                    <input type="hidden" name="rv_name" value="{$this->param['rv_name']}"/>
                    <input type="hidden" name="rv_email" value="{$this->param['rv_email']}"/>
                    <input type="hidden" name="rv_mobil" value="{$this->param['rv_mobil']}"/>
                    <input type="hidden" name="rv_amount" value="{$this->param['rv_amount']}"/>
                    <input style="display:none" type="submit"/>
                 </form> 

                 <script>
                    document.getElementById("ezship_form").submit();
                 </script>
EOD;

		echo $str;
	}
	 
	// --------------------------------------------------------------------

	/**
	 * 方法 : is_checkReturn
	 * 說明 : 檢查返回值是否正確(第一次)。
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function is_checkReturn($param){
		//驗證成功
		if($param['sn_id'] != "00000000")
		return true;
		else
		return false;
	}

}


/* End of file Ezship.php */