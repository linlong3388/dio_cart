<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * ATM轉帳
 *    無需串金流     
 */
class No_Flow_Atm extends Cash_flow{

	protected $param;

	/**
	 * 建構方法 : 成員和物件初始化
	 * 說明           : 設置[固定屬性]值
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		
		$this->CI =& get_instance();
		$this->CI->load->library(array('Dio_email'));
		$this->CI->load->helper(array('url','dio_url'));
		
		$this->CI->load->model("frontend/order_model","order");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : setParams
	 * 說明 : 設置[非固定屬性]值。
	 * 傳入參數 : $param['info']
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function setParams(array $param){

		$this->param['Status']      = $param['order']['master']['status'];
		$this->param['Msg']         = "";
		$this->param['RsURL']       = getUserURL('checkout_return/atm?order_show_id='.$param['order']['master']['order_show_id']);
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : getParams
	 * 說明 : 取得轉帳資料。
	 *
	 * @access	public
	 * @param
	 * @return array
	 */
	public function getParams(){

		return $this->param;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : dataTransport
	 * 說明 : 傳送資料到遠端主機。
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function dataTransport(){
		
		redirect($this->param['RsURL']);
	}
	 
	// --------------------------------------------------------------------

	/**
	 * 方法 : is_checkReturn
	 * 說明 : 檢查返回值是否正確(第一次)。
	 *
	 * @access	public
	 * @param	    t/f
	 * @return    t/f
	 */
	public function is_checkReturn($param){
		 
		return true;
	}

}


/* End of file Atm.php */