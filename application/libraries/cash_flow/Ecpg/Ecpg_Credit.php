<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 聯合信用卡處理中心
 *    
 */
class Ecpg_Credit extends Cash_flow{

	protected $param;

	/**
	 * 建構方法 : 成員和物件初始化
	 * 說明           : 設置[固定屬性]值
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		
		$this->CI =& get_instance();
		$this->CI->load->helper(array('url','dio_url'));
		
		$this->CI->load->model("frontend/order_model","order");
		
		$this->mac_key = hash('sha256', 'ffc5eea6ff6c873a976730b36e225c5a916861a4658c8288f26d1abf3e360494');
		$this->url     = "https://nccnet-ectest.nccc.com.tw/merchant/HPPRequest";
		
		$this->param['MerchantID'] = "6600800020";
		$this->param['TerminalID'] = "70502932";
		$this->param['NotifyURL']   = getUserURL('checkout_return/ecpg_credit');
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : setParams
	 * 說明 : 設置[非固定屬性]值。
	 * 傳入參數 : $param['info']
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function setParams(array $param){

		$this->param['Install']     = 1;
		$this->param['OrderID']     = $param['order']['master']['order_show_id'];
		$this->param['TransMode']   = 0;
		$this->param['TransAmt']    = $param['order']['master']['total'];
		$this->param['Signature']   = $this->getSignature($this->param);
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : getParams
	 * 說明 : 取得資料。
	 *
	 * @access	public
	 * @param
	 * @return array
	 */
	public function getParams(){

		return $this->param;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : dataTransport
	 * 說明 : 傳送資料到遠端主機，採用隱藏欄位並搭配Iframe進行資料POST傳遞。
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function dataTransport(){
		
		$html_input = "交易處理中，請勿關閉視窗...";
		
		foreach ($this->param as $key => $val) {
			$html_input .= "<input type='hidden' name='" . $key . "' value='" . $val . "'><BR>";
		}
			
		$str = <<<EOD
	        	<body onload="document.HPP.submit();"> 
                 <form name="HPP" method="post" target="HPPFrame" action="{$this->url}">
                 <iframe name="HPPFrame" id="mainFrame" height="600" width="800" Frameborder="0"></iframe> 
                      $html_input
                    <input style="display:none" type="submit"/>
                 </form>
		        </body>
            
EOD;
		
		echo $str;
	}
	 
	// --------------------------------------------------------------------

	/**
	 * 方法 : is_checkReturn
	 * 說明 : 檢查返回值是否正確(第一次)。
	 *
	 * @access	public
	 * @param	    t/f
	 * @return    t/f
	 */
	public function is_checkReturn($param){
		
		if($param['ResponseCode'] == '00' || $param['ResponseCode'] == '08' || $param['ResponseCode'] == '11'){
			return true;
		}else{
			return false;
		}
		
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : getSignature
	 * 說明 : 取得訊息認證碼
	 *
	 * @access	public
	 * @param	    
	 * @return   
	 */
	public function getSignature($param){
			
		$signature = "MerchantID=".$param['MerchantID']
		           ."&TerminalID=".$param['TerminalID']
                      ."&OrderID=".$param['OrderID']                                            
                     ."&TransAmt=".$param['TransAmt']
                    ."&TransMode=".$param['TransMode']
                      ."&Install=".$param['Install']
                    ."&NotifyURL=".$param['NotifyURL']
."&CSS_URL="
."&BankNo="
."&TEMPLATE="
."&TravelLocCode="
."&TravelStartDate="
."&TravelEndDate="
."&".$this->mac_key;
		
		return hash('sha256', $signature);
	}
	
}


/* End of file Atm.php */