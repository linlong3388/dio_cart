<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 彰化銀行 ATM 串接 (萬用帳號)
 *     說明 : 訂單完成後，會產生一組虛擬匯款帳號供會員匯款
 */
class Chb_Atm_Uni extends Cash_flow{

	protected $param;

	/**
	 * 建構方法 : 成員和物件初始化
	 * 說明           : 設置[固定屬性]值
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		
		$this->CI =& get_instance();
		$this->CI->load->helper(array('url','dio_url'));
		
		//$this->url = "https://www.chbipay.com/api"; //正式站
		$this->url       = "http://210.65.204.170/api"; //測試站
		$this->key       = "vAFMrwfKyMKuVwdx4YDRwn6RpkvlaBqM8XEJpKEn4iAYFEnFDTJi84ZISkXgifm";
		$this->member_id = "2015121010000006";
		$this->RsURL     = getUserURL('checkout_return/chb_atm');		
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : setParams
	 * 說明 : 設置串接參數。
	 * 傳入參數 : $param['info']
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function setParams(array $param){
		
		$expiry = date("Ymd", strtotime("+30 day"));
		$expiry = $expiry."000000";
		
		$this->param['amount']        = $param['order']['master']['total'];
		$this->param['currency']      = 'TWD';
		$this->param['delivery_addr'] = $param['order']['master']['local'] .' '.$param['order']['master']['address'];
		$this->param['delivery_req']  = 'N';
		$this->param['expiry']        = $expiry;
		$this->param['ext_data']      = '123';
		$this->param['member_id']     = $this->member_id;
		$this->param['nonce']         = 'iPayUAT';
		$this->param['order_desc']    = '測試訂單';
		$this->param['order_no']      = $param['order']['master']['order_show_id'];
		$this->param['type']          = 'PAY_REQ'; //'QUERY_REQ';
		$this->param['version']       = '1.1';

		//取得sign簽章
		$this->param['sign'] = $this->getSignValue($this->param);
		
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : getParams
	 * 說明 : 取得轉帳資料。
	 *
	 * @access	public
	 * @param
	 * @return array
	 */
	public function getParams(){

		return $this->param;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : dataTransport
	 * 說明 : 傳送資料到遠端主機。
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function dataTransport(){
	
	   $json_data =	json_encode($this->getParams());
		
	   $curl = curl_init( $this->url );
	
	   curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	   curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
	   curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
	   curl_setopt($curl, CURLOPT_POSTFIELDS,$json_data);

	   $curl_response = curl_exec($curl);
	   
	   curl_close($curl);

	   $result = json_decode($curl_response, true);
	   
	   //將金流返回參數，自行POST到 checkout_return
	   if( !empty($result) ) {

	      $html_input = "";
	   	
	   	  foreach ($result as $key => $val) {
	   		  $html_input .= "<input type='hidden' name='" . $key . "' value='" . $val . "'><BR>";
	   	  }
	   		
	   	  $str = <<<EOD
                 <form id="form" style="display:none" action="{$this->RsURL}" method="post">
                      $html_input
                    <input type="submit"/>
                 </form>
	   	
                 <script>
                    document.getElementById("form").submit();
                 </script>
EOD;
	   	
	   	echo $str;
	   	
	   }else{

	   	 redirect($this->RsURL);
	   }
	   
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : getSignValue
	 * 說明 : 取得sign簽章
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getSignValue( $param )
	{
	   $str = "amount=".$param['amount']."&currency=".$param['currency'];
	   
	   if ($param['delivery_addr'] != '')
		  $str = $str."&delivery_addr=".$param['delivery_addr'];
	   if ($param['delivery_req'] != '')
		  $str = $str."&delivery_req=".$param['delivery_req'];
	      $str = $str."&expiry=".$param['expiry'];
	   if ($param['ext_data'] != '')
		  $str = $str."&ext_data=".$param['ext_data'];
	      $str = $str."&member_id=".$param['member_id']."&nonce=".$param['nonce'];
	      $str = $str."&order_desc=".$param['order_desc']."&order_no=".$param['order_no'];
	      $str = $str."&type=".$param['type']."&version=".$param['version']."&key=".$this->key;
	
	      return base64_encode(hash('sha256', $str, TRUE));
	}
	 
	// --------------------------------------------------------------------

	/**
	 * 方法 : is_checkReturn
	 * 說明 : 檢查返回值是否正確(第一次)。
	 *
	 * @access	public
	 * @param	  t/f
	 * @return    t/f
	 */
	public function is_checkReturn($param){

		if( empty($param) )
			return false;
		
	    if($param['return_code'] != '000000') 
	    	return false;
	    
	    return true;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : insAtmPaymentInfo
	 * 說明 : 寫入 ATM 繳費資訊
	 *
	 * @access	public
	 * @param	  t/f
	 * @return    t/f
	 */
	public function insAtmPaymentInfo($param){

		$data_msg = array( 'order_no'       => $param['order_no']
				          ,'transaction_id' => $param['transaction_id']
				          ,'currency'       => $param['currency']
				          ,'amount'         => $param['amount']
				          ,'expiry'         => $param['expiry']
			    	      ,'payment_url'    => $param['payment_url']
				          ,'ext_data'       => $param['ext_data']
		            );
		
		//驗證資料庫資料是否存在
		$this->CI->db->where('order_show_id' ,$param['order_no']);
		$query = $this->CI->db->get('order_cash_return_info');
		
		if( $query->num_rows() <= 0 ){

			$data = array( 'order_show_id' => $param['order_no'] 
					      ,'msg' => serialize( $data_msg )
					      ,'cdate' => date('Y-m-d H:i:s'));	
					
			$this->CI->db->insert('order_cash_return_info' ,$data);
			
		}else{

			$data = array( 'order_show_id' => $param['order_no']
				          ,'msg' => serialize( $data_msg ));
				
			$this->CI->db->where('order_show_id' ,$param['order_no']);
			$this->CI->db->update('order_cash_return_info' ,$data);				
		}
		
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : showAtmPaymentInfo
	 * 說明 : 顯示 ATM 繳費資訊
	 *
	 * @access	public
	 * @param	  
	 * @return    string
	 */
	public function showAtmPaymentInfo($param){
		
		$expiry = getStringToDate($param['expiry']); 
		
		$data['info'] = "您的 ATM 繳費資訊如下 :" . "<p>" 
			           . "訂單編號 : " . $param['order_no'] . "<p>" 
			           . "平台交易編號 : " . $param['transaction_id'] . "<p>" 
                       . "幣別 : " . $param['currency'] . "<p>" 
			           . "總額 : " . addCommas($param['amount']) . "<p>" 
			           . "有效期限 : " . $expiry . "<p>" 
			           . "備註: " . $param['ext_data'] . "<p>" ;		

		$data['url'] = $param['payment_url'];
		
		return $data;		
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : isAtmPaymentQuery
	 * 說明 : 付款查詢
	 *
	 * @access	public
	 * @param
	 * @return    string
	 */
	public function isAtmPaymentQuery( $order_show_id ){
		
		//取得訂單資料
		$this->CI->db->where('order_show_id' ,$order_show_id);
		$query = $this->CI->db->get('order')->row_array();
		
		//驗證ATM付款結果
		$param['type']          = 'PAY_REQ'; //'QUERY_REQ';
		$param['version']       = '1.1';
		$param['member_id']     = $this->member_id;
		$param['order_no']      = $param['order']['master']['order_show_id'];
		$param['nonce']         = 'iPayUAT';
		
		//取得sign簽章
		$param['sign'] = $this->getSignValueQuery($param);
		
		$result = $this->dataTransportQuery($param);
		
		if ( $result['return_code'] == '000000'){
			return true;
		}
		
		return false;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : getSignValueQuery
	 * 說明 : 取得sign簽章(for 訂單查詢)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getSignValueQuery( $param )
	{
		$str = "type=".$param['type']."&version=".$param['version'];
		$str = $str."&member_id=".$param['member_id']."&order_no=".$param['order_no'];
		$str = $str."&nonce=".$param['nonce'];
		
		return base64_encode(hash('sha256', $str, TRUE));
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : dataTransportQuery
	 * 說明 : 傳送資料到遠端主機(for 訂單查詢)。
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function dataTransportQuery( $param ){
	
		$json_data =	json_encode( $param );
	
		$curl = curl_init( $this->url );
	
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_POSTFIELDS,$json_data);
	
		$curl_response = curl_exec($curl);
	
		curl_close($curl);
	
		return json_decode($curl_response, true);
	}	
	
}


/* End of file First_Bank_Atm.php */