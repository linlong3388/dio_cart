<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 類別功能 : 綠界/超商取貨付款(for 7-11)。
 * 說明 : 此為物流串接，當傳遞  :
 *        "CollectionAmount 代收金額" 和 "IsCollection 是否代收貨款" 兩個欄位時，
 *        即可使用金流代收款項的作用，也就是(物流+金流)。 
 * 
 * @controllerName Dio_ECPay_Logistics_UNIMART
 * @author Dio
 *
 */
class Dio_ECPay_Logistics_UNIMART extends Cash_flow{

	protected $param;

	/**
	 * 建構方法 : 成員和物件初始化
	 * 說明           : 設置[固定屬性]值
	 *
	 * 每建構一套新系統，都要這裡重新確認參數資料是否正確，如業主帳號
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		
		//載入綠界物流(Logistics)模組
		require_once('ECPay.Logistics.Integration.php');
		             
		$this->CI =& get_instance();
		
		//測試站
		/*
		$this->param['HashKey']     = 'XBERn1YOvpM9nfZc';
		$this->param['HashIV']      = 'h1ONHk4P4yqbl5LK';
		$this->param['MerchantID']  = '2000933';
		*/	
			
		//正式站
		$this->param['HashKey']     = '5hdxm5Tk7m5uYMB4'; 
		$this->param['HashIV']      = '2SqzrNfrAOr837a3';
		$this->param['MerchantID']  = '3045527';		                               
		$this->param['EncryptType'] = '1';
		
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : setParams
	 * 說明 : 設置[非固定屬性]值，如:訂單編號、訂購金額、數量...等
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function setParams(array $param){

	    try {
                $this->AL = new ECPayLogistics();
                $this->AL->HashKey = $this->param['HashKey'];
                $this->AL->HashIV  = $this->param['HashIV'];
                
                $this->AL->Send = array(
					'MerchantID'        => $this->param['MerchantID'],
					'MerchantTradeNo'   => $param['order']['master']['order_show_id'],
					'MerchantTradeDate' => date ( 'Y/m/d H:i:s' ),
					'LogisticsType'     => LogisticsType::CVS,
					'GoodsAmount'       => (int) $param['order']['master']['total'], //金額範圍為 1~20,000
					'CollectionAmount'  => (int) $param['order']['master']['total'], //代收金額
					'IsCollection'      => IsCollection::YES , 
					'GoodsName'         => '一般商品',
					'SenderName'        => $_SESSION['sys_info']['cvs_name'],
					'SenderPhone'       => $_SESSION['sys_info']['cvs_phone'],
					'SenderCellPhone'   => $_SESSION['sys_info']['cvs_mobile'],
                	
                	'ReceiverName'      => $param['order']['master']['name'],
					'ReceiverPhone'     => $param['order']['master']['phone'],
					'ReceiverCellPhone' => $param['order']['master']['mobile'],
					'ReceiverEmail'     => $param['order']['master']['email'],
                	'TradeDesc'         => "", //$param['order']['master']['memo'],

                	'ServerReplyURL'       => getUserURL('checkout_return/ecpay_Logistics_unimart_status_offLine'), //物流狀態通知
                    'ClientReplyURL'       => getUserURL('checkout_return/ecpay_Logistics_unimart'), //訂單成立
                	'LogisticsC2CReplyURL' => getUserURL('checkout_return/ecpay_Logistics_unimart_problem'),
					'Remark'            => $_SESSION['sys_info']['cvs_memo'], //超商取貨備註
					'PlatformID'        => '',

                	'CheckMacValue'     => '',
                 );
                
                //設定超商類型(7-11)
                if( $param['order']['master']['cvs_logistics_sub_type'] == 'UNIMARTC2C'){
                	$this->AL->Send['LogisticsSubType'] = LogisticsSubType::UNIMART_C2C;
                }
                
                //全家
                if( $param['order']['master']['cvs_logistics_sub_type'] == 'FAMIC2C' ){
                	$this->AL->Send['LogisticsSubType'] = LogisticsSubType::FAMILY_C2C;
                }
                
                //收件人門市代號
                $this->AL->SendExtend = array(
                    'ReceiverStoreID' => $_SESSION['my_order']['cvs']['cvs_store_id'],
                    'ReturnStoreID'   => '',
                 );
             
	    } catch(Exception $e) {
            echo $e->getMessage();
        }
        
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : setParamsMap
	 * 說明 : 設置地圖
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function setParamsMap(array $param){
	
		try {
			
			$this->AL = new ECPayLogistics();
			
			$this->AL->Send = array(
					'MerchantID'        => $this->param['MerchantID'],
					'MerchantTradeNo'   => getCvsStoreTransId() ,
					'LogisticsType'     => 'CVS' ,
					'IsCollection'      => IsCollection::NO ,
					'ServerReplyURL'    => getUserURL('checkout/step4'),
					'ExtraData'         => '',//額外資訊
					'Device'            => Device::PC,  
			);
		
			//7-11
			if($param['pay_method'] == 2){
                $this->AL->Send['LogisticsSubType'] = LogisticsSubType::UNIMART_C2C;
			}
			
			//全家
			if($param['pay_method'] == 3){
				$this->AL->Send['LogisticsSubType'] = LogisticsSubType::FAMILY_C2C; 
			}
			
			//CvsMap(Button名稱, Form target)
			 $html = $this->AL->CvsMap('');
			 echo $html;
		} catch(Exception $e) {
			 echo $e->getMessage();
		}
		
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : getParams
	 * 說明 : 取得所有傳遞至遠端主機的參數，可以隨時列印出來，確定傳遞參數是否正確。
	 *
	 * @access	public
	 * @param
	 * @return array
	 */
	public function getParams(){
	
		return $this->param;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : dataTransport
	 * 說明 : 傳送資料到遠端主機，採用隱藏欄位並搭配JS進行資料POST傳遞。
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function dataTransport(){

		echo "交易處理中，請勿關閉視窗...";
		
		$Result = $this->AL->CreateShippingOrder();
		
		$str = <<<EOD
                 $Result
                 <script>
                     document.getElementById("ECPayForm").style.display = "none";	
                	 document.getElementById("ECPayForm").submit();
                 </script>
EOD;
		
		echo $str;
	}
	 	
	// --------------------------------------------------------------------

	/**
	 * 方法 : is_checkReturn
	 * 說明 : 檢查返回值是否正確。
	 *    
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function is_checkReturn($param){
		
		if( empty($param['RtnCode']) ){
			return false;
		}
		
		return true;
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : _getMacValue
	 * 說明 : 產生檢查碼
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function _getMacValue($hash_key, $hash_iv, $form_array)
	{
		$encode_str = "HashKey=" . $hash_key;
		foreach ($form_array as $key => $value)
		{
			$encode_str .= "&" . $key . "=" . $value;
		}
		$encode_str .= "&HashIV=" . $hash_iv;
		$encode_str = strtolower(urlencode($encode_str));
		$encode_str = $this->_replaceChar($encode_str);
	
		return md5($encode_str);
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : _replaceChar
	 * 說明 : 特殊字元置換
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function _replaceChar($value)
	{
		$search_list = array('%2d', '%5f', '%2e', '%21', '%2a', '%28', '%29');
		$replace_list = array('-', '_', '.', '!', '*', '(', ')');
		$value = str_replace($search_list, $replace_list ,$value);
	
		return $value;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : natcaseksort
	 * 說明 : 解決php>=5.4 的版本中，ksort函數多了兩個 SORT_NATURAL 和 SORT_FLAG_CASE 這兩個 sort_flags
	 *       如果這樣的函數
     *       ksort($arr, SORT_NATURAL | SORT_FLAG_CASE);
     *       用在php<5.4 環境中，就會出現這樣的錯誤：
     *       PHP Notice: Use of undefined constant SORT_NATURAL - assumed 'SORT_NATURAL' in ..
     *       PHP Notice: Use of undefined constant SORT_FLAG_CASE - assumed 'SORT_FLAG_CASE' in .
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function natcaseksort( &$array ) 
	{
		uksort( $array, 'strnatcasecmp' );
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 金流返回單號驗證
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function is_order_id_dupli($order_show_id){
		
		$this->CI->db->where('order_show_id' ,$order_show_id);
		$query = $this->CI->db->get('order_cash_return_info');
			
		if( $query->num_rows() <= 0 ){
			return true;
		}else{
			return false;
		}
	}
}


/* End of file Dio_ECPay_Logistics_UNIMART.php */