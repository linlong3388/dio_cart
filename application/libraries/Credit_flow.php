<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 類別功能 : 金流串接類別。
 *
 * @controllerName Credit_flow
 * @author Dio
 *
 */
class Credit_flow{

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		$this->CI =& get_instance();

		error_reporting(E_WARNING);
		require_once 'ctmall_api/POSAPI.php';
		require_once 'ctmall_api/common.php';
		require_once 'ctmall_api/auth_transac.php';
		require_once 'ctmall_api/authRecur_transac.php';
		require_once 'ctmall_api/redeem_transac.php';
		require_once 'ctmall_api/redeem_money_transac.php';
			
		$this->auth_obj  = new auth_transac(); //一般
		$this->authRecur_obj  = new authRecur_transac(); //分期
		$this->redeem_obj  = new redeem_transac(); //紅利
		$this->redeem_money_obj  = new redeem_money_transac(); //紅利加現金

		//庫存量計算
		$this->CI->load->library(array('Order_status'));
		$this->os = new Order_status();
			
		$this->result = array();
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 授權的方法
	 * 說明 : 區分[自付額][分期付款][紅利]共三類授權方式
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function authTransac($data ,$credit_data){

		$type = explode(',', $data['pay_method']);

		switch ($type[0]) {

			//紅利支付
			case 'bonus':
				$result = $this->redeem_obj->redeem_transac(array(
	       	      	                'LID-M' => $data['order_detail_id'],   //訂單編號
                                      'PAN' => $credit_data['PAN'],        //信用卡號+末三碼(19碼)
                                  'ExpDate' => $credit_data['ExpDate'],    //信用卡有效期限(6碼 ,格式 YYYYMM)
                                 'PRODCODE' => '00',                       //產品碼
				                      'ECI' => $credit_data['ECI'],        //交易安全等級
				//'purchAmt' => calc_bonus_money( get_payMethod_money( 'bonus' ,$data['product_total'] )) //實際訂單總金額
                                 'purchAmt' => $data['product_total'] //實際訂單總金額
				));
				break;

				//紅利加金額
			case 'bonus_money':
				$result = $this->redeem_money_obj->redeem_money_transac(array(
	       	      	                'LID-M' => $data['order_detail_id'],   //訂單編號
                                      'PAN' => $credit_data['PAN'],        //信用卡號+末三碼(19碼)
                                  'ExpDate' => $credit_data['ExpDate'],    //信用卡有效期限(6碼 ,格式 YYYYMM)
                                 'PRODCODE' => '00',                       //產品碼
				                      'ECI' => $credit_data['ECI'],        //交易安全等級
				//'purchAmt' => calc_bonus_money( get_payMethod_money( 'bonus' ,$data['product_total'] )) //實際訂單總金額
                                 'purchAmt' => $data['product_total'] //實際訂單總金額
				));
				break;

					
				//分期付款(3/6)
			case 'staging_3':
			case 'staging_6':
					
				//至少一期
				$staging = 1;
				if($type[0] == 'staging_3') $staging = 3;
				if($type[0] == 'staging_6') $staging = 6;
					
				$result = $this->authRecur_obj->authRecur_transac(array(
	                                'LID-M' => $data['order_detail_id'],  //訂單編號
                                      'PAN' => $credit_data['PAN'],       //信用卡號+末三碼(19碼)
                                  'ExpDate' => $credit_data['ExpDate'],   //信用卡有效期限(6碼 ,格式 YYYYMM)
				                      'ECI' => $credit_data['ECI'],        //交易安全等級
                                 'purchAmt' => $data['product_total'],    //訂單總金額
	                            'RECUR_NUM' => $staging                   //期數
				));

				break;
					
		}
			
		//授權成功，更新訂單狀態
		if($result['RespCode']==0 && $result['ErrCode']=="00"){

			$OffsetAmt = 0;

			//如果為紅利折抵的回傳值
			if( isset($result['OffsetAmt']) && isset($result['OriginalAmt']) ){
				$OffsetAmt_ary = explode(' ',$result['OffsetAmt']);
				$OffsetAmt = $OffsetAmt_ary[1];
			}

			//更新金流參數
			$this->CI->db->where('order_detail_id' ,$data['order_detail_id']);
			$this->CI->db->update('order_detail' ,array(     //'status' => '1' ,
                      	                                      'Auth_XID' => $result['XID'],
                      	                                'Auth_AuthRRPID' => $result['AuthRRPID'],
                      	                                 'Auth_AuthCode' => $result['AuthCode'],
                      	                                  'Auth_TermSeq' => $result['TermSeq'],
			                                             'Auth_OffsetAmt' => $OffsetAmt   
			));

			//更新訂單狀態與庫存量
			$this->os->change_status(array(
	                                'status' => '1' ,
	                       'order_detail_id' => $data['order_detail_id']
			));
			 
			return $result;
		}else{
				
			//授權失敗，將金額設定為0
			$this->CI->db->where('order_detail_id' ,$data['order_detail_id']);
			$this->CI->db->update('order_detail' ,array('product_total' => 0));
				
			auth_error_log('authTransac' ,$result);
			return false;
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 請款的方法
	 * 說明 : [自付額][分期付款][紅利]共用
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function capTransac($data){

		$cap = array(     'XID' => $data['Auth_XID'],        //交易識別碼
                    'AuthRRPID' => $data['Auth_AuthRRPID'],  //SSL交易識別碼                                                
                       'orgAmt' => $data['product_total'],   //原授權金額
                       'capAmt' => $data['product_total'],   //請款金額                                                       
                     'AuthCode' => $data['Auth_AuthCode'],   //交易授權碼
                      'TermSeq' => $data['Auth_TermSeq'] );  //調閱序號

		$type = explode(',', $data['pay_method']);

		switch ($type[0]) {

			//紅利點數
			case 'bonus':
				$result = $this->redeem_obj->cap_transac($cap);
				break;

				//點加金
			case 'bonus_money':
				$result = $this->redeem_money_obj->cap_transac($cap);
				break;

				//分期付款(3/6)
			case 'staging_3':
			case 'staging_6':
					
				//至少一期
				$staging = 1;
				if($type[0] == 'staging_3') $staging = 3;
				if($type[0] == 'staging_6') $staging = 6;
					
				$result = $this->authRecur_obj->cap_transac($cap);
				break;
					
		}
			
		//請款成功，更新訂單狀態
		if($result['RespCode']==0 && $result['ErrCode']=="00"){
			$this->CI->db->where('order_detail_id' ,$data['order_detail_id']);
			$this->CI->db->update('order_detail' ,array('status' => '2' ,
                 	                                  'Cap_BatchID' => $result['BatchID'],
                      	                             'Cap_BatchSeq' => $result['BatchSeq']));
			return true;
		}else{
			cap_error_log('capTransac' ,$result);
			return false;
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 取消請款的方法
	 * 說明 : [分期付款][紅利]共用
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function capRevTransac($data){

		$capRev = array(
                         'XID' => $data['XID'],
                   'AuthRRPID' => $data['AuthRRPID'],                    
                      'orgAmt' => $data['orgAmt'],                   
                    'AuthCode' => $data['AuthCode'],
                     'TermSeq' => $data['TermSeq'],
                     'BatchID' => $data['BatchID'], 
                    'BatchSeq' => $data['BatchSeq']
		);

		$type = explode(',', $data['pay_method']);

		switch ($type[0]) {

			//紅利點數
			case 'bonus':
				$result = $this->redeem_obj->capRev_transac($capRev);
				break;

				//點加金
			case 'bonus_money':
				$result = $this->redeem_money_obj->capRev_transac($capRev);
				break;

				//分期付款(3/6)
			case 'staging_3':
			case 'staging_6':
					
				//至少一期
				$staging = 1;
				if($type[0] == 'staging_3') $staging = 3;
				if($type[0] == 'staging_6') $staging = 6;
					
				$result = $this->authRecur_obj->capRev_transac($capRev);
				break;
					
		}
			
		//取消請款成功，更新訂單狀態
		if($result['RespCode']==0 && $result['ErrCode']=="00"){
			$this->CI->db->where('order_detail_id' ,$data['order_detail_id']);
			$this->CI->db->update('order_detail' ,array('status' => '1'));

			return true;
		}else{
			authRev_error_log('capRevTransac' ,$result);
			return false;
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 取消授權的方法
	 * 說明 : [分期付款][紅利]公用
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function authRevTransac($data){

		$authRev = array(	 'XID' => $data['XID'],        //交易識別碼
		               'AuthRRPID' => $data['AuthRRPID'],  //SSL交易識別碼  
	  	                  'orgAmt' => $data['orgAmt'],     //原授權金額
		                'AuthCode' => $data['AuthCode'],   //交易授權碼
                         'TermSeq' => $data['TermSeq']     //調閱序號
		);

		$type = explode(',', $data['pay_method']);

		switch ($type[0]) {

			//紅利點數
			case 'bonus':
				$result = $this->redeem_obj->authRev_transac($authRev);
				break;

				//點加金
			case 'bonus_money':
				$result = $this->redeem_money_obj->authRev_transac($authRev);
				break;

				//分期付款(3/6)
			case 'staging_3':
			case 'staging_6':
					
				//至少一期
				$staging = 1;
				if($type[0] == 'staging_3') $staging = 3;
				if($type[0] == 'staging_6') $staging = 6;
					
				$result = $this->authRecur_obj->authRev_transac($authRev);
				break;
					
		}

		//取消授權成功，更新訂單狀態
		if($result['RespCode']==0 && $result['ErrCode']=="00"){
				
			//更新訂單狀態
			$this->CI->db->where('order_detail_id' ,$data['order_detail_id']);
			$this->CI->db->update('order_detail' ,array('status' => '4'));
			 
			return true;
		}else{
			authRev_error_log('authRevTransac' ,$result);
			return false;
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 退款的方法
	 * 說明 :
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function credTransac($data){

		$cred = array(
                  'XID' => $data['XID'],         //交易識別碼
            'AuthRRPID' => $data['AuthRRPID'],   //SSL交易識別碼  
               'orgAmt' => $data['orgAmt'],      //原授權金額
              'credAmt' => $data['credAmt'],     //退款金額
             'AuthCode' => $data['AuthCode'],    //交易授權碼
           'CapBatchID' => $data['CapBatchID'],  //請款交易批次編號
          'CapBatchSeq' => $data['CapBatchSeq']  //請款交易批次序號
		);
		 
		$type = explode(',', $data['pay_method']);

		switch ($type[0]) {

			//紅利點數
			case 'bonus':
				$result = $this->redeem_obj->cred_transac($cred);
				break;

				//點加金
			case 'bonus_money':
				$result = $this->redeem_money_obj->cred_transac($cred);
				break;

				//分期付款(3/6)
			case 'staging_3':
			case 'staging_6':
					
				//至少一期
				$staging = 1;
				if($type[0] == 'staging_3') $staging = 3;
				if($type[0] == 'staging_6') $staging = 6;
					
				$result = $this->authRecur_obj->cred_transac($cred);
				break;
					
		}

		//取消授權成功，更新訂單狀態
		if($result['RespCode']==0 && $result['ErrCode']=="00"){
			$this->CI->db->where('order_detail_id' ,$data['order_detail_id']);
			$this->CI->db->update('order_detail' ,array('status' => '3'));

			return true;
		}else{
			authRev_error_log('authRevTransac' ,$result);
			return false;
		}

	}


}


/* End of file Credit_flow.php */