<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 類別功能 : 屬性分類(Property)。
 *
 * 說明 :資料表有定義好的結構，必須遵循該結構。
 *
 * @controllerName Property
 * @author Dio
 *
 */
class Property{

	// --------------------------------------------------------------------

	/**
	 * 方法 : 設置基本資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function _set($tb_name){

		$this->CI =& get_instance();
		$this->tb_name = $tb_name;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 取得(所有)商品分類
	 *
	 * @access	public
	 * @param	$data
	 * @return
	 */
	public function get_all(){

		//依path欄位做排序 ,即可撈出樹狀結構資料
		$this->CI->db->order_by('path' ,'ASC');
		$query = $this->CI->db->get($this->tb_name)->result_array();

		return $query;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 依(頂級)取得(次級)所有分類項
	 * 備註 : 回傳的結果集為樹狀(階層)結構
	 *
	 * @access	public
	 * @param	$path
	 * @return　array
	 */
	public function get_child($path){

		$path_comma = $path . ',';

		$sql = "SELECT * FROM ($this->tb_name) WHERE `path` = '".$path."' OR  `path` LIKE '%".$path_comma."%' ORDER BY `path` ASC";
		$query = $this->CI->db->query($sql)->result_array();

		return $query;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 依(頂級)取得(次級)所有分類項，但 level < 4
	 * 備註 : 回傳的結果集為樹狀(階層)結構
	 *
	 * @access	public
	 * @param	$path
	 * @return　array
	 */
	public function get_child2($path){

		$path_comma = $path . ',';

		$sql = "SELECT * FROM ($this->tb_name) WHERE `level` < 3  AND (`path` = '".$path."' OR  `path` LIKE '%".$path_comma."%') ORDER BY `path` ASC";
			
		$query = $this->CI->db->query($sql)->result_array();

		return $query;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 取得(頂級)分類
	 *
	 * @access	public
	 * @param
	 * @return	array
	 */
	public function get_1parent(){

		$this->CI->db->where('level' ,'2');
		$this->CI->db->order_by('sort_order' ,'DESC');
		$query = $this->CI->db->get($this->tb_name)->result_array();

		return $query;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 取得(頂級)分類 /含商品總數
	 *
	 * @access	public
	 * @param
	 * @return	array
	 */
	public function get_1parent_total(){

		$sql = "SELECT p.property_id, p.name ,p.parent_id ,p.level ,p.path ,p.sort_order ,p.status ,p.base_image, p.created_at ,p.updated_at ,COUNT(ps.property_id) as ps_total
		          FROM `ct_property` p
                  LEFT JOIN `product_speci` ps ON p.property_id = ps.property_id
                GROUP BY p.property_id HAVING p.level = '2'";

		$result = $this->CI->db->query( $sql )->result_array();

		return $result;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 取得(次級)分類
	 *
	 * @access	public
	 * @param	$property_id
	 * @return	array
	 */
	public function get_2parent($property_id){
			
		$this->CI->db->where('parent_id' ,$property_id );
		$query = $this->CI->db->get($this->tb_name)->result_array();

		return $query;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 依`path`做排序，取得所有商品分類
	 *
	 * @access	public
	 * @param	$data
	 * @return
	 */
	public function get_last_parent(){

		//依path欄位做排序 ,即可撈出樹狀結構資料
		$sql = "SELECT * FROM `ct_property` WHERE `level` = 3 ORDER BY `path` ASC";

		$query = $this->CI->db->query($sql)->result_array();

		return $query;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 取得(單筆)商品分類
	 *
	 * @access	public
	 * @param	$data
	 * @return
	 */
	public function get_single($property_id){

		$this->CI->db->where('property_id' ,$property_id);
		$query = $this->CI->db->get($this->tb_name)->row_array();

		return $query;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 取得單筆商品分類名稱
	 *
	 * @access	public
	 * @param	string，格式: 0,3,6
	 * @return	string，格式: 服飾館 > 鞋子 > 女鞋
	 */
	public function get_name($path){

		$str = '';
		$path = explode(',', $path);

		foreach ($path as $key => $val){
			if($val != '0'){
				$this->CI->db->where_in('property_id', $val);
				$result = $this->CI->db->get($this->tb_name)->row_array();
					
				if($result){
					$str .= $result['name'] . ' > ' ;
				}
			}
		}

		return rtrim($str , ' > ');
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 取得單筆商品分類名稱(含url路徑)
	 *
	 * @access	public
	 * @param	string，格式: 0,3,6
	 * @return	string，格式: <a href="#">服飾館</a>
	 *                        <a href="#">鞋子</a>
	 *                        <a href="#">女鞋</a>
	 *						中間沒有>符號，用css:before表現
	 */
	public function get_name2($property_id){

		$str = '';
		$path_ary = explode(',', get_assign_field('property', 'property_id', $property_id, 'path'));

		foreach ($path_ary as $key => $val){
			if($val != '0'){
				$path = get_assign_field('property', 'property_id', $val, 'path');
				$url = base_url("index.php/frontend/product/multiple?property_id={$val}&path={$path}");

				$this->CI->db->where_in('property_id', $val);
				$result = $this->CI->db->get($this->tb_name)->row_array();

				if($result){
					$str .= "<a href=".$url.">" . $result['name'] . "</a>" ;
				}
			}
		}

		// 修正breadcrumb沒有html結尾
		// return rtrim($str , ' > ');
		return $str;

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 取得單筆商品分類名稱 / 追加樣式版
	 *
	 * @access	public
	 * @param	string，格式: 0,3,6
	 * @return	string，格式: 服飾館 > 鞋子 > 女鞋
	 */
	public function get_name_template($path){

		$str = '';
		$path = explode(',', $path);

		$timer = 0;

		foreach ($path as $key => $val){

			if($val != '0'){
				$this->CI->db->where_in('property_id', $val);
				$result = $this->CI->db->get($this->tb_name)->row_array();
					
				if($result){
					if($timer == 0){
						$str .= '<b style="color:#428bca">'.$result['name'].'</b>' . ' > ' ;
					}else{
						$str .= $result['name'] . ' > ' ;
					}
					$timer++;
				}
			}
		}

		return rtrim($str , ' > ');
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 新增商品分類
	 * 說明 : 採用兩段式寫入方式，先新增後更新該筆資料。
	 *
	 * @access	public
	 * @param	array
	 * @return
	 */
	public function insert($data){
			
		//1)先新增一筆商品分類
		$this->CI->db->insert($this->tb_name ,$data);

		//2)更新相關欄位資料
		$parent_id = end( explode(',', $data['path']) );     //父階取最後一個元素
		$level     = count(explode(',', $data['path'])) + 1; //層級+1
		$path      = $data['path'].','.$this->CI->db->insert_id() ;  //路徑追加本身ID
			
		$this->CI->db->where('property_id' ,$this->CI->db->insert_id());
		$this->CI->db->update($this->tb_name ,array('parent_id' => $parent_id ,'level' => $level ,'path' => $path ));
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 判斷該目錄是否有子目錄存在
	 *
	 * @access	public
	 * @param	$data
	 * @return
	 */
	public function is_sub_property($property_id){

		$query = $this->CI->db->get('property')->result_array();

		foreach ($query as $row){
			$pos = strpos($row['path'] ,($property_id.','));

			if (!$pos === false) {
				return true;
			}

		}

		return false;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 判斷該目錄是否有產品關聯
	 *
	 * @access	public
	 * @param	$data
	 * @return
	 */
	public function is_sub_product($property_id){

		$query = $this->CI->db->get('product')->result_array();

		foreach ($query as $row){

			if($row['property_id'] == $property_id){
				return true;
			}
		}

		return false;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 取得SKU群組編號
	 *
	 * @access	public
	 * @param	$data
	 * @return
	 */
	public function get_sku_group(){

		$sql = "SELECT * FROM `ct_sku_group` ORDER BY `property_id` ASC";

		$query = $this->CI->db->query($sql)->result_array();

		return $query;
	}

}


/* End of file Property.php */