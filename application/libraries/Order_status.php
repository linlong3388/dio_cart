<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 類別功能 : 訂單狀態控制類別。
 *
 * @controllerName Order_status
 * @author Dio
 *
 */
class Order_status{

	private $order_id;
	private $status;

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		$this->CI =& get_instance();
		$this->tb_name = 'order';

		$this->CI->load->helper(array('ctmall'));

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 改變訂單狀態的方法
	 * 說明 : 訂單狀態表
	 *              '0' =>  '失敗' ,
	 *              '1' =>  '處理中' , //通過金流
	 *              '2' =>  '已出貨' , //(-)庫存
	 *              '3' =>  '已取消' ,
	 *          	'4' =>  '已退貨'   //(+)庫存
	 *
	 * @access	public
	 * @param [order_id][status]
	 * @return t/f
	 */
	public function change_status($order_id ,$status){

		$this->order_id = $order_id;
		$this->status   = $status;

		switch ($this->status) {
				
			case '0':
			case '1':
			case '2': //更新商品庫存量(-)				
				break;
			case '3':
			case '4': //更新商品庫存量(+)
				break;
		}

		//都會執行到的共通程式
		$this->CI->db->where('order_id' ,$this->order_id);
		$this->CI->db->update($this->tb_name ,array('status' => $this->status));

		if($this->CI->db->affected_rows() > 0) return true;
		else return false;

	}
	 
}


/* End of file Order_status.php */