<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 類別功能 : 訂單類別。
 *
 * @controllerName Dio_order
 * @author Dio
 *
 */
class Dio_order{

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		$this->CI =& get_instance();
		
		$this->o   = 'order';
		$this->od  = 'order_detail';
		$this->ob  = 'order_box';
		$this->odi = 'order_promo';
		 
		$this->c   = 'customer';
		$this->p   = 'product';
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 依訂單編號，取得完整訂單資訊(單筆)
	 *
	 * @access	public
	 * @param	order_id
	 * @return  array
	 */
	public function getOrderInfo($order_id){
		
		//轉換訂單編號
		$order_id = transOrderShowIdToOrderId($order_id);
		
		//取得訂單資料(主表)和總額資料
		$sql = "SELECT `{$this->o}`.*
		               ,{$this->c}.cdate as mbr_cdate
		               ,{$this->c}.last_name as mb_name
		               ,{$this->c}.email as mb_email
		               ,{$this->c}.phone as mb_phone
		               ,{$this->c}.mobile as mb_mobile
		               ,(SELECT SUM(entity) FROM {$this->od} WHERE {$this->od}.order_id = `{$this->o}`.order_id  GROUP BY {$this->od}.order_id ) as entity
		               ,(IFNULL((SELECT SUM(total) FROM {$this->od} WHERE {$this->od}.order_id = `{$this->o}`.order_id  GROUP BY {$this->od}.order_id ),0)) as sub_total
		               ,(IFNULL((SELECT SUM(total) FROM {$this->od} WHERE {$this->od}.order_id = `{$this->o}`.order_id  GROUP BY {$this->od}.order_id ),0)
	              	     + IFNULL((SELECT SUM(total) FROM {$this->odi} WHERE order_id = `{$this->o}`.order_id AND item_type = '+'),0)
		                 - IFNULL((SELECT SUM(total) FROM {$this->odi} WHERE order_id = `{$this->o}`.order_id AND item_type = '-'),0)) as total
		        FROM `{$this->o}`
		          LEFT JOIN {$this->c} ON `{$this->o}`.customer_id = {$this->c}.customer_id
		          WHERE `{$this->o}`.order_id = {$order_id}";
		
		$data['order']['master'] = $this->CI->db->query($sql)->row_array();
		
		//取得訂單資料(副表)
		$sql = "SELECT od.* 
		              ,p.category_multi_id
		              ,p.image
		              ,p.name
		              ,p.branch
		         FROM `order_detail` as od
		         LEFT JOIN `product` as p ON od.product_id = p.product_id
		         WHERE od.order_id = {$order_id}";
		
		$data['order']['detail'] = $this->CI->db->query($sql)->result_array();
		
		//取得訂單折扣
		$this->CI->db->where('order_id' ,$order_id);
		$data['order']['promo'] = $this->CI->db->get('order_promo')->result_array();
		
	    return $data;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得完整訂單資訊(單筆 ) / 含禮盒明細
	 *
	 * @access	public
	 * @param	$param
	 * @return  array
	 */
	public function getOrderInfoBox($param){
		
		foreach ($param['order']['detail'] as $key=>$row){

			if($row['type_id'] == 1){
				
				//取得禮盒資料
				$this->CI->db->where('product_id' ,$row['product_box_id']);
				$order_box = $this->CI->db->get($this->ob)->result_array();
				
				foreach ($order_box as $key2=>$row2){
					
					//取得商品資料
					$this->CI->db->where('product_id' ,$row2['product_box_id']);
					$product = $this->CI->db->get($this->p)->row_array();
					
					$order_box[$key2]['pname'] = $product['name'];
					
					$param['order']['detail'][$key]['box'] = $order_box;
				}
				
			}			
		}
			
		return $param;
		
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 驗證是否屬於某會員的訂單
	 *
	 * @access	public
	 * @param	order_id
	 * @return  array
	 */
	public function isMyOrder($order_id ,$customer_id){
		
		$this->CI->db->where('order_id' ,$order_id);
		$this->CI->db->where('customer_id' ,$customer_id);
		
		$query = $this->CI->db->get('order');
		
		if( $query->num_rows() > 0 ){
	        return true;
		}
		
		return false;
	}

}


/* End of file dio_order.php */