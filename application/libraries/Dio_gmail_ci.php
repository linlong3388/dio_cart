<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 類別功能 : 透過Gmail@主機發送email類別。
 *
 * 備註 : 資料表目前需獨立設定
 *
 * @controllerName Dio_gmail
 * @author Dio
 *
 */
class Dio_gmail_ci{
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		
		$this->CI =& get_instance();
		$this->CI->load->library(array('email'));
		
		$this->config = $this->gmail_setting(); 
	}
		
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : gmail 基本參數設定
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function gmail_setting(){
		
		return Array(
				//'protocol'  => 'smtp',
				//'smtp_host'  => 'ssl://smtp.gmail.com',
				'smtp_host'  => 'smtp.gmail.com',
				'smtp_port'  => '465',
				'smtp_user'  => 'froghip69@gmail.com',
				'smtp_pass'  => 'fishsaut',
				'smtp_timeout'  => '60',
				'mailtype'  => 'html' ,
				'validation' => TRUE
		);
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 發送 Email 
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function send_email($data){
		
		$this->CI->email->initialize($this->config);
		
		$this->CI->email->from($data['info']['from'], $data['info']['subject']);
		$this->CI->email->to($data['info']['to']);
			
		$this->CI->email->subject($data['info']['subject']);
		$this->CI->email->message($data['info']['message']);
		
		if ($this->CI->email->send()){
			return true;
		}else{
			/*
			 echo $this->CI->email->print_debugger();
			 exit;
			 */
			return false;
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 電子報
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edm($data){
		
		$data['info']['from']     = DIO_EMAIL;
		$data['info']['to']       = $data['email'];
		$data['info']['company']  = DIO_COMPANY;
		$data['info']['webpage']  = DIO_WEBPAGE;
		$data['info']['subject']  = $data['title'];
		$data['info']['message']  = $data['content'];
		$data['info']['template'] = $this->CI->load->view('widget/email/edm.tpl',$data,true);
	
		return $this->send_email($data);
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 聯絡我們
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function contact($data){
			
		$data['info']['from']    = DIO_EMAIL;
		$data['info']['to']      = DIO_EMAIL;
		$data['info']['company'] = DIO_COMPANY;
		$data['info']['webpage'] = DIO_WEBPAGE;
		$data['info']['subject'] = "您有一筆來自 【" .$data['info']['company']. "】的訊息" ;
		$data['info']['message'] = $this->CI->load->view('widget/email/contact.tpl',$data,true);
		
		return $this->send_email($data);
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 註冊成功
	 *
	 * @access	public
	 * @param	
	 * @return	
	 */
    public function join($data){

    	$data['info']['from']    = DIO_EMAIL;
    	$data['info']['to']      = $data['email'];
    	$data['info']['company'] = DIO_COMPANY;
    	$data['info']['webpage'] = DIO_WEBPAGE;
    	$data['info']['subject'] = "恭喜您註冊成功，歡迎加入 【" .$data['info']['company']. "】" ;
    	$data['info']['message'] = $this->CI->load->view('widget/email/join.tpl',$data,true);
    
        return $this->send_email($data);
    } 
    
    // --------------------------------------------------------------------

	/**
	 * 方法 : 忘記密碼
	 *
	 * @access	public
	 * @param	
	 * @return	
	 */
    public function forget($param){
    
    	$data['info']['new_password'] = uniqid();
    	
    	$data['info']['from']    = DIO_EMAIL;
    	$data['info']['to']      = $param['email'];
    	$data['info']['company'] = DIO_COMPANY;
    	$data['info']['webpage'] = DIO_WEBPAGE;
    	$data['info']['subject'] = "【" .$data['info']['company']. "】忘記密碼通知" ;
    	$data['info']['message'] = $this->CI->load->view('widget/email/forget.tpl',$data,true);
    	
      	if ($this->send_email($data)){
      	    //更新會員密碼  
      	    if( !empty($data['info']['to']) ){
      		   $this->CI->db->where('TRIM(email)',$data['info']['to']);
      	       $this->CI->db->update('customer',array('password' => MD5($data['info']['new_password']) ));
      	    }
      	    return true;  
      	}else{
      	    return false;  
      	}       	
        
    }

    // --------------------------------------------------------------------
    
    /**
     * 方法 : 更新密碼
     *
     * @access	public
     * @param
     * @return
     */
    public function forget_update($data){
    	 
    	$data['info']['new_password'] = uniqid();
    	 
    	$data['info']['from']    = DIO_EMAIL;
    	$data['info']['to']      = $data['email'];
    	$data['info']['company'] = DIO_COMPANY;
    	$data['info']['webpage'] = DIO_WEBPAGE;
    	$data['info']['subject'] = "【" .$data['info']['company']. "】更新密碼通知" ;
    	$data['info']['message'] = $this->CI->load->view('widget/email/forget_update.tpl',$data,true);
    	 
    	if ($this->send_email($data)){
    		return true;
    	}else{
    		return false;
    	}
    
    }
   
   // --------------------------------------------------------------------

	/**
	 * 方法 : 訂單訂購成功發送 Email 
	 *
	 * @access	public
	 * @param	
	 * @return	
	 */
    public function order($data){
    	
    	$data['info']['from']    = DIO_EMAIL;
    	$data['info']['to']      = !empty($_SESSION['customer_info']['email']) ? $_SESSION['customer_info']['email'] : $data['order']['master']['email'];
    	$data['info']['company'] = DIO_COMPANY;
    	$data['info']['webpage'] = DIO_WEBPAGE;
    	$data['info']['subject'] = "您有一筆來自 【" .$data['info']['company']. "】的訊息" ;
    	$data['info']['data']    = $data;
    	$data['info']['message'] = $this->CI->load->view('widget/email/order.tpl',$data,true);
    	  
    	return $this->send_email($data);
    }
    
    // --------------------------------------------------------------------
    
    /**
     * 方法 : 預購單成功發送 Email
     *
     * @access	public
     * @param
     * @return
     */
    public function order_pre($data){
        	
    	$data['info']['from']    = DIO_EMAIL;
    	$data['info']['to']      = DIO_EMAIL;
    	$data['info']['company'] = DIO_COMPANY;
    	$data['info']['webpage'] = DIO_WEBPAGE;
    	$data['info']['subject'] = "您有一筆來自 【" .$data['info']['company']. "】的訊息" ;
    	$data['info']['data']    = $data;
    	$data['info']['message'] = $this->CI->load->view('widget/email/order_pre.tpl',$data,true);

    	return $this->send_email($data);
    }

 }
 

/* End of file email.php */