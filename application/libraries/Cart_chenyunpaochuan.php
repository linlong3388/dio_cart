<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 類別功能 : 購物車(Cart) / 針對陳允宝泉專案 。
 *
 * 說明 : 針對禮盒組合部份，用 SESSION 暫存購物資訊。
 *
 * @controllerName Cart_chenyunpaochuan
 * @author Dio
 *
 */
class Cart_chenyunpaochuan{

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){

		$this->CI =& get_instance();
	
		$this->CI->load->helper(array('dio_business'));
		
		$this->p    = 'product';
		$this->pc   = 'product_comb';
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 重組禮盒商品資料
	 *
	 * @access	public
	 * @param	格式 Array(    
	 *                 [0] => 1|2|1200|2|6|product/59b4d90b918ed9863_0.jpg    
	 *                 [1] => 2|3|980|1|5|product/59b4d9f49cde6454_0.jpg )
	 * @return              
	 */
	public function reSetCartCombProductInfo( $param ){
		            
		$data       = $this->getProductTypeId_1();
		
		if( empty($data) )
		    return false;
		
		foreach ($data as $key=>$val){ //購物車/禮盒類型
			
		   //如果product_id相同
	       if( $_SESSION['my_order']['productTypeId_1'] == $val['product_id'] ){

	       	  $this->calcCombProductInfo( $key ,$param );
	       	  $this->calcProductInfo($key);
	       	  
	       	  return true;
	       }
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 計算禮盒資料
	 *
	 * @access	public
	 * @param	
	 * @return $_SESSION
	 */
	public function calcProductInfo($cart_id){
		
		$price = 0;
		$total = 0;
		
		foreach ($_SESSION['my_order']['cart'][$cart_id]['box'] as $row){
			$price += $row['total_sub'];
		}
		 
		//再加上禮盒製作費用
		$price = ( $price + $_SESSION['my_order']['cart'][$cart_id]['price_box']);
		
		$_SESSION['my_order']['cart'][$cart_id]['price']  = $price;
		$_SESSION['my_order']['cart'][$cart_id]['total']  = $_SESSION['my_order']['cart'][$cart_id]['entity'] * $price;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 計算禮盒(明細)資料
	 *
	 * @access	public
	 * @param	product_id|product_comb_id|price|unit_pre|entity|image
	 * 
	 *          格式 Array(
	 *                 [0] => 1|2|1200|2|6|product/59b4d90b918ed9863_0.jpg
	 *                 [1] => 2|3|980|1|5|product/59b4d9f49cde6454_0.jpg )
	 *                 
	 * @return $_SESSION     
	 */
	public function calcCombProductInfo( $cart_id ,$param ){
		
		//重設禮盒明細
		if( isset($_SESSION['my_order']['cart'][$cart_id]['box']) )  
			unset($_SESSION['my_order']['cart'][$cart_id]['box']);
		
		foreach ( $param as $key=>$val ){
			
			$data = explode('|', $val);

			//僅寫入已選取的商品資料
			if( count($data) > 1 ){
				
				$_SESSION['my_order']['cart'][$cart_id]['box'][$key]['product_id']      = $data[0];
				$_SESSION['my_order']['cart'][$cart_id]['box'][$key]['product_comb_id'] = $data[1];
				$_SESSION['my_order']['cart'][$cart_id]['box'][$key]['price']           = $data[2];
				$_SESSION['my_order']['cart'][$cart_id]['box'][$key]['unit_pre']        = $data[3];
				$_SESSION['my_order']['cart'][$cart_id]['box'][$key]['entity']          = $data[4];
				$_SESSION['my_order']['cart'][$cart_id]['box'][$key]['total_sub']       = $data[2]*$data[4];
				$_SESSION['my_order']['cart'][$cart_id]['box'][$key]['image']           = $data[5];
				$_SESSION['my_order']['cart'][$cart_id]['box'][$key]['barcode']         = $data[6];
				$_SESSION['my_order']['cart'][$cart_id]['box'][$key]['name']            = $data[7];
			}
		}
		
		//重設陣列索引
		$_SESSION['my_order']['cart'][$cart_id]['box'] = array_values($_SESSION['my_order']['cart'][$cart_id]['box']);
		
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得購物車禮盒資料
	 *        type_id = 1
	 *
	 * @access	public
	 * @param
	 * @return array
	 */
	public function getProductTypeId_1(){
		
		if( empty($_SESSION['my_order']['cart']) )
			return array();
		
		$data = array();
		
		foreach ($_SESSION['my_order']['cart'] as $key=>$row){
			if( $row['type_id'] == 1){
				$data[$key] = $row;
			}
		}
		
		return $data;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 視圖 / 取得購物車禮盒 / 組圖
	 *       顯示用
	 *       
	 * @access	public
	 * @param
	 * @return array
	 */
	public function view_getCartProductTypeId1Img($product_id){
		
		//取得單筆購物車資料
		$param = $this->CI->cls_cart->getCartInfoByProductID( $product_id );
		
		$data = array_fill(0, 4, '#');
		$box  = array();
		
		if( empty($param['box']) ) return $data;

		//取得盒裝組數
		foreach ($param['box'] as $key=>$val){
			for( $i=0 ; $i<$val['unit_pre'] ; $i++ ){
				array_push($box, base_url( DIO_PATH_PIMG.$val['image'] ));
			}
		}
		
		//取得最後組圖
		foreach ($data as $key=>$val){
		   foreach ($box as $key2=>$val2){
				if($key == $key2) 
					 $data[$key] = $val2;
		   }
		}		
		
		return $data;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 視圖 / 取得購物車禮盒 / 商品
	 *       顯示用
	 *
	 * @access	public
	 * @param
	 * @return array
	 */
	public function view_getCartProductTypeId1Product(){
		
		//取得所有購物車資料
		$param = $this->CI->cls_cart->select_all();
		$data  = array();
		
		if( !$this->isProductTypeId1Box($param) )
			 return $data;
		
		foreach ($param as $key=>$val){
			if($val['type_id'] == 1) {
				$data[$key] = $val;
			}
		}
		
		return  array_values($data);
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 視圖 / 取得購商品禮盒資料 / 已註記
	 *       顯示用
	 *
	 * @access	public
	 * @param  [已選購商品](單筆資料)、[所有商品]
	 * @return array
	 */
	public function view_getProductTypeId1ListIsCheck($param_cart ,$param_all){
		
		if( empty($param_cart) )
			return $param_all;
		
		if( empty($param_cart['box']) )
			return $param_all;
		
		foreach ($param_all as $key=>$val){
			foreach ($val['comb'] as $key2=>$val2){
				
				foreach ($param_cart['box'] as $key3=>$val3){
					if( $val2['product_comb_id'] == $val3['product_comb_id'] ){
						$param_all[$key]['comb'][$key2]['isCheck'] = 1;
					}	
				}
				
			}
		}
		
		return $param_all;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 判斷購物車禮盒是否有明細資料
	 *       
	 * @access	public
	 * @param  
	 * @return array
	 */
	public function isProductTypeId1Box($param_cart){
		
		if( empty($param_cart) )
			return false;
		
		foreach ($param_cart as $row){
			if( empty($row['box']) )
				return false;
			else 
				return true;
		}
		
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 判斷購物車禮盒單位是否組合正確o
	 *         $combUnit 為 n入(個數) 暫不使用
	 *
	 * @access	public
	 * @param  
	 * @return array
	 */
	public function isProductTypeId1BoxCombOk( $combUnit = 0 ){
		
		$products = $this->getProductTypeId_1();
		
		foreach ($products as $row){  //單筆購物車組合商品資料
			if( $_SESSION['my_order']['productTypeId_1'] == $row['product_id'] ){
				
				if( empty($row['box']) )
					return false;
					
				$unit_pre = 0;
				
				foreach ($row['box'] as $row2){ //組合商品資料
					$unit_pre += $row2['unit_pre'];
				}
				
				if( $unit_pre != 4 ) //固定四組
				  	return false;
				
			}
		}
		
		return true;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 重設購物車資料(針對組合商品)
	 *         有些禮盒未能組合完畢，便離開該頁面，因此需要重設購物車資料
	 *         採用遞迴技巧
	 *
	 * @access	public
	 * @param
	 * @return array
	 */
	public function resetCartData(){
		
		if( $this->CI->cls_cart->count_all_entity() <= 0 )
		   return false;		
		
		$cart = $this->CI->cls_cart->select_all();
		
		foreach ($cart as $key=>$row){
			
			//禮品組合
			if($row['type_id'] == 1){
				
				//如果為空禮盒，則刪除該項目
				if( empty($row['box']) ){
					$this->CI->cls_cart->del_id($key);
					$this->resetCartData();
					break;
				}
								
				//如果為禮盒單位不符
				$unit_pre = 0;
				
				foreach ($row['box'] as $row2){
					$unit_pre += $row2['unit_pre'];
				}
				
				if( $unit_pre != 4 ){ //固定四組
					$this->CI->cls_cart->del_id($key);
					$this->resetCartData();
					break;
				}
				
			}
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 清空組合商品資料，但保留禮盒基本資料
	 *
	 * @access	public
	 * @param
	 * @return array
	 */
	public function clearCombData($product_id){
		
		if(isset($_SESSION['my_order']['cart'])){
		
			foreach ($_SESSION['my_order']['cart'] as $key=>$val) {
				
				if($val['product_id'] == $product_id ) {

				   $_SESSION['my_order']['cart'][$key]['price'] = $_SESSION['my_order']['cart'][$key]['price_box']; 
			       $_SESSION['my_order']['cart'][$key]['total'] = $_SESSION['my_order']['cart'][$key]['price_box'];
					
				   unset($_SESSION['my_order']['cart'][$key]['box']);
				   
				   return false;
				}   
			}
		}
		
	}
	
}


/* End of file Cart_chenyunpaochuan.php */