<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 處理[折價券]相關。
 *   僅適用於前端購物流程 (因為有採用SESSION) 
 *   
 * @controllerName Coupon
 * @author Dio
 *
 */
class Coupon{

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){

		$this->CI =& get_instance();
		$this->CI->load->helper(array('base'));
	}

	// --------------------------------------------------------------------
	
	/**
	 * 判斷是否啟用
	 *
	 * @access	public
	 * @param
	 * @return  return t/f
	 */
	public function is_enabled(){

		if( empty($_SESSION['sys_info']['promo']['coupon']) ){
			return false;
		}else{
			return true;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	 * 判斷是否符合折價券條件
	 *    for 結帳步驟專用，需有登入SESSION
	 *  
	 * @access	public
	 * @param	
	 * @return  return t/f
	 */
	public function is_coupon(){

		$total = $this->CI->cls_cart->count_all_total();
		
		if( $this->is_enabled() ){
		   if($total >= $_SESSION['sys_info']['promo']['coupon']['total']){
		      return true; 	
		   }
		}		
		
		return false;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 *  寫入會員折價券
	 *     for 結帳步驟專用，需有登入SESSION
	 *
	 * @access	public
	 * @param
	 * @return  
	 */
	public function write($order_id){
		
		$total = $this->CI->cls_cart->count_all_total();
		
		//取得折價券張數
		$entity = floor( $total / $_SESSION['sys_info']['promo']['coupon']['total'] );
		
		if( $entity > 0 ){
						
			if( $_SESSION['sys_info']['promo']['coupon']['code_type'] == 0 ){
				$total = $total*$_SESSION['sys_info']['promo']['coupon']['code_val']*0.01;
			}else{
				$total = $_SESSION['sys_info']['promo']['coupon']['code_val'];
			}
			
			for($i=0 ; $i<$entity ;$i++){
			   
			   $data = array(
					'order_id'    => $order_id,
					'customer_id' => $_SESSION['customer_info']['customer_id'],
					'name'        => $_SESSION['sys_info']['promo']['coupon']['name'],
					'code'        => $_SESSION['sys_info']['promo']['coupon']['code'],
			   		'code_use'    => 0, //g_coupon_id(),
			   		'total'       => $total,
					'sdate'       => date('Y-m-d H:i:s'),
					'edate'       => $_SESSION['sys_info']['promo']['coupon']['udate'],
			   		'status'      => 2,
					'cdate'       => date('Y-m-d H:i:s')
			   );
			
			   $this->CI->db->insert('customer_coupon' ,$data);
		    }
		}

	}

	// --------------------------------------------------------------------
	
	/**
	 *  取得會員折價券
	 *
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function get($code){
		
		$this->CI->db->where('code' ,$code);
		$this->CI->db->where('customer_id' ,$_SESSION['customer_info']['customer_id']);
		$this->CI->db->where('status' ,1);
		
		$query = $this->CI->db->get('customer_coupon')->row_array();
		
		return $query; 
	}
	
	// --------------------------------------------------------------------
	
	/**
	 *  取得會員折價券( for使用代碼 )
	 *  
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getByCodeUse($code_use){
		
		$this->CI->db->where('code_use' ,$code_use);
		$this->CI->db->where('customer_id' ,$_SESSION['customer_info']['customer_id']);
		$this->CI->db->where('status' ,1);
		
		$query = $this->CI->db->get('customer_coupon')->row_array();
		
		return $query;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 *  更新折價券狀態
	 *
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function status($code ,$status){
		
		$this->CI->db->where('code' ,$code);
		$this->CI->db->update('customer_coupon' ,array('status' => $status));
	}
	
	// --------------------------------------------------------------------
	
	/**
	 *  更新折價券狀態(依欄位)
	 *
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function statusByField($field ,$status){
		
		foreach ($field as $key=>$val){
			$this->CI->db->where($key ,$val);
		}
		
		$this->CI->db->update('customer_coupon' ,array('status' => $status));
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 依到期日取消折價券
	 *   規則 1) 判斷到期日，將狀態改為2(過期)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function enabled( $customer_id ){
	
		$sql = "UPDATE `customer_coupon` SET `status`='2'
   	 		     WHERE `status`= '1'
   	 		    AND SUBSTRING(edate ,1,10) <= '".date('Y-m-d')."'
   	 		    AND `customer_id` = '".$customer_id."'";
			
		$this->CI->db->query($sql);
	}
	
	
	// --------------------------------------------------------------------
	
	/**
	 * 依到期日取消折價券(多筆)
	 *   建立 cronjob 於每日 24:00進行條件判斷
	 *   規則 1) 判斷到期日，將狀態改為2(過期) 
	 *  
	 * @access	public
	 * @param
	 * @return
	 */
	public function enabledCronJob(){
		
		$sql = "UPDATE `customer_coupon` SET `status`='2'
   	 		     WHERE `status`= '1'
   	 		    AND SUBSTRING(edate ,1,10) <= '".date('Y-m-d')."'";
			
		$this->CI->db->query($sql);
	}
    
}


/* End of file Coupon.php */