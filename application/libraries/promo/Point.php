<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 處理[還原金]相關。
 *   注意: 使用前提會員需為登入狀態，因為以登入SESSION為前提
 *   
 * @controllerName Point
 * @author Dio
 *
 */
class Point{

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){

		$this->CI =& get_instance();
		$this->CI->load->helper(array('base'));
	}

	// --------------------------------------------------------------------
	
	/**
	 * 判斷是否啟用
	 *
	 * @access	public
	 * @param
	 * @return  return t/f
	 */
	public function is_enabled(){

		if( empty($_SESSION['sys_info']['promo']['point']) ){
			return false;
		}else{
			return true;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	 * 判斷是否符合還原金(使用)條件
	 * 
	 * @access	public
	 * @param	
	 * @return  return t/f
	 */
	public function is_point_use( $point ){

		if( $this->is_enabled() ){
		   if($point > $this->get($_SESSION['customer_info']['customer_id'])){
		      return false; 	
		   }else{
		   	  return true;
		   }
		}		
		
		return false;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 判斷是否符合還原金(累積)條件
	 *
	 * @access	public
	 * @param
	 * @return  return t/f
	 */
	public function is_point_write(){
	
		$total = $this->CI->cls_cart->count_all_total();
	
		if( $this->is_enabled() ){
			if($total >= $_SESSION['sys_info']['promo']['point']['total']){
				return true;
			}else{
				return false;
			}
		}
	
		return false;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 *  寫入會員還原金
	 *  
	 *
	 * @access	public
	 * @param
	 * @return  
	 */
	public function write($order_id){
		
		$total = $this->CI->cls_cart->count_all_total();
		
		if($_SESSION['sys_info']['promo']['point']['total'] < $total ){
		
		   $total = floor( ($_SESSION['sys_info']['promo']['point']['discount']/100)*$total );
		
		   $data = array(
				'order_id'    => $order_id,
				'customer_id' => $_SESSION['customer_info']['customer_id'],
				'name'        => '還原金',
				'total'       => $total,
				//'sdate'       => date('Y-m-d H:i:s'),
				//'edate'       => $_SESSION['sys_info']['promo']['point']['udate'],
				'status'      => 0,
				'cdate'       => date('Y-m-d H:i:s')
		   );
			
		   $this->CI->db->insert('customer_point' ,$data);
		}
		
	}

	// --------------------------------------------------------------------
	
	/**
	 *  取得會員還原金(可用餘額)
	 *
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function get($customer_id){
		
		$sql = "SELECT ( IFNULL( SUM(total),0 ) 
				         - IFNULL( (SELECT SUM(op.total) FROM order_promo op 
				              LEFT JOIN `order` o ON op.order_id = o.order_id
				             WHERE op.item_id = '1' 
				             AND o.customer_id = '".$customer_id."'
				             AND op.status = 1 ),0) ) as total 
				  FROM customer_point
			        WHERE customer_id = '".$customer_id."'
			        AND status=1 ";
		
		$query = $this->CI->db->query($sql)->row_array();
		
		return empty($query['total']) ? 0 : $query['total'];
	}
	
	// --------------------------------------------------------------------
	
	/**
	 *  取得會員還原金(使用餘額)
	 *
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function get_use($customer_id){
		
		$sql = "SELECT SUM(op.total) as total
				  FROM order_promo op 
				LEFT JOIN `order` o ON op.order_id = o.order_id
				  WHERE op.item_id = '1' 
				  AND o.customer_id = '".$customer_id."'
				  AND op.status = 1 ";
		
		$query = $this->CI->db->query($sql)->row_array();
		
		return empty($query['total']) ? 0 : $query['total'];
	}
		
	// --------------------------------------------------------------------
	
	/**
	 *  取得會員還原金(使用記錄)
	 *
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function get_log_use($customer_id){
	
		$sql = "SELECT o.order_show_id ,op.* 
				  FROM order_promo op
				LEFT JOIN `order` o ON op.order_id = o.order_id  
                 WHERE op.item_id = 1
				 AND op.status = 1 
				 AND op.total > 0
				 AND o.customer_id = '".$customer_id."'";
	
		$query = $this->CI->db->query($sql)->result_array();
	
		return $query;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 *  取得會員還原金(累積記錄)
	 *
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function get_log($customer_id){
	
		$sql = "SELECT o.order_show_id ,cp.* 
				  FROM customer_point cp
				LEFT JOIN `order` o ON cp.order_id = o.order_id  
                 WHERE cp.status = 1 
				 AND cp.total > 0
				 AND o.customer_id = '".$customer_id."'";
	
		$query = $this->CI->db->query($sql)->result_array();
	
		return $query;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 *  更新會員還原金狀態
	 *
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function status($order_id ,$status){
		
		//還原金累積
		/*
		$this->CI->db->where('order_id' ,$order_id);
		$this->CI->db->update('customer_point' ,array('status' => $status));
		*/
		
		//還原金使用
		$this->CI->db->where('order_id' ,$order_id);
		$this->CI->db->update('order_promo' ,array('status' => $status));
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 依到期日取消還原金
	 *   規則 1) 判斷到期日，將狀態改為2(過期)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function enabled( $customer_id ){
	
		$sql = "UPDATE `customer_point` SET `status`='2'
   	 		     WHERE `status`= '1'
   	 		    AND SUBSTRING(edate ,1,10) <= '".date('Y-m-d')."'
   	 		    AND `customer_id` = '".$customer_id."'";
			
		$this->CI->db->query($sql);
	}
	
	
	// --------------------------------------------------------------------
	
	/**
	 * 依到期日取消還原金
	 *   建立 cronjob 於每日 24:00進行條件判斷
	 *   規則 1) 判斷到期日，將狀態改為2(過期) 
	 *  
	 * @access	public
	 * @param
	 * @return
	 */
	public function enabledCronJob(){
		
		$sql = "UPDATE `customer_point` SET `status`='2'
   	 		     WHERE `status`= '1'
   	 		    AND SUBSTRING(edate ,1,10) <= '".date('Y-m-d')."'";
			
		$this->CI->db->query($sql);
	}
    
}


/* End of file Point.php */