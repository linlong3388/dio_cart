<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 處理[會員VIP]相關。
 *   注意: 使用前提會員需為登入狀態，因為以登入SESSION為前提
 *
 * @controllerName VIP
 * @author Dio
 *
 */
class VIP{

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){

		$this->CI =& get_instance();
	}

	// --------------------------------------------------------------------

	/**
	 * 判斷是否為 VIP 會員
	 * 
	 * @access	public
	 * @param	
	 * @return  return t/f
	 */
	public function is_vip(){

		if($_SESSION['customer_info']['level'] == 0){
			return false;
		}else{
			return true;
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 判斷是否符合昇等 VIP會員條件
	 *   傳入值為購物車總額
	 *
	 * @access	public
	 * @param   
	 * @return  return t/f
	 */
	public function is_up($total){
	
		//昇等
		if($_SESSION['customer_info']['level'] == 0){

			if($_SESSION['sys_info']['promo']['vip']['total'] <= $total){
				return true;
			}else{
				return false;
			}
			
		//續約僅限一次
		}else{
			
			if($_SESSION['customer_info']['level'] < 2){
				
				if($_SESSION['sys_info']['promo']['vip']['ctotal'] <= $total){
					return true;
				}else{
					return false;
				}
			}
		}
		
	}

	// --------------------------------------------------------------------
	
	/**
	 * 折扣金額
	 *   傳入值為購物車總額
	 *
	 * @access	public
	 * @param
	 * @return  return t/f
	 */
	public function price($total){
	
       return ($_SESSION['sys_info']['promo']['vip']['discount']/100)*$total;
	}
	
   // --------------------------------------------------------------------
   
   /**
    * 昇等會員VIP狀態
    *   
    *
    * @access	public
    * @param
    * @return  
    */
   public function up(){
   	
   	  //昇等
   	  if($_SESSION['customer_info']['level'] == 0){
   		
   	 	 $data = array(
   	 			  'vip_sdate' => date('Y-m-d H:i:s'),
   	 			  'vip_edate' => date('Y-m-d H:i:s', strtotime('+1 years')),
   	 			  'level'     => $_SESSION['customer_info']['level'] + 1 
   	 	 );
   	 	
   	 	 $this->CI->db->where('customer_id' ,$_SESSION['customer_info']['customer_id']);
   	 	 $this->CI->db->update('customer' ,$data);
   	 	
   	  //續約僅限一次
   	  }else{
   		
   	 	 if($_SESSION['customer_info']['level'] < 2){
   	 		
   	 		$data = array(
   	 				'vip_edate' => date('Y-m-d H:i:s', strtotime('+1 year', strtotime($_SESSION['customer_info']['vip_edate']))) ,
   	 				'level'     => $_SESSION['customer_info']['level'] + 1
   	 		);
   	 		
   	 		$this->CI->db->where('customer_id' ,$_SESSION['customer_info']['customer_id']);
   	 		$this->CI->db->update('customer' ,$data);
   	 	 }
   	 }
   	 
   }
   
   // --------------------------------------------------------------------
    
   /**
    * 降等會員VIP狀態
    *   規則 1) 判斷到期日，將會員等級1或2降等為0 
    *
    * @access	public
    * @param
    * @return
    */
   public function down( $customer_id ){
   	
   	 $sql = "UPDATE `customer` SET `level`='0' 
   	 		  WHERE (`level`= '1' OR `level`= '2')
   	 		  AND SUBSTRING(vip_edate ,1,10) <= '".date('Y-m-d')."'
   	 		  AND `customer_id` = '".$customer_id."'";
   
   	 $this->CI->db->query($sql);
   }
   
   
   // --------------------------------------------------------------------
    
   /**
    * 降等會員VIP狀態
    *   建立 cronjob 於每日 24:00 (23:50 提前)進行條件判斷
    *   規則 1) 判斷到期日，將會員等級1或2降等為0 
    *
    * @access	public
    * @param
    * @return
    */
   public function downCronJob(){
   	
   	 $sql = "UPDATE `customer` SET `level`='0' 
   	 		  WHERE (`level`= '1' OR `level`= '2')
   	 		  AND SUBSTRING(vip_edate ,1,10) <= '".date('Y-m-d')."'";
   
   	 $this->CI->db->query($sql);
   }
    
}


/* End of file VIP.php */