<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 類別功能 : 將csv檔轉為陣列格式
 * 說明 :
 *
 * @author Dio
 * @date 2016/03/11
 */

class Csv_to_array
{
	
	private $title ;
	private $file ;
	private $rows ;
	private $count = 0 ;


	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		$this->CI =& get_instance();
	}
	

	/**
	 * 方法 : 設置基本資料
	 *
	 * @access	public
	 * @param   $name 必須為主機檔案路徑的檔名，而非url路徑
	 * @return
	 */
	public function _set($name){
		
		$this->file = new SplFileObject($name);
		
		$this->file->setFlags(
				       SplFileObject::READ_CSV |
				       SplFileObject::SKIP_EMPTY |
				       SplFileObject::READ_AHEAD);
		
		$this->file->setCsvControl(",");
		
		$this->setArray();
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得陣列格式資料
	 * 說明 :
	 *
	 * @access	public
	 * @param
	 * @return array
	 */
	private function setArray(){
		
		$this->title = null;
		
		foreach ($this->file as $row)
		{
			if ($this->title === null)
			{
				$this->title = $row;
				continue;
			}

			$this->rows[] = array_combine($this->title, $row);
            $this->count++ ; 			
       }

	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得陣列格式資料
	 * 說明 : 
	 *
	 * @access	public
	 * @param
	 * @return array
	 */
	public function getArray()
	{
       return $this->rows;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得總筆數
	 * 說明 :
	 *
	 * @access	public
	 * @param
	 * @return array
	 */
	public function getCount()
	{
	   return $this->count;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得標頭陣列
	 * 說明 :
	 *
	 * @access	public
	 * @param
	 * @return array
	 */
	public function getTitle()
	{
		return $this->title;
	}
	
}
