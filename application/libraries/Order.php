<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 類別功能 : 訂單類別。
 *
 * @controllerName order
 * @author Dio
 *
 */
class Order{

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){

		$this->CI =& get_instance();

		$this->order = 'order';
		$this->order_detail = 'order_detail';

		//訂單主表的編號
		$this->order_id = 0;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 訂單查詢
	 *
	 * @access	public
	 * @param	[訂單編號][會員編號][日期區間]
	 * @return
	 */
	public function member_order_select( $data ){

		//WHERE語句
		$sql_where = "";

		//[訂單編號]搜尋(預設全部)
		if( !empty($data['order_id']) ){
			$sql_where .= " WHERE `order_id` = '{$data['order_id']}' ";
		}else{
			$sql_where .= " WHERE `order_id` <> '' ";
		}

		//[日期區間](預設全部)
		if( !empty($data['date1']) && !empty($data['date2']) ){
			$sql_where .= " AND SUBSTR(cdate,1,10) BETWEEN '".$data['date1']."' AND '".$data['date2']."'";
		}else{
			$sql_where .= "";
		}

		//[會員編號]
		$sql_where .= " AND `customer_id` = '{$data['customer_id']}' ";

		$query = $this->CI->db->query("SELECT * FROM {$this->order} {$sql_where} ")->result_array();
			
		if($query){
			return $query;
		}else{
			return FALSE;
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 結帳流程
	 * 說明 :
	 *       流程 : 1) 寫入訂單主副表(訂單狀態為0)
	 *             2) 金流串接
	 *             3) 訂購成功/失敗
	 *
	 * @access	public
	 * @param	[data] 分[客戶資料]和[訂單資料]
	 * @return
	 */
	public function checkout($data){

		//寫入相關資料表
		$this->checkout_order_ins($data);
		$this->ins_order_total_item($data);

		//取出該訂單資料，並串接金流
		$this->CI->db->where('order_id' ,$this->order_id);
		$query = $this->CI->db->get('order_detail')->result_array();

		//if($_SESSION['my_order']['info']['pay_method'] == 'bank_transfer'){
		//銀行轉帳
		require_once(APPPATH.'libraries/cash_flow/Cash_flow.php');
		 
		$cashFlowObj = Cash_flow::getInstance('bank_transfer');
		$cashFlowObj->setParams(array('info' => "請匯款至銀行帳戶<p>銀行：國泰世華銀行 011<p>帳號：095-50-678939-1"));
		$data['cash_flow'] = $cashFlowObj->dataTransport();

		return $query;
		//return false;
		//}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 結帳流程 /寫入[訂單](主)(副)表
	 * 
	 * @access	public
	 * @param	[data] 分[客戶資料]和[訂單資料]
	 * @return  return t/f
	 */
	public function checkout_order_ins($data){

		//---------------------------------------------------------------------------
		// 訂單寫入主副表
		//---------------------------------------------------------------------------
		/*$entity = 0;
			$total = 0;

				
			foreach ($data['cart'] as $row){

			//再次比對該商品是否存在
			$this->db->where('product_id' ,$row['product_id']);
			$is_product_ary = $this->db->get('product')->row_array();

			if(empty($is_product_ary)){
			redirect('member/error_product?product_id='.$row['product_id'].'&name='.$row['name'] );
			}else{
			$entity += $row['entity'] ;
			$total += $row['total'] ;
			}
			}*/
			
		
		
		$data_master = array(
			                    'customer_id' => empty($data['info']['customer_id']) ? 0 : $data['info']['customer_id'] ,
                                 'first_name' => $data['info']['first_name'] ,
                                  'last_name' => $data['info']['last_name'] ,
			                        //'phone' => $data['info']['phone'] ,
                                     'mobile' => $data['info']['mobile'] ,
			                          'email' => $data['info']['email'] ,
                                    'conutry' => $data['info']['conutry'] ,
                                       'city' => $data['info']['city'] ,
                                        'zip' => $data['info']['zip'] ,
                                      'local' => $data['info']['local'] ,
                                    'address' => $data['info']['address'] ,
			                       'currency' => strtoupper($_SESSION['motion_currency']),
			                           'memo' => $data['info']['memo'] ,
			                         'status' => 1 , //訂單狀態(處理中)
                                      'cdate' => date('Y-m-d H:i:s')
		);
			
		$this->CI->db->insert('order' ,$data_master);

		$this->order_id = $this->CI->db->insert_id();

		foreach ($data['cart'] as $row) {
			$data_detail = array(
	       	                   'order_id' => $this->order_id,
				             'product_id' => $row['product_id'] ,
			           'product_speci_id' => $row['product_speci_id'] ,	
			               'product_name' => $row['name'] ,	
				          'product_color' => $row['prc_name'] ,                               
				                  'price' => $row['price'],
                                 'entity' => $row['entity'],
			                      'total' => $row['total'],
				                  'cdate' => date('Y-m-d H:i:s')
			);

			$this->CI->db->insert('order_detail' , $data_detail);
		}
			
	}

}


/* End of file customer_addr.php */