<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 類別功能 : 登入 Google 類別。
 *
 * @controllerName dio_google
 * @author Dio
 *
 */
class Dio_google{

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		
		$this->CI =& get_instance();
		$this->CI->load->helper(array('url'));
		$this->CI->load->library(array('google/Google_Client'));
		
		//由於CI的建構函數不包含傳入值，所以和Google的API衝突，需獨立引入
		require_once(APPPATH.'libraries/google/contrib/Google_Oauth2Service.php');
		
		//設定 Google[應用程式主控板] 的 帳/密
		$this->clientId = '780039590156-k0eee24uhaojh007vlu9eeord1p00820.apps.googleusercontent.com';
		$this->clientSecret = 'cQi2Zzc5Mu7AJF_GXusf4pPX'; 
	    $this->redirect_uri = getUserURL('sign_in/login_google');  

	    //初始化 Google API
        $this->gClient = new Google_Client();
        $this->gClient->setApplicationName('Login to ALL-in');
        $this->gClient->setClientId($this->clientId);
        $this->gClient->setClientSecret($this->clientSecret);
        $this->gClient->setRedirectUri($this->redirect_uri);
        
        $this->google_oauthV2 = new Google_Oauth2Service($this->gClient);
 	}                          

	// --------------------------------------------------------------------

	/**
	 * 方法 : 登入
	 *
	 * @access	public
	 * @param	
	 * @return
	 */
	public function login(){
		
	    $code = $this->CI->input->get('code');	
		
	    if( isset($code)){
	        $this->gClient->authenticate($code);
	        $_SESSION['google']['token'] = $this->gClient->getAccessToken();
	        header('Location: ' . filter_var($this->redirect_uri, FILTER_SANITIZE_URL));
         }
		         
        if (isset($_SESSION['google']['token'])) {
            $this->gClient->setAccessToken($_SESSION['google']['token']);
        }
        
        //登入成功，返回會員資料
        if ($this->gClient->getAccessToken()) {
      	    $gpUserProfile = $this->google_oauthV2->userinfo->get();
      	    return $gpUserProfile;
        }
      
        return array();
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得登入網址
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function get_login_url(){
		$authUrl = $this->gClient->createAuthUrl();
		return filter_var($authUrl, FILTER_SANITIZE_URL);
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 登出
	 *
	 * @access	public
	 * @param
	 * @return
	 */
    public function logout(){
        unset($_SESSION['google']['token']);
        
        $this->gClient->revokeToken(); //Reset OAuth access token
    }

}


/* End of file dio_fb */