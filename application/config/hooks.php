<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/


//----------------------------------------------------------------------
//
// 設置系統參數
//
//----------------------------------------------------------------------
$hook['post_controller_constructor'][] = array(
		'class'    => '',
		'function' => 'setSystemToSession',
		'filename' => 'setting.php',
		'filepath' => 'hooks',
		'params'   => ''
);


//----------------------------------------------------------------------
//
// 設置購物金參數
//
//----------------------------------------------------------------------
$hook['post_controller_constructor'][] = array(
		'class'    => '',
		'function' => 'setPromoFeedbackGold',
		'filename' => 'setting.php',
		'filepath' => 'hooks',
		'params'   => ''
);


//----------------------------------------------------------------------
//
// 設置VIP條件參數
//
//----------------------------------------------------------------------
$hook['post_controller_constructor'][] = array(
		'class'    => '',
		'function' => 'setPromoVIP',
		'filename' => 'setting.php',
		'filepath' => 'hooks',
		'params'   => ''
);


//----------------------------------------------------------------------
//
// 設置折價券條件參數
//
//----------------------------------------------------------------------
$hook['post_controller_constructor'][] = array(
		'class'    => '',
		'function' => 'setPromoCoupon',
		'filename' => 'setting.php',
		'filepath' => 'hooks',
		'params'   => ''
);


//----------------------------------------------------------------------
//
// 系統維護中
//
//----------------------------------------------------------------------
$hook['post_controller_constructor'][] = array(
		'class'    => '',
		'function' => 'is_enabled',
		'filename' => 'setting.php',
		'filepath' => 'hooks',
		'params'   => ''
);

