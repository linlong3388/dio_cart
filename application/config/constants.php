<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/*
 |--------------------------------------------------------------------------
 | (一)自行定義的全域變數
 |--------------------------------------------------------------------------
 |
 | 底下為自行定義的系統全域變數與各種設定。
 |
 */

/*
 |---------------------------------------------------------------
 | 定義MySQL錯誤訊息顯示
 |---------------------------------------------------------------
 */
define('DIO_DEBUG', true);


/*
 |---------------------------------------------------------------
 | 自行定義台北時區
 |---------------------------------------------------------------
*/
date_default_timezone_set('Asia/Taipei');


/*
 |---------------------------------------------------------------
 | 解決javascript alert 視窗亂碼
 |---------------------------------------------------------------
*/
ini_set('default_charset','utf-8');


/*
 |---------------------------------------------------------------
 | 定義密鑰
 |---------------------------------------------------------------
*/
define('DIO_KEY', '27b205035c328b16d8c8329c4b41e87e');


/*
 |---------------------------------------------------------------
 | 定義google圖形驗證密鑰
 |   前端和主機端
 |---------------------------------------------------------------
*/
define('DIO_GOOGLE_SITEKEY', '6LfHcjUUAAAAACjyF7zMzyz5PNM96HwISvsbJB1B');
define('DIO_GOOGLE_SECRET', '6LfHcjUUAAAAALqjNYao0f9M3ZhPf0fBTv2KTYqX');


/*
 |---------------------------------------------------------------
 | 定義 幣別 的前綴符號
 |---------------------------------------------------------------
*/
define('DIO_CURRENCY', '$');


/*
 |---------------------------------------------------------------
 | 定義圖片大小
 | (公式 1M = 1*1024*1024 )
 |---------------------------------------------------------------
*/
define('KB', 1024);
define('MB', 1048576);
define('GB', 1073741824);
define('TB', 1099511627776);


/*
 |---------------------------------------------------------------
 | 定義SESSION初始化
 |---------------------------------------------------------------
*/
session_name('dio_cart_session');
//session_start();


/*
 |---------------------------------------------------------------
 | 定義常用訊息
 |---------------------------------------------------------------
*/
define('DIO_MSG_SUCCESS_UPDATE', '資料庫異動成功!');
define('DIO_MSG_SUCCESS_INSERT', '您已成功新增一筆資料!');
define('DIO_MSG_SUCCESS_DELETE', '您已成功刪除一筆資料!');
define('DIO_MSG_SUCCESS_LOGOUT', '您已成功登出!');
define('DIO_MSG_ERROR_CATEGORY', '請先刪除該分類的所屬內容!');


/*
 |---------------------------------------------------------------
 | 定義系統基本資料 / 公司資料
 |
 | 參數說明:
 |    DIO_COMPANY      名稱
 |    DIO_MANAGER      負責人
 |    DIO_WEBPAGE      網站
 |    DIO_EMAIL        Email
 |    DIO_PHONE        電話
 |    DIO_FAX          傳真
 |    DIO_SERVICE_TIME 服務時段
 |    DIO_ADDRESS      地址
 |    DIO_MEMO         備註
 |---------------------------------------------------------------
*/

$dio_webname_pre = empty($_SESSION['sys_info']['web_name']) ? '' : $_SESSION['sys_info']['web_name'];
$dio_weburl_pre  = empty($_SESSION['sys_info']['web_url']) ? '' : $_SESSION['sys_info']['web_url'];
$dio_email_pre   = empty($_SESSION['sys_info']['web_email']) ? '' : $_SESSION['sys_info']['web_email'];
$dio_phone_pre   = empty($_SESSION['sys_info']['web_phone']) ? '' : $_SESSION['sys_info']['web_phone'];
$dio_fax_pre     = empty($_SESSION['sys_info']['web_fax']) ? '' : $_SESSION['sys_info']['web_fax'];
$dio_addr_pre    = empty($_SESSION['sys_info']['web_addr']) ? '' : $_SESSION['sys_info']['web_addr'];

define('DIO_WEBNAME', $dio_webname_pre);
define('DIO_COMPANY', $dio_webname_pre);
define('DIO_MANAGER', 'Dio');
define('DIO_WEBPAGE', $dio_weburl_pre);
define('DIO_EMAIL', $dio_email_pre);
define('DIO_PHONE', $dio_phone_pre);
define('DIO_FAX', $dio_fax_pre);
define('DIO_SERVICE_TIME', '週一至週五 09:00-18:00');
define('DIO_ADDRESS', $dio_addr_pre );
define('DIO_MEMO', '祝您購物愉快!');
define('DIO_LOGIN_FOOTER', '© dio_cart House');


/*
 |---------------------------------------------------------------
 | 定義系統基本資訊 / Gmail
 |---------------------------------------------------------------
*/
define('DIO_SMTP_HOST', 'ssl://smtp.gmail.com');
define('DIO_SMTP_PORT', '465');
define('DIO_SMTP_USER', 'linlong3388@gmail.com');
define('DIO_SMTP_PASS', 'xxxxxxx');
define('DIO_SMTP_TIMEOUT', '60');
define('DIO_SMTP_MAILTYPE', 'html');


/*
 |---------------------------------------------------------------
 |  系統後台名稱
 |---------------------------------------------------------------
*/
define('DIO_SYS_NAME', 'dio_cart House');


/*
 |---------------------------------------------------------------
 | 定義 預設的META頁籤資料
 |---------------------------------------------------------------
*/
define('DIO_META_TITLE', 'dio_cart House');
define('DIO_META_DESCRIPTION', 'dio_cart House');
define('DIO_META_KEYWORDS', 'dio_cart House');


/*
 |---------------------------------------------------------------
 | 定義常用路徑
 |---------------------------------------------------------------
*/
define('DIO_PATH_FILE', 'application/views/frontend/');
define('DIO_PATH_PIMG','resources/uploads/');
define('DIO_PATH_URL', ''); //(前台)移除 index.php
//define('DIO_PATH_URL', 'index.php/frontend/'); //(前台)保留 index.php

define('DIO_ADMIN_PATH_URL', 'backend/'); //(後台)移除 index.php
define('DIO_STORE_PATH_URL', 'storend/'); //(分店)移除 index.php
//define('DIO_ADMIN_PATH_URL', 'index.php/backend/'); //(後台)保留 index.php
