<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 前台(會員中心/預購訂單)管理首頁
 * @controllerName customer_order_pre
 * @author Dio
 *
 */
class customer_order_pre extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){

		parent::__construct();

		//載入模組		
		$this->load->helper(array('form','url','cookie','ctmall','database','base','motion','dio_transfer'));
		$this->load->library(array('form_validation','session'));
		
		//登入驗證
		if(!IsLoginCustomer()){
			redirect('sign_in/login');
		}         

		//載入model
		$this->load->model("backend/order_pre_model","order_pre");
		
		/*********************************
		 /* 設置幣別
		 *********************************/
		if( empty($_SESSION['motion_currency']) ){
			$_SESSION['motion_currency'] = 'twd';
		}
		
		$this->load->model("frontend/order_model","order");
		
		if($this->input->get('page')){ //頁碼
			$this->page = $this->input->get('page');
		}else{
			$this->page = 1;
		}
		
		$this->srh_page_per = 20; //每頁筆數
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists(){
			
		$pg = new Dio_pagination();
		$pg_ary = $pg->get_limit($this->input->get('page'),10);

		//列表
		$this->db->order_by('cdate' ,'DESC');
		$this->db->where('customer_id' ,$_SESSION['customer_info']['customer_id']);
		$data['query'] = $this->db->get('order_pre')->result_array();
		
		//總筆數
		$this->db->where('customer_id' ,$_SESSION['customer_info']['customer_id']);
		$data['query_total'] = $this->db->count_all_results('order_pre');
		
		//設定分頁
		$data['pagination']  = $pg->pagination($data['query_total'],5);
				
		$data['func'] = getUserMenu('customer_order_pre_lists');
		
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);		
		$this->load->view('frontend/customer/order_pre/lists.tpl');
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 明細
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function view(){
		
		$order_pre_id = $this->input->get('order_pre_id');
		
		//驗證會員所屬資料
		$isMyOrder = $this->isMyOrder($order_pre_id ,$_SESSION['customer_info']['customer_id']);
		if( !$isMyOrder ) {
			 show_404();
			 exit;
		}
	
		$data['query'] = $this->order_pre->shr_order_pre_info($order_pre_id);
	
        $data['func'] = getUserMenu('customer_order_pre_view');
		
		//檢視view
		$this->load->view('frontend/common/header.tpl' ,$data);		
		$this->load->view('frontend/customer/order_pre/view.tpl');
		
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 驗證是否屬於某會員的訂單
	 *
	 * @access	public
	 * @param	order_id
	 * @return  array
	 */
	public function isMyOrder($order_pre_id ,$customer_id){
		
		$this->db->where('order_pre_id' ,$order_pre_id);
		$this->db->where('customer_id' ,$customer_id);
		
		$query = $this->db->get('order_pre');
		
		if( $query->num_rows() > 0 ){
	        return true;
		}
		
		return false;
	}

}


/* End of file customer.tpl */
/* Location: ./application/controllers/customer_order_pre  */