<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 說明 : 處理 [優惠券]相關請求
 * @controllerName Discount_coupon
 * @author Dio
 *
 */
class Discount_coupon extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->helper(array('base','motion','url','is_valid'));
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 檢驗編號是否合法
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function is_valid(){
		
		 $code = trim($this->input->post('code'));
		$total = trim($this->input->post('total'));
		
		$data = is_valid_discount_coupon($code ,$total); 
		
		$data['err_msg'] = "";
		
		if($data['err_code'] == 1){
			$data['err_msg'] = "抱歉!無此優惠券編號!";
			echo json_encode($data);
			return false ;
		}
		
		if($data['err_code'] == 2){
			$data['err_msg'] = "抱歉!未達購物總額!";
			echo json_encode($data);
			unset($_SESSION['my_order']['discount_coupon']);
			return false;
		}
		
		if($data['err_code'] == 3){
			$data['err_msg'] = "抱歉!已超過使用此數!";
			echo json_encode($data);
			unset($_SESSION['my_order']['discount_coupon']);
			return false;
		}
		
	     //格式修正
	     $data['code_val_format'] = get_currency_format($data['code_val']);	
		
	     $_SESSION['my_order']['discount_coupon'] = $data;	
	   
 	     echo json_encode($data);
	}
	
}


/* End of file discount_coupon.php */
/* Location: ./application/controllers/frontend/promo/discount_coupon.php */