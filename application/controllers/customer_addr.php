<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 前台(會員中心/地址)管理首頁
 * @controllerName customer_addr
 * @author Dio
 *
 */
class Customer_addr extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){

		parent::__construct();

		//載入模組
		$this->load->helper(array('form','url','cookie','ctmall','database','base','motion'));
		$this->load->library(array('form_validation','session'));
		
		//登入驗證
		if(!IsLoginCustomer()){
			redirect('login/main');
		}         

		/*********************************
		 /* 設置幣別
		 *********************************/
		if( empty($_SESSION['motion_currency']) ){
			$_SESSION['motion_currency'] = 'twd';
		}
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists(){

		$data['query']       = $this->getAddrList($_SESSION['customer_info']['customer_id']);
		
		$data['func'] = 'customer_addr_lists';
		
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);
		$this->load->view('frontend/customer/addr/lists.tpl');
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add(){

		//$this->form_validation->set_rules('first_name','*收件人/姓','trim|required');
		$this->form_validation->set_rules('last_name','*收件人','trim|required|max_length[5]');
		$this->form_validation->set_rules('email','*Email','callback_check_email');
		$this->form_validation->set_rules('mobile','*手機','callback_check_mobile');
		$this->form_validation->set_rules('phone','*電話','trim');
		$this->form_validation->set_rules('local','*區號','trim|required');
		$this->form_validation->set_rules('address','*地址','trim|required');

		if ($this->form_validation->run() == TRUE){
				
			//$this->defaults('add', $_SESSION['customer_info']['customer_id']);
			
			$data = array(
                  'customer_id' => $_SESSION['customer_info']['customer_id'],
                   //'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                       'mobile' => $this->input->post('mobile'),
					    'phone' => $this->input->post('phone'),
					    'email' => $this->input->post('email'),
                        'local' => $this->input->post('local'),
                      'address' => $this->input->post('address'),
                        'cdate' => date('Y-m-d H:i:s')
			);

			$this->db->insert('customer_addr',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
			
			redirect('customer_addr/lists');
				
		}else{
			
			$this->db->where('customer_id' ,$_SESSION['customer_info']['customer_id']);
			$data['query'] = $this->db->get('customer_addr')->result_array();
	
			$data['func'] = 'customer_addr_add';
			
			$this->load->view('frontend/common/header.tpl',$data);
			$this->load->view('frontend/customer/addr/add.tpl');
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit(){
			
		//$this->form_validation->set_rules('first_name','*收件人/姓','trim|required');
		$this->form_validation->set_rules('last_name','*收件人','trim|required|max_length[5]');
		$this->form_validation->set_rules('email','*Email','callback_check_email');
		$this->form_validation->set_rules('mobile','*手機','callback_check_mobile');
		$this->form_validation->set_rules('phone','*電話','trim');
		$this->form_validation->set_rules('local','*地址(區碼)','trim|required');
		$this->form_validation->set_rules('address','*地址','trim|required');
		//$this->form_validation->set_rules('sort_order','*排序','trim|required');

		if ($this->form_validation->run() == TRUE){
					
			$data = array(
                      //'first_name' => $this->input->post('first_name'),
					   'last_name' => $this->input->post('last_name'),
				           'email' => $this->input->post('email'),
					      'mobile' => $this->input->post('mobile'),
					       'phone' => $this->input->post('phone'),
					       'local' => $this->input->post('local'),
					     'address' => $this->input->post('address'),
					  //'sort_order' => $this->input->post('sort_order'),
					      'status' => $this->input->post('status')
			);
				
			$this->db->where('customer_addr_id' ,$this->input->get('customer_addr_id'));
			$this->db->update('customer_addr',$data);
				
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
				
			redirect('customer_addr/edit?'.$_SERVER["QUERY_STRING"]);
			
		}else{

			$this->db->where('customer_id' ,$_SESSION['customer_info']['customer_id']);
			$this->db->where('customer_addr_id' ,$this->input->get('customer_addr_id'));
			$data['query'] = $this->db->get('customer_addr')->row_array();
			
			$data['func'] = 'customer_addr_edit';
			
			//驗證會員所屬資料
			if(empty($data['query'])) show_404();				
						
			//檢視view
			$this->load->view('frontend/common/header.tpl',$data);
			$this->load->view('frontend/customer/addr/edit.tpl');
		}
	}
		
	// --------------------------------------------------------------------

	/**
	 * 方法 : 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del() {
			
		$this->db->where('customer_id' ,$_SESSION['customer_info']['customer_id']);
		$this->db->delete('customer_addr' ,array('customer_addr_id' => $this->input->get('customer_addr_id')));
		
		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
			
		redirect('customer_addr/lists');
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 刪除商店
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del_store() {
			
		$this->db->where('customer_id' ,$_SESSION['customer_info']['customer_id']);
		$this->db->delete('customer_store' ,array('customer_store_id' => $this->input->get('customer_store_id')));
	
		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
			
		redirect('customer_addr/lists');
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 預設地址
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function defaults($type ,$customer_id) {
		
		switch ($type) {
			
			case 'add':
		      $this->db->where('customer_id' ,$customer_id);
		      $this->db->update('customer_addr',array('status' => 0));
			break;
			
			case 'edit':
			;
			break;			
			
			case 'del':
			;
			break;
		}
		
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得地址列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getAddrList($customer_id) {

		$this->db->where('customer_id' ,$customer_id);
		$this->db->order_by('sort_order' ,'DESC');
		return $this->db->get('customer_addr')->result_array();
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 驗證Email
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function check_email($email){
	
		$email = trim($email);
	
		if( empty($email) ){
			$this->form_validation->set_message('check_email', '*Email不可空白!');
			return FALSE;
		}
	
		if ( !filter_var($email, FILTER_VALIDATE_EMAIL) ) {
			$this->form_validation->set_message('check_email', '*Email格式不符');
			return FALSE;
		}
	
		$this->db->where('email' ,$email);
		$this->db->where('email <> ' ,$_SESSION['customer_info']['email']);
		$query = $this->db->get('customer');
	
		if( $query->num_rows() > 0 ){
			$this->form_validation->set_message('check_email', '*此Email已存在，請重新輸入!');
			return FALSE;
		}
	
		return TRUE;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 驗證手機
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function check_mobile($mobile){
		
		$mobile = trim($mobile);
		
		if( empty($mobile) ){
			$this->form_validation->set_message('check_mobile', '*手機不可空白!');
			return FALSE;
		}
		
		if( strlen($mobile) != 10 ) {
			$this->form_validation->set_message('check_mobile', '*手機必須為10碼!');
			return FALSE;
		}
		
		if( !is_numeric($mobile) ){
			$this->form_validation->set_message('check_mobile', '*手機必須為數字!');
			return FALSE;
		}
		
		if(  substr($mobile ,0,2) != '09' ){
			$this->form_validation->set_message('check_mobile', '*手機前兩碼必須為"09"開頭!');
			return FALSE;
		}
		
		return TRUE;
	}

}


/* End of file customer_addr.tpl */
/* Location: ./application/controllers/customer_addr  */