<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 說明 : 處理[會員登入]的頁面請求 
 * @controllerName sign_in
 * @author Dio
 *
 */
class sign_in extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();
		
		$this->load->library(array('session','form_validation','Dio_gmail','promo/VIP','fb/Dio_fb','google/Dio_google','google_captcha/dio_google_captcha'));
		$this->load->helper(array('url','cookie','ctmall','motion','database','base'));
		$this->load->helper(array('dio_captcha'));
		
		$this->load->model("frontend/sign_in_model" ,"sign_in");
		
		$this->dio_email = new Dio_gmail();
		$this->g_captcha = new dio_google_captcha();
		
		$this->vip = new VIP();
		$this->fb  = new Dio_fb();
		//$this->google = new Dio_google();
		
		$this->err_msg = "";
		
		$this->customer_info = array() ;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 登入
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function login(){
	
		$this->form_validation->set_rules('username','* 帳號', 'callback_check['.$this->input->post('password').']');
		$this->form_validation->set_rules('secure_code','* 驗證碼', 'callback_check_captcha');
	
		if ($this->form_validation->run() == TRUE){
					
			 getLoginRedirectPage();
				
		}else{
				
			if(IsLoginCustomer()){
				redirect('customer_order/lists');
			}
	
			//$data['ad']         = $this->ad();
			//$data['hot']        = $this->hot();
				
			$data['func']             = 'login';
				
			$data['fb_login_url']     = $this->fb->get_login_url();
			//$data['google_login_url'] = $this->google->get_login_url();
				
			$this->load->view('frontend/common/header.tpl',$data);
			//$this->load->view('frontend/common/menu.tpl');
			$this->load->view('frontend/sign_in/login.tpl');
		}
	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 登入 / Facebook
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function login_fb(){
	
	   $data['user_profile'] = $this->fb->login();
				
		 if( empty($data['user_profile']['id']) ) {
             redirect('sign_in/login');
			 return;
		}		 
					
		$query = $this->sign_in->getCustomerInfoBySociety($data['user_profile']['id']);
							
		if( empty($query) ){ //帳號不存在(第一次登入)
	
				$data_ins = array(
						'log_id'    => 1 ,
						'username'  => $data['user_profile']['id'] ,
						'last_name' => $data['user_profile']['name'] ,
						'cdate'     => date('Y-m-d H:i:s')
				);
	
				$this->db->insert('customer' ,$data_ins);
	
				$_SESSION['customer_info']           = $this->sign_in->getCustomerInfoBySociety($data['user_profile']['id']);
				//$_SESSION['customer_info']['coupon'] = $this->sign_in->getCustomerCoupon($_SESSION['customer_info']['customer_id']);
				
				getLoginRedirectPage();
	
		}else{
	
				//寫入session
				$_SESSION['customer_info']           = $this->sign_in->getCustomerInfoBySociety($data['user_profile']['id']);
				//$_SESSION['customer_info']['coupon'] = $this->sign_in->getCustomerCoupon($_SESSION['customer_info']['customer_id']);
	
				getLoginRedirectPage();
		}
	
	}	
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 註冊
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function register(){
			
		$this->form_validation->set_rules('username','*帳號','trim|callback_check_user_duplicate');
		$this->form_validation->set_rules('password','*密碼','trim|required|min_length[6]|max_length[12]|matches[passconf]');
		$this->form_validation->set_rules('passconf','*密碼確認', 'trim|required');
		//$this->form_validation->set_rules('first_name','*姓','trim|required');
		$this->form_validation->set_rules('last_name','*姓名','trim|required|max_length[5]');
		$this->form_validation->set_rules('birthday_year','*生日/年','trim');
		$this->form_validation->set_rules('birthday_month','*生日/月','trim');
		$this->form_validation->set_rules('birthday_day','*生日/日','trim');
		$this->form_validation->set_rules('email','*收件email','trim|valid_email|required');
		$this->form_validation->set_rules('mobile','*手機','callback_check_mobile');
		$this->form_validation->set_rules('phone','*電話','trim');
		//$this->form_validation->set_rules('local','*區號','trim|required');
		//$this->form_validation->set_rules('address','*地址','trim|required');
		$this->form_validation->set_rules('secure_code','* 驗證碼', 'callback_check_captcha');
	
		if ($this->form_validation->run() == TRUE){
	
			//新增主表
			$data_master = array(
					'username'       => $this->input->post('username'),
					'password'       => md5($this->input->post('password')),
					//'first_name'     => $this->input->post('first_name'),
					'last_name'      => $this->input->post('last_name'),
					'birthday_year'  => $this->input->post('birthday_year'),
					'birthday_month' => $this->input->post('birthday_month'),
					'birthday_day'   => $this->input->post('birthday_day'),
					'email'          => $this->input->post('email'),
					'cdate'          => date('Y-m-d H:i:s')
			);
	
			$this->db->insert('customer',$data_master);
	
			//新增副表
			$customer_id = $this->db->insert_id();
				
			$data_detail = array(
					'customer_id' => $customer_id,
					//'first_name'  => $this->input->post('first_name'),
					'last_name'   => $this->input->post('last_name'),
					'mobile'      => $this->input->post('mobile'),
					//'phone'       => $this->input->post('phone'),
					'email'       => $this->input->post('email'),
					//'local'       => $this->input->post('local'),
					//'address'     => $this->input->post('address'),
					'cdate'       => date('Y-m-d H:i:s')
			);
				
			$this->db->insert('customer_addr',$data_detail);
				
			//$this->session->set_flashdata('msg','您已完成註冊!');
	
			//寄發Email
			$this->dio_email->join( array('email' => $data_master['email']) );
	
			//寫入session
			$this->customer_info = $this->sign_in->getCustomerInfo($this->input->post('username') ,$this->input->post('password'));
			
			$_SESSION['customer_info'] = $this->customer_info;
	
            redirect('customer_order/lists');
	
		}else{
				
			if( IsLoginCustomer() ){
				redirect('customer_order/lists');
			}
				
			//$data['ad']           = $this->ad();
			//$data['hot']          = $this->hot();
				
			$data['func'] = 'register';
				
			//錯誤訊息燈箱
			$data['is_ErrMsg']         = (validation_errors() != false) ? 1 : 0 ;
			$data['validation_errors'] = validation_errors();
	
			$this->load->view('frontend/common/header.tpl',$data);
			//$this->load->view('frontend/common/menu.tpl');
			$this->load->view('frontend/sign_in/register.tpl');
		}
	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 忘記密碼
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function forget(){
		
		$this->form_validation->set_rules('username','*帳號','trim|required');
		$this->form_validation->set_rules('email','*電子郵件','trim|required|valid_email');
	
		if ($this->form_validation->run() == TRUE){
				
			//驗證帳號和email
			$this->db->where('username' ,$this->input->post('username'));
			$this->db->where('email' ,$this->input->post('email'));
				
			$query = $this->db->get('customer');

			if( $query->num_rows() > 0 ){
				$this->dio_email->forget( array('username' => $this->input->post('username')
						                       ,'email' => $this->input->post('email') ));
				
				dioRedirect('index/main' ,'信件已送出，請到您指定的email接收信件!');
			}else{
				
				dioRedirect('sign_in/forget' ,'抱歉!您輸入的資料有誤，請重新填寫!');
			}
				
		} else { 
			
			$data['func'] = getUserMenu('forget'); 
	
			$this->load->view('frontend/common/header.tpl' ,$data);
			$this->load->view('frontend/sign_in/forget.tpl');
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢查
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function check($username ,$password)
	{
		//帳號
		if(!$username)
		{
			$this->form_validation->set_message('check', '*帳號 不可空白');
			return false;
		}
	
		//密碼
		if(!$password)
		{
			$this->form_validation->set_message('check', '*密碼 不可空白');
			return false;
		}
	
		if( strlen($password) < 8 || strlen($password) > 16)
		{
			$this->form_validation->set_message('check', '*密碼  必須介於8-16個字');
			return false;
		}

		if( !preg_match("/[a-z]/", $password) )
		{
			$this->form_validation->set_message('check', '*密碼  至少需要一個小寫字母');
			return false;
		}
				
		//身份比對
		if( !$this->sign_in->isCustomerInfo($username ,$password) )
		{
			$this->form_validation->set_message('check', '*登入失敗,帳號或密碼有誤!');
			return false;
		}
	
		$_SESSION['customer_info']           = $this->sign_in->getCustomerInfo($username ,$password);
		//$_SESSION['customer_info']['coupon'] = $this->sign_in->getCustomerCoupon($_SESSION['customer_info']['customer_id']);
		
		return true;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢查密碼
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function check_pass($password ,$password_conf)
	{
		if( empty($password) || empty($password_conf))
		{
			$this->form_validation->set_message('check_pass', '*密碼 不可空白');
			return FALSE;
		}
	
		if( strlen($password) < 8 || strlen($password) > 16)
		{
			$this->form_validation->set_message('check_pass', '*密碼  必須介於8-16個字');
			return FALSE;
		}
	
		//if( !preg_match("/[a-z]/i", $password) )
		if( !preg_match("/[a-z]/", $password) )
		{
			$this->form_validation->set_message('check_pass', '*密碼  至少需要一個小寫字母');
			return FALSE;                                           
		}
	
		return TRUE;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢查 / 驗證碼
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function check_captcha($secure_code)
	{
		
		if(!$secure_code)
		{
			$this->form_validation->set_message('check_captcha', '*驗證碼 不可空白');
			return FALSE;
		}
		
		if( $secure_code != $_SESSION['captcha'] )
		{
			$this->form_validation->set_message('check_captcha', '*驗證碼必須一致');
			return FALSE;
		}
		
		return TRUE;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢查 / google圖形驗證碼
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function check_google_captcha($g_recaptcha_response)
	{
	
		if(!$g_recaptcha_response)
		{
			$this->form_validation->set_message('check_google_captcha', '*請勾選圖形驗證碼');
			return FALSE;
		}
	
		$is_g_captcha = $this->g_captcha->valid( $g_recaptcha_response  );
	
		if( !empty($is_g_captcha) )
		{
			$this->form_validation->set_message('check_google_captcha', '*圖形驗證碼必須一致');
			return FALSE;
		}
	
		return TRUE;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 :  檢查 / 帳號重複註冊
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	function check_user_duplicate($username){
		
		if( empty($username) )
		{
			$this->form_validation->set_message('check_user_duplicate', '*帳號 不可空白');
			return FALSE;
		}
		
		if( !preg_match("/^[a-z\d_]{2,20}$/i", $username) )
		{
			$this->form_validation->set_message('check_user_duplicate', '*帳號請以英文或數字符號輸入');
			return FALSE;
		}
		
		$this->db->where('username' ,$username);
		$query = $this->db->get('customer');
	
		if( $query->num_rows() > 0 ){
			$this->form_validation->set_message('check_user_duplicate', '*此帳號已存在，請重新輸入!');
			return FALSE;
		}
	
		return TRUE;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 :  檢查 / Email重複註冊
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	function check_email_duplicate($email){
	
		if( empty($email) )
		{
			$this->form_validation->set_message('check_email_duplicate', '*Email不可空白');
			return FALSE;
		}
	
		if ( !filter_var($email, FILTER_VALIDATE_EMAIL) ) {
			$this->form_validation->set_message('check_email_duplicate', '*Email格式不符');
			return FALSE;
		}
		
		$this->db->where('email' ,$email);
		$query = $this->db->get('customer');
	
		if( $query->num_rows() > 0 ){
			$this->form_validation->set_message('check_email_duplicate', '*此Email已存在，請重新輸入');
			return FALSE;
		}
	
		return TRUE;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 驗證手機
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function check_mobile($mobile){
	
		$mobile = trim($mobile);
	
		if( empty($mobile) ){
			$this->form_validation->set_message('check_mobile', '*手機不可空白!');
			return FALSE;
		}
	
		if( strlen($mobile) != 10 ) {
			$this->form_validation->set_message('check_mobile', '*手機必須為10碼!');
			return FALSE;
		}
	
		if( !is_numeric($mobile) ){
			$this->form_validation->set_message('check_mobile', '*手機必須為數字!');
			return FALSE;
		}
	
		if(  substr($mobile ,0,2) != '09' ){
			$this->form_validation->set_message('check_mobile', '*手機前兩碼必須為"09"開頭!');
			return FALSE;
		}
	
		return TRUE;
	}
	
   // --------------------------------------------------------------------
    
   /**
    * 方法 : 圖形驗證碼
    *
    * @access	public
    * @param
    * @return
    */
   public function captcha(){
   	 
   	  echo captcha();
   }
   
   // --------------------------------------------------------------------
   
   /**
    * 方法 : 登出
    *
    * @access	public
    * @param
    * @return
    */
   public function logout(){
   
   	 unset($_SESSION['customer_info']);
   	 unset($_SESSION['my_order']['step']);
   
   	 $this->session->set_flashdata('msg',DIO_MSG_SUCCESS_LOGOUT);
   
   	 redirect('sign_in/login');
   }
	
	
}


/* End of file login.tpl */
/* Location: ./application/controllers/sign_in  */