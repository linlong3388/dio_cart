<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 前台管理首頁(Ajax)
 *
 * 說明 : 提供Ajax專用的請求控制器
 * @controllerName Center
 * @author Dio
 *
 */
class Ajax extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();
		
		$this->load->helper(array('base','motion','url'));
		$this->load->library(array('form_validation','Cart_chenyunpaochuan'));
		
		$this->cart_chenyunpaochuan  = new Cart_chenyunpaochuan();
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 改變語系
	 *
	 * @access	public
	 * @param
	 * @return json
	 */
	public function setLanguage(){
	
       $language = $this->input->post('language');
       
       if (array_key_exists($language ,getLanguage())){
       	   changeLanguage($language);
       }
       
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 取得會員常用發票資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function get_member_unifi(){
			
		$this->db->where('customer_unifi_id' ,$this->input->get('customer_unifi_id'));
		$query = $this->db->get('customer_unifi')->row_array();

		//隱碼資料
		$mk_data = array(
		           'customer_unifi_id' => $query['customer_unifi_id'],	
                         'customer_id' => $query['customer_id'],	
                     'ticket3_unified' => substr_replace($query['ticket3_unified'],"****", 4 ),	
                       'ticket3_title' => substr_replace($query['ticket3_title'], '******', 6),	
                              'status' => $query['status'],
                               'cdate' => $query['cdate']
		);
		 
		echo json_encode($mk_data);
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 是否訂閱電子報
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	function is_edm(){

		$data = array(
	                'is_edm' => $this->input->get('is_edm') 
		);

		$this->db->where('customer_id' , $this->input->get('customer_id'));
		$this->db->update('customer' ,$data);
	}
	

	/*
	 * ----------------------------------------------------------------------------------
	 *
	 *  商品的方法
	 *
	 * ----------------------------------------------------------------------------------
	 *
	 */
	 
	// --------------------------------------------------------------------

	/**
	 * 方法 : 商品主表 / 列表
	 *
	 * @access	public
	 * @param
	 * @return json
	 */
	public function get_product(){

		//依搜尋條件取得資料
		$category_id = explode('_',$this->input->post('category_id',true));
		$action_id = $this->input->post('action_id',true);
		
		$srh_data['srh_parent_id'] = array_values(array_unique($srh_data['srh_parent_id']));
		$srh_data['srh_category_id_1'] = $category_id[0];
		$srh_data['srh_category_id_2'] = $category_id[1];
		$srh_data['srh_action_id'] = isset($action_id) ? $action_id : 0;

		$sql_where = $this->shr_product_condition($srh_data);

		$sql = "SELECT DISTINCT(catp.product_id), p.*
	               FROM product p
                   LEFT JOIN ct_action_to_product catp ON p.product_id = catp.product_id
                   {$sql_where}";

                   $query = $this->db->query($sql)->result_array();


                   //-------------------------------------------------------------
                   //
                   // 幣別/資料格式整理
                   //
                   //-------------------------------------------------------------
                   for($i=0 ; $i< COUNT($query) ; $i++){
                   	$query[$i]['price'] =  get_currency_format($query[$i]['price_'.$_SESSION['motion_currency']]);
                   }


                   //-------------------------------------------------------------
                   //
                   // 輸出
                   //
                   //-------------------------------------------------------------

                   echo json_encode($query);
	}

	// --------------------------------------------------------------------

	/**
	 * 方法: 商品主表 / 搜尋條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function shr_product_condition($srh_data){

		$sql_where = " WHERE p.status = 1 ";

		//商品庫存量大於0
		$sql_where .= " AND (SELECT SUM(stock) FROM `product_speci` WHERE product_id = p.product_id) > 0" ;

		//依[類別] / 商品分類搜尋(大分類)
		if( !empty($srh_data['srh_category_id_1']) ){
			$sql_where .= " AND ( p.category_id IN (SELECT category_id FROM `category` WHERE path LIKE ('%,{$srh_data['srh_category_id_1']},%')) )" ;
		}
	  
		//依[類別] / 商品分類搜尋(次分類)
		if( !empty($srh_data['srh_category_id_2']) ){
			$sql_where .= " AND  p.category_id = {$srh_data['srh_category_id_2']}" ;
		}
	  
		//依[活動] / 商品活動搜尋
		if( !empty($srh_data['srh_action_id']) ){
			$sql_where .= " AND catp.action_id = {$srh_data['srh_action_id']}" ;
		}

		//依[屬性分類]
		if( !empty($srh_data['srh_parent_id']) ){

			for($i=0 ; $i<COUNT($srh_data['srh_parent_id']) ; $i++){

				switch ($srh_data['srh_parent_id'][$i]) {
					case 1: //顏色
						$sql_where .= " AND ( p.product_id IN (SELECT DISTINCT(product_id) FROM `product_speci` WHERE property_id_color IN ({$srh_data['srh_property_id_color']})  ) )" ;
						break;
						 
					case 2: //材質
						$sql_where .= " AND ( p.product_id IN (SELECT DISTINCT(product_id) FROM `product_speci` WHERE property_id_material IN ({$srh_data['srh_property_id_material']}) ) )" ;
						break;
						 
					case 3: //尺寸
						$sql_where .= " AND ( p.product_id IN (SELECT DISTINCT(product_id) FROM `product_speci` WHERE property_id_size IN ({$srh_data['srh_property_id_size']}) ) )" ;
						break;
				}
			}
		}
	  
		return $sql_where;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 商品副表 / 列表
	 *
	 * @access	public
	 * @param
	 * @return json
	 */
	public function get_product_speci(){

		$this->db->where('product_id' ,$this->input->post('product_id'));
		$this->db->where('status' ,1);
		$query = $this->db->get('product_speci')->result_array();

		echo json_encode($query);
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 依[顏色]取得商品[尺寸]資料 / 多筆
	 *
	 * @access	public
	 * @param	[product_id]、[property_id]
	 * @return json
	 */
	public function get_product_speciSize(){

		//取得參數
		$product_id = $this->input->get('product_id');
		$property_id = $this->input->get('property_id');

		$sql = "SELECT ps.*
		          FROM product_speci ps
		        WHERE ps.product_id = {$product_id}";

		$query = $this->db->query($sql)->result_array();

		echo json_encode($query);
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 依[顏色]/[尺寸]取得商品[sku]和[數量]資料 / 單筆
	 *
	 * @access	public
	 * @param	[product_id]、[property_id]
	 * @return json
	 */
	public function get_product_skuStock(){

		//取得參數
		$product_id = $this->input->get('product_id');
		$property_id_color = $this->input->get('property_id_color');
		$property_id_size = $this->input->get('property_id_size');

		$sql = "SELECT ps.* ,prc.name as prc_name ,prs.name as prs_name ,prm.name as prm_name
		          FROM product_speci ps
		          LEFT JOIN ct_property prc ON ps.property_id_color = prc.property_id  
		          LEFT JOIN ct_property prs ON ps.property_id_size = prs.property_id  
		          LEFT JOIN ct_property prm ON ps.property_id_material = prm.property_id  
		        WHERE ps.product_id = {$product_id}
		        AND ps.property_id_color ={$property_id_color}
		        AND ps.property_id_size ={$property_id_size}";

		$query = $this->db->query($sql)->row_array();

		echo json_encode($query);
	}

	/*
	 * ----------------------------------------------------------------------------------
	 *
	 *  商品評價的方法
	 *
	 * ----------------------------------------------------------------------------------
	 *
	 */

	// --------------------------------------------------------------------

	/**
	 * 方法 : 設置商品評價 /是否有幫助
	 *
	 * @access	public
	 * @param
	 * @return json
	 */
	public function product_review_is_help(){

		$product_reviews_id  = $this->input->post('product_reviews_id');
		$assign  = $this->input->post('assign');

		$sql = "";
		$data['err_msg'] = "";

		//判斷會員是否登入
		if(!IsLoginCustomer()){
			$data['err_msg'] = "請先登入會員才可進行投票，謝謝!";
			echo json_encode($data);
			return 0;
		}

		$this->db->where('product_reviews_id' ,$product_reviews_id);
		$this->db->where('customer_id' ,$_SESSION['customer_info']['customer_id']);
		$query_customer = $this->db->get('product_reviews_customer_help')->row_array();

		//判斷會員是否已投過票
		if(!empty($query_customer)){
			$data['err_msg'] = "抱歉!不可重複投票!";
			echo json_encode($data);
			return 0 ;
		}

		if($assign == 'yes'){
			$sql = "UPDATE product_reviews SET is_help_yes = is_help_yes + 1
	       	         WHERE product_reviews_id = ? LIMIT 1";
		}elseif($assign == 'no'){
			$sql = "UPDATE product_reviews SET is_help_no = is_help_no + 1
	       	         WHERE product_reviews_id = ? LIMIT 1";		
		}

		$this->db->query($sql ,array($product_reviews_id));

		if($this->db->affected_rows() > 0){
			$this->db->insert('product_reviews_customer_help' ,array('product_reviews_id' => $product_reviews_id ,
			                                                            'customer_id' => $_SESSION['customer_info']['customer_id'] ,
			                                                            'is_help' => 1 ,
                                                                        'cdate' => date('Y-m-d H:i:s')
			));
		}


		$this->db->where('product_reviews_id' ,$product_reviews_id);
		$query = $this->db->get('product_reviews')->row_array();

		echo json_encode($query);
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得最新消息資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function get_news(){

		$page  = $this->input->get('page');  //目前頁碼
		$count = $this->input->get('count'); //顯示筆數

		$data = array();
		
		if( !empty($page) ){
			$this->db->limit($count ,(($page-1)*$count));
		}
		
		$this->db->where('status' ,1);
		$query = $this->db->get('news')->result_array();
		
		$i = 0;
		foreach ($query as $row){
		   $data[$i]['url']              = getUserURL('news/view?news_id='.$row['news_id']);
		   $data[$i]['news_id']          = $row['news_id'];
		   $data[$i]['news_category_id'] = $row['news_category_id'];
		   $data[$i]['title']            = $row['title'];
		   $data[$i]['content']          = mb_substr(strip_tags($row['content']) , 0 ,150 ,"UTF-8")."...";
		   $data[$i]['image']            = base_url( DIO_PATH_PIMG .'/'. $row['image']);
		   $data[$i]['status']           = $row['status'];
		   $data[$i]['cdate']            = substr($row['cdate'], 0,10);
		   	
		   $i++;	
		}
		
		echo json_encode($data);
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取消訂單
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function orderStatusCancel(){
		
		$order_show_id   = $this->input->post('order_show_id');
		
		$this->db->where('order_show_id' ,$order_show_id);
		$this->db->update('order' ,array('status' => 3));
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 新增地址
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function addAddr_unLogin(){
		
		$err_msg = "";
	
		//$this->form_validation->set_rules('first_name','*收件人/姓','trim|required');
		$this->form_validation->set_rules('last_name','*收件人','trim|required');
		$this->form_validation->set_rules('phone','*電話','trim|required');
		$this->form_validation->set_rules('email','*Email','trim|valid_email');
		//$this->form_validation->set_rules('mobile','*手機','trim|required|min_length[10]||max_length[10]');
		$this->form_validation->set_rules('local','*區號','trim|required');
		$this->form_validation->set_rules('address','*地址','trim|required');
		//$this->form_validation->set_rules('secure_code','* 驗證碼', 'callback_check_captcha');
			
		if ($this->form_validation->run() == TRUE){
	
			$data = array(
					'customer_id' => $_SESSION['customer_info']['customer_id'],
					//'first_name' => $this->input->post('first_name'),
					'last_name' => $this->input->post('last_name'),
					//'mobile' => $this->input->post('mobile'),
					'phone' => $this->input->post('phone'),
					'email' => $this->input->post('email'),
					'local' => $this->input->post('local'),
					'address' => $this->input->post('address'),
					'cdate' => date('Y-m-d H:i:s')
			);
	
	
			$this->db->insert('customer_addr',$data);
	
			echo json_encode($err_msg);
	
		}else{
	
			$err_msg = validation_errors();
	
			echo json_encode($err_msg);
		}
	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 新增會員超商資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function addCustomerStore(){
	
		$err_msg = "";

		$this->form_validation->set_rules('sno','*店號','trim|required');
		$this->form_validation->set_rules('name','*門市名稱','trim|required');
		$this->form_validation->set_rules('local','*區號','trim|required');
		$this->form_validation->set_rules('address','*地址','trim|required');
		//$this->form_validation->set_rules('sort_order','*排序','trim|required');
		
		
		if ($this->form_validation->run() == TRUE){
			
			$data = array(
					'customer_id' => $_SESSION['customer_info']['customer_id'],
					'sno'     => $this->input->post('sno'),
					'name'    => $this->input->post('name'),
					'local'   => $this->input->post('local'),
					'address' => $this->input->post('address'),
					//'sort_order' => $this->input->post('sort_order')
			);
	
			$this->db->insert('customer_store',$data);
	
			echo json_encode($err_msg);
	
		}else{
	
			$err_msg = validation_errors();
	
			echo json_encode($err_msg);
		}
	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得會員常用地址資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getCustomerAddr(){
			
		$this->db->where('customer_addr_id' ,$this->input->post('customer_addr_id'));
		$this->db->where('status' ,1);
		$query = $this->db->get('customer_addr')->row_array();
			
		echo json_encode($query);
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得會員常用超商資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getCustomerStore(){
			
		$this->db->where('customer_store_id' ,$this->input->post('customer_store_id'));
		$this->db->where('status' ,1);
		$query = $this->db->get('customer_store')->row_array();
		
		echo json_encode($query);
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 重設購物車product_id
	 *         商品組合
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function reSetCartProductId(){

		$product_data = $this->input->post('product_data');
		
		$this->cart_chenyunpaochuan->reSetCartCombProductInfo( $product_data );
		
		echo json_encode(1);
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 依`product_id`取得購物車資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getCartInfoByProductId(){
	
		$product_id = $this->input->post('product_id');
	
		//取得購物車資料
		$result = $this->cls_cart->getCartInfoByProductID( $product_id );
		
		//售價格式化
		$result['price'] = DIO_CURRENCY.addCommas($result['price']); 
		
		echo json_encode($result);
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 依`product_id`取得購物車組圖資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getCartProductTypeId1Img_ajax(){
		
		$product_id = $this->input->post('product_id');

		$result = $this->cart_chenyunpaochuan->view_getCartProductTypeId1Img( $product_id );
		
		echo json_encode($result);
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 結帳步驟2 / 依付款方式決定配送方式
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getTransMethodByPayMethod(){
	
		$payMethodId = $this->input->post('payMethodId');

		$trans_method = (!empty($_SESSION['my_order']['info']) && !empty($_SESSION['my_order']['info']['trans_method'])) ? $_SESSION['my_order']['info']['trans_method'] : 0;
		
		$result = array();
		$data   = array();
		
		$result = reSortArrayMutil(3 ,$trans_method , getTransMethod());
		
		switch ($payMethodId) {
        	
			//線上刷卡
        	case 0:
        		$data['keys']   = array_keys($result);
        		$data['values'] = array_values($result);
        	break;
        	
        	//貨到付款
        	case 1:
        		unset($result[1]);

        		$data['keys']   = array_keys($result);
        		$data['values'] = array_values($result);
        	break;

        	//ATM轉帳
        	case 2:
        		$data['keys']   = array_keys($result);
        		$data['values'] = array_values($result);
            break;
       }
	   
       echo json_encode($data);
		
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 驗證組合數量是否ok
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function isCombEntityOk_ajax(){
	
		$combUnit = $this->input->post('combUnit');
		$result   = '0';
		
		if( !empty($combUnit) ){
		   //驗證組合	
           $isCombOk = $this->cart_chenyunpaochuan->isProductTypeId1BoxCombOk( $combUnit );

           if($isCombOk){
               $result = '1';
		   }
		
		}	
	
		echo json_encode($result);
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 清空組合資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function clearCombData_ajax(){
		
		$product_id = $_SESSION['my_order']['productTypeId_1'];
		
		$this->cart_chenyunpaochuan->clearCombData($product_id);
	
		echo json_encode(1);
	}
	
}


/* End of file ajax.php */
/* Location: ./application/controllers/ajax.php */