<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 說明 : 處理[商品專區]的頁面請求
 * @controllerName product_property
 * @author Dio
 *
 */
class product_property extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){

		parent::__construct();

		//載入模組
		$this->load->helper(array('form','url','cookie','database','ctmall','base','motion'));
		$this->load->library(array('form_validation','session'));

		$this->load->model("frontend/product_model","product");

		//定義常數
		define('DIO_FILE_PATH' ,base_url('application/views/frontend/'));
		define('DIO_BASE_URL' ,base_url('index.tpl/frontend/'));
		define('DIO_URL_LAST' ,getUrlLastSection());
		define('DIO_URL_2LAST' ,getUrlLast2Section());
		
		//建立購物車記錄
		$this->cart_log = new Cart_log();

		/*********************************
		 /* 設置幣別
		 *********************************/
		if( empty($_SESSION['motion_currency']) ){
			$_SESSION['motion_currency'] = 'twd';
		}
		
		if($this->input->get('page')){ //頁碼
		   $this->page = $this->input->get('page'); 	
		}else{
		   $this->page = 1;	
		}
		
	    $this->srh_page_per = 20; //每頁筆數	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 列表 
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists(){
		
		$property_id = $this->input->get('property_id');
		
		$pg = new Dio_pagination();
		$pg_ary = $pg->get_limit($this->input->get('page'),10);
		
		//取得列表資料
		$sql_and = "";
		if( isset($property_id) && is_numeric($property_id) ){
			$sql_and = " AND property_id = '".$property_id."'";
		}
		
		$sql = "SELECT * FROM `product` WHERE  status='1' 
                 AND `product_id` IN ( SELECT `product_id` FROM `product_attr` WHERE status='1' {$sql_and} )
                 ORDER BY cdate DESC
		        LIMIT {$pg_ary['limit1']} ,{$pg_ary['limit2']}";
		
		$data['query'] = $this->db->query($sql)->result_array();
		
		//取得總數
		$sql2 = "SELECT COUNT(product_id) as count FROM `product` WHERE status='1' 
                 AND `product_id` IN ( SELECT `product_id` FROM `product_attr` WHERE status='1' {$sql_and} )";
		
		$query_total = $this->db->query($sql2 ,array($property_id))->row_array();
		$data['query_total'] = $query_total['count']; 
		
		//設定分頁
		$data['pagination']  = $pg->pagination($data['query_total'],5);
		
		$data['property'] = $this->get_property();
		
		//$data['ad']           = $this->ad($srh_data['srh_category_id_1']);
		
		$data['func'] = 'category_lists';
		
		$this->load->view('frontend/common/header.tpl' ,$data);
		$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/product/product_property/lists.tpl');
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 廣告
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function ad($category_id){
	
		$this->db->where('status' ,'1');
		$this->db->limit(2);
		$this->db->where('category_id' ,$category_id);
		$this->db->order_by('category_image_id' ,'DESC');
		$ad = $this->db->get('category_image')->result_array();
		
		$data['a'] = array();
		
		foreach ($ad as $row) {
			switch ($row['type']) {
				case 'a':
					array_push($data['a'], $row);
					break;
			}
		}
	
		return $data;
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 搜尋
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function search ($data ,$pg_ary) {
		
		if(isset($data) && !empty($data)){
		   foreach ($data as $key=>$val) {
             if($val == ''){
		      	unset($data[$key]);
             }
           }
		}

		//兩層分類(格式如1_5)
		$data['srh_category_id_1'] = '';
		$data['srh_category_id_2'] = '';
		
		if( isset($data['srh_category']) && $data['srh_category'] != '*' ){
		   $category_id = explode('_',$data['srh_category']);
		   $data['srh_category_id_1'] = $category_id[0];
		   $data['srh_category_id_2'] = $category_id[1];
		}
		
		$data['srh_limit1']   = $pg_ary['limit1'];
		$data['srh_limit2']   = $pg_ary['limit2'];
		
		return $data;
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 下拉式 / 分類
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function slt_category () {
		
		$data = array();
		
		foreach ($this->cls_category->get_last_parent() as $row) {
			$data[$row['category_id']] = $this->cls_category->get_name($row['path']);
		}
		
		return $data;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 下拉式 / 分類2
	 *       key 值格式為"父_子" 分類 如"2_6723"
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function slt_category2 () {
	
		$data = array();
	
		foreach ($this->cls_category->get_last_parent() as $row) {
			$data[$row['parent_id'].'_'.$row['category_id']] = $this->cls_category->get_name($row['path']);
		}
	
		return $data;
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 檢視
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function view(){
		
		//採用燈箱，所以require無法設定 
		$this->form_validation->set_rules('name','*姓名','trim');
		$this->form_validation->set_rules('phone','*電話','trim');
		$this->form_validation->set_rules('email','*Email','trim');
		$this->form_validation->set_rules('question','*留言','trim');
		$this->form_validation->set_rules('secure_code','* 驗證碼', 'callback_check_captcha');
		
		if ($this->form_validation->run() == TRUE){
			
			$data = array(
					//'customer_id' => $_SESSION['customer_info']['customer_id'],
					'product_id'  => $this->input->get('product_id'),
					'name'        => $this->input->post('name'),
					'phone'       => $this->input->post('phone'),
					'email'       => $this->input->post('email'),
					'question'    => $this->input->post('question'),
					'cdate'       => date('Y-m-d H:i:s')
			);
			
			$this->db->insert('product_question',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
				
			redirect('product/view?product_id='.$data['product_id']);
			
		}else{
			
		    $product_id = $this->input->get('product_id');
		
		    //取(主)表資料
		    $query = $this->product->get_view($product_id);
		    
		    if( empty($query) ){
		    	show_404();
		    	exit;
		    } 
		    
		    //取(副)表資料
		    $size  = array();
		    $color = array();
		    
		    $query_detail = $this->product->get_view_detail( array('product_id' => $product_id));
		    
		    foreach ($query_detail as $row){
		   	  $size[$row['property_id_size']]   = $row['prs_name'] ;
		   	  $color[$row['product_speci_id'].','.$row['property_id_color']] = $row['prc_name'] ;
		   	  $query['product_speci_id'] = $row['product_speci_id'];
		    }
		
		    $query['size']  = $size; 
		    $query['color'] = $color;		    

		    $data['query'] = $query;
		    
		    $data['func']  = 'product_view';
			  
			//檢視view
			$this->load->view('frontend/common/header.tpl' ,$data);
			$this->load->view('frontend/common/menu.tpl');
			$this->load->view('frontend/product/view.tpl');
			
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢查 / 驗證碼
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function check_captcha($secure_code)
	{
	
		if(!$secure_code)
		{
			$this->form_validation->set_message('check_captcha', '*驗證碼 不可空白');
			return FALSE;
		}
	
		if( $secure_code != $_SESSION['captcha'] )
		{
			$this->form_validation->set_message('check_captcha', '*驗證碼必須一致');
			return FALSE;
		}
	
		return TRUE;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 取得[檢視]的URL參數
	 *       
	 *       為簡化各別不同請求頁面，統一自帶 'product_id'參數即可
	 *       ，不需要額外帶'category_id'等參數
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function get_view_url_param( $product_id ){
       
		if( is_numeric($product_id)){
			
			$sql = "SELECT * FROM `category` WHERE category_id = (SELECT category_id FROM `product` WHERE product_id= {$product_id}) ";
			 
			$query = $this->db->query($sql)->row_array();
			
			return array(
					       'srh_category' => $query['parent_id'].'_'.$query['category_id'] ,
					       'product_id'   => $product_id
			        );
		}else{
			
			show_404();
		}
	
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得屬性群組
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function get_property(){
		
		$query = $this->db->get('property')->result_array();
		return $query;
	}
	

}


/* End of file product_property.tpl */
/* Location: ./application/controllers/product_property.tpl */