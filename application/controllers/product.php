<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 說明 : 處理[商品]的頁面請求
 * @controllerName product
 * @author Dio
 *
 */
class product extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){

		parent::__construct();

		//載入模組
		$this->load->helper(array('form','url','cookie','database','ctmall','base','motion'));
		$this->load->library(array('form_validation','session','product/product_recommend'));

		$this->load->model("frontend/product_model","product");	
		
		//$this->recommend = new product_recommend();
		
		//建立購物車記錄
		$this->cart_log = new Cart_log();

		/*********************************
		 /* 設置幣別
		 *********************************/
		if( empty($_SESSION['motion_currency']) ){
			$_SESSION['motion_currency'] = 'twd';
		}
		
		if($this->input->get('page')){ //頁碼
		   $this->page = $this->input->get('page'); 	
		}else{
		   $this->page = 1;	
		}
		
	    $this->srh_page_per = 20; //每頁筆數	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 列表 
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists(){
		
		$pg = new dio_pagination();
		
		$pg_ary   = $pg->get_limit($this->input->get('page'),12);
		$srh_data = $this->search($this->input->get() ,$pg_ary);
		
		$data['query']       = $this->product->lists($srh_data);
		
		$data['query_total'] = $this->product->lists_group_by($srh_data);
        		
		$data['query_category'] = $this->getCategory($srh_data);
		
		//META 設定
		$data['query_meta'] = seoMeta('category' ,$data['query_category']);
		$data['query_fb']   = fb_like($data['query_category']);
    	
		//設定分頁
		$data['pagination']  = $pg->pagination($data['query_total'],5);
		
		$data['srh_data']     = $srh_data;	
		
	    $data['func']  = 'product_lists';
		
		$this->load->view('frontend/common/header.tpl' ,$data);
		$this->load->view('frontend/product/lists.tpl');
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 搜尋
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function search ($data ,$pg_ary) {
		
		if(isset($data) && !empty($data)){
		   foreach ($data as $key=>$val) {
             if($val == ''){
		      	unset($data[$key]);
             }
           }
		}

		//兩層分類(格式如1_5)
		$data['srh_category_id_1'] = '';
		$data['srh_category_id_2'] = '';
		
		if( isset($data['srh_category']) && $data['srh_category'] != '*' ){
		   $category_id = explode('_',$data['srh_category']);
		   $data['srh_category_id_1'] = $category_id[0];
		   $data['srh_category_id_2'] = $category_id[1];
		}
		
		$data['srh_limit1']   = $pg_ary['limit1'];
		$data['srh_limit2']   = $pg_ary['limit2'];
		
		return $data;
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 下拉式 / 分類
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function slt_category () {
		
		$data = array();
		
		foreach ($this->cls_category->get_last_parent() as $row) {
			$data[$row['category_id']] = $this->cls_category->get_name($row['path']);
		}
		
		return $data;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 下拉式 / 分類2
	 *       key 值格式為"父_子" 分類 如"2_6723"
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function slt_category2 () {
	
		$data = array();
	
		foreach ($this->cls_category->get_last_parent() as $row) {
			$data[$row['parent_id'].'_'.$row['category_id']] = $this->cls_category->get_name($row['path']);
		}
	
		return $data;
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 檢視
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function view(){
		
		//採用燈箱，所以require無法設定 
		$this->form_validation->set_rules('name','*姓名','trim');
		$this->form_validation->set_rules('phone','*電話','trim');
		$this->form_validation->set_rules('email','*Email','trim');
		$this->form_validation->set_rules('question','*留言','trim');
		$this->form_validation->set_rules('secure_code','* 驗證碼', 'callback_check_captcha');
		
		if ($this->form_validation->run() == TRUE){
			
			$data = array(
					//'customer_id' => $_SESSION['customer_info']['customer_id'],
					'product_id'  => $this->input->get('product_id'),
					'name'        => $this->input->post('name'),
					'phone'       => $this->input->post('phone'),
					'email'       => $this->input->post('email'),
					'question'    => $this->input->post('question'),
					'cdate'       => date('Y-m-d H:i:s')
			);
			
			$this->db->insert('product_question',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
				
			redirect('product/view?product_id='.$data['product_id']);
			
		}else{
			
			$srh_category      = $this->input->get('srh_category');
			$srh_category_ary  = explode('_', $srh_category);
			
			$product_id    = $this->input->get('product_id');
			
		    //取(主)表資料
		    $data['query'] = $this->product->view($srh_category_ary[0],$product_id);
		    
		    if( empty($data['query']) ){
		    	show_404();
		    	exit;
		    } 
		    
		    //商品屬性資料
		    $data['query_product_attr']    = $this->getProductAttr( $data['query']['product_id'] );
		    
		    //促銷活動資料
		    //$data['query_category_promo']  = getProductPromoPrice($data['query']['product_id'] ,$data['query']['price']);
		    
		    //META 設定
		    $data['query_meta'] = seoMeta('content' ,$data['query']);
		    $data['query_fb']   = fb_like($data['query']);
		    
		    //$data['stock'] = getProductEntity($product_id);
		    
		    $data['func'] = 'product_view'; 
		    
			//檢視view
			$this->load->view('frontend/common/header.tpl' ,$data);
			//$this->load->view('frontend/common/menu.tpl');
			$this->load->view('frontend/product/view.tpl');
			
		}
	}
	
	// --------------------------------------------------------------------
	 
	/**
	 * 方法 : 點擊率累加
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function ctr($product_id){
		 
		$sql = "UPDATE `product` SET `ctr_unlimited` = ctr_unlimited + 1 ,
   			                   `ctr_month` = ctr_month + 1
	 			  WHERE `product`.`product_id` = ".$product_id;
		 
		$this->db->query($sql);
	}
	
	// --------------------------------------------------------------------
	 
	/**
	 * 方法 : 也許你還喜歡(依點擊率排序,無期限)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function hobby_unlimited(){
	
		$srh_data = array(
				         'srh_sort' => 'ctr_unlimited' ,
			       	   'srh_limit1' => 0 ,
				       'srh_limit2' => 4 ,
		            );
		
		return $this->product->lists($srh_data);
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 也許你還喜歡(隨機)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function rand_unlimited(){
	
		$srh_data = array(
				'srh_sort' => 'rand' ,
				'srh_limit1' => 0 ,
				'srh_limit2' => 4 ,
		);
	
		return $this->product->lists($srh_data);
	}
	
	// ------------------------------------------------------------------------
	 
	/**
	 * hobby_month
	 *
	 * 方法 : 也許你還喜歡(依點擊率排序(毎月))
	 *
	 * @access	public
	 * @return	array
	 */
	public function hobby_month(){
		 
		$this->db->limit(5);
		$this->db->where('status' ,1);
		//$this->db->where('SUBSTRING(cdate ,1,7) =', date('Y-m'));
		$this->db->order_by('ctr_month' ,'DESC');
		 
		return $this->db->get('product')->result_array();
	}

	// --------------------------------------------------------------------
	 
	/**
	 * 方法 : 依月份，重設毎月點擊率
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function reset_ctr(){
	
		$data = array( 'ctr_month' => 0 );
	
		$this->db->where('SUBSTRING(cdate ,1,7) <', date('Y-m'));
		$this->db->update('product' ,$data);
	}	
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢查 / 驗證碼
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function check_captcha($secure_code)
	{
	
		if(!$secure_code)
		{
			$this->form_validation->set_message('check_captcha', '*驗證碼 不可空白');
			return FALSE;
		}
	
		if( $secure_code != $_SESSION['captcha'] )
		{
			$this->form_validation->set_message('check_captcha', '*驗證碼必須一致');
			return FALSE;
		}
	
		return TRUE;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 取得[檢視]的URL參數
	 *       
	 *       為簡化各別不同請求頁面，統一自帶 'product_id'參數即可
	 *       ，不需要額外帶'category_id'等參數
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function get_view_url_param( $product_id ){
       
		if( is_numeric($product_id)){
			
			$sql = "SELECT * FROM `category` WHERE category_id = (SELECT category_id FROM `product` WHERE product_id= {$product_id}) ";
			 
			$query = $this->db->query($sql)->row_array();
			
			return array(
					       'srh_category' => $query['parent_id'].'_'.$query['category_id'] ,
					       'product_id'   => $product_id
			        );
		}else{
			
			show_404();
		}
	
	}
	
	// --------------------------------------------------------------------
	 
	/**
	 * 方法 : 定義臉書分享
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function set_fbShare($query){
		 
		return array('is_fbShare' => true ,
				'og_title'        => $query['name'] ,
				'og_description'  => $query['description'] ,
				'og_image'        => $query['image']
		);
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得頂級分類資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getCategory($param){
			
		$this->db->where('category_id' ,$param['srh_category_id_1']);
		$this->db->order_by('sort_order' ,'DESC');
		$this->db->order_by('created_at' ,'DESC');
		
		$query = $this->db->get('category')->row_array();
		
		return $query;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 過濾重複的 URL參數
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function removeUrlDuplicParams(){
	
		$url  = $this->input->get('url');
	
		echo json_encode(getQueryStringParamURL($url));
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得副表資料(屬性)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getProductAttr( $product_id ){
	
		$this->db->where('product_id', $product_id );
		$this->db->where('status', 1 );
		$this->db->order_by('sort_order','DESC');
		
		$query = $this->db->get('product_attr')->result_array();
		
		return $query;
	}

}


/* End of file Product.tpl */
/* Location: ./application/controllers/Product.tpl */