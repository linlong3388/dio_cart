<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 說明 : 處理[首頁]相關請求
 * @controllerName index
 * @author Dio
 *
 */
class index extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();	

		$this->load->library(array('session'));
		$this->load->helper(array('url','cookie','ctmall','motion','database','base'));
				
		//定義類別變數
		$this->err_msg = "";
		
		$this->xml = simplexml_load_file('application/models/xml/index_layout.xml');
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 首頁
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function main(){
		
		$data['ad'] = $this->ad();
		
		$data['xml']            = $this->xml;
		$data['news']           = $this->news();
		$data['news_is_show_home']  = $this->news_is_show_home();
		
		$data['product']          = $this->product();
		$data['product_grain']    = $this->product_grain();
		
		$data['product_category'] = $this->getCategory($srh_data = array('srh_is_show_home' => 1
	  		                                        ,'srh_category_id_1' => 2
	  		                                        ,'srh_category_id_2' => '*' ));
		
		$data['category_promo'] = $this->category_promo();
		
		//META 設定
		$meta_title       =  (array) $data['xml']->meta->title;
		$meta_description =  (array) $data['xml']->meta->description;
		$meta_keyword     =  (array) $data['xml']->meta->keyword;
		
		$meta['meta_title']       =  !empty($meta_title[0]) ? $meta_title[0] : '';
		$meta['meta_description'] =  !empty($meta_description[0]) ? $meta_description[0] : '';
		$meta['meta_keyword']     =  !empty($meta_keyword[0]) ? $meta_keyword[0] : '';
		
		$data['query_meta'] = seoMeta('category' ,$meta);
		$data['query_fb']   = fb_like($meta);
		
		
		$data['func'] = getUserMenu('index');		
		
		$this->load->view('frontend/common/header.tpl',$data);
		//$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/index.tpl');
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 廣告
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function ad(){
		
		$this->db->where('status' ,'1');
		$this->db->order_by('sort_order' ,'DESC');
		$data = $this->db->get('ad_home')->result_array();
        
        return $data;
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 商品資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function product(){
		
	  $this->load->model("frontend/product_model","product");	
	  	
	  return $this->product->lists($srh_data = array('srh_is_show_home' => 1
	  		                                        ,'srh_category_id_1' => 2
	  		                                        ,'srh_category_id_2' => '*' ));	  
   }
   
   // --------------------------------------------------------------------
   
   /**
    * 方法 : 助糧平台
    *
    * @access	public_grain
    * @param
    * @return
    */
   public function product_grain(){
   
   	$this->load->model("frontend/product_model","product");
   
   	return $this->product->lists($srh_data = array('srh_is_show_home' => 1
   			                                      ,'srh_category_id_1' => 3
   			                                      ,'srh_category_id_2' => '*' ));
   }
   
   // --------------------------------------------------------------------
   
   /**
    * 方法 : 熱銷好康
    *
    * @access	public
    * @param
    * @return
    */
   public function category_promo(){
   
     $this->db->where('status' ,1);
   	 $this->db->order_by('sort_order' ,'DESC');
   	 $this->db->limit(5);
   	 return $this->db->get('category_promo')->result_array();
   }
  
   // --------------------------------------------------------------------
   
   /**
    * 方法 : 活動訊息
    *
    * @access	public
    * @param
    * @return
    */
   public function news(){
   	 
   	$this->db->where('status' ,1);
   	$this->db->order_by('date_show' ,'DESC');
   	$this->db->limit(3);
   	return $this->db->get('news')->result_array();
   }
   
   // --------------------------------------------------------------------
    
   /**
    * 方法 : 活動訊息 (顯示在首頁)
    *
    * @access	public
    * @param
    * @return
    */
   public function news_is_show_home(){
   	 
   	  $this->db->where('status' ,1);
   	  $this->db->where('is_show_home' ,1);
   	  $this->db->order_by('cdate' ,'DESC');
   	  $this->db->limit(3);
   	
   	  return $this->db->get('news')->result_array();
   }
   
   // --------------------------------------------------------------------
   
   /**
    * 方法 : 取得頂級分類資料
    *
    * @access	public
    * @param
    * @return
    */
   public function getCategory($param){
   		
   	$this->db->where('category_id' ,$param['srh_category_id_1']);
   	$this->db->order_by('sort_order' ,'DESC');
   	$this->db->order_by('created_at' ,'DESC');
   
   	$query = $this->db->get('category')->row_array();
   
   	return $query;
   }
}


/* End of file index.tpl */
/* Location: ./application/controllers/frontend/index */