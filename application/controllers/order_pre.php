<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 說明 : 處理[預購單]的頁面請求
 * @controllerName order_pre
 * @author Dio
 *
 */
class order_pre extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->library(array('form_validation','session','Dio_gmail'));
		$this->load->helper(array('form','url','cookie','ctmall','motion','database','base','dio_upload'));
		
		$this->load->model("frontend/product/view_model","product_view");
		
		//發送Email
		$this->dio_email = new Dio_gmail();
		
		//定義類別變數
		$this->err_msg = "";
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 檢視
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function view(){
		
		$this->form_validation->set_rules('product_id','*商品編號','trim|required');
		$this->form_validation->set_rules('attr1','*商品屬性1','trim');
		$this->form_validation->set_rules('attr2','*商品屬性2','trim');
		$this->form_validation->set_rules('entity','*數量','trim|required');
		$this->form_validation->set_rules('name','*姓名','trim|required');
		$this->form_validation->set_rules('phone','*電話','trim|required');
		$this->form_validation->set_rules('email', '*電子信箱', 'trim|valid_email|required');
		$this->form_validation->set_rules('local','*區域碼','trim');
		$this->form_validation->set_rules('address','*地址','trim');
		$this->form_validation->set_rules('memo','*備註','trim');
		//$this->form_validation->set_rules('secure_code','* 驗證碼', 'callback_check_captcha');
		
		if ($this->form_validation->run() == TRUE){
			
			$customer_id = !empty($_SESSION['customer_info']['customer_id']) ? $_SESSION['customer_info']['customer_id'] : 0 ;
			
			$data = array(
					'customer_id' => $customer_id,
					'product_id' => $this->input->post('product_id'),
					'attr1'      => $this->input->post('attr1'),
					'attr2'      => $this->input->post('attr2'),
					'entity'     => $this->input->post('entity'),
					'name'       => $this->input->post('name'),
					'phone'      => $this->input->post('phone'),
					'email'      => $this->input->post('email'),
					'local'      => $this->input->post('local'),
					'address'    => $this->input->post('address'),
					'memo'       => $this->input->post('memo'),
					'cdate'      => date('Y-m-d H:i:s')
			);
				
			$this->db->insert('order_pre',$data);
				
			//寄送 Email 或 訊息
			/*
			$insert_id = $this->db->insert_id();
		
			$this->db->where('order_pre_id' ,$insert_id);
			$info = $this->db->get('order_pre')->row_array();
			
			if( $info['is_email_send'] == 0){
					
				$this->dio_email->order_pre($info);
		
				//更新發送狀態，避免重複發送
				$this->db->where('order_pre_id' ,$insert_id);
				$this->db->update('order_pre' ,array('is_email_send' => '1'));
			}*/			
			
			//$this->session->set_flashdata('msg','感謝您提供寶貴的意見 ,訊息已成功送出!');
			//redirect('order_pre/view');

		    dioRedirect('index.php' ,'*感謝您填寫預購單 ,訊息已成功送出!');
			  
		}else{
		
			$srh_category  = $this->input->get('srh_category');
			$product_id    = $this->input->get('product_id');
			$attr1         = $this->input->get('attr1');
			$attr2         = $this->input->get('attr2');
				
			$data['func'] = getUserMenu('order_pre_view');
			
			$data['query'] = $this->product_view->view($product_id);
			
			$data['srh_category'] = $srh_category;
			$data['product_id']   = $product_id;
			$data['attr1']        = $attr1;
			$data['attr2']        = $attr2;
			
			//檢視view
			$this->load->view('frontend/common/header.tpl',$data);
			$this->load->view('frontend/order_pre/view.tpl');
		}	
			
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢查 / 驗證碼
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function check_captcha($secure_code)
	{
	
		if(!$secure_code)
		{
			$this->form_validation->set_message('check_captcha', '*驗證碼 不可空白');
			return FALSE;
		}
	
		if( $secure_code != $_SESSION['captcha'] )
		{
			$this->form_validation->set_message('check_captcha', '*驗證碼必須一致');
			return FALSE;
		}
	
		return TRUE;
	}
	
	
}


/* End of file order_pre.tpl */
/* Location: ./application/controllers/frontend/order_pre */