<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 前台(會員中心)管理首頁
 * @controllerName Customer
 * @author Dio
 *
 */
class Customer extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){

		parent::__construct();

		//載入模組
		$this->load->helper(array('form','url','cookie','ctmall','database','base','motion'));
		$this->load->library(array('form_validation','session','promo/VIP'));
		
		//建立VIP物建
		$this->vip = new VIP();
		
		//登入驗證
		if(!IsLoginCustomer()){
			redirect('sign_in/login');
		}         

		/*********************************
		 /* 設置幣別
		 *********************************/
		if( empty($_SESSION['motion_currency']) ){
			$_SESSION['motion_currency'] = 'twd';
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 基本資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function info(){

		//資料驗證
		//$this->form_validation->set_rules('first_name','*姓','trim|required');
		$this->form_validation->set_rules('last_name','*姓名','trim|required');
		$this->form_validation->set_rules('mobile','*手機','trim|required|exact_length[10]');
		$this->form_validation->set_rules('phone','*電話','trim');
		$this->form_validation->set_rules('email','*電子郵件','callback_check_email');
		$this->form_validation->set_rules('local','*區碼','trim');
		$this->form_validation->set_rules('address','*地址','trim');
		$this->form_validation->set_rules('birthday_year','*生日(年)','trim');
		$this->form_validation->set_rules('birthday_month','*生日(月)','trim');
		$this->form_validation->set_rules('birthday_day','*生日(日)','trim');
		//$this->form_validation->set_rules('is_edm','*是否訂閱','trim');
		
		if ($this->form_validation->run() == TRUE){
			
			$data = array(
                         //'first_name' => $this->input->post('first_name'),
                          'last_name' => $this->input->post('last_name'),
		                     'mobile' => $this->input->post('mobile'),
                              'phone' => $this->input->post('phone'),
			                  'email' => $this->input->post('email'),
					          'local' => $this->input->post('local'),
					        'address' => $this->input->post('address'),
					  'birthday_year' => $this->input->post('birthday_year'),
					 'birthday_month' => $this->input->post('birthday_month'),
					   'birthday_day' => $this->input->post('birthday_day'),
					       //'is_edm' => $this->input->post('is_edm'),
					          'udate' => date('Y-m-d H:i:s') 
			);
				
			$this->db->where('customer_id', $_SESSION['customer_info']['customer_id']);
			$this->db->update('customer',$data);

			//更新SESSION
			$_SESSION['customer_info'] = $this->db->get_where('customer',array('customer_id' => $_SESSION['customer_info']['customer_id']))->row_array();

			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
				
			redirect('customer/info');
			
		} else { 

			$this->db->where('customer_id' ,$_SESSION['customer_info']['customer_id']);
			$data['query'] = $this->db->get('customer')->row_array();
		
			$data['func']  = 'customer_info';
						
			//檢視view
			$this->load->view('frontend/common/header.tpl' ,$data);			
			$this->load->view('frontend/customer/info.tpl');			
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 密碼
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function pass(){

		//資料驗證
		$this->form_validation->set_rules('passorig', '*舊密碼', 'callback_validate_passorig');
		$this->form_validation->set_rules('password', '*新密碼', 'trim|required|matches[passconf]|md5');
		$this->form_validation->set_rules('passconf', '*密碼確認', 'callback_check_pass['.$this->input->post('passconf').']');

		if ($this->form_validation->run() == TRUE){

			//將POST資料 ,轉為陣列
			$data = array( 'password' => $this->input->post('password') );

			$this->db->where('customer_id', $_SESSION['customer_info']['customer_id']);
			$this->db->update('customer',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
			
			redirect('customer/pass');
		
		}else{
				
			$this->db->where('customer_id' ,$_SESSION['customer_info']['customer_id']);
			$data['customer'] = $this->db->get('customer')->row_array();
		
			$data['func'] = 'customer_pass';
			
			$this->load->view('frontend/common/header.tpl' ,$data);			
			$this->load->view('frontend/customer/pass.tpl');			
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 頭像
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function photo(){

		//資料驗證
		$this->form_validation->set_rules('image','*相片','trim|required');

		if ($this->form_validation->run() == TRUE){

			//-----------------------------------------------------------
			// 圖片處理
			//-----------------------------------------------------------
			$this->load->helper(array('dio_upload'));

			$image = $this->input->post('image');

			// 圖片-主
			if ( isset($_FILES['image']['name']) && !empty($_FILES['image']['name']) ) {
				$image_ary =  dio_upload($_SESSION['customer_info']['customer_id'] ,'image');

				if(isset($image_ary['fail']) && !empty($image_ary['fail']) ){
					dioredirect('customer/dashboard' ,$image_ary['fail']);
					exit;
				}
					
				if(isset($image_ary['success']) && !empty($image_ary['success']) ){
					$image = $image_ary['success'];
				}
			}

			//將POST資料 ,轉為陣列
			$data = array( 'image' => $image );
				
			$this->db->where('customer_id', $_SESSION['customer_info']['customer_id']);
			$this->db->update('customer',$data);
				
			//更新SESSION
			$_SESSION['customer_info'] = $this->db->get_where('customer',array('customer_id' => $_SESSION['customer_info']['customer_id']))->row_array();
				
			dioredirect('customer/dashboard');

		}else{
				
			$this->db->where('customer_id' ,$_SESSION['customer_info']['customer_id']);
			$data['query'] = $this->db->get('customer')->row_array();
		
			$data['func'] = 'customer_photo';
			
			//檢視view
			$this->load->view('frontend/common/header.tpl' ,$data);
			
			$this->load->view('frontend/photo.tpl');
			
		}
	}

	
	/*
	 * ----------------------------------------------------------------------------------
	 *
	 *  底下為常用的驗證方法 :
	 *
	 * ----------------------------------------------------------------------------------
	 *
	 */

	/**
	 * 方法 : 驗證原密碼
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function validate_passorig($password){

		if( empty($password) ){
			$this->form_validation->set_message('validate_passorig', '*原密碼欄位不可空白!');
			return FALSE;
		}

		$this->db->where('customer_id' ,$_SESSION['customer_info']['customer_id']);
		$this->db->where('password' , MD5($password) );
		$query = $this->db->get('customer')->row_array();

		if(empty($query)){
			$this->form_validation->set_message('validate_passorig', '*原密碼錯誤，請重新輸入!');
			return FALSE;
		}

		return TRUE;
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢查密碼
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function check_pass($password ,$password_conf)
	{
		if( empty($password) || empty($password_conf))
		{
			$this->form_validation->set_message('check_pass', '*密碼確認 不可空白');
			return FALSE;
		}
	
		if( strlen($password) < 8 || strlen($password) > 16)
		{
			$this->form_validation->set_message('check_pass', '*密碼確認  必須介於8-16個字');
			return FALSE;
		}
	
		//if( !preg_match("/[a-z]/i", $password) )
		if( !preg_match("/[a-z]/", $password) )
		{
			$this->form_validation->set_message('check_pass', '*密碼確認  至少需要一個小寫字母');
			return FALSE;
		}
	
		return TRUE;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 驗證Email
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function check_email($email){
	
		$email = trim($email);
	
		if( empty($email) ){
			$this->form_validation->set_message('check_email', '*Email不可空白!');
			return FALSE;
		}
	
		if ( !filter_var($email, FILTER_VALIDATE_EMAIL) ) {
			$this->form_validation->set_message('check_email', '*Email格式不符');
			return FALSE;
		}
	
		$this->db->where('email' ,$email);
		$this->db->where('email <> ' ,$_SESSION['customer_info']['email']);
		$query = $this->db->get('customer');
	
		if( $query->num_rows() > 0 ){
			$this->form_validation->set_message('check_email', '*此Email已存在，請重新輸入!');
			return FALSE;
		}
	
		return TRUE;
	}
}


/* End of file customer.tpl */
/* Location: ./application/controllers/customer  */