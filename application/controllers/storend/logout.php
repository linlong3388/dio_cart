<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理[登出]的控制器
 * @controllerName logout
 * @author Dio
 *
 */
class logout extends Storend_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));

		//載入model
		$this->load->model("storend/dashboard_model","model");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 登出
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function main(){
			
		//清除'admin_info'的session值
		unset($_SESSION['admin_info']);

		dioRedirect('storend/login/valid','您已成功登出!');
	}

}


/* End of file logout.php */
/* Location: ./application/controllers/storend/logout.php */