<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 說明 : 處理[退貨單]的控制器。
 * 
 * @controllerName returns
 * @author Dio
 *
 */
class returns extends Storend_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session','category'));

		//登入驗證
		if(!is_login_store()){
			redirect('storend/login/valid');
		}
		
		//載入model
		$this->load->model("storend/return_model" ,"return");

		//建立商品分類物件
		$this->cls_category = new category();
		
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists(){
	
		$data['query'] = $this->return->shr_return_list();
	
		//檢視view
		$this->load->view('storend/common/header.tpl' ,$data);
		$this->load->view('storend/returns/lists.tpl');
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢視
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function view(){
	
		$this->form_validation->set_rules('memo_admin','*備註','trim');
		$this->form_validation->set_rules('status','*狀態','trim|required');
	
		if ($this->form_validation->run() == TRUE){
	
			$data = array(
					'memo_admin'  => $this->input->post('memo_admin'),
					'status'      => $this->input->post('status'),
			);
				
			$this->db->where('return_id' ,$this->input->post('return_id'));
			$this->db->update('return' ,$data);
				
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
			
			redirect('storend/returns/lists');
		
		}else{
	
			$data['query'] = $this->return->shr_return_detail($this->input->get('return_id'));
						
			//檢視view
			$this->load->view('storend/common/header.tpl' ,$data);
			$this->load->view('storend/returns/view.tpl');
		}
	
	}

}

/* End of file returns.php */
/* Location: ./application/controllers/storend/returns.php */