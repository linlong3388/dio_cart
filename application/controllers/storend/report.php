<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理[各式報表統計]的控制器
 * @controllerName report
 * @author Dio
 *
 */
class report extends Storend_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base' ,'is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));
		
		//登入驗證
		if(!is_login_store()){
			redirect('storend/login/valid');
		}

		$this->load->model("storend/statistics_model","statistics_model");
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 商品銷售統計
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function sales_statistics_list () {

		$srh_status = $this->input->get('srh_status',true);

		//搜尋條件
		$srh_data = array(
                       'srh_cdate1' => $this->input->get('srh_cdate1',true),
		               'srh_cdate2' => $this->input->get('srh_cdate2',true),
		           'srh_group_date' => $this->input->get('srh_group_date',true),
		               'srh_status' => $this->input->get('srh_status',true),
                   'srh_pay_method' => $this->input->get('srh_pay_method',true),
		               'srh_status' => empty($srh_status) ? "1,2" : $srh_status ,
		 
		             'srh_page_per' => 30,
	               	   'limit_page' => $this->input->get('page',true) 		
		);

		$data['srh_param'] = $srh_data;
		$data['query'] = $this->statistics_model->shr_sales_statistics($srh_data);

		//----------------------------------------------------------------------
		// 設定分頁
		//----------------------------------------------------------------------
		$query_group_by_ary = $this->statistics_model->shr_sales_statistics_group_by($srh_data);

		$this->load->library('Dio_paginator');
		$pages = new Dio_paginator();
		$pages->set($query_group_by_ary[0]['count'],5,array(30,3,6,9,12,25,50,100,250,'All'));

		$data['pages'] = $pages;
		$data['query_group_by'] = $query_group_by_ary;

		$page = 1;
		if($this->input->get('page')){
			$page = $this->input->get('page');
		}

		$data['page_startEnd'] = $pages->get_startEnd_page($page ,$query_group_by_ary[0]['count'] ,30);

		$this->load->view('storend/common/header.tpl' ,$data);
		$this->load->view('storend/report/sales_statistics_list.tpl');

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 商品購買統計
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function buy_statistics_list () {

		$srh_sort = $this->input->get('srh_sort',true);
		$srh_sort_type = $this->input->get('srh_sort_type',true);

		//搜尋條件
		$srh_data = array(
                       'srh_cdate1' => $this->input->get('srh_cdate1',true),
		               'srh_cdate2' => $this->input->get('srh_cdate2',true),
		               'srh_product_id' => $this->input->get('srh_product_id',true),
                       'srh_name' => $this->input->get('srh_name',true),
				       'srh_sort' => empty($srh_sort) ? 'product_speci.product_speci_id' : $srh_sort,
		               'srh_sort_type' => empty($srh_sort_type) ? 'DESC' : $srh_sort_type,

		               'srh_page_per' => 100,
	               	   'limit_page' => $this->input->get('page',true) 	
		);

		//觸發搜尋條件
		$data['srh_param'] = $srh_data;
		
		$data['query'] = $this->statistics_model->shr_buy_statistics($srh_data);

		//----------------------------------------------------------------------
		// 設定分頁
		//----------------------------------------------------------------------
		$query_group_by_ary = $this->statistics_model->shr_buy_statistics_group_by($srh_data);

		$this->load->library('Dio_paginator');
		$pages = new Dio_paginator();
		$pages->set($query_group_by_ary[0]['count'],5,array(100,3,6,9,12,25,50,100,250,'All'));

		$data['pages'] = $pages;
		$data['query_group_by'] = $query_group_by_ary;

		$page = 1;
		if($this->input->get('page')){
			$page = $this->input->get('page');
		}

		$data['page_startEnd'] = $pages->get_startEnd_page($page ,$query_group_by_ary[0]['count'] ,100);

		//檢視view
		$this->load->view('storend/common/header.tpl' ,$data);
		$this->load->view('storend/report/buy_statistics_list.tpl');
	}



}


/* End of file report.php */
/* Location: ./application/controllers/storend/report.php */