<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理[群組權限]的控制器
 * @controllerName admin_group_role
 * @author Dio
 *
 */
class admin_group_role extends Storend_Controller {
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->library(array('form_validation','session'));
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		
		//登入驗證
		if(!is_login_store()){
			redirect('storend/login/valid');
		}
		
		//權限驗證
		if(  $_SESSION['admin_info']['role'] != 66 ){
			redirect('storend/dashboard/main');
		}
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit () {
	
		$admin_group_id  = $this->input->get_post('admin_group_id');
		$data_post       = $this->input->post();
		
		if ( !empty($data_post) ){
					
			$this->updateAdminRole($data_post);
			     
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
			
			redirect('storend/admin_group_role/edit?admin_group_id='.$admin_group_id[0]);			
			
		} else { 
			
			$data['admin_group_id'] = $admin_group_id;
			
			//群組資料
			$this->db->where('admin_group_id' , $admin_group_id);
			$data['query'] = $this->db->get('admin_group')->row_array();
			
			//管理員權限
	        $data['query_role'] = $this->getAdminGroupRoleList($admin_group_id);
			 
			//檢視view
			$this->load->view("storend/common/header.tpl" ,$data);
			$this->load->view("storend/admin_group_role/edit.tpl");
		}

	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得群組權限列表 / 已註記
	 *
	 * @access	public
	 * @param
	 * @return  格式:  [ad_banner] => 橫幅廣告|0
     *                   [article] => 課程重點|1
	 */
	public function getAdminGroupRoleList($admin_group_id) {
		
		//已選取的權限
		$this->db->where('admin_group_id' , $admin_group_id);
		$query = $this->db->get('admin_group_role')->result_array();
		$query_role = transKeyPairArray($query, 'tb_name', 'tb_name');
		
		//所有功能權限
		$result = getAdminMenu();
		
		foreach ($result as $key=>$val){
			if( in_array($key, $query_role) ){
				$result[$key] = $val.'|1';
			}else{
				$result[$key] = $val.'|0';
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 更新權限
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function updateAdminRole($param) {

		$admin_group_id = $param['admin_group_id'];
		
		//刪除管理員權限
		$this->db->where('admin_group_id' ,$admin_group_id);
		$this->db->delete('admin_group_role');
		
		//排除 admin_group_id
		unset($param['admin_group_id']);
		
		foreach ( $param as $key=>$val) {
		        
			//寫入管理員權限
			$data = array(
					'admin_group_id' => $admin_group_id,
					'tb_name'  => $key,
					'cdate'    => date('Y-m-d H:i:s'),
			);
				
			$this->db->insert('admin_group_role',$data);
		}
		
	}
	

}


/* End of file admin_group_role.php */
/* Location: ./application/controllers/storend/admin_group_role.php */