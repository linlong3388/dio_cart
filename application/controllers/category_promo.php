<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 說明 : 處理[熱銷好康]的頁面請求
 * @controllerName category_promo
 * @author Dio
 *
 */
class category_promo extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->helper(array('url','cookie','ctmall','motion','database','base'));
		$this->load->library(array('form_validation','session'));
		
		$this->load->model("frontend/product_model","product");
		
		//定義類別變數
		$this->err_msg = "";
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists(){
	
		$srh_data_name = $this->input->get('srh_data');
			
		$pg = new Dio_pagination();
		$pg_ary = $pg->get_limit($this->input->get('page'),10);

		//取得列表資料
		$this->db->limit($pg_ary['limit2'] ,$pg_ary['limit1']);
		if( !empty($srh_data_name) ){
			$this->db->like('title', $srh_data_name);
		}
		
		$this->db->order_by('sort_order' ,'DESC');
		$this->db->where('status' ,1);
		$data['query'] = $this->db->get('category_promo')->result_array();
		
		//取得總數
		if( !empty($srh_data_name) ){
			$this->db->like('title', $srh_data_name);
		}
		
		$this->db->where('status' ,1);
		$data['query_total'] = $this->db->count_all_results('category_promo');
		
		//設定分頁
		$data['pagination']  = $pg->pagination($data['query_total'],5);
		$data['func'] = getUserMenu('category_promo_lists') ;
		
        //META 設定
        $data['query_meta'] = seoMeta('content' ,$data['query']);
        $data['query_fb']   = fb_like($data['query']);
        
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);
		//$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/category_promo/lists.tpl');
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢視
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function view(){
			
		$category_promo_id = $this->input->get('category_promo_id');
		$srh_sort          = $this->input->get('srh_sort');
		
		$data['query_info']  = $this->getInfo($category_promo_id);
		
		$query_product       =  $this->getProductList( array('category_promo_id' => $category_promo_id
				                                            ,'srh_sort'          => $srh_sort
		                                               )); //$this->product->lists();
		
		$data['query_product'] = $this->calcPromo($data['query_info']['rate_price'], $query_product);
		
		//META 設定
		$data['query_meta'] = seoMeta('content' ,$data['query_info']);
		$data['query_fb']   = fb_like($data['query_info']);
		
		$data['srh_data']   = $this->input->get();
		
		$data['func']       = getUserMenu('category_promo_view') ;
		
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);
		$this->load->view('frontend/category_promo/view.tpl');
	}	
		
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 折扣計算
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function calcPromo($rate ,$param){
		 
        foreach ($param as $key=>$row){
        	$param[$key]['price'] = $row['price'] * (1 - $rate/100) ; 
        }
        
        return $param;
	}	
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得商品列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getProductList( $param ){
		 
		$sql_sort = $this->sort($param);
		
		$sql = "SELECT p.*
		          FROM category_promo_product cpp
		        LEFT JOIN product p ON cpp.product_id = p.product_id
		          WHERE cpp.category_promo_id= '".$param['category_promo_id']."'
		          AND p.status = 1
		         {$sql_sort} ";
		
		$query = $this->db->query($sql)->result_array();
		
		return $query;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法: 排序條件
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function sort($srh_data){
	
		$sql_sort = "";
	
		//串接SQL語句ORDER BY
		if( !empty($srh_data['srh_sort']) ){
	
			switch ($srh_data['srh_sort']) {
	
				case 'news':
					$sql_sort = " ORDER BY `cdate` DESC";
					break;
	
				case 'price_high':
					$sql_sort = " ORDER BY `price` DESC";
					break;
	
				case 'price_low':
					$sql_sort = " ORDER BY `price` ASC";
					break;
						
				case 'char_low':
					$sql_sort = " ORDER BY `name` ASC";
					break;
	
				default:
					$sql_sort = " ORDER BY `sort_order` ASC";
					break;
			}
	
		}
	
		return $sql_sort;
	}
	
   // --------------------------------------------------------------------
    
   /**
    * 方法 : 取得基本資料
    *
    * @access	public
    * @param
    * @return
    */
   public function getInfo($category_promo_id){
   
   	  $this->db->where('status' ,1);
   	  $this->db->where('category_promo_id' ,$category_promo_id);
   	  
   	  return $this->db->get('category_promo')->row_array();
   }
   
   // --------------------------------------------------------------------
   
   /**
    * 方法 : 定義臉書分享
    *
    * @access	public
    * @param
    * @return
    */
   public function set_fbShare($query){
   
     	return array('is_fbShare' => true ,
   			'og_title'        => $query['title'] ,
   			'og_description'  => $query['content'] ,
   			'og_image'        => $query['image']
   	    );
   }
    
   
}


/* End of file category_promo.tpl */
/* Location: ./application/controllers/frontend/category_promo.tpl */