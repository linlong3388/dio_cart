<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 說明 : 處理[知識專欄]的頁面請求
 * @controllerName knowledge
 * @author Dio
 *
 */
class knowledge extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->helper(array('url','cookie','ctmall','motion','database','base'));
		$this->load->library(array('form_validation','session'));
		
		//定義類別變數
		$this->err_msg = "";
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists(){
	
		$knowledge_category_id = $this->input->get('knowledge_category_id');
				
		$pg = new Dio_pagination();
		$pg_ary = $pg->get_limit($this->input->get('page'),10);

		//取得列表資料
		$this->db->limit($pg_ary['limit2'] ,$pg_ary['limit1']);
		if( isset($knowledge_category_id) && is_numeric($knowledge_category_id) ){
			$this->db->where('knowledge_category_id',$knowledge_category_id);
		}
		$this->db->order_by('cdate' ,'DESC');
		$this->db->where('status' ,1);
		$data['query'] = $this->db->get('knowledge')->result_array();
				
		//取得總數
		if( isset($knowledge_category_id) && is_numeric($knowledge_category_id) ){
			$this->db->where('knowledge_category_id',$knowledge_category_id);
		}
		$this->db->where('status' ,1);
		$data['query_total'] = $this->db->count_all_results('knowledge');
		
		//設定分頁
		$data['pagination']  = $pg->pagination($data['query_total'],5);
		
		$data['knowledge_category_id'] = $knowledge_category_id;			
		
	    $data['func'] = 'knowledge_lists';
		
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);
		//$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/knowledge/lists.tpl');
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢視
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function view(){
	
		$knowledge_id = $this->get_knowledge_id($this->input->get('knowledge_id'));
		
		//$data['ctr']        = $this->ctr($new_id);
		$this->db->order_by('sort_order' ,'DESC');		
		$data['query_title'] = $this->db->get('knowledge')->result_array();
		$data['query']      = $this->get_knowledge_view($knowledge_id);
		
		//META 設定
		$data['query_meta'] = seoMeta('content' ,$data['query']);
		$data['query_fb']   = fb_like($data['query']);
		
		$data['func']       = getUserMenu('knowledge_view');
		$data['func']['id'] = $knowledge_id;
		
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);
		//$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/knowledge/view.tpl');
	}	
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得knowledge_id
	 *       適用於 view，當後台資料刪除時，必需確保最新的一筆資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function get_knowledge_id($knowledge_id){
	
		if( empty($knowledge_id) ){
	
			$this->db->order_by('sort_order' ,'DESC');
			$query = $this->db->get('knowledge')->row_array();
			 
			return $query['knowledge_id'];
		}
	
		return $knowledge_id;
	}

	// --------------------------------------------------------------------
	 
	/**
	 * 方法 : 點擊率累加
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function ctr($knowledge_id){
		 
		$sql = "UPDATE `knowledge` SET `ctr_unlimited` = ctr_unlimited + 1 ,
   			                   `ctr_month` = ctr_month + 1
	 			  WHERE `knowledge`.`knowledge_id` = ".$knowledge_id;
		 
		$this->db->query($sql);
	}
		
   // --------------------------------------------------------------------
    
   /**
    * 方法 : 取得最新消息/檢視
    *
    * @access	public
    * @param
    * @return
    */
   public function get_knowledge_view($knowledge_id){
   
   	  $this->db->where('status' ,1);
   	  $this->db->where('knowledge_id' ,$knowledge_id);
   	  
   	  return $this->db->get('knowledge')->row_array();
   }
   
   // --------------------------------------------------------------------
   
   /**
    * 方法 : 定義臉書分享
    *
    * @access	public
    * @param
    * @return
    */
   public function set_fbShare($query){
   
     	return array('is_fbShare' => true ,
   			'og_title'        => $query['title'] ,
   			'og_description'  => $query['content'] ,
   			'og_image'        => $query['image']
   	    );
   }
    
   
}


/* End of file knowledge.tpl */
/* Location: ./application/controllers/frontend/knowledge.tpl */