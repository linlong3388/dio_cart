<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 說明 : 處理[常見問題]的頁面請求
 * @controllerName qa
 * @author Dio
 *
 */
class qa extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->helper(array('url','cookie','ctmall','motion','database','base'));
		$this->load->library(array('form_validation','session'));
		
		//定義類別變數
		$this->err_msg = "";
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists(){
	
		$qa_category_id = $this->input->get('qa_category_id');
				
		$pg = new Dio_pagination();
		$pg_ary = $pg->get_limit($this->input->get('page'),10);

		//取得列表資料
		$this->db->limit($pg_ary['limit2'] ,$pg_ary['limit1']);
		if( isset($qa_category_id) && is_numeric($qa_category_id) ){
			$this->db->where('qa_category_id',$qa_category_id);
		}
		$this->db->order_by('sort_order' ,'DESC');
		$this->db->where('status' ,1);
		$data['query'] = $this->db->get('qa')->result_array();
				
		//取得總數
		if( isset($qa_category_id) && is_numeric($qa_category_id) ){
			$this->db->where('qa_category_id',$qa_category_id);
		}
		$this->db->where('status' ,1);
		$data['query_total'] = $this->db->count_all_results('qa');
		
		//設定分頁
		$data['pagination']  = $pg->pagination($data['query_total'],5);
		
		$data['qa_category_id'] = $qa_category_id;			
		
	    $data['func'] = getUserMenu('qa_lists');
		
	    //QA分類
	    $this->db->where('qa_category_id' ,$qa_category_id);
	    $data['query_category'] = $this->db->get('qa_category')->row_array();
	    
	    //META 設定
	    $data['query_meta'] = seoMeta('category' ,$data['query_category']);
	    $data['query_fb']   = fb_like($data['query_category']);
	    
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);
		//$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/qa/lists.tpl');
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢視
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function view(){
	
		$qa_category_id = $this->input->get('qa_category_id');
		$qa_id          = 1;
		
		//$data['ctr']        = $this->ctr($qa_id);		
		$this->db->where('status' ,1);
		$this->db->where('qa_id' ,$qa_id);
		$data['query']  = $this->db->get('qa')->row_array();

		//META 設定
		$data['query_meta'] = seoMeta('content' ,$data['query']);
		$data['query_fb']   = fb_like($data['query']);
		
		$data['qa_category_id'] = $qa_category_id;
		
		$data['func']   = getUserMenu('qa_view') ;
		
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);
		//$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/qa/view.tpl');
	}	

	// --------------------------------------------------------------------
	 
	/**
	 * 方法 : 點擊率累加
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function ctr($qa_id){
		 
		$sql = "UPDATE `qa` SET `ctr_unlimited` = ctr_unlimited + 1 ,
   			                   `ctr_month` = ctr_month + 1
	 			  WHERE `qa`.`qa_id` = ".$qa_id;
		 
		$this->db->query($sql);
	}
   
   // --------------------------------------------------------------------
   
   /**
    * 方法 : 定義臉書分享
    *
    * @access	public
    * @param
    * @return
    */
   public function set_fbShare($query){
   
     	return array('is_fbShare' => true ,
   			'og_title'        => $query['title'] ,
   			'og_description'  => $query['content'] ,
   			'og_image'        => $query['image']
   	    );
   }
    
   
}


/* End of file qa.tpl */
/* Location: ./application/controllers/frontend/qa.tpl */