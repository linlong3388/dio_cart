<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 說明 : 處理[品牌總覽]的頁面請求
 * @controllerName brand
 * @author Dio
 *
 */
class brand extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->helper(array('url','cookie','ctmall','motion','database','base'));
		$this->load->library(array('form_validation','session'));

		//定義類別變數
		$this->err_msg = "";
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists(){
	
		$brand_category_id = $this->input->get('brand_category_id');
				
		$pg = new Dio_pagination();
		$pg_ary = $pg->get_limit($this->input->get('page'),10);

		//取得列表資料
		$this->db->limit($pg_ary['limit2'] ,$pg_ary['limit1']);
		if( isset($brand_category_id) && is_numeric($brand_category_id) ){
			$this->db->where('brand_category_id',$brand_category_id);
		}
		$this->db->order_by('cdate' ,'DESC');
		$this->db->where('status' ,1);
		$data['query'] = $this->db->get('brand')->result_array();
		
		//取得總數
		if( isset($brand_category_id) && is_numeric($brand_category_id) ){
			$this->db->where('brand_category_id',$brand_category_id);
		}
		$this->db->where('status' ,1);
		$data['query_total'] = $this->db->count_all_results('brand');
		
		//設定分頁
		$data['pagination']  = $pg->pagination($data['query_total'],10);
		
		$data['brand_category_id'] = $brand_category_id;		
			
	    $data['func'] = 'brand_lists';
		
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);
		$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/brand/lists.tpl');
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢視
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function view(){
		
		$this->db->where('brand_id' ,$this->input->get('brand_id'));
		$data['query'] = $this->db->get('brand')->row_array();
		
		$data['query_product'] = $this->get_product($this->input->get('brand_id'));
		
		$data['func'] = 'brand_view';
		
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);
		$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/brand/view.tpl');
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得商品資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function get_product($brand_id){
		
		$sql = "SELECT p.* ,b.title
		         FROM `product` p
	    	     LEFT JOIN brand b ON p.brand_id = b.brand_id
		        WHERE p.brand_id = ? ";
		
		$query = $this->db->query($sql,array($brand_id))->result_array();
			
		if( !empty($query) ){
			return $query;
		}else{
			return array();
		}
	}
	
	
   
}


/* End of file brand.tpl */
/* Location: ./application/controllers/frontend/brand.tpl */