<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 前台(會員中心/信用卡)管理首頁
 * @controllerName customer_credit_card
 * @author Dio
 *
 */
class customer_credit_card extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){

		parent::__construct();

		//載入模組
		$this->load->helper(array('form','url','cookie','ctmall','database','base','motion'));
		$this->load->library(array('form_validation','session'));
		
		//登入驗證
		if(!IsLoginCustomer()){
			redirect('login/main');
		}         

		/*********************************
		 /* 設置幣別
		 *********************************/
		if( empty($_SESSION['motion_currency']) ){
			$_SESSION['motion_currency'] = 'twd';
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 付款方式 /列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function payment_list(){
		
		$this->db->where('customer_id' ,$_SESSION['customer_info']['customer_id']);
		$this->db->order_by('cdate' ,'DESC');
		$data['query'] = $this->db->get('customer_credit_card')->result_array();
	
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);
		$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/payment_list.tpl');
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 付款方式 /新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function payment_ins(){
	
		//資料驗證
		$this->form_validation->set_rules('card_number','*信用卡號','trim|required');
		$this->form_validation->set_rules('last3_number','*末三碼','trim|required');
		$this->form_validation->set_rules('card_month','*到期日/月','trim|required');
		$this->form_validation->set_rules('card_year','*到期日/年','trim|required');
	
		if ($this->form_validation->run() == TRUE){
	
			//將POST資料 ,轉為陣列
			$data = array(
					'customer_id' => $_SESSION['customer_info']['customer_id'],
					'card_number' => $this->input->post('card_number'),
					'last3_number' => $this->input->post('last3_number'),
					'card_month' => $this->input->post('card_month'),
					'card_year' => $this->input->post('card_year'),
					'cdate' => date('Y-m-d H:i:s')
			);
	
			$this->db->insert('customer_credit_card',$data);
			dioredirect('customer/payment_list');
	
		}else{
		
			$this->db->where('customer_id' ,$_SESSION['customer_info']['customer_id']);
			$data['query'] = $this->db->get('customer_credit_card')->result_array();
	
			//檢視view
			$this->load->view('frontend/common/header.tpl',$data);
			$this->load->view('frontend/common/menu.tpl');
			$this->load->view('frontend/payment_ins.tpl');
		}
	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 付款方式 /編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function payment_upd(){
	
		//資料驗證
		$this->form_validation->set_rules('card_number','*信用卡號','trim|required');
		$this->form_validation->set_rules('last3_number','*末三碼','trim|required');
		$this->form_validation->set_rules('card_month','*到期日/月','trim|required');
		$this->form_validation->set_rules('card_year','*到期日/年','trim|required');
	
		if ($this->form_validation->run() == TRUE){
	
			//將POST資料 ,轉為陣列
			$data = array(
					'card_number' => $this->input->post('card_number'),
					'last3_number' => $this->input->post('last3_number'),
					'card_month' => $this->input->post('card_month'),
					'card_year' => $this->input->post('card_year'),
					'is_default' => $this->input->post('is_default'),
					'status' => $this->input->post('status')
			);
	
			$this->db->where('customer_credit_card_id' ,$this->input->post('customer_credit_card_id'));
			$this->db->update('customer_credit_card',$data);
	
			dioredirect('customer/payment_list');
	
		}else{
	
			$this->db->where('customer_credit_card_id' ,$this->input->get('customer_credit_card_id'));
			$data['query'] = $this->db->get('customer_credit_card')->row_array();
	
			//檢視view
			$this->load->view('frontend/common/header.tpl',$data);
			$this->load->view('frontend/common/menu.tpl');
			$this->load->view('frontend/payment_upd.tpl');
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 付款方式  /刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function payment_del() {
			
		$this->db->delete('customer_credit_card' ,array('customer_credit_card_id' => $this->input->get('customer_credit_card_id')));
		dioredirect('customer/payment_list');
	}
	

}


/* End of file customer_credit_card.tpl */
/* Location: ./application/controllers/customer_credit_card  */