<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 前台(會員中心/商品提問)管理首頁
 * @controllerName customer_product_question
 * @author Dio
 *
 */
class customer_product_question extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		//載入模組
		$this->load->helper(array('form','url','cookie','ctmall','database','base','motion'));
		$this->load->library(array('form_validation','session'));
		
		//登入驗證
		if(!IsLoginCustomer()){
			redirect('login/main');
		}         

		/*********************************
		 /* 設置幣別
		 *********************************/
		if( empty($_SESSION['motion_currency']) ){
			$_SESSION['motion_currency'] = 'twd';
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists(){	
		
		$this->db->select('product_question.product_question_id ,product.category_id ,product.product_id ,product.name ,product_question.status ,product_question.cdate');
		$this->db->where('customer_id' ,$_SESSION['customer_info']['customer_id']);
		$this->db->order_by('product_question_id' ,'DESC');
		$this->db->join('product', 'product.product_id = product_question.product_id', 'left');
		$data['query'] = $this->db->get('product_question')->result_array();
		
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);
		$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/customer/product_question/lists.tpl');
		
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del() {
			
		$this->db->where('customer_id' ,$_SESSION['customer_info']['customer_id']);
		$this->db->delete('customer_product_question' ,array('customer_product_question_id' => $this->input->get('customer_product_question_id')));
	
		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
			
		redirect('customer_product_question/lists');
	}	
	
}


/* End of file favorites.php */
/* Location: ./application/controllers/frontend/favorites.php */