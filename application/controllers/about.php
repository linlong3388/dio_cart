<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 說明 : 處理[關於我們]的頁面請求
 * @controllerName about
 * @author Dio
 *
 */
class about extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->helper(array('url','cookie','ctmall','motion','database','base'));
		$this->load->library(array('form_validation','session'));
		
		//定義類別變數
		$this->err_msg = "";
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists(){
	
		$about_category_id = $this->input->get('about_category_id');
				
		$pg = new Dio_pagination();
		$pg_ary = $pg->get_limit($this->input->get('page'),10);

		//取得列表資料
		$this->db->limit($pg_ary['limit2'] ,$pg_ary['limit1']);
		if( isset($about_category_id) && is_numeric($about_category_id) ){
			$this->db->where('about_category_id',$about_category_id);
		}
		$this->db->order_by('cdate' ,'DESC');
		$this->db->where('status' ,1);
		$data['query'] = $this->db->get('about')->result_array();
				
		//取得總數
		if( isset($about_category_id) && is_numeric($about_category_id) ){
			$this->db->where('about_category_id',$about_category_id);
		}
		$this->db->where('status' ,1);
		$data['query_total'] = $this->db->count_all_results('about');
		
		//設定分頁
		$data['pagination']  = $pg->pagination($data['query_total'],5);
		
		$data['about_category_id'] = $about_category_id;			
		
	    $data['func'] = 'about_lists';
		
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);
		//$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/about/lists.tpl');
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢視
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function view(){
		
		$about_id = $this->get_about_id($this->input->get('about_id'));
		
		//$data['ctr']        = $this->ctr($new_id);		
		$this->db->order_by('sort_order' ,'DESC');
		$data['query_title'] = $this->db->get('about')->result_array();
		
		$data['query']      = $this->get_about_view($about_id);
		
		//META 設定
		$data['query_meta'] = seoMeta('content' ,$data['query']);
		$data['query_fb']   = fb_like($data['query']);
		
		$data['func']       = 'about_view';
		
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);
		//$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/about/view.tpl');
	}	
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得about_id
	 *       適用於 view，當後台資料刪除時，必需確保最新的一筆資料 
	 *       
	 * @access	public
	 * @param
	 * @return
	 */
	public function get_about_id($about_id){

		if( empty($about_id) ){

		   $this->db->order_by('sort_order' ,'DESC');
		   $query = $this->db->get('about')->row_array();
		   
		   return $query['about_id'];
       }
		
		return $about_id;
	}

	// --------------------------------------------------------------------
	 
	/**
	 * 方法 : 點擊率累加
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function ctr($about_id){
		 
		$sql = "UPDATE `about` SET `ctr_unlimited` = ctr_unlimited + 1 ,
   			                   `ctr_month` = ctr_month + 1
	 			  WHERE `about`.`about_id` = ".$about_id;
		 
		$this->db->query($sql);
	}
		
   // --------------------------------------------------------------------
    
   /**
    * 方法 : 取得最新消息/檢視
    *
    * @access	public
    * @param
    * @return
    */
   public function get_about_view($about_id){
   
   	  $this->db->where('status' ,1);
   	  $this->db->where('about_id' ,$about_id);
   	  
   	  return $this->db->get('about')->row_array();
   }
   
   // --------------------------------------------------------------------
   
   /**
    * 方法 : 定義臉書分享
    *
    * @access	public
    * @param
    * @return
    */
   public function set_fbShare($query){
   
     	return array('is_fbShare' => true ,
   			'og_title'        => $query['title'] ,
   			'og_description'  => $query['content'] ,
   			'og_image'        => $query['image']
   	    );
   }
    
   
}


/* End of file about.tpl */
/* Location: ./application/controllers/frontend/about.tpl */