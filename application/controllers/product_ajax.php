<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 商品(Ajax)
 *
 * 說明 : 提供Ajax專用的請求控制器
 * @controllerName Center
 * @author Dio
 *
 */
class Product_ajax extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->helper(array('base','url'));
		
		$this->load->model("frontend/product_model","product");
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 商品資料 / ajax
	 *       顯示在燈箱(右側資料)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function info_ajax(){
	
		$product_id = $this->input->get('product_id');
	
		//取(主)表資料
		$query = $this->product->get_view($product_id);
	
		$query['name']  = $query[changeLanguageDb('name')];
		
		
		//取(副)表資料
		$size  = array();
		$color = array();
	
		$query_detail = $this->product->get_view_detail( array('product_id' => $product_id) );
	
		foreach ($query_detail as $row){
			$size[$row['property_id_size']]   = $row[changeLanguageDb('prs_name')];
			$color[$row['property_id_color']] = $row['product_speci_id'].','.$row['property_id_color'].','.$row['prc_name'] ;
		}
	
		$query['size']  = $size;
		$query['color'] = $color;
		
		
		
		echo json_encode($query);
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 商品資料2 / ajax
	 *       顯示在燈箱(左側圖)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function info_ajax2(){
	
		$product_speci_id = $this->input->get('product_speci_id');
	
		$this->db->where('product_speci_id' ,$product_speci_id);
		$query = $this->db->get('product_speci_image')->result_array();
	
		echo json_encode($query);
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 商品資料3 / ajax
	 *       尺寸帶出顏色
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function info_ajax3(){
	
		$product_id       = $this->input->get('product_id');
		$property_id_size = $this->input->get('property_id_size');
	
		$size  = array();
		$color = array();
	
		$sql = "SELECT ps.*
		          ,prc.name as prc_name ,prs.name as prs_name
				  ,prc.name_en as prc_name_en ,prs.name_en as prs_name_en
		          ,prc.name_jp as prc_name_jp ,prs.name_jp as prs_name_jp
		        FROM `product_speci` ps
		          LEFT JOIN `property` prc ON ps.property_id_color = prc.property_id
	         	  LEFT JOIN `property` prs ON ps.property_id_size = prs.property_id
		        WHERE  ps.product_id = '".$product_id."'
		        AND ps.property_id_size =  '".$property_id_size."'";
		
		$query_detail = $this->db->query($sql)->result_array();
		
		foreach ($query_detail as $row){
			$size[$row['property_id_size']]   = $row['prs_name'] ;
			$color[$row['property_id_color']] = $row['product_speci_id'].','.$row['property_id_color'].','.$row[changeLanguageDb('prc_name')];
		}
	
		$query['size']  = $size;
		$query['color'] = $color;	
		
		echo json_encode($query);
	}

}


/* End of file center.php */
/* Location: ./application/controllers/product_ajax.php */