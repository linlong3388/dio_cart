<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 說明 : 處理[聯絡我們]的頁面請求
 * @controllerName content
 * @author Dio
 *
 */
class contact extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->library(array('form_validation','session','Dio_gmail','google_captcha/dio_google_captcha'));
		$this->load->helper(array('form','url','cookie','ctmall','motion','database','base','dio_upload'));
		
		//發送Email
		$this->dio_email = new Dio_gmail();
		$this->g_captcha = new dio_google_captcha();
		
		//定義類別變數
		$this->err_msg = "";
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 檢視
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function view(){
		
		//$this->form_validation->set_rules('contact_category_id','*聯絡事項','trim|required');
		//$this->form_validation->set_rules('branch_id','*服務據點','trim|required|numeric');
		$this->form_validation->set_rules('name','*姓名','trim|required');
		$this->form_validation->set_rules('phone','*電話','trim|required');
		$this->form_validation->set_rules('email', '*電子信箱', 'trim|valid_email|required');
		//$this->form_validation->set_rules('title','*主旨','trim');
		$this->form_validation->set_rules('content','*內容','trim|required');
		//$this->form_validation->set_rules('g-recaptcha-response','* 圖形驗證碼', 'callback_check_google_captcha');
		
		if ($this->form_validation->run() == TRUE){
			
			$data = array(
					//'contact_category_id' => $this->input->post('contact_category_id'),
					//'branch_id'  => $this->input->post('branch_id'),
					'name'       => $this->input->post('name'),
					'phone'      => $this->input->post('phone'),
					'email'      => $this->input->post('email'),
					//'title'      => $this->input->post('title'),
					'content'    => $this->input->post('content'),
					'status'     => 0,
					'cdate'      => date('Y-m-d H:i:s')
			);
				
			$this->db->insert('contact',$data);
				
			//寄送 Email 或 訊息
			$insert_id = $this->db->insert_id();
		
			$this->db->where('contact_id' ,$insert_id);
			$info = $this->db->get('contact')->row_array();

			if( $info['is_email_send'] == 0){
					
				$this->dio_email->contact($info);
		
				//更新發送狀態，避免重複發送
				$this->db->where('contact_id' ,$insert_id);
				$this->db->update('contact' ,array('is_email_send' => '1'));
			}			
			
			//$this->session->set_flashdata('msg','感謝您提供寶貴的意見 ,訊息已成功送出!');
			//redirect('contact/view');

		    dioRedirect('contact/view' ,'*感謝您提供寶貴的意見 ,訊息已成功送出!');
			  
		}else{
		
			$result = $this->getBranchToMap();
			
			$data['map']      = $result['map'];
			$data['map_sub']  = $result['map_sub'];
						
			//META 設定
			$meta['meta_title']       =  !empty($_SESSION['sys_info']['meta_title']) ? $_SESSION['sys_info']['meta_title'] : '';
			$meta['meta_description'] =  !empty($_SESSION['sys_info']['meta_description']) ? $_SESSION['sys_info']['meta_description'] : '';
			$meta['meta_keyword']     =  !empty($_SESSION['sys_info']['meta_keyword']) ? $_SESSION['sys_info']['meta_keyword'] : '';
			
			$data['query_meta'] = seoMeta('content' ,$meta);
			
			$data['func']     = 'contact_view';
			
			//檢視view
			$this->load->view('frontend/common/header.tpl',$data);
			$this->load->view('frontend/contact/view.tpl');
		}	
			
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢查 / 驗證碼
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function check_captcha($secure_code)
	{
	
		if(!$secure_code)
		{
			$this->form_validation->set_message('check_captcha', '*驗證碼 不可空白');
			return FALSE;
		}
	
		if( $secure_code != $_SESSION['captcha'] )
		{
			$this->form_validation->set_message('check_captcha', '*驗證碼必須一致');
			return FALSE;
		}
	
		return TRUE;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢查 / google圖形驗證碼
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function check_google_captcha($g_recaptcha_response)
	{
	
		if(!$g_recaptcha_response)
		{
			$this->form_validation->set_message('check_google_captcha', '*圖形驗證碼 不可空白');
			return FALSE;
		}
	
		$is_g_captcha = $this->g_captcha->valid( $g_recaptcha_response  );
	
		if( !empty($is_g_captcha) )
		{
			$this->form_validation->set_message('check_google_captcha', '*圖形驗證碼必須一致');
			return FALSE;
		}
	
		return TRUE;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得服務據點並轉GoogleMap格式
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getBranchToMap(){
		
		$data     = array();
		$map      = "";
		$map_sub  = "";
		
		foreach (getBranch() as $key=>$val) {
			$map     .= "{'title': '".$val['title']."', 'add': '".$val['local'].$val['addr']."', 'tel': '".$val['phone']."', 'fax': '".$val['fax']."', 'lat': ".$val['latitude'].", 'lng': ".$val['longitude']."} ," ;
			$map_sub .= "{lat: ".$val['latitude'].", lng: ".$val['longitude']."},";
		}
		
		$data['map']     = rtrim($map ,',');
		$data['map_sub'] = rtrim($map_sub ,',');
		
		return $data;
    }
	
}


/* End of file content.tpl */
/* Location: ./application/controllers/frontend/content */