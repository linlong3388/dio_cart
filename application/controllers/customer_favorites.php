<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 前台(會員中心/收藏)管理首頁
 * @controllerName customer_favorites
 * @author Dio
 *
 */
class customer_favorites extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		//載入模組
		$this->load->helper(array('form','url','cookie','ctmall','database','base','motion'));
		$this->load->library(array('form_validation','session'));

		
		/*********************************
		 /* 設置幣別
		 *********************************/
		if( empty($_SESSION['motion_currency']) ){
			$_SESSION['motion_currency'] = 'twd';
		}
		
		$this->err_msg = 0;
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 加入項目(採Ajax)
	 *
	 * @access	public
	 * @param
	 * @return json
	 */
	public function add(){
	
		$product_id = $this->input->get_post('product_id');
		
		//登入驗證
		if(!IsLoginCustomer()){
			$this->err_msg = '請先登入會員!';
		}elseif( !is_numeric($product_id) ){
			$this->err_msg = '抱歉!商品ID不正確!';
		}elseif( !$this->is_duplicate($_SESSION['customer_info']['customer_id'] ,$product_id) ){
			$this->err_msg = '該商品你已收藏了!';
		}
		else{
			
		    $data = array(
		              'customer_id'  => $_SESSION['customer_info']['customer_id'],
				      'product_id'   => $product_id,
			          'cdate'        => date('Y-m-d H:i:s')
		    );
			
		    $this->db->insert('customer_favorites' ,$data);
		}
		
		echo json_encode($this->err_msg);		
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists(){
	
		//登入驗證
		if(!IsLoginCustomer()){
			redirect('sign_in/login');
		}		
		
		$this->db->where('customer_id' ,$_SESSION['customer_info']['customer_id']);
		$this->db->order_by('customer_favorites_id' ,'DESC');
		$this->db->join('product', 'product.product_id = customer_favorites.product_id', 'left');
		$data['query'] = $this->db->get('customer_favorites')->result_array();
		
		$data['func'] = 'customer_favorite_lists';
		
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);
		$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/customer/favorite/lists.tpl');
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del() {
			
		//登入驗證
		if(!IsLoginCustomer()){
			redirect('sign_in/login');
		}
		
		$this->db->where('customer_id' ,$_SESSION['customer_info']['customer_id']);
		$this->db->delete('customer_favorites' ,array('customer_favorites_id' => $this->input->get('customer_favorites_id')));
	
		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
			
		redirect('customer_favorites/lists');
	}	
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 判斷資料是否重複
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function is_duplicate($customer_id ,$product_id) {
			
		//登入驗證
		if(!IsLoginCustomer()){
			redirect('sign_in/login');
		}
	
		$this->db->where('customer_id' ,$customer_id);
		$this->db->where('product_id' ,$product_id);
		$query = $this->db->get('customer_favorites');
		
		if( $query->num_rows() <= 0 ){
			return true;
		}else{
			return false;
		}

	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 訂購商品
	 * 說明 : 由收藏轉入購物車
	 *       ( ps: ctmall的舊程式，待重構 ) 
	 *       
	 * @access	public
	 * @param
	 * @return
	 */
	/*
	public function favorites_to_cart () {
	
		//登入驗證
		if(!isset($_SESSION['customer_info']['customer_id']) || empty($_SESSION['customer_info']['customer_id']) ){
			redirect('index/view');
		}
	
		$this->db->where('customer_favorites_id' ,$this->input->get('customer_favorites_id'));
		$query = $this->db->get('customer_favorites')->row_array();
	
		$data = array(
				'parent_category_id' => '1', //根目錄分類暫時設定'1'
				'category_id'        => $query['category_id'],
				'product_id'         => $query['product_id'],
				'vendor_id'          => $query['vendor_id'],
				'name'               => $query['name'],
				'model'              => $query['model'],
				'image'              => $query['image'],
				'entity'             => $query['entity'],
				'price'              => $query['price'],
				'total'              => $query['entity'] * $query['price'],
				'cdate'              => date('Y-m-d H:i:s')
		);
	
		$this->cls_cart->add( $data );
	
		if(IsLoginCustomer()){
			//寫入購物車記錄表
			$this->cart_log->setCartData($_SESSION['my_order']['cart']);
		}
	
		dioredirect('member/favorites' ,'恭喜，商品已順利加入購物車!');
	}*/
	
	
	
	
}


/* End of file favorites.php */
/* Location: ./application/controllers/frontend/favorites.php */