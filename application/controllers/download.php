<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 說明 : 處理[檔案下載]的頁面請求
 * @controllerName download
 * @author Dio
 *
 */
class download extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->helper(array('url','cookie','ctmall','motion','database','base'));
		$this->load->library(array('form_validation','session'));
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists(){
			
		$download_category_id = $this->input->get('download_category_id');
		$city = $this->input->get('city');
		
		$pg = new Dio_pagination();
		$pg_ary = $pg->get_limit($this->input->get('page'),99999);
		
		//取得列表資料
		$this->db->limit($pg_ary['limit2'] ,$pg_ary['limit1']);
		if( isset($download_category_id) && is_numeric($download_category_id) ){
			$this->db->where('download_category_id',$download_category_id);
		}
		//搜尋
		if( !empty($city) && $city != '*'){
			$this->db->like('local' ,$city);
		}
		$this->db->order_by('sort_order' ,'DESC');
		$this->db->where('status' ,1);
		$data['query'] = $this->db->get('download')->result_array();
				
		//取得總數
		if( isset($download_category_id) && is_numeric($download_category_id) ){
			$this->db->where('download_category_id',$download_category_id);
		}
		//搜尋
		if( !empty($city) && $city != '*'){
			$this->db->like('local' ,$city);
		}
		
		$this->db->where('status' ,1);
		$data['query_total'] = $this->db->count_all_results('download');
		
		//設定分頁
		$data['pagination']  = $pg->pagination($data['query_total'],5);
		
		$data['download_category_id'] = $download_category_id;
			
		$data['city'] = $city;
		
		$data['func'] = getUserMenu('download_lists');
	    
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);
		//$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/download/lists.tpl');
	}
	
   // --------------------------------------------------------------------
    
   /**
    * 方法 : 定義臉書分享
    *
    * @access	public
    * @param
    * @return
    */
   public function set_fbShare($query){
   	 
   	return array('is_fbShare' => true ,
   			'og_title'        => $query['title'] ,
   			'og_description'  => $query['content'] ,
   			'og_image'        => $query['image']
   	);
   }
      
}


/* End of file download.tpl */
/* Location: ./application/controllers/frontend/download.tpl */