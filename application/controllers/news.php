<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 說明 : 處理[最新消息]的頁面請求
 * @controllerName news
 * @author Dio
 *
 */
class news extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->helper(array('url','cookie','ctmall','motion','database','base'));
		$this->load->library(array('form_validation','session'));
		
		//定義類別變數
		$this->err_msg = "";
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists(){
	
		$news_category_id = $this->input->get('news_category_id');
		$srh_data_name = $this->input->get('srh_data');
			
		$pg = new Dio_pagination();
		$pg_ary = $pg->get_limit($this->input->get('page'),10);

		//取得列表資料
		$this->db->limit($pg_ary['limit2'] ,$pg_ary['limit1']);
		if( !empty($srh_data_name) ){
			$this->db->like('title', $srh_data_name);
		}
		if( isset($news_category_id) && is_numeric($news_category_id) ){
			$this->db->where('news_category_id',$news_category_id);
		}
		
		//定義共用的排序規則
		//$this->db->order_by('date_show' ,'DESC');
		$this->db->order_by('cdate' ,'DESC');
		$this->db->where('status' ,1);
		$data['query'] = $this->db->get('news')->result_array();
		
		//取得總數
		if( !empty($srh_data_name) ){
			$this->db->like('title', $srh_data_name);
		}
		if( isset($news_category_id) && is_numeric($news_category_id) ){
			$this->db->where('news_category_id',$news_category_id);
		}
		$this->db->where('status' ,1);
		$data['query_total'] = $this->db->count_all_results('news');
		
		//設定分頁
		$data['pagination']  = $pg->pagination($data['query_total'],5);
		$data['news_category_id'] = $news_category_id;			
		$data['func'] = getUserMenu('news_lists') ;
		
	    //消息分類
	    $this->db->where('news_category_id' ,$news_category_id);
        $data['query_category'] = $this->db->get('news_category')->row_array();	   

        //META 設定
        $data['query_meta'] = seoMeta('category' ,$data['query_category']);
        $data['query_fb']   = fb_like($data['query_category']);
        
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);
		//$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/news/lists.tpl');
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢視
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function view(){
	
		$news_category_id = $this->input->get('news_category_id');
		$new_id = $this->input->get('news_id');
		
		$data['ctr']        = $this->ctr($new_id);		
		$data['query']      = $this->get_news_view($new_id);
		
		$data['news_category_id'] = $news_category_id;
		
		//META 設定
		$data['query_meta'] = seoMeta('content' ,$data['query']);
		$data['query_fb']   = fb_like($data['query']);
		
		$data['func']       = getUserMenu('news_view') ;
		
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);
		//$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/news/view.tpl');
	}	

	// --------------------------------------------------------------------
	 
	/**
	 * 方法 : 點擊率累加
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function ctr($news_id){
		 
		$sql = "UPDATE `news` SET `ctr_unlimited` = ctr_unlimited + 1 ,
   			                   `ctr_month` = ctr_month + 1
	 			  WHERE `news`.`news_id` = ".$news_id;
		 
		$this->db->query($sql);
	}
		
   // --------------------------------------------------------------------
    
   /**
    * 方法 : 取得最新消息/檢視
    *
    * @access	public
    * @param
    * @return
    */
   public function get_news_view($news_id){
   
   	  $this->db->where('status' ,1);
   	  $this->db->where('news_id' ,$news_id);
   	  
   	  return $this->db->get('news')->row_array();
   }
   
   // --------------------------------------------------------------------
   
   /**
    * 方法 : 定義臉書分享
    *
    * @access	public
    * @param
    * @return
    */
   public function set_fbShare($query){
   
     	return array('is_fbShare' => true ,
   			'og_title'        => $query['title'] ,
   			'og_description'  => $query['content'] ,
   			'og_image'        => $query['image']
   	    );
   }
    
   
}


/* End of file news.tpl */
/* Location: ./application/controllers/frontend/news.tpl */