<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 說明 : 處理[得獎記錄]的頁面請求
 * @controllerName awards
 * @author Dio
 *
 */
class awards extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->helper(array('url','cookie','ctmall','motion','database','base'));
		$this->load->library(array('form_validation','session'));
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists(){
		
		$srh_data_name = $this->input->get('srh_data');
			
		$pg = new Dio_pagination();
		$pg_ary = $pg->get_limit($this->input->get('page'),10);

		//取得列表資料
		$this->db->limit($pg_ary['limit2'] ,$pg_ary['limit1']);
		if( !empty($srh_data_name) ){
			$this->db->like('title', $srh_data_name);
		}
		
		$this->db->order_by('cdate' ,'DESC');
		$this->db->where('status' ,1);
		$data['query'] = $this->db->get('awards')->result_array();
		
		//取得總數
		if( !empty($srh_data_name) ){
			$this->db->like('title', $srh_data_name);
		}
		$this->db->where('status' ,1);
		$data['query_total'] = $this->db->count_all_results('awards');
		
		//設定分頁
		$data['pagination']  = $pg->pagination($data['query_total'],5);
		$data['func']        = getUserMenu('awards_lists') ;
		
        //META 設定
        $data['query_meta']  = seoMeta('content' ,$data['query']);
        $data['query_fb']    = fb_like($data['query']);
        
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);
		//$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/awards/lists.tpl');
	}
	 
   // --------------------------------------------------------------------
   
   /**
    * 方法 : 定義臉書分享
    *
    * @access	public
    * @param
    * @return
    */
   public function set_fbShare($query){
   
     	return array('is_fbShare' => true ,
   			'og_title'        => $query['title'] ,
   			'og_description'  => $query['content'] ,
   			'og_image'        => $query['image']
   	    );
   }
    
   
}


/* End of file awards.tpl */
/* Location: ./application/controllers/frontend/awards.tpl */