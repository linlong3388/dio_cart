<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 說明 : 處理[食在安心]的頁面請求
 * @controllerName certification
 * @author Dio
 *
 */
class certification extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->helper(array('url','cookie','ctmall','motion','database','base'));
		$this->load->library(array('form_validation','session'));
		
	    //載入xml檔，並轉儲為變數
	    $this->xml = simplexml_load_file('application/models/xml/certification_layout.xml');
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢視
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function view(){
		
		$data['xml']        = $this->xml;
				
		//META 設定
		$data['query'] = $this->getImageList();
		
		$meta['meta_title']       = (string)$data['xml']->meta->title;
		$meta['meta_description'] = (string)$data['xml']->meta->description;
		$meta['meta_keyword']     = (string)$data['xml']->meta->keyword;
		
		$data['query_meta'] = seoMeta('content' ,$meta);
		$data['query_fb']   = fb_like($data['xml']);
		
		$data['func']       = getUserMenu('certification_view') ;
		
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);
		//$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/certification/view.tpl');
	}	
	 
   // --------------------------------------------------------------------
   
   /**
    * 方法 : 定義臉書分享
    *
    * @access	public
    * @param
    * @return
    */
   public function set_fbShare($query){
   
     	return array('is_fbShare' => true ,
   			'og_title'        => $query['title'] ,
   			'og_description'  => $query['content'] ,
   			'og_image'        => $query['image']
   	    );
   }
   
   // --------------------------------------------------------------------
   
   /**
    * 方法 : 取得圖片列表
    *
    * @access	public
    * @param
    * @return
    */
   public function getImageList(){
   	 
   	   $this->db->where('func' ,'certification_img');
   	   $this->db->where('status' ,1);
   	   $this->db->order_by('sort_order' ,'DESC');
   	
   	   return $this->db->get('common_picture')->result_array();
   }
   
}


/* End of file certification.tpl */
/* Location: ./application/controllers/frontend/certification.tpl */