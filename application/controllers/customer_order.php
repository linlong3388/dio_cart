<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 前台(會員中心/訂單)管理首頁
 * @controllerName customer_order
 * @author Dio
 *
 */
class customer_order extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){

		parent::__construct();

		//載入模組		
		$this->load->helper(array('form','url','cookie','ctmall','database','base','motion','dio_transfer'));
		$this->load->library(array('form_validation','session','Dio_order'));
		
		$this->dio_order = new Dio_order();
		
		//登入驗證
		if(!IsLoginCustomer()){
			redirect('sign_in/login');
		}         

		/*********************************
		 /* 設置幣別
		 *********************************/
		if( empty($_SESSION['motion_currency']) ){
			$_SESSION['motion_currency'] = 'twd';
		}
		
		$this->load->model("frontend/order_model","order");
		
		if($this->input->get('page')){ //頁碼
			$this->page = $this->input->get('page');
		}else{
			$this->page = 1;
		}
		
		$this->srh_page_per = 20; //每頁筆數
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists(){
		
		//分頁
		$pg        = new dio_pagination();
		$srh_data  = $pg->get_limit($this->page ,1000);
		$srh_data['srh_customer_id'] = $_SESSION['customer_info']['customer_id'];
		
		$data['query']       = $this->order->shr_order_list($srh_data);
		$data['query_total'] = $this->order->shr_order_list_total($srh_data);  
		$data['query_favorites'] = $this->favorites();
		
		//設定分頁
		$data['pagination']  = $pg->pagination($data['query_total'],10);

		$data['func'] = 'customer_order_lists';
		
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);		
		$this->load->view('frontend/customer/order/lists.tpl');
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 明細
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function view(){
		
		$order_id = $this->input->get('order_id');
		
		//驗證會員所屬資料
		$isMyOrder = $this->dio_order->isMyOrder($order_id ,$_SESSION['customer_info']['customer_id']);
		if( !$isMyOrder ) {
			 show_404();
			 exit;
		}
		
		$data['query'] = $this->dio_order->getOrderInfo($order_id);
		
		$data['func'] = 'customer_order_view';
		
		//檢視view
		$this->load->view('frontend/common/header.tpl' ,$data);		
		$this->load->view('frontend/customer/order/view.tpl');
		
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 退貨
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function returns(){
		
		$this->form_validation->set_rules('order_detail_id','*訂單明細編號','trim|required');
		$this->form_validation->set_rules('reason_id','*申請原因','callback_check_reason');
		$this->form_validation->set_rules('name','*姓名','trim|required');
		$this->form_validation->set_rules('mobile','*手機','trim');
		$this->form_validation->set_rules('phone','*電話','trim|required');
		$this->form_validation->set_rules('email','*Email','trim|required');
		$this->form_validation->set_rules('local','*區碼','trim|required');
		$this->form_validation->set_rules('address','*地址','trim|required');
		$this->form_validation->set_rules('entity','*數量','callback_check_entity['.$this->input->post('order_detail_id').']');
		$this->form_validation->set_rules('reason','*退(換)貨原因','trim|required');
		
		if ($this->form_validation->run() == TRUE){
			$entity      = $this->input->post('entity');
			$query_valid = $this->is_info_valid($this->input->post('order_detail_id'));
			
			$data = array(
					'order_detail_id' => $this->input->post('order_detail_id'),
					'customer_id'     => $_SESSION['customer_info']['customer_id'],
					'name'            => $this->input->post('name'),
					//'mobile'          => $this->input->post('mobile'),
					'phone'           => $this->input->post('phone'),
					'email'           => $this->input->post('email'),
					'local'           => $this->input->post('local'),
					'address'         => $this->input->post('address'),
					'entity'          => $this->input->post('entity'),
					'price'           => $query_valid['price'],
					'total'           => ($entity*$query_valid['price']),
					'reason_id'       => $this->input->post('reason_id'),
					'reason'          => $this->input->post('reason'),
					'cdate'           => date('Y-m-d H:i:s')
			);
			
			$this->db->insert('return',$data);
			
			$this->session->set_flashdata('msg' ,DIO_MSG_SUCCESS_INSERT);
			
			redirect('customer_return/lists?id='.uniqid().'&page=1');
		
		}else {
			
			$data['query'] = $this->order->shr_order_detail_info($_SESSION['customer_info']['customer_id'] ,$this->input->get('order_detail_id'));
					
		    //驗證會員所屬資料
		    if(empty($data['query'])) show_404();
						
		    $data['func']     = getUserMenu('customer_order_returns');

		    $data['order_id'] = $this->input->get('order_id');
 		    
			//檢視view
			$this->load->view('frontend/common/header.tpl' ,$data);
			$this->load->view('frontend/customer/order/returns.tpl');
		}
			
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢查 / 數量
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function check_entity($entity ,$order_detail_id)
	{
		if( empty($entity) )
		{
			$this->form_validation->set_message('check_entity', '*退貨數量 不可空白!');
			return FALSE;
		}
	
		if( !IsReturnEntity($order_detail_id ,$entity) )
		{
			$this->form_validation->set_message('check_entity', '*退貨數量不可大於訂購量!');
			return FALSE;
		}	
	
		return TRUE;
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 狀態
	 * 說明 :
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function status(){

		$this->db->where('order_id' , $this->input->get('order_id'));
		$this->db->update('order' ,array('status' => 3));

		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
		
		redirect('customer_order/view?order_id='.$this->input->get('order_id'));
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 退貨資料驗證
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function is_info_valid($order_detail_id){
	
		$sql = "SELECT price ,entity ,(price*entity) as total
		FROM order_detail WHERE order_detail_id = ".$order_detail_id;
	
		return $this->db->query($sql)->row_array();
   }


   // --------------------------------------------------------------------
   
   /**
    * 方法 : 關注商品
    *
    * @access	public
    * @param
    * @return
    */
   public function favorites(){
   	
   	  $this->db->where('customer_id' ,$_SESSION['customer_info']['customer_id']);
   	  $this->db->order_by('customer_favorites_id' ,'DESC');
   	  $this->db->join('product', 'product.product_id = customer_favorites.product_id', 'left');
   	  return  $this->db->get('customer_favorites')->result_array();
   }
   
   // --------------------------------------------------------------------
   
   /**
    * 方法 : 檢查 / 申請原因
    *
    * @access	public
    * @param
    * @return
    */
   public function check_reason($reason_id)
   {
   
      if( !is_numeric( $reason_id) )
   	  {
   		 $this->form_validation->set_message('check_reason', '*申請原因  必需選取');
   		 return FALSE;
   	  }
   
   	  return TRUE;
   }

}


/* End of file customer.tpl */
/* Location: ./application/controllers/customer_order  */