<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 控制器 - 處理[結帳流程]的控制器( for 菊芬版)
 *           僅處理金流前半段的拋轉，後半段流程由"checkout_return"處理
 * 
 * 
 * @controllerName Checkout
 * @author Dio
 *
 */
class Checkout extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){

		parent::__construct();

		//載入模組
		$this->load->helper(array('form','url','cookie','database','base','is_valid','dio_format'));
		$this->load->library(array('form_validation','session','promo/VIP','Dio_order'));
		
		$this->load->model("frontend/order_model" ,"order");
		$this->load->model("frontend/checkout_model" ,"checkout");
		$this->load->model("frontend/checkout_info_model" ,"checkout_info");
		$this->load->model("frontend/checkout_calc_model" ,"checkout_calc");
		$this->load->model("frontend/checkout_coupon_model" ,"checkout_coupon");
		
		$this->order = new Dio_order();
		
		//串接金流(CI無法引入抽象類別，所以需獨立引入)
		require_once(APPPATH.'libraries/cash_flow/Cash_flow.php');
		
		$this->First_Bank_Atm    = Cash_flow::getInstance('First_Bank/First_Bank_Atm');
		
		$this->Fisc_Credit       = Cash_flow::getInstance('Fisc/Fisc_Credit');
		
		$this->No_Flow_Atm       = Cash_flow::getInstance('No_Flow/No_Flow_Atm');
		$this->No_Flow_Cod       = Cash_flow::getInstance('No_Flow/No_Flow_Cod');
		
		$this->Dio_Allpay_Atm    = Cash_flow::getInstance('Allpay/Dio_Allpay_Atm');
		$this->Dio_Allpay_Credit = Cash_flow::getInstance('Allpay/Dio_Allpay_Credit');
		
		$this->Ecpg_Credit       = Cash_flow::getInstance('Ecpg/Ecpg_Credit');
		
		$this->Chb_Atm_Uni       = Cash_flow::getInstance('Chb/Chb_Atm_Uni');
	}


// --------------------------------------------------------------------
//
// 基本購物流程
//
// --------------------------------------------------------------------	

	/**
	 * 方法 : 步驟0 / 前置處理
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function step0(){
        
		//重設購物車
		$this->cart_chenyunpaochuan->resetCartData();
		
		//導向
		redirect('checkout/step1');
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 步驟1 / 檢視訂單
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function step1(){
			
		$data['func'] = 'checkout_step1';
		
		$this->load->view('frontend/common/header.tpl',$data);
	    $this->load->view('frontend/checkout/step1.tpl');
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 步驟2 / 填寫購物資料
	 * 
	 * @access	public
	 * @param
	 * @return
	 */
	public function step2(){
		
		if(!IsLoginCustomer()){
			redirect("sign_in/login");
		}

		if( empty($_SESSION['my_order']) ){
			redirect("checkout/step0");
		}
		
		//資料驗證
		$this->form_validation->set_rules('order_name','*訂購人/姓名','trim|required');
		$this->form_validation->set_rules('order_phone','*訂購人/電話','trim|required');
		$this->form_validation->set_rules('order_email','*訂購人/Email','trim|required');
		$this->form_validation->set_rules('order_local','*訂購人/區碼','trim');
		$this->form_validation->set_rules('order_address','*訂購人/地址','trim');
		
		$this->form_validation->set_rules('pay_method','*付款方式','trim|required');
		$this->form_validation->set_rules('trans_method','*配送方式','trim|required');
		
		$this->form_validation->set_rules('code_use','*折價券','callback_is_coupon');
		
		$this->form_validation->set_rules('store_sno','*超商店號','trim');
		$this->form_validation->set_rules('store_name','*超商名稱','trim');
		$this->form_validation->set_rules('store_addr','*超商地址','trim');
		
		$this->form_validation->set_rules('the_same_order','*同訂購人','trim');
		
		$trans_method = $this->input->post('trans_method');
		
		if($trans_method == 0){ //宅配到府
		   //$this->form_validation->set_rules('is_addr_default','*預設地址','trim');
		   $this->form_validation->set_rules('reci_name','*收件人/姓名','trim|required');
		   $this->form_validation->set_rules('reci_phone','*收件人/電話','trim|required');
		   $this->form_validation->set_rules('reci_email','*收件人/Email','trim');
		   $this->form_validation->set_rules('reci_zip','*收件人/配送地區','trim|required');
		   $this->form_validation->set_rules('reci_local','*收件人/區碼','trim|required');
		   $this->form_validation->set_rules('reci_address','*收件人/地址','trim|required');
		}else{
			//$this->form_validation->set_rules('is_addr_default','*預設地址','trim');
			$this->form_validation->set_rules('reci_name','*收件人/姓名','trim');
			$this->form_validation->set_rules('reci_phone','*收件人/電話','trim');
			$this->form_validation->set_rules('reci_email','*收件人/Email','trim');
			$this->form_validation->set_rules('reci_zip','*收件人/配送地區','trim');
			$this->form_validation->set_rules('reci_local','*收件人/區碼','trim');
			$this->form_validation->set_rules('reci_address','*收件人/地址','trim');
		}
		
		
		$this->form_validation->set_rules('ticket_type','*發票類型','trim');
				
		$ticket_type = $this->input->post('ticket_type');
		if($ticket_type > 0){
		     $this->form_validation->set_rules('ticket3_title','*公司抬頭','trim|required');
		     $this->form_validation->set_rules('ticket3_unified','*統一編號','trim|required');		
		}else{
			$this->form_validation->set_rules('ticket3_title','*公司抬頭','trim');
			$this->form_validation->set_rules('ticket3_unified','*統一編號','trim');
		}
		
		$this->form_validation->set_rules('ticket_addr_id','*發票寄送地址','trim');
		
		//$this->form_validation->set_rules('zip','*配送地區','trim|required');
		$this->form_validation->set_rules('date_arrival','*指定到貨日','trim');
		$this->form_validation->set_rules('time_arrival','*指定到貨時段','trim');
		$this->form_validation->set_rules('memo','*備註','trim');
		
		if ($this->form_validation->run() == TRUE){
			
			$data = array(
      	              'customer_id' =>  $_SESSION['customer_info']['customer_id'] ,   
					
					       'o_name' =>  $this->input->post('order_name') ,
					      'o_phone' =>  $this->input->post('order_phone') ,
					      'o_email' =>  $this->input->post('order_email') ,
					      'o_local' =>  $this->input->post('order_local') ,
					    'o_address' =>  $this->input->post('order_address') ,
						
					   'pay_method' =>  $this->input->post('pay_method') ,
					 'trans_method' =>  $this->input->post('trans_method') ,
					
				  //'is_addr_default' =>  $this->input->post('is_addr_default') ,
			             'code_use' =>  $this->input->post('code_use') ,
					
					    'store_sno' =>  $this->input->post('store_sno') ,
					   'store_name' =>  $this->input->post('store_name') ,
					   'store_addr' =>  $this->input->post('store_addr') ,
					
				   'the_same_order' =>  $this->input->post('the_same_order') ,
					
					         'name' =>  $this->input->post('reci_name') ,
					        'phone' =>  $this->input->post('reci_phone') ,
					        'email' =>  $this->input->post('reci_email') ,
					     'reci_zip' =>  $this->input->post('reci_zip') ,
					        'local' =>  $this->input->post('reci_local') ,
					      'address' =>  $this->input->post('reci_address') ,
						
					  'ticket_type' =>  $this->input->post('ticket_type') ,
					'ticket3_title' =>  $this->input->post('ticket3_title') ,
				  'ticket3_unified' =>  $this->input->post('ticket3_unified') ,
					
					        //'zip' =>  $this->input->post('zip') ,
				   'ticket_addr_id' =>  $this->input->post('ticket_addr_id') ,
					
					 'time_arrival' =>  $this->input->post('time_arrival') ,
					 'date_arrival' =>  $this->input->post('date_arrival') ,
					         'memo' =>  $this->input->post('memo') ,
			                'cdate' =>  date('Y-m-d H:i:s') ,
			);
			
			//取得[發票寄送地址]資料
			$data['ticket_addr'] = $this->checkout_info->getTicketAddr( $data );
					
			//建立訂單資料
			$this->cls_cart->add_order($data);
			
			redirect('checkout/step3');

		} else { 
		  
			//驗證庫存量
			$this->checkout_info->isStock();
			
			//購物車總計、運費、各種折扣(寫入SESSION，以便跨頁)
			$this->checkout_calc->calc_entity($this->cls_cart->count_all_entity());
			
			$data['shipping_fee'] = empty($_SESSION['my_order']['promo']['shipping_fee']['total']) ? $_SESSION['sys_info']['shipping_fee'] : $_SESSION['my_order']['promo']['shipping_fee']['total'] ;
			
			//取得基本資料
			$data['customer_addr_list']    = $this->checkout_info->getCustomerAddr($_SESSION['customer_info']['customer_id']);
			$data['customer_store_list']   = $this->checkout_info->getStoreInfo($_SESSION['customer_info']['customer_id']);
			
			$data['func'] = getUserMenu('checkout_step2');
		    
			//錯誤訊息燈箱
			$data['lightbox_is_ErrMsg']  = (validation_errors() != false) ? 1 : 0 ;
			$data['lightbox_validation_errors'] = validation_errors();
			
			//檢視view
			$this->load->view('frontend/common/header.tpl' ,$data);
			$this->load->view('frontend/checkout/step2.tpl');
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 結帳流程3 / 確認訂單
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function step3(){

		if(!IsLoginCustomer()){
			redirect("sign_in/login");
		}
		
		if( empty($_SESSION['my_order']) ){
			redirect("checkout/step0");
		}
		
		//購物車總計、運費、各種折扣(寫入SESSION，以便跨頁)
		$this->checkout_calc->getFeeCod( $_SESSION['my_order']['info']['pay_method'] );
		$this->checkout_calc->getFee($this->cls_cart->select_all());
		$this->checkout_calc->getCoupon();
		$this->checkout_calc->getTotal();
		
		$data['func'] = getUserMenu('checkout_step3'); 
		
		$this->load->view('frontend/common/header.tpl' ,$data);
		$this->load->view('frontend/checkout/step3.tpl');
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 結帳流程4 / 送出訂單(金流串接)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function step4(){
		
		if(!IsLoginCustomer()){
			redirect("sign_in/login");
		}	

		if( empty($_SESSION['my_order']) ){
			redirect("checkout/step0");
		}
		
		//建立訂單資料
		$_SESSION['my_order']['order_id'] = $this->checkout->addOrder($_SESSION['my_order']);
		
		//建立運費資料
		$this->checkout->addFee($_SESSION['my_order']);
		
		//建立折價券資料
		$this->checkout->addCoupon($_SESSION['my_order']);

		//建立貨到付款/手續費資料
		$this->checkout->addFeeCod($_SESSION['my_order']);
		
		//判斷哪一種金流
		switch ( $_SESSION['my_order']['info']['pay_method'] ) {
				
			case 0: //財金信用卡for彰銀
				   $this->fisc_Credit();
				break;
				
			case 1: //貨到付款
				   $this->cod();
				break;
				
			case 2:  //ATM 轉帳
				//$this->atm();
				$this->Chb_Atm_Uni();
				break;
		}
		
	}
	

	
// --------------------------------------------------------------------
//
// 資料驗證
//
// --------------------------------------------------------------------	

	/**
	 * 方法 : 驗證折價券
	 *           
	 * @access	public
	 * @param
	 * @return
	 */
	 public function is_coupon($code_use)
	 {
	 	if( !empty($code_use) )
	 	{	
	 	   //依使用者填寫的折價券，重設 SESSION 值	
	 	   $this->checkout_coupon->reSetPromoCoupon( $code_use );
	 		
	 	   if( !$this->checkout_coupon->isCouponCode($code_use) )
	 	   {
	 			$this->form_validation->set_message('is_coupon', '*抱歉，無此折價券編號!');
	 			return FALSE;
	 	   }
	 	   
	 	   if( !$this->checkout_coupon->isCouponUseTotal() )
	 	   {
	 	     	$this->form_validation->set_message('is_coupon', '*抱歉，此折價券已達發放上限!');
	 	   	    return FALSE;
	 	   }	 	   
	 	   
	 	   if( !$this->checkout_coupon->isCouponTotal() )
	       {
	            $this->form_validation->set_message('is_coupon', '*抱歉，購物總額需滿   '.DIO_CURRENCY.addCommas($_SESSION['sys_info']['promo']['coupon']['total']).' 才可使用折價券!');
	            return FALSE;
	       }
	       
	       if( !$this->checkout_coupon->isCouponUseCategoryAndProduct() )
	       {
	       	    $this->form_validation->set_message('is_coupon', '*抱歉，折價券為限定商品分類或商品使用!');
	       	    return FALSE;
	       }
	       
	       if( !$this->checkout_coupon->isCouponUseCountCustomer() )
	       {
	       	    $this->form_validation->set_message('is_coupon', '*抱歉，此折價券您已使用超過上限 '.$_SESSION['sys_info']['promo']['coupon']['use_count_customer'].' 次!');
	       	    return false;
	       }

	 	}
	    	 	
	    return TRUE;
	 }
	 
	
// --------------------------------------------------------------------
//
// 客製禮盒購物流程
//
// --------------------------------------------------------------------	
	
	/**
	 * 方法 : 步驟1-1 / 選擇禮盒
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function step1_comb_1(){
		
		$combUnit   = $this->input->post('combUnit');
		$product_id = $this->input->post('product_id');
		$entity     = $this->input->post('entity');
	
		if( !empty($combUnit) ){
		   //驗證組合	
           $isCombOk = $this->cart_chenyunpaochuan->isProductTypeId1BoxCombOk( $combUnit );

           if($isCombOk){
           	  //更新購物車
           	  if( $this->cls_cart->editByProductId($product_id ,$entity) ){
           	
           		 //優惠券驗證
           		 //$this->discount_coupon_check();
           	
           		 //重算運費
           		 $this->checkout_calc->getFee($this->cls_cart->select_all());
           		 $this->checkout_calc->getTotal();
           	   }
           	   
               redirect('checkout/step0');
		   }else{
		   	   //dioRedirect('checkout/step1_comb_2' ,'*禮盒尚未組合完成 '.$combUnit.'(入)，請確認!');
		       dioRedirect('checkout/step1_comb_2' ,'*請確認禮盒總盒數是否為 四組!');
           }
		
		}else{
		
		   $data['query'] = $this->checkout_info->getProductBoxList();
		
		   $data['func'] = getUserMenu('checkout_step1_comb_1');
				
		   $this->load->view('frontend/common/header.tpl',$data);
		   $this->load->view('frontend/checkout/step1_comb_1.tpl');
		}	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 步驟1-2 / 預覽組合
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function step1_comb_2(){

		if( empty($_SESSION['my_order']['productTypeId_1']) )
			redirect('checkout/step1_comb_1');
		
		$product_id = $this->input->get_post('product_id');
		$product_id = empty($product_id) ? $_SESSION['my_order']['productTypeId_1'] : $product_id;
		
		$data['productInfoTypeId1'] = $this->checkout_info->getProductInfoByTypeId1( $product_id );
		
		$data['func'] = getUserMenu('checkout_step1_comb_2');
		
		$product_comb              = $this->checkout_info->getProductBoxCombList($data['productInfoTypeId1']['unit']);
		
		$data['curtCartData']      = $this->cls_cart->getCartInfoByProductID( $product_id );
		$data['cart_image_comb']   = $this->cart_chenyunpaochuan->view_getCartProductTypeId1Img( $product_id );
		$cart_product_comb         = $this->cart_chenyunpaochuan->view_getCartProductTypeId1Product($product_id);
		$data['product_comb']      = $this->cart_chenyunpaochuan->view_getProductTypeId1ListIsCheck($data['curtCartData'] ,$product_comb);
		
		//[上一步]連結導向
		$data['prev']         = $this->checkout_info->step1Comb2Redirect();
		
		//驗證總數是否可以啟用
		$data['isCombEntity'] = $data['curtCartData']['price_box'] < $data['curtCartData']['price'] ? '1' : '0';     
		                               
		//商品編號
		$data['product_id'] = $product_id;
		
		$this->load->view('frontend/common/header.tpl',$data);
		$this->load->view('frontend/checkout/step1_comb_2.tpl');
	}
	
	// --------------------------------------------------------------------	
	
	/**
	 * 方法 : 步驟2 / 選擇訂購方式 (若為門市訂購)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function step2_comb(){
		
		//資料驗證
		$this->form_validation->set_rules('slt_barcode','*選擇訂購方式','trim|required');
		
		if ($this->form_validation->run() == TRUE){
			
			$slt_barcode = $this->input->post('slt_barcode');
			
			if($slt_barcode == 0){
				redirect('checkout/step3_comb');
			}else{
				redirect('checkout/step2');
			}
			
		}else{
			
			$data['func'] = getUserMenu('checkout_step2_comb');
			
			$this->load->view('frontend/common/header.tpl',$data);
			$this->load->view('frontend/checkout/step2_comb.tpl');
		}
		
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 步驟3 / 條碼結帳
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function step3_comb(){
		
		$data = array();
		
	    $i = 0;
	    
		foreach ($_SESSION['my_order']['cart'] as $row){
			
			$data[$i]['product_id'] = $row['product_id'];
			$data[$i]['barcode']    = $row['barcode'];
			$data[$i]['name']       = $row['name'];
			$data[$i]['price']      = $row['price_original'];
			$data[$i]['entity']     = $row['entity'];
			
			if( !empty($row['box']) ){

				foreach ($row['box'] as $row2){
					$i++;

					$data[$i]['product_id'] = $row2['product_id'];
					$data[$i]['barcode']    = $row2['barcode'];
					$data[$i]['name']       = $row2['name'];
					$data[$i]['price']      = $row2['price'];
					$data[$i]['entity']     = $row2['entity'];
				}
			}
			
			$i++;
		}

		$data['query'] = $data;
		
		$data['func']  = getUserMenu('checkout_step3_comb');
	
		$this->load->view('frontend/common/header.tpl',$data);
		$this->load->view('frontend/checkout/step3_comb.tpl');
	}
	

// --------------------------------------------------------------------
//
// 金流串接 
//	
// --------------------------------------------------------------------
	
	/**
	 * 方法 : 彰銀ATM 萬用帳號
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function Chb_Atm_Uni(){
	
		$data = $this->order->getOrderInfo($_SESSION['my_order']['order_id']);
	
		$this->Chb_Atm_Uni->setParams($data);
		$this->Chb_Atm_Uni->dataTransport();
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : ECPG 聯合信用卡
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function ecpg_credit(){
	
		$data = $this->order->getOrderInfo($_SESSION['my_order']['order_id']);
	
		$this->Ecpg_Credit->setParams($data);
		$this->Ecpg_Credit->dataTransport();
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 財金信用卡
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function fisc_credit(){
	
		$data = $this->order->getOrderInfo($_SESSION['my_order']['order_id']);
		
		$this->Fisc_Credit->setParams($data);
		$this->Fisc_Credit->dataTransport();
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : ATM 轉帳
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function atm(){
	
		$data = $this->order->getOrderInfo($_SESSION['my_order']['order_id']);
	
		$this->No_Flow_Atm->setParams($data);
		$this->No_Flow_Atm->dataTransport();
	}	
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 貨到付款
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function cod(){
		
		$data = $this->order->getOrderInfo($_SESSION['my_order']['order_id']);
	
		$this->No_Flow_Cod->setParams($data);
		$this->No_Flow_Cod->dataTransport();
	}
	
}


/* End of file checkout.tpl */
/* Location: ./application/controllers/checkout */