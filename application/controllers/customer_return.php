<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 前台(會員中心/退貨)管理首頁
 * @controllerName customer_return
 * @author Dio
 *
 */
class customer_return extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){

		parent::__construct();

		//載入模組
		$this->load->helper(array('form','url','cookie','ctmall','database','base','motion'));
		$this->load->library(array('form_validation','session'));
		
		//登入驗證
		if(!IsLoginCustomer()){
			redirect('sign_in/login');
		}         
		
		/*********************************
		 /* 設置幣別
		 *********************************/
		if( empty($_SESSION['motion_currency']) ){
			$_SESSION['motion_currency'] = 'twd';
		}

		$this->load->model("frontend/return_model" ,"return");
		
		if($this->input->get('page')){ //頁碼
			$this->page = $this->input->get('page');
		}else{
			$this->page = 1;
		}
		
		$this->srh_page_per = 20; //每頁筆數
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists(){
		
		//分頁
		$pg        = new dio_pagination();
		$srh_data  = $pg->get_limit($this->page ,8);
		$srh_data['srh_customer_id'] = $_SESSION['customer_info']['customer_id'];
		
		$data['query']       = $this->return->shr_list($srh_data);
		$data['query_total'] = $this->return->shr_list_total($srh_data);
		
		//設定分頁
		$data['pagination']  = $pg->pagination($data['query_total'],10);
		
		$data['func'] = 'customer_return_lists';
					
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);
		$this->load->view('frontend/customer/return/lists.tpl');		
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 明細
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function view(){
		
		$data['query'] = $this->return->shr_detail($_SESSION['customer_info']['customer_id'] ,$this->input->get('return_id'));
		
		//驗證會員所屬資料
		if(empty($data['query'])) show_404();
		
		$data['func'] = 'customer_return_view';
						
		//檢視view
		$this->load->view('frontend/common/header.tpl' ,$data);
		$this->load->view('frontend/customer/return/view.tpl');
	}

}


/* End of file customer.tpl */
/* Location: ./application/controllers/customer_return  */