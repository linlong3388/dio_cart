<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 說明 : 處理[新手上路]的頁面請求
 * @controllerName novice
 * @author Dio
 *
 */
class novice extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->library(array('session'));
		$this->load->helper(array('url','cookie','ctmall','motion','database','base'));
		
		//定義類別變數
		$this->err_msg = "";
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 主頁面
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function main(){

		$data['ad']           = $this->ad();
		$data['hot']          = $this->hot();		
			
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);
		$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/novice.tpl');
		
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 廣告
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function ad(){
		
		$this->db->where('status' ,'1');
		$this->db->order_by('ad_home_id' ,'DESC');
		$ad = $this->db->get('ad_home')->result_array();

	    foreach (get_ad_home_type_data() as $key=>$val) {
        	$data[$key] = array();
        }
        
        foreach ($ad as $row) {
        	switch ($row['type']) {
        		case 'a':
					array_push($data['a'], $row);
					break;
        		case 'b':
					array_push($data['b'], $row);
					break;
			}		
        }
                
        return $data;
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 熱銷商品
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function hot(){
		
      $this->load->model('frontend/product_model' ,'product');

	  //商品搜尋條件
	  $srh_data['srh_action_id'] = 6;
	  $srh_data['num'] = 40;

	  $data = $this->product->shr_product($srh_data);

	  return $data;
   }
	
	
}


/* End of file novice.tpl */
/* Location: ./application/controllers/frontend/novice.tpl */