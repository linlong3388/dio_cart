<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理[結帳流程/返回]的控制器
 *            一頁式結帳流程，僅處理金流後半段的接收返回，前半段流程由"checkout"處理
 * 
 * @controllerName checkout_return
 * @author Dio
 *
 */
class checkout_return extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){

		parent::__construct();

		//載入模組
		$this->load->helper(array('form','url','cookie','database','base','dio_upload','dio_format','dio_date'));
		$this->load->library(array('form_validation','session','Dio_email','Dio_order'));
		
		
		$this->load->model("frontend/order_model","order");
		$this->load->model("frontend/checkout_return_model","checkout_return");

		$this->order = new Dio_order();
		
		//串接金流(CI無法引入抽象類別，所以需獨立引入)
		require_once(APPPATH.'libraries/cash_flow/Cash_flow.php');
		
		$this->First_Bank_Atm    = Cash_flow::getInstance('First_Bank/First_Bank_Atm');
		$this->Fisc_Credit       = Cash_flow::getInstance('Fisc/Fisc_Credit');
		
		$this->No_Flow_Atm       = Cash_flow::getInstance('No_Flow/No_Flow_Atm');
		$this->No_Flow_Cod       = Cash_flow::getInstance('No_Flow/No_Flow_Cod');
		
		$this->Dio_Allpay_Atm    = Cash_flow::getInstance('Allpay/Dio_Allpay_Atm');
		$this->Dio_Allpay_Credit = Cash_flow::getInstance('Allpay/Dio_Allpay_Credit');
		
		$this->Ecpg_Credit       = Cash_flow::getInstance('Ecpg/Ecpg_Credit');
		
		$this->Dio_Ecpay_Credit  = Cash_flow::getInstance('ECPay/Dio_ECPay_Credit');
		$this->Dio_Ecpay_Cvs     = Cash_flow::getInstance('ECPay/Dio_ECPay_Cvs');
		
		//超商取貨物流(for 7-11)
		$this->Dio_ECPay_Logistics_UNIMART = Cash_flow::getInstance('ECPay/Dio_ECPay_Logistics_UNIMART');
		
		$this->Chb_Atm_Uni       = Cash_flow::getInstance('Chb/Chb_Atm_Uni');
		
		$this->Dio_Process       = Cash_flow::getInstance('Dio_Process');
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 返回確認 / 聯合信用卡中心ECPG (信用卡)
	 * 	   ( 提供給遠端金流做為返回驗證的URL )
	 *       1) 驗證回傳結果
	 *       2) 更新訂單(狀態為1),更新促銷活動(狀態為1)
	 *       3) 寄送Email
	 *       4) 客制商務邏輯(可選)
	 *       5) 將金流訊息回寫到資料庫(可選)
	 *       6) 撈取這次的訂單資料(可選)
	 *
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function ecpg_credit(){
	
		$data  = $this->input->post();
		$is_ok = $this->Ecpg_Credit->is_checkReturn($data);
	
		//成功
		if( $is_ok ){
				
			$order_show_id = $data['OrderID'];
				
			$this->Dio_Process->updateOrderStatus( $order_show_id );
			//$this->Dio_Process->BusinessLogic(array('order_id' => transOrderShowIdToOrderId($order_show_id) ));
				
			$info = $this->order->getOrderInfo($order_show_id);

			$this->Dio_Process->sendEmail($info);
			
			$msg = "";
				
			$this->data_post( getUserURL('checkout_return/success')
			               	 ,array('info' => $msg));
				
				
		//失敗
		}else{
				
			$order_show_id = $data['OrderID'];
				
			$this->Dio_Process->updateOrderStatus( $order_show_id ,0 );
				
			$msg = "錯誤代碼: " . $data["ResponseCode"]."<p>"
				   ."錯誤訊息: " . $data["ResponseMsg"]."<p>" ;
				
			$this->data_post( getUserURL('checkout_return/fail')
		             		,array('info' => $msg));

		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 財金信用卡(for 彰銀)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function fisc_Credit(){

	    $data = $this->input->post();
	
		$order_show_id = $data['lidm'];
	
		$is_ok = $this->Fisc_Credit->is_checkReturn( $data );
		
		//成功
		if( $is_ok ){
	
			$this->Dio_Process->updateOrderStatus($order_show_id);
			$this->Dio_Process->BusinessLogic(array('order_id' => transOrderShowIdToOrderId($order_show_id)
					,'code_use' => $_SESSION['my_order']['info']['code_use']
			));
	
			$info = $this->order->getOrderInfo($order_show_id);
			
			$this->Dio_Process->sendEmail($info);
			
			$msg = "";
	
			$this->data_post( getUserURL('checkout_return/success')
					,array('info' => $msg));
				
		//失敗
		}else{
				
			$this->Dio_Process->updateOrderStatus( $order_show_id ,0 );
				
			$msg =  "";  //"錯誤狀態(代碼): " . $data["status"] ." ( ".$data["errcode"] ." ) ";
				
			$this->data_post( getUserURL('checkout_return/fail')
		             		,array('info' => $msg));

		}
	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 綠界信用卡
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function ecpay_Credit(){
	
		$data = $this->input->post();
	
		$order_show_id = $data['MerchantTradeNo'];
	
		$is_ok = $this->Dio_Ecpay_Credit->is_checkReturn( $data );
	
		//成功
		if( $is_ok ){
	
			$this->Dio_Process->updateOrderStatus($order_show_id);
			$this->Dio_Process->BusinessLogic(array('order_id' => transOrderShowIdToOrderId($order_show_id)
					,'code_use' => $_SESSION['my_order']['info']['code_use']
			));
	
			$info = $this->order->getOrderInfo($order_show_id);
				
			$this->Dio_Process->updateReturnStock($info);
			$this->Dio_Process->sendEmail($info);
				
			$msg = "";
	
			$this->data_post( getUserURL('checkout_return/success')
					,array('info' => $msg));
	
			//失敗
		}else{
	
			$this->Dio_Process->updateOrderStatus( $order_show_id ,0 );
	
			$msg =  "";  //"錯誤狀態(代碼): " . $data["status"] ." ( ".$data["errcode"] ." ) ";
	
			$this->data_post( getUserURL('checkout_return/fail')
					,array('info' => $msg));
	
		}
	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 綠界超商繳費
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function ecpay_Cvs(){
	
		$data = $this->input->post();
	
		$order_show_id = $data['MerchantTradeNo'];
	
		$is_ok = $this->Dio_Ecpay_Cvs->is_checkReturn( $data );
	
		//成功
		if( $is_ok ){
	
			$this->Dio_Process->updateOrderStatus($order_show_id);
			$this->Dio_Process->BusinessLogic(array('order_id' => transOrderShowIdToOrderId($order_show_id)
					,'code_use' => $_SESSION['my_order']['info']['code_use']
			));
	
			$info = $this->order->getOrderInfo($order_show_id);
	
			$this->Dio_Process->updateReturnStock($info);
			$this->Dio_Process->sendEmail($info);
	
			$msg = "";
	
			$this->data_post( getUserURL('checkout_return/success')
					,array('info' => $msg));
	
			//失敗
		}else{
	
			$this->Dio_Process->updateOrderStatus( $order_show_id ,0 );
	
			$msg =  "";  //"錯誤狀態(代碼): " . $data["status"] ." ( ".$data["errcode"] ." ) ";
	
			$this->data_post( getUserURL('checkout_return/fail')
					,array('info' => $msg));
	
		}
	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 返回確認 / 綠界(信用卡) / 離線驗證
	 * 	  以 "OrderResultURL" 參數的即時驗證為主
	 *
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function ecpay_credit_offLine(){
	
		$data_return = $this->input->post();
	
		$is_ok = $this->Dio_Ecpay_Credit->is_checkReturn($data_return);
	
		//成功
		if( $is_ok ){
	
			echo $data_return['RtnCode'].'|OK';
			exit;
	
			//失敗
		}else{
	
			$msg = "";
	
			$this->data_post( getUserURL('checkout_return/fail')
					,array('info' => $msg));
	
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 返回確認 / 綠界(超商繳費) / 訂單成立 / 離線驗證
	 * 	  以 "OrderResultURL" 參數的即時驗證為主
	 *
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function ecpay_cvs_offLine_order_create(){
		
		$data_return = $this->input->post();
		
		$is_ok = $this->Dio_Ecpay_Cvs->is_checkReturn($data_return);
		
		//成功
		if( $is_ok ){
				
			//回應綠界Server
			echo $data_return['RtnCode'].'|OK';
				
			$info = $this->order->getOrderInfo( $data_return['MerchantTradeNo'] );
				
			$this->Dio_Process->updateOrderStatus( $data_return['MerchantTradeNo'],4 );
				
			exit;
		
			/* 離線付款，無須導向
			 $this->data_post( getUserURL('checkout_return/success')
			 ,array('info' => $msg));*/
				
			//失敗
		}else{
		
			$msg = "";
		
			$this->data_post( getUserURL('checkout_return/fail')
					,array('info' => $msg));
		
		}		
		
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 返回確認 / 綠界(超商繳費) / 繳費成功 / 離線驗證
	 * 	  以 "OrderResultURL" 參數的即時驗證為主
	 *
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function ecpay_cvs_offLine(){
	
		$data_return = $this->input->post();
		
		$is_ok = $this->Dio_Ecpay_Cvs->is_checkReturn($data_return);
		
		//成功
		if( $is_ok ){
			
			//回應綠界Server
			echo $data_return['RtnCode'].'|OK';
			
			$info = $this->order->getOrderInfo( $data_return['MerchantTradeNo'] );
			
			$this->Dio_Process->updateOrderStatus( $data_return['MerchantTradeNo'] );
			$this->Dio_Process->BusinessLogic(array('order_id' => transOrderShowIdToOrderId( $data_return['MerchantTradeNo'] )
					,'code_use' => $info['order']['master']['promo_coupon_code_use']
			));
				
			$this->Dio_Process->updateReturnStock($info);
			$this->Dio_Process->sendEmail($info);
				
			$msg = "";
			
			exit;
				
			/* 離線付款，無須導向
			 $this->data_post( getUserURL('checkout_return/success')
			 ,array('info' => $msg));*/	
			
		//失敗
		}else{
	
			$msg = "";
	
			$this->data_post( getUserURL('checkout_return/fail')
					,array('info' => $msg));
	
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 返回確認 / 綠界(超商取貨) / 物流狀態通知 / 離線驗證
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function ecpay_Logistics_unimart_status_offLine(){
		
		$data = $this->input->post();
	
		$is_ok = $this->Dio_ECPay_Logistics_UNIMART->is_checkReturn($data);
	
		//成功
		if( $is_ok ){
				
			//回應綠界Server
			echo $data['RtnCode'].'|OK';
				
			//更新物流狀態
			$this->checkout_return->updateOrderLogisticsCvs( $data );
				
			exit;
				
		//失敗
		}else{
	
			$msg = "";
	
			$this->data_post( getUserURL('checkout_return/fail')
					,array('info' => $msg));
	
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 返回確認 / 綠界(超商取貨) / 物流訂單確認
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function ecpay_Logistics_unimart(){
	
		$data = $this->input->post();
		
		$is_ok = $this->Dio_ECPay_Logistics_UNIMART->is_checkReturn( $data );
		
		//成功
		if( $is_ok ){
			
			$order_show_id = $data['MerchantTradeNo'];
			
			//取得物流資訊並回存
			$this->checkout_return->updateOrderLogisticsCvs( $data );
			
			$this->Dio_Process->updateOrderStatus($order_show_id,4);
			$this->Dio_Process->BusinessLogic(array('order_id' => transOrderShowIdToOrderId($order_show_id)
					,'code_use' => $_SESSION['my_order']['info']['code_use']
			));
	
			$info = $this->order->getOrderInfo($order_show_id);
	
			$this->Dio_Process->updateReturnStock($info);
			$this->Dio_Process->sendEmail($info);
	
			$msg = "";
	
			$this->data_post( getUserURL('checkout_return/success')
					,array('info' => $msg));
	
			//失敗
		}else{
	
			//$this->Dio_Process->updateOrderStatus( $order_show_id ,0 );
	
			$msg =  "";  //"錯誤狀態(代碼): " . $data["status"] ." ( ".$data["errcode"] ." ) ";
	
			$this->data_post( getUserURL('checkout_return/fail')
					,array('info' => $msg));
	
		}
	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : ATM轉帳(無金流)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function atm(){
	
		$order_show_id = $this->input->get('order_show_id');
	
		$data = $this->No_Flow_Atm->is_checkReturn(array('order_show_id' => $order_show_id));
	
		//成功
		if( $data ){
	
			$this->Dio_Process->updateOrderStatus($order_show_id ,4);
			$this->Dio_Process->BusinessLogic(array('order_id' => transOrderShowIdToOrderId($order_show_id)) );
			
			$info = $this->order->getOrderInfo($order_show_id);
			
			$this->Dio_Process->updateReturnStock($info);
			$this->Dio_Process->sendEmail($info);
			
			$msg = $_SESSION['sys_info']['pay_method_atm']
			.'<span id="success_total">匯款總額:'. DIO_CURRENCY.addCommas($info['order']['master']['total']) .'</span>';
	
			$this->data_post( getUserURL('checkout_return/success?pay_method=5')
					,array('info' => $msg));
		}
	
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 返回確認 / 歐付寶(ATM) 
	 * 	   ( 提供給遠端金流做為返回驗證的URL )
	 *       1) 驗證回傳結果
	 *       2) 更新訂單(狀態為1),更新促銷活動(狀態為1)
	 *       3) 寄送Email
	 *       4) 客制商務邏輯(可選)
	 *       5) 將金流訊息回寫到資料庫(可選)
	 *       6) 撈取這次的訂單資料(可選)
	 * 
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function pay_allpay_atm(){
		
		$data = $this->Dio_Allpay_Atm->is_checkReturn(array());
		
		//成功
		if( !empty($data) && $data['szRtnCode'] == 1 ){
			
			$order_show_id = $data['szMerchantTradeNo'];			
			
			$this->Dio_Process->updateOrderStatus( $order_show_id );
			$this->Dio_Process->BusinessLogic(array('order_id' => transOrderShowIdToOrderId($data['szMerchantTradeNo']) ));
			
			$info = $this->order->getOrderInfo($order_show_id);
			
			$msg = "";
			
			/*
			$this->data_post( getUserURL('checkout_return/success')
					,array('info' => $msg));
			*/			
			
			echo "{$data['szRtnCode']}|OK";
			
		//失敗	
		}else{
			
			$order_show_id = $data['szMerchantTradeNo'];
			
			$this->Dio_Process->updateOrderStatus( $order_show_id ,0 );
			
			$msg = "";
			
			/*
			$this->data_post( getUserURL('checkout_return/fail')
					,array('info' => $msg));
			*/
			
			echo "0|Fail";

		}		
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 返回確認 / 歐付寶(信用卡)
	 * 	   ( 提供給遠端金流做為返回驗證的URL )
	 *       1) 驗證回傳結果
	 *       2) 更新訂單(狀態為1),更新促銷活動(狀態為1)
	 *       3) 寄送Email
	 *       4) 客制商務邏輯(可選)
	 *       5) 將金流訊息回寫到資料庫(可選)
	 *       6) 撈取這次的訂單資料(可選)
	 *
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function pay_allpay_credit(){
	
		$data = $this->Dio_Allpay_Credit->is_checkReturn(array());
	
		//成功
		if( !empty($data) && $data['szRtnCode'] == 1 ){
				
			$order_show_id = $data['szMerchantTradeNo'];
				
			$this->Dio_Process->updateOrderStatus( $order_show_id );
			$this->Dio_Process->BusinessLogic(array('order_id' => transOrderShowIdToOrderId($data['szMerchantTradeNo']) ));
				
			$info = $this->order->getOrderInfo($order_show_id);
				
			$msg = "";
				
			$this->data_post( getUserURL('checkout_return/success')
					,array('info' => $msg));
			
			echo "{$data['szRtnCode']}|OK";
				
			//失敗
		}else{
				
			$msg = "";
				
			$this->data_post( getUserURL('checkout_return/fail')
					,array('info' => $msg));
	
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 返回確認 / 彰銀(ATM) 萬用帳號 (iPay)
	 * 	   ( 提供給遠端金流做為返回驗證的URL )
	 *       1) 驗證回傳結果
	 *       2) 更新訂單(狀態為1),更新促銷活動(狀態為1)
	 *       3) 寄送Email
	 *       4) 客制商務邏輯(可選)
	 *       5) 將金流訊息回寫到資料庫(可選)
	 *       6) 撈取這次的訂單資料(可選)
	 *
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function chb_atm(){
	
		$data = $this->input->post();
		
		$is_ok = $this->Chb_Atm_Uni->is_checkReturn( $data );
	
		//成功
		if( $is_ok ){
				
			$this->Dio_Process->updateOrderStatus( $data['order_no'] ,4);
			$this->Dio_Process->BusinessLogic(array('order_id' => transOrderShowIdToOrderId( $data['order_no'] ) ));
				
			$info = $this->order->getOrderInfo( $data['order_no'] );

			$this->Dio_Process->sendEmail($info);
			
			$msg = $this->Chb_Atm_Uni->showAtmPaymentInfo( $data );
			
			$this->data_post( getUserURL('checkout_return/success')
			  	             ,array( 'info' => $msg['info']
			  	             		 ,'url' => $msg['url'] 
			  	             		 ,'order_show_id' => $data['order_no']
			  	             ));
				
		//失敗
		}else{
			
			$this->Dio_Process->updateOrderStatus( $data['order_no'] ,0 );
				
			$msg = "";

			$this->data_post( getUserURL('checkout_return/fail')
			               	  ,array('info' => $msg));
	
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 返回確認 / 彰銀(ATM) 萬用帳號
	 *         付款通知
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function chb_atm_order_notice(){
		
		$data = $this->input->post();
		
		if( $this->Chb_Atm_Uni->isAtmPaymentQuery( $data['order_no'] ) ){
			$this->Dio_Process->updateOrderStatus( $data['order_no'] ,1);
		}	
		
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 貨到付款(無金流)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function cod(){
		
		$order_show_id = $this->input->get('order_show_id');
		
		$data = $this->No_Flow_Cod->is_checkReturn(array('order_show_id' => $order_show_id));
	
		//成功
		if( $data ){
			
			$this->Dio_Process->updateOrderStatus($order_show_id);
			$this->Dio_Process->BusinessLogic(array('order_id' => transOrderShowIdToOrderId($order_show_id)));

			$info = $this->order->getOrderInfo($order_show_id);

			$this->Dio_Process->updateReturnStock($info);
			$this->Dio_Process->sendEmail($info);
			
			$msg = "";
			
			$this->data_post( getUserURL('checkout_return/success')
					          ,array('info' => $msg));
		}
	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 將資料返回結果頁面
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function data_post($url ,$param){

		$html_input = "";
		
		foreach ($param as $key => $val) {
			$html_input .= "<input type='hidden' name='" . $key . "' value='" . $val . "'><BR>";
		}
			
		$str = <<<EOD
                 <form id="form" style="display:none" action="{$url}" method="post">
                      $html_input
                    <input type="submit"/>
                 </form>
		
                 <script>
                    document.getElementById("form").submit();
                 </script>
EOD;
		
		echo $str;
	}
	

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 成功
	 *       依拋轉過來的金流資訊，值接顯示資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function success(){

		$data['func'] = getUserMenu('success');
		
		//接收 get/post資料
		$data['query']     = $this->input->get_post('info');
		$data['query_url'] = $this->input->get_post('url');
		
		//回寫ATM記錄
		if( !empty($data['query']) ){
			
			$this->db->where('order_show_id' ,$this->input->get_post('order_show_id'));
			$this->db->where('status' ,4);
			
			$this->db->update('order' ,array(  'pay_atm_link' => $data['query_url'] ,
					                     'pay_method_content' => $data['query'] ));
		}
		
		$this->load->view('frontend/common/header.tpl' ,$data);
		$this->load->view('frontend/checkout/success.tpl');
	
		$this->Dio_Process->cleanSession();
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 失敗
	 *       依拋轉過來的金流資訊，直接顯示資料
	 *       
	 * @access	public
	 * @param
	 * @return
	 */
	public function fail(){
	
		$data['func'] = getUserMenu('fail');
		
		//接收 get/post資料
		$data['query'] = $this->input->get_post('info');
		
		$this->load->view('frontend/common/header.tpl' ,$data);
		$this->load->view('frontend/checkout/fail.tpl');
		
		$this->Dio_Process->cleanSession();
	}
	
	
}


/* End of file checkout_return.tpl */
/* Location: ./application/controllers/checkout_return */