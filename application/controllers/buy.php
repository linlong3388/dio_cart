<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 說明 : 處理[購物]的控制器
 * @controllerName buy
 * @author Dio
 *
 */
class buy extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('url','base','motion','is_valid','dio_business'));
		
		$this->load->model("frontend/product_model","product");
		$this->load->model("frontend/checkout_calc_model" ,"checkout_calc");
		
		$this->load->library(array('session'));
		
		//定義類別變數
		$this->err_msg = "";
	}
    
    // --------------------------------------------------------------------
    
    /**
     * 方法 : 設置基本資料(全域)
     *
     * @access	public
     * @param
     * @return
     */
    private function setInfo($product_speci_id){
    
    	$this->info = $this->product->info($product_speci_id);
    }
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 加入項目(採Ajax)
	 *
	 * @access	public
	 * @param
	 * @return json
	 */
	public function add(){
		
		//取得輸入值
		$buy_post = array(
			        'product_speci_id' => $this->input->get_post('product_speci_id'),
		                   'sku' => $this->input->get_post('sku'),
			            'entity' => $this->input->get_post('entity')
		             );
		
		//設置基本資料
		$this->setInfo( array('product_speci_id' => $buy_post['product_speci_id']) );
		
		//資料驗證             
		if($buy_post['entity'] <= 0){
			$this->err_msg = '商品數量不可為0!';
			
		}elseif(!$this->is_stock($buy_post)){
			
			$this->err_msg = '抱歉!商品庫存量不足!';			
		}else{
		
		  //寫入購物車SESSION
		  $data = array(
		              'category_id'        => $this->info['category_multi_id'],
		              'product_id'         => $this->info['product_id'],
		              'product_speci_id'   => $this->info['product_speci_id'],
				      'sku'                => $this->info['sku'],
		              'sku2'               => $this->info['sku2'],
				      'speci'              => $this->info['speci'],
		  		      'prc_name'           => $this->info['prc_name'],
				      'prs_name'           => $this->info['prs_name'],
				      'entity'             => $buy_post['entity'],
		              'image'              => $this->info['p_image'],
		              'name'               => $this->info['name'],
			          'price'              => $this->info['price'],
		              'total'              => $buy_post['entity'] * $this->info['price'],
		              'cdate'              => date('Y-m-d H:i:s')
		  );
		
		  $this->cls_cart->add($data);
			
		  //寫入購物車記錄表
		  if(IsLoginCustomer()){
			 $this->cart_log->setCartData($_SESSION['my_order']['cart']);
		  }
	
		}
		
		//依json格式返回
		echo json_encode($this->err_msg);	
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯項目
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit(){
		
		$cart_id = $this->input->get('cart_id');
		$entity = $this->input->get('entity');
		
		//更新購物車
		if( $this->cls_cart->edit($cart_id,$entity) ){
			
			//優惠券驗證
			//$this->discount_coupon_check();
			
			//重算運費
			$this->checkout_calc->getFee($this->cls_cart->select_all());
			$this->checkout_calc->getTotal();
		}	
		
		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
		
		redirect('checkout/step0');
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 刪除項目(單筆)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del(){

		$sku2 = $this->input->get_post('sku2');
		
		//更新購物車
		if( $this->cls_cart->del_sku($sku2) ){

			//優惠券驗證
			//$this->discount_coupon_check();
			
			//寫入購物車記錄表
			if(IsLoginCustomer()){
			   $this->cart_log->setCartData($_SESSION['my_order']['cart']);
		    }
		}else{
			$this->err_msg = "刪除失敗!";
		}
			
		//依json格式返回
		echo json_encode($this->err_msg);	
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 商品庫存量驗證
	 *
	 * @access	public
	 * @param
	 * @return 
	 */
	public function is_stock($post){
		
		return true;
		
		if($this->info['stock'] >= $post['entity']){
			return true;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 優惠券條件驗證
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function discount_coupon_check(){
	
		if( isset($_SESSION['my_order']['discount_coupon']) ){
		   $data = is_valid_discount_coupon($_SESSION['my_order']['discount_coupon']['code'] ,$this->cls_cart->count_all_total()); 

		   if($data['err_code'] == 2){
              unset($_SESSION['my_order']['discount_coupon']);
		   }
		}
		
	}
	
}


/* End of file buy.php */
/* Location: ./application/controllers/frontend/buy.php */