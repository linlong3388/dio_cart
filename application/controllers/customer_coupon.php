<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 前台(會員中心/折價券)管理首頁
 * @controllerName customer_coupon
 * @author Dio
 *
 */
class customer_coupon extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){

		parent::__construct();

		//載入模組
		$this->load->library(array('form_validation','session','promo/Coupon'));
		$this->load->helper(array('form','url','cookie','ctmall','database','base','motion'));
		
		//登入驗證
		if(!IsLoginCustomer()){
			redirect('sign_in/login');
		}         

		/*********************************
		 /* 設置幣別
		 *********************************/
		if( empty($_SESSION['motion_currency']) ){
			$_SESSION['motion_currency'] = 'twd';
		}
		
		//會員折價券
		$this->Coupon = new Coupon();
		
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists(){

		//驗證購物券是否過期
		$this->Coupon->enabled($_SESSION['customer_info']['customer_id']);
		
		$sql = "SELECT cc.* ,o.order_show_id
		          FROM customer_coupon cc
		         LEFT JOIN `order` o ON cc.order_id = o.order_id
				WHERE cc.customer_id = '".$_SESSION['customer_info']['customer_id']."'
				ORDER BY customer_coupon_id DESC";
		
		$data['query'] = $this->db->query($sql)->result_array();
		
		$data['func'] = 'customer_coupon_lists';
		
		$this->load->view('frontend/common/header.tpl',$data);
		$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/customer/coupon/lists.tpl');		
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 明細
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function view(){
	
		$this->load->model("frontend/order_model","order");
		$data['query_master'] = $this->order->shr_order_customer( $_SESSION['customer_info']['customer_id'] ,$this->input->get('order_id') );
		$data['query_detail'] = $this->order->shr_order_detail( array('order_id' => $this->input->get('order_id')) );
		
		//驗證會員所屬資料
		if(empty($data['query_master'])) show_404();

		$data['func'] = 'customer_coupon_view';
		
		//檢視view
		$this->load->view('frontend/common/header.tpl' ,$data);
		$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/customer/order/view.tpl');
		
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 退貨
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function returns(){
		
		$this->form_validation->set_rules('first_name','*姓','trim|required');
		$this->form_validation->set_rules('last_name','*名','trim|required');
		$this->form_validation->set_rules('mobile','*手機','trim|required');
		$this->form_validation->set_rules('phone','*電話','trim');
		$this->form_validation->set_rules('email','*Email','trim|required');
		$this->form_validation->set_rules('local','*區碼','trim|required');
		$this->form_validation->set_rules('address','*地址','trim|required');
		$this->form_validation->set_rules('entity','*數量','trim|required');
		$this->form_validation->set_rules('reason','*退(換)貨原因','trim|required');
		
		if ($this->form_validation->run() == TRUE){
		
			$entity = $this->input->post('entity');
			
			$is_entity_valid = $this->is_order_return_entity_valid($this->input->post('order_detail_id') ,$entity);
				
			if($entity <= 0){
				$this->session->set_flashdata('msg_err' ,'退貨數量不可等於0!');
				redirect('customer_coupon/returns?order_detail_id='.$this->input->post('order_detail_id'));
				return false;
			}
			
			if(!$is_entity_valid){
				$this->session->set_flashdata('msg_err' ,'退貨數量不可大於訂購量!');
				redirect('customer_coupon/returns?order_detail_id='.$this->input->post('order_detail_id'));
				return false;
			}
			
			$query_valid = $this->is_info_valid($this->input->post('order_detail_id'));
			
			$data = array(
					'order_detail_id' => $this->input->post('order_detail_id'),
					'customer_id'     => $_SESSION['customer_info']['customer_id'],
					'first_name'      => $this->input->post('first_name'),
					'last_name'       => $this->input->post('last_name'),
					'mobile'          => $this->input->post('mobile'),
					'phone'           => $this->input->post('phone'),
					'email'           => $this->input->post('email'),
					'local'           => $this->input->post('local'),
					'address'         => $this->input->post('address'),
					'entity'          => $entity,
					'price'           => $query_valid['price'],
					'total'           => ($entity*$query_valid['price']),
					'reason'          => $this->input->post('reason'),
					'cdate'           => date('Y-m-d H:i:s')
			);

			$this->db->insert('return',$data);
			
			$this->session->set_flashdata('msg' ,DIO_MSG_SUCCESS_INSERT);
			
			redirect('customer_return/lists');
		
		}else {

			$this->load->model("frontend/order_model","order");
			$data['query'] = $this->order->shr_order_detail_info($_SESSION['customer_info']['customer_id'] ,$this->input->get('order_detail_id'));
			
		    //驗證會員所屬資料
		    if(empty($data['query'])) show_404();
						
			//檢視view
			$this->load->view('frontend/common/header.tpl' ,$data);
			$this->load->view('frontend/common/menu.tpl');
			$this->load->view('frontend/customer/order/returns.tpl');
			
		}
			
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 狀態
	 * 說明 :
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function status(){

		$this->db->where('order_id' , $this->input->get('order_id'));
		$this->db->update('order' ,array('status' => 3));

		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
		
		redirect('customer_coupon/view?order_id='.$this->input->get('order_id'));
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 退貨數量驗證
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function is_order_return_entity_valid($order_detail_id ,$entity){
	
		$sql = "SELECT entity - IFNULL( (SELECT SUM(entity) FROM `return` WHERE order_detail_id = {$order_detail_id}) ,0 ) as entity
		FROM order_detail WHERE order_detail_id = ".$order_detail_id;
	
		$query = $this->db->query($sql)->row_array();
	
		if($query['entity'] < $entity){
	    	return false;
		}else{
		    return true;
		}
	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 退貨資料驗證
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function is_info_valid($order_detail_id){
	
		$sql = "SELECT price ,entity ,(price*entity) as total
		FROM order_detail WHERE order_detail_id = ".$order_detail_id;
	
		return $this->db->query($sql)->row_array();
   }

}


/* End of file customer.tpl */
/* Location: ./application/controllers/customer_coupon  */