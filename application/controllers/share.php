<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 說明 : 處理[客戶分享]的頁面請求
 * @controllerName share
 * @author Dio
 *
 */
class share extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->helper(array('url','cookie','ctmall','motion','database','base'));
		$this->load->library(array('form_validation','session'));
		
		//定義類別變數
		$this->err_msg = "";
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists(){
	
		$share_category_id = $this->input->get('share_category_id');
				
		$pg = new Dio_pagination();
		$pg_ary = $pg->get_limit($this->input->get('page'),10);

		//取得列表資料
		$this->db->limit($pg_ary['limit2'] ,$pg_ary['limit1']);
		if( isset($share_category_id) && is_numeric($share_category_id) ){
			$this->db->where('share_category_id',$share_category_id);
		}
		$this->db->order_by('cdate' ,'DESC');
		$this->db->where('status' ,1);
		$data['query'] = $this->db->get('share')->result_array();
		
		//取得總數
		if( isset($share_category_id) && is_numeric($share_category_id) ){
			$this->db->where('share_category_id',$share_category_id);
		}
		$this->db->where('status' ,1);
		$data['query_total'] = $this->db->count_all_results('share');
		
		//設定分頁
		$data['pagination']  = $pg->pagination($data['query_total'],5);
		
		$data['share_category_id'] = $share_category_id;
		
		$data['ad']           = $this->ad();
		$data['hot']          = $this->hot();

		$data['func'] = 'share_lists';
		
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);
		$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/share/lists.tpl');
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢視
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function view(){
	
		$share_id = $this->input->get('share_id');
		
		//$data['ad']           = $this->ad();
		//$data['hot']          = $this->hot();
		
		$data['ctr']          = $this->ctr($share_id);
		$data['hobby']        = $this->hobby_month();
		
		$data['query']        = $this->get_share_view( $share_id );

		//臉書分享
		$data['query_fb']     = $this->set_fbShare($data['query']);
		
		$this->reset_ctr();
		
		$data['func'] = 'share_view';
		
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);
		$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/share/view.tpl');
	}	
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 廣告
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function ad(){
		
		$this->db->where('status' ,'1');
		$this->db->order_by('ad_home_id' ,'DESC');
		$ad = $this->db->get('ad_home')->result_array();

	    foreach (get_ad_home_type_data() as $key=>$val) {
        	$data[$key] = array();
        }
        
        foreach ($ad as $row) {
        	switch ($row['type']) {
        		case 'a':
					array_push($data['a'], $row);
					break;
        		case 'b':
					array_push($data['b'], $row);
					break;
			}		
        }
                
        return $data;
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 熱銷商品
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function hot(){
		
      $this->load->model('frontend/product_model' ,'product');

	  //商品搜尋條件
	  $srh_data['srh_action_id'] = 6;
	  $srh_data['num'] = 40;

	  $data = $this->product->shr_product($srh_data);

	  return $data;
   }

   // --------------------------------------------------------------------
    
   /**
    * 方法 : 取得客戶分享/檢視
    *
    * @access	public
    * @param
    * @return
    */
   public function get_share_view($share_id){
   
   	  $this->db->where('status' ,1);
   	  $this->db->where('share_id' ,$share_id);
   	  
   	  return $this->db->get('share')->row_array();
   }
   
   // --------------------------------------------------------------------
   
   /**
    * 方法 : 點擊率累加
    *
    * @access	public
    * @param
    * @return
    */
   public function ctr($share_id){
   
   	 $sql = "UPDATE `share` SET `ctr_unlimited` = ctr_unlimited + 1 ,
   			                   `ctr_month` = ctr_month + 1 
	 			  WHERE `share`.`share_id` = ".$share_id;
   	 
   	 $this->db->query($sql);
   }    
   
   // --------------------------------------------------------------------
   
   /**
    * 方法 : 也許你還喜歡(依點擊率排序,無期限)
    *        
    * @access	public
    * @param
    * @return
    */
   public function hobby_unlimited(){
   
   	 $this->db->limit(5);
   	 $this->db->where('status' ,1);
   	 //$this->db->where('SUBSTRING(cdate ,1,7) =', date('Y-m'));
   	 $this->db->order_by('ctr_unlimited' ,'DESC');
   	 
   	 return $this->db->get('share')->result_array();
   }
   
   // ------------------------------------------------------------------------
   
   /**
    * hobby_month
    *
    * 方法 : 也許你還喜歡(依點擊率排序(毎月))
    *
    * @access	public
    * @return	array
    */
    public function hobby_month(){
    	
   		$this->db->limit(5);
   		$this->db->where('status' ,1);
   		//$this->db->where('SUBSTRING(cdate ,1,7) =', date('Y-m'));
   		$this->db->order_by('ctr_month' ,'DESC');
   
   		return $this->db->get('share')->result_array();
   	}
   
   // --------------------------------------------------------------------
   
   /**
    * 方法 : 依月份，重設毎月點擊率
    *
    * @access	public
    * @param
    * @return
    */
   public function reset_ctr(){
   	
   	  $data = array( 'ctr_month' => 0 );
   	
   	  $this->db->where('SUBSTRING(cdate ,1,7) <', date('Y-m'));
      $this->db->update('share' ,$data);
   }
   
   // --------------------------------------------------------------------
    
   /**
    * 方法 : 定義臉書分享
    *
    * @access	public
    * @param
    * @return
    */
   public function set_fbShare($query){
   	 
    	return array('is_fbShare' => true ,
   			'og_title'        => $query['title'] ,
   			'og_description'  => $query['content'] ,
   			'og_image'        => $query['image']
   	    );
   }   
   
}


/* End of file share.tpl */
/* Location: ./application/controllers/frontend/share.tpl */