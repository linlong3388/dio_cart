<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 說明 : 處理[網站內容]的頁面請求
 * @controllerName content
 * @author Dio
 *
 */
class content extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();
		
		$this->load->library(array('session'));
		$this->load->helper(array('url','cookie','ctmall','motion','database','base'));
		
		//定義類別變數
		$this->err_msg = "";
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 檢視
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function view(){
		
		$content_id = $this->input->get('content_id');
		
		$data['query_category'] = $this->db->get('content')->result_array();
		
		if( !empty($content_id) ){
		   $this->db->where( 'content_id' ,$content_id );
		}
		$this->db->limit(1);
		$data['query'] = $this->db->get('content')->row_array();
		
		//臉書分享
		$data['query_fb']     = $this->set_fbShare($data['query']);
		
		$data['content_id'] = $content_id;
		
		$data['func'] = 'content_view';
		
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);
		$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/content/view.tpl');
	}
	   
   // --------------------------------------------------------------------
   
   /**
    * 方法 : 取得網站內容 /列表
    *
    * @access	public
    * @param
    * @return
    */
   public function get_content_list($content_category_id){
   	  
   	  $this->db->order_by('sort_order' ,'DESC');
   	  $this->db->where('content_category_id' ,$content_category_id);
   	  
   	  return $this->db->get('content')->result_array();
   }
   
   // --------------------------------------------------------------------
   
   /**
    * 方法 : 定義臉書分享
    *
    * @access	public
    * @param
    * @return
    */
   public function set_fbShare($query){
   	 
   	  return array('is_fbShare' => true ,
   			'og_title'        => $query['title'] ,
   			'og_description'  => $query['content'] ,
   			'og_image'        => $query['image']
   	  );
   }  
  
}


/* End of file content.tpl */
/* Location: ./application/controllers/frontend/content */