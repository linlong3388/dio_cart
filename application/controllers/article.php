<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 說明 : 處理[手作課程]的頁面請求
 * @controllerName article
 * @author Dio
 *
 */
class article extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->helper(array('url','cookie','ctmall','motion','database','base'));
		$this->load->library(array('form_validation','session'));
		
		$this->load->model("frontend/article_model","article");
		
		//定義類別變數
		$this->err_msg = "";
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists(){
	
		$pg = new dio_pagination();
		
		$pg_ary   = $pg->get_limit($this->input->get('page'),12);
		$srh_data = $this->search($this->input->get() ,$pg_ary);
	
		$data['query']       = $this->article->lists($srh_data);
		
		$data['query_total'] = $this->article->lists_group_by($srh_data);
		
		//META 設定
		$data['query_meta'] = seoMeta('content' ,$data['query']);
		$data['query_fb']   = fb_like($data['query']);
		
		//設定分頁
		$data['pagination']  = $pg->pagination($data['query_total'],5);
		
	    $data['func']        = getUserMenu('article_lists') ;
		
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);
		//$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/article/lists.tpl');
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢視
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function view(){
	
		$article_id = $this->input->get('article_id');
		
		//$data['ctr']        = $this->ctr($article_id);		
		$data['query']      = $this->get_article_view($article_id);

		//META 設定
		$data['query_meta'] = seoMeta('content' ,$data['query']);
		$data['query_fb']   = fb_like($data['query']);
		
		$data['func']       = getUserMenu('article_view') ;
		
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);
		//$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/article/view.tpl');
	}	

	// --------------------------------------------------------------------
	 
	/**
	 * 方法 : 點擊率累加
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function ctr($article_id){
		 
		$sql = "UPDATE `article` SET `ctr_unlimited` = ctr_unlimited + 1 ,
   			                   `ctr_month` = ctr_month + 1
	 			  WHERE `article`.`article_id` = ".$article_id;
		 
		$this->db->query($sql);
	}
		
   // --------------------------------------------------------------------
    
   /**
    * 方法 : 取得最新消息/檢視
    *
    * @access	public
    * @param
    * @return
    */
   public function get_article_view($article_id){
   
   	  $this->db->where('status' ,1);
   	  $this->db->where('article_id' ,$article_id);
   	  
   	  return $this->db->get('article')->row_array();
   }
   
   // --------------------------------------------------------------------
   
   /**
    * 方法 : 定義臉書分享
    *
    * @access	public
    * @param
    * @return
    */
   public function set_fbShare($query){
   
     	return array('is_fbShare' => true ,
   			'og_title'        => $query['title'] ,
   			'og_description'  => $query['content'] ,
   			'og_image'        => $query['image']
   	    );
   }
    
   // --------------------------------------------------------------------
   
   /**
    * 方法 : 搜尋
    *
    * @access	public
    * @param
    * @return
    */
   public function search ($data ,$pg_ary) {
   
   	if(isset($data) && !empty($data)){
   		foreach ($data as $key=>$val) {
   			if($val == ''){
   				unset($data[$key]);
   			}
   		}
   	}
   
   	//兩層分類(格式如1_5)
   	$data['srh_category_id_1'] = '';
   	$data['srh_category_id_2'] = '';
   
   	if( isset($data['srh_category']) && $data['srh_category'] != '*' ){
   		$category_id = explode('_',$data['srh_category']);
   		$data['srh_category_id_1'] = $category_id[0];
   		$data['srh_category_id_2'] = $category_id[1];
   	}
   
   	$data['srh_limit1']   = $pg_ary['limit1'];
   	$data['srh_limit2']   = $pg_ary['limit2'];
   
   	return $data;
   }
   
   
}


/* End of file article.tpl */
/* Location: ./application/controllers/frontend/article.tpl */