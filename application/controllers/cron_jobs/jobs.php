<?php //if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 *
 * 說明 : 處理[排程]的控制器。
 *         
 * @controllerName jobs
 * @author Dio
 *
 */
class jobs extends CI_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		
		parent::__construct();
		
		$this->load->database();
		$this->load->helper(array('url','file','dio_array','dio_url'));
		$this->load->library(array('promo/VIP','promo/Coupon'));
		
		/*
		 |---------------------------------------------------------------
		 | 設定記憶體大小&最大執行時間(3小時)
		 |---------------------------------------------------------------
		 */
		ini_set('memory_limit','512M');
		ini_set('max_execution_time','10800');
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 會員降等
	 *      
	 * @access	public
	 * @param
	 * @return
	 */
	public function vip_down(){	
        
		$vip = new VIP();
		$vip->down();
		
		echo '會員降等異動完成! :)';
		
		exit;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 折價券過期
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function conpon_Expired(){
	
		$coupon = new Coupon();
		$coupon->enabled();
	
		echo '折價券過期異動 ，完成! :)';
	
		exit;
	}
	
}


// --------------------------------------------------------------------
//
// 啟動類別方法
//
// --------------------------------------------------------------------
$j   = new jobs();
$j->{getUrlLastSection()}();


/* End of file jobs.php */
/* Location: ./cron_jobs/jobs.php */