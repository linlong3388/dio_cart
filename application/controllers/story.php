<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 說明 : 處理[品牌故事]的頁面請求
 * @controllerName story
 * @author Dio
 *
 */
class story extends FrontEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->helper(array('url','cookie','ctmall','motion','database','base'));
		$this->load->library(array('form_validation','session'));
		
		//定義類別變數
		$this->err_msg = "";
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢視
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function view(){
	
		$this->db->where('status' ,1);
		$data['query']      = $this->db->get('story')->result_array();

		$this->db->where('status' ,1);
		$this->db->where('func' ,'story_img');
		$this->db->order_by('sort_order' ,'DESC');
		$data['query_img']  = $this->db->get('common_picture')->result_array();
		
		//META 設定
		$data['query_meta'] = seoMeta('content' ,$data['query']);
		$data['query_fb']   = fb_like($data['query']);
		
		$data['func']       = getUserMenu('story_view') ;
		
		//檢視view
		$this->load->view('frontend/common/header.tpl',$data);
		//$this->load->view('frontend/common/menu.tpl');
		$this->load->view('frontend/story/view.tpl');
	}	
	 
   // --------------------------------------------------------------------
   
   /**
    * 方法 : 定義臉書分享
    *
    * @access	public
    * @param
    * @return
    */
   public function set_fbShare($query){
   
     	return array('is_fbShare' => true ,
   			'og_title'        => $query['title'] ,
   			'og_description'  => $query['content'] ,
   			'og_image'        => $query['image']
   	    );
   }
    
   
}


/* End of file story.tpl */
/* Location: ./application/controllers/frontend/story.tpl */