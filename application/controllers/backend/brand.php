<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理 [品牌]的控制器
 * @controllerName brand
 * @author Dio
 *
 */
class brand extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));
		
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists () {

		$data['slt_status'] = $this->slt_status();
		$data['query'] = $this->db->get('brand')->result_array();

		//檢視view
		$this->load->view("backend/common/header.tpl",$data);
		$this->load->view("backend/brand/lists.tpl");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add () {

		$this->form_validation->set_rules('title','*標題','trim|required');
		$this->form_validation->set_rules('logo','*logo','trim|required');
		$this->form_validation->set_rules('image','*橫幅','trim');
		//$this->form_validation->set_rules('content','*內容','trim|required');
		
		if ($this->form_validation->run() == TRUE){

			$data = array(
					    'title' => $this->input->post('title'),
					     'logo' => $this->input->post('logo'),
					    'image' => $this->input->post('image'),
				   	  //'content' => $this->input->post('content'),
                        'cdate' => date('Y-m-d H:i:s') 
			        );

			$this->db->insert('brand',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
			
			redirect('backend/brand/lists');
			
		} else { 

			$this->load->view("backend/common/header.tpl");
			$this->load->view("backend/brand/add.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit () {
		
		$this->form_validation->set_rules('title','*標題','trim|required');
		$this->form_validation->set_rules('logo','*logo','trim|required');
		$this->form_validation->set_rules('image','*橫幅','trim');
		$this->form_validation->set_rules('url','*URL','trim');
		//$this->form_validation->set_rules('content','*內容','trim|required');
		$this->form_validation->set_rules('sort_order','*排序','trim|required');
		$this->form_validation->set_rules('status','*狀態','trim|required');
		
		if ($this->form_validation->run() == TRUE){

			$data = array(
					    'title' => $this->input->post('title'),
					     'logo' => $this->input->post('logo'),
					    'image' => $this->input->post('image'),
					      'url' => $this->input->post('url'),
				   	  //'content' => $this->input->post('content'),
				   'sort_order' => $this->input->post('sort_order'),
				 	   'status' => $this->input->post('status')
				    );
			
			$this->db->where('brand_id', $this->input->post('brand_id'));
			$this->db->update('brand',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);

			Redirect('backend/brand/edit?brand_id='.$this->input->post('brand_id'));

		} else { 

			$data['query']  = $this->db->get_where('brand', array('brand_id' => $this->input->get('brand_id')) )->row_array();

			//檢視view
			$this->load->view("backend/common/header.tpl",$data);
			$this->load->view("backend//brand/edit.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del () {
			
		$this->db->delete('brand' , array('brand_id' => $this->input->get('brand_id')));
		
		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
		
		Redirect('backend/brand/lists');
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 下拉式 / 狀態
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function slt_status () {
		
		return array(
		               '0' => "<b style='color:red'>停用</b>" ,
                       '1' => '啟用'
	            );
	}
	
}


/* End of file brand.php */
/* Location: ./application/controllers/backend/brand.php */