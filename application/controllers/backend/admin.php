<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理 [管理員設定]的控制器
 * @controllerName admin
 * @author Dio
 *
 */
class admin extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));
		
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
		
		//載入model
		$this->load->model("backend/admin_model","admin");
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists () {
		
		$data['query'] = $this->admin->getAdminLists();
		
		$this->load->view("backend/common/header.tpl",$data);
		$this->load->view("backend/admin/lists.tpl");
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add () {
	
		$this->form_validation->set_rules('admin_group_id','*群組','trim|required');
		$this->form_validation->set_rules('username','*帳號','trim|required');
		$this->form_validation->set_rules('password','*密碼','trim|required');
		
		if ($this->form_validation->run() == TRUE){
	
			$data = array(
					'admin_group_id' => $this->input->post('admin_group_id'),
					'username' => $this->input->post('username'),
					'password' => md5($this->input->post('password')),
					'nickname' => $this->input->post('nickname'),
					'email' => $this->input->post('email'),
					'cdate' => date('Y-m-d H:i:s')
			);
	
			$this->db->insert('admin',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
				
			redirect('backend/admin/lists');
	
	
		} else { //轉向預設頁面
			
			$data['query_admin_group']  = $this->getAdminGroup();
				
			$this->load->view("backend/common/header.tpl" ,$data);
			$this->load->view("backend/admin/add.tpl");
		}
	
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit () {
		
		$this->form_validation->set_rules('admin_group_id','*群組','trim|required');
		$this->form_validation->set_rules('admin_id','*編號','trim|required');
		$this->form_validation->set_rules('username','*帳號','trim|required');
		$this->form_validation->set_rules('nickname','*暱稱','trim');
		$this->form_validation->set_rules('email','*Email','trim|valid_email');
		$this->form_validation->set_rules('mobile','*手機','trim');
		$this->form_validation->set_rules('status','*狀態','trim|required');
		
		if ($this->form_validation->run() == TRUE){
			
			$data = array(
					'admin_group_id' => $this->input->post('admin_group_id'),
                     'username' => $this->input->post('username'),
                     'nickname' => $this->input->post('nickname'),
                        'email' => $this->input->post('email'),
					   'mobile' => $this->input->post('mobile'),
                       'status' => $this->input->post('status'),
			);
			
			$this->db->where('admin_id', $this->input->post('admin_id'));
			$this->db->update('admin',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
			
			redirect('backend/admin/edit?admin_id='.$this->input->post('admin_id'));			
			
		} else { 

			$data['query'] = $this->db->get_where('admin', array('admin_id' => $this->input->get('admin_id')) )->row_array();
			$data['query_admin_group']  = $this->getAdminGroup();
			
			//檢視view
			$this->load->view("backend/common/header.tpl" ,$data);
			$this->load->view("backend/admin/edit.tpl");
		}

	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 編輯密碼
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function pass () {
	
		$this->form_validation->set_rules('admin_id', '*ID', 'trim|required');
		$this->form_validation->set_rules('password', '*新密碼', 'trim|required|matches[passconf]|md5');
		$this->form_validation->set_rules('passconf', '*密碼確認', 'trim|required');
	
		if ($this->form_validation->run() == TRUE){
				
			$data = array(
					'password' => $this->input->post('password')
			);
	
			$this->db->where('admin_id', $this->input->post('admin_id'));
			$this->db->update('admin',$data);
				
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
				
			redirect('backend/admin/pass?admin_id='.$this->input->post('admin_id'));
	
		}else{
	
			$data['query']  = $this->db->get_where('admin', array('admin_id' => $this->input->get('admin_id')) )->row_array();
	
			//檢視view
			$this->load->view("backend/common/header.tpl" ,$data);
			$this->load->view("backend/admin/pass.tpl");
		}
	
	}
		
	// --------------------------------------------------------------------

	/**
	 * 方法 : 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del () {
			
		$this->db->delete('admin' , array('admin_id' => $this->input->get('admin_id')));
		
		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
		
		Redirect('backend/admin/lists');
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 下拉式 / 狀態
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function slt_status () {
		
		return array(
		               '0' => '停用' ,
                       '1' => '啟用'
	            );
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得管理員群組資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getAdminGroup () {
				
		$this->db->where('status','1');
		$query = $this->db->get('admin_group')->result_array();
		
		return $query;
	}
	
}


/* End of file admin.php */
/* Location: ./application/controllers/backend/admin.php */