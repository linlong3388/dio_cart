<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理 [得獎記錄]的控制器
 * @controllerName awards
 * @author Dio
 *
 */
class awards extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));
		
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists () {

		$data['slt_status'] = $this->slt_status();
		$data['query'] = $this->db->get('awards')->result_array();

		//檢視view
		$this->load->view("backend/common/header.tpl",$data);
		$this->load->view("backend/awards/lists.tpl");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add () {

		$this->form_validation->set_rules('title','*標題','trim|required');
		$this->form_validation->set_rules('image','*圖片','trim');
		//$this->form_validation->set_rules('content','*內容','trim|required');
			
		if ($this->form_validation->run() == TRUE){

			$data = array(
					    'title' => $this->input->post('title'),
					    'image' => $this->input->post('image'),
					  //'content' => $this->input->post('content'),
                        'cdate' => date('Y-m-d H:i:s') 
			        );

			$this->db->insert('awards',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
			
			redirect('backend/awards/lists');
			
		} else { 

			$this->load->view("backend/common/header.tpl");
			$this->load->view("backend/awards/add.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit () {
		
		$this->form_validation->set_rules('awards_id','*編號','trim|required');
		$this->form_validation->set_rules('title','*標題','trim|required');
		$this->form_validation->set_rules('image','*圖片','trim');
		//$this->form_validation->set_rules('content','*內容','trim|required');
		$this->form_validation->set_rules('status','*狀態','trim|required');
		$this->form_validation->set_rules('cdate','*建檔日期','trim|required');
		
		if ($this->form_validation->run() == TRUE){

			$data = array(
					     'title' => $this->input->post('title'),
					     'image' => $this->input->post('image'),
					   //'content' => $this->input->post('content'),
				        'status' => $this->input->post('status')
			        );

			$this->db->where('awards_id', $this->input->post('awards_id'));
			$this->db->update('awards',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);

			Redirect('backend/awards/edit?awards_id='.$this->input->post('awards_id'));

		} else { //轉向預設頁面

			$data['query']  = $this->db->get_where('awards', array('awards_id' => $this->input->get('awards_id')) )->row_array();

			//檢視view
			$this->load->view("backend/common/header.tpl",$data);
			$this->load->view("backend//awards/edit.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del () {
			
		$this->db->delete('awards' , array('awards_id' => $this->input->get('awards_id')));
		
		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
		
		Redirect('backend/awards/lists');
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 下拉式 / 狀態
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function slt_status () {
		
		return array(
		               '0' => '停用' ,
                       '1' => '啟用'
	            );
	}
	
}


/* End of file awards.php */
/* Location: ./application/controllers/backend/awards.php */