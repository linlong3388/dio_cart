<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理[活動]的控制器
 * @controllerName action
 * @author Dio
 *
 */
class action extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));

		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
		
		//載入model
		$this->load->model("backend/action_model","action");
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists(){

		$data['query'] = $this->action->shr_action_list();

		//檢視view
		$this->load->view("backend/common/header.tpl" ,$data);
		$this->load->view("backend/action/lists.tpl");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add() {			
		
		$this->form_validation->set_rules('type','*活動類型','trim|required');
		$this->form_validation->set_rules('name','*活動名稱','trim|required');

		if ($this->form_validation->run() == TRUE){
			
			$data = array(
                          'type' => $this->input->post('type'),
	  	                  'name' => $this->input->post('name'),
	  	                 'cdate' => date('Y-m-d H:i:s') 
			);

			$this->db->insert('ct_action' ,$data);
				
			dioRedirect('backend/action/lists');

		} else { //轉向預設頁面

			//檢視view
			$this->load->view("backend/common/header.tpl");
			$this->load->view("backend/action/add.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit(){

		$this->form_validation->set_rules('action_id','*編號','trim|required');
		$this->form_validation->set_rules('name','*活動名稱','trim|required');
		$this->form_validation->set_rules('sort_order','*排序','trim');
		$this->form_validation->set_rules('status','*狀態','trim|required');
		$this->form_validation->set_rules('cdate','*新增日期','trim');

		if ($this->form_validation->run() == TRUE){
			
			$data = array(
                      'name' => $this->input->post('name'),
			    'sort_order' => $this->input->post('sort_order'),
			        'status' => $this->input->post('status'),
                     'udate' => date('Y-m-d H:i:s')
			);

			$this->db->where('action_id', $this->input->post('action_id'));
			$this->db->update('ct_action',$data);
				
			dioRedirect('backend/action/lists');

		} else { //轉向預設頁面

			$this->db->where('action_id' ,$this->input->get('action_id'));
			$data['query'] = $this->db->get('ct_action')->row_array();

			//檢視view
			$this->load->view("backend/common/header.tpl" ,$data);
			$this->load->view("backend/action/edit.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del () {		
	
		//if(!$this->cls_category->is_sub_category($this->input->get('category_id'))){

		$this->db->delete('ct_action' , array('action_id' => $this->input->get('action_id')));
		dioRedirect('backend/action/lists');
		//}else{

		//dioRedirect('backend/action/category_list' ,'*不可刪除! 此分類項已有關聯子分類!');
		//}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 檢視商品
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function view_product(){

		$data['query'] = $this->action->shr_action_product_list_has($this->input->get('action_id'));

		//檢視view
		$this->load->view("backend/common/header.tpl" ,$data);
		$this->load->view("backend/action/view_product.tpl");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 加入商品
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add_product(){
		
		$data['query'] = $this->action->shr_action_product_list($this->input->get('action_id'));

		//檢視view
		$this->load->view("backend/common/header.tpl" ,$data);
		$this->load->view("backend/action/add_product.tpl");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 活動分類 / 加入商品 / 更新
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add_product_upd() {
		
			$action_id = $this->input->get('action_id');
			$product_id_ary = explode(',', $this->input->get('aryList'));

			for($i=0 ;$i<COUNT($product_id_ary) ;$i++){

				$this->db->insert('ct_action_to_product' ,array('action_id' => $action_id ,
                                                              'product_id' => $product_id_ary[$i] ,
			                                                       'cdate' => date('Y-m-d H:i:s')
				));

			}

			dioRedirect('backend/action/lists');
		

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 活動分類 / 加入商品 / 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add_product_del() {
	
			$action_id = $this->input->get('action_id');
			$product_id_ary = explode(',', $this->input->get('aryList'));

			for($i=0 ;$i<COUNT($product_id_ary) ;$i++){

				$this->db->where('action_id' ,$action_id);
				$this->db->where('product_id' ,$product_id_ary[$i]);
				$this->db->delete('ct_action_to_product');
			}

			dioRedirect('backend/action/lists');
		

	}
	
}


/* End of file action.php */
/* Location: ./application/controllers/backend/action.php */