<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 -  處理[禮盒]的控制器
 *          說明 : [禮盒]和商品同為一張資料表(product)，概念上將禮盒視為商品的一種。
 * 
 * @controllerName product_box
 * @author Dio
 *
 */
class product_box extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid','elfinder'));
		$this->load->helper(array('dio_string','dio_message','dio_business'));
		$this->load->library(array('form_validation','session','Dio_paginator'));

		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
		
		$this->load->model("backend/product_model","model");
	}
		
	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists () {

		$this->db->where('type_id' ,1);
		$data['query'] = $this->db->get('product')->result_array();
		
		//檢視view
		$this->load->view("backend/common/header.tpl" ,$data);
		$this->load->view("backend/product_box/lists.tpl");
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add(){
		
		$this->form_validation->set_rules('barcode','*國際條碼','callback_check_is_barcode');
		$this->form_validation->set_rules('fee_category_id','*運費類別','trim|required');
		$this->form_validation->set_rules('name','*禮盒名稱','trim|required');
		$this->form_validation->set_rules('price','*價格','trim|required');
		$this->form_validation->set_rules('unit','*單位','trim|required');
		$this->form_validation->set_rules('image','*主圖','trim');
	
		if ($this->form_validation->run() == TRUE){
				
			//產生國際條碼
			$this->isGetBarcode($this->input->post('barcode'));
			
			$sku = getRandId('BOX');
			
			//----------------------------------------------------------------------
			// 寫入商品主表
			//----------------------------------------------------------------------
			$data_master = array(
					'category_multi_id' => 0,
					'sku' => $sku,
					'barcode' => $this->input->post('barcode'),
					'type_id' => 1,
					'fee_category_id' => $this->input->post('fee_category_id'),
					'name' => $this->input->post('name'),
					'price' => $this->input->post('price'),
					'unit' => $this->input->post('unit'),
					'image' => $this->input->post('image'),
					'cdate' => date('Y-m-d H:i:s')
			);
			
			//1)寫入商品主表
			$this->db->insert('product',$data_master);
	
			$product_id = $this->db->insert_id();
	
			$data_detail = array(
					'product_id' => $product_id,
					'cdate' => date('Y-m-d H:i:s')
			);
	
			//2)寫入商品主表/備註
			$this->db->insert('product_memo' , $data_detail);
	
			//3)寫入商品子表/屬性
			$data_speci = array(
					'product_id'        => $product_id,
					'sku'               => $sku,
					'sku2'              => $sku,
					'property_id_size'  => '',
					'property_id_color' => '',
					'stock'             => $this->input->post('stock'),
					//'image'             => $post_data['image'],
					'cdate'             => date('Y-m-d H:i:s')
			);
		  
			$this->db->insert('product_speci' ,$data_speci);
				
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
				
			redirect('backend/product_box/lists?page=1');
				
		}else{
				
			$data['query_category'] = $this->db->get('category')->result_array();
	
			$this->load->view('backend/common/header.tpl' ,$data);
			$this->load->view('backend/product_box/add.tpl');
		}
	
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit(){

		$this->form_validation->set_rules('fee_category_id','*運費類別','trim|required');
		$this->form_validation->set_rules('barcode','*國際條碼','callback_check_is_barcode');
		$this->form_validation->set_rules('name','*名稱','trim|required');
		$this->form_validation->set_rules('price','*價格','trim|required');
		$this->form_validation->set_rules('unit','*單位','trim|required');
		$this->form_validation->set_rules('image','*主圖','trim');	
		$this->form_validation->set_rules('status','*狀態','trim|required');
		$this->form_validation->set_rules('sort_order','*排序','trim|required');
		$this->form_validation->set_rules('meta_title','*Meta標籤標題','trim');
		$this->form_validation->set_rules('meta_description','*Meta標籤描述','trim');
		$this->form_validation->set_rules('meta_keyword','*Meta標籤關鍵字','trim');
			
		if ($this->form_validation->run() == TRUE){

			//產生國際條碼
			$this->isGetBarcode($this->input->post('barcode'));
		
			//----------------------------------------------------------------------
			// 更新商品主/副表
			//----------------------------------------------------------------------
			$data_master = array(
		       'fee_category_id' => $this->input->post('fee_category_id'),
					  'barcode' => $this->input->post('barcode'),
                         'name' => $this->input->post('name'),
				  	    'price' => $this->input->post('price'),
						'image' => $this->input->post('image'),	
					     'unit' => $this->input->post('unit'),
                   'sort_order' => $this->input->post('sort_order'),
                       'status' => $this->input->post('status'),
                        'udate' => date('Y-m-d H:i:s'),
					'meta_title'         => $this->input->post('meta_title'),
					'meta_description'   => $this->input->post('meta_description'),
					'meta_keyword'       => $this->input->post('meta_keyword'),
			);

	 	//更新商品主表
	 	$this->db->where('product_id', $this->input->post('product_id'));
	 	$this->db->update('product',$data_master);
	 	
	 	$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
	 	
	 	redirect('backend/product_box/edit?'.getQueryStringParam());

	 } else { //轉向預設頁面
	 	 
	 	$data['query'] = $this->model->shr_product_memo_view($this->input->get('product_id'));
	 	
	 	$this->db->where('product_id' ,$this->input->get('product_id'));
	 	$this->db->order_by('sort_order' ,'DESC');
	 	$data['query_attr'] = $this->db->get('product_attr')->result_array();
	 	
	 	//檢視view
	 	$this->load->view('backend/common/header.tpl' ,$data);
	 	$this->load->view('backend/product_box/edit.tpl');
	 }
	  
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del () {

		$this->db->delete('product' ,array('product_id' => $this->input->get('product_id')));
		
		//此專案`product_speci`未做外鍵關聯，所以需獨立刪除
		$this->db->delete('product_speci' ,array('product_id' => $this->input->get('product_id')));

		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
		
		redirect('backend/product_box/lists');
	}
	

	// --------------------------------------------------------------------

	/**
	 * 方法 : 商品 / 編輯 /圖片
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function product_box_edit_image(){
	
		//商品屬性
		$data['query'] = $this->model->shr_product_box_speci_info($this->input->get('product_speci_id'));

		//商品屬性 /圖片
		$this->db->where('product_speci_id' ,$this->input->get('product_speci_id'));
		$data['query_image'] = $this->db->get('product_box_speci_image')->result_array();

		//檢視view
		$this->load->view('backend/common/header.tpl' ,$data);
		$this->load->view('backend/product_box/product_box_edit_image.tpl');
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 商品 / 編輯 /圖片 /新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function product_box_edit_image_add(){
	
		$err_msg = "";
			
		parse_str($this->input->post('data') ,$post_data);
			
		//欄位驗證&規則
		if(empty($post_data['image'])){
			$err_msg = "*圖片 不可空白!";
			echo $err_msg;
			return false;
		}

		$data = array(
		     'product_speci_id' => $post_data['product_speci_id'],
                        'image' => $post_data['image'],
                    'image_alt' => $post_data['image_alt'],
                        'cdate' => date('Y-m-d H:i:s')
		);
	 	
		//新增商品
		$this->db->insert('product_box_speci_image',$data);
	}


	// --------------------------------------------------------------------

	/**
	 * 方法 : 商品 / 編輯 /圖片 /刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function product_box_edit_image_del(){
	
        	$this->db->where('product_box_speci_image_id' ,$this->input->get('product_box_speci_image_id'));
			$this->db->delete('product_box_speci_image');
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢查 / 國際條碼格式
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function check_is_barcode( $barcode )
	{
		$barcode = trim($barcode);
	
		if( empty($barcode) )
		{
			$this->form_validation->set_message('check_is_barcode', '*國際條碼不可空白');
			return false;
		}
	
		if( strlen($barcode) != 13)
		{
			$this->form_validation->set_message('check_is_barcode', '*國際條碼必須為13碼');
			return false;
		}
	
		if( !is_numeric($barcode) )
		{
			$this->form_validation->set_message('check_is_barcode', '*國際條碼必須數字');
			return false;
		}
	
		if( substr($barcode,0,3) != '471' ) //國碼(471)
		{
			$this->form_validation->set_message('check_is_barcode', '*國際條碼前3碼必須為 (471)');
			return false;
		}
	
		if( substr($barcode,3,6) != '297259' ) //公司前置碼需為(297259)
		{
			$this->form_validation->set_message('check_is_barcode', '*國際條碼4-9碼必須為(297259)');
			return false;
		}
	
		return true;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 是否取得條碼資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function isGetBarcode( $barcode ){
	
		//引入 barcode 掛件
		require_once(APPPATH.'libraries/barcode/Exceptions/BarcodeException.php');
		require_once(APPPATH.'libraries/barcode/Exceptions/InvalidCharacterException.php');
		require_once(APPPATH.'libraries/barcode/Exceptions/InvalidCheckDigitException.php');
		require_once(APPPATH.'libraries/barcode/Exceptions/InvalidFormatException.php');
		require_once(APPPATH.'libraries/barcode/Exceptions/InvalidLengthException.php');
		require_once(APPPATH.'libraries/barcode/Exceptions/UnknownTypeException.php');
	
		require_once(APPPATH.'libraries/barcode/BarcodeGenerator.php');
		require_once(APPPATH.'libraries/barcode/BarcodeGeneratorJPG.php');
	
		$generatorJPG = new BarcodeGeneratorJPG();
	
		$file = 'resources/barcode/'.$barcode.'.jpg';
	
		file_put_contents($file, $generatorJPG->getBarcode($barcode, $generatorJPG::TYPE_EAN_13));
			
		if( file_exists($file))
			return true;
	
		return false;
	}
	

}


/* End of file product_box.php */
/* Location: ./application/controllers/backend/product_box.php */