<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理[預購單]的控制器
 * @controllerName order_pre
 * @author Dio
 *
 */
class order_pre extends BackEnd_Controller {
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session','category','Dio_paginator'));
		
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
		
		//載入model
		$this->load->model("backend/order_pre_model","order_pre");

		//建立商品分類物件
		$this->cls_category = new category();
		
		
		if($this->input->get('page')){ //頁碼
		   $this->page = $this->input->get('page'); 	
		}else{
		   $this->page = 1;	
		}
		
	    $this->srh_page_per = 20; //每頁筆數	
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists(){

        $srh_data = $this->search($this->input->get());
        
		$data['query'] = $this->order_pre->shr_order_pre($srh_data);		
		
		//----------------------------------------------------------------------
		// 設定分頁
		//----------------------------------------------------------------------
		$query_group_by_ary = $this->order_pre->shr_order_pre_group_by($srh_data);
		
		$pages = new Dio_paginator();
		$pages->set($query_group_by_ary[0]['count'],5,array($this->srh_page_per,3,6,9,12,25,50,100,250,'All'));

		$data['pages'] = $pages;
		$data['query_group_by'] = $query_group_by_ary;
		$data['page_startEnd'] = $pages->get_startEnd_page($this->page ,$query_group_by_ary[0]['count'] ,$this->srh_page_per);

		//檢視view
		$this->load->view('backend/common/header.tpl' ,$data);
		$this->load->view('backend/order_pre/lists.tpl');
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 搜尋
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function search ($data) {
		
		if(isset($data) && !empty($data)){
		   foreach ($data as $key=>$val) {
             if($val == ''){
		      	unset($data[$key]);
             }
           }
		}
		
		$data['srh_page_per'] = $this->srh_page_per;
		$data['srh_limit_page'] = $this->page;
		
		return $data;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢視
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function view(){
		
		$this->form_validation->set_rules('memo_admin','*備註','trim');
		$this->form_validation->set_rules('is_notice','*是否通知','trim|required');
		$this->form_validation->set_rules('status','*狀態','trim|required');
		
		if ($this->form_validation->run() == TRUE){
	
			$data = array(
					'memo_admin'  => $this->input->post('memo_admin'),
					'is_notice'   => $this->input->post('is_notice'),
					'status'      => $this->input->post('status'),
			);
		
			$this->db->where('order_pre_id', $this->input->get('order_pre_id'));
			$this->db->update('order_pre',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
			
			redirect('backend/order_pre/view?'.$_SERVER["QUERY_STRING"]);
	
		}else{
	
			$order_pre_id  = $this->input->get('order_pre_id');
	
			$data['query'] = $this->order_pre->shr_order_pre_info($order_pre_id);
			
			$this->load->view('backend/common/header.tpl' ,$data);
			$this->load->view('backend/order_pre/view.tpl');
		}
	
	}

}


/* End of file order_pre.php */
/* Location: ./application/controllers/backend/center.php */