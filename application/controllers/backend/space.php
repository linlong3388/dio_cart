<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理 [空間案例]的控制器
 * @controllerName space
 * @author Dio
 *
 */
class space extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));
		
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists () {

		$data['slt_status'] = $this->slt_status();
		$data['query'] = $this->db->get('space')->result_array();
		
		//檢視view
		$this->load->view("backend/common/header.tpl",$data);
		$this->load->view("backend/space/lists.tpl");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add () {

		$this->form_validation->set_rules('space_category_id','* 分類ID','trim|required');
		$this->form_validation->set_rules('title','*標題','trim|required');
		$this->form_validation->set_rules('image','*圖片','trim');
		$this->form_validation->set_rules('content','*內容','trim|required');
			
		if ($this->form_validation->run() == TRUE){
			
			$data = array(
			'space_category_id' => $this->input->post('space_category_id'),
					    'title' => $this->input->post('title'),
					    'image' => $this->input->post('image'),
					  'content' => $this->input->post('content'),
                        'cdate' => date('Y-m-d H:i:s') 
			        );

			$this->db->insert('space',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
			
			redirect('backend/space/lists');
			
		} else { 

			$this->load->view("backend/common/header.tpl");
			$this->load->view("backend/space/add.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit () {
		
		$this->form_validation->set_rules('space_category_id','* 分類ID','trim|required');
		$this->form_validation->set_rules('space_id','*編號','trim|required');
		$this->form_validation->set_rules('title','*標題','trim|required');	
		$this->form_validation->set_rules('image','*圖片','trim');
		$this->form_validation->set_rules('image1','*圖片(2)','trim');
		$this->form_validation->set_rules('content','*內容','trim|required');
		$this->form_validation->set_rules('is_show_home','*是否顯示在首頁','trim');
		$this->form_validation->set_rules('status','*狀態','trim|required');
		$this->form_validation->set_rules('date_show','*顯示日期','trim');
		$this->form_validation->set_rules('cdate','*建檔日期','trim|required');
		$this->form_validation->set_rules('meta_title','*Meta標籤標題','trim');
		$this->form_validation->set_rules('meta_description','*Meta標籤描述','trim');
		$this->form_validation->set_rules('meta_keyword','*Meta標籤關鍵字','trim');

		if ($this->form_validation->run() == TRUE){
			
			$data = array(
			 'space_category_id' => $this->input->post('space_category_id'),
					     'title' => $this->input->post('title'),	
				     	 'image' => $this->input->post('image'),
					    'image1' => $this->input->post('image1'),
					   'content' => $this->input->post('content'),
					'is_show_home' => $this->input->post('is_show_home'),
					 'date_show' => $this->input->post('date_show'),
			            'status' => $this->input->post('status'),
					'meta_title'         => $this->input->post('meta_title'),
					'meta_description'   => $this->input->post('meta_description'),
					'meta_keyword'       => $this->input->post('meta_keyword'),
			        );

			$this->db->where('space_id', $this->input->post('space_id'));
			$this->db->update('space',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);

			Redirect('backend/space/edit?space_id='.$this->input->post('space_id'));

		} else { //轉向預設頁面

			$data['query']  = $this->db->get_where('space', array('space_id' => $this->input->get('space_id')) )->row_array();

			//檢視view
			$this->load->view("backend/common/header.tpl",$data);
			$this->load->view("backend//space/edit.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del () {
			
		$this->db->delete('space' , array('space_id' => $this->input->get('space_id')));
		
		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
		
		Redirect('backend/space/lists');
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 下拉式 / 狀態
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function slt_status () {
		
		return array(
		               '0' => '停用' ,
                       '1' => '啟用'
	            );
	}
	
}


/* End of file space.php */
/* Location: ./application/controllers/backend/space.php */