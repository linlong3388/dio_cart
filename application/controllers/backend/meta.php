<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理 [meta管理]的控制器
 * @controllerName meta
 * @author Dio
 *
 */
class meta extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));
		
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists () {

		$data['slt_status'] = $this->slt_status();
		$data['query'] = $this->db->get('meta')->result_array();

		//檢視view
		$this->load->view("backend/common/header.tpl",$data);
		$this->load->view("backend/meta/lists.tpl");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add () {

		$this->form_validation->set_rules('name','*頁面名稱','trim|required');
		$this->form_validation->set_rules('url','*網址','trim|required');
		$this->form_validation->set_rules('meta_title','*Meta標籤標題','trim');
		$this->form_validation->set_rules('meta_description','*Meta標籤描述','trim');
		$this->form_validation->set_rules('meta_keyword','*Meta標籤關鍵字','trim');			
		
		if ($this->form_validation->run() == TRUE){

			//移除網域名
			$url = $this->input->post('url');
			$url = str_ireplace(base_url(), "", $url);
			
			$data = array(
		  			     'name' => $this->input->post('name'),
						  'url' => $url,
					'meta_title'       => $this->input->post('meta_title'),
					'meta_description' => $this->input->post('meta_description'),
					'meta_keyword'     => $this->input->post('meta_keyword'),
					   'cdate' => date('Y-m-d H:i:s')
			        );

			$this->db->insert('meta',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
			
			redirect('backend/meta/lists');
			
		} else { 

			$this->load->view("backend/common/header.tpl");
			$this->load->view("backend/meta/add.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit () {
		
		$this->form_validation->set_rules('name','*頁面名稱','trim|required');
		$this->form_validation->set_rules('url','*網址','trim|required');
		$this->form_validation->set_rules('meta_title','*Meta標籤標題','trim');
		$this->form_validation->set_rules('meta_description','*Meta標籤描述','trim');
		$this->form_validation->set_rules('meta_keyword','*Meta標籤關鍵字','trim');
		$this->form_validation->set_rules('status','*狀態','trim|required');
		
		if ($this->form_validation->run() == TRUE){
			
			//移除網域名
			$url = $this->input->post('url');
			$url = str_ireplace(base_url(), "", $url);

			$data = array(
		  			     'name' => $this->input->post('name'),
						  'url' => $url,
					'meta_title'       => $this->input->post('meta_title'),
					'meta_description' => $this->input->post('meta_description'),
					'meta_keyword'     => $this->input->post('meta_keyword'),
    				'status' => $this->input->post('status')
			        );

			$this->db->where('meta_id', $this->input->post('meta_id'));
			$this->db->update('meta',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);

			Redirect('backend/meta/edit?meta_id='.$this->input->post('meta_id'));

		} else { //轉向預設頁面

			$data['query']  = $this->db->get_where('meta', array('meta_id' => $this->input->get('meta_id')) )->row_array();

			//檢視view
			$this->load->view("backend/common/header.tpl",$data);
			$this->load->view("backend//meta/edit.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del () {
			
		$this->db->delete('meta' , array('meta_id' => $this->input->get('meta_id')));
		
		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
		
		Redirect('backend/meta/lists');
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 下拉式 / 狀態
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function slt_status () {
		
		return array(
		               '0' => '停用' ,
                       '1' => '啟用'
	            );
	}
	
}


/* End of file meta.php */
/* Location: ./application/controllers/backend/meta.php */