<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理[公益計劃]的控制器
 * @controllerName plan
 * @author Dio
 *
 */
class plan extends BackEnd_Controller {
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));
		
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
		
		//載入xml檔，並轉儲為變數
		$this->xml = simplexml_load_file('application/models/xml/plan_layout.xml');
	}
	
   // --------------------------------------------------------------------
	
  /**
   * 方法 : 編輯
   *
   * @access	public
   * @param
   * @return
   */
  public function edit () {
	
       $this->form_validation->set_rules('blockA_title','*標題','trim');
       $this->form_validation->set_rules('blockA_title_sub','*短標','trim');
       $this->form_validation->set_rules('blockA_img','*圖片','trim');
       $this->form_validation->set_rules('blockA_url','*連結','trim');
       $this->form_validation->set_rules('blockA_content','*內容','trim');
       
       $this->form_validation->set_rules('blockB_title','*常見問題/標題','trim');
       $this->form_validation->set_rules('blockB_img','*常見問題/圖片','trim');
       $this->form_validation->set_rules('blockB_url','*常見問題/連結','trim');
       $this->form_validation->set_rules('blockB_content','*常見問題/內容','trim');
       
       $this->form_validation->set_rules('blockC_title','*加入百順/標題','trim');
       $this->form_validation->set_rules('blockC_img','*加入百順/圖片','trim');
       $this->form_validation->set_rules('blockC_url','*加入百順/連結','trim');
       $this->form_validation->set_rules('blockC_content','*加入百順/內容','trim');       
       
       $this->form_validation->set_rules('meta_title','*Meta標籤標題','trim');
       $this->form_validation->set_rules('meta_description','*Meta標籤描述','trim');
       $this->form_validation->set_rules('meta_keyword','*Meta標籤關鍵字','trim');
       
	 if ($this->form_validation->run() == TRUE){
	
	       $this->xml->blockA->title   = $this->input->post('blockA_title');
	       $this->xml->blockA->title_sub   = $this->input->post('blockA_title_sub');
	       $this->xml->blockA->img     = $this->input->post('blockA_img');
	       $this->xml->blockA->url     = $this->input->post('blockA_url');
	       $this->xml->blockA->content = $this->input->post('blockA_content');

	       $this->xml->blockB->title   = $this->input->post('blockB_title');
	       $this->xml->blockB->img     = $this->input->post('blockB_img');
	       $this->xml->blockB->url     = $this->input->post('blockB_url');
	       $this->xml->blockB->content = $this->input->post('blockB_content');
	       
	       $this->xml->blockC->title   = $this->input->post('blockC_title');
	       $this->xml->blockC->img     = $this->input->post('blockC_img');
	       $this->xml->blockC->url     = $this->input->post('blockC_url');
	       $this->xml->blockC->content = $this->input->post('blockC_content');
	       
	       $this->xml->meta->title     = $this->input->post('meta_title');
	       $this->xml->meta->description    = $this->input->post('meta_description');
	       $this->xml->meta->keyword   = $this->input->post('meta_keyword');
	       
	       $this->xml->asXML('application/models/xml/plan_layout.xml');
	       
		   $this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
	
		   redirect('backend/plan/edit');
	
	 } else { 
	
		   $data['xml'] = $this->xml;
										
		   $this->load->view("backend/common/header.tpl",$data);
		   $this->load->view("backend/plan/edit.tpl");
	}
	
  }
	
	
}


/* End of file plan.php */
/* Location: ./application/controllers/plan.php */