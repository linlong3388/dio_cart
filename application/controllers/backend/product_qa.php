<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 -  處理[商品問答]的控制器
 * @controllerName product
 * @author Dio
 *
 */
class product_qa extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));

		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
		
		if($this->input->get('page')){ //頁碼
		   $this->page = $this->input->get('page'); 	
		}else{
		   $this->page = 1;	
		}
		
	    $this->srh_page_per = 20; //每頁筆數	
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 商品問答
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists(){

		$this->db->order_by('product_question_id' ,'DESC');
		$data['query'] = $this->db->get('product_question')->result_array();

		//檢視view
		$this->load->view('backend/common/header.tpl',$data);
		$this->load->view('backend/product/qa/lists.tpl');
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 回覆
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function reply(){

		$this->form_validation->set_rules('product_question_id','*提問編號','trim|required');
		$this->form_validation->set_rules('customer_id','*會員編號','trim|required');
		$this->form_validation->set_rules('question','*內容','trim|required');
		$this->form_validation->set_rules('re_question','*回覆','trim|required');
			
		if ($this->form_validation->run() == TRUE){

			//將POST資料 ,轉為陣列
			$data_post = array(
                         're_question' => $this->input->post('re_question'),
                         'udate'       => date('Y-m-d H:i:s'),
			             'status'      => 2
			);
				
			$this->db->where('product_question_id', $this->input->post('product_question_id'));
			$this->db->update('product_question',$data_post);

			/*
			 if($this->db->affected_rows() > 0){

				$customer_email = get_assign_field('customer', 'customer_id', $this->input->post('customer_id'), 'email');

				//---------------------------------------------------------------------------
				// 回覆成功發送 Email
				//---------------------------------------------------------------------------
				$data_email = array(
				'email_from'  => $_SESSION['vendor_info']['email'],
				'email_to'  => $customer_email,
				'question'  => $this->input->post('question'),
				're_question'  => $data_post['re_question']
				);
					
				$this->load->library(array('Dio_gmail'));
				$order_email = new Dio_gmail();
				$order_email->qa_to_customer_for_product( $this->input->post('customer_id') ,$data_email );
				}*/
				
			dioRedirect('backend/product_qa/lists');
				
		}else{
				
			$this->db->where('product_question_id' ,$this->input->get('product_question_id'));
			$data['query_question'] = $this->db->get('product_question')->row_array();

			//檢視view
			$this->load->view('backend/common/header.tpl' ,$data);
			$this->load->view('backend/product/qa/reply.tpl');
		}

	}
	
}


/* End of file product_qa.php */
/* Location: ./application/controllers/backend/product_qa.php */