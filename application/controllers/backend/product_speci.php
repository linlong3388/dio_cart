<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 -  處理[商品屬性]的控制器
 * 說明 : 與商品主表關聯的屬性(子表)
 * 
 * @controllerName product_speci
 * @author Dio
 *
 */
class product_speci extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session','category'));
		
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
		
		$this->load->model("backend/product_model","model");
		
		$this->cls_category = new category();
		
	}	

	// --------------------------------------------------------------------

	/**
	 * 方法 : 屬性/新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add(){

	  $err_msg = "";
		
	  parse_str($this->input->post('data') ,$post_data);
	  
	  if( !empty($post_data) ){
	  
	  	 if(empty($post_data['product_id'])){
	  		$err_msg = "*商品編號 不可空白!";
	  		echo $err_msg;
	  		return false;
	  	 }
	  	 
	  	 if( !$this->is_sku2( $post_data['sku2'] )){
	  		$err_msg = "*子sku 不可空白或重複!";
	  		echo $err_msg;
	  		return false;
	  	 }
	  	 
	  	 $mutil_key = $this->is_mutil_key( $post_data['sku'] ,$post_data['property_id_color'] ,$post_data['property_id_size'] );
	  	 
	  	 if( !empty($mutil_key) ){
	  	 	$err_msg = $mutil_key;
	  	 	echo $err_msg;
	  	 	return false;
	  	 }
	  
	  	 
	  	 if(empty($post_data['stock'])){
	  		$err_msg = "*庫存 不可空白!";
	  		echo $err_msg;
	  		return false;
	  	 }
	  	
	  	 $data = array(
	  			'product_id'        => $post_data['product_id'],
	  			'sku'               => $post_data['sku'],
	  			'sku2'              => $post_data['sku2'],
	  			'property_id_size'  => $post_data['property_id_size'],
	  			'property_id_color' => $post_data['property_id_color'],
	  			'stock'             => $post_data['stock'],
	  			//'image'             => $post_data['image'],
	  			'cdate'             => date('Y-m-d H:i:s')
	  	 );
	  	 
	  	 $this->db->insert('product_speci' ,$data);	  
   
	  }else{

	  	$this->load->view('backend/common/header.tpl');
	  	$this->load->view('backend/product_speci/add.tpl');	  	
	  }	 
	  	 
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 屬性/編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit(){
	
		$sku2 = $this->input->post('sku2');
		
		if ( !empty($sku2) ){
			
			$data = array(
					'property_id_size'  => $this->input->post('property_id_size'),
					'property_id_color' => $this->input->post('property_id_color'),
					'stock'             => $this->input->post('stock'),
					'image'             => $this->input->post('image')
			);
			
			//更新商品副表
			$this->db->where('sku2', $this->input->post('sku2'));
			$this->db->update('product_speci',$data);
	
			redirect('backend/product/edit?product_id='.$this->input->post('product_id'));
	
		} else {
			
			
	
			$this->db->where('sku2' ,$this->input->get('sku2'));
			$data['query'] = $this->db->get('product_speci')->row_array();
				
			//檢視view
			$this->load->view('backend/common/header.tpl' ,$data);
			$this->load->view('backend/product_speci/edit.tpl');
		}
			
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 屬性/刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del(){

			$this->db->where('product_speci_id' ,$this->input->get('product_speci_id') );
			$this->db->delete('product_speci');
	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 圖片/編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	/*
	public function img_edit(){
	
		$product_speci_id = $this->input->get('product_speci_id');
		
		//主表
		$this->db->where('product_speci_id' ,$product_speci_id);
		$data['query'] = $this->db->get('product_speci')->row_array();
		
		//圖片列表
		$this->db->where('product_speci_id' ,$product_speci_id);
		$data['query_list'] = $this->db->get('product_speci_image')->result_array();
	
		//檢視view
		$this->load->view('backend/common/header.tpl' ,$data);
		$this->load->view('backend/product_speci/img_edit.tpl');			
	}*/
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 驗證 sku2 是否重複
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function is_sku2( $sku2 ){
	
		if( empty($sku2) ) {
			return false;
		}
		
		$this->db->where('sku2' ,$sku2);
		$query = $this->db->get('product_speci')->row_array();
	             
		if( empty($query) ) return true;
		else return false;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 驗證 [sku] ,[顏色] ,[尺寸] 是否重複
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function is_mutil_key( $sku, $property_id_color ,$property_id_size ){
	
		$err_msg = "";
		
		if( empty($sku) ){
			$err_msg = "*sku 不可空白!";
		}
		
		if( empty($property_id_color) ){
			$err_msg = "*屬性/顏色  不可空白!";
		}
		 
		if( empty($property_id_size)){
			$err_msg = "*屬性/尺寸 不可空白!";
		}
				
		$this->db->where('sku' ,$sku);
		$this->db->where('property_id_color' ,$property_id_color);
		$this->db->where('property_id_size' ,$property_id_size);
		
		$query = $this->db->get('product_speci')->row_array();
		
		if( !empty($query) ) {
			$err_msg = "*sku/顏色/尺寸 三項目不可重複!";
		}
		
		return $err_msg;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 :圖片/新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add_img(){
	
		$this->form_validation->set_rules('product_speci_id','* 屬性編號','trim|required');
		$this->form_validation->set_rules('sku2','* sku子項','trim|required');
		$this->form_validation->set_rules('image','* 圖片','trim|required');
		$this->form_validation->set_rules('image_alt','* 圖片說明','trim');
		
		if ($this->form_validation->run() == TRUE){
			
			$data = array(
				   'product_speci_id'  => $this->input->post('product_speci_id'),
				   'image'             => $this->input->post('image'),
				   'image_alt'         => $this->input->post('image_alt'),
				   'cdate'             => date('Y-m-d H:i:s')
			);
			
			
			$this->db->insert('product_speci_image',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);			
			
			redirect('backend/product_speci/add_img?product_speci_id='.$this->input->post('product_speci_id'));
			
		}else{
			
			$product_speci_id = $this->input->get('product_speci_id');
			
			//主表
			$this->db->where('product_speci_id' ,$product_speci_id);
			$data['query'] = $this->db->get('product_speci')->row_array();
			
			//圖片列表
			$this->db->where('product_speci_id' ,$product_speci_id);
			$data['query_list'] = $this->db->get('product_speci_image')->result_array();
			
			//檢視view
			$this->load->view('backend/common/header.tpl' ,$data);
			$this->load->view('backend/product_speci/add_img.tpl');
		}
	
	}	
}


/* End of file product.php */
/* Location: ./application/controllers/backend/product.php */