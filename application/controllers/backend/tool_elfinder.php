<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理[文件管理器]的控制器
 * @controllerName elfinder
 * @author Dio
 *
 */
class tool_elfinder extends BackEnd_Controller {
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();
	
		$this->load->database();
		$this->load->helper(array('form','cookie','path','file','base','url','elfinder'));
		$this->load->library(array('form_validation','session'));
	}

   // --------------------------------------------------------------------
	
   /**
	* 方法 : 主頁
    *
	* @access	public
	* @param
	* @return
	*/
	public function index () {
		
		$this->load->view("backend/common/header.tpl");
		$this->load->view("backend/tool_elfinder/index.tpl");
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 啟動
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function init() {
	
		$opts = array(
				'bind' => array(
						'duplicate upload rm paste' => 'elfinder_upload_rename'
				),
		
				'roots' => array(
						array(
								'driver'  => 'LocalFileSystem',
								'path'    => set_realpath('./resources/uploads/'),
								'URL'     => site_url('resources/uploads/'),
								'uploadMaxSize' => '5M' ,
								'accessControl' => 'elfinder_access',
								'uploadAllow' => array('image'),
								'attributes'  => array(
										array(
												'pattern' => '/.*/',
												'read'  => true,
												'write' => true
										)
								),
						)
				)
		);
		
		$this->load->library('elfinder_cls', $opts);
				
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 啟動兩個root
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function init_two_root() {
	
		$opts = array(
				'bind' => array(
						//'duplicate upload rm paste' => 'elfinder_upload_rename' ,
						'upload.presave' => array(
								'Plugin.AutoResize.onUpLoadPreSave'
						)
	
				),
	
				'roots' => array(
						array(
								'driver'  => 'LocalFileSystem',
								'path'    => set_realpath('./resources/uploads/'),
								'URL'     => site_url('resources/uploads/'),
								'uploadMaxSize' => '5M' ,
								'accessControl' => 'elfinder_access',
								'uploadAllow' => array('image'),
								'attributes'  => array(
										array(
												'pattern' => '/.*/',
												'read'  => true,
												'write' => true
										)
								),
						),
						array(
								'driver' => 'LocalFileSystem',
								'path'   => set_realpath('./resources/thumb/'),
								'URL'    => site_url('resources/thumb/'),
								'uploadMaxSize' => '1M' ,
								'plugin' => array(
										'AutoResize' => array(
												'enable'         => true,       // For control by volume driver
												'maxWidth'       => 300,       // Path to Water mark image
												'maxHeight'      => 300,       // Margin right pixel
												'quality'        => 70,        // JPEG image save quality
												'preserveExif'   => false,
												'targetType'     => IMG_GIF|IMG_JPG|IMG_PNG|IMG_WBMP
										)
								)
						)
				)
	
	
		);
		
		$this->load->library('elfinder_cls', $opts);		
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢視 /iframe
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function iframe () {
	
		$this->load->view("backend/tool_elfinder/iframe.tpl");
	}
	
	// --------------------------------------------------------------------
	
	/**
 	 * 方法 : 檢視 /dialog
 	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function dialog () {
		
		$this->load->view("backend/tool_elfinder/dialog.tpl");
	}
	
}


/* End of file elfinder.php */
/* Location: ./application/controllers/elfinder.php */