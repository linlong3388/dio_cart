<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理[會員優惠券紀錄]的控制器
 * @controllerName customer_coupon
 * @author Dio
 *
 */
class customer_coupon extends BackEnd_Controller {
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->library(array('form_validation','session','Dio_paginator','promo/VIP'));
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		
		$this->load->model("backend/order_model","order");
		$this->load->model("backend/customer_model" ,"customer");
		
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
		
		if($this->input->get('page')){ //頁碼
			$this->page = $this->input->get('page');
		}else{
			$this->page = 1;
		}
		
		$this->srh_page_per = 20; //每頁筆數
		
		//會員VIP
		$this->vip = new VIP();

		$this->customer_id = $this->input->get_post('customer_id');
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists () {

		//會員資料
		$this->db->where('customer_id' ,$this->customer_id);
		$data['query'] = $this->db->get('customer')->row_array();
		
		//優惠券資料
		$sql = "SELECT cc.* ,o.order_show_id
		          FROM customer_coupon cc
		         LEFT JOIN `order` o ON cc.order_id = o.order_id
				WHERE cc.customer_id = '".$this->customer_id."'
				ORDER BY customer_coupon_id DESC";
		
		$data['query_coupon'] = $this->db->query($sql)->result_array();
		
		//檢視view
		$this->load->view("backend/common/header.tpl" ,$data);
		$this->load->view("backend/customer_coupon/lists.tpl");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 搜尋
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function search ($data) {
		
		if(isset($data) && !empty($data)){
		   foreach ($data as $key=>$val) {
             if($val == ''){
		      	unset($data[$key]);
             }
           }
		}
		
		$data['srh_page_per'] = $this->srh_page_per;
		$data['srh_limit_page'] = $this->page;

		return $data;
	}
	

}


/* End of file customer_coupon.php */
/* Location: ./application/controllers/backend/customer_coupon.php */