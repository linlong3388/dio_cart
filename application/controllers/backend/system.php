<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理[系統]的控制器
 * @controllerName system
 * @author Dio
 *
 */
class system extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));
		
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}

		//載入model
		$this->load->model("backend/dashboard_model","model");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 系統設定
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function main () {
		
		$this->form_validation->set_rules('is_enabled','*系統維護中','trim|required');
		$this->form_validation->set_rules('is_time_arrival','*預計到貨時段','trim|required');
		$this->form_validation->set_rules('web_name','*網站名稱','trim');
		$this->form_validation->set_rules('web_url','*網址','trim');
		$this->form_validation->set_rules('web_email','*管理員信箱','trim');
		$this->form_validation->set_rules('web_phone','*電話','trim');
		$this->form_validation->set_rules('web_fax','*傳真','trim');
		$this->form_validation->set_rules('web_addr','*地址','trim');
		$this->form_validation->set_rules('shipping_fee','*運費設定','trim');
		$this->form_validation->set_rules('shipping_fee_full','*免運額','trim');
		$this->form_validation->set_rules('logo','*Logo','trim');
		$this->form_validation->set_rules('favicon','*Favicon','trim');
		$this->form_validation->set_rules('pay_method_atm','*付款方式描述(ATM)','trim');
		$this->form_validation->set_rules('pay_method_cod','*付款方式描述(貨到付款)','trim');
		$this->form_validation->set_rules('ga','*google分析','trim');
		$this->form_validation->set_rules('rule_flow','*客製化流程','trim');
		$this->form_validation->set_rules('rule_register','*會員服務條款','trim');
		$this->form_validation->set_rules('rule_shipping_fee','*購物須知','trim');
		$this->form_validation->set_rules('rule_coupon','*優惠券須知','trim');
		$this->form_validation->set_rules('trans_method1','*運送服務說明','trim');
		$this->form_validation->set_rules('trans_method2','*門市店取說明','trim');
		
		$this->form_validation->set_rules('content_header','*自訂header','trim');
		$this->form_validation->set_rules('content_body','*自訂body','trim');
		
		$this->form_validation->set_rules('cvs_name','*超商取貨/寄件人姓名','trim|required');
		$this->form_validation->set_rules('cvs_phone','*超商取貨/寄件人電話','trim');
		$this->form_validation->set_rules('cvs_mobile','*超商取貨/寄件人手機','callback_check_mobile');
		$this->form_validation->set_rules('cvs_memo','*超商取貨/寄件人備註','trim');
		
		$this->form_validation->set_rules('meta_title','*Meta標籤標題','trim');
		$this->form_validation->set_rules('meta_description','*Meta標籤描述','trim');
		$this->form_validation->set_rules('meta_keyword','*Meta標籤關鍵字','trim');		
		
		if ($this->form_validation->run() == TRUE){

			$data = array(
                         'is_enabled' => $this->input->post('is_enabled'),
					'is_time_arrival' => $this->input->post('is_time_arrival'),
					       'web_name' => $this->input->post('web_name'),
					        'web_url' => $this->input->post('web_url'),
					      'web_email' => $this->input->post('web_email'),
					      'web_phone' => $this->input->post('web_phone'),
					        'web_fax' => $this->input->post('web_fax'),
					       'web_addr' => $this->input->post('web_addr'),
			  		   'shipping_fee' => $this->input->post('shipping_fee'),
				  'shipping_fee_full' => $this->input->post('shipping_fee_full'),
					           'logo' => $this->input->post('logo'),
					        'favicon' => $this->input->post('favicon'),
					 'pay_method_atm' => $this->input->post('pay_method_atm'),
					 'pay_method_cod' => $this->input->post('pay_method_cod'),
					             'ga' => $this->input->post('ga'),
					      'rule_flow' => $this->input->post('rule_flow'),
					  'rule_register' => $this->input->post('rule_register'),
				  'rule_shipping_fee' => $this->input->post('rule_shipping_fee'),
				    	'rule_coupon' => $this->input->post('rule_coupon'),
					  'trans_method1' => $this->input->post('trans_method1'),
					  'trans_method2' => $this->input->post('trans_method2'),
					
					'content_header'  => $this->input->post('content_header'),
					'content_body'    => $this->input->post('content_body'),					
					
					'cvs_name'        => $this->input->post('cvs_name'),
					'cvs_phone'       => $this->input->post('cvs_phone'),
					'cvs_mobile'      => $this->input->post('cvs_mobile'),
					'cvs_memo'        => $this->input->post('cvs_memo'),
					
					     'meta_title' => $this->input->post('meta_title'),
				   'meta_description' => $this->input->post('meta_description'),
					   'meta_keyword' => $this->input->post('meta_keyword'),
		                      'udate' => date('Y-m-d H:i:s')					
		         	);

			$this->db->where('system_id', '1');
			$this->db->update('system',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
		
			Redirect('backend/system/main');

		} else { //轉向預設頁面

			$data['query']  = $this->db->get_where('system', array('system_id' => '1') )->row_array();
			
			//檢視view
			$this->load->view("backend/common/header.tpl" ,$data);
			$this->load->view("backend/system/main.tpl");
		}

	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 驗證手機
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function check_mobile($mobile){
	
		$mobile = trim($mobile);
	
		if( empty($mobile) ){
			$this->form_validation->set_message('check_mobile', '*手機不可空白!');
			return FALSE;
		}
	
		if( strlen($mobile) != 10 ) {
			$this->form_validation->set_message('check_mobile', '*手機必須為10碼!');
			return FALSE;
		}
	
		if( !is_numeric($mobile) ){
			$this->form_validation->set_message('check_mobile', '*手機必須為數字!');
			return FALSE;
		}
	
		if(  substr($mobile ,0,2) != '09' ){
			$this->form_validation->set_message('check_mobile', '*手機前兩碼必須為"09"開頭!');
			return FALSE;
		}
	
		return TRUE;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 資料庫備份
	 *       需考量一般 share hosting 不支援命令列函數，所以必須掛在 vps 等級的主
	 *       機才能使用。
	 *       
	 *       匯出若為空白資料，可能是OS本身的路徑問題，可直接丟到 Linux 主機進行測
	 *       試!
	 *       
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function backup () {
		
		if( $this->input->post('download_db') ){
				
			//---------------------------------------------------
			// 資料庫匯出
			//---------------------------------------------------
			$filename = "backup-" . date("Y-m-d") . ".sql";
			$mime = "application/x-gzip";
			
			header( "Content-Type: " . $mime );
			header( 'Content-Disposition: attachment; filename="' . $filename . '"' );
			
			$cmd = "mysqldump -u".DIO_DBUSER." -p".DIO_DBPASS." ".DIO_DBNAME." --default-character-set=utf8 | gzip --best";
			
			passthru( $cmd );
			
			exit;

		}else{

			$data['query']  = $this->db->get_where('system', array('system_id' => '1') )->row_array();
			
			//檢視view
			$this->load->view("backend/common/header.tpl" ,$data);
			$this->load->view("backend/system/backup.tpl");
		}
	
	}

}


/* End of file system.php */
/* Location: ./application/controllers/backend/system.php */