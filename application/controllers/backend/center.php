<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 後台管理首頁
 * @controllerName center
 * @author Dio
 *
 */
class center extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid','elfinder'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));
		
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
		
		//載入model
		$this->load->model("backend/dashboard_model","model");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 電子郵件設定
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function sys_email () {
		dioRedirect('backend/index/main' ,'*建構中，需提供電子郵件相關資料才能進行設置!');
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 管理員設定 / 權限
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function admin_role () {
	
		$this->form_validation->set_rules('admin_id','*','trim|required');

		if ($this->form_validation->run() == TRUE){
				
			//將POST資料 ,轉為陣列
			$data = array(
                      'admin_id' => $this->input->post('admin_id'),
                           'tb_name' => $this->input->post('tb_name')
			);
				
			//將該會員原先的功能權限歸0
			$this->db->where('admin_id' ,$data['admin_id']);
			$this->db->update('ct_role_tables' ,array('status' => 0));

			//依所選取的項目重新設定
			foreach ($data['tb_name'] as $key=>$val){
				//INSERT 或 UPDATE
				$sql = "INSERT INTO `ct_role_tables` (`admin_id`,`tb_name`,`status`,`cdate`) VALUES ('".$data['admin_id']."','".$val."', 1, '".date('Y-m-d H:i:s')."')
                          ON DUPLICATE KEY UPDATE `status`=1, `cdate`='".date('Y-m-d H:i:s')."'";
				 
				$this->db->query($sql);
			}
			 
			dioRedirect('backend/center/admin_list');

		}else{ //轉向預設頁面
			 
			//取得所有功能表
			$this->db->where('admin_id' ,$this->input->get('admin_id'));
			$this->db->where('status' ,'1');
			$query_tables = $this->db->get('ct_role_tables')->result_array();
			 
			$tb_name = array();
			 
			foreach ( $query_tables as $row) {
				array_push($tb_name, $row['tb_name']);
			}
			 
			//取得管理員的資料
			$this->db->where('admin_id' ,$this->input->get('admin_id'));
			$data['query'] = $this->db->get('admin')->row_array();
			$data['query_tables'] = $tb_name;
			 
			//檢視view
			$this->load->view("backend/common/header.tpl" ,$data);
			$this->load->view("backend/admin_role.tpl");
		}
			
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 管理員設定 / 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function admin_delete () {
	
		//刪除管理員
		$this->db->delete('admin' , array('admin_id' => $this->input->get('admin_id')));

		//刪除權限表
		$this->db->delete('ct_role_tables' ,array('admin_id' => $this->input->get('admin_id')) );

		dioRedirect('backend/center/admin_list');
	}

}


/* End of file center.php */
/* Location: ./application/controllers/center.php */