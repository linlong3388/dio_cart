<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理[首頁廣告]的控制器
 * @controllerName ad_home
 * @author Dio
 *
 */
class ad_home extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid','elfinder'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));

		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
		
		//載入model
		$this->load->model("backend/dashboard_model","model");
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 首頁廣告 / 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists () {
		
		$data['query'] = $this->db->get('ad_home')->result_array();

		//檢視view
		$this->load->view("backend/common/header.tpl",$data);
		$this->load->view("backend/ad_home/lists.tpl");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 首頁廣告 / 新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add () {

		$this->form_validation->set_rules('type','*型態','trim|required');
		$this->form_validation->set_rules('title','*標題','trim');
		$this->form_validation->set_rules('description','*簡述','trim');
		$this->form_validation->set_rules('url','*URL','trim');

		if ($this->form_validation->run() == TRUE){
				
			$data = array(
                         'type' => $this->input->post('type'),
		             	'title' => $this->input->post('title'),
			      'description' => $this->input->post('description'),
                          'url' => $this->input->post('url'),
                        'image' => $this->input->post('image'),
                        'cdate' => date('Y-m-d H:i:s') 
			);

			$this->db->insert('ad_home',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
				
			redirect('backend/ad_home/lists');			
		
		} else { //轉向預設頁面

			//檢視view
			$this->load->view("backend/common/header.tpl");
			$this->load->view("backend/ad_home/add.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 首頁廣告 / 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit () {
		
		$this->form_validation->set_rules('ad_home_id','*編號','trim|required');
		$this->form_validation->set_rules('type','*類型','trim|required');
		$this->form_validation->set_rules('title','*標題','trim');
		$this->form_validation->set_rules('description','*簡述','trim');
		$this->form_validation->set_rules('url','*URL','trim');
		$this->form_validation->set_rules('sort_order','*排序','trim|required');
		$this->form_validation->set_rules('status','*狀態','trim|required');
		$this->form_validation->set_rules('cdate','建立日期','trim');

		if ($this->form_validation->run() == TRUE){

			$data = array(
                         'type' => $this->input->post('type'),
		             	'title' => $this->input->post('title'),
			      'description' => $this->input->post('description'),			
                          'url' => $this->input->post('url'), 
                        'image' => $this->input->post('image'),
				   'sort_order' => $this->input->post('sort_order'),
                       'status' => $this->input->post('status')
			);

			$this->db->where('ad_home_id', $this->input->post('ad_home_id'));
			$this->db->update('ad_home',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
			
			redirect('backend/ad_home/edit?ad_home_id='.$this->input->post('ad_home_id'));			
		
		} else { //轉向預設頁面

			$data['query']  = $this->db->get_where('ad_home', array('ad_home_id' => $this->input->get('ad_home_id')) )->row_array();

			//檢視view
			$this->load->view("backend/common/header.tpl" ,$data);
			$this->load->view("backend/ad_home/edit.tpl");

		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 首頁廣告 / 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del () {
		
		$this->db->delete('ad_home' , array('ad_home_id' => $this->input->get('ad_home_id')));

		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
		
		redirect('backend/ad_home/lists');		
	}

}


/* End of file ad_home.php */
/* Location: ./application/controllers/backend/ad_home.php */