<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理[會員]的控制器
 * @controllerName customer
 * @author Dio
 *
 */
class customer extends BackEnd_Controller {
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->library(array('form_validation','session','Dio_paginator','promo/VIP'));
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		
		$this->load->model("backend/customer_model" ,"customer");
		
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
		
		//會員VIP
		$this->vip = new VIP();

		$this->customer_id = $this->input->get_post('customer_id');
		
		$this->initial();
	}

	// --------------------------------------------------------------------
		
	/**
	 * 參數初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	private function initial(){
		
		//分頁
		$this->page = $this->input->get('page');
		$this->srh_page_per = 20;
		
		//排序欄位
		$this->sort_by = array(
				               '' => '--' ,
				    //'customer_id' => '會員編號' ,
				          'email' => '帳號 /email' ,
				     //'first_name' => '姓' ,
				      'last_name' => '姓名' ,
				         'status' => '狀態' ,
				          'cdate' => '建立日期'
		                 );
		
	}
		
	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists () {
		
		if( $this->input->get('submit_output') ){
		
			$this->output( $this->search($this->input->get()) );
		
		}else{
			
			//VIP會員驗證
			$this->vip->downCronJob();
			
			$srh_data = $this->search($this->input->get());
			
			$data['query']     = $this->customer->shr_customer($srh_data);
			$data['data_sort'] = $this->sort_by;
			
			//----------------------------------------------------------------------
			// 設定分頁
			//----------------------------------------------------------------------
			$query_group_by_ary = $this->customer->shr_customer_group_by($srh_data);
			
			$pages = new Dio_paginator();
			$pages->set($query_group_by_ary[0]['count'],5,array($this->srh_page_per,3,6,9,12,25,50,100,250,'All'));
			
			$data['pages'] = $pages;
			$data['query_group_by'] = $query_group_by_ary;
			$data['page_startEnd'] = $pages->get_startEnd_page($this->page ,$query_group_by_ary[0]['count'] ,$this->srh_page_per);
			
			//檢視view
			$this->load->view("backend/common/header.tpl" ,$data);
			$this->load->view("backend/customer/lists.tpl");
		}

	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 訂單資料 / 匯出(Excel)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function output ($srh_data) {
	
		//下載用檔頭
		header('Content-type: application/vnd.ms-excel');
		header("Content-Disposition: attachment; filename=CustomerExport_".date('Ymd').".xls");
	
		$this->load->library(array('PHPExcel/Classes/PHPExcel.php'));
			
		$data['query']     = $this->customer->output_xls($srh_data);
		
		$objPHPExcel = new PHPExcel();
	
		//設定檔案資訊
		$objPHPExcel->getProperties()->setCreator("Dio")
		            ->setLastModifiedBy("Dio")
		            ->setTitle("chenyunpaochuan")
		            ->setSubject("Customer export")
		            ->setDescription("--")
		            ->setKeywords("--")
		            ->setCategory("--");
	
		//設定表格間距寬度
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);		
	
		//設定樣式
		$objPHPExcel->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
	
		//設定欄位抬頭
		$objPHPExcel->setActiveSheetIndex(0)
		            ->setCellValue('A1', "會員ID")
		            ->setCellValue('B1', "帳號")
		            ->setCellValue('C1', "姓名")
		            ->setCellValue('D1', "電話")
		            ->setCellValue('E1', "Email")
		            ->setCellValue('F1', "郵遞區號")
		            ->setCellValue('G1', "地址")
		            ->setCellValue('H1', "會員狀態")
		            ->setCellValue('I1', "管理員備註")
		            ->setCellValue('J1', "建立日期");
	
		$row_id = 2;
	
		//資料筆數
		for($i=0 ; $i<( COUNT($data['query']) ) ; $i++){
			
			$objPHPExcel->setActiveSheetIndex(0)
			            ->setCellValueExplicit('A'.$row_id, $data['query'][$i]['customer_id'], PHPExcel_Cell_DataType::TYPE_STRING)
		 	            ->setCellValue('B'.$row_id, $data['query'][$i]['username'])
			            ->setCellValue('C'.$row_id, $data['query'][$i]['last_name'])
			            ->setCellValueExplicit('D'.$row_id, $data['query'][$i]['phone'], PHPExcel_Cell_DataType::TYPE_STRING)
			            ->setCellValue('E'.$row_id, $data['query'][$i]['email'])
			            ->setCellValue('F'.$row_id, $data['query'][$i]['local'])
			            ->setCellValue('G'.$row_id, $data['query'][$i]['address'])
			            ->setCellValue('H'.$row_id, $data['query'][$i]['status'])
		  	            ->setCellValue('I'.$row_id, $data['query'][$i]['memo_admin'])
			            ->setCellValue('J'.$row_id, $data['query'][$i]['cdate']);
	
			$row_id++;
		}
	
		//工作區命名
		$objPHPExcel->getActiveSheet()->setTitle('Worksheet1');
	
		//將檔案寫入暫存區，供下載用
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	
		$objPHPExcel->disconnectWorksheets();
		unset($objPHPExcel);
	}
	
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 搜尋
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function search ($data) {
		
		if(isset($data) && !empty($data)){
		   foreach ($data as $key=>$val) {
             if($val == ''){
		      	unset($data[$key]);
             }
           }
		}
		
		$data['srh_page_per'] = $this->srh_page_per;
		$data['srh_limit_page'] = $this->page;

		return $data;
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit () {
		
		$this->form_validation->set_rules('customer_id','*會員編號','trim|required');
		//$this->form_validation->set_rules('mobile','*手機','trim|required');
		$this->form_validation->set_rules('email','*電子郵件','trim|required');
		//$this->form_validation->set_rules('birthday_year','*生日(年)','trim|required');
		//$this->form_validation->set_rules('birthday_month','*生日(月)','trim|required');
		//$this->form_validation->set_rules('birthday_day','*生日(日)','trim|required');
		//$this->form_validation->set_rules('level','*會員等級','trim|required');
		$this->form_validation->set_rules('memo_admin','*管理員備註','trim');
		$this->form_validation->set_rules('status','*會員狀態','trim|required');

		if ($this->form_validation->run() == TRUE){
			
			$data = array(
				  'memo_admin' => $this->input->post('memo_admin'),
                       'level' => $this->input->post('level'),
                      'status' => $this->input->post('status'),
                       'udate' => date('Y-m-d H:i:s') 
			);

			$this->db->where('customer_id', $this->customer_id);
			$this->db->update('customer',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
			
			redirect('backend/customer/edit?'.$_SERVER["QUERY_STRING"]);

		} else { //轉向預設頁面

			$data['query']       = $this->db->get_where('customer', array('customer_id' => $this->customer_id ) )->row_array();
			$data['query_addr']  = $this->getCustomerAddr();
			$data['query_store'] = $this->getCustomerStore();
			
			//檢視view
			$this->load->view("backend/common/header.tpl" ,$data);
			$this->load->view("backend/customer/edit.tpl");
		}

	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得會員地址資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getCustomerAddr () {
		
		$this->db->where('customer_id', $this->customer_id);
		$this->db->order_by('sort_order' ,'DESC');
		
		return $this->db->get('customer_addr')->result_array();
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得會員超商資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getCustomerStore() {
	
		$this->db->where('customer_id' ,$this->customer_id);
		$this->db->order_by('sort_order' ,'DESC');
		
		return $this->db->get('customer_store')->result_array();
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯密碼
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function pass () {
		
		$this->form_validation->set_rules('email', '*會員帳號', 'trim|required');
		$this->form_validation->set_rules('password', '*更改密碼', 'trim|required|matches[passconf]|md5');
		$this->form_validation->set_rules('passconf', '*密碼確認', 'trim|required');

		if ($this->form_validation->run() == TRUE){
			
			$data = array(
                         'password' => $this->input->post('password')
			);

			$this->db->where('email', $this->input->post('email'));
			$this->db->update('customer',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
			
			redirect('backend/customer/pass?'.$_SERVER['QUERY_STRING']);

		}else{

			$data['query']  = $this->db->get_where('customer', array('customer_id' => $this->customer_id ) )->row_array();

			//檢視view
			$this->load->view("backend/common/header.tpl" ,$data);
			$this->load->view("backend/customer/pass.tpl");
		}

	}

}


/* End of file customer.php */
/* Location: ./application/controllers/backend/customer.php */