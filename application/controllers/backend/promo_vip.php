<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 後台管理首頁  for 行銷
 * @controllerName promo_vip
 * @author Dio
 *
 */
class promo_vip extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session','category'));
	
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
		
		//建立商品分類物件
		$this->cls_category = new category();
		
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists () {

		$data['query'] = $this->db->get('promo_vip')->result_array();

		//檢視view
		$this->load->view("backend/common/header.tpl" ,$data);
		$this->load->view("backend/promo_vip/lists.tpl");
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add () {

		$this->form_validation->set_rules('name','*名稱','trim|required');
		$this->form_validation->set_rules('code','*代碼','trim|required');
		$this->form_validation->set_rules('code_type','*類型','trim|required');		
		$this->form_validation->set_rules('discount','*折扣','trim|required');
		$this->form_validation->set_rules('total','*總金額','trim|required');
		$this->form_validation->set_rules('use_count','*使用次數','trim');
		$this->form_validation->set_rules('sdate','*開始日期','trim|required');
		$this->form_validation->set_rules('edate','*結束日期','trim|required');
		
		if ($this->form_validation->run() == TRUE){

			$data = array(
                      'name' => $this->input->post('name'),
			          'code' => $this->input->post('code'),
			     'code_type' => $this->input->post('code_type'), 
				  'discount' => $this->input->post('discount'),
					 'total' => $this->input->post('total'),
				 'use_count' => $this->input->post('use_count'),
			         'sdate' => $this->input->post('sdate'),
			         'edate' => $this->input->post('edate'),
                     'cdate' => date('Y-m-d H:i:s') );

			$this->db->insert('promo_vip_id',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
				
			redirect('backend/promo_vip/lists');
			
		} else { 
			
			$this->load->view("backend/common/header.tpl");
			$this->load->view("backend/promo_vip/add.tpl");
		}

	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit () {
		
		$promo_vip_id = 1;
		
		$this->form_validation->set_rules('discount','*折扣','trim|required');
		$this->form_validation->set_rules('total','*滿額金額','trim|required');
		$this->form_validation->set_rules('ctotal','*續約金額','trim|required');
		$this->form_validation->set_rules('sdate','*開始日期','trim|required');
		$this->form_validation->set_rules('edate','*結束日期','trim|required');
		//$this->form_validation->set_rules('status','*狀態','trim|required');
		
		if ($this->form_validation->run() == TRUE){
			
			//將POST資料 ,轉為陣列
			$data = array(
    		  	  'discount' => $this->input->post('discount'),
					 'total' => $this->input->post('total'),
					'ctotal' => $this->input->post('ctotal'),
					 'sdate' => $this->input->post('sdate'),
			         'edate' => $this->input->post('edate')
			        //'status' => $this->input->post('status')
			        );
			
			$this->db->where('promo_vip_id', $promo_vip_id);
			$this->db->update('promo_vip',$data);

			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
			
			Redirect('backend/promo_vip/edit?promo_vip_id='.$promo_vip_id);
			
		} else { 
			
			$data['query']  = $this->db->get_where('promo_vip', array('promo_vip_id' => $promo_vip_id) )->row_array();
			
			$this->load->view("backend/common/header.tpl",$data);
			$this->load->view("backend/promo_vip/edit.tpl");
		}

	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del () {
		
		$this->db->delete('promo_vip_id' , array('promo_vip_id' => $this->input->get('promo_vip_id')));
		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
		
		Redirect('backend/promo_vip/lists');
	}
	
}

/* End of file promo_vip.php */
/* Location: ./application/controllers/promo_vip.php */