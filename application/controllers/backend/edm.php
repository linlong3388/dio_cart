<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理 [電子報(EDM)]的控制器
 * @controllerName edm
 * @author Dio
 *
 */
class edm extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session','Dio_email'));
		
		$this->dio_email = new Dio_email();
		
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists () {

		$data['slt_status'] = $this->slt_status();
		$data['query'] = $this->db->get('edm')->result_array();

		//檢視view
		$this->load->view("backend/common/header.tpl",$data);
		$this->load->view("backend/edm/lists.tpl");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add () {

		$this->form_validation->set_rules('email_from','*寄件人','trim|required');
		$this->form_validation->set_rules('title','*主旨','trim|required');
		$this->form_validation->set_rules('content','*內容','trim|required');
			
		if ($this->form_validation->run() == TRUE){

			$this->db->where('is_edm' ,1);
			$this->db->where('status' ,1);
			$query = $this->db->get('customer')->result_array();
			
			$data = array(
					'email_from' => $this->input->post('email_from'),
					  'email_to' => $query,
				    	 'title' => $this->input->post('title'),
					   'content' => $this->input->post('content'),
                         'cdate' => date('Y-m-d H:i:s') 
			        );

			$this->dio_email->edm($data);
			
			$this->session->set_flashdata('msg','您已成功寄送Email!');
			
			redirect('backend/edm/add');
			
		} else { 

			$this->load->view("backend/common/header.tpl");
			$this->load->view("backend/edm/add.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit () {
		
		$this->form_validation->set_rules('edm_category_id','* 分類ID','trim|required');
		$this->form_validation->set_rules('edm_id','*編號','trim|required');
		$this->form_validation->set_rules('title','*標題','trim|required');
		$this->form_validation->set_rules('title_en','*標題(英)','trim');
		$this->form_validation->set_rules('title_jp','*標題(日)','trim');		
		$this->form_validation->set_rules('content','*內容','trim|required');
		$this->form_validation->set_rules('content_en','*內容(英)','trim');
		$this->form_validation->set_rules('content_jp','*內容(日)','trim');
		$this->form_validation->set_rules('status','*狀態','trim|required');
		$this->form_validation->set_rules('cdate','*建檔日期','trim|required');

		if ($this->form_validation->run() == TRUE){

			$data = array(
					'edm_category_id' => $this->input->post('edm_category_id'),
					     'title' => $this->input->post('title'),
					  'title_en' => $this->input->post('title_en'),
					  'title_jp' => $this->input->post('title_jp'),					
					   'content' => $this->input->post('content'),
					'content_en' => $this->input->post('content_en'),
					'content_jp' => $this->input->post('content_jp'),
			            'status' => $this->input->post('status')
			        );

			$this->db->where('edm_id', $this->input->post('edm_id'));
			$this->db->update('edm',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);

			Redirect('backend/edm/edit?edm_id='.$this->input->post('edm_id'));

		} else { //轉向預設頁面

			$data['query']  = $this->db->get_where('edm', array('edm_id' => $this->input->get('edm_id')) )->row_array();

			//檢視view
			$this->load->view("backend/common/header.tpl",$data);
			$this->load->view("backend//edm/edit.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del () {
			
		$this->db->delete('edm' , array('edm_id' => $this->input->get('edm_id')));
		
		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
		
		Redirect('backend/edm/lists');
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 下拉式 / 狀態
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function slt_status () {
		
		return array(
		               '0' => '停用' ,
                       '1' => '啟用'
	            );
	}
	
}


/* End of file edm.php */
/* Location: ./application/controllers/backend/edm.php */