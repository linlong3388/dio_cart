<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理 [最新消息]的控制器
 * @controllerName news
 * @author Dio
 *
 */
class news extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));
		
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists () {

		$data['slt_status'] = $this->slt_status();
		$data['query'] = $this->db->get('news')->result_array();

		//檢視view
		$this->load->view("backend/common/header.tpl",$data);
		$this->load->view("backend/news/lists.tpl");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add () {

		//$this->form_validation->set_rules('news_category_id','* 分類ID','trim|required');
		$this->form_validation->set_rules('title','*標題','trim|required');
		$this->form_validation->set_rules('image','*圖片','trim');
		$this->form_validation->set_rules('content','*內容','trim|required');
			
		if ($this->form_validation->run() == TRUE){

			$data = array(
					//'news_category_id' => $this->input->post('news_category_id'),
					    'title' => $this->input->post('title'),
					    'image' => $this->input->post('image'),
					  'content' => $this->input->post('content'),
                        'cdate' => date('Y-m-d H:i:s') 
			        );

			$this->db->insert('news',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
			
			redirect('backend/news/lists');
			
		} else { 

			$this->load->view("backend/common/header.tpl");
			$this->load->view("backend/news/add.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit () {
		
		//$this->form_validation->set_rules('news_category_id','* 分類ID','trim|required');
		$this->form_validation->set_rules('news_id','*編號','trim|required');
		$this->form_validation->set_rules('title','*標題','trim|required');
		$this->form_validation->set_rules('image','*圖片','trim');
		$this->form_validation->set_rules('content','*內容','trim|required');
		$this->form_validation->set_rules('is_show_home','*是否顯示在首頁','trim');
		$this->form_validation->set_rules('status','*狀態','trim|required');
		$this->form_validation->set_rules('is_hot','*是否熱門','trim');
		$this->form_validation->set_rules('date_show','*顯示日期','trim');
		$this->form_validation->set_rules('cdate','*建檔日期','trim|required');
		$this->form_validation->set_rules('meta_title','*Meta標籤標題','trim');
		$this->form_validation->set_rules('meta_description','*Meta標籤描述','trim');
		$this->form_validation->set_rules('meta_keyword','*Meta標籤關鍵字','trim');

		if ($this->form_validation->run() == TRUE){

			$data = array(
					//'news_category_id' => $this->input->post('news_category_id'),
					     'title' => $this->input->post('title'),
					     'image' => $this->input->post('image'),
					   'content' => $this->input->post('content'),
					'is_show_home' => $this->input->post('is_show_home'),
				 	 'date_show' => $this->input->post('date_show'),
					'is_hot' => $this->input->post('is_hot'),
					'meta_title'         => $this->input->post('meta_title'),
					'meta_description'   => $this->input->post('meta_description'),
					'meta_keyword'       => $this->input->post('meta_keyword'),
			            'status' => $this->input->post('status')
			        );

			$this->db->where('news_id', $this->input->post('news_id'));
			$this->db->update('news',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);

			Redirect('backend/news/edit?news_id='.$this->input->post('news_id'));

		} else { //轉向預設頁面

			$data['query']  = $this->db->get_where('news', array('news_id' => $this->input->get('news_id')) )->row_array();

			//檢視view
			$this->load->view("backend/common/header.tpl",$data);
			$this->load->view("backend//news/edit.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del () {
			
		$this->db->delete('news' , array('news_id' => $this->input->get('news_id')));
		
		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
		
		Redirect('backend/news/lists');
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 下拉式 / 狀態
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function slt_status () {
		
		return array(
		               '0' => '停用' ,
                       '1' => '啟用'
	            );
	}
	
}


/* End of file news.php */
/* Location: ./application/controllers/backend/news.php */