<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 -  處理[商品分類]的控制器
 *            子分類項
 * 
 * @controllerName space_category_child
 * @author Dio
 *
 */
class space_category_child extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));
	
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
		
		if($this->input->get('page')){ //頁碼
		   $this->page = $this->input->get('page'); 	
		}else{
		   $this->page = 1;	
		}
		
	    $this->srh_page_per = 20; //每頁筆數	
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists() {

		$query_category = $this->cls_category_space->get_single( $this->input->get('category_id') );
			
		
		$data['query'] = $this->cls_category_space->get_child( $query_category['path'] );
			
		$this->load->view("backend/common/header.tpl" ,$data);
		$this->load->view("backend/space/category_child/lists.tpl");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add () {
		
		$this->form_validation->set_rules('name','*分類名稱','trim|required');
		$this->form_validation->set_rules('path','*路徑','trim|required');

		if ($this->form_validation->run() == TRUE){

			if(count(explode(',', $this->input->post('path'))) > 3){
				dioRedirect('backend/space_category_child/add?category_id='.$this->input->get('category_id') ,'*商品分類最多不可超過3層');
				exit;
			}

			$data = array(
				        	'name' => $this->input->post('name'),
	  	                    'path' => $this->input->post('path'),
                      'created_at' => date('Y-m-d H:i:s') 
			);

			$this->cls_category_space->insert($data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
			
			redirect('backend/space_category_child/lists?'.$_SERVER["QUERY_STRING"]);

		} else { //轉向預設頁面

			$query_category = $this->cls_category_space->get_single( $this->input->get('category_id') );

			
			$data['category_id']  = $this->input->get('category_id');
			$data['query_master'] = $this->cls_category_space->get_child2( $query_category['path'] );

			//檢視view
			$this->load->view("backend/common/header.tpl" ,$data);
			$this->load->view("backend/space/category_child/add.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit () {
		
		$this->form_validation->set_rules('category_id','*分類編號','trim|required|numeric');
		$this->form_validation->set_rules('name','*分類名稱','trim|required');
		$this->form_validation->set_rules('image','*圖片','trim');
		$this->form_validation->set_rules('content','*內容','trim');
		$this->form_validation->set_rules('status','*狀態','trim|required');
		$this->form_validation->set_rules('created_at','*新增日期','trim');
		$this->form_validation->set_rules('meta_title','*Meta標籤標題','trim');
		$this->form_validation->set_rules('meta_description','*Meta標籤描述','trim');
		$this->form_validation->set_rules('meta_keyword','*Meta標籤關鍵字','trim');
			
		if ($this->form_validation->run() == TRUE){

			$data = array(
                      'name' => $this->input->post('name'),
					'image' => $this->input->post('image'),
					'content' => $this->input->post('content'),					
					'status' => $this->input->post('status'),
                'updated_at' => date('Y-m-d H:i:s'),
					'meta_title'         => $this->input->post('meta_title'),
					'meta_description'   => $this->input->post('meta_description'),
					'meta_keyword'       => $this->input->post('meta_keyword'),
			);

			$this->db->where('category_id', $this->input->post('category_id'));
			$this->db->update('space_category',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
			
			redirect('backend/space_category_child/edit?'.$_SERVER["QUERY_STRING"]);

		} else { //轉向預設頁面

			
			$data['query'] = $this->cls_category_space->get_single( $this->input->get('category_id') );

			//檢視view
			$this->load->view("backend/common/header.tpl" ,$data);
			$this->load->view("backend/space/category_child/edit.tpl");

		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del() {

		$parent_category_id = $this->input->get('parent_category_id');
		$category_id = $this->input->get('category_id');

		if( (!$this->cls_category_space->is_sub_category($category_id)) && (!$this->cls_category_space->is_sub_product($category_id))  ) {

			$this->db->delete('space_category' , array('category_id' => $category_id));
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
			
			redirect('backend/space_category_child/lists?category_id='.$parent_category_id);
		}else{

			$this->session->set_flashdata('msg_err','不可刪除! 此分類項已有關聯子分類或商品!');
			
			redirect('backend/space_category_child/lists?'.$_SERVER["QUERY_STRING"]);
		}
	}

}


/* End of file space_category_child.php */
/* Location: ./application/controllers/backend/space_category_child.php */