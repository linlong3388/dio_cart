<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 -  處理[功能屬性]的控制器
 * 說明 : 與商品主表關聯的屬性(子表)
 * 
 * @controllerName product_attr
 * @author Dio
 *
 */
class product_attr extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session','category'));
		
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
		
		$this->load->model("backend/product_model","model");
		
		$this->cls_category = new category();
		
	}	

	// --------------------------------------------------------------------

	/**
	 * 方法 : 新增/屬性
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add(){
	
	    parse_str($this->input->post('data') ,$data_post);
	    
		if ( !empty($data_post) ){
			
			$data = array(
					'product_id'  => $data_post['product_id'],
					'image'       => $data_post['image'],
					'cdate'       => date('Y-m-d H:i:s')
			);
	
			$this->db->insert('product_attr',$data);
			
			redirect('backend/product/edit?product_id='.$this->input->post('product_id'));			
			
		}else{
			
			$this->load->view('backend/common/header.tpl');
			$this->load->view('backend/product_attr/add.tpl');
		}
		
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 編輯/屬性
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit(){
	
	    $product_attr_id = $this->input->post('product_attr_id');
	    
		if ( !empty($product_attr_id) ){
			
			$data = array(
					'image'      => $this->input->post('image'),
					'sort_order' => $this->input->post('sort_order') ,
			);
				
			$this->db->where('product_attr_id', $product_attr_id);
			$this->db->update('product_attr',$data);
	
			redirect('backend/product/edit?product_id='.$this->input->post('product_id'));
	
		} else {
				
			//$data['query'] = $this->model->shr_product_attr_info($this->input->get('product_attr_id'));
			$this->db->where('product_attr_id' ,$this->input->get('product_attr_id'));
			$data['query'] = $this->db->get('product_attr')->row_array();
			
			//檢視view
			$this->load->view('backend/common/header.tpl' ,$data);
			$this->load->view('backend/product_attr/edit.tpl');
		}
			
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del(){

			$this->db->where('product_attr_id' ,$this->input->get('product_attr_id') );
			$this->db->delete('product_attr');
	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 依[屬性分類]取得[屬性]
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function get_property(){
		
		$this->db->where('property_category_id' ,$this->input->get('property_category_id') );
		$query = $this->db->get('property')->result_array();
		
		echo json_encode($query);
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 驗證[產品ID][屬性ID]是否重複
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function is_mutil_key( $product_id ,$property_id ){
	
		$err_msg = "";
		
		if( empty($product_id) ){
			$err_msg = "*商品ID 不可空白!";
		}
		
		if( empty($property_id) ){
			$err_msg = "*屬性ID 不可空白!";
		}
		
		$this->db->where('product_id' ,$product_id);
		$this->db->where('property_id' ,$property_id);
		
		$query = $this->db->get('product_attr')->row_array();
		
		if( !empty($query) ) {
			$err_msg = "*商品ID/屬性ID 二項目不可重複!";
		}
		
		return $err_msg;
	}

}


/* End of file product_attr.php */
/* Location: ./application/controllers/backend/product_attr.php */