<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理[報表統計/訂單]的控制器
 * @controllerName report_order
 * @author Dio
 *
 */
class report_order extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base' ,'is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));
		
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}

		$this->load->model("backend/report_order_model","order");
	
		if($this->input->get('page')){ //頁碼
			$this->page = $this->input->get('page');
		}else{
			$this->page = 1;
		}
		
		$this->srh_page_per = 20; //每頁筆數	
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists () {

		$srh_data = $this->search($this->input->get());
		
		$data['query'] = $this->order->shr_order($srh_data);
		
		$data['data_sort'] = $this->sort_by();

		//----------------------------------------------------------------------
		// 設定分頁
		//----------------------------------------------------------------------
		/*
		$query_group_by_ary = $this->statistics_model->shr_sales_statistics_group_by($srh_data);

		$this->load->library('Dio_paginator');
		$pages = new Dio_paginator();
		$pages->set($query_group_by_ary[0]['count'],5,array(30,3,6,9,12,25,50,100,250,'All'));

		$data['pages'] = $pages;
		$data['query_group_by'] = $query_group_by_ary;

		$page = 1;
		if($this->input->get('page')){
			$page = $this->input->get('page');
		}

		$data['page_startEnd'] = $pages->get_startEnd_page($page ,$query_group_by_ary[0]['count'] ,30);
        */

		$this->load->view('backend/common/header.tpl' ,$data);
		$this->load->view('backend/report/order/lists.tpl');

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 搜尋
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function search ($data) {
	
		if(isset($data) && !empty($data)){
			foreach ($data as $key=>$val) {
				if($val == ''){
					unset($data[$key]);
				}
			}
		}
	
		$data['srh_page_per'] = $this->srh_page_per;
		$data['srh_limit_page'] = $this->page;
	
		return $data;
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 排序
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function sort_by () {
	
		return array(
				"*" => '--' ,
				"product.sku" => 'sku' ,
				"product.name" => '商品名稱' ,
				"SUM(order_detail.entity)" => '購買數量' ,
		);
	}

}


/* End of file report.php */
/* Location: ./application/controllers/backend/report.php */