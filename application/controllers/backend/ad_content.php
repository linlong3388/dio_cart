<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理[商品頁廣告]的控制器
 * @controllerName ad_content
 * @author Dio
 *
 */
class ad_content extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid','elfinder'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session','category'));

		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
	
		$this->cls_category = new category();
		
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 商品頁廣告 / 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists () {
		
		$data['query'] = $this->db->get('ad_content')->result_array();
		
		
		$this->load->view("backend/common/header.tpl",$data);
		$this->load->view("backend/ad_content/lists.tpl");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 商品頁廣告 / 新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add () {

		//$this->form_validation->set_rules('ad_content_category_id','*商品分類','trim|required');
		$this->form_validation->set_rules('type','*型態','trim|required');
		//$this->form_validation->set_rules('title','*標題','trim');
		//$this->form_validation->set_rules('description','*簡述','trim');
		$this->form_validation->set_rules('image','*圖片','trim');
		$this->form_validation->set_rules('url','*URL','trim');
		
		if ($this->form_validation->run() == TRUE){
				
			$data = array(
	   //'ad_content_category_id' => $this->input->post('ad_content_category_id'),
					     'type' => $this->input->post('type'),
		             	//'title' => $this->input->post('title'),
			      //'description' => $this->input->post('description'),
                          'url' => $this->input->post('url'),
                        'image' => $this->input->post('image'),
                        'cdate' => date('Y-m-d H:i:s') 
			);

			$this->db->insert('ad_content',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
				
			redirect('backend/ad_content/lists');			
		
		} else { 
			
			$this->load->view("backend/common/header.tpl");
			$this->load->view("backend/ad_content/add.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 商品頁廣告 / 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit () {
		
		$this->form_validation->set_rules('ad_content_id','*編號','trim|required');
		//$this->form_validation->set_rules('ad_content_category_id','*商品分類','trim|required');
		$this->form_validation->set_rules('type','*類型','trim|required');
		//$this->form_validation->set_rules('title','*標題','trim');
		//$this->form_validation->set_rules('description','*簡述','trim');
		$this->form_validation->set_rules('image','*圖片','trim');
		$this->form_validation->set_rules('url','*URL','trim');
		$this->form_validation->set_rules('status','*狀態','trim|required');
		$this->form_validation->set_rules('cdate','建立日期','trim');

		if ($this->form_validation->run() == TRUE){

			$data = array(
			      //'ad_content_category_id' => $this->input->post('ad_content_category_id'),					
                         'type' => $this->input->post('type'),
		             	//'title' => $this->input->post('title'),
			      //'description' => $this->input->post('description'),			
                          'url' => $this->input->post('url'), 
                        'image' => $this->input->post('image'),
                       'status' => $this->input->post('status')
			);

			$this->db->where('ad_content_id', $this->input->post('ad_content_id'));
			$this->db->update('ad_content',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
			
			redirect('backend/ad_content/edit?ad_content_id='.$this->input->post('ad_content_id'));			
		
		} else { 

			$data['query']  = $this->db->get_where('ad_content', array('ad_content_id' => $this->input->get('ad_content_id')) )->row_array();
					
			$this->load->view("backend/common/header.tpl" ,$data);
			$this->load->view("backend/ad_content/edit.tpl");

		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 商品頁廣告 / 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del () {
		
		$this->db->delete('ad_content' , array('ad_content_id' => $this->input->get('ad_content_id')));

		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
		
		redirect('backend/ad_content/lists');		
	}

}


/* End of file ad_content.php */
/* Location: ./application/controllers/backend/ad_content.php */