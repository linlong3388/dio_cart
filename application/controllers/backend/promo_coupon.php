<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 後台管理首頁  for 行銷
 * @controllerName promo_coupon
 * @author Dio
 *
 */
class promo_coupon extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session','category'));
	
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
		
		//建立商品分類物件
		$this->cls_category = new category();
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists () {

		$data['query'] = $this->db->get('promo_coupon')->result_array();

		//檢視view
		$this->load->view("backend/common/header.tpl" ,$data);
		$this->load->view("backend/promo_coupon/lists.tpl");
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add () {

		$this->form_validation->set_rules('name','*名稱','trim|required');
		$this->form_validation->set_rules('code_type','*類型','trim|required');		
		$this->form_validation->set_rules('code_val','*折扣','trim|required');
		$this->form_validation->set_rules('total','*總金額','trim|required');
		$this->form_validation->set_rules('use_total','*使用總數','trim');
		$this->form_validation->set_rules('use_count','*使用次數','trim');
		$this->form_validation->set_rules('use_count_customer','*客戶可使用次數','trim');
		$this->form_validation->set_rules('sdate','*活動開始日期','trim|required');
		$this->form_validation->set_rules('edate','*活動結束日期','trim|required');
		//$this->form_validation->set_rules('udate','*使用有效日期','trim|required');
		
		if ($this->form_validation->run() == TRUE){

			$data = array(
                      'name' => $this->input->post('name'),
			          'code' => g_coupon_id(),
			     'code_type' => $this->input->post('code_type'), 
				  'code_val' => $this->input->post('code_val'),
					 'total' => $this->input->post('total'),
				 'use_total' => $this->input->post('use_total'),
				 'use_count' => $this->input->post('use_count'),
		'use_count_customer' => $this->input->post('use_count_customer'),
			         'sdate' => $this->input->post('sdate'),
			         'edate' => $this->input->post('edate'),
					 //'udate' => $this->input->post('udate'),
                     'cdate' => date('Y-m-d H:i:s') );

			$this->db->insert('promo_coupon',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
				
			redirect('backend/promo_coupon/lists');
			
		} else { 
			
			$this->load->view("backend/common/header.tpl");
			$this->load->view("backend/promo_coupon/add.tpl");
		}

	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit () {
		
		$promo_coupon_id = $this->validPromoCouponId($this->input->get('promo_coupon_id'));
		
		$this->form_validation->set_rules('name','*名稱','trim|required');
		$this->form_validation->set_rules('code','*代碼','trim');
		$this->form_validation->set_rules('code_type','*類型','trim');
		$this->form_validation->set_rules('code_val','*折扣','trim|required');
		//$this->form_validation->set_rules('enable_category[]','*限定分類','trim');
		//$this->form_validation->set_rules('enable_product[]','*限定商品','trim');
		$this->form_validation->set_rules('total','*總金額','trim|required');
		$this->form_validation->set_rules('use_total','*使用總數','trim');
		$this->form_validation->set_rules('use_count','*使用次數','trim');
		$this->form_validation->set_rules('use_count_customer','*客戶可使用次數','trim');
		$this->form_validation->set_rules('sdate','*活動開始日期','trim|required');
		$this->form_validation->set_rules('edate','*活動結束日期','trim|required');
		//$this->form_validation->set_rules('udate','*使用有效日期','trim|required');
		$this->form_validation->set_rules('status','*狀態','trim|required');
		
		if ($this->form_validation->run() == TRUE){
			
			//限定分類
			//$enable_category = $this->input->post('enable_category');
			//$enable_category = isset($enable_category) ? implode(',' ,$enable_category) : ''; 
			
			//限定商品
			//$enable_product = $this->input->post('enable_product');
			//$enable_product = isset($enable_product) ? implode(',' ,$enable_product) : '';
						
			//將POST資料 ,轉為陣列
			$data = array(
                      'name' => $this->input->post('name'),
			          //'code' => $this->input->post('code'),
			     'code_type' => $this->input->post('code_type'),
			  	  'code_val' => $this->input->post('code_val'),
		   //'enable_category' => $enable_category,
			//'enable_product' => $enable_product,
					 'total' => $this->input->post('total'),
				 'use_total' => $this->input->post('use_total'),
     		     'use_count' => $this->input->post('use_count'),
		'use_count_customer' => $this->input->post('use_count_customer'),
			         'sdate' => $this->input->post('sdate'),
			         'edate' => $this->input->post('edate'),
					 //'udate' => $this->input->post('udate'),				
				    'status' => $this->input->post('status')
			);
			
			//預設一個檔期，只提供一種促銷活動，更新狀態為0
			/*
			if($data['status'] == 1){
				$this->db->update('promo_coupon' ,array('status' => 0));
			}*/
			
			$this->db->where('promo_coupon_id', $promo_coupon_id);
			$this->db->update('promo_coupon',$data);

			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
			
			Redirect('backend/promo_coupon/edit?promo_coupon_id='.$promo_coupon_id);
			
		} else { 
			
			$data['query_product_category']  = $this->cls_category->get_2parent(1);
			$data['query_product']  =  $this->getProductList();
			
			$data['query']  = $this->db->get_where('promo_coupon', array('promo_coupon_id' => $promo_coupon_id) )->row_array();
			$data['query_coupon']  = $this->getCouponUseCount($data['query']['code']);

			//$data['query']['is_enable_category'] = empty($data['query']['enable_category']) ? '0' : '1';
			//$data['query']['enable_category']    = $this->getEnableCategoryList($data['query']['enable_category']);
			
			//$data['query']['is_enable_product'] = empty($data['query']['enable_product']) ? '0' : '1';
			//$data['query']['enable_product']    = $this->getEnableProductList($data['query']['enable_product']);
			
			$this->load->view("backend/common/header.tpl",$data);
			$this->load->view("backend/promo_coupon/edit.tpl");
		}

	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del () {
		
		$this->db->delete('promo_coupon' , array('promo_coupon_id' => $this->input->get('promo_coupon_id')));
		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
		
		Redirect('backend/promo_coupon/lists');
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得限定分類列表，並整理成js可用格式
	 *
	 * @access	public
	 * @param   string 
	 *          格式如 : 8,2,6
	 *          
	 * @return  string 
	 *          格式如 : 8|招牌特產,2|中式點心,6|訂婚喜餅	 
	 */
	public function getEnableCategoryList ($param) {
	
		$data = "";
		
		$param = explode(',' ,$param);
	
        $this->db->where_in('category_id', $param);
		$query = $this->db->get('category')->result_array();
		
		foreach (transKeyPairArray($query, 'category_id', 'name') as $key=>$val) {
			$data .= $key .'|'. $val.',';
		}
		
		return trim($data ,',');
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得限定商品列表，並整理成js可用格式
	 *
	 * @access	public
	 * @param   string
	 *          格式如 : 8,2,6
	 *
	 * @return  string
	 *          格式如 : 8|招牌特產,2|中式點心,6|訂婚喜餅
	 */
	public function getEnableProductList ($param) {
	
		$data = "";
	
		$param = explode(',' ,$param);
	
		$this->db->where_in('product_id', $param);
		$query = $this->db->get('product')->result_array();
	
		foreach (transKeyPairArray($query, 'product_id', 'name') as $key=>$val) {
			$data .= $key .'|'. $val.',';
		}
	
		return trim($data ,',');
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得商品列表資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getProductList() {
	
		$this->db->where('status', 1);
		$this->db->where('type_id', 0);
		$query = $this->db->get('product')->result_array();
		
		return $query;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 驗證輸入資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function validPromoCouponId($promo_coupon_id){
		
		if( !is_numeric($promo_coupon_id) ) {
			show_404();
			exit;
		}
		 
		return $promo_coupon_id;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得折價券已使用次數
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function getCouponUseCount($code) {
	
		$this->db->where('code' ,$code);
		return $this->db->count_all_results('customer_coupon');
	}
	
}

/* End of file promo_coupon.php */
/* Location: ./application/controllers/promo_coupon.php */