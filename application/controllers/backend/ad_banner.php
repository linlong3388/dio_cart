<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理[橫幅廣告]的控制器
 * @controllerName ad_banner
 * @author Dio
 *
 */
class ad_banner extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid','elfinder'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));

		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
		
		//載入model
		$this->load->model("backend/dashboard_model","model");
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 內容廣告 / 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists () {
		
		$data['query'] = $this->db->get('ad_banner')->result_array();
		
		//檢視view
		$this->load->view("backend/common/header.tpl",$data);
		$this->load->view("backend/ad_banner/lists.tpl");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 內容廣告 / 新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add () {

		$this->form_validation->set_rules('type','*型態','trim|required');
		$this->form_validation->set_rules('image','*圖片','trim');
		//$this->form_validation->set_rules('title','*標題','trim');
		//$this->form_validation->set_rules('description','*簡述','trim');
		$this->form_validation->set_rules('url','*URL','trim');

		if ($this->form_validation->run() == TRUE){
				
			$data = array(
                         'type' => $this->input->post('type'),
					    'image' => $this->input->post('image'),
					  //'title' => $this->input->post('title'),
			    //'description' => $this->input->post('description'),
                          'url' => $this->input->post('url'),
                        'cdate' => date('Y-m-d H:i:s') 
			);

			$this->db->insert('ad_banner',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
				
			redirect('backend/ad_banner/lists');			
		
		} else { //轉向預設頁面

			//檢視view
			$this->load->view("backend/common/header.tpl");
			$this->load->view("backend/ad_banner/add.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 內容廣告 / 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit () {
		
		$this->form_validation->set_rules('ad_banner_id','*編號','trim|required');
		$this->form_validation->set_rules('type','*類型','trim|required');
		//$this->form_validation->set_rules('title','*標題','trim');
		//$this->form_validation->set_rules('description','*簡述','trim');
		$this->form_validation->set_rules('image','*圖片','trim');
		$this->form_validation->set_rules('url','*URL','trim');
		$this->form_validation->set_rules('status','*狀態','trim|required');
		$this->form_validation->set_rules('cdate','建立日期','trim');

		if ($this->form_validation->run() == TRUE){

			$data = array(
                         'type' => $this->input->post('type'),
		             	//'title' => $this->input->post('title'),
			      //'description' => $this->input->post('description'),			
					    'image' => $this->input->post('image'),
					      'url' => $this->input->post('url'), 
                       'status' => $this->input->post('status')
			);

			$this->db->where('ad_banner_id', $this->input->post('ad_banner_id'));
			$this->db->update('ad_banner',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
			
			redirect('backend/ad_banner/lists');			
		
		} else { //轉向預設頁面

			$data['query']  = $this->db->get_where('ad_banner', array('ad_banner_id' => $this->input->get('ad_banner_id')) )->row_array();

			//檢視view
			$this->load->view("backend/common/header.tpl" ,$data);
			$this->load->view("backend/ad_banner/edit.tpl");

		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 內容廣告 / 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del () {
		
		$this->db->delete('ad_banner' , array('ad_banner_id' => $this->input->get('ad_banner_id')));

		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
		
		redirect('backend/ad_banner/lists');		
	}

}


/* End of file ad_banner.php */
/* Location: ./application/controllers/backend/ad_banner.php */