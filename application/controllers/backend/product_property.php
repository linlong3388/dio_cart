<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 -  處理[商品屬性]的控制器
 * @controllerName product_property
 * @author Dio
 *
 */
class product_property extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session','category','property'));

		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
		
		//載入model
		$this->load->model("backend/product_model","model");

		//建立商品分類物件
		$this->cls_category = new category();
		

		//建立商品屬性物件
		$this->cls_property = new property();
		$this->cls_property->_set('property');
		
		if($this->input->get('page')){ //頁碼
		   $this->page = $this->input->get('page'); 	
		}else{
		   $this->page = 1;	
		}
		
	    $this->srh_page_per = 20; //每頁筆數	
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : (頂層)屬性分類 / 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists () {

		$this->db->order_by('parent_id','ASC');
		$data['query'] = $this->db->get('property')->result_array();

		//檢視view
		$this->load->view("backend/common/header.tpl" ,$data);
		$this->load->view("backend/product/property/lists.tpl");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : (頂層)屬性分類 / 新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add () {
		
		$this->form_validation->set_rules('property_category_id','*分類ID','trim|required');
		$this->form_validation->set_rules('parent_id','*父類ID','trim');
		$this->form_validation->set_rules('name','*名稱','trim|required');

		if ($this->form_validation->run() == TRUE){

			//將POST資料 ,轉為陣列
			$data = array(
		  'property_category_id' => $this->input->post('property_category_id'),
                     'parent_id' => $this->input->post('parent_id'),
	  	                  'name' => $this->input->post('name'),
	  	                 'cdate' => date('Y-m-d H:i:s') 
			);

			$this->db->insert('property',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
				
			redirect('backend/product_property/lists');
	
		} else { //轉向預設頁面

			//檢視view
			$this->load->view("backend/common/header.tpl");
			$this->load->view("backend/product/property/add.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : (頂層)屬性分類 / 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit () {

		$this->form_validation->set_rules('property_category_id','*分類ID','trim|required');
		$this->form_validation->set_rules('parent_id','*父類ID','trim');
		$this->form_validation->set_rules('name','*名稱(中)','trim|required');
		$this->form_validation->set_rules('name_en','*名稱(英)','trim');
		$this->form_validation->set_rules('name_jp','*名稱(日)','trim');
		$this->form_validation->set_rules('status','*狀態','trim|required');

		if ($this->form_validation->run() == TRUE){

			//將POST資料 ,轉為陣列
			$data = array(
	  'property_category_id' => $this->input->post('property_category_id'),
		              'name' => $this->input->post('name'),
				   'name_en' => $this->input->post('name_en'),
				   'name_jp' => $this->input->post('name_jp'),
			        'status' => $this->input->post('status')
			);

			$this->db->where('property_id', $this->input->post('property_id'));
			$this->db->update('property',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);

			Redirect('backend/product_property/edit?property_id='.$this->input->post('property_id'));
		
		} else { //轉向預設頁面

			$data['cls_property'] = $this->cls_property;
			$data['query'] = $this->cls_property->get_single( $this->input->get('property_id') );

			//檢視view
			$this->load->view("backend/common/header.tpl" ,$data);
			$this->load->view("backend/product/property/edit.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : (頂層)屬性分類 / 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del () {
	
		/*
		 if(!$this->cls_property->is_sub_property($this->input->get('property_id'))){

			$this->db->delete('property' , array('property_id' => $this->input->get('property_id')));
			dioRedirect('backend/product_property/lists');
			}else{

			dioRedirect('backend/product_property/lists' ,'*不可刪除! 此屬性分類已有關聯子分類!');
			}*/

		$this->db->delete('property' , array('property_id' => $this->input->get('property_id')));
		dioRedirect('backend/product_property/lists');
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : (次級)屬性分類 / 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists_2 () {
		
		$query_property = $this->cls_property->get_single( $this->input->get('property_id') );
			
		$data['cls_property'] = $this->cls_property;
		$data['query'] = $this->cls_property->get_child( $query_property['path'] );
			
		//檢視view
		$this->load->view("backend/common/header.tpl" ,$data);
		$this->load->view("backend/product/property/lists_2.tpl");
	}

}


/* End of file product_property.php */
/* Location: ./application/controllers/backend/product_property.php */