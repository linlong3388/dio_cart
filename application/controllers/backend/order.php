<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理[訂單]的控制器
 * @controllerName order
 * @author Dio
 *
 */
class order extends BackEnd_Controller {
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session','category','Dio_paginator','promo/Coupon'));
		
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
		
		//載入model
		$this->load->model("backend/order_model","order");

		//建立商品分類物件
		$this->cls_category = new category();
		
		//會員折價券
		$this->Coupon = new Coupon();
		
		if($this->input->get('page')){ //頁碼
		   $this->page = $this->input->get('page'); 	
		}else{
		   $this->page = 1;	
		}
		
	    $this->srh_page_per = 20; //每頁筆數	
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists(){
		
		if( $this->input->get('submit_output') ){
		
			$this->output( $this->search($this->input->get()) );
		
		}else{
		
            $srh_data = $this->search($this->input->get());
        
		    $data['query'] = $this->order->shr_order($srh_data);
		
            $data['data_sort'] = $this->sort_by();
		
		    //----------------------------------------------------------------------
		    // 設定分頁
		    //----------------------------------------------------------------------
		    $query_group_by_ary = $this->order->shr_order_group_by($srh_data);
		
		    $pages = new Dio_paginator();
		    $pages->set($query_group_by_ary[0]['count'],5,array($this->srh_page_per,3,6,9,12,25,50,100,250,'All'));

		    $data['pages']          = $pages;
		    $data['query_group_by'] = $query_group_by_ary;
		    $data['page_startEnd']  = $pages->get_startEnd_page($this->page ,$query_group_by_ary[0]['count'] ,$this->srh_page_per);

		    //檢視view
		    $this->load->view('backend/common/header.tpl' ,$data);
		    $this->load->view('backend/order/lists.tpl');
		
		}
		
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 訂單資料 / 匯出(Excel)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function output ($srh_data) {
		
		//下載用檔頭
		header('Content-type: application/vnd.ms-excel');
		header("Content-Disposition: attachment; filename=OrderExport_".date('Ymd').".xls");
	
		$this->load->library(array('PHPExcel/Classes/PHPExcel.php'));
			
		$data['query']     = $this->order->output_xls($srh_data);
				
		$data['data_sort'] = $this->sort_by();
		
		$objPHPExcel = new PHPExcel();
	
		//設定檔案資訊
		$objPHPExcel->getProperties()->setCreator("Dio")
		->setLastModifiedBy("Dio")
		->setTitle("PET House")
		->setSubject("Order export")
		->setDescription("--")
		->setKeywords("--")
		->setCategory("--");
	
		//設定表格間距寬度
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(20);
	
		//設定樣式
		$objPHPExcel->getActiveSheet()->getStyle("A1:W1")->getFont()->setBold(true);
	
		//設定欄位抬頭
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A1', "訂單時間")
		->setCellValue('B1', "訂單狀態")
		->setCellValue('C1', "訂單編號")
		->setCellValue('D1', "指定送貨")
		->setCellValue('E1', "訂購人")		
		->setCellValue('F1', "訂購人帳號")
		->setCellValue('G1', "Email")			
		->setCellValue('H1', "收件人")
		->setCellValue('I1', "收件地址")
		->setCellValue('J1', "收件人電話")
		->setCellValue('K1', "收件人手機")
		->setCellValue('L1', "訂單備註")
		->setCellValue('M1', "發票類型")
		->setCellValue('N1', "抬頭")
		->setCellValue('O1', "統一編號")
		->setCellValue('P1', "發票地址")
		->setCellValue('Q1', "付款方式")
		->setCellValue('R1', "訂購明細")
		->setCellValue('S1', "運費")
		->setCellValue('T1', "總計")
		->setCellValue('U1', "系統備註")
		->setCellValue('V1', "超商取貨/店編")
		->setCellValue('W1', "超商取貨/店名");
	
		$row_id = 2;
	
		//資料筆數
		for($i=0 ; $i<( COUNT($data['query']) ) ; $i++){
			
			//黑貓代收款項
			$cat_money = ($data['query'][$i]['pay_method'] == 0) ? $data['query'][$i]['total'] : 0;
				
			//發票格式
			$ticket_type = reSortArrayMutil(2,$data['query'][$i]['ticket_type'],getTicketType());
	
			$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$row_id, $data['query'][$i]['cdate'])
			->setCellValue('B'.$row_id, reSortArrayMutil(2,$data['query'][$i]['status'] ,getOrderStatus()))
			->setCellValueExplicit('C'.$row_id, $data['query'][$i]['order_show_id'], PHPExcel_Cell_DataType::TYPE_STRING)
			->setCellValue('D'.$row_id, substr($data['query'][$i]['date_arrival'],0,15))
			->setCellValue('E'.$row_id, $data['query'][$i]['c_name'])
			->setCellValue('F'.$row_id, $data['query'][$i]['c_username'])
			->setCellValue('G'.$row_id, $data['query'][$i]['o_email'])
			->setCellValue('H'.$row_id, $data['query'][$i]['name'])
			->setCellValue('I'.$row_id, $data['query'][$i]['local'] .' ' .$data['query'][$i]['address'])
			->setCellValueExplicit('J'.$row_id, $data['query'][$i]['phone'], PHPExcel_Cell_DataType::TYPE_STRING)
			->setCellValueExplicit('K'.$row_id, $data['query'][$i]['mobile'], PHPExcel_Cell_DataType::TYPE_STRING)
			->setCellValue('L'.$row_id, $data['query'][$i]['memo'])
			->setCellValue('M'.$row_id, $ticket_type)
			->setCellValue('N'.$row_id, $data['query'][$i]['ticket3_title'])
			->setCellValue('O'.$row_id, $data['query'][$i]['ticket3_unified'])
			->setCellValue('P'.$row_id, '')
			->setCellValue('Q'.$row_id, reSortArrayMutil(2,$data['query'][$i]['pay_method'] ,getPayMethod()))
			->setCellValue('R'.$row_id, $this->output_detail ( $data['query'][$i]['order_id'] ))
			->setCellValue('S'.$row_id, DIO_CURRENCY.addCommas($this->output_fee ( $data['query'][$i]['order_id'] )) )			
			->setCellValue('T'.$row_id, DIO_CURRENCY.addCommas($data['query'][$i]['total']) )
			->setCellValue('U'.$row_id, $data['query'][$i]['memo_admin'])
		    ->setCellValueExplicit('V'.$row_id, $data['query'][$i]['cvs_store_id'], PHPExcel_Cell_DataType::TYPE_STRING)
			->setCellValue('W'.$row_id, $data['query'][$i]['cvs_store_name']);
							
			$row_id++;
		}
	
		//工作區命名
		$objPHPExcel->getActiveSheet()->setTitle('Worksheet1');
		
		//隱藏欄位
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setVisible(false);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setVisible(false);
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setVisible(false);
		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setVisible(false);
		$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setVisible(false);
		
		//將檔案寫入暫存區，供下載用
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	
		$objPHPExcel->disconnectWorksheets();
		unset($objPHPExcel);
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 訂單資料 / 匯出(Excel) / 商品明細
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function output_detail ( $order_id ) {
		
		$str = "";
		
		$query = $this->order->shr_order_detail($order_id);
		
		foreach ($query as $row){
			$str .= $row['name'].'x'.$row['entity'].','; 
		}
		
		return trim($str ,',');
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 訂單資料 / 匯出(Excel) / 運費明細
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function output_fee ( $order_id ) {
	
		$str = "";
	
		$this->db->where('order_id' ,$order_id);
		$query = $this->db->get('order_promo')->result_array();
		
		foreach ($query as $row){
			$str += $row['total'];
		}
	
		return $str;
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 搜尋
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function search ($data) {
		
		if(isset($data) && !empty($data)){
		   foreach ($data as $key=>$val) {
             if($val == ''){
		      	unset($data[$key]);
             }
           }
		}
		
		$data['srh_page_per'] = $this->srh_page_per;
		$data['srh_limit_page'] = $this->page;
		
		return $data;
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 排序
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function sort_by () {
		
		return array(
		                 '' => '--' ,
            'order_show_id' => '訂單編號',
			    'last_name' => '客戶姓名',
			        'total' => '總額',
			       'status' => '狀態', 	       
                    'cdate' => '訂購日期');
                                   
	}	

	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢視
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function view(){
		
		$this->form_validation->set_rules('trans_no','*貨號','trim');
		$this->form_validation->set_rules('pay_atm_no','*匯款帳號後五碼','trim');
		$this->form_validation->set_rules('memo_admin','*備註','trim');
		$this->form_validation->set_rules('status','*狀態','trim|required');
		
		if ($this->form_validation->run() == TRUE){
	
			$order_id = $this->input->post('order_id');
			
			$data = array(
					'trans_no'    => $this->input->post('trans_no'),
					'pay_atm_no'    => $this->input->post('pay_atm_no'),
					'memo_admin'  => $this->input->post('memo_admin'),
					'status'      => $this->input->post('status'),
			);
			
			$this->db->where('order_id', $order_id);
			$this->db->update('order',$data);
			
			//更新折價券狀態
		    if($data['status'] == 2){
				$this->Coupon->statusByField(array('order_id' => $order_id) ,1);
			}else{
				$this->Coupon->statusByField(array('order_id' => $order_id) ,2);
			}
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
			
			redirect('backend/order/view?'.$_SERVER["QUERY_STRING"]);
	
		}else{
	
			$order_id = $this->input->get('order_id');
	
			$data['query']        = $this->order->shr_order_info($order_id);
			$data['query_detail'] = $this->order->shr_order_detail($order_id);
				
			$this->db->where('order_id' ,$order_id);
			$data['query_promo'] = $this->db->get('order_promo')->result_array();
			
			$this->load->view('backend/common/header.tpl' ,$data);
			$this->load->view('backend/order/view.tpl');
		}
	
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 檢視
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	/*
	public function view(){

		$order_id = $this->input->get('order_id');
		$status = $this->input->post('status');

		if( !(empty($status) && $status !== '0') ){

			//訂單狀態類別
			$this->load->library(array('Order_status'));
			$os = new Order_status();
			if($os->change_status($order_id, $status)) {

				$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
				
				redirect('backend/order/view?'.$_SERVER["QUERY_STRING"]);
			}else{
				redirect('backend/order/error_order_status?'.$_SERVER["QUERY_STRING"].'&order_id='.$order_id);
			}

		}else{
				
			$order_id = $this->input->get('order_id');
				
			$data['query']        = $this->order->shr_order_info($order_id);
			$data['query_detail'] = $this->order->shr_order_detail($order_id);
			
			$this->db->where('order_id' ,$order_id);
			$data['query_promo'] = $this->db->get('order_promo')->result_array();
			
			$this->load->view('backend/common/header.tpl' ,$data);
			$this->load->view('backend/order/view.tpl');
		}

	}*/

	// --------------------------------------------------------------------

	/**
	 * 方法 : 待處理訂單  / 列表(採用Ajax取得資料)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function order_list_ajax(){

		//傳入的欄位(需和表格欄位數一致)
		$aColumns = array( 'order_detail_id' ,'order_id','entity','product_total','pay_method','cdate','status','is_excel' ,'cdate');
		$result = $this->order->get_data_list('order_detail' ,'order_detail_id' ,$aColumns);

		//欄位修飾
		$counter = 0;
		foreach ($result['aaData'] as $row) {
			$result['aaData'][$counter][1] = get_assign_field('order', 'order_id', $row[1], 'reci_name');
			$result['aaData'][$counter][8] =  "<a class='btn btn-success' href='". getAdminURL('order/view?order_detail_id='.$row[0]) ."'>"
			."<i class='icon-edit icon-white'></i>檢視</a>"
			."<a class='btn btn-info' href='javascript:my_confirm($row[0])'>"
			."<i class='icon-edit icon-white'></i>出貨</a>";

			$counter++;
		}

		echo json_encode( $result );
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 訂單下載
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function order_export_list(){

		$this->form_validation->set_rules('srh_cdate1','*日期區間1','trim|required');
		$this->form_validation->set_rules('srh_cdate2','*日期區間2','trim|required');
		//$this->form_validation->set_rules('status','*狀態','trim|required');

		$srh_data = array(
                       'srh_cdate1' => $this->input->post('srh_cdate1'),
		               'srh_cdate2' => $this->input->post('srh_cdate2'),
		               'srh_status' => $this->input->post('srh_status')	
		);

		if ($this->form_validation->run() == TRUE){

			$data['srh_param'] = $srh_data;
			$query = $this->order->get_csv($srh_data);
			$data['query'] = array();

			for($i=0 ;$i<COUNT($query) ;$i++){

				//拆解付款方式  
				/*
				 * 注意 ，get_payMethod_money_tpl_result2 函數已被刪除，之後的應用需修改此處 2016/07/24 by Dio
				 */ 
				$pay_method_ary = get_payMethod_money_tpl_result2($query[$i]['pay_method'] ,$query[$i]['product_total'] ,$query[$i]['Auth_OffsetAmt']);

				//回填陣列欄位
				$data['query'][$i]['cdate'] = $query[$i]['cdate'];
				$data['query'][$i]['order_detail_id'] = $query[$i]['order_detail_id'];
				$data['query'][$i]['order_detail_show_id'] = $query[$i]['order_detail_show_id'];
				$data['query'][$i]['reci_name'] = $query[$i]['reci_name'];
				$data['query'][$i]['money'] = !empty($pay_method_ary['money']) ? $pay_method_ary['money'] : '';
				$data['query'][$i]['bonus'] = !empty($pay_method_ary['bonus']) ? $pay_method_ary['bonus'] : '';
				$data['query'][$i]['pay_method'] = !empty($pay_method_ary['pay_method']) ? $pay_method_ary['pay_method'] : '';
				$data['query'][$i]['entity'] = $query[$i]['entity'];
				$data['query'][$i]['shipping_id'] = $query[$i]['shipping_id'];
				$data['query'][$i]['product_total'] = $query[$i]['product_total'];
			}

			//檢視view
			$this->load->view('backend/common/header.tpl' ,$data);
			$this->load->view('backend/order/order_export_list.tpl');

		}else{

			$data['srh_param'] = $srh_data;

			//檢視view
			$this->load->view('backend/common/header.tpl' ,$data);
			$this->load->view('backend/order/order_export_list.tpl');
		}
			
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 訂單下載  / 匯出csv
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function order_export_excel(){

		//搜尋條件
		$srh_data = array(
                       'srh_cdate1' => $this->input->get('srh_cdate1'),
		               'srh_cdate2' => $this->input->get('srh_cdate2'),
		               'srh_status' => $this->input->get('srh_status')	
		);

		//----------------------------------------------------------
		// 匯出csv檔
		//----------------------------------------------------------
		$query_order = $this->order->get_csv($srh_data);
			
		$order_detail_id = array();
			
		// output headers so that the file is downloaded rather than displayed
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=data.csv');

		// create a file pointer connected to the output stream
		$fp = fopen('php://output', 'w');
			
		//抬頭
		fputcsv($fp, array( big5_csv('訂單日期'),big5_csv('流水編'), big5_csv('訂單號碼'), big5_csv('持卡人'), big5_csv('收件人'), big5_csv('商品代碼'), big5_csv('商品名稱'), big5_csv('金額')
		, big5_csv('點數'), big5_csv('授權碼'), big5_csv('分期') , big5_csv('數量'), big5_csv('手機'), big5_csv('家裡電話'), big5_csv('收件地址'), big5_csv('統一編號')
		, big5_csv('發票抬頭'), big5_csv('出貨單號') , big5_csv('Email')) );

		//資料
		foreach ($query_order as $row){

			//拆解付款方式
			$pay_method_ary = get_payMethod_money_tpl_result2($row['pay_method'] ,$row['product_total'] ,$row['Auth_OffsetAmt']);

			$bonus = !empty($pay_method_ary['bonus']) ? $pay_method_ary['bonus'] : '';

			$money = !empty($pay_method_ary['money']) ? $pay_method_ary['money'] : '';

			$pay_method = !empty($pay_method_ary['pay_method']) ? $pay_method_ary['pay_method'] : '';

			//付款方式若為分期，則顯示總額
			if($pay_method == '3' || $pay_method == '6'){
				$money = addCommas($row['product_total']);
			}

			$product_ary = explode(',', $row['pay_method']);

			array_push($order_detail_id, $row['order_detail_id']);

			fputcsv($fp, array( big5_csv($row['cdate']),  //訂單日期
			big5_csv($row['order_detail_id']),  //流水編
			big5_csv($row['order_detail_show_id']),  //訂單號碼
                                 '',  //持卡人
			big5_csv($row['reci_name']),     //收件人
			big5_csv($product_ary[1]),    //商品代碼
			big5_csv($row['product_name']),  //商品名稱
			$money, //金額
			$bonus, //點數
			big5_csv($row['Auth_AuthCode']), //授權碼
			$pay_method, //分期
			big5_csv($row['entity']), //數量
			big5_csv($row['reci_mobile']), //手機
			big5_csv($row['reci_phone']),  //家裡電話
			big5_csv($row['reci_local'].$row['reci_address']), //收件地址
			big5_csv($row['ticket3_unified']), //統一編號
			big5_csv($row['ticket3_title']), //發票抬頭
			big5_csv($row['shipping_id']),  //出貨單號
			big5_csv($row['reci_email'])  //email
			));
		}

		fclose($fp);

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 訂單匯入 / 檢視
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function order_import_view(){

		//檢視view
		$this->load->view("backend/common/header.tpl");
		$this->load->view("backend/order/order_import_csv.tpl");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 訂單匯入 / csv
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function order_import_csv(){

		if ($_FILES["csv"]["error"] > 0){

			$fail = '';

			switch ($_FILES[$field]["error"]) {
				case UPLOAD_ERR_INI_SIZE:
					$fail = "*抱歉!檔案大小超出了伺服器上傳限制!";
					break;
				case UPLOAD_ERR_FORM_SIZE:
					$fail = "*抱歉!要上傳的檔案大小超出瀏覽器限制";
					break;
				case UPLOAD_ERR_PARTIAL:
					$fail = "*抱歉!檔案僅部分被上傳";
					break;
				case UPLOAD_ERR_NO_FILE:
					$fail = "*抱歉!檔案未能順利上傳!";
					break;
				case UPLOAD_ERR_NO_TMP_DIR:
					$fail = "*抱歉!伺服器臨時資料夾遺失!";
					break;
				case UPLOAD_ERR_CANT_WRITE:
					$fail = "*抱歉!檔案無法寫入硬碟!";
					break;
				case UPLOAD_ERR_EXTENSION:
					$fail = "File upload stopped by extension";
					break;

				default:
					$fail = "*抱歉!發生未知的檔案寫入錯誤!";
					break;
			}

		 dioRedirect('backend/product/order_import_view' ,$fail);
		 exit;

		}else{

			//只能允許csv
			if (strcasecmp($fext, "csv") != 0 ) {
				$data['fail'] = "* 抱歉!檔案格式需為 csv";
				return $data;
			}


			if ($_FILES["csv"]["size"] > 0) {
					
				//取得 csv檔案
				$file = $_FILES["csv"]["tmp_name"];
				$handle = fopen($file,"r");

				$i=0;
				while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

					if($i>0){
						//依流水編更新[出貨單號]&[狀態]
						$this->db->where('order_detail_id' ,addslashes($data[1]));
						$this->db->update('order_detail' ,array('shipping_id' => addslashes($data[16]) , 'status' => '9') );
					}

					$i=1;
				}

				dioRedirect('backend/order/order_import_view');
			}

		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 待處理訂單  / 匯出txt (未完成)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function order_export_txt(){

		//(一)匯出文字檔
		/*
		 $query_order = $this->center->get_order_csv();

		 $csv_ary = array();
		 $order_detail_id = array();

		 foreach ($query_order as $row){

		 array_push($order_detail_id, $row['order_detail_id']);

		 $csv_ary[] = $row['cdate'].','.
		 $row['order_detail_id'].','.
		 '-'.','.
		 $row['reci_name'].','.
		 $row['product_id'].','.
		 $row['product_name'].','.
		 $row['product_total'].','.
		 '0'.','.
		 $row['entity'].','.
		 $row['reci_mobile'].','.
		 $row['reci_phone'].','.
		 '-'.','.
		 $row['reci_local'].
		 $row['reci_address'].','.
		 $row['ticket3_unified'].','.
		 $row['ticket3_title'].','.
		 '-';
		 }

		 $fp = fopen('./application/views/resources/uploads/data/export'.date('Ymd_His').'.txt', 'a+');
		 foreach ( $csv_ary as $line ) {
		 $val = explode(",", $line);
		 fputcsv($fp, $val);
		 }
		 fclose($fp);
		 */
	}


	// --------------------------------------------------------------------

	/**
	 * 方法 : 待處理訂單  / 狀態編輯失敗
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function error_order_status(){

		//檢視view
		$this->load->view('backend/common/header.tpl');
		$this->load->view('backend/order/error_order_status.tpl');
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 待處理訂單  / 出貨
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function order_shipping(){

		//資料驗證
		$this->form_validation->set_rules('order_detail_show_id','*訂單編號','trim|required');
		$this->form_validation->set_rules('shipping_id','*出貨單號','trim|required');

		if ($this->form_validation->run() == TRUE){

			//將POST資料 ,轉為陣列
			$data = array(
                      'shipping_id' => $this->input->post('shipping_id'),
			               'status' => '9',
                            'sdate' => date('Y-m-d H:i:s')
			);

			$this->db->where('order_detail_id' ,$this->input->get('order_detail_id'));
			$this->db->update('order_detail' ,$data);

			dioRedirect('backend/order/lists?'.$_SERVER["QUERY_STRING"]);

		} else { //轉向預設頁面

			$this->db->where('order_detail_id' ,$this->input->get('order_detail_id'));
			$data['query'] = $this->db->get('order_detail')->row_array();

			//檢視view
			$this->load->view('backend/common/header.tpl' ,$data);
			$this->load->view('backend/order/order_shipping.tpl');
		}

	}
	
	function big5_csv($str)
	{
		return mb_convert_encoding($str , "Big5" , "UTF-8");
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 編輯明細
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit_detail(){
		
		$err_msg = "";
	
		parse_str($this->input->post('data') ,$post_data);
	
		//欄位驗證&規則
		if(empty($post_data['price']) || !is_numeric($post_data['price'])){
			$err_msg = "價格不可空白!";
			echo $err_msg;
			return false;
		}
	
		if(!is_numeric($post_data['entity'])){
			$err_msg = "數量請填入數字!";
			echo $err_msg;
			return false;
		}

		if(empty($post_data['total']) || !is_numeric($post_data['total'])){
			$err_msg = "小計不可空白!";
			echo $err_msg;
			return false;
		}
		
		if( ($post_data['price']*$post_data['entity']) != $post_data['total'] ){
			$err_msg = "小計有誤!";
			echo $err_msg;
			return false;			
		}
		
		$data = array(
				'order_detail_id'  => $post_data['order_detail_id'],
				'price'            => $post_data['price'],
				'entity'           => $post_data['entity'],
				'total'            => $post_data['total']
		);
	
		$this->db->where('order_detail_id' ,$this->input->post('order_detail_id'));
		$this->db->update('order_detail' ,$data);
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 編輯退貨
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function order_return(){
		
		$this->form_validation->set_rules('order_detail_id','*訂單明細編號','trim|required');
		$this->form_validation->set_rules('customer_id','*客戶編號','trim|required');
		$this->form_validation->set_rules('name','*姓名','trim|required');
		//$this->form_validation->set_rules('first_name','*姓','trim|required');
		//$this->form_validation->set_rules('last_name','*名','trim|required');
		//$this->form_validation->set_rules('mobile','*手機','trim|required');
		$this->form_validation->set_rules('phone','*電話','trim');
		$this->form_validation->set_rules('email','*Email','trim|required');
		$this->form_validation->set_rules('local','*區碼','trim|required');
		$this->form_validation->set_rules('address','*地址','trim|required');
		$this->form_validation->set_rules('entity','*數量','trim|required');
		$this->form_validation->set_rules('price','*價格','trim|required');
		$this->form_validation->set_rules('total','*小計','trim|required');
		$this->form_validation->set_rules('reason','*退(換)貨原因','trim|required');
				
		if ($this->form_validation->run() == TRUE){
			
			$is_entity_valid = $this->is_order_return_entity_valid($this->input->post('order_detail_id') ,$this->input->post('entity'));
			
			if(!$is_entity_valid){
				$this->session->set_flashdata('msg_err' ,'退貨數量不可大於訂購量!');
				redirect('backend/order/order_return?'.$_SERVER["QUERY_STRING"]);
				return false;
			}
			
			$data = array(
					'order_detail_id' => $this->input->post('order_detail_id'),
					'customer_id'     => $this->input->post('customer_id'),
					'name'            => $this->input->post('name'),
					//'first_name'      => $this->input->post('first_name'),
					//'last_name'       => $this->input->post('last_name'),
					//'mobile'          => $this->input->post('mobile'),
					'phone'           => $this->input->post('phone'),
					'email'           => $this->input->post('email'),
					'local'           => $this->input->post('local'),
					'address'         => $this->input->post('address'),
					'entity'          => $this->input->post('entity'),
					'price'           => $this->input->post('price'),
					'total'           => $this->input->post('total'),
					'reason'          => $this->input->post('reason'),
					'cdate'           => date('Y-m-d H:i:s')
			);

			$this->db->insert('return',$data);
			
			$this->session->set_flashdata('msg' ,DIO_MSG_SUCCESS_INSERT);
			
			redirect('backend/returns/lists');
		
		} else {
			
			$data['query'] = $this->order->shr_order_detail_info($this->input->get('order_detail_id'));
			
			$this->load->view("backend/common/header.tpl" ,$data);
			$this->load->view("backend/order/order_return.tpl");
		}
		 
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 退貨數量驗證
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function is_order_return_entity_valid($order_detail_id ,$entity){
		
		$sql = "SELECT entity - IFNULL( (SELECT SUM(entity) FROM `return` WHERE order_detail_id = {$order_detail_id}) ,0 ) as entity 
				   FROM order_detail WHERE order_detail_id = ".$order_detail_id;

		$query = $this->db->query($sql)->row_array();
		
		if($query['entity'] < $entity){
		   	return false;
		}else{
			return true;
		}
		
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 變更訂單狀態 /多筆 / 已出貨
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function multi_order_status_2(){
	
		$order_id_ary = explode(',', $this->input->get('aryList'));
	
		$this->db->where_in('order_id' ,$order_id_ary);
		$this->db->update('order' ,array('status' => '2'));
	
		//寄發Email
		/*
		foreach ($order_id_ary as $key=>$val){
			//避免phpMailer暫存，每次間隔都重新產生實體
			$dio_email    = new Dio_gmail();
				
			$info = $this->cls_order->getOrderInfoForEmail( $val );
			$dio_email->order($info);
		}*/
	
		redirect('backend/order/lists');
	}

}


/* End of file order.php */
/* Location: ./application/controllers/backend/center.php */