<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 店家管理(Ajax)
 *
 * 說明 : 提供Ajax專用的請求控制器
 * @controllerName Center
 * @author Dio
 *
 */
class Ajax extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		//載入模組
		$this->load->database();
		$this->load->helper(array('ctmall'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('category'));

		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
		
		//建立產品分類物件
		$this->cls_category = new category();
		
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 新增商品 / 父階分類
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function get_parent_category(){
			
		$data = $this->cls_category->get_1parent();
		echo json_encode($data);
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 新增商品 / 子階分類
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function get_child_category(){
			
		$data = $this->cls_category->get_2parent($this->input->get('category_id'));
		echo json_encode($data);
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 依付款方式計算金額
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	function get_payMethod_money_tpl(){

		$money = $this->input->get('price');

		$data[0] = ' 紅利點數<b>'.get_payMethod_money('bonus' ,$money ).'</b> 點';

		$bonus_money = ceil($money/2);
		$bonus = calc_bonus($bonus_money);
		$data[1] = ' 自付額<b>'.$bonus.'</b> 點 + <b>NT$'.addCommas($bonus_money).'</b>';
		$data[2] = ' 分期價<b>NT$'.addCommas(get_payMethod_money( 'staging_3' ,$money )).'</b> x 3期';
		$data[3] = ' 分期價<b>NT$'.addCommas(get_payMethod_money( 'staging_6' ,$money )).'</b> x 6期';

		echo json_encode($data);

	}


}


/* End of file center.php */
/* Location: ./application/controllers/ajax.php */