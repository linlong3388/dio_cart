<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 -  處理[最新消息分類]的控制器
 *            父分類項
 *            
 * @controllerName news_category
 * @author Dio
 *
 */
class news_category extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session','category'));
	
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
		
		//建立商品分類物件
		$this->cls_category = new category();
		
		if($this->input->get('page')){ //頁碼
		   $this->page = $this->input->get('page'); 	
		}else{
		   $this->page = 1;	
		}
		
	    $this->srh_page_per = 20; //每頁筆數	
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists () {
		
		$data['query'] = $this->db->get('news_category')->result_array();
		
		//檢視view
		$this->load->view("backend/common/header.tpl" ,$data);
		$this->load->view("backend/news/category/lists.tpl");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 :新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add () {
	
		$this->form_validation->set_rules('name','*分類名稱','trim|required');
		
		if ($this->form_validation->run() == TRUE){

			//將POST資料 ,轉為陣列
			$data = array(
                          'name' => $this->input->post('name'),
	  	                 'cdate' => date('Y-m-d H:i:s') 
			);

			$this->db->insert('news_category' ,$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
			
			redirect('backend/news_category/lists');

		} else { //轉向預設頁面
		
			//檢視view
			$this->load->view("backend/common/header.tpl");
			$this->load->view("backend/news/category/add.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit () {
	
		$this->form_validation->set_rules('news_category_id','*編號','trim|required');
		$this->form_validation->set_rules('name','*分類名稱','trim|required');
		$this->form_validation->set_rules('sort_order','*排序','trim|required');
		$this->form_validation->set_rules('status','*狀態','trim|required');
		$this->form_validation->set_rules('cdate','*建立日期','trim');
		$this->form_validation->set_rules('meta_title','*Meta標籤標題','trim');
		$this->form_validation->set_rules('meta_description','*Meta標籤描述','trim');
		$this->form_validation->set_rules('meta_keyword','*Meta標籤關鍵字','trim');
		
		if ($this->form_validation->run() == TRUE){

			//將POST資料 ,轉為陣列
			$data = array(
                      'name' => $this->input->post('name'),
			    'sort_order' => $this->input->post('sort_order'),
			        'status' => $this->input->post('status'),
                'udate' => date('Y-m-d H:i:s'),
					'meta_title'         => $this->input->post('meta_title'),
					'meta_description'   => $this->input->post('meta_description'),
					'meta_keyword'       => $this->input->post('meta_keyword'),
			);

			$this->db->where('news_category_id', $this->input->post('news_category_id'));
			$this->db->update('news_category',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
			
			redirect('backend/news_category/edit?news_category_id='.$this->input->post('news_category_id'));

		} else { 

			$this->db->where('news_category_id' ,$this->input->get('news_category_id'));
			$data['query'] = $this->db->get('news_category')->row_array();

			//檢視view
			$this->load->view("backend/common/header.tpl" ,$data);
			$this->load->view("backend/news/category/edit.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del () {

	   	if( $this->exist_category($this->input->get('news_category_id')) ){	
	   		
	   		$this->session->set_flashdata('msg_err' ,DIO_MSG_ERROR_CATEGORY);
	   		
	   		redirect('backend/news_category/lists');

    	}else{
			$this->db->delete('news_category' , array('news_category_id' => $this->input->get('news_category_id')));
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
			
			redirect('backend/news_category/lists');
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 判斷該分類是否有內容存在
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	function exist_category($news_category_id){
	
		$CI =& get_instance();
	
		$CI->db->where('news_category_id' ,$news_category_id);
		$query = $CI->db->get('news')->row_array();
	
		if(!empty($query['news_category_id'])){
			return true;
		}else{
			return false;
		}
	}	

}


/* End of file news_category.php */
/* Location: ./application/controllers/backend/news_category.php */