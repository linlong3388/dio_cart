<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理 [貨到付款 / 手續費]的控制器
 * @controllerName fee_cod
 * @author Dio
 *
 */
class fee_cod extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));
		
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit () {
		
		$this->form_validation->set_rules('fee_cod' ,'*貨到付款(手續費)','trim|required');

		if ($this->form_validation->run() == TRUE){

			$data = array( 'fee_cod' => $this->input->post('fee_cod') );

			$this->db->where('system_id', '1');
			$this->db->update('system',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);

			Redirect('backend/fee_cod/edit');

		} else { //轉向預設頁面

			$data['query']  = $this->db->get_where('system', array('system_id' => 1) )->row_array();
			
			//檢視view
			$this->load->view("backend/common/header.tpl",$data);
			$this->load->view("backend/fee_cod/edit.tpl");
		}

	}
	
	
}


/* End of file fee_cod.php */
/* Location: ./application/controllers/backend/fee_cod.php */