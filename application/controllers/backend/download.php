<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理 [檔案下載]的控制器
 * @controllerName download
 * @author Dio
 *
 */
class download extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));
		
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists () {

		$data['slt_status'] = $this->slt_status();
		$data['query'] = $this->db->get('download')->result_array();

		//檢視view
		$this->load->view("backend/common/header.tpl",$data);
		$this->load->view("backend/download/lists.tpl");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add () {

		//$this->form_validation->set_rules('download_category_id','* 分類ID','trim|required');
		$this->form_validation->set_rules('name','*標題','trim|required');
		$this->form_validation->set_rules('url1','*連結','trim');
		$this->form_validation->set_rules('image','*圖片','trim');
		$this->form_validation->set_rules('content','*內容','trim');
			
		if ($this->form_validation->run() == TRUE){

			$data = array(
					//'download_category_id' => $this->input->post('download_category_id'),
					     'name' => $this->input->post('name'),
					     'url1' => $this->input->post('url1'),
					    'image' => $this->input->post('image'),
					  'content' => $this->input->post('content'),
                        'cdate' => date('Y-m-d H:i:s') 
			        );

			$this->db->insert('download',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
			
			redirect('backend/download/lists');
			
		} else { 

			$this->load->view("backend/common/header.tpl");
			$this->load->view("backend/download/add.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit () {
		
		//$this->form_validation->set_rules('download_category_id','* 分類ID','trim|required');
		$this->form_validation->set_rules('download_id','*編號','trim|required');
		$this->form_validation->set_rules('name','*標題','trim|required');
		$this->form_validation->set_rules('url1','*連結','trim');
		$this->form_validation->set_rules('image','*圖片','trim');
		$this->form_validation->set_rules('content','*內容','trim');	
		$this->form_validation->set_rules('status','*狀態','trim|required');		
		$this->form_validation->set_rules('cdate','*建檔日期','trim|required');
		$this->form_validation->set_rules('sort_order','*排序','trim');
		$this->form_validation->set_rules('meta_title','*Meta標籤標題','trim');
		$this->form_validation->set_rules('meta_description','*Meta標籤描述','trim');
		$this->form_validation->set_rules('meta_keyword','*Meta標籤關鍵字','trim');

		if ($this->form_validation->run() == TRUE){

			$data = array(
					//'download_category_id' => $this->input->post('download_category_id'),
					      'name' => $this->input->post('name'),
					      'url1' => $this->input->post('url1'),
					     'image' => $this->input->post('image'),
					   'content' => $this->input->post('content'),	
					'sort_order' => $this->input->post('sort_order'),
					'meta_title' => $this->input->post('meta_title'),
			  'meta_description' => $this->input->post('meta_description'),
				 'meta_keyword'  => $this->input->post('meta_keyword'),
			            'status' => $this->input->post('status')
			        );

			$this->db->where('download_id', $this->input->post('download_id'));
			$this->db->update('download',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);

			Redirect('backend/download/edit?download_id='.$this->input->post('download_id'));

		} else { //轉向預設頁面

			$data['query']  = $this->db->get_where('download', array('download_id' => $this->input->get('download_id')) )->row_array();

			//檢視view
			$this->load->view("backend/common/header.tpl",$data);
			$this->load->view("backend//download/edit.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del () {
			
		$this->db->delete('download' , array('download_id' => $this->input->get('download_id')));
		
		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
		
		Redirect('backend/download/lists');
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 下拉式 / 狀態
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function slt_status () {
		
		return array(
		               '0' => '停用' ,
                       '1' => '啟用'
	            );
	}
	
}


/* End of file download.php */
/* Location: ./application/controllers/backend/download.php */