<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理 [聯絡我們]的控制器
 * @controllerName contact
 * @author Dio
 *
 */
class contact extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));
		
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists () {

		$data['slt_status'] = $this->slt_status();
		
		$sql = "SELECT c.* 
				      ,b.title as b_title
		          FROM `contact` c 
		        LEFT JOIN `branch` b ON c.branch_id = b.branch_id
		        ORDER BY c.cdate DESC";
		
		$data['query'] = $this->db->query($sql)->result_array();

		//檢視view
		$this->load->view("backend/common/header.tpl",$data);
		$this->load->view("backend/contact/lists.tpl");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add () {

		$this->form_validation->set_rules('contact_category_id','* 分類ID','trim|required');
		$this->form_validation->set_rules('title','*標題','trim|required');
		$this->form_validation->set_rules('content','*內容','trim|required');
			
		if ($this->form_validation->run() == TRUE){

			$data = array(
					'contact_category_id' => $this->input->post('contact_category_id'),
					    'title' => $this->input->post('title'),
					  'content' => $this->input->post('content'),
                        'cdate' => date('Y-m-d H:i:s') 
			        );

			$this->db->insert('contact',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
			
			redirect('backend/contact/lists');
			
		} else { 

			$this->load->view("backend/common/header.tpl");
			$this->load->view("backend/contact/add.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 檢視
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function view () {
		
		$this->form_validation->set_rules('status','*狀態','trim|required');
		$this->form_validation->set_rules('memo_admin','*管理員備註','trim');
			
		if ($this->form_validation->run() == TRUE){
			
			$data = array(
					'status'     => $this->input->post('status'),
					'memo_admin' => $this->input->post('memo_admin'));
			
			$this->db->where('contact_id', $this->input->get('contact_id'));
			$this->db->update('contact',$data);
				
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
			
			Redirect('backend/contact/view?contact_id='.$this->input->get('contact_id'));
			
		}else{
			
			$contact_id = $this->input->get('contact_id');
			
			$sql = "SELECT c.*
				      ,b.title as b_title
		          FROM `contact` c
		        LEFT JOIN `branch` b ON c.branch_id = b.branch_id
		        WHERE c.contact_id='".$contact_id."'";
			
			$data['query']  = $this->db->query($sql)->row_array();
			
			//$data['query']  = $this->db->get_where('contact', array('contact_id' => $this->input->get('contact_id')) )->row_array();
			
			//檢視view
			$this->load->view("backend/common/header.tpl",$data);
			$this->load->view("backend//contact/view.tpl");
		}
		
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del () {
			
		$this->db->delete('contact' , array('contact_id' => $this->input->get('contact_id')));
		
		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
		
		Redirect('backend/contact/lists');
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 下拉式 / 狀態
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function slt_status () {
		
		return array(
		               '0' => '停用' ,
                       '1' => '啟用'
	            );
	}
	
}


/* End of file contact.php */
/* Location: ./application/controllers/backend/contact.php */