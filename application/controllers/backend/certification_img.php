<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理 [食在安心 / 圖片]的控制器
 * @controllerName certification_img
 * @author Dio
 *
 */
class certification_img extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));
		
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
		
		//取得ID
		$this->common_picture_id = $this->input->get('common_picture_id');
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists () {
		
		$this->db->where('func' ,'certification_img');
		$this->db->order_by('cdate' ,'DESC');
		
		$data['query'] = $this->db->get('common_picture')->result_array();
		
		$data['slt_status']  = $this->slt_status();
		
		//檢視view
		$this->load->view("backend/common/header.tpl",$data);
		$this->load->view("backend/certification_img/lists.tpl");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add () {
		
		$this->form_validation->set_rules('title','*標題','trim');
		$this->form_validation->set_rules('image','*圖片','trim|required');
		$this->form_validation->set_rules('image_alt','*圖片說明','trim');
			
		if ($this->form_validation->run() == TRUE){
			
			$id = getCommonDataMaxFuncId('common_picture' ,'certification_img');
			
			$data = array(
					'func'   => 'certification_img',
					'id'     => $id,
					'title'  => $this->input->post('title'),
					'image'  => $this->input->post('image'),
					'image_alt'  => $this->input->post('image_alt'),
					'cdate' => date('Y-m-d H:i:s'),
			);
			
			$this->db->insert('common_picture',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
			
			redirect('backend/certification_img/lists');
			
		} else { 
		
			$this->load->view("backend/common/header.tpl");
			$this->load->view("backend/certification_img/add.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit () {
		
		$this->form_validation->set_rules('title','*標題','trim');
		$this->form_validation->set_rules('image','*主圖','trim|required');
		$this->form_validation->set_rules('image_alt','*圖片說明','trim');
		$this->form_validation->set_rules('sort_order','*排序','trim');
		$this->form_validation->set_rules('status','*狀態','trim|required');
		
		if ($this->form_validation->run() == TRUE){
			
			$data = array(
					'title'      => $this->input->post('title'),
					'image'      => $this->input->post('image'),
					'image_alt'  => $this->input->post('image_alt'),
					'sort_order' => $this->input->post('sort_order'),
					'status'     => $this->input->post('status')
			);
			
			$this->db->where('common_picture_id', $this->common_picture_id);
			$this->db->update('common_picture',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);

			redirect( getCurrentFullUrl() );

		} else { //轉向預設頁面
			
			$data['query']  = $this->db->get_where('common_picture', array('common_picture_id' => $this->common_picture_id) )->row_array();

			//檢視view
			$this->load->view("backend/common/header.tpl",$data);
			$this->load->view("backend/certification_img/edit.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del () {
			
		$this->db->where('common_picture_id' , $this->common_picture_id);
		$this->db->delete('common_picture');
		
		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
		
		redirect('backend/certification_img/lists');
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 下拉式 / 狀態
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function slt_status () {
		
		return array(  '0' => '停用' ,
                       '1' => '啟用' );
	}
	
}


/* End of file certification_img.php */
/* Location: ./application/controllers/backend/certification_img.php */