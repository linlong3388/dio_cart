<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 -  處理[商品組圖]的控制器
 * @controllerName product_comb
 * @author Dio
 *
 */
class product_comb extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid','elfinder'));
		$this->load->helper(array('dio_string','dio_message','dio_business'));
		$this->load->library(array('form_validation','session','Dio_paginator'));

		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
		
		$this->load->model("backend/product_model","model");
	}
		
	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists () {

		$data['product_id'] = $this->input->get('product_id');
		
		$this->db->where('product_id' ,$data['product_id']);
		$data['query'] = $this->db->get('product_comb')->result_array();
		
		//檢視view
		$this->load->view("backend/common/header.tpl" ,$data);
		$this->load->view("backend/product_comb/lists.tpl");
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add(){
		
		$product_id = $this->input->get('product_id');
		
		$this->form_validation->set_rules('unit','*禮盒類型','trim|required');
		$this->form_validation->set_rules('unit_pre','*每組','trim|required');
		$this->form_validation->set_rules('entity','*入','trim|required');
		//$this->form_validation->set_rules('price','*價格','trim|required');
		$this->form_validation->set_rules('image','*圖片','trim|required');
		$this->form_validation->set_rules('image_alt','*圖片說明','trim');
	
		if ($this->form_validation->run() == TRUE){
			
			$data = array(
					'product_id' => $this->input->post('product_id'),
					'unit'       => $this->input->post('unit'),
					'unit_pre'   => $this->input->post('unit_pre'),
					'entity'     => $this->input->post('entity'),
					//'price'      => $this->input->post('price'),
					'image'      => $this->input->post('image'),
					'image_alt'  => $this->input->post('image_alt'),
					'cdate'      => date('Y-m-d H:i:s')
			);
			
			$this->db->insert('product_comb',$data);
				
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
				
			redirect('backend/product_comb/lists?product_id='.$product_id.'page=1');
				
		}else{

			$data['product_id']     = $product_id;
			
			$data['query_category'] = $this->db->get('category')->result_array();
	
			$this->load->view('backend/common/header.tpl' ,$data);
			$this->load->view('backend/product_comb/add.tpl');
		}
	
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit(){

		$product_id      = $this->input->get('product_id');
		$product_comb_id = $this->input->post('product_comb_id');
		
		$this->form_validation->set_rules('unit','*禮盒類型','trim|required');
		$this->form_validation->set_rules('unit_pre','*每組','trim|required');
		$this->form_validation->set_rules('entity','*入','trim|required');
		//$this->form_validation->set_rules('price','*價格','trim|required');
		$this->form_validation->set_rules('image','*圖片','trim|required');
		$this->form_validation->set_rules('image_alt','*圖片說明','trim');
		$this->form_validation->set_rules('sort_order','*排序','trim');
		$this->form_validation->set_rules('status','*狀態','trim|required');
		
		if ($this->form_validation->run() == TRUE){

			$data = array(
					'unit'       => $this->input->post('unit'),
					'unit_pre'   => $this->input->post('unit_pre'),
					'entity'     => $this->input->post('entity'),
					//'price'      => $this->input->post('price'),
					'image'      => $this->input->post('image'),
					'image_alt'  => $this->input->post('image_alt'),
					'sort_order' => $this->input->post('sort_order'),
					'status'     => $this->input->post('status'),
			);

	 	    $this->db->where('product_comb_id', $product_comb_id);
	 	    $this->db->update('product_comb',$data);
	 	
	 	    $this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
	 	
	 	    redirect('backend/product_comb/edit?product_id='.$product_id.'&product_comb_id='.$product_comb_id);

	 } else { //轉向預設頁面
	 	
	 	$data['product_id']     = $product_id;
	 	
	 	$this->db->where('product_comb_id' ,$this->input->get('product_comb_id'));
	 	$data['query'] = $this->db->get('product_comb')->row_array();
	 	
	 	//檢視view
	 	$this->load->view('backend/common/header.tpl' ,$data);
	 	$this->load->view('backend/product_comb/edit.tpl');
	 }
	  
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del () {

		$this->db->delete('product_comb' ,array('product_comb_id' => $this->input->get('product_comb_id')));

		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
		
		redirect('backend/product_comb/lists?product_id='.$this->input->get('product_id'));
	}

}


/* End of file product_comb.php */
/* Location: ./application/controllers/backend/product_comb.php */