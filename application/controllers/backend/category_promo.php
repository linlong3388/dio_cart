<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 -  處理[商品促銷分類]的控制器
 *            
 * @controllerName category_promo
 * @author Dio
 *
 */
class category_promo extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session','category'));
	
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
		
		//建立商品分類物件
		$this->cls_category = new category();
		
		//載入model
		$this->load->model("backend/category_promo_model" ,"category_promo");
		
		if($this->input->get('page')){ //頁碼
		   $this->page = $this->input->get('page'); 	
		}else{
		   $this->page = 1;	
		}
		
	    $this->srh_page_per = 20; //每頁筆數	
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists () {
		
		$data['query'] = $this->category_promo->getProductList();
		
		//檢視view
		$this->load->view("backend/common/header.tpl" ,$data);
		$this->load->view("backend/category_promo/lists.tpl");
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 :新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add () {
	
		$this->form_validation->set_rules('name','*標題','trim');
		$this->form_validation->set_rules('image','*圖片','trim|required');
		$this->form_validation->set_rules('sdate','*活動日期(起)','trim|required');
		$this->form_validation->set_rules('edate','*活動日期(迄)','trim|required');
		$this->form_validation->set_rules('rate_price','*折扣(%)','trim|required');
		$this->form_validation->set_rules('rate_entity','*可購數量(%)','trim|required');
		
		if ($this->form_validation->run() == TRUE){

			$data = array(
                         'name' => $this->input->post('name'),
					     'sdate' => $this->input->post('sdate'),
					     'edate' => $this->input->post('edate'),
					     'image' => $this->input->post('image'),
					     'rate_price' => $this->input->post('rate_price'),
					     'rate_entity' => $this->input->post('rate_entity'),
				     	 'cdate' => date('Y-m-d H:i:s') 
			);

			$this->db->insert('category_promo' ,$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
			
			redirect('backend/category_promo/lists');

		} else { //轉向預設頁面
		
			//檢視view
			$this->load->view("backend/common/header.tpl");
			$this->load->view("backend/category_promo/add.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit () {
	
		$this->form_validation->set_rules('category_promo_id','*編號','trim|required');
		$this->form_validation->set_rules('name','*標題','trim');
		$this->form_validation->set_rules('image','*圖片','trim|required');
		$this->form_validation->set_rules('sdate','*活動日期(起)','trim|required');
		$this->form_validation->set_rules('edate','*活動日期(迄)','trim|required');
		$this->form_validation->set_rules('rate_price','*折扣(%)','trim|required');
		$this->form_validation->set_rules('rate_entity','*可購數量(%)','trim|required');
		$this->form_validation->set_rules('sort_order','*排序','trim');
		$this->form_validation->set_rules('status','*狀態','trim|required');
		$this->form_validation->set_rules('meta_title','*Meta標籤標題','trim');
		$this->form_validation->set_rules('meta_description','*Meta標籤描述','trim');
		$this->form_validation->set_rules('meta_keyword','*Meta標籤關鍵字','trim');
		
		if ($this->form_validation->run() == TRUE){

			$data = array(
                        'name' => $this->input->post('name'),
					   'sdate' => $this->input->post('sdate'),
					   'edate' => $this->input->post('edate'),
					   'image' => $this->input->post('image'),
				  'sort_order' => $this->input->post('sort_order'),
			          'status' => $this->input->post('status'),
				  'rate_price' => $this->input->post('rate_price'),
				 'rate_entity' => $this->input->post('rate_entity'),
               	  'meta_title' => $this->input->post('meta_title'),
	     	'meta_description' => $this->input->post('meta_description'),
			    'meta_keyword' => $this->input->post('meta_keyword'),
			);
			
			$this->db->where('category_promo_id', $this->input->post('category_promo_id'));
			$this->db->update('category_promo',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
			
			redirect('backend/category_promo/edit?category_promo_id='.$this->input->post('category_promo_id'));

		} else { 

			$this->db->where('category_promo_id' ,$this->input->get('category_promo_id'));
			$data['query'] = $this->db->get('category_promo')->row_array();

			//檢視view
			$this->load->view("backend/common/header.tpl" ,$data);
			$this->load->view("backend/category_promo/edit.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del () {

	   	if( $this->exist_category($this->input->get('category_promo_id')) ){	
			
			$this->session->set_flashdata('msg_err' ,DIO_MSG_ERROR_CATEGORY);

			redirect('backend/category_promo/lists');

    	}else{
			$this->db->delete('category_promo' , array('category_promo_id' => $this->input->get('category_promo_id')));
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
			
			redirect('backend/category_promo/lists');
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 判斷該分類是否有內容存在
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	function exist_category($category_promo_id){
	
		$CI =& get_instance();
	
		$CI->db->where('category_promo_id' ,$category_promo_id);
		$query = $CI->db->get('category_promo_product')->row_array();
	
		if(!empty($query['category_promo_id'])){
			return true;
		}else{
			return false;
		}
	}	

}


/* End of file category_promo.php */
/* Location: ./application/controllers/backend/category_promo.php */