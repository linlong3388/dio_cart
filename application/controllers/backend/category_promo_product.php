<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 -  處理[商品促銷分類 / 商品明細]的控制器
 *            
 * @controllerName category_promo_product
 * @author Dio
 *
 */
class category_promo_product extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session','category'));
	
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
		
		//建立商品分類物件
		$this->cls_category = new category();
		
		$this->load->model("backend/product_model","product");
		
		if($this->input->get('page')){ //頁碼
		   $this->page = $this->input->get('page'); 	
		}else{
		   $this->page = 1;	
		}
		
	    $this->srh_page_per = 100; //每頁筆數	
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists () {
		
		$srh_data = $this->search($this->input->get());
		
		$data['category_promo_id'] = $srh_data['category_promo_id'];
		$product_data              = $this->product->shr_product($srh_data);
		
		$data['product'] = $this->product_select( $srh_data['category_promo_id'] ,$product_data );
		
		$this->db->where('category_promo_id' ,$this->input->get('category_promo_id'));
		$data['query'] = $this->db->get('category_promo')->row_array();
		
		//檢視view
		$this->load->view("backend/common/header.tpl" ,$data);
		$this->load->view("backend/category_promo_product/lists.tpl");
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 取得商品列表 / 已註記
	 *
	 * @access	public
	 * @param   商品資料
	 * @return
	 */
	public function product_select ($category_promo_id ,$param) {
		
		//促銷分類商品明細
		$this->db->where('category_promo_id' ,$category_promo_id);
		$query = $this->db->get('category_promo_product')->result_array();
		
		$query_product_ids = array();
		
		foreach ($query as $row) {
			array_push($query_product_ids, $row['product_id']);
		}
	
		foreach ($param as $key=>$row) {
			if( in_array($row['product_id'], $query_product_ids) ){
				$param[$key]['isSelect'] = 1;
			}else{
				$param[$key]['isSelect'] = 0;
			}
		}
		
		return $param;
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 搜尋
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function search ($data) {
		
		if(isset($data) && !empty($data)){
		   foreach ($data as $key=>$val) {
             if($val == ''){
		      	unset($data[$key]);
             }
           }
		}
		
		$data['srh_page_per'] = $this->srh_page_per;
		$data['srh_limit_page'] = $this->page;
		
		return $data;
	}

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 多筆/ 選取商品
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function multi_select(){
		
		$category_promo_id = $this->input->get('category_promo_id');
		$product_id_ary    = explode(',', $this->input->get('aryList'));

		//刪除商品
		$this->db->delete('category_promo_product' , array('category_promo_id' => $category_promo_id));
		
		//選取商品
		$this->db->where_in('product_id' ,$product_id_ary);
		$query = $this->db->get('product')->result_array();
		
		foreach ($query as $row){
			//加入商品
			$data = array(
					    'category_promo_id' => $category_promo_id ,
					    'product_id'        => $row['product_id'] ,
					    'sort_order'        => $row['sort_order'] ,
					    'status'            => $row['status'] ,
					    'cdate'             => date('Y-m-d H:i:s') ,
				    );
			
			$this->db->insert('category_promo_product',$data);
		}
		
		redirect('backend/category_promo_product/lists?category_promo_id='.$category_promo_id);
	
	}

}


/* End of file category_promo_product.php */
/* Location: ./application/controllers/backend/category_promo_product.php */