<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理[關於我們]的控制器
 * @controllerName about
 * @author Dio
 *
 */
class about extends BackEnd_Controller {
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
		
		parent::__construct();
		
		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));
		
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
	}

   // --------------------------------------------------------------------
	
   /**
	* 方法 : 列表
	*
	* @access	public
	* @param
	* @return
	*/
	public function lists () {
	
        $data['query'] =  $this->db->get('about')->result_array();
	
		//檢視view
		$this->load->view("backend/common/header.tpl",$data);
		$this->load->view("backend/about/lists.tpl");
	}
	
	// --------------------------------------------------------------------
	
	/**
	* 方法 : 新增
	*
	* @access	public
	* @param
	* @return
	*/
	public function add () {
	
	   $this->form_validation->set_rules('title','*標題','trim|required');
	   //$this->form_validation->set_rules('about_category_id','*分類','trim|required');
	   $this->form_validation->set_rules('content','*內容','trim|required');
	   $this->form_validation->set_rules('image','*banner橫幅','trim');
	   //$this->form_validation->set_rules('image_alt','*圖片說明','trim');
	   
	   if ($this->form_validation->run() == TRUE){
	
		   $data = array(
				'title'               => $this->input->post('title'),
		   		//'about_category_id' => $this->input->post('about_category_id'),
		   		'content'             => $this->input->post('content'),
		   		'cdate'               => date('Y-m-d H:i:s'),
		   		'image'               => $this->input->post('image'),
				//'image_alt' => $this->input->post('image_alt'),
		   );
	
		   $this->db->insert('about',$data);
		   
		   $this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
		   	
		   redirect('backend/about/lists');
		  
		} else { 

			$this->load->view("backend/common/header.tpl");
			$this->load->view("backend/about/add.tpl");
		}
	
	}
	
   // --------------------------------------------------------------------
	
  /**
   * 方法 : 編輯
   *
   * @access	public
   * @param
   * @return
   */
  public function edit () {
  
	 //$this->form_validation->set_rules('title','*標題','trim|required');
	 //$this->form_validation->set_rules('about_category_id','*分類','trim|required');
	 $this->form_validation->set_rules('content','*內容','trim|required');
	 $this->form_validation->set_rules('sort_order','*排序','trim');
     $this->form_validation->set_rules('image','*橫幅圖片','trim');
	 //$this->form_validation->set_rules('image_alt','*圖片說明','trim');
     $this->form_validation->set_rules('meta_title','*Meta標籤標題','trim');
     $this->form_validation->set_rules('meta_description','*Meta標籤描述','trim');
     $this->form_validation->set_rules('meta_keyword','*Meta標籤關鍵字','trim');
	
	 if ($this->form_validation->run() == TRUE){
	
	 	$data = array(
	              //'title'               => $this->input->post('title'),
	 			  //'about_category_id' => $this->input->post('about_category_id'),
	 			  'content'             => $this->input->post('content'),
	 			  'sort_order'          => $this->input->post('sort_order'),
	 			  'image'               => $this->input->post('image'),
	          //'image_alt' => $this->input->post('image_alt'),
	 			'meta_title'         => $this->input->post('meta_title'),
	 			'meta_description'   => $this->input->post('meta_description'),
	 			'meta_keyword'       => $this->input->post('meta_keyword'),
	    );
	
	    $this->db->where('about_id', $this->input->post('about_id'));
		$this->db->update('about',$data);
	
		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
		
		redirect('backend/about/edit?about_id='.$this->input->post('about_id'));
		
	 } else { 
	
	 	$data['query'] = $this->db->get_where('about', array('about_id' => $this->input->get('about_id')) )->row_array();
	        							
		$this->load->view("backend/common/header.tpl" ,$data);
		$this->load->view("backend/about/edit.tpl");
	}
	
  }
	
  // --------------------------------------------------------------------
	
  /**
   * 方法 : 刪除
   *
   * @access	public
   * @param
   * @return
   */
  public function del() {
	
	 $this->db->delete('about' ,array('about_id' => $this->input->get('about_id')));
	 
	 $this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
	 
	 redirect('backend/about/lists');
  }
	
	
}


/* End of file about.php */
/* Location: ./application/controllers/about.php */