<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理 [金流]的控制器
 * @controllerName cash
 * @author Dio
 *
 */
class cash extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));
		
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : ATM轉帳
	 *       (讀寫 XML 格式)
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function atm () {
		
		$this->form_validation->set_rules('cash_id','*編號','trim|required');
		$this->form_validation->set_rules('content','*內容','trim|required');
        $this->form_validation->set_rules('status','*狀態','trim|required');
		$this->form_validation->set_rules('cdate','*建檔日期','trim|required');

		if ($this->form_validation->run() == TRUE){

			$data = array(
                       'content' => $this->input->post('content'),
			            'status' => $this->input->post('status'),
                         'cdate' => $this->input->post('cdate')
			        );

			$this->db->where('cash_id', $this->input->post('cash_id'));
			$this->db->update('cash',$data);

			dioRedirect('backend/cash/lists');

		} else { //轉向預設頁面

			$data['query']  = $this->db->get_where('cash', array('cash_id' => $this->input->get('cash_id')) )->row_array();

			//檢視view
			$this->load->view("backend/common/header.tpl",$data);
			$this->load->view("backend/system/cash/edit.tpl");
		}

	}

	
	
}


/* End of file cash.php */
/* Location: ./application/controllers/backend/cash.php */