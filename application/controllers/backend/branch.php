<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理[服務據點]的控制器
 * @controllerName branch
 * @author Dio
 *
 */
class branch extends BackEnd_Controller {
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));
		
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
	}

   // --------------------------------------------------------------------
	
   /**
	* 方法 : 列表
    *
	* @access	public
	* @param
	* @return
	*/
	public function lists () {
		
		$this->db->order_by('cdate' ,'DESC');
		$data['query'] = $this->db->get('branch')->result_array();
	
		$this->load->view("backend/common/header.tpl" ,$data);
		$this->load->view("backend/branch/lists.tpl");
	}
	
	// --------------------------------------------------------------------
	
	/**
	* 方法 : 新增
	*
	* @access	public
	 * @param
	 * @return
	 */
	 public function add () {
	
 	    //$this->form_validation->set_rules('branch_category_id','*分類','trim|required');
		$this->form_validation->set_rules('title','*服務據點','trim|required');
		$this->form_validation->set_rules('phone','*電話','trim|required');
		$this->form_validation->set_rules('mobile','*手機','trim');
		$this->form_validation->set_rules('fax','*傳真','trim');
		$this->form_validation->set_rules('local','*區碼','trim|required');
		$this->form_validation->set_rules('addr','*地址','trim|required');
		$this->form_validation->set_rules('link','*相關連結','trim');
		$this->form_validation->set_rules('image','*圖片','trim');
		$this->form_validation->set_rules('action_time','*營業時間','trim');
		$this->form_validation->set_rules('content','*徵才資訊','trim');
		$this->form_validation->set_rules('username','*管理員帳號','trim|required');
		//$this->form_validation->set_rules('latitude','*緯度','trim|required');
		//$this->form_validation->set_rules('longitude','*經度','trim|required');
	
		if ($this->form_validation->run() == TRUE){
	
			$data = array(
		//'branch_category_id' => $this->input->post('branch_category_id'),
					 'title' => $this->input->post('title'),
					 'local' => $this->input->post('local'),
					  'addr' => $this->input->post('addr'),
			 		 'phone' => $this->input->post('phone'),
				 	'mobile' => $this->input->post('mobile'),
				       'fax' => $this->input->post('fax'),
					  'link' => $this->input->post('link'),
					 'image' => $this->input->post('image'),
			   'action_time' => $this->input->post('action_time'),
				   'content' => $this->input->post('content'),
				  'username' => $this->input->post('username'),
				//'latitude' => $this->input->post('latitude'),
				 //'longitude' => $this->input->post('longitude'),
				 	 'cdate' => date('Y-m-d H:i:s')
		    );
			    	
			$this->db->insert('branch' ,$data);

			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
			
			redirect('backend/branch/lists');
	
		} else { 
	
			$data['query']['image'] = '#';
						
			$this->load->view("backend/common/header.tpl" ,$data);
	        $this->load->view("backend/branch/add.tpl");
		}
	
	}
	
	// --------------------------------------------------------------------
	
	/**
	* 方法 : 編輯
	 *
	 * @access	public
	* @param
	* @return
	 */
	public function edit () {
	
		//$this->form_validation->set_rules('branch_category_id','*分類','trim|required');
		$this->form_validation->set_rules('title','*服務據點','trim|required');
		$this->form_validation->set_rules('phone','*電話','trim|required');
		$this->form_validation->set_rules('mobile','*手機','trim');
		$this->form_validation->set_rules('fax','*傳真','trim');
		$this->form_validation->set_rules('local','*區碼','trim|required');
		$this->form_validation->set_rules('addr','*地址','trim|required');
		$this->form_validation->set_rules('link','*相關連結','trim');
		$this->form_validation->set_rules('image','*圖片','trim');	
		$this->form_validation->set_rules('action_time','*營業時間','trim');
		$this->form_validation->set_rules('content','*徵才資訊','trim');
		//$this->form_validation->set_rules('latitude','*緯度','trim|required');
		//$this->form_validation->set_rules('longitude','*經度','trim|required');
		$this->form_validation->set_rules('sort_order','*排序','trim|required');
		$this->form_validation->set_rules('username','*管理員帳號','trim|required');
		$this->form_validation->set_rules('meta_title','*Meta標籤標題','trim');
		$this->form_validation->set_rules('meta_description','*Meta標籤描述','trim');
		$this->form_validation->set_rules('meta_keyword','*Meta標籤關鍵字','trim');
	
		if ($this->form_validation->run() == TRUE){
		
			$data = array(
		//'branch_category_id' => $this->input->post('branch_category_id'),
					 'title' => $this->input->post('title'),
					 'local' => $this->input->post('local'),
				   	  'addr' => $this->input->post('addr'),
				 	 'phone' => $this->input->post('phone'),
					'mobile' => $this->input->post('mobile'),
				       'fax' => $this->input->post('fax'),
					  'link' => $this->input->post('link'),
				 	 'image' => $this->input->post('image'),
			   'action_time' => $this->input->post('action_time'),
				   'content' => $this->input->post('content'),
				  //'latitude' => $this->input->post('latitude'),
				 //'longitude' => $this->input->post('longitude'),
				'sort_order' => $this->input->post('sort_order'),
				  'username' => $this->input->post('username'),
			    'meta_title' => $this->input->post('meta_title'),
		  'meta_description' => $this->input->post('meta_description'),
			  'meta_keyword' => $this->input->post('meta_keyword'),
			);			
		
			$this->db->where('branch_id', $this->input->get('branch_id'));
			$this->db->update('branch',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
				
			redirect('backend/branch/edit?branch_id='.$this->input->get('branch_id'));
	
		} else { //轉向預設頁面
	
			$data['query'] = $this->db->get_where('branch', array('branch_id' => $this->input->get('branch_id')) )->row_array();
	
			$this->load->view("backend/common/header.tpl" ,$data);
			$this->load->view("backend/branch/edit.tpl");
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 編輯密碼
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function pass () {
	
		$this->form_validation->set_rules('username', '*帳號', 'trim|required');
		$this->form_validation->set_rules('password', '*更改密碼', 'trim|required|matches[passconf]|md5');
		$this->form_validation->set_rules('passconf', '*密碼確認', 'trim|required');
	
		if ($this->form_validation->run() == TRUE){
				
			$data = array(
					'password' => $this->input->post('password')
			);
	
			$this->db->where('username', $this->input->post('username'));
			$this->db->update('branch',$data);
				
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
				
			redirect('backend/branch/pass?'.$_SERVER['QUERY_STRING']);
	
		}else{
	
			$data['query']  = $this->db->get_where('branch', array('branch_id' => $this->input->get('branch_id')) )->row_array();
	
			//檢視view
			$this->load->view("backend/common/header.tpl" ,$data);
			$this->load->view("backend/branch/pass.tpl");
		}
	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del () {
	
		$this->db->delete('branch' ,array('branch_id' => $this->input->get('branch_id')));
		
		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
			
		redirect('backend/branch/lists');
	}
	
}


/* End of file branch.php */
/* Location: ./application/controllers/branch.php */