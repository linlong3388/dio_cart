<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 後台管理首頁  for 行銷
 * @controllerName promo
 * @author Dio
 *
 */
class promo extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session','category'));
	
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
		
		//建立商品分類物件
		$this->cls_category = new category();
		
	}	

	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 回饋金 / 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function feedback_gold_update () {
	
		$this->form_validation->set_rules('total','*總金額','trim|required');
		$this->form_validation->set_rules('money','*回饋金額','trim|required');
    	$this->form_validation->set_rules('money_limit','*回饋金額/上限','trim');
		//$this->form_validation->set_rules('use_date','*使用期限','trim');
		$this->form_validation->set_rules('status','*狀態','trim|required');
		
		if ($this->form_validation->run() == TRUE){
				
			$data = array(
					'total'       => $this->input->post('total'),
					'money'       => $this->input->post('money'),
					'money_limit' => $this->input->post('money_limit'),
					//'use_date'    => $this->input->post('use_date'),
					'status'      => $this->input->post('status')
			);
				
			$this->db->update('promo_feedback_gold',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
			
			redirect('backend/promo/feedback_gold_update?feedback_gold_id='.$this->input->post('feedback_gold_id'));
			
		} else {
	
			$this->db->limit(1);
			$data['query']  = $this->db->get_where('promo_feedback_gold')->row_array();
	
			$this->load->view("backend/common/header.tpl",$data);
			$this->load->view("backend/promo/feedback_gold_update.tpl");
		}
	
	}
	
	
}

/* End of file promo.php */
/* Location: ./application/controllers/promo.php */