<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理[儀表板]的控制器
 * @controllerName dashboard
 * @author Dio
 *
 */
class dashboard extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();
		
		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string' ,'dio_message'));
		$this->load->library(array('form_validation','session'));

		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
		
		//載入model
		$this->load->model("backend/dashboard_model","model");
		$this->load->model("backend/order_model","order");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 首頁
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function main()
	{	
		//訂單資料
		$query_order = $this->model->get_order_status_group();

		$data['query_order_count_1'] = 0;
		$data['query_order_count_3'] = 0;

		foreach ($query_order as $row) {
			if($row['status'] == 1){
				$data['query_order_count_1'] = $row['count'];
			}elseif($row['status'] == 3){
				$data['query_order_count_3'] = $row['count'];
			}
		}

		//預購訂單
		$this->db->where('status' ,'0');
		$data['query_pre_order'] = $this->db->count_all_results('order_pre');
		
		//商品資料
		$this->db->where('type_id' ,0);
		$data['query_product'] = $this->db->count_all_results('product');

		//最新10筆訂單
		$data['query_order_new'] = $this->order->shr_order(array('srh_page_per' => 10));

		//連絡我們
		$this->db->where('status' ,'0');
		$data['query_contact'] = $this->db->count_all_results('contact');
		
		//檢視view
		$this->load->view("backend/common/header.tpl" ,$data);
		$this->load->view("backend/dashboard/main.tpl");
	}

}


/* End of file dashboard.php */
/* Location: ./application/controllers/backend/dashboard.php */