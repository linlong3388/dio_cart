<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理 [公告]的控制器
 * @controllerName notice
 * @author Dio
 *
 */
class notice extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));
		
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists () {

		$data['slt_status'] = $this->slt_status();
		$data['query'] = $this->db->get('notice')->result_array();

		//檢視view
		$this->load->view("backend/common/header.tpl",$data);
		$this->load->view("backend/system/notice/lists.tpl");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add () {

		//資料驗證
		$this->form_validation->set_rules('content','*訊息','trim|required');
			
		if ($this->form_validation->run() == TRUE){

			$data = array(
                      'content' => $this->input->post('content'),
                        'cdate' => date('Y-m-d H:i:s') 
			        );

			$this->db->insert('notice',$data);
			dioRedirect('backend/notice/lists');

		} else { 

			$this->load->view("backend/common/header.tpl");
			$this->load->view("backend/system/notice/add.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit () {
		
		$this->form_validation->set_rules('notice_id','*編號','trim|required');
		$this->form_validation->set_rules('content','*內容','trim|required');
        $this->form_validation->set_rules('status','*狀態','trim|required');
		$this->form_validation->set_rules('cdate','*建檔日期','trim|required');

		if ($this->form_validation->run() == TRUE){

			$data = array(
                       'content' => $this->input->post('content'),
			            'status' => $this->input->post('status'),
                         'cdate' => $this->input->post('cdate')
			        );

			$this->db->where('notice_id', $this->input->post('notice_id'));
			$this->db->update('notice',$data);

			dioRedirect('backend/notice/lists');

		} else { //轉向預設頁面

			$data['query']  = $this->db->get_where('notice', array('notice_id' => $this->input->get('notice_id')) )->row_array();

			//檢視view
			$this->load->view("backend/common/header.tpl",$data);
			$this->load->view("backend/system/notice/edit.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del () {
			
		$this->db->delete('notice' , array('notice_id' => $this->input->get('notice_id')));
		dioRedirect('backend/notice/lists');
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 下拉式 / 狀態
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function slt_status () {
		
		return array(
		               '0' => '停用' ,
                       '1' => '啟用'
	            );
	}
	
}


/* End of file notice.php */
/* Location: ./application/controllers/backend/notice.php */