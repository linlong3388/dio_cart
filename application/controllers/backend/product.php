<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 -  處理[商品]的控制器
 * @controllerName product
 * @author Dio
 *
 */
class product extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid','elfinder'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session','category','property','Dio_paginator'));

		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
		
		$this->load->model("backend/product_model","model");
		
		$this->cls_category = new category();		
		
		$this->cls_property = new property();
		$this->cls_property->_set('property');
				
		$this->initial();
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 參數初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	private function initial(){
		
		//分頁
		$this->page = $this->input->get('page');
		$this->srh_page_per = 10;

		//排序欄位
		$this->slt_sort_by = array(
		               		 '' => '--' ,
				  'category_id' => '分類' ,
				          'sku' => 'sku' ,
				         'name' => '商品名稱' ,
				        'price' => '價格' ,
				        'stock' => '庫存量' ,
				   'sort_order' => '排序' ,
				       'status' => '狀態' ,
				        'cdate' => '建立日期'
		 				 );
		
		//下拉式 / 狀態欄位
		$this->slt_status = array(
					        '0' => '停用' ,
					        '1' => '啟用'
			            );
		
		//下拉式 / 分類欄位
		$this->slt_category = array();
		
		foreach ($this->cls_category->get_last_parent() as $row) {
			$this->slt_category[$row['category_id']] = $this->cls_category->get_name($row['path']);
		}
		
	}
		
	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists () {
		
		$srh_data = $this->search($this->input->get());
		
		$data['query'] = $this->model->shr_product($srh_data);
		
		$data['slt_sort_by']  = $this->slt_sort_by;
		$data['slt_status']   = sort_array($this->slt_status ,4 ,$this->input->get('srh_status'));
		$data['slt_category'] = sort_array($this->slt_category ,4 ,$this->input->get('srh_category'));
		
		//----------------------------------------------------------------------
		// 設定分頁
		//----------------------------------------------------------------------
		$query_group_by_ary = $this->model->shr_product_group_by($srh_data);

		$pages = new Dio_paginator();
		$pages->set($query_group_by_ary[0]['count'],5,array(10,3,6,9,12,25,50,100,250,'All'));

		$data['pages'] = $pages;
		$data['query_group_by'] = $query_group_by_ary;
		$data['page_startEnd'] = $pages->get_startEnd_page($this->page ,$query_group_by_ary[0]['count'] ,$this->srh_page_per);
					
		//檢視view
		$this->load->view("backend/common/header.tpl" ,$data);
		$this->load->view("backend/product/lists.tpl");
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 搜尋
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function search ($data) {
		
		if(isset($data) && !empty($data)){
		   foreach ($data as $key=>$val) {
             if($val == ''){
		      	unset($data[$key]);
             }
           }
		}
		
		$data['srh_page_per'] = $this->srh_page_per;
		$data['srh_limit_page'] = $this->page;
		
		return $data;
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit(){

		//指定商品驗證
		$is_no_fee_param = $this->input->post('is_no_fee_entity')
		                   .','.$this->input->post('is_no_fee_price')
		                   .','.$this->input->post('is_no_fee_sdate')
		                   .','.$this->input->post('is_no_fee_edate');
		
		//$this->form_validation->set_rules('group_id','* 群組分類','trim|required');
		//$this->form_validation->set_rules('category_id','*商品類別','trim|required');
		$this->form_validation->set_rules('category_multi_id[]','*商品類別','trim|required');
		$this->form_validation->set_rules('fee_category_id','*運費類別','trim|required');
		//$this->form_validation->set_rules('barcode','*國際條碼','callback_check_is_barcode');
		//$this->form_validation->set_rules('ad_id','*廣告類別','trim');
		$this->form_validation->set_rules('brand_id','*品牌分類','trim');
		$this->form_validation->set_rules('name','*名稱','trim|required');
		$this->form_validation->set_rules('price_original','*建議售價','trim');
		$this->form_validation->set_rules('price','*活動售價','trim|required');
		$this->form_validation->set_rules('promo_msg','*促銷訊息','trim');
		$this->form_validation->set_rules('branch','*展售店','trim');
		$this->form_validation->set_rules('attr1','*規格1','trim');
		$this->form_validation->set_rules('attr2','*規格2','trim');
		$this->form_validation->set_rules('delivery','*運送服務','trim');
		$this->form_validation->set_rules('delivery_msg','*運送服務內容','trim');
		$this->form_validation->set_rules('unit','*單位','trim');
		$this->form_validation->set_rules('image','*主圖','trim');	
		$this->form_validation->set_rules('ps_stock','*庫存量','trim|required');
		$this->form_validation->set_rules('is_comb','*是否為組合商品','trim');
		$this->form_validation->set_rules('is_show_home','*是否顯示在首頁','trim');
		$this->form_validation->set_rules('keyword','*關鍵字','trim');
		$this->form_validation->set_rules('is_no_fee','*指定商品免運','callback_check_is_no_fee['.$is_no_fee_param.']');
		$this->form_validation->set_rules('is_no_fee_entity','*指定商品免運(數量)','trim');
		$this->form_validation->set_rules('is_no_fee_price','*指定商品免運(金額)','trim');
		$this->form_validation->set_rules('is_no_fee_sdate','*指定商品免運(日期(起))','trim');
		$this->form_validation->set_rules('is_no_fee_edate','*指定商品免運(日期(迄))','trim');
		$this->form_validation->set_rules('status','*狀態','trim|required');
		$this->form_validation->set_rules('description','*詳細資訊','trim');
		$this->form_validation->set_rules('specification','*產品規格','trim');
		$this->form_validation->set_rules('techenique','*技術','trim');
		$this->form_validation->set_rules('notice','*注意事項','trim');
		$this->form_validation->set_rules('sort_order','*排序','trim|required');
		$this->form_validation->set_rules('meta_title','*Meta標籤標題','trim');
		$this->form_validation->set_rules('meta_description','*Meta標籤描述','trim');
		$this->form_validation->set_rules('meta_keyword','*Meta標籤關鍵字','trim');
			
		if ($this->form_validation->run() == TRUE){

			//驗證商品規格
			/*
			$this->db->where('product_id' ,$this->input->post('product_id'));
			$this->db->limit(1);
			$query = $this->db->get('product_speci')->row_array();
			
			if(empty($query)){
				echo "<script>alert('*請建立商品規格表和數量');</script>";
				echo "<script>history.go(-1);</script>";
				exit;
			}*/
			
			//產生國際條碼
			//$this->isGetBarcode($this->input->post('barcode'));
			
			$category_multi_id = implode($this->input->post('category_multi_id'), ',');

			//----------------------------------------------------------------------
			// 更新商品主/副表
			//----------------------------------------------------------------------
			$data_master = array(
		    'category_multi_id' => $category_multi_id,
			  'fee_category_id' => $this->input->post('fee_category_id'),	
					  //'barcode' => $this->input->post('barcode'),
             //'category_func_id' => $this->input->post('category_func_id'),
		          //'category_id' => $this->input->post('category_id'),
					 //'group_id' => $this->input->post('group_id'),
					    //'ad_id' => $this->input->post('ad_id'),
				   	 'brand_id' => $this->input->post('brand_id'),
			             'name' => $this->input->post('name'),
			   'price_original' => $this->input->post('price_original'),					
			            'price' => $this->input->post('price'),			        
	            		'image' => $this->input->post('image'),	
					    //'stock' => $this->input->post('stock'),
			            'brief' => $this->input->post('brief'),
				       'branch' => $this->input->post('branch'),
					    'attr1' => $this->input->post('attr1'),
					    'attr2' => $this->input->post('attr2'),
				 	 'delivery' => $this->input->post('delivery'),
				 'delivery_msg' => $this->input->post('delivery_msg'),
					     'unit' => $this->input->post('unit'),
                    'promo_msg' => $this->input->post('promo_msg'),                               
                 'bonus_redeem' => $this->input->post('bonus_redeem'),
                    'order_max' => $this->input->post('order_max'),                         
                     'returned' => $this->input->post('returned'),
				   'sort_order' => $this->input->post('sort_order'),
                    'stock_min' => $this->input->post('stock_min'),
				      'is_comb' => $this->input->post('is_comb'),
				 'is_show_home' => $this->input->post('is_show_home'),
					  'keyword' => $this->input->post('keyword'),
					'is_no_fee' => $this->input->post('is_no_fee'),
			 'is_no_fee_entity' => $this->input->post('is_no_fee_entity'),
			  'is_no_fee_price' => $this->input->post('is_no_fee_price'),
			  'is_no_fee_sdate' => $this->input->post('is_no_fee_sdate'),
		      'is_no_fee_edate' => $this->input->post('is_no_fee_edate'),
                       'status' => $this->input->post('status'),
                        'udate' => date('Y-m-d H:i:s'),
					'meta_title'         => $this->input->post('meta_title'),
					'meta_description'   => $this->input->post('meta_description'),
					'meta_keyword'       => $this->input->post('meta_keyword'),
			);

	 	//更新商品主表
	 	$this->db->where('product_id', $this->input->post('product_id'));
	 	$this->db->update('product',$data_master);
	 	
	 	$data_detail = array(
	                  'description' => $this->input->post('description'),
                    'specification' => $this->input->post('specification'),	
	 		   	       'techenique' => $this->input->post('techenique'),
	 			           'notice' => $this->input->post('notice'),
	 	);	 	
	 	
	 	//更新商品副表
	 	$this->db->where('product_id', $this->input->post('product_id'));
	 	$this->db->update('product_memo',$data_detail);      

	 	//更新商品子表/屬性
	 	$this->db->where('product_id', $this->input->post('product_id'));
	 	$this->db->update('product_speci',array('stock' => $this->input->post('ps_stock')));
	 	
	 	$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);
	 	
	 	redirect('backend/product/edit?'.getQueryStringParam());

	 } else { //轉向預設頁面
	 	 
	 	$data['query'] = $this->model->shr_product_memo_view($this->input->get('product_id'));
	 	
	 	//商品子項
	 	/*
	 	$this->db->where('status',1);
	 	$data['query_property'] = $this->db->get('property')->result_array();	 
	 	$data['query_specification'] = $this->model->shr_product_speci_list($this->input->get('product_id'));
	 	*/
	 	
	 	//$data['query_attr']          = $this->model->shr_product_attr_list($this->input->get('product_id'));		
	 	
	 	$this->db->where('product_id' ,$this->input->get('product_id'));
	 	$this->db->order_by('sort_order' ,'DESC');
	 	$data['query_attr'] = $this->db->get('product_attr')->result_array();
	 	
	 	//檢視view
	 	$this->load->view('backend/common/header.tpl' ,$data);
	 	$this->load->view('backend/product/edit.tpl');
	 }
	  
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del () {

		$this->db->delete('product' ,array('product_id' => $this->input->get('product_id')));
		
		//此專案`product_speci`未做外鍵關聯，所以需獨立刪除
		$this->db->delete('product_speci' ,array('product_id' => $this->input->get('product_id')));

		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
		
		redirect('backend/product/lists');
	}
	

	// --------------------------------------------------------------------

	/**
	 * 方法 : 商品 / 編輯 /圖片
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function product_edit_image(){
	
		//商品屬性
		$data['query'] = $this->model->shr_product_speci_info($this->input->get('product_speci_id'));

		//商品屬性 /圖片
		$this->db->where('product_speci_id' ,$this->input->get('product_speci_id'));
		$data['query_image'] = $this->db->get('product_speci_image')->result_array();

		//檢視view
		$this->load->view('backend/common/header.tpl' ,$data);
		$this->load->view('backend/product/product_edit_image.tpl');
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 商品 / 編輯 /圖片 /新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function product_edit_image_add(){
	
		$err_msg = "";
			
		parse_str($this->input->post('data') ,$post_data);
			
		//欄位驗證&規則
		if(empty($post_data['image'])){
			$err_msg = "*圖片 不可空白!";
			echo $err_msg;
			return false;
		}

		$data = array(
		     'product_speci_id' => $post_data['product_speci_id'],
                        'image' => $post_data['image'],
                    'image_alt' => $post_data['image_alt'],
                        'cdate' => date('Y-m-d H:i:s')
		);
	 	
		//新增商品
		$this->db->insert('product_speci_image',$data);
	}


	// --------------------------------------------------------------------

	/**
	 * 方法 : 商品 / 編輯 /圖片 /刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function product_edit_image_del(){
	
        	$this->db->where('product_speci_image_id' ,$this->input->get('product_speci_image_id'));
			$this->db->delete('product_speci_image');
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 新增 step1
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function product_add_step1(){

		$this->form_validation->set_rules('category_multi_id[]','*商品類別','trim|required');
		//$this->form_validation->set_rules('group_id','* 群組分類','trim|required');
		//$this->form_validation->set_rules('category_id','*商品分類','trim|required');
		//$this->form_validation->set_rules('ad_id','*廣告類別','trim');
		//$this->form_validation->set_rules('brand_id','*品牌分類','trim|required');
		$this->form_validation->set_rules('sku','*編號(sku)','trim|required');
		//$this->form_validation->set_rules('barcode','*國際條碼','callback_check_is_barcode');
		$this->form_validation->set_rules('name','*商品名稱','trim|required');
		$this->form_validation->set_rules('price_original','*建議售價','trim');
		$this->form_validation->set_rules('price','*活動售價','trim|required');
		$this->form_validation->set_rules('image','*主圖','trim');
		$this->form_validation->set_rules('stock','*庫存量','trim|required');
		$this->form_validation->set_rules('keyword','*關鍵字','trim');
		$this->form_validation->set_rules('brief','*特色簡述','trim');
		$this->form_validation->set_rules('promo_msg','*促銷訊息','trim');
	    $this->form_validation->set_rules('description','*描述','trim');
		$this->form_validation->set_rules('specification','*規格','trim');
		$this->form_validation->set_rules('techenique','*技術','trim');
		$this->form_validation->set_rules('notice','*注意事項','trim');
		
		$this->form_validation->set_rules('meta_title','*Meta標籤標題','trim');
		$this->form_validation->set_rules('meta_description','*Meta標籤描述','trim');
		$this->form_validation->set_rules('meta_keyword','*Meta標籤關鍵字','trim');
	    
		if ($this->form_validation->run() == TRUE){
			
			//產生國際條碼
			//$this->isGetBarcode($this->input->post('barcode'));
			
			$category_multi_id = implode($this->input->post('category_multi_id'), ',');
			
			//----------------------------------------------------------------------
			// 寫入商品主表
			//----------------------------------------------------------------------
			$data_master = array(
					'category_multi_id' => $category_multi_id,
				      //'category_id' => $this->input->post('category_id'),
				     	   // 'ad_id' => $this->input->post('ad_id'),
				     	// 'group_id' => $this->input->post('group_id'),
					          'sku' => $this->input->post('sku'),
					      //'barcode' => $this->input->post('barcode'),
                         'brand_id' => $this->input->post('brand_id'),
                             'name' => $this->input->post('name'),	
			                'image' => $this->input->post('image'),	
					      //'stock' => $this->input->post('stock'),
				   'price_original' => $this->input->post('price_original'),					
			                'price' => $this->input->post('price'),				                            
                            'brief' => $this->input->post('brief'),
					      'keyword' => $this->input->post('keyword'),
                        'promo_msg' => $this->input->post('promo_msg'),                    
		             'bonus_redeem' => $this->input->post('bonus_redeem'),
                         'delivery' => $this->input->post('delivery'),
			            'order_max' => $this->input->post('order_max'),
                         'returned' => $this->input->post('returned'),
                        'stock_min' => $this->input->post('stock_min'),  
				            'cdate' => date('Y-m-d H:i:s'),
					
					'meta_title'         => $this->input->post('meta_title'),
					'meta_description'   => $this->input->post('meta_description'),
					'meta_keyword'       => $this->input->post('meta_keyword'),
			);
			
			//1)寫入商品主表
			$this->db->insert('product',$data_master);
				
			$product_id = $this->db->insert_id();
				
			$data_detail = array(
			           'product_id' => $product_id,
                      'description' => $this->input->post('description'),
                    'specification' => $this->input->post('specification'),		                  
                       'techenique' => $this->input->post('techenique'),
				   	       'notice' => $this->input->post('notice'),
                            'cdate' => date('Y-m-d H:i:s')
			);
				
			//2)寫入商品主表/備註
			$this->db->insert('product_memo' , $data_detail);
				
			//3)寫入商品子表/屬性
		    $data_speci = array(
	  			'product_id'        => $product_id,
	  			'sku'               => $this->input->post('sku'),
	  			'sku2'              => $this->input->post('sku'),
	  			'property_id_size'  => '',
	  			'property_id_color' => '',
	  			'stock'             => $this->input->post('stock'),
	  			//'image'             => $post_data['image'],
	  			'cdate'             => date('Y-m-d H:i:s')
	  	    );
	  	 
	  	    $this->db->insert('product_speci' ,$data_speci);	  
					
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
			
			redirect('backend/product/lists?page=1');
			
			//redirect('backend/product/product_add_step2?product_id='.$product_id);
		}else{
			
			$data['query_category'] = $this->db->get('category')->result_array();

			$this->load->view('backend/common/header.tpl' ,$data);
			$this->load->view('backend/product/product_add_step1.tpl');
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 新增 step2 / 商品規格
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function product_add_step2(){

		$this->form_validation->set_rules('product_id','*商品編號','trim|required');
			
		if ($this->form_validation->run() == TRUE){

			//驗證商品規格
			$this->db->where('product_id' ,$this->input->post('product_id'));
			$this->db->limit(1);
			$query = $this->db->get('product_speci')->row_array();

			if(empty($query)){
				dioRedirect('backend/product/product_add_step2?product_id='.$this->input->post('product_id') ,'*請至少建立一筆商品規格和數量');
			}else{
				
				$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
					
				redirect('backend/product/lists');				
			}

		}else{
			
			$this->db->where('product_id' ,$this->input->get('product_id'));
			$data['query'] = $this->db->get('product')->row_array();
			$data['query_speci'] = $this->model->shr_product_speci_list( $this->input->get('product_id') );
			
			$this->load->view('backend/common/header.tpl' ,$data);
			$this->load->view('backend/product/product_add_step2.tpl');
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 商品新增 step2 / 檢查
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function product_add_step2_check(){

		$category_id = $this->input->post('category_id');
			
		if( empty($category_id) ){
			dioRedirect('backend/product/product_add_step1' ,'*抱歉，分類尚未選取完成');
			exit;
		}else
		redirect('backend/product/product_add_step2?category_id='.$category_id);
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 商品新增 step3
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function product_add_step3(){
	
		$this->load->view('backend/common/header.tpl');
		$this->load->view('backend/product/product_add_step3.tpl');
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 商品批次上傳 / 檢視
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function product_import_view(){

		$this->load->view("backend/common/header.tpl");
		$this->load->view("backend/product/product_import_view.tpl");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 商品批次上傳 / csv
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function product_import_csv(){

		if ($_FILES["csv"]["error"] > 0){
			
			$fail = '';

			switch ($_FILES[$field]["error"]) {
				case UPLOAD_ERR_INI_SIZE:
					$fail = "*抱歉!檔案大小超出了伺服器上傳限制!";
					break;
				case UPLOAD_ERR_FORM_SIZE:
					$fail = "*抱歉!要上傳的檔案大小超出瀏覽器限制";
					break;
				case UPLOAD_ERR_PARTIAL:
					$fail = "*抱歉!檔案僅部分被上傳";
					break;
				case UPLOAD_ERR_NO_FILE:
					$fail = "*抱歉!檔案未能順利上傳!";
					break;
				case UPLOAD_ERR_NO_TMP_DIR:
					$fail = "*抱歉!伺服器臨時資料夾遺失!";
					break;
				case UPLOAD_ERR_CANT_WRITE:
					$fail = "*抱歉!檔案無法寫入硬碟!";
					break;
				case UPLOAD_ERR_EXTENSION:
					$fail = "File upload stopped by extension";
					break;

				default:
					$fail = "*抱歉!發生未知的檔案寫入錯誤!";
					break;
			}

		 dioRedirect('backend/product/product_import_view' ,$fail);
		 exit;

		}else{

			if ($_FILES["csv"]["size"] > 0) {
				
				//引入商品批次上傳類別
				$this->load->library(array('Csv_to_array'));

				$pd_csv = new Csv_to_array();
				$pd_csv->_set($_FILES["csv"]["tmp_name"]);
				
				$product_id = 0;
                $base_sku_ary = delTwoDDupli($pd_csv->getArray() ,'base_sku');
                                
                //驗證商品資料是否重複
                /*
                $this->db->select('sku');
                $query_duplicate = $this->db->get('product')->result_array();
                
                foreach ($query_duplicate as $row){
                	if($row['sku'] == ){
                		
                	}
                }*/
                
                //寫入父表
			    foreach ($base_sku_ary as $row){

			    	$data_product['sku'] = $row['base_sku'];
			    	$data_product['category_id'] = $row['rakuten_product_category_id'];
			    	$data_product['image'] = $row['image_url_1'];
			    	$data_product['name'] = $row['name'];
			    	$data_product['price'] = $row['price'];
			    	$data_product['checkout'] = $row['payment_option_1'];
			    	$data_product['delivery'] = $row['shipping_option_1'];
			    	$data_product['cdate'] = date('Y-m-d H:i:s');
			    	
			    	$this->db->insert('product' ,$data_product);
			    		
			    	$product_id = $this->db->insert_id();
			    		
			    	$data_product_memo['product_id'] = $product_id;
			    	$data_product_memo['description'] = $row['description_1'];
			    	$data_product_memo['cdate'] = date('Y-m-d H:i:s');
			    	
			    	$this->db->insert('product_memo' ,$data_product_memo);
			    	
			      //寫入子表
				  foreach ($pd_csv->getArray() as $row2){
				  	 if( ($row['base_sku'] == $row2['base_sku']) && !empty($row2['sku'])) {
				  	 	
				  	 	$data_product_speci['product_id'] = $product_id;
				  	 	$data_product_speci['sku'] = $row['base_sku'];
				  	 	$data_product_speci['sku2'] = $row2['sku'];
				  	 	$data_product_speci['speci'] = $row2['attribute_1'];
				  	 	$data_product_speci['stock'] = 0;
				  	 	$data_product_speci['cdate'] = date('Y-m-d H:i:s');
				  	 
				  	 	$this->db->insert('product_speci' ,$data_product_speci);
				  	 }
				  }	
				  	
				}
              
				dioRedirect('backend/product/lists');
			}		
			
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : SKU群組編號 / 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function sku_group_list () {

		
		$data['query'] = $this->db->get('ct_sku_group')->result_array();

		//檢視view
		$this->load->view("backend/common/header.tpl" ,$data);
		$this->load->view("backend/product/sku_group_list.tpl");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : SKU群組編號 / 新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function sku_group_insert () {

		$this->form_validation->set_rules('category_id','*商品類別','trim|required');
		$this->form_validation->set_rules('sku','*sku群組代號','trim|required');

		if ($this->form_validation->run() == TRUE){
				
			//將POST資料 ,轉為陣列
			$data = array(
                         'category_id' => $this->input->post('category_id'),
                                 'sku' => $this->input->post('sku'),
                               'cdate' => date('Y-m-d H:i:s') 
			);

			$this->db->insert('ct_sku_group',$data);
			dioRedirect('backend/product/sku_group_list');

		} else { //轉向預設頁面

			
				
			//檢視view
			$this->load->view("backend/common/header.tpl" ,$data);
			$this->load->view("backend/product/sku_group_insert.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : SKU群組編號 / 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function sku_group_update () {

    	$this->form_validation->set_rules('category_id','*商品類別','trim|required');
		$this->form_validation->set_rules('sku','*sku群組代號','trim|required');

		if ($this->form_validation->run() == TRUE){
				
			//----------------------------------------------------------------------
			// 將POST資料 ,轉為陣列
			//----------------------------------------------------------------------
			$data = array(
                         'category_id' => $this->input->post('category_id'),
                        'sku' => $this->input->post('sku')
			);

			$this->db->where('sku_group_id', $this->input->post('sku_group_id'));
			$this->db->update('ct_sku_group',$data);
			dioRedirect('backend/product/sku_group_list');

		} else { //轉向預設頁面

			
			$data['query']  = $this->db->get_where('ct_sku_group', array('sku' => $this->input->get('sku')) )->row_array();

			//檢視view
			$this->load->view("backend/common/header.tpl" ,$data);
			$this->load->view("backend/product/sku_group_update.tpl");

		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : SKU群組編號 / 產品檢視
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function sku_group_view () {

		
		$data['query']  = $this->db->get_where('product', array('sku' => $this->input->get('sku')) )->result_array();

		//檢視view
		$this->load->view("backend/common/header.tpl" ,$data);
		$this->load->view("backend/product/sku_group_view.tpl");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : SKU群組編號 / 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function sku_group_delete () {

		/*
			if(!$this->cls_category->is_sub_category($this->input->get('category_id'))){

			$this->db->delete('category' , array('category_id' => $this->input->get('category_id')));
			dioRedirect('backend/product_category/lists');
			}else{

			dioRedirect('backend/product_category/lists' ,'*不可刪除! 此分類項已有關聯子分類');
			}*/
	}
	
	/*
	 * ---------------------------------------------------------------------------------------------
	 *
	 *  批次資料處理
	 *
	 * ---------------------------------------------------------------------------------------------
	 *
	 * 針對列表的快捷鍵，做批次資料處理
	 */
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 組圖 / 多筆
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function multi_comb(){
	
		$enabled  = $this->input->get('enabled');
		$sort_ary = explode(',', $this->input->get('aryList'));
			
		$data = array();
	
		foreach ($sort_ary as $key=>$val) {
	
			//更新資料
			$this->db->where('product_id' ,$val);
			$this->db->update('product' ,array('is_comb' => $enabled));
		}
		 
		$this->session->set_flashdata('msg','您已成功異動'.COUNT($sort_ary).'筆資料');
	
		redirect('backend/product/lists?page=1');
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢查 / 指定商品
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function check_is_no_fee( $is_no_fee ,$param )
	{
		$param = explode(',', $param);
		
		if($is_no_fee == 1)
		{
			if( $param[0] <= 0 && $param[1] <= 0 && ($param[2] == '0000-00-00' && $param[3] == '0000-00-00') ){ 
				$this->form_validation->set_message('check_is_no_fee', '*若為指定商品免運，請填寫相關條件');
				return false;
			}
			
		}else{
			
			if( $param[0] > 0 && $param[1] > 0 && ($param[2] != '0000-00-00' && $param[3] != '0000-00-00') ){
				$this->form_validation->set_message('check_is_no_fee', '*非指定商品免運，不可請填寫相關條件');
				return false;
			}
		}
		
		return true;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢查 / 國際條碼格式
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function check_is_barcode( $barcode )
	{
		$barcode = trim($barcode);
		
		if( empty($barcode) )
		{
			$this->form_validation->set_message('check_is_barcode', '*國際條碼不可空白');
			return false;
		}
		
		if( strlen($barcode) != 13)
		{
			$this->form_validation->set_message('check_is_barcode', '*國際條碼必須為13碼');
			return false;
		}
		
		if( !is_numeric($barcode) )
		{
			$this->form_validation->set_message('check_is_barcode', '*國際條碼必須數字');
			return false;
		}
		
		if( substr($barcode,0,3) != '471' ) //國碼(471)
		{
			$this->form_validation->set_message('check_is_barcode', '*國際條碼前3碼必須為 (471)');
			return false;
		}
		
		if( substr($barcode,3,6) != '297259' ) //公司前置碼需為(297259)
		{
			$this->form_validation->set_message('check_is_barcode', '*國際條碼4-9碼必須為(297259)');
			return false;
		}
		
		return true;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 是否取得條碼資料
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function isGetBarcode( $barcode ){

		//引入 barcode 掛件
		require_once(APPPATH.'libraries/barcode/Exceptions/BarcodeException.php');
		require_once(APPPATH.'libraries/barcode/Exceptions/InvalidCharacterException.php');
		require_once(APPPATH.'libraries/barcode/Exceptions/InvalidCheckDigitException.php');
		require_once(APPPATH.'libraries/barcode/Exceptions/InvalidFormatException.php');
		require_once(APPPATH.'libraries/barcode/Exceptions/InvalidLengthException.php');
		require_once(APPPATH.'libraries/barcode/Exceptions/UnknownTypeException.php');
		
		require_once(APPPATH.'libraries/barcode/BarcodeGenerator.php');
		require_once(APPPATH.'libraries/barcode/BarcodeGeneratorJPG.php');
		
		$generatorJPG = new BarcodeGeneratorJPG();
		
		$file = 'resources/barcode/'.$barcode.'.jpg';
		
		file_put_contents($file, $generatorJPG->getBarcode($barcode, $generatorJPG::TYPE_EAN_13));
			
        if( file_exists($file))
        	return true;
		
		return false;
	}

}


/* End of file product.php */
/* Location: ./application/controllers/backend/product.php */