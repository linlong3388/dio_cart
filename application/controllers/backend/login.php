<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理[登入]的控制器
 * @controllerName login
 * @author Dio
 *
 */
class login extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base' ,'is_valid','dio_captcha'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));

		//載入model
		$this->load->model("backend/dashboard_model","model");
		
		//登入驗證
		if(is_login_admin()){
			redirect('backend/dashboard/main');
		}
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 登入驗證
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function valid(){
		
		$this->form_validation->set_rules('username','帳號','trim|required|min_length[4]|max_length[15]');
		$this->form_validation->set_rules('password','密碼','trim|required|min_length[4]|max_length[15]');
		$this->form_validation->set_rules('secure_code','* 驗證碼', 'callback_check_captcha');
			
		if ($this->form_validation->run() == TRUE){

			$data = array(
			            'username' => $this->input->post('username'),
			            'password' => md5($this->input->post('password')),
			            'status'   => '1'
			            );
			             
			            $query_admin = $this->db->get_where('admin',$data)->row_array();
			             
			            //登入成功頁面
			            if($query_admin){
			            	$_SESSION['admin_info'] = $query_admin;
			            	$this->write_log($query_admin);
			            	redirect('backend/dashboard/main');
			            }else{
			            	dioRedirect('backend/login/valid','登入失敗! 帳號或密碼有誤，或是已被停用!');
			            }

		}else{ //轉向預設頁面
			$this->load->view('backend/common/header.tpl');
			$this->load->view('backend/login/valid.tpl');
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢查 / 驗證碼
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function check_captcha($secure_code)
	{
	
		if(!$secure_code)
		{
			$this->form_validation->set_message('check_captcha', '*驗證碼 不可空白');
			return FALSE;
		}
	
		if( $secure_code != $_SESSION['captcha'] )
		{
			$this->form_validation->set_message('check_captcha', '*驗證碼必須一致');
			return FALSE;
		}
	
		return TRUE;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 圖形驗證碼
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function captcha(){
			
		echo captcha();
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 寫入登錄紀錄
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function write_log($param){
		
		$this->db->where('admin_id' ,$param['admin_id']);
		$this->db->update('admin' , array('log_date' => date('Y-m-d H:i:s')) );
	}	
	
}


/* End of file ad_home.php */
/* Location: ./application/controllers/backend/login.php */