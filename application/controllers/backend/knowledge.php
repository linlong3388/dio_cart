<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理 [知識專欄]的控制器
 * @controllerName knowledge
 * @author Dio
 *
 */
class knowledge extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));
		
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 列表
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function lists () {

		$data['slt_status'] = $this->slt_status();
		$data['query'] = $this->db->get('knowledge')->result_array();

		//檢視view
		$this->load->view("backend/common/header.tpl",$data);
		$this->load->view("backend/knowledge/lists.tpl");
	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 新增
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function add () {

		//$this->form_validation->set_rules('knowledge_category_id','* 分類ID','trim|required');
		$this->form_validation->set_rules('title','*標題','trim|required');
		$this->form_validation->set_rules('image','*圖片','trim');
		$this->form_validation->set_rules('content','*內容','trim|required');
			
		if ($this->form_validation->run() == TRUE){

			$data = array(
					//'knowledge_category_id' => $this->input->post('knowledge_category_id'),
					    'title' => $this->input->post('title'),
					    'image' => $this->input->post('image'),
					  'content' => $this->input->post('content'),
                        'cdate' => date('Y-m-d H:i:s') 
			        );

			$this->db->insert('knowledge',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_INSERT);
			
			redirect('backend/knowledge/lists');
			
		} else { 

			$this->load->view("backend/common/header.tpl");
			$this->load->view("backend/knowledge/add.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit () {
		
		//$this->form_validation->set_rules('knowledge_category_id','* 分類ID','trim|required');
		$this->form_validation->set_rules('knowledge_id','*編號','trim|required');
		$this->form_validation->set_rules('title','*標題','trim|required');	
		$this->form_validation->set_rules('image','*圖片','trim');
		$this->form_validation->set_rules('content','*內容','trim|required');
		$this->form_validation->set_rules('sort_order','*排序','trim');
		$this->form_validation->set_rules('status','*狀態','trim|required');
		$this->form_validation->set_rules('cdate','*建檔日期','trim|required');
		$this->form_validation->set_rules('meta_title','*Meta標籤標題','trim');
		$this->form_validation->set_rules('meta_description','*Meta標籤描述','trim');
		$this->form_validation->set_rules('meta_keyword','*Meta標籤關鍵字','trim');

		if ($this->form_validation->run() == TRUE){

			$data = array(
					//'knowledge_category_id' => $this->input->post('knowledge_category_id'),
					     'title' => $this->input->post('title'),	
				     	 'image' => $this->input->post('image'),
					   'content' => $this->input->post('content'),
					'sort_order' => $this->input->post('sort_order'),
			            'status' => $this->input->post('status'),
					'meta_title'         => $this->input->post('meta_title'),
					'meta_description'   => $this->input->post('meta_description'),
					'meta_keyword'       => $this->input->post('meta_keyword'),
			        );

			$this->db->where('knowledge_id', $this->input->post('knowledge_id'));
			$this->db->update('knowledge',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);

			Redirect('backend/knowledge/edit?knowledge_id='.$this->input->post('knowledge_id'));

		} else { //轉向預設頁面

			$data['query']  = $this->db->get_where('knowledge', array('knowledge_id' => $this->input->get('knowledge_id')) )->row_array();

			//檢視view
			$this->load->view("backend/common/header.tpl",$data);
			$this->load->view("backend//knowledge/edit.tpl");
		}

	}

	// --------------------------------------------------------------------

	/**
	 * 方法 : 刪除
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function del () {
			
		$this->db->delete('knowledge' , array('knowledge_id' => $this->input->get('knowledge_id')));
		
		$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_DELETE);
		
		Redirect('backend/knowledge/lists');
	}
	
	// --------------------------------------------------------------------

	/**
	 * 方法 : 下拉式 / 狀態
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function slt_status () {
		
		return array(
		               '0' => '停用' ,
                       '1' => '啟用'
	            );
	}
	
}


/* End of file knowledge.php */
/* Location: ./application/controllers/backend/knowledge.php */