<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理[barcode文件管理器]的控制器
 * @controllerName barcode
 * @author Dio
 *
 */
class barcode extends BackEnd_Controller {
	
	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();
	
		$this->load->database();
		$this->load->helper(array('form','cookie','path','file','base','url','elfinder'));
		$this->load->library(array('form_validation','session'));
	}

   // --------------------------------------------------------------------
	
   /**
	* 方法 : 主頁
    *
	* @access	public
	* @param
	* @return
	*/
	public function index () {
		
		$this->load->view("backend/common/header.tpl");
		$this->load->view("backend/barcode/index.tpl");
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 啟動
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function init() {
	
		$opts = array(
				'bind' => array(
						'duplicate upload rm paste' => 'elfinder_upload_rename'
				),
		
				'roots' => array(
						array(
								'driver'  => 'LocalFileSystem',
								'path'    => set_realpath('./resources/barcode/'),
								'URL'     => site_url('resources/barcode/'),
								'uploadMaxSize' => '5M' ,
								'accessControl' => 'elfinder_access',
								'uploadAllow' => array('image'),
								'attributes'  => array(
										array(
												'pattern' => '/.*/',
												'read'  => true,
												'write' => true
										)
								),
						)
				)
		);
		
		$this->load->library('elfinder_cls', $opts);
				
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 方法 : 檢視 /iframe
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function iframe () {
	
		$this->load->view("backend/barcode/iframe.tpl");
	}
	
	// --------------------------------------------------------------------
	
	/**
 	 * 方法 : 檢視 /dialog
 	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function dialog () {
		
		$this->load->view("backend/barcode/dialog.tpl");
	}
	
}


/* End of file barcode.php */
/* Location: ./application/controllers/barcode.php */