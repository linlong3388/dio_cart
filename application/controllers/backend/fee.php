<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 控制器 - 處理 [宅配運費設定]的控制器
 * @controllerName fee
 * @author Dio
 *
 */
class fee extends BackEnd_Controller {

	/**
	 * 建構方法 : 成員和物件初始化
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function __construct(){
			
		parent::__construct();

		$this->load->database();
		$this->load->helper(array('form','url','cookie','path','database','ctmall','base','is_valid'));
		$this->load->helper(array('dio_string','dio_message'));
		$this->load->library(array('form_validation','session'));
		
		//登入驗證
		if(!is_login_admin()){
			redirect('backend/login/valid');
		}
	}


	// --------------------------------------------------------------------

	/**
	 * 方法 : 編輯
	 *
	 * @access	public
	 * @param
	 * @return
	 */
	public function edit () {
		
		$this->form_validation->set_rules('shipping_fee','*常溫運費','trim|required|numeric');
		/*
		$this->form_validation->set_rules('shipping_fee_refri','*冷藏運費','trim|required|numeric');
		$this->form_validation->set_rules('shipping_fee_cool','*冷凍運費','trim|required|numeric');
		$this->form_validation->set_rules('shipping_fee_island','*離島運費','trim|required|numeric');
		$this->form_validation->set_rules('shipping_fee_full','*免運額','trim|required|numeric');
		*/
		
		if ($this->form_validation->run() == TRUE){

			$data = array(
			               'shipping_fee' => $this->input->post('shipping_fee'),
					/*
				     'shipping_fee_refri' => $this->input->post('shipping_fee_refri'),
					  'shipping_fee_cool' => $this->input->post('shipping_fee_cool'),
					'shipping_fee_island' => $this->input->post('shipping_fee_island'),
				      'shipping_fee_full' => $this->input->post('shipping_fee_full')
				      */
			);

			$this->db->where('system_id', 1);
			$this->db->update('system',$data);
			
			$this->session->set_flashdata('msg',DIO_MSG_SUCCESS_UPDATE);

			Redirect('backend/fee/edit?fee_id=1');

		} else { //轉向預設頁面

			$data['query']  = $this->db->get_where('system', array('system_id' => 1) )->row_array();

			//檢視view
			$this->load->view("backend/common/header.tpl",$data);
			$this->load->view("backend/fee/edit.tpl");
		}

	}
	
}


/* End of file fee.php */
/* Location: ./application/controllers/backend/fee.php */