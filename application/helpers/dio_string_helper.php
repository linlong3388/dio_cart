<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 方法 : 字串/格式函數
 * 
 * @helpers dio_string
 * @author Dio
 *
 */

// ------------------------------------------------------------------------

/**
 * addCommas
 *
 * 數字格式轉成千分位 (格式如: 125,000)
 *
 * @access	public
 * @param	string
 * @return	string
 */
if ( ! function_exists('addCommas'))
{
	function addCommas($money)
	{
	  $format = number_format($money, 3, '', ',');

	  return substr($format, 0, (strlen($format)-3));;
	}
}

// ------------------------------------------------------------------------

/**
 * delCommas
 *
 * 取消數字千分位格式
 *
 * @access	public
 * @param	string
 * @return	string
 */
if ( ! function_exists('delCommas'))
{
	function delCommas($money)
	{
		return str_replace(',','',$money);
	}
}

// ------------------------------------------------------------------------

/**
 * addZero
 *
 * 在個位數前追加0
 *
 * @access	public
 * @param	string
 * @return	string
 */
if ( ! function_exists('addZero'))
{
	function addZero($var)
	{
		$str = sprintf("%d" ,$var);
		
		if(strlen($str) < 2 )
			$str = '0'.$str;
		else
			$str;
		
		return $str;
	}
}

// ------------------------------------------------------------------------

/**
 * delBlank
 *
 * 刪除字串裡的空白字元
 *
 * @access	public
 * @param	string
 * @return	string
 */
if ( ! function_exists('delBlank'))
{
	function delBlank($str)
	{
		return str_replace(' ', '', $str);
	}
}

// ------------------------------------------------------------------------

/**
 * insDupliChar
 *
 * 在字串的指定位址 ,重複加入特定字元
 * 如 :
 *     傳入參數("0205092122253034414345474954565859606566" ,'@',2)
 *     回傳  "02@05@09@21@22@25@30@34@41@43@45@47@49@54@56@58@59@60@65@66"
 *
 * @access	public
 * @param	string 
 * @param	char
 * @param	int
 * @return	string
 */
if ( ! function_exists('insDupliChar'))
{
	function insDupliChar($str,$wha,$cnt)
	{
		$strip = false;
		if (strlen($str) % $cnt == 0) {
			$strip = true;
		}
		$tmp = preg_replace('/(.{'.$cnt.'})/',"$1$wha", $str);
		if ($strip) {
			$tmp = substr($tmp,0,-1);
		}
		return $tmp;
	}
}

// ------------------------------------------------------------------------

/**
 * getContentFirstImage
 *
 * 取得本文欄位的第一個圖片連結
 *
 * @access	public
 * @param	string
 * @return	string
 */
if ( ! function_exists('getContentFirstImage'))
{
	function getContentFirstImage($content)
	{
	  preg_match( '|img src="([^"]+)"|', $content, $matches );

	  $str = '#';

	  if( !empty($matches[1]) ){
	 	 $str = substr_replace($matches[1], '', 'http:' ,stripos($matches[1], 'application'));
	  }

	  return $str;
	}
}

// ------------------------------------------------------------------------

/**
 * changeContentImagePath
 *
 * 改變本文內容的圖片路徑
 *   由於後台的url 和前台的 url 圖片路徑格式不符，所以需做調整
 *
 * @access	public
 * @param	string
 * @return	string
 */
if ( ! function_exists('changeContentImagePath'))
{
	function changeContentImagePath($content)
	{
		return str_replace("../../resources" ,"../resources" ,$content);
	}
}

// ------------------------------------------------------------------------

/**
 * changeContentImageURL
 *
 * 改變內文的圖片路徑
 *   由於前台做短網址的SEO，導致內文的圖片路徑不一致，所以前台輸出後，需進行調教
 *
 * @access	public
 * @param	string
 * @return	string
 */
if ( ! function_exists('changeContentImageURL'))
{
	function changeContentImageURL($content)
	{
		return str_replace('img src="../../' ,'img src="../' ,$content);
	}
}

// ------------------------------------------------------------------------

/**
 * getCheckOutInputInfo
 *
 * 取得購物車輸入資料
 *
 * @access	public
 * @param	string
 * @return	string
 */
if ( ! function_exists('getCheckOutInputInfo'))
{
	function getCheckOutInputInfo($filed)
	{
	   $set_value =	set_value($filed);
		
       if( !empty($_SESSION['my_order']['info'][$filed]) ){
       	  return $_SESSION['my_order']['info'][$filed];
       
       }elseif( !empty($set_value) ){
          return $set_value;
       
       }elseif( $_SESSION['customer_info'][$filed]){
       	  return $_SESSION['customer_info'][$filed];
       }		
		
	   return '';
	}
}

// --------------------------------------------------------------------

/**
 * 方法 : 文字欄位預設值/編輯
 *
 *       舊的格式寫法，需以快取值為優先 :
 *       isset($query['description']) ? $query['description'] : set_value('description')
 *
 * @access	public
 * @param
 * @return
 */
function fieldTextDefaultEdit($fname ,$fval){

	//快取值優先
	if( set_value($fname) )
		return set_value($fname);

	//資料庫值
	return $fval;
}

/* End of file dio_string */