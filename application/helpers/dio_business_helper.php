<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 方法 : 商務專用函數
 * 
 * @helpers dio_business
 * @author Dio
 *
 */

// ------------------------------------------------------------------------

/**
 * getIsEdateValid
 *   
 * 檢查建立日期是否到期
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getIsEdateValid'))
{
	function getIsEdateValid($edate)
	{
	   $dt1 = date('Y-m-d');
	   $dt2 = substr( $edate ,0 ,10 );

	   if( strtotime($dt1) > strtotime($dt2)){
		 return false;
	   }else{
		 return true;
	   }
	} 
}

// ------------------------------------------------------------------------

/**
 * getRandId
 *
 * 產生各種隨機ID
 *   
 *   格式:  $sign + time(10) + 隨機碼(6)
 *           ?     1469866221 989205
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getRandId'))
{
	function getRandId($sign="")
	{
	  return $sign . time() . rand(100000, 999999);
	}  
}

// --------------------------------------------------------------------

/**
 * 方法 : 登入後 / 判斷管理員，可使用的功能
 *
 * @access	public
 * @param
 * @return
 */
if ( ! function_exists('isAdminRoleTbName'))
{
	function isAdminRoleTbName($tb_name){
		
		if( $_SESSION['admin_info']['role'] == 66) return true;

		$CI =& get_instance();
		$CI->load->model("backend/business_model" ,"business");
		
		$query = $CI->business->getAdminGroupRoleLists($_SESSION['admin_info']['admin_id']);
		
		foreach ($query as $row) {
			if($row['tb_name'] == $tb_name){
				return true;
			}
		}

		return false;
	}
}

// ------------------------------------------------------------------------

/**
 * getProductPromoPrice
 *
 * 取得商品促銷價格，由於有多種促銷活動，所以逐一判斷各活動，並取出最低價格
 *
 * @access	public
 * @param   $product_id ,$price
 * @return	array
 */
if ( ! function_exists('getProductPromoPrice'))
{
	function getProductPromoPrice($product_id ,$price)
	{
		
		//定義預設值
		$data['is_category_promo'] = 0;
		$data['name']              = "";
		$data['price']             = $price;
		$data['edate']             = "0000-00-00";
		
		$CI =& get_instance();
		
		$sql = "SELECT cp.*
		          FROM `category_promo_product` cpp
		         LEFT JOIN `category_promo` cp 
		           ON cpp.category_promo_id = cp.category_promo_id
		         WHERE cpp.product_id = '".$product_id."'
		         AND cp.status = 1
		         AND cp.edate >= '".date('Y-m-d')."'
		        ORDER BY CONVERT( cp.rate_price,UNSIGNED INTEGER) DESC 
		         LIMIT 1";
		
		$query = $CI->db->query($sql)->row_array();
		
		if( !empty($query) ){
			$data['is_category_promo'] = 1;
			$data['name']              = $query['name'];
			$data['price']             = (1 - $query['rate_price']/100) * $price;
			$data['edate']             = $query['edate'];
		}
			
		return $data;
		
	}
}

// ------------------------------------------------------------------------

/**
 * getBarcodeFileName
 *
 * 取得Barcode圖檔名稱
 *
 *   格式: 店別 + 年月日時分秒 + 隨機碼(6)
 *          3_20180111091012_920512
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getBarcodeFileName'))
{
	function getBarcodeFileName( $branch_id = 0 )
	{
		return $branch_id . '_' . date('YmdHis') . '_' . rand(100000, 999999);
	}
}

// ------------------------------------------------------------------------

/**
 * isCategory_3ForCart
 * 
 * 判斷購物車的商品是否為助糧商品
 *
 * @access	public
 * @return	boolean
 */
if ( ! function_exists('isCategory_3ForCart'))
{
	function isCategory_3ForCart()
	{
		$CI =& get_instance();
		$item_cart = $CI->cls_cart->select_all();

		foreach ($item_cart as $row){
			if( isCategory_3($row['category_id']) ){
				return true;
			}
	    }
	   
	    return false;
	}
}


/* End of file dio_business */