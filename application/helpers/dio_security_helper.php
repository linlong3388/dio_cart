<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 方法 : 安全性函數
 * 
 * @helpers dio_security
 * @author Dio
 *
 */

// ------------------------------------------------------------------------

/**
 * encrypt1/decrypt1
 *
 * 加密函數1與解密函數1 搭配使用
 *
 * @access	public
 * @param	string
 * @param	string
 * @return	string
 */
if ( ! function_exists('encrypt1'))
{
	function encrypt1($data, $key)
	{
	  $key	=	md5($key);
	  $x		=	0;
	  $len	=	strlen($data);
	  $l		=	strlen($key);
	  $char   =   '';
	  $str    =   '';
	
	  for ($i = 0; $i < $len; $i++)
	  {
		if ($x == $l)
		{
			$x = 0;
		}
		
		$char .= $key{$x};
		$x++;
	  }
	
	  for ($i = 0; $i < $len; $i++)
	  {
		$str .= chr(ord($data{$i}) + (ord($char{$i})) % 256);
	  }
	
	  return base64_encode($str);
	}
}

// ------------------------------------------------------------------------

/**
 * encrypt1/decrypt1
 *
 * 加密函數1與解密函數1 搭配使用
 *
 * @access	public
 * @param	string
 * @param	string
 * @return	string
 */
if ( ! function_exists('decrypt1'))
{
	function decrypt1($data, $key)
	{
	  $key = md5($key);
	  $x = 0;
	  $data = base64_decode($data);
	  $len = strlen($data);
	  $l = strlen($key);
	  $char   =   '';
	  $str    =   '';
	
	  for ($i = 0; $i < $len; $i++)
	  {
		 if ($x == $l)
		 {
			$x = 0;
	 	 }
		 
	 	 $char .= substr($key, $x, 1);
		 $x++;
	  }
	
	  for ($i = 0; $i < $len; $i++)
	  {
		if (ord(substr($data, $i, 1)) < ord(substr($char, $i, 1)))
		{
			$str .= chr((ord(substr($data, $i, 1)) + 256) - ord(substr($char, $i, 1)));
		}
		else
		{
			$str .= chr(ord(substr($data, $i, 1)) - ord(substr($char, $i, 1)));
		}
	  }
	  
	  return $str;
	}
}

// ------------------------------------------------------------------------

/**
 * isSqlInject
 *
 * 以正則表達式，過濾危險字串，防SQL資料隱碼攻擊
 *
 * @access	public
 * @param	string
 * @return	t/f
 */
if ( ! function_exists('isSqlInject'))
{
	function isSqlInject($str)
	{
	  $check = preg_match("/select|insert|update|delete|\'|\/\*|\*|\.\.\/|\.\/|union|into|load_file|outfile/i", $str);
	
	  if($check)
		return true;	  
	  else
		return false;
	  
	}
}

	
/* End of file dio_security */