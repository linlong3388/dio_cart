<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 方法 : 樣式函數
 * 
 * @helpers dio_template
 * @author Dio
 *
 */

// ------------------------------------------------------------------------

/**
 * srhResultHighLight
 *
 * 搜尋結果高亮點顯示
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('srhResultHighLight'))
{
	function srhResultHighLight($str ,$srh_str='')
	{
		if(!$srh_str) return $str;
		
		return str_ireplace($srh_str , "<b style='background:yellow'>".$srh_str."</b>" ,$str);
	}
}


/* End of file dio_template */