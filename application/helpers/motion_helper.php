<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 方法 : motion專用函數
 * @helpers motion
 * @author Dio
 *
 */

/*****************************************************
 /* 名稱           : add_viewed
 * 功能           : 加入瀏覽紀錄
 ****************************************************/
function add_viewed($data){

	$re_data = array();

	if(isset($_COOKIE["motion_cookie"])){
		//累加 cookie佇列
		$re_data = unserialize($_COOKIE["motion_cookie"]);
		$re_data[] = $data;
		$re_data = delTwoDDupli($re_data ,'product_id');

		//限制6筆
		if( COUNT($re_data) >= 7){
			array_shift($re_data);
		}

	}else{
		$re_data[] = $data;
	}

	setcookie("motion_cookie", serialize($re_data), time()+3600, '/');
}


/*****************************************************
 /* 名稱           : get_viewed
 * 功能           : 取得瀏覽紀錄
 ****************************************************/
function get_viewed(){

	if(isset($_COOKIE["motion_cookie"])) {
		return unserialize($_COOKIE["motion_cookie"]);
	}
}


/*****************************************************
 /* 名稱           : del_viewed
 * 功能           : 刪除瀏覽紀錄
 ****************************************************/
function del_viewed($ck_id=""){
	 
	if(isset($_COOKIE["motion_cookie"])) {

		if(isset($ck_id) && $ck_id != ""){
			 
			$temp_ary = unserialize($_COOKIE["motion_cookie"]);
			 
			for($i=0 ; $i<COUNT($temp_ary) ;$i++){
				if($ck_id == $i){
					unset($temp_ary[$i]);
					break;
				}
			}

			setcookie("motion_cookie", serialize(array_values($temp_ary)), time()+3600, '/');
			 
		}else{
			setcookie("motion_cookie", '', time()-3600, '/'); // empty value and old timestamp
			unset($_COOKIE["motion_cookie"]);
		}
	}
}


/********************************************************************
 /* 名稱           : get_input_category_id
 * 功能           : category_id 的傳入格式為 '7_0' ,'1_3'，第一個元素為大分類，
 *           第二個元素為次分類。
 * 參數           :
 *******************************************************************/
function get_input_category_id($category_id){

	$reVal = explode('_',$category_id);

	if(isset($reVal)){
		//依次分類是否為0，判斷搜尋的category_id
		if($reVal[1] == 0){
			return $reVal[0];
		}else{
			return $reVal[1];
		}
	}

	return 0;
}


/*****************************************************
 /* 名稱           : get_currency_format
 * 功能           : 取得售價的幣別格式。
 * 參數           :
 ****************************************************/
function get_currency_format($price){

	return '$'.strtoupper($_SESSION['motion_currency']).' '.addCommas($price);
}


/* End of file motion */