<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 方法 : 日期時間函數
 * 
 * @helpers dio_date
 * @author Dio
 *
 */

// ------------------------------------------------------------------------

/**
 * getThisWeek
 *
 * 本週區間
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('getThisWeek'))
{
	function getThisWeek()
	{
	  $day = date('w');
	  $week_start = date('Y-m-d', strtotime('-'.$day.' days'));
	  $week_end = date('Y-m-d', strtotime('+'.(6-$day).' days'));

	  return array( 'start' => $week_start ,
			          'end' => $week_end );
	}
}

// ------------------------------------------------------------------------

/**
 * getDateToAd
 *
 * 民國年轉西元年
 *
 * @access	public
 * @param	string (格式'1050320')
 * @return	string
 */
if ( ! function_exists('getDateToAd'))
{
	function getDateToAd($in_date)
	{
	  $cyear = substr($in_date, 0, -4);
	  $year = ((int) $cyear )+1911;
	  $mon = substr($in_date, -4, 2);
	  $day = substr($in_date, -2);

	  return date($year).'/'. $mon .'/'.$day ;
	}
}

// ------------------------------------------------------------------------

/**
 * getStringToDate
 *
 * 將字串日期傳為日期格式
 *
 * @access	public
 * @param	string (格式'2017121900000005')
 * @return	string
 */
if ( ! function_exists('getStringToDate'))
{
	function getStringToDate($date_str)
	{
		return substr($date_str, 0,4).'/'.substr($date_str, 4,2).'/'.substr($date_str, 6,2).' '.substr($date_str, 8,2).':'.substr($date_str, 10,2);
	}
}


/* End of file dio_date */