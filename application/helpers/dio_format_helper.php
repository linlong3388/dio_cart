<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 方法 : 格式函數
 * 
 * @helpers dio_format
 * @author Dio
 *
 */

// ------------------------------------------------------------------------

/**
 * getAminMenuFormat
 *
 * 取得後台管理的清單格式
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('getAminMenuFormat'))
{
	function getAminMenuFormat($segment)
	{
	  $seg = explode('/', $segment);

	  return "<a href=".getAdminURL($segment).">".reSortArrayMutil(2,$seg[0] ,getAdminMenu())."</a>";
	}
}

// ------------------------------------------------------------------------

/**
 * setDataToStar
 *
 * 設定隱碼資料格式
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('setDataToStar'))
{
	function setDataToStar($original, $replacement, $position, $length)
	{
		$startString = mb_substr($original, 0, $position, "UTF-8");
		$endString = mb_substr($original, $position + $length, mb_strlen($original), "UTF-8");

		$out = $startString . $replacement . $endString;

		return $out;
	}
}

// --------------------------------------------------------------------

/**
 * 方法 : 購物車專屬欄位預設值
 *         優先順序 : 1)快取值
 *                   2)SESSION值
 *                   3)預設值 
 *
 * @access	public
 * @param [欄位名稱],[預設值]
 * @return
 */      
function fieldCartDefault($fname ,$fval){
	
	if( set_value($fname) )
		return set_value($fname);

	if( !empty($_SESSION['my_order']['info'][$fname]) )
		return $_SESSION['my_order']['info'][$fname];
		
	//預設值
	return $fval;
}


/* End of file dio_format */