<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 方法 : elfinder函數
 * 
 * @helpers elfinder
 * @author Dio
 *
 */

// ------------------------------------------------------------------------

/**
 * elfinder_access
 *
 * 移除 elfinder(文件管裡掛件)的 .附檔名檔案
 *     if file/folder begins with '.' (dot)
 *     set read+write to false, other (locked+hidden) set to true
 *     else elFinder decide it itself
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('elfinder_access'))
{
	function elfinder_access($attr, $path, $data, $volume)
	{
	  return strpos(basename($path), '.') === 0 ? !($attr == 'read' || $attr == 'write') : null;
	}
}

// ------------------------------------------------------------------------

/**
 * elfinder_upload_rename
 *
 * 檔案上傳重新定義檔名
 * 格式 : 時間戳記+隨機數+單次流水編
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('elfinder_upload_rename'))
{
	function elfinder_upload_rename($cmd, $result, $args, $elfinder)
	{
		$files = $result['added'];
		
		foreach ($files as $i => $file) {
		
			$filename_temp = explode('.', $file['name']);
			$filename = uniqid().rand(1,9999).'_'.$i.'.'.$filename_temp[1];
				
			$arg = array('target' => $file['hash'], 'name' => $filename);
			$elfinder->exec('rename', $arg);
		}
		
		return true;
	}
}

// ------------------------------------------------------------------------

/**
 * validName
 *
 * 定義檔案管理掛件elFinder檔名格式
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('validName'))
{
	function validName($name)
	{
	  $name_ary = explode('.', $name);

	  if( ctype_alnum(str_replace('_', '', $name_ary[0])))
		return true;
  	  else
		return false;
	}
}



/* End of file elfinder */