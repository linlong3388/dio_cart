<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 方法 : 中國信託(紅利商城)專用函數
 * @helpers ctmall
 * @author Dio
 *
 */

/*****************************************************
 /* 名稱           : get_backend_funcs
 * 功能           : 取得後台所有功能表
 * 說明           :
 ****************************************************/
function get_backend_funcs($key_id=''){

	return array(
	            'customer_list' => "<b>會員列表</b>" ,
	// 'customer_update' => "會員列表 / 編輯" ,
	//  'customer_pass' => "會員列表 / 密碼修改" ,

	            'category_list' => "<b>商品分類</b>" ,
	// 'category_update' => "商品分類 / 編輯" ,
	//  'category_2list' => "商品分類 / (次級)分類列表" ,
	// 'category_2insert' => "商品分類 / (次級)分類新增" ,
	// 'category_2update' => "商品分類 / (次級)分類編輯" ,

	             'product_list' => "<b>商品列表</b>" ,
	//      'product_edit' => "商品列表 / 編輯" ,
	// 'product_add_step1' => "商品列表 / 新增 Step1" ,
	// 'product_add_step2' => "商品列表 / 新增 Step2" ,

	               'order_list' => "<b>待處理訂單</b>" ,
	// 'order_status_edit' => "待處理訂單 / 檢視" ,

	        'order_return_list' => "<b>退貨列表</b>" ,
	// 'order_return_view' => "退貨列表 / 檢視" ,

        	'order_export_list' => "<b>訂單下載</b>" ,

	        'order_import_view' => "<b>訂單匯入</b>" ,

	    'sales_statistics_list' => "<b>訂單統計</b>" ,

	      'buy_statistics_list' => "<b>商品統計</b>" ,

	             'ad_home_list' => "<b>首頁廣告</b>" ,

	                 'elfinder' => "<b>檔案管理</b>" ,

	                    'index' => "<b>管理中心(預設)</b>" , 

	                 'sys_info' => "<b>基本資料設定</b>" ,

	           'admin_list' => "<b>管理員設定</b>"
	           );
}


/*****************************************************
 /* 名稱           : get_backend_funcs_for_id
 * 功能           : 依管理者ID，取得權限功能表
 * 說明           :
 ****************************************************/
function get_backend_funcs_for_id($admin_id){

	$CI =& get_instance();

	$CI->db->where('admin_id' ,$admin_id);
	$CI->db->where('status' ,1);
	$query = $CI->db->get('ct_role_tables')->result_array();

	$result = array();

	foreach ($query as $row) {
		array_push($result, $row['tb_name']);
	}

	return $result;
}


/*****************************************************
 /* 名稱           : specification_format
 * 功能           : 定義產品規格顯示格式。
 ****************************************************/
function specification_format($str){

	$ary_temp = explode(',', $str);
	$ary_new  = array();

	for($i=0 ; $i < count($ary_temp) ; $i++){
		$ary_new[] = $ary_temp[$i];
	}

	return $ary_new;
}


/*****************************************************
 /* 名稱           : is_edate_valid
 * 功能           : 檢查商家到期日。
 ****************************************************/
function is_edate_valid($edate){

	$dt1 = date('Y-m-d');
	$dt2 = substr( $edate ,0 ,10 );

	if( strtotime($dt1) > strtotime($dt2)){
		return true;
	}else{
		return false;
	}

}


/*****************************************************
 /* 名稱           : get_product_stock
 * 功能           : 依商品ID，回傳該商品的庫存量。
 ****************************************************/
function get_product_stock($product_id){

	$CI =& get_instance();

	$CI->db->where('product_id', $product_id);
	$query = $CI->db->get('product_speci')->result_array();

	return $query;
}


/*****************************************************
 /* 名稱           : calc_bonus
 * 功能           : 紅利點數計算。
 ****************************************************/
function calc_bonus($money){

	//1)加計紅利客戶負擔5%金額(無條件進位)
	//$bonus = $money * 1.05;
	$bonus = $money * 1;

	//2)換算紅利點數(百位以下無條件進入)
	//$bonus = ceil($bonus/7)* 100;
	$bonus = round( (100/7)*$bonus ,2);

	return $bonus;
}


/*****************************************************
 /* 名稱           : calc_bonus_money
 * 功能           : 紅利點數 / 實際授權金額。
 ****************************************************/
function calc_bonus_money($bonus){

	return $bonus/100*7;
}


/*****************************************************
 /* 名稱           : get_products
 * 功能           : 依分類ID，回傳所屬分類之所有商品。
 ****************************************************/
function get_products($category_id){

	$CI =& get_instance();

	$CI->db->where('category_id', $category_id);
	$query = $CI->db->get('product')->result_array();

	return $query;
}


/*****************************************************
 /* 名稱           : get_payMethod_money
 * 功能           : 依付款方式計算金額。
 ****************************************************/
function get_payMethod_money( $type ,$money ){

	switch ($type) {

		//紅利支付(返回紅利點數)
		case 'bonus':
			return calc_bonus($money);
			break;

			//點加金
		case 'bonus_money':
			$money = ceil($money/2);
			$bonus = calc_bonus($money);
			return addCommas($bonus).','.addCommas($money);
			break;

			//分3期
		case 'staging_3':
			return ( $money / 3 );
			break;

			//分6期
		case 'staging_6':
			return ( $money / 6 );
			break;

	}

}


/*****************************************************
 /* 名稱           : get_payMethod_money_tpl
 * 功能           : 依付款方式計算金額。
 * 傳入值      : $pay_method 格式 : 'staging_3,cy1252222'
 ****************************************************/
function get_payMethod_money_tpl( $pay_method ,$money ){

	$type = explode(',', $pay_method);

	switch ($type[0]) {

		//紅利支付(返回紅利點數)
		case 'bonus':
			return '<strong>'.$type[1].'</strong>'.' 紅利點數<b>'.addCommas(get_payMethod_money( $type[0] ,$money )).'</b> 點';

			//點加金
		case 'bonus_money':
			$money = ceil($money/2);
			$bonus = calc_bonus($money);
			return '<strong>'.$type[1].'</strong>'.' 自付額<b>'.addCommas($bonus).'</b> 點 + <b>NT$'.addCommas($money).'</b>';

			//分3期
		case 'staging_3':
			return '<strong>'.$type[1].'</strong>'.' 分期價<b>NT$'.addCommas(get_payMethod_money( $type[0] ,$money )).'</b> x 3期';

			//分6期
		case 'staging_6':
			return '<strong>'.$type[1].'</strong>'.' 分期價<b>NT$'.addCommas(get_payMethod_money( $type[0] ,$money )).'</b> x 6期';

	}

}




/*****************************************************
 /* 名稱         : get_payMethod_money_tpl_result
 * 功能           : 依付款方式計算金額。
 * 傳入值      : $pay_method 格式 : 'staging_3,cy1252222'
 *           $OriginalAmt 付款金額
 *           $OffsetAmt 折抵金額
 ****************************************************/
function get_payMethod_money_tpl_result( $pay_method ,$OriginalAmt ,$OffsetAmt ){

	$type = explode(',', $pay_method);

	switch ($type[0]) {

		//紅利支付(返回紅利點數)
		case 'bonus':
			return '<strong>'.$type[1].'</strong>'.' 紅利點數<b>'.addCommas(calc_bonus($OffsetAmt)).'</b> 點,自付額<b>NT$'.addCommas($OriginalAmt-$OffsetAmt).'</b>';

			//點加金
		case 'bonus_money':
			return '<strong>'.$type[1].'</strong>'.' 自付額<b>'.addCommas(calc_bonus($OffsetAmt)).'</b> 點 + <b>NT$'.addCommas($OriginalAmt-$OffsetAmt).'</b>';

			//分3期
		case 'staging_3':
			return '<strong>'.$type[1].'</strong>'.' 分期價<b>NT$'.addCommas(get_payMethod_money( $type[0] ,$OriginalAmt )).'</b> x 3期';

			//分6期
		case 'staging_6':
			return '<strong>'.$type[1].'</strong>'.' 分期價<b>NT$'.addCommas(get_payMethod_money( $type[0] ,$OriginalAmt )).'</b> x 6期';

	}

}


/*****************************************************
 /* 名稱           : srh_customer_list
 * 功能           : [會員列表]搜尋狀態列
 * 說明           : 針對下拉式選單的表現方式。
 ****************************************************/
function srh_customer_list($key){

	$data = array(
         '1,0' =>  '不拘' ,        
	       '1' =>  '啟用' ,  
           '0' =>  '停用' 
           );
            
           if(!array_key_exists($key ,$data)){
           	return $data;
           }else{
           	$data = array($key => $data[$key]) + $data;
           	return $data;
           }
            
}


/* End of file ctmall */
