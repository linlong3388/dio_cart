<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 方法 : 資料庫函數
 * @helpers database
 * @author Dio
 *
 */


/*****************************************************
 /* 名稱           : check_duplicate
 * 功能           : 欄位不可重複的檢查(多欄位)。
 * 傳入參數 : Array(資料表名 ,欄位名1 ,欄位名2,欄位名3...)
 * 回傳值      : T/F
 ****************************************************/
function check_duplicate($data){

	$CI =& get_instance();

	//拆解陣列元素 ,表名 / 欄位 / 資料
	$data_table = $data['table_name'];
	array_shift($data);

	foreach($data as $key => $val){
		$CI->db->where($key ,$val);
	}

	$row = $CI->db->get($data_table)->row_array();

	if($row){
		return true;
	}else{
		return false;
	}
}


/*****************************************************
 /* 名稱           : get_assign_field
 * 功能           : 取得指定的欄位。
 * 傳入參數 : 資料表名  ,鍵名 ,鍵ID值 ,返回的欄位名
 * 回傳值      :
 ****************************************************/
function get_assign_field($table_name ,$pkey_name ,$id ,$field){

	$CI =& get_instance();
	$row = $CI->db->get_where($table_name ,array("{$pkey_name}" => $id ))->row_array();

	if($row){
		return $row[$field];
	}else{
		return '';
	}
}


/*****************************************************
 /* 名稱           : get_table_data
 * 功能           : 取得Table資料。
 * 傳入參數        : [資料表名]、[筆數]
 * 回傳值
 ****************************************************/
function get_table_data($tbname ,$count=10){

	$CI =& get_instance();
	$CI->db->limit($count);
	$query = $CI->db->get($tbname)->result_array();

	return $query;
}


/*****************************************************
 /* 名稱           : get_master_detail_id
 * 功能           : 在M-D架構下 ,取得detail_id的最大值。
 * 傳入參數 : 資料表名 ,主鍵 ,主鍵值,副鍵值
 * 回傳值
 ****************************************************/
function get_master_detail_id($tbname ,$master_field ,$master_id ,$detail_id){

	$CI =& get_instance();

	$CI->db->select_max("{$detail_id}" ,'max');
	$CI->db->where("{$master_field}" ,$master_id);

	$max_id = $CI->db->get("{$tbname}")->row_array();
	$max_id = $max_id['max'] + 1;

	return $max_id;
}


/*****************************************************
 /* 名稱           : get_session_info
 * 功能           : 取得 session 的綜合資訊  ,方便存入資料庫。
 ****************************************************/
function get_session_info() {

	$CI =& get_instance();

	$data = array(
                   'session_id'  =>  session_id() ,   
                   'ip_address'  =>  $CI->input->ip_address() ,
                   'user_agent'  =>  $CI->input->user_agent() ,
                   'cdate'       =>  date('Y-m-d H:i:s')
	);

	return $data;
}


/*****************************************************
 /* 名稱           : get_action_by_type
 * 功能           : 取得action資料。
 * 傳入參數 :
 * 回傳值
 ****************************************************/
function get_action_by_type($type){

	$CI =& get_instance();
	$CI->db->where('type',$type);
	$CI->db->limit(10);

	$query = $CI->db->get('ct_action')->result_array();

	return $query;
}


/*****************************************************
 /* 名稱           : get_breadcrumb_name
 * 功能           : 導覽(麵包屑)功能名稱。
 * 傳入參數 :
 * 回傳值      :
 ****************************************************/
function get_breadcrumb_name($category_id_1 , $category_id_2 ,$action_id){

	$CI =& get_instance();
	$query = array();

	//依[類別] / 商品分類搜尋(大分類)
	if( !empty($category_id_1) ){
		$CI->db->where('category_id',$category_id_1);
		$query = $CI->db->get('category')->row_array();
	}

	//依[類別] / 商品分類搜尋(次分類)
	if( !empty($category_id_2) ){
		$CI->db->where('category_id',$category_id_2);
		$query = $CI->db->get('category')->row_array();
	}

	//依[活動分類]
	if( !empty($action_id) ){
		$CI->db->where('action_id',$action_id);
		$query = $CI->db->get('ct_action')->row_array();
	}

	if($query){
		return $query['name'];
	}else{
		return '';
	}

}


/*****************************************************
/* 名稱           : get_why_not_try
 * 回傳值      :
 ****************************************************/
function get_why_not_try(){

	$CI =& get_instance();
	$CI->load->model('frontend/product_model' ,'get_why_not_try');

	//---------------------------------------------------------------
	// 商品資料搜尋
	//---------------------------------------------------------------
	$srh_data['srh_action_id'] = 5;
	$srh_data['num'] = 40;

	$data['srh_param'] = $srh_data;
	$data['query_product'] = $CI->get_why_not_try->shr_product($srh_data);

	return $data['query_product'];
}


/*****************************************************
 /* 名稱           : get_product_speci_image
 * 功能           : 依product_speci_id取得product_speci_image資料。
 * 傳入參數 :
 * 回傳值
 ****************************************************/
function get_product_speci_image($product_speci_id){

	$CI =& get_instance();
	$CI->db->where('product_speci_id' ,$product_speci_id);
	$query = $CI->db->get('product_speci_image')->result_array();

	return $query;
}


/* End of file database */
