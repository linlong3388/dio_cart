<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 方法 : 系統函數
 * 
 * @helpers dio_sys
 * @author Dio
 *
 */

// ------------------------------------------------------------------------

/**
 * changeLanguage
 *
 * 改變語系
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('changeLanguage'))
{
	function changeLanguage($language)
	{
 	  if( !empty($language) ){
		 $_SESSION['language'] = $language;
	  }
	}
}

// ------------------------------------------------------------------------

/**
 * changeLanguageDb
 *
 * 改變語系(資料庫)
 *
 * @access	public
 * @return	string
 */
function changeLanguageDb($val){

	switch ($_SESSION['language']) {
		case 'zh_tw':
			return $val;

		case 'en':
			return $val.'_en';
			
		case 'jp':
			return $val.'_jp';
	}
}


/* End of file dio_sys */