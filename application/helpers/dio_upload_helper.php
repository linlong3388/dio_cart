<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 方法 : 檔案上傳函數
 * @helpers dio_upload
 * @author Dio
 *
 */

/*****************************************************
 /* 名稱           : dio_upload
 * 功能           : 檔案上傳
 * 傳入值      : [編號][圖檔欄位名]
 * 說明           : 1)配置檔案路徑、檔名、和各種判斷
 *           2)返回圖檔路徑
 ****************************************************/
function dio_upload($id = 'img' ,$field){

	//-------------------------------------------------------------------
	// 基本資料設定
	//-------------------------------------------------------------------
	$fpath = './resources/user/';  //路徑
	$fext = end(explode('.', $_FILES[$field]["name"])); //副檔名
	$fname = $id.'_'.uniqid().'.'.$fext; //檔名

	$data = array();

	if ($_FILES[$field]["error"] > 0){
		 
		switch ($_FILES[$field]["error"]) {
			case UPLOAD_ERR_INI_SIZE:
				$data['fail'] = "*抱歉!檔案大小超出了伺服器上傳限制!";
				break;
			case UPLOAD_ERR_FORM_SIZE:
				$data['fail'] = "*抱歉!要上傳的檔案大小超出瀏覽器限制";
				break;
			case UPLOAD_ERR_PARTIAL:
				$data['fail'] = "*抱歉!檔案僅部分被上傳";
				break;
			case UPLOAD_ERR_NO_FILE:
				$data['fail'] = "*抱歉!檔案未能順利上傳!";
				break;
			case UPLOAD_ERR_NO_TMP_DIR:
				$data['fail'] = "*抱歉!伺服器臨時資料夾遺失!";
				break;
			case UPLOAD_ERR_CANT_WRITE:
				$data['fail'] = "*抱歉!檔案無法寫入硬碟!";
				break;
			case UPLOAD_ERR_EXTENSION:
				$data['fail'] = "File upload stopped by extension";
				break;

			default:
				$data['fail'] = "*抱歉!發生未知的檔案寫入錯誤!";
				break;
		}

		return $data;
		 
	}else{
		 
		//檔名不得重複
		if(file_exists($fpath.$fname)){
			$data['fail'] = "* 抱歉!檔案已經存在，請勿重覆上傳相同檔案!";
			return $data;
		}

		//只能允許圖檔格式
		if (strcasecmp($fext, "jpg") != 0 && strcasecmp($fext, "png") != 0 && strcasecmp($fext, "jpeg") != 0 && strcasecmp($fext, "gif") != 0 ) {
			$data['fail'] = "* 抱歉!檔案格式需為 JPG,JPEG,PNG,GIF!";
			return $data;
		}

		//檔案不得超過 1M = 1*1024*1024
		if ($_FILES[$field]["size"] > (1*MB) ) {
			$data['fail'] = "* 抱歉!您的檔案太大，無法上傳!";
			return $data;
		}
		 
		move_uploaded_file($_FILES[$field]["tmp_name"] ,$fpath.$fname);
		$data['success'] = $fname;

		return $data;
	}

}

?>