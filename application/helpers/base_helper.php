<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 方法 : 基本常數定義
 * 說明 : 系統常用的常數定義，通常是幾乎不會再異動的參數值(如:訂單狀態)。
 *
 * @helpers base
 * @author Dio
 *
 */

/*****************************************************************************
 /* 名稱           : get_order_status
 * 功能           : 取得訂單狀態資料
 * 說明           : 返回值有底下幾種情況 ，如:
 *           1) 傳入空值 返回 array() 全部資料
 *           2) 傳入鍵值 返回  單筆資料
 *           3) 傳入鍵值 排序後，返回 array() 全部資料
 *           4) 傳入空值 排序後，返回 array() 全部資料(搜尋用)
 ****************************************************************************/
function get_order_status($type=1 ,$key_id=0){

	$data =  array(
	               '0' =>  '失敗' ,
	               '1' =>  '處理中' , //通過金流
	               '2' =>  '已出貨' ,  
	               '3' =>  '已取消' 
	);

	switch ($type) {
		case 1:  //返回陣列
			return $data;
			break;
			 
		case 2: //返回字串
			return $data[$key_id];
			break;
			 
		case 3: //返回陣列(排序)
			$data = array($key_id => $data[$key_id]) + $data;
			return $data;
			break;

		case 4: //返回陣列(排序)
			if($key_id == "" || $key_id == "*"){
				$data = array('*' => '不拘') + array($key_id => $data[$key_id]) + $data;
			}else{
				$data = array($key_id => $data[$key_id]) + array('*' => '不拘') + $data;
			}
			return $data;
			break;

	}

}


/*****************************************************************************
/* 名稱           : get_common_status
 * 功能           : 取得一般(通用)狀態資料
 ****************************************************************************/
function get_common_status($key_id=1){

	$data =  array( '0' =>  '停用' ,
	                '1' =>  '啟用' 
	              );

	return $data[$key_id];
}


/*****************************************************************************
 /* 名稱           : get_language
 * 功能           : 取得語系狀態資料
 * 說明           : 返回值有底下幾種情況 ，如:
 *           1) 傳入空值 返回 array() 全部資料
 *           2) 傳入鍵值 返回  單筆資料
 *           3) 傳入鍵值 排序後，返回 array() 全部資料
 *           4) 傳入空值 排序後，返回 array() 全部資料(搜尋用)
 ****************************************************************************/
function get_language($type=1 ,$key_id=0){

	$data =  array('en' => 'en',
                 'tw' => 'tw',
                 'cn' => 'cn',
                 'sp' => 'sp');

	switch ($type) {
		case 1:  //返回陣列
			return $data;
			break;
			 
		case 2: //返回字串
			return $data[$key_id];
			break;
			 
		case 3: //返回陣列(排序)
			$data = array($key_id => $data[$key_id]) + $data;
			return $data;
			break;

		case 4: //返回陣列(排序)
			if($key_id == "" || $key_id == "*"){
				$data = array('*' => '不拘') + array($key_id => $data[$key_id]) + $data;
			}else{
				$data = array($key_id => $data[$key_id]) + array('*' => '不拘') + $data;
			}
			return $data;
			break;

	}

}


/*****************************************************************************
 /* 名稱           : get_currency
 * 功能           : 取得幣別狀態資料
 * 說明           : 返回值有底下幾種情況 ，如:
 *           1) 傳入空值 返回 array() 全部資料
 *           2) 傳入鍵值 返回  單筆資料
 *           3) 傳入鍵值 排序後，返回 array() 全部資料
 *           4) 傳入空值 排序後，返回 array() 全部資料(搜尋用)
 ****************************************************************************/
function get_currency($type=1 ,$key_id=0){

	$data =  array('gbp' => 'gbp',
                 'twd' => 'twd',
                 'usd' => 'usd',
                 'spd' => 'spd');

	switch ($type) {
		case 1:  //返回陣列
			return $data;
			break;
			 
		case 2: //返回字串
			return $data[$key_id];
			break;
			 
		case 3: //返回陣列(排序)
			$data = array($key_id => $data[$key_id]) + $data;
			return $data;
			break;

		case 4: //返回陣列(排序)
			if($key_id == "" || $key_id == "*"){
				$data = array('*' => '不拘') + array($key_id => $data[$key_id]) + $data;
			}else{
				$data = array($key_id => $data[$key_id]) + array('*' => '不拘') + $data;
			}
			return $data;
			break;

	}

}


/*****************************************************************************
 /* 名稱           : get_birthday_year
 * 功能           : 取得生日/年
 * 說明           : 返回值有底下幾種情況 ，如:
 *           1) 傳入空值 返回 array() 全部資料
 *           2) 傳入鍵值 返回  單筆資料
 *           3) 傳入鍵值 排序後，返回 array() 全部資料
 *           4) 傳入空值 排序後，返回 array() 全部資料(搜尋用)
 ****************************************************************************/
function get_birthday_year($type=1 ,$key_id=0){

	$data = array();

	for($i=1923 ; $i<=2000 ; $i++ ) {
		$data[$i] = $i;
	}

	switch ($type) {
		case 1:  //返回陣列
			return $data;
			break;
			 
		case 2: //返回字串
			return $data[$key_id];
			break;
			 
		case 3: //返回陣列(排序)
			$data = array($key_id => $data[$key_id]) + $data;
			return $data;
			break;

		case 4: //返回陣列(排序)
			if($key_id == "" || $key_id == "*"){
				$data = array('*' => '不拘') + array($key_id => $data[$key_id]) + $data;
			}else{
				$data = array($key_id => $data[$key_id]) + array('*' => '不拘') + $data;
			}
			return $data;
			break;

	}

}


/*****************************************************************************
 /* 名稱           : get_credit_card_year
 * 功能           : 取得信用卡到期日/年
 * 說明           : 返回值有底下幾種情況 ，如:
 *           1) 傳入空值 返回 array() 全部資料
 *           2) 傳入鍵值 返回  單筆資料
 *           3) 傳入鍵值 排序後，返回 array() 全部資料
 *           4) 傳入空值 排序後，返回 array() 全部資料(搜尋用)
 ****************************************************************************/
function get_credit_card_year($type=1 ,$key_id=0){

	$data = array();

	for($i=date('Y') ; $i<=date('Y', strtotime('+15 year')) ; $i++ ) {
		$data[$i] = $i;
	}

	switch ($type) {
		case 1:  //返回陣列
			return $data;
			break;
			 
		case 2: //返回字串
			return $data[$key_id];
			break;
			 
		case 3: //返回陣列(排序)
			$data = array($key_id => $data[$key_id]) + $data;
			return $data;
			break;

		case 4: //返回陣列(排序)
			if($key_id == "" || $key_id == "*"){
				$data = array('*' => '不拘') + array($key_id => $data[$key_id]) + $data;
			}else{
				$data = array($key_id => $data[$key_id]) + array('*' => '不拘') + $data;
			}
			return $data;
			break;

	}

}


/*****************************************************************************
 /* 名稱           : get_action
 * 功能           : 取得幣別狀態資料
 * 說明           : 返回值有底下幾種情況 ，如:
 *           1) 傳入空值 返回 array() 全部資料
 *           2) 傳入鍵值 返回  單筆資料
 *           3) 傳入鍵值 排序後，返回 array() 全部資料
 *           4) 傳入空值 排序後，返回 array() 全部資料(搜尋用)
 ****************************************************************************/
function get_action($type=1 ,$key_id=0){

	$data =  array('1' => '一般' //,
	//'2' => 'WHY NOT TRY'
	);

	switch ($type) {
		case 1:  //返回陣列
			return $data;
			break;
			 
		case 2: //返回字串
			return $data[$key_id];
			break;
			 
		case 3: //返回陣列(排序)
			$data = array($key_id => $data[$key_id]) + $data;
			return $data;
			break;

		case 4: //返回陣列(排序)
			if($key_id == "" || $key_id == "*"){
				$data = array('*' => '不拘') + array($key_id => $data[$key_id]) + $data;
			}else{
				$data = array($key_id => $data[$key_id]) + array('*' => '不拘') + $data;
			}
			return $data;
			break;

	}

}


/*****************************************************************************
 /* 名稱           : get_return_status
 * 功能           : 取得退貨單狀態資料
 * 說明           : 返回值有底下幾種情況 ，如:
 *           1) 傳入空值 返回 array() 全部資料
 *           2) 傳入鍵值 返回  單筆資料
 *           3) 傳入鍵值 排序後，返回 array() 全部資料
 *           4) 傳入空值 排序後，返回 array() 全部資料(搜尋用)
 ****************************************************************************/
function get_return_status($type=1 ,$key_id=0){

	$data =  array(
			       '0' =>  '失敗' ,
	               '1' =>  '處理中' , 
	               '2' =>  '已退款' ,  
	               '3' =>  '已更換' 
	               );

	               switch ($type) {
	               	case 1:  //返回陣列
	               		return $data;
	               		break;
	               		 
	               	case 2: //返回字串
	               		return $data[$key_id];
	               		break;
	               		 
	               	case 3: //返回陣列(排序)
	               		$data = array($key_id => $data[$key_id]) + $data;
	               		return $data;
	               		break;

	               	case 4: //返回陣列(排序)
	               		if($key_id == "" || $key_id == "*"){
	               			$data = array('*' => '不拘') + array($key_id => $data[$key_id]) + $data;
	               		}else{
	               			$data = array($key_id => $data[$key_id]) + array('*' => '不拘') + $data;
	               		}
	               		return $data;
	               		break;

	               }

}


/*****************************************************************************
/* 名稱           : get_ad_home_type_data
 * 功能           : 取得首頁廣告類型的資料
 * 說明           : 返回值有底下幾種情況 ，如:
 *           1) 傳入空值 返回 array() 全部資料
 *           2) 傳入鍵值 返回  單筆資料
 *           3) 傳入鍵值 排序後，返回 array() 全部資料
 *           4) 傳入空值 排序後，返回 array() 全部資料(搜尋用)
 ****************************************************************************/
function get_ad_home_type_data($type=1 ,$key_id=0){

	$data =  array(
	               'a' => 'A區 - (頂部)大輪播器(1920x600)' ,
	           );                             

	switch ($type) {
		case 1:  //返回陣列
			return $data;
			break;
			 
		case 2: //返回字串
			return $data[$key_id];
			break;
			 
		case 3: //返回陣列(排序)
			$data = array($key_id => $data[$key_id]) + $data;
			return $data;
			break;

		case 4: //返回陣列(排序)
			if($key_id == "" || $key_id == "*"){
				$data = array('*' => '不拘') + array($key_id => $data[$key_id]) + $data;
			}else{
				$data = array($key_id => $data[$key_id]) + array('*' => '不拘') + $data;
			}
			return $data;
			break;

	}

}


/*****************************************************************************
/* 名稱           : sort_array
 * 功能           : 排序陣列資料
 * 說明           : 返回值有底下幾種情況 ，如:
 *           1) 傳入空值 返回 array() 全部資料
 *           2) 傳入鍵值 返回  單筆資料
 *           3) 傳入鍵值 排序後，返回 array() 全部資料
 *           4) 傳入空值 排序後，返回 array() 全部資料(搜尋用)
 ****************************************************************************/ 
 function sort_array($data ,$type=1 ,$key_id=0){
     	
	   switch ($type) {
		  case 1:  //返回陣列
			return $data;
			break;
			 
		  case 2: //返回字串
			return $data[$key_id];
			break;
			 
		  case 3: //返回陣列(排序)
			$data = array($key_id => $data[$key_id]) + $data;
			return $data;
			break;

		  case 4: //返回陣列(排序)
			if($key_id == "" || $key_id == "*"){
				$data = array('*' => '不拘') + $data; 
			}else{
				$data = array($key_id => $data[$key_id]) + array('*' => '不拘') + $data;
			}
			return $data;
			break;
	    } 
 
}


/*****************************************************
/* 名稱           : g_order_id
 * 功能           : 產生訂單編號
 * 說明           :
 *           格式:  time(10) + 隨機碼(6)
 *                  1469866221 989205 
 ****************************************************/
function g_order_id(){

	return time() . rand(100000, 999999);
}


/*****************************************************
/* 名稱           : getCvsStoreTransId
 * 功能           : 產生超商交易編號
 * 說明           :
 *           格式: cvs + time(10) + 隨機碼(7)
 *                 cvs + 1469866221 9892051
 ****************************************************/
function getCvsStoreTransId(){

	return 'cvs' . time() . rand(1000000, 9999999);
}


/*****************************************************
/* 名稱           : g_coupon_id
 * 功能           : 產生折價券編號
 * 說明           :
 *           格式:  隨機碼(10)
 *                  54698989205
 ****************************************************/
function g_coupon_id(){

	return rand(1000000000, 9999999999);
}


/* End of file base */