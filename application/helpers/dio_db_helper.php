<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 方法 : 資料庫函數
 * 
 * @helpers dio_db
 * @author Dio
 *
 */

// ------------------------------------------------------------------------

/**
 * getPropertyCategoryItem
 *
 * 取得[商品屬性]項目
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getPropertyCategoryItem'))
{
	function getPropertyCategoryItem($property_category_id)
	{
	   $CI =& get_instance();
	   //$CI->db->order_by('sort_order' ,'DESC');
	   $CI->db->where('property_category_id' ,$property_category_id);
	   $CI->db->where('status' ,1);
	   
	   return $CI->db->get('property')->result_array();
	}
}

// ------------------------------------------------------------------------

/**
 * getProdductSpeciImage
 *   
 * 取得[商品屬性]圖片
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getProdductSpeciImage'))
{
	function getProdductSpeciImage($product_speci_id)
	{
		$CI =& get_instance();
		$CI->db->where('product_speci_id' ,$product_speci_id);
		$CI->db->where('status' ,1);

		return $CI->db->get('product_speci_image')->result_array();
	}
}

// ------------------------------------------------------------------------

/**
 * getQaCategory
 *
 * 取得所有[Q&A分類]
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getQaCategory'))
{
	function getQaCategory()
	{
		$CI =& get_instance();
		//$CI->db->where('status' ,1);
		$CI->db->order_by('sort_order' ,'DESC');

		return $CI->db->get('qa_category')->result_array();
	}
}

// ------------------------------------------------------------------------

/**
 * getNewsCategory
 *
 * 取得所有[活動訊息分類]
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getNewsCategory'))
{
	function getNewsCategory()
	{
		$CI =& get_instance();
		//$CI->db->where('status' ,1);
		$CI->db->order_by('sort_order' ,'DESC');

		return $CI->db->get('news_category')->result_array();
	}
}

// ------------------------------------------------------------------------

/**
 * transOrderShowIdToOrderId
 *
 * 轉換訂單編號
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('transOrderShowIdToOrderId'))
{
	function transOrderShowIdToOrderId($order_id)
	{
		if( strlen($order_id) >= 16 ) {
			$CI =& get_instance();
			$CI->db->where('order_show_id' ,$order_id);
			$query = $CI->db->get('order')->row_array();

			$order_id = $query['order_id'];
		}

		return $order_id;
	}

}

// --------------------------------------------------------------------

/**
 * 方法 : 依ID取得商品數量
 *
 * @access	public
 * @param
 * @return
 */
if ( ! function_exists('getProductEntity'))
{
	function getProductEntity($product_id)
	{
		$CI =& get_instance();

		$CI->db->where('product_id' ,$product_id);
		$query = $CI->db->get('product_speci')->row_array();
		
		return $query['stock']; 
	}
}

// ------------------------------------------------------------------------

/**
 * getBranch
 *
 * 取得[服務據點]
 * 
 * @access	public
 * @return	array
 */
if ( ! function_exists('getBranch'))
{
	function getBranch()
	{
		$CI =& get_instance();

		$CI->db->where('status' ,1);
		$CI->db->order_by('sort_order' ,'DESC');
		
		return $CI->db->get('branch')->result_array();
	}
}

// ------------------------------------------------------------------------

/**
 * getCategoryPromo
 *
 * 取得[促銷分類]
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getCategoryPromo'))
{
	function getCategoryPromo()
	{
		$CI =& get_instance();

		$CI->db->where('status' ,1);
		$CI->db->order_by('sort_order' ,'DESC');

		return $CI->db->get('category_promo')->result_array();
	}
}

// --------------------------------------------------------------------

/**
 * 方法 : 取得商品相關資料列表
 *
 * @access	public
 * @param
 * @return
 */
if ( ! function_exists('getProductRelatedList'))
{
	function getProductRelatedList( $param )
	{
		
		$CI =& get_instance();

		$CI->load->model("frontend/product_model","product");
		
		$srh_data = setProductListSearch(array('srh_keyword' => $param['keyword'] 
				                              ,'srh_product_id' => $param['product_id']) ,array('limit1' => 0 ,'limit2' => 11));
		
		return $CI->product->lists($srh_data);
	}
}

// --------------------------------------------------------------------

/**
 * 方法 : 搜尋
 *
 * @access	public
 * @param
 * @return
 */
function setProductListSearch ($data ,$pg_ary) {

	if(isset($data) && !empty($data)){
		foreach ($data as $key=>$val) {
			if($val == ''){
				unset($data[$key]);
			}
		}
	}

	//兩層分類(格式如1_5)
	$data['srh_category_id_1'] = '';
	$data['srh_category_id_2'] = '';

	if( isset($data['srh_category']) && $data['srh_category'] != '*' ){
		$category_id = explode('_',$data['srh_category']);
		$data['srh_category_id_1'] = $category_id[0];
		$data['srh_category_id_2'] = $category_id[1];
	}

	$data['srh_limit1']   = $pg_ary['limit1'];
	$data['srh_limit2']   = $pg_ary['limit2'];

	return $data;
}

// ------------------------------------------------------------------------

/**
 * getCommonDataMaxFuncId
 *
 * 取得共用資料最後ID值
 *
 * @access	public
 * @param  [表名],[功能名]
 * @return	array
 */
if ( ! function_exists('getCommonDataMaxFuncId'))
{
	function getCommonDataMaxFuncId($tb_name ,$func)
	{
		$CI =& get_instance();

		$CI->db->where('func' ,$func);
		$CI->db->select_max('id');

		$query = $CI->db->get($tb_name)->row_array();
			
		return ($query['id'] + 1 );
	}
}

// ------------------------------------------------------------------------

/**
 * isCategory_3
 *
 * 是否為助糧平台的子分類項商品
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('isCategory_3'))
{
	function isCategory_3( $category_multi_id )
	{
		$category_multi_id_ary = explode(',', $category_multi_id);
		
		$CI =& get_instance();

		$sql = "SELECT * FROM `category` c
		          WHERE c.status = 1 
				  AND c.level = 3
				  AND c.path REGEXP '(^|,)(3)(,|$)' ";
		
		$query = $CI->db->query($sql)->result_array();
		
		foreach ($query as $row) {
			
			if(in_array($row['category_id'], $category_multi_id_ary)){
				return true;
			}
		}
		
		return false;
	}
}

/* End of file dio_db */