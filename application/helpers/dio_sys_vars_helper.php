<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 方法 : 系統常用變數
 * 
 * @helpers dio_sys_vars
 * @author Dio
 *
 */

// ------------------------------------------------------------------------

/**
 * getAdminMenu
 *
 * 取得系統後台選單
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('getAdminMenu'))
{
	function getAdminMenu()
	{
		return array(
				
				//主控台
				'dashboard'          => '主控台' ,
				
				//關於我們
				'about'              => '關於我們' , 
				'story_img'          => '品牌故事 / 懷舊照片' ,
				'story'              => '品牌故事 / 列表' ,
				'certification_info' => '食在安心 / 介紹' ,
				'certification'      => '食在安心' ,
				'certification_img'  => '食在安心 / 圖片' ,
				'awards'             => '得獎記錄',
				'plan'               => '公益計劃' ,
				'branch'             => '門市據點',
				'download'           => '檔案下載' ,
				
				
				//網站內容
				'index'              => '首頁' ,
				'news_category'      => '最新消息 / 分類' ,
				'news'               => '最新消息' ,
				'article'            => '手作課程',
				'qa_category'        => '常見問題 / 分類' ,
				'qa'                 => '常見問題' ,
				'contact'            => '聯絡我們' ,
				
				
				//會員管理
				'customer'           => '會員管理' ,
				'customer_coupon'    => '會員優惠券' ,
				
				
				//商品管理
				'product_category'   => '商品分類' ,
				'product_category_child'   => '次類' ,
				'product'            => '商品列表' ,
				'product_comb'       => '組盒圖列表' ,
				'product_box'        => '禮盒列表' ,
				'product_attr'       => '商品屬性' ,
				'product_property'   => '商品屬性' ,
				'product_speci'      => '商品圖片屬性' ,
				'product_qa'         => '商品問答' ,
				'brand'              => '品牌管理',
				
				
				//訂單管理
				'order'              => '訂單管理' ,
				'order_pre'          => '預購單管理' ,
				'returns'            => '退貨管理' ,
				
				
				//廣告&行銷
				'ad_home'            => '首頁廣告' ,
				'ad_action'          => '活動廣告',
				'ad_content'         => '內頁廣告',
				'ad_banner'          => '橫幅廣告',
				'category_promo'     => '促銷分類' ,
				'category_promo_product'     => '促銷分類/明細' ,				
				'meta'               => 'meta管理' ,	
				'promo'              => '折價券設定' ,
				
				
				//統計報表
				'report_order'       => '訂單統計' ,
				'report_customer'    => '會員統計' ,
				'report_product'     => '商品統計' ,
				
				
				//系統管理
				'system'             => '系統設定' ,
				'fee'                => '基本運費' ,
				'fee_cod'            => '貨到付款(手續費)' ,
				'admin_group'        => '群組設定',
				'admin_group_role'   => '群組權限',
				'admin'              => '管理員',
				'tool_elfinder'      => '檔案管理' ,
				'barcode'            => '條碼管理' ,
				
				//未使用 
				'edm'                => '電子報' ,
				
				'lottery'            => '抽獎活動' ,
				'logout'             => '登出',
				
				'knowledge_category' => '知識專欄 / 分類',
				'knowledge'          => '知識專欄',
								
				'notice'             => '公告' ,
				
				'promo_coupon'       => '折價券設定' ,
				'promo_vip'          => 'VIP會員條件設定' ,
				
				'space_category'      => '空間案例 / 分類' ,
				'space_category_child'   => '次類' ,
				'space'               => '空間案例 / 列表' ,
		);
	}
}

// ------------------------------------------------------------------------

/**
 * getAdminMenuTop
 *
 * 取得系統後台頂級選單
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('getAdminMenuTop'))
{
	function getAdminMenuTop()
	{
		return array(
				'dashboard'        => '主控台' ,
				'about'            => '關於我們' ,
				'webContent'       => '網站內容' ,
				'customer'         => '會員管理' ,
				'product'          => '商品管理' ,
				'order'            => '訂單管理' ,
				'sale'             => '廣告&行銷' ,
				'statistics'       => '統計' ,
				'system'           => '系統管理' ,
		);
	}
}


// ------------------------------------------------------------------------

/**
 * getPayMethod
 *
 * 取得付款方式
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getPayMethod'))
{
	function getPayMethod()
	{
		$data = array();
		
		switch ($_SESSION['language']) {
		
			case 'zh_tw':
				$data = array(
 			  	           '0' => '線上刷卡' , //助糧商品
				           '1' => '貨到付款' ,
				           '2' => '超商取貨(7-11)' ,
				           '3' => '超商取貨(全家)' ,
				           '4' => '超商繳費' , //助糧商品
				           '5' => 'ATM轉帳' ,  //助糧商品
				);

			break;
		}
		
		return $data;
	}
}

// ------------------------------------------------------------------------

/**
 * getTransMethod
 *
 * 取得運送方式
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getTransMethod'))
{
	function getTransMethod()
	{
		$data = array();

		switch ($_SESSION['language']) {

			case 'zh_tw':
				$data =  array(
				           '0' => '宅配到府' ,
				           '1' => '門市取貨(限自由店/草悟道店) ' ,
						);
						
			break;
		}

		return $data;

	}
}

// ------------------------------------------------------------------------

/**
 * getTicketType
 *
 * 取得發票類型
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getTicketType'))
{
	function getTicketType()
	{
		return array( '0' => '二聯式發票',
				      '1' => '三聯式發票');
	}
}

// ------------------------------------------------------------------------

/**
 * getOrderStatus
 *
 * 取得訂單狀態
 * 
 * 流程 :
 *     
 *     (一) 非即時金流(如:超商付款、網路ATM...)
 *     
 *          失敗 -> 未付款 -> 處理中 -> 已出貨
 *                            |     
 *                            └---> 已取消
 *    
 *     (二) 即時金流(如:貨到付款、信用卡...)
 *
 *          失敗 -> 處理中 -> 已出貨 
 *                    |     
 *                    └---> 已取消
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getOrderStatus'))
{
	function getOrderStatus()
	{
		return  array(
				      '0' =>  '失敗' ,
				      '1' =>  '處理中' , //通過金流
				      '2' =>  '已出貨' ,
				      '3' =>  '已取消' ,		
				      '4' =>  '未付款' , //有些付款方式為延遲付款
				);	
	}
}

// ------------------------------------------------------------------------

/**
 * getOrderStatusIcon
 *
 * 取得訂單狀態圖示
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getOrderStatusIcon'))
{
	function getOrderStatusIcon($status)
	{
		$data =  array(
				     '0' =>  ' fail' ,
				     '1' =>  ' wit' , //通過金流
				     '2' =>  ' suc' ,
				     '3' =>  ' fail' ,
				     '4' =>  ' wit' ,
				);
		
		return reSortArrayMutil(2,$status , $data);
	}
}

// ------------------------------------------------------------------------

/**
 * getLoginType
 *
 * 取得登入類型
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getLoginType'))
{
	function getLoginType()
	{
	    return array( '0' => '一般',
	     			  '1' => 'Facebook' ,
		);
	}
}

// ------------------------------------------------------------------------

/**
 * getBirthDayYear
 *
 * 取得生日/年
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getBirthDayYear'))
{
	function getBirthDayYear()
	{
		$data = array();
		
		for($i=1923 ; $i<2001 ; $i++ ) {
			$data[$i] = $i;
		}
		
        return $data;
	}
}

// ------------------------------------------------------------------------

/**
 * getBirthDayMonth
 *
 * 取得生日/月
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getBirthDayMonth'))
{
	function getBirthDayMonth()
	{
		$data = array();

		for($i=1 ; $i<=12 ; $i++ ){
			$data[$i] = $i;
		}

		return $data;
	}
}

// ------------------------------------------------------------------------

/**
 * getBirthDayDay
 *
 * 取得生日/日
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getBirthDayDay'))
{
	function getBirthDayDay()
	{
		$data = array();

		for($i=1 ; $i<=31 ; $i++ ) {
			$data[$i] = $i;
		}

		return $data;
	}
}

// ------------------------------------------------------------------------

/**
 * getLocalCity
 *
 * 取得區碼/縣市
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getLocalCity'))
{
	function getLocalCity()
	{
	return array( '100 台北市 中正區' => '100 台北市 中正區',
'103 台北市 大同區' => '103 台北市 大同區',
'104 台北市 中山區' => '104 台北市 中山區',
'105 台北市 松山區' => '105 台北市 松山區',
'106 台北市 大安區' => '106 台北市 大安區',
'108 台北市 萬華區' => '108 台北市 萬華區',
'110 台北市 信義區' => '110 台北市 信義區',
'111 台北市 士林區' => '111 台北市 士林區',
'112 台北市 北投區' => '112 台北市 北投區',
'114 台北市 內湖區' => '114 台北市 內湖區',
'115 台北市 南港區' => '115 台北市 南港區',
'116 台北市 文山區' => '116 台北市 文山區',
'200 基隆市 仁愛區' => '200 基隆市 仁愛區',
'201 基隆市 信義區' => '201 基隆市 信義區',
'202 基隆市 中正區' => '202 基隆市 中正區',
'203 基隆市 中山區' => '203 基隆市 中山區',
'204 基隆市 安樂區' => '204 基隆市 安樂區',
'205 基隆市 暖暖區' => '205 基隆市 暖暖區',
'206 基隆市 七堵區' => '206 基隆市 七堵區',
'207 新北市 萬里區' => '207 新北市 萬里區',
'208 新北市 金山區' => '208 新北市 金山區',
'220 新北市 板橋區' => '220 新北市 板橋區',
'221 新北市 汐止區' => '221 新北市 汐止區',
'222 新北市 深坑區' => '222 新北市 深坑區',
'223 新北市 石碇區' => '223 新北市 石碇區',
'224 新北市 瑞芳區' => '224 新北市 瑞芳區',
'226 新北市 平溪區' => '226 新北市 平溪區',
'227 新北市 雙溪區' => '227 新北市 雙溪區',
'228 新北市 貢寮區' => '228 新北市 貢寮區',
'231 新北市 新店區' => '231 新北市 新店區',
'232 新北市 坪林區' => '232 新北市 坪林區',
'233 新北市 烏來區' => '233 新北市 烏來區',
'234 新北市 永和區' => '234 新北市 永和區',
'235 新北市 中和區' => '235 新北市 中和區',
'236 新北市 土城區' => '236 新北市 土城區',
'237 新北市 三峽區' => '237 新北市 三峽區',
'238 新北市 樹林區' => '238 新北市 樹林區',
'239 新北市 鶯歌區' => '239 新北市 鶯歌區',
'241 新北市 三重區' => '241 新北市 三重區',
'242 新北市 新莊區' => '242 新北市 新莊區',
'243 新北市 泰山區' => '243 新北市 泰山區',
'244 新北市 林口區' => '244 新北市 林口區',
'247 新北市 蘆洲區' => '247 新北市 蘆洲區',
'248 新北市 五股區' => '248 新北市 五股區',
'249 新北市 八里區' => '249 新北市 八里區',
'251 新北市 淡水區' => '251 新北市 淡水區',
'252 新北市 三芝區' => '252 新北市 三芝區',
'253 新北市 石門區' => '253 新北市 石門區',
'260 宜蘭縣 宜蘭市' => '260 宜蘭縣 宜蘭市',
'261 宜蘭縣 頭城鎮' => '261 宜蘭縣 頭城鎮',
'262 宜蘭縣 礁溪鄉' => '262 宜蘭縣 礁溪鄉',
'263 宜蘭縣 壯圍鄉' => '263 宜蘭縣 壯圍鄉',
'264 宜蘭縣 員山鄉' => '264 宜蘭縣 員山鄉',
'265 宜蘭縣 羅東鎮' => '265 宜蘭縣 羅東鎮',
'266 宜蘭縣 三星鄉' => '266 宜蘭縣 三星鄉',
'267 宜蘭縣 大同鄉' => '267 宜蘭縣 大同鄉',
'268 宜蘭縣 五結鄉' => '268 宜蘭縣 五結鄉',
'269 宜蘭縣 冬山鄉' => '269 宜蘭縣 冬山鄉',
'270 宜蘭縣 蘇澳鎮' => '270 宜蘭縣 蘇澳鎮',
'272 宜蘭縣 南澳鄉' => '272 宜蘭縣 南澳鄉',
'300 新竹市 ' => '300 新竹市 ',
'300 新竹市 北區' => '300 新竹市 北區',
'300 新竹市 東區' => '300 新竹市 東區',
'300 新竹市 香山區' => '300 新竹市 香山區',
'302 新竹縣 竹北市' => '302 新竹縣 竹北市',
'303 新竹縣 湖口鄉' => '303 新竹縣 湖口鄉',
'304 新竹縣 新豐鄉' => '304 新竹縣 新豐鄉',
'305 新竹縣 新埔鎮' => '305 新竹縣 新埔鎮',
'306 新竹縣 關西鎮' => '306 新竹縣 關西鎮',
'307 新竹縣 芎林鄉' => '307 新竹縣 芎林鄉',
'308 新竹縣 寶山鄉' => '308 新竹縣 寶山鄉',
'310 新竹縣 竹東鎮' => '310 新竹縣 竹東鎮',
'311 新竹縣 五峰鄉' => '311 新竹縣 五峰鄉',
'312 新竹縣 橫山鄉' => '312 新竹縣 橫山鄉',
'313 新竹縣 尖石鄉' => '313 新竹縣 尖石鄉',
'314 新竹縣 北埔鄉' => '314 新竹縣 北埔鄉',
'315 新竹縣 峨眉鄉' => '315 新竹縣 峨眉鄉',
'320 桃園市 中壢區' => '320 桃園市 中壢區',
'324 桃園市 平鎮區' => '324 桃園市 平鎮區',
'325 桃園市 龍潭區' => '325 桃園市 龍潭區',
'326 桃園市 楊梅區' => '326 桃園市 楊梅區',
'327 桃園市 新屋區' => '327 桃園市 新屋區',
'328 桃園市 觀音區' => '328 桃園市 觀音區',
'330 桃園市 桃園區' => '330 桃園市 桃園區',
'333 桃園市 龜山區' => '333 桃園市 龜山區',
'334 桃園市 八德區' => '334 桃園市 八德區',
'335 桃園市 大溪區' => '335 桃園市 大溪區',
'336 桃園市 復興區' => '336 桃園市 復興區',
'337 桃園市 大園區' => '337 桃園市 大園區',
'338 桃園市 蘆竹區' => '338 桃園市 蘆竹區',
'350 苗栗縣 竹南鎮' => '350 苗栗縣 竹南鎮',
'351 苗栗縣 頭份鎮' => '351 苗栗縣 頭份鎮',
'352 苗栗縣 三灣鄉' => '352 苗栗縣 三灣鄉',
'353 苗栗縣 南庄鄉' => '353 苗栗縣 南庄鄉',
'354 苗栗縣 獅潭鄉' => '354 苗栗縣 獅潭鄉',
'356 苗栗縣 後龍鎮' => '356 苗栗縣 後龍鎮',
'357 苗栗縣 通霄鎮' => '357 苗栗縣 通霄鎮',
'358 苗栗縣 苑裡鎮' => '358 苗栗縣 苑裡鎮',
'360 苗栗縣 苗栗市' => '360 苗栗縣 苗栗市',
'361 苗栗縣 造橋鄉' => '361 苗栗縣 造橋鄉',
'362 苗栗縣 頭屋鄉' => '362 苗栗縣 頭屋鄉',
'363 苗栗縣 公館鄉' => '363 苗栗縣 公館鄉',
'364 苗栗縣 大湖鄉' => '364 苗栗縣 大湖鄉',
'365 苗栗縣 泰安鄉' => '365 苗栗縣 泰安鄉',
'366 苗栗縣 銅鑼鄉' => '366 苗栗縣 銅鑼鄉',
'367 苗栗縣 三義鄉' => '367 苗栗縣 三義鄉',
'368 苗栗縣 西湖鄉' => '368 苗栗縣 西湖鄉',
'369 苗栗縣 卓蘭鎮' => '369 苗栗縣 卓蘭鎮',
'400 台中市 中區' => '400 台中市 中區',
'401 台中市 東區' => '401 台中市 東區',
'402 台中市 南區' => '402 台中市 南區',
'403 台中市 西區' => '403 台中市 西區',
'404 台中市 北區' => '404 台中市 北區',
'406 台中市 北屯區' => '406 台中市 北屯區',
'407 台中市 西屯區' => '407 台中市 西屯區',
'408 台中市 南屯區' => '408 台中市 南屯區',
'411 台中市 太平區' => '411 台中市 太平區',
'412 台中市 大里區' => '412 台中市 大里區',
'413 台中市 霧峰區' => '413 台中市 霧峰區',
'414 台中市 烏日區' => '414 台中市 烏日區',
'420 台中市 豐原區' => '420 台中市 豐原區',
'421 台中市 后里區' => '421 台中市 后里區',
'422 台中市 石岡區' => '422 台中市 石岡區',
'423 台中市 東勢區' => '423 台中市 東勢區',
'424 台中市 和平區' => '424 台中市 和平區',
'426 台中市 新社區' => '426 台中市 新社區',
'427 台中市 潭子區' => '427 台中市 潭子區',
'428 台中市 大雅區' => '428 台中市 大雅區',
'429 台中市 神岡區' => '429 台中市 神岡區',
'432 台中市 大肚區' => '432 台中市 大肚區',
'433 台中市 沙鹿區' => '433 台中市 沙鹿區',
'434 台中市 龍井區' => '434 台中市 龍井區',
'435 台中市 梧棲區' => '435 台中市 梧棲區',
'436 台中市 清水區' => '436 台中市 清水區',
'437 台中市 大甲區' => '437 台中市 大甲區',
'438 台中市 外埔區' => '438 台中市 外埔區',
'439 台中市 大安區' => '439 台中市 大安區',
'500 彰化縣 彰化市' => '500 彰化縣 彰化市',
'502 彰化縣 芬園鄉' => '502 彰化縣 芬園鄉',
'503 彰化縣 花壇鄉' => '503 彰化縣 花壇鄉',
'504 彰化縣 秀水鄉' => '504 彰化縣 秀水鄉',
'505 彰化縣 鹿港鎮' => '505 彰化縣 鹿港鎮',
'506 彰化縣 福興鄉' => '506 彰化縣 福興鄉',
'507 彰化縣 線西鄉' => '507 彰化縣 線西鄉',
'508 彰化縣 和美鎮' => '508 彰化縣 和美鎮',
'509 彰化縣 伸港鄉' => '509 彰化縣 伸港鄉',
'510 彰化縣 員林鎮' => '510 彰化縣 員林鎮',
'511 彰化縣 社頭鄉' => '511 彰化縣 社頭鄉',
'512 彰化縣 永靖鄉' => '512 彰化縣 永靖鄉',
'513 彰化縣 埔心鄉' => '513 彰化縣 埔心鄉',
'514 彰化縣 溪湖鎮' => '514 彰化縣 溪湖鎮',
'515 彰化縣 大村鄉' => '515 彰化縣 大村鄉',
'516 彰化縣 埔鹽鄉' => '516 彰化縣 埔鹽鄉',
'520 彰化縣 田中鎮' => '520 彰化縣 田中鎮',
'521 彰化縣 北斗鎮' => '521 彰化縣 北斗鎮',
'522 彰化縣 田尾鄉' => '522 彰化縣 田尾鄉',
'523 彰化縣 埤頭鄉' => '523 彰化縣 埤頭鄉',
'524 彰化縣 溪州鄉' => '524 彰化縣 溪州鄉',
'525 彰化縣 竹塘鄉' => '525 彰化縣 竹塘鄉',
'526 彰化縣 二林鎮' => '526 彰化縣 二林鎮',
'527 彰化縣 大城鄉' => '527 彰化縣 大城鄉',
'528 彰化縣 芳苑鄉' => '528 彰化縣 芳苑鄉',
'530 彰化縣 二水鄉' => '530 彰化縣 二水鄉',
'540 南投縣 南投市' => '540 南投縣 南投市',
'541 南投縣 中寮鄉' => '541 南投縣 中寮鄉',
'542 南投縣 草屯鎮' => '542 南投縣 草屯鎮',
'544 南投縣 國姓鄉' => '544 南投縣 國姓鄉',
'545 南投縣 埔里鎮' => '545 南投縣 埔里鎮',
'546 南投縣 仁愛鄉' => '546 南投縣 仁愛鄉',
'551 南投縣 名間鄉' => '551 南投縣 名間鄉',
'552 南投縣 集集鎮' => '552 南投縣 集集鎮',
'553 南投縣 水里鄉' => '553 南投縣 水里鄉',
'555 南投縣 魚池鄉' => '555 南投縣 魚池鄉',
'556 南投縣 信義鄉' => '556 南投縣 信義鄉',
'557 南投縣 竹山鎮' => '557 南投縣 竹山鎮',
'558 南投縣 鹿谷鄉' => '558 南投縣 鹿谷鄉',
'600 嘉義市 西區' => '600 嘉義市 西區',
'600 嘉義市 東區' => '600 嘉義市 東區',
'602 嘉義縣 番路鄉' => '602 嘉義縣 番路鄉',
'603 嘉義縣 梅山鄉' => '603 嘉義縣 梅山鄉',
'604 嘉義縣 竹崎鄉' => '604 嘉義縣 竹崎鄉',
'605 嘉義縣 阿里山鄉' => '605 嘉義縣 阿里山鄉',
'606 嘉義縣 中埔鄉' => '606 嘉義縣 中埔鄉',
'607 嘉義縣 大埔鄉' => '607 嘉義縣 大埔鄉',
'608 嘉義縣 水上鄉' => '608 嘉義縣 水上鄉',
'611 嘉義縣 鹿草鄉' => '611 嘉義縣 鹿草鄉',
'612 嘉義縣 太保市' => '612 嘉義縣 太保市',
'613 嘉義縣 朴子市' => '613 嘉義縣 朴子市',
'614 嘉義縣 東石鄉' => '614 嘉義縣 東石鄉',
'615 嘉義縣 六腳鄉' => '615 嘉義縣 六腳鄉',
'616 嘉義縣 新港鄉' => '616 嘉義縣 新港鄉',
'621 嘉義縣 民雄鄉' => '621 嘉義縣 民雄鄉',
'622 嘉義縣 大林鎮' => '622 嘉義縣 大林鎮',
'623 嘉義縣 溪口鄉' => '623 嘉義縣 溪口鄉',
'624 嘉義縣 義竹鄉' => '624 嘉義縣 義竹鄉',
'625 嘉義縣 布袋鎮' => '625 嘉義縣 布袋鎮',
'630 雲林縣 斗南鎮' => '630 雲林縣 斗南鎮',
'631 雲林縣 大埤鄉' => '631 雲林縣 大埤鄉',
'632 雲林縣 虎尾鎮' => '632 雲林縣 虎尾鎮',
'633 雲林縣 土庫鎮' => '633 雲林縣 土庫鎮',
'634 雲林縣 褒忠鄉' => '634 雲林縣 褒忠鄉',
'635 雲林縣 東勢鄉' => '635 雲林縣 東勢鄉',
'636 雲林縣 台西鄉' => '636 雲林縣 台西鄉',
'637 雲林縣 崙背鄉' => '637 雲林縣 崙背鄉',
'638 雲林縣 麥寮鄉' => '638 雲林縣 麥寮鄉',
'640 雲林縣 斗六市' => '640 雲林縣 斗六市',
'643 雲林縣 林內鄉' => '643 雲林縣 林內鄉',
'646 雲林縣 古坑鄉' => '646 雲林縣 古坑鄉',
'647 雲林縣 莿桐鄉' => '647 雲林縣 莿桐鄉',
'648 雲林縣 西螺鎮' => '648 雲林縣 西螺鎮',
'649 雲林縣 二崙鄉' => '649 雲林縣 二崙鄉',
'651 雲林縣 北港鎮' => '651 雲林縣 北港鎮',
'652 雲林縣 水林鄉' => '652 雲林縣 水林鄉',
'653 雲林縣 口湖鄉' => '653 雲林縣 口湖鄉',
'654 雲林縣 四湖鄉' => '654 雲林縣 四湖鄉',
'655 雲林縣 元長鄉' => '655 雲林縣 元長鄉',
'700 台南市 中西區' => '700 台南市 中西區',
'701 台南市 東區' => '701 台南市 東區',
'702 台南市 南區' => '702 台南市 南區',
'704 台南市 北區' => '704 台南市 北區',
'708 台南市 安平區' => '708 台南市 安平區',
'709 台南市 安南區' => '709 台南市 安南區',
'710 台南市 永康區' => '710 台南市 永康區',
'711 台南市 歸仁區' => '711 台南市 歸仁區',
'712 台南市 新化區' => '712 台南市 新化區',
'713 台南市 左鎮區' => '713 台南市 左鎮區',
'714 台南市 玉井區' => '714 台南市 玉井區',
'715 台南市 楠西區' => '715 台南市 楠西區',
'716 台南市 南化區' => '716 台南市 南化區',
'717 台南市 仁德區' => '717 台南市 仁德區',
'718 台南市 關廟區' => '718 台南市 關廟區',
'719 台南市 龍崎區' => '719 台南市 龍崎區',
'720 台南市 官田區' => '720 台南市 官田區',
'721 台南市 麻豆區' => '721 台南市 麻豆區',
'722 台南市 佳里區' => '722 台南市 佳里區',
'723 台南市 西港區' => '723 台南市 西港區',
'724 台南市 七股區' => '724 台南市 七股區',
'725 台南市 將軍區' => '725 台南市 將軍區',
'726 台南市 學甲區' => '726 台南市 學甲區',
'727 台南市 北門區' => '727 台南市 北門區',
'730 台南市 新營區' => '730 台南市 新營區',
'731 台南市 後壁區' => '731 台南市 後壁區',
'732 台南市 白河區' => '732 台南市 白河區',
'733 台南市 東山區' => '733 台南市 東山區',
'734 台南市 六甲區' => '734 台南市 六甲區',
'735 台南市 下營區' => '735 台南市 下營區',
'736 台南市 柳營區' => '736 台南市 柳營區',
'737 台南市 鹽水區' => '737 台南市 鹽水區',
'741 台南市 善化區' => '741 台南市 善化區',
'742 台南市 大內區' => '742 台南市 大內區',
'743 台南市 山上區' => '743 台南市 山上區',
'744 台南市 新市區' => '744 台南市 新市區',
'745 台南市 安定區' => '745 台南市 安定區',
'800 高雄市 新興區' => '800 高雄市 新興區',
'801 高雄市 前金區' => '801 高雄市 前金區',
'802 高雄市 苓雅區' => '802 高雄市 苓雅區',
'803 高雄市 鹽埕區' => '803 高雄市 鹽埕區',
'804 高雄市 鼓山區' => '804 高雄市 鼓山區',
'805 高雄市 旗津區' => '805 高雄市 旗津區',
'806 高雄市 前鎮區' => '806 高雄市 前鎮區',
'807 高雄市 三民區' => '807 高雄市 三民區',
'811 高雄市 楠梓區' => '811 高雄市 楠梓區',
'812 高雄市 小港區' => '812 高雄市 小港區',
'813 高雄市 左營區' => '813 高雄市 左營區',
'814 高雄市 仁武區' => '814 高雄市 仁武區',
'815 高雄市 大社區' => '815 高雄市 大社區',
'820 高雄市 岡山區' => '820 高雄市 岡山區',
'821 高雄市 路竹區' => '821 高雄市 路竹區',
'822 高雄市 阿蓮區' => '822 高雄市 阿蓮區',
'823 高雄市 田寮區' => '823 高雄市 田寮區',
'824 高雄市 燕巢區' => '824 高雄市 燕巢區',
'825 高雄市 橋頭區' => '825 高雄市 橋頭區',
'826 高雄市 梓官區' => '826 高雄市 梓官區',
'827 高雄市 彌陀區' => '827 高雄市 彌陀區',
'828 高雄市 永安區' => '828 高雄市 永安區',
'829 高雄市 湖內區' => '829 高雄市 湖內區',
'830 高雄市 鳳山區' => '830 高雄市 鳳山區',
'831 高雄市 大寮區' => '831 高雄市 大寮區',
'832 高雄市 林園區' => '832 高雄市 林園區',
'833 高雄市 鳥松區' => '833 高雄市 鳥松區',
'840 高雄市 大樹區' => '840 高雄市 大樹區',
'842 高雄市 旗山區' => '842 高雄市 旗山區',
'843 高雄市 美濃區' => '843 高雄市 美濃區',
'844 高雄市 六龜區' => '844 高雄市 六龜區',
'845 高雄市 內門區' => '845 高雄市 內門區',
'846 高雄市 杉林區' => '846 高雄市 杉林區',
'847 高雄市 甲仙區' => '847 高雄市 甲仙區',
'848 高雄市 桃源' => '848 高雄市 桃源',
'849 高雄市 那瑪夏區' => '849 高雄市 那瑪夏區',
'851 高雄市 茂林區' => '851 高雄市 茂林區',
'852 高雄市 茄萣區' => '852 高雄市 茄萣區',
'900 屏東縣 屏東市' => '900 屏東縣 屏東市',
'901 屏東縣 三地門' => '901 屏東縣 三地門',
'902 屏東縣 霧台鄉' => '902 屏東縣 霧台鄉',
'903 屏東縣 瑪家鄉' => '903 屏東縣 瑪家鄉',
'904 屏東縣 九如鄉' => '904 屏東縣 九如鄉',
'905 屏東縣 里港鄉' => '905 屏東縣 里港鄉',
'906 屏東縣 高樹鄉' => '906 屏東縣 高樹鄉',
'907 屏東縣 盬埔鄉' => '907 屏東縣 盬埔鄉',
'908 屏東縣 長治鄉' => '908 屏東縣 長治鄉',
'909 屏東縣 麟洛鄉' => '909 屏東縣 麟洛鄉',
'911 屏東縣 竹田鄉' => '911 屏東縣 竹田鄉',
'912 屏東縣 內埔鄉' => '912 屏東縣 內埔鄉',
'913 屏東縣 萬丹鄉' => '913 屏東縣 萬丹鄉',
'920 屏東縣 潮州鎮' => '920 屏東縣 潮州鎮',
'921 屏東縣 泰武鄉' => '921 屏東縣 泰武鄉',
'922 屏東縣 來義鄉' => '922 屏東縣 來義鄉',
'923 屏東縣 萬巒鄉' => '923 屏東縣 萬巒鄉',
'924 屏東縣 崁頂鄉' => '924 屏東縣 崁頂鄉',
'925 屏東縣 新埤鄉' => '925 屏東縣 新埤鄉',
'926 屏東縣 南州鄉' => '926 屏東縣 南州鄉',
'927 屏東縣 林邊鄉' => '927 屏東縣 林邊鄉',
'928 屏東縣 東港鎮' => '928 屏東縣 東港鎮',
'929 屏東縣 琉球鄉' => '929 屏東縣 琉球鄉',
'931 屏東縣 佳冬鄉' => '931 屏東縣 佳冬鄉',
'932 屏東縣 新園鄉' => '932 屏東縣 新園鄉',
'940 屏東縣 枋寮鄉' => '940 屏東縣 枋寮鄉',
'941 屏東縣 枋山鄉' => '941 屏東縣 枋山鄉',
'942 屏東縣 春日鄉' => '942 屏東縣 春日鄉',
'943 屏東縣 獅子鄉' => '943 屏東縣 獅子鄉',
'944 屏東縣 車城鄉' => '944 屏東縣 車城鄉',
'945 屏東縣 牡丹鄉' => '945 屏東縣 牡丹鄉',
'946 屏東縣 恆春鎮' => '946 屏東縣 恆春鎮',
'947 屏東縣 滿州鄉' => '947 屏東縣 滿州鄉',
'950 台東縣 台東市' => '950 台東縣 台東市',
'953 台東縣 延平鄉' => '953 台東縣 延平鄉',
'954 台東縣 卑南鄉' => '954 台東縣 卑南鄉',
'955 台東縣 鹿野鄉' => '955 台東縣 鹿野鄉',
'956 台東縣 關山鎮' => '956 台東縣 關山鎮',
'957 台東縣 海端鄉' => '957 台東縣 海端鄉',
'958 台東縣 池上鄉' => '958 台東縣 池上鄉',
'959 台東縣 東河鄉' => '959 台東縣 東河鄉',
'961 台東縣 成功鎮' => '961 台東縣 成功鎮',
'962 台東縣 長濱鄉' => '962 台東縣 長濱鄉',
'963 台東縣 太麻里' => '963 台東縣 太麻里',
'964 台東縣 金峰鄉' => '964 台東縣 金峰鄉',
'965 台東縣 大武鄉' => '965 台東縣 大武鄉',
'966 台東縣 達仁鄉' => '966 台東縣 達仁鄉',
'970 花蓮縣 花蓮市' => '970 花蓮縣 花蓮市',
'971 花蓮縣 新城鄉' => '971 花蓮縣 新城鄉',
'972 花蓮縣 秀林鄉' => '972 花蓮縣 秀林鄉',
'973 花蓮縣 吉安鄉' => '973 花蓮縣 吉安鄉',
'974 花蓮縣 壽豐鄉' => '974 花蓮縣 壽豐鄉',
'975 花蓮縣 鳳林鎮' => '975 花蓮縣 鳳林鎮',
'976 花蓮縣 光復鄉' => '976 花蓮縣 光復鄉',
'977 花蓮縣 豐濱鄉' => '977 花蓮縣 豐濱鄉',
'978 花蓮縣 瑞穗鄉' => '978 花蓮縣 瑞穗鄉',
'979 花蓮縣 萬榮鄉' => '979 花蓮縣 萬榮鄉',
'981 花蓮縣 玉里鎮' => '981 花蓮縣 玉里鎮',
'982 花蓮縣 卓溪鄉' => '982 花蓮縣 卓溪鄉',
'983 花蓮縣 富里鄉' => '983 花蓮縣 富里鄉',
            '離島' => '離島',
        '海外直送' => '海外直送',
				
		);
	}
}

// ------------------------------------------------------------------------

/**
 * getDateGroup
 *
 * 取得日期群組
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getDateGroup'))
{
	function getDateGroup()
	{
		return array( 'year'  =>  '年' ,
		              'month' =>  '月' ,
		              'week'  =>  '週' ,
	                  'day'   =>  '日' );
	}
}

// ------------------------------------------------------------------------

/**
 * getLanguage
 *
 * 取得語系
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getLanguage'))
{
	function getLanguage()
	{
		return array(
				'zh_tw' =>  '中文' ,
				'en'    =>  '英文' ,
				'jp'    =>  '日文' ,
		);
	}
}

// ------------------------------------------------------------------------

/**
 * getContentCategory
 *
 * 取得網站內容分類
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getContentCategory'))
{
	function getContentCategory()
	{
		return array( 
		 		   '0' =>  'ABOUT US' /*,
				   '1' =>  '新手上路' ,
				   '2' =>  '費用說明'*/
		 );
	}
}


// ------------------------------------------------------------------------

/**
 * getAdProduct
 *
 * 取得商品頁廣告
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getAdProduct'))
{
	function getAdProduct()
	{
		return array(
	             'a' => '左側(200x138)'
		);
	}
}

// ------------------------------------------------------------------------

/**
 * getAdProductCategory
 *
 * 取得商品廣告分類
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getAdProductCategory'))
{
	function getAdProductCategory()
	{
		return array(
				'0' => '產品型錄' ,
				'1' => '會員專區' ,
				'2' => '就是愛包裝' ,
		);
	}
}

// ------------------------------------------------------------------------

/**
 * getChineseMonth
 *
 * 取得中國月份
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getChineseMonth'))
{
	function getChineseMonth()
	{
		return array('01' => '一月' ,
				'02' => '二月' ,
				'03' => '三月' ,
				'04' => '四月' ,
				'05' => '五月' ,
				'06' => '六月' ,
				'07' => '七月' ,
				'08' => '八月' ,
				'09' => '九月' ,
				'10' => '十月' ,
				'11' => '十一月' ,
				'12' => '十二月' 
		);
	}
}

// ------------------------------------------------------------------------

/**
 * getBranchCategory
 *
 * 取得[分店類別]
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getBranchCategory'))
{
	function getBranchCategory()
	{
		$data = array();
		
		switch ($_SESSION['language']) {
		
			case 'zh_tw':
				$data =  array(
				           '0' => '北部展館' ,
				           '1' => '中部展館' ,
				           '2' => '南部展館' ,
						 );
			break;
		}
		
		return $data;				
	}
}

// ------------------------------------------------------------------------

/**
 * getQaCategory
 *
 * 取得[Qa]分類
 *
 * @access	public
 * @return	array
 */
/*
if ( ! function_exists('getQaCategory'))
{
	function getQaCategory()
	{	
		$data = array();		
		
		switch ($_SESSION['language']) {
		
			case 'zh_tw':
				 $data =  array(
				              '0' => '品質測試' ,
			 	              '1' => '保固說明' ,
				              '2' => '常見問題' 
						);
						
			break;					
		}
		
		return $data;			
	}
}*/

// ------------------------------------------------------------------------

/**
 * getProductGroup
 *
 * 取得[商品群組]分類
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getProductGroup'))
{
	function getProductGroup()
	{
		$data = array();
		
		switch ($_SESSION['language']) {
		
			case 'zh_tw':
				$data =  array(
				            '0' => '無' ,
				            '1' => '新品上市' ,
				            '2' => '熱賣商品' ,
				            '3' => 'OUTLET'
						);
				
			break;							
		}
		
		return $data;				
	}
}

// ------------------------------------------------------------------------

/**
 * getPropertyCategory
 *
 * 取得[商品屬性]分類
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getPropertyCategory'))
{
	function getPropertyCategory()
	{
		
		$data = array();		
		
		switch ($_SESSION['language']) {
		
			case 'zh_tw':
				$data =  array(
				            '0' => '尺寸' ,
				            '1' => '顏色' ,
				            '2' => '天數' ,
				            '3' => '類型' ,
				            '4' => '功能'
				);
				
			break;					
		}
		
		return $data;		
	}
}

// ------------------------------------------------------------------------

/**
 * getContactCategory
 *
 * 取得[聯絡我們類別]
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getContactCategory'))
{
	function getContactCategory()
	{
		return array(
				'0'    => '公司產品諮詢' ,
				'1'    => '相關費用詢價' ,
				'2'    => '合作相關事宜' ,
				'3'    => '其他服務' 
		);
	}
}

// ------------------------------------------------------------------------

/**
 * getTransZip
 *
 * 取得[配送地區]
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getTransZip'))
{
	function getTransZip()
	{
		$data = array();
		
		switch ($_SESSION['language']) {

			case 'zh_tw':
				$data =  array(
				          '0' => '本島' ,
				          '1' => '離島' ,
				          '2' => '海外直送' ,
				);
				
			break;					
		}

		return $data;
	}
}

// ------------------------------------------------------------------------

/**
 * getCouponStatus
 *
 * 取得折價券狀態
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getCouponStatus'))
{
	function getCouponStatus()
	{
		return array( 
				'0' =>  '已使用' ,
				'1' =>  '未使用' , 
				'2' =>  '無法使用' 
		);
	}
}


// ------------------------------------------------------------------------

/**
 * getCustomerLevel
 *
 * 取得會員等級
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getCustomerLevel'))
{
	function getCustomerLevel()
	{		
		$data = array();
		
		switch ($_SESSION['language']) {
		
			case 'zh_tw':
				$data =  array(
				           '0' =>  '一般會員' ,
				           '1' =>  'VIP會員' ,
				           '2' =>  'VIP會員'  //續約一年
				);

			break;
		}
		
		return $data;		
		
	}
}

// ------------------------------------------------------------------------

/**
 * getBranchCity
 *
 * 取得分店城市
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getBranchCity'))
{
	function getBranchCity()
	{	
		$data = array();
		
		switch ($_SESSION['language']) {
		
			case 'zh_tw':
				$data =  array(
'南投縣' =>  '南投縣' ,
'台北市' =>  '台北市' ,
'嘉義市' =>  '嘉義市' ,
'嘉義縣' =>  '嘉義縣' ,
'基隆市' =>  '基隆市' ,
'宜蘭縣' =>  '宜蘭縣' ,
'屏東縣' =>  '屏東縣' ,
'彰化市' =>  '彰化市' ,
'彰化縣' =>  '彰化縣' ,
'新北市' =>  '新北市' ,
'新竹市' =>  '新竹市' ,
'新竹縣' =>  '新竹縣' ,
'桃園市' =>  '桃園市' ,
'澎湖縣' =>  '澎湖縣' ,
'台中市' =>  '台中市' ,
'台南市' =>  '台南市' ,
'台東縣' =>  '台東縣' ,
'花蓮縣' =>  '花蓮縣' ,
'苗栗縣' =>  '苗栗縣' ,
'連江縣' =>  '連江縣' ,
'金門縣' =>  '金門縣' ,
'雲林縣' =>  '雲林縣' ,
'高雄市' =>  '高雄市' ,
						);
						break;
			
		}
		
		return $data;		
		
	}
}


// ------------------------------------------------------------------------

/**
 * getAdBanner
 *
 * 取得橫幅廣告類型
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getAdBanner'))
{
	function getAdBanner()
	{
		return array(
				'about'    =>  '關於我們(1920*350)' ,
				//'branch'   =>  '服務據點(1920x300)' ,
				//'contact'  =>  '聯絡我們(1920x300)' ,
				//'customer' =>  '會員專區(1920x300)' ,
				//'knowledge'     =>  '知識專欄(1920x300)' ,
				//'news'     =>  '最新消息(1920x300)' ,
				'product'  =>  '商品分類(1920*350)' ,				
				//'qa'       =>  '常見問題(1920x300)' ,
				//'promo'    =>  '熱銷好康(1200x400)' ,
		);
	}
}

// ------------------------------------------------------------------------

/**
 * getDelivery
 *
 * 取得[運送方式]
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getDelivery'))
{
	function getDelivery()
	{
		return array(			
				'0'  => '運送服務' ,				
		);
	}
}

// ------------------------------------------------------------------------

/**
 * getUserMenu
 *
 * 取得前台選單
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('getUserMenu'))
{
	function getUserMenu( $func = 0)
	{

		$data = array(

				'article_lists'  => array(
						'func'  => 'article_lists' ,
						'title' => '手作課程' ,
						'id'    => 'article_id' , ) ,
				
				'article_view'  => array(
						'func'  => 'article_view' ,
						'title' => '手作課程' ,
						'id'    => 'article_id' , ) ,
				
				
				'about_view'  => array(
						     'func'  => 'about_view' ,
						     'title' => '關於晶華' ,
				             'id'    => 'about_id' , ) ,

				
				'awards_lists'  => array(
						     'func'  => 'awards_lists' ,
						     'title' => '關於晶華' ,
						     'id'    => 'about_id' , ) ,
				
				
				'branch_lists'  => array(
						'func'  => 'branch_lists' ,
						'title' => '服務據點' ,
						'id'    => 'branch_id' , ) ,
				
				'branch_view'  => array(
						'func'  => 'branch_view' ,
						'title' => '服務據點' ,
						'id'    => 'branch_id' , ) ,

				
				'category_promo_lists'  => array(
						'func'  => 'category_promo_lists' ,
						'title' => '促銷分類' ,
						'id'    => 'category_promo_id' , ) ,
				
				'category_promo_view'  => array(
						'func'  => 'category_promo_view' ,
						'title' => '促銷分類' ,
						'id'    => 'category_promo_id' , ) ,
				
				

				'checkout_step1'  => array(
						'func'  => 'checkout_step1' ,
						'title' => '01 商品明細確認' ,
						'id'    => 'checkout_step1_id' , ) ,
				
				'checkout_step2'  => array(
						'func'  => 'checkout_step2' ,
						'title' => '02 配送付款方式' ,
						'id'    => 'checkout_step2_id' , ) ,
				
				'checkout_step3'  => array(
						'func'  => 'checkout_step3' ,
						'title' => '03 訂單明細確認' ,
						'id'    => 'checkout_step3_id' , ) ,
				
				
				'checkout_step1_comb_1'  => array(
						'func'  => 'checkout_step1_comb_1' ,
						'title' => '選擇禮盒規格 / 樣式' ,
						'id'    => 'checkout_step1_comb_1_id' , ) ,
				
				'checkout_step1_comb_2'  => array(
						'func'  => 'checkout_step1_comb_2' ,
						'title' => '組合預覽' ,
						'id'    => 'checkout_step1_comb_2_id' , ) ,
				
				'checkout_step2'  => array(
						'func'  => 'checkout_step2' ,
						'title' => '選擇訂購方式' ,
						'id'    => 'checkout_step2_id' , ) ,
				
				'checkout_step3'  => array(
						'func'  => 'checkout_step3' ,
						'title' => '結帳付款' ,
						'id'    => 'checkout_step3_id' , ) ,
				
								
				'contact_view'  => array(
						'func'  => 'contact_view' ,
						'title' => '連絡我們' ,
						'id'    => 'contact_view_id' , ) ,
				
				
				'customer_info'  => array(
						'func'  => 'customer_info' ,
						'title' => '會員中心' ,
						'id'    => 'customer_info_id' , ) ,
				
				'customer_pass'  => array(
						'func'  => 'customer_pass' ,
						'title' => '修改密碼' ,
						'id'    => 'customer_pass_id' , ) ,
				
				'customer_order_lists'  => array(
						'func'  => 'customer_order_lists' ,
						'title' => '訂單紀錄' ,
						'id'    => 'customer_order_lists_id' , ) ,
				
				'customer_order_view'  => array(
						'func'  => 'customer_order_view' ,
						'title' => '訂單明細' ,
						'id'    => 'customer_order_view_id' , ) ,

				'customer_order_returns'  => array(
						'func'  => 'customer_order_returns' ,
						'title' => '訂單退換' ,
						'id'    => 'customer_order_returns_id' , ) ,
				
				'customer_return_lists'  => array(
						'func'  => 'customer_return_lists' ,
						'title' => '退貨紀錄' ,
						'id'    => 'customer_return_lists_id' , ) ,
				
				'customer_return_view'  => array(
						'func'  => 'customer_return_view' ,
						'title' => '退貨明細' ,
						'id'    => 'customer_return_view_id' , ) ,
				
				
				'certification_view'  => array(
						'func'  => 'certification_view' ,
						'title' => '食在安心' ,
						'id'    => 'certification_view_id' , ) ,
				
				
				'download_lists'  => array(
						'func'  => 'download_lists' ,
						'title' => '檔案下載' ,
						'id'    => 'download_id' , ) ,				
				
				
				'forget'  => array(
						'func'  => 'forget' ,
						'title' => '忘記密碼' ,
						'id'    => 'forget_id' , ) ,
				
				'fail'  => array(
						'func'  => 'fail' ,
						'title' => '失敗' ,
						'id'    => 'fail_id' , ) ,
				
				
				'index'  => array(
						'func'  => 'index' ,
						'title' => '首頁' ,
						'id'    => 'index_id' , ) ,
				

				'knowledge_lists'  => array(
						'func'  => 'knowledge_lists' ,
						'title' => '知識專欄' ,
						'id'    => 'knowledge_id' , ) ,
				
				'knowledge_view'  => array(
						'func'  => 'knowledge_view' ,
						'title' => '知識專欄' ,
						'id'    => 'knowledge_id' , ) ,
				
				
				'login'  => array(
						'func'  => 'login' ,
						'title' => '會員登入' ,
						'id'    => 'login_id' , ) ,
				
				
				'news_lists'  => array(
						     'func'  => 'news_lists' ,
						     'title' => '活動訊息' ,
						     'id'    => 'news_id' , ) ,
				
				'news_view'  => array(
						     'func'  => 'news_view' ,
						     'title' => '活動訊息' ,
						      'id'    => 'news_id' , ) ,
				

				'customer_order_pre_lists'  => array(
						'func'  => 'customer_order_pre_lists' ,
						'title' => '預購記錄' ,
						'id'    => 'order_pre_id' , ) ,
				
				'customer_order_pre_view'  => array(
						'func'  => 'customer_order_pre_view' ,
						'title' => '預購記錄' ,
						'id'    => 'order_pre_id' , ) ,

				'order_pre_view'  => array(
						'func'  => 'order_pre_view' ,
						'title' => '填寫預購單' ,
						'id'    => 'order_pre_view_id' , ) ,
				
				'product_lists'  => array(
						'func'  => 'product_lists' ,
						'title' => '商品分類' ,
						'id'    => 'product_id' , ) ,
				
				'product_view'  => array(
						'func'  => 'product_view' ,
						'title' => '商品分類' ,
						'id'    => 'product_id' , ) ,
				
				
				   'plan_view'  => array(
						'func'  => 'plan_view' ,
						'title' => '公益計劃' ,
						'id'    => 'plan_id' , ) ,
				
				
				'qa_lists'  => array(
						'func'  => 'qa_lists' ,
						'title' => '常見問題' ,
						'id'    => 'qa_id' , ) ,
				
				'qa_view'  => array(
						'func'  => 'qa_view' ,
						'title' => '常見問題' ,
						'id'    => 'qa_id' , ) ,

				
				'register'  => array(
						'func'  => 'register' ,
						'title' => '會員註冊' ,
						'id'    => 'register_id' , ) ,
				
				
				'space_lists'  => array(
						'func'  => 'space_lists' ,
						'title' => '空間案例' ,
						'id'    => 'space_id' , ) ,
				
				'space_view'  => array(
						'func'  => 'space_view' ,
						'title' => '空間案例' ,
						'id'    => 'space_id' , ) ,
				
				'step6_success'  => array(
						'func'  => 'step6_success' ,
						'title' => '04 完成' ,
						'id'    => 'step6_success_id' , ) ,
				
				'step6_fail'  => array(
						'func'  => 'step6_fail' ,
						'title' => '04 完成' ,
						'id'    => 'step6_fail_id' , ) ,
				
				'success'  => array(
						'func'  => 'success' ,
						'title' => '成功' ,
						'id'    => 'success_id' , ) ,
				
				'story_view'  => array(
						'func'  => 'story_view' ,
						'title' => '品牌故事' ,
						'id'    => 'story_id' , ) ,
				
		);
		
		return $data;
	}
}

// --------------------------------------------------------------------

/**
 * 方法 : SEO meta 設定
 *
 * @access	public
 * @param
 * @return
 */
if ( ! function_exists('seoMeta'))
{
	function seoMeta($type ,$param)
	{
		//if( empty($param) ) return "";

		switch ($type) {

			case 'first':

				$CI =& get_instance();

				$data['first']['meta_title']       = !empty($CI->global_meta['first']['meta_title']) ? $CI->global_meta['first']['meta_title'] : $param['meta_title'];
				$data['first']['meta_description'] = !empty($CI->global_meta['first']['meta_description']) ? $CI->global_meta['first']['meta_description'] : $param['meta_description'];
				$data['first']['meta_keyword']     = !empty($CI->global_meta['first']['meta_keyword']) ? $CI->global_meta['first']['meta_keyword'] : $param['meta_keyword'];
				break;
					
			case 'category':
				
				if( empty($param['meta_title']) && empty($param['meta_description']) && empty($param['meta_keyword']))
					return false;
				
				$data['category']['meta_title']       = $param['meta_title'];
				$data['category']['meta_description'] = $param['meta_description'];
				$data['category']['meta_keyword']     = $param['meta_keyword'];
				break;

			case 'content':
				
				if( empty($param['meta_title']) && empty($param['meta_description']) && empty($param['meta_keyword'])) 
				   return false;
				   
				$data['content']['meta_title']       = $param['meta_title'];
				$data['content']['meta_description'] = $param['meta_description'];
				$data['content']['meta_keyword']     = $param['meta_keyword'];
				break;
					
			case 'other':
				$data['other']['meta_title']         = $param['meta_title'];
				$data['other']['meta_description']   = $param['meta_description'];
				$data['other']['meta_keyword']       = $param['meta_keyword'];
				break;

			case 'default':
				$data['default']['meta_title']       = $param['meta_title'];
				$data['default']['meta_description'] = $param['meta_description'];
				$data['default']['meta_keyword']     = $param['meta_keyword'];
				break;
		}

		return $data;
	}
}

// --------------------------------------------------------------------

/**
 * 方法 : fb_like
 *       定義臉書分享
 *       抓取流程優先指定-> meta -> 內文 -> 預設
 * @access	public
 * @param
 * @return
 */
function fb_like($query){

	$CI =& get_instance();

	$og_title       = "" ;
	$og_description = "" ;
	$og_image       = "" ;

	//title
	if( !empty($CI->global_meta['first']['meta_title']) ) {
		$og_title = $CI->global_meta['first']['meta_title'];

	}elseif( !empty($query['meta_title']) ) {
		$og_title = $query['meta_title'];

	}elseif( !empty($query['title']) ){
		$og_title = $query['title'];

	}else{
		$og_title = DIO_WEBNAME;
	}

	//description
	if( !empty($CI->global_meta['first']['meta_description']) ) {
		$og_description = mb_substr( strip_tags($CI->global_meta['first']['meta_description']),0,180,"utf-8");

	}elseif( !empty($query['meta_description']) ) {
		$og_description = mb_substr( strip_tags($query['meta_description']),0,180,"utf-8");

	}elseif( !empty($query['content']) ){
		$og_description = mb_substr( strip_tags($query['content']),0,180,"utf-8");

	}elseif( !empty($query['description']) ){
		$og_description = mb_substr( strip_tags($query['description']),0,180,"utf-8");

	}else{
		$og_description = DIO_WEBNAME;
	}

	//image
	if( !empty($query['image'])  ) {
		$og_image = base_url('resources/uploads/'.$query['image']);

	}else{
		$og_image = '#';
	}

	$data = array('is_fbShare' => true ,
			'og_title' => $og_title ,
			'og_description' => $og_description ,
			'og_image' => $og_image
	);

	return $data;
}

// ------------------------------------------------------------------------

/**
 * getOrderPreStatus
 *
 * 取得預購單狀態
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getOrderPreStatus'))
{
	function getOrderPreStatus()
	{
		return array(
				'0' =>  '未處理' ,
				'1' =>  '處理中' ,
				'2' =>  '已處理'
		);
	}
}

// ------------------------------------------------------------------------

/**
 * getOrderPreIsNotice
 *
 * 取得預購單是否已通知
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getOrderPreIsNotice'))
{
	function getOrderPreIsNotice()
	{
		return array(
				'0' =>  '未通知' ,
				'1' =>  '已通知'
		);
	}
}


// ------------------------------------------------------------------------

/**
 * getReasonId
 *
 * 取得退換貨申請原因
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getReasonId'))
{
	function getReasonId()
	{
		return array(
				'0' =>  '取消訂單→尚未收到商品以及貨運通知信件' ,
				'1' =>  '退貨退款→已收到商品以及貨運通知信件' ,
				'2' =>  '換 貨→商品瑕疵，希望換貨' ,
		);		
	}
}

// ------------------------------------------------------------------------

/**
 * getFeeCategory
 *
 * 取得預購單狀態
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getFeeCategory'))
{
	function getFeeCategory()
	{
		return array(
				'0' =>  '常溫宅配' ,
				//'1' =>  '冷藏宅配' ,
				//'2' =>  '冷凍宅配'
		);
	}
}

// ------------------------------------------------------------------------

/**
 * getCheckOutPromoType
 *
 * 取得購物車的優惠類型(含運費)
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getCheckOutPromoType'))
{
	function getCheckOutPromoType()
	{
		return array(
				   'fee' =>  '運費' ,
				'coupon' =>  '優惠券' ,
		);
	}
}

// ------------------------------------------------------------------------

/**
 * getProductType1Unit
 *
 * 取得禮盒單位
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getProductType1Unit'))
{
	function getProductType1Unit()
	{
		return array(
				   '8' =>  '每盒8入' ,
				  '12' =>  '每盒12入' ,
		);
	}
}

// ------------------------------------------------------------------------

/**
 * getRecommendSize
 *
 * 取得圖片尺寸
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getRecommendSize'))
{
	function getRecommendSize()
	{
		return array(
				'about'          => '(建議尺寸：1440x1080)',
				'article'        => '(建議尺寸：164x126)',

				'download'       => '(640x640)',
				
				'favicon'        => '(32x32)',
				
				'index'          => '(建議尺寸：470X850 格式PNG)',
				'index_youtube'  => '(建議尺寸：640X480)',
				'index_img'      => '(建議尺寸：480X480)',
				'image'          => '(建議尺寸：800X1080)',
				
				'logo'           => '(建議尺寸：280X82)',

				'news'           => '(640x640)',

				'product'        => '(建議尺寸：640x640)',
		);
	}
}

// ------------------------------------------------------------------------

/**
 * getPromoCouponCodeType
 *
 * 取得折價券設定類型
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getPromoCouponCodeType'))
{
	function getPromoCouponCodeType()
	{
		return array(
				'0' =>  '百分比' ,
				'1' =>  '金額'
		);
	}
}

// ------------------------------------------------------------------------

/**
 * getTicketAddrId
 *
 * 取得發票寄送地址Id
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getTicketAddrId'))
{
	function getTicketAddrId()
	{
		return array(
				'0' =>  '同訂購人 ' ,
				'1' =>  '同收貨人 '
		);
	}
}

// ------------------------------------------------------------------------

/**
 * getNumberFoematChCharLow
 *    
 * 取得國字小寫格式
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getNumberFoematChCharLow'))
{
	function getNumberFoematChCharLow()
	{
		return array( 
				'1'  =>  '一' ,
				'2'  =>  '二' ,
				'3'  =>  '三' ,
				'4'  =>  '四' ,
				'5'  =>  '五' ,
				'6'  =>  '六' ,
				'7'  =>  '七' ,
				'8'  =>  '八' ,
				'9'  =>  '九' ,
				'10'  =>  '十' ,
		);
	}
}

// ------------------------------------------------------------------------

/**
 * getBranchEnabledFunc
 *
 * 取得分店可用頁面，僅限組合商品(依URL)
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getBranchEnabledFunc'))
{
	function getBranchEnabledFunc()
	{
		return array( 'checkout/step1_comb_1' 
				     ,'checkout/step1_comb_2'
				     ,'checkout/step3_comb'
				     ,'checkout/step0'
				     ,'checkout/step1'
				     ,'checkout/step2'
				     ,'checkout/step3'
				     ,'checkout/step4'
				    
				     //返回
				     ,'checkout_return/ecpg_credit'
				     ,'checkout_return/atm'
				     ,'checkout_return/pay_allpay_atm'
				     ,'checkout_return/pay_allpay_credit'
				     ,'checkout_return/cod'
				     ,'checkout_return/success'
				     ,'checkout_return/fail'
								
				     //隱性觸發的 URL，如Ajax
				     ,'buy/add'
				     ,'buy/edit'
				     ,'buy/del'
				     ,'ajax/reSetCartProductId'
				     ,'ajax/getCartInfoByProductId'
				     ,'ajax/getCartProductTypeId1Img_ajax'
				     ,'ajax/isCombEntityOk_ajax'
				     ,'ajax/getTransMethodByPayMethod'
				      
				     ,'sign_in/login'
				
		);
	}
}

// ------------------------------------------------------------------------

/**
 * getTimeArrival
 *
 * 取得指定到貨時段
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getTimeArrival'))
{
	function getTimeArrival()
	{
		return array(
				'0' => '不限 ' ,
				'1' => '上午' ,
				'2' => '下午' ,
				'3' => '晚上' ,
		);
	}
}

// ------------------------------------------------------------------------

/**
 * getValidController
 *
 * 取得合法的控制器
 *   主要應用於頁面的存取和導向
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getValidController'))
{
	function getValidController()
	{
		return array(
				'about/view' ,
				'article/lists' ,
				'article/view' ,
				'article/lists_vedio' ,
			
				'contact/view',
				'checkout/step0' ,
				'checkout/step1' ,
				'checkout/step2' ,
				'checkout/step3' ,
				'checkout/step4' ,
				'checkout/step5' ,
				'checkout_return/success' ,
				'checkout_return/fail' ,
				'customer_order/lists' ,
				'customer_order/view' ,
				'customer/info' ,
				'customer_addr/lists' ,
				'customer_addr/add' ,
				'customer_addr/edit' ,
				'customer/pass' ,
				'customer_return/lists' ,
			
				'index/main',
					
				'news/lists',
				'news/view' ,
					
				'product/lists',
				'product/view',
					
				'product_grain/lists',
				'product_grain/view',
					
				'qa_category/view' ,
				'qa_category/view_sub' ,
				'qa/lists' ,
				'qa/view',

				'service/view' ,
		);
	}
		
}

// ------------------------------------------------------------------------

/**
 * getECPayCvsLogisticsSubType
 *
 * 取得超商類型
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getECPayCvsLogisticsSubType'))
{
	function getECPayCvsLogisticsSubType()
	{
		return array(
				'UNIMARTC2C' =>  '7-11 超商' ,
				'FAMIC2C'    =>  '全家 超商' ,
		);
	}
}


/* End of file dio_sys_vars */