<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 方法 : 目錄 /檔案 函數
 * 
 * @helpers dio_file
 * @author Dio
 *
 */

// ------------------------------------------------------------------------

/**
 * mkDirs
 *
 * 建立階層式目錄(遞回)
 * 如 : 
 *     My project/blog/index
 *
 * @access	public
 * @param	string 
 * @param	int
 * @return	t/f
 */
if ( ! function_exists('mkDirs'))
{
	function mkDirs($pathname, $mode=0777)
	{
	  $dir = @split("/", $pathname);
	  $path = "";
	  for ($i = 0; $i < count($dir); $i++) {
		$path .= $dir[$i]."/";
		if (!is_dir($path)) @mkdir($path, $mode);
		//@chmod($path, $mode);
	  }
	  
	  return (is_dir($pathname));
	}
}

// ------------------------------------------------------------------------

/**
 * delDirFiles
 *
 * 刪除指定目錄下的所有目錄與檔案
 *
 * @access	public
 * @param	string
 * @return	t/f
 */
if ( ! function_exists('delDirFiles'))
{
	function delDirFiles($dir)
	{
	  if (!file_exists($dir)) 
		  return true;
	  
	  if (!is_dir($dir))
		  return unlink($dir);
	 
	  foreach (scandir($dir) as $item) {
		
	  	if ($item == '.' || $item == '..') 
			continue;
		
		if (!delDirFiles($dir . DIRECTORY_SEPARATOR . $item))
			return false;
		
	  }

	   return rmdir($dir);
	}
}


/* End of file dio_file */