<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 說明 : 處理系統的各種資料驗證。
 *
 * @helpers is_valid
 * @author Dio
 *
 */

// ------------------------------------------------------------------------

/**
 * isIdentity
 *
 * 身分證檢查
 *
 * @access	public
 * @param	string
 * @return	t/f
 */
if ( ! function_exists('isIdentity'))
{
	function isIdentity($id)
	{
		//建立字母分數陣列
		$head = array('A'=>1,'I'=>39,'O'=>48,'B'=>10,'C'=>19,'D'=>28,'E'=>37,'F'=>46,'G'=>55,'H'=>64,'J'=>73,'K'=>82,
				'L'=>2,'M'=>11,'N'=>20,'P'=>29,'Q'=>38,'R'=>47,'S'=>56,'T'=>65,'U'=>74,'V'=>83,'W'=>21,'X'=>3,'Y'=>12,'Z'=>30,
				'a'=>1,'i'=>39,'o'=>48,'b'=>10,'c'=>19,'d'=>28,'e'=>37,'f'=>46,'g'=>55,'h'=>64,'j'=>73,'k'=>82,
				'l'=>2,'m'=>11,'n'=>20,'p'=>29,'q'=>38,'r'=>47,'s'=>56,'t'=>65,'u'=>74,'v'=>83,'w'=>21,'x'=>3,'y'=>12,'z'=>30
		);

		$multiply = array(8,7,6,5,4,3,2,1); //建立加權基數陣列

		//檢查身份字格式是否正確
		if (preg_match("/^[a-zA-Z][1-2][0-9]+$/",$id) && strlen($id) == 10){
			$len = strlen($id); //切開字串

			for($i=0; $i<$len; $i++){
				$stringArray[$i] = substr($id,$i,1);
			}

			$total = $head[array_shift($stringArray)]; //取得字母分數
			$point = array_pop($stringArray); //取得比對碼
			$len = count($stringArray); //取得數字分數

			for($j=0; $j<$len; $j++){
				$total += $stringArray[$j]*$multiply[$j];
			}

			if (($total%10 == 0 )?0:10-$total%10 != $point) { //檢查比對碼
				return false;
			} else {
				return true;
			}

		}  else {
			return false;
		}
	}
}




/*****************************************************************************
/* 名稱    : is_valid_discount_coupon
 * 說明    : 優惠券驗證
 * 參數    : 	 
 * 返回    : array , err_code : 
 *          0 成功
 *          1 無此資料
 *          2 未達條件 (購物金額不足)
 *          3 未達條件 (已超過使用次數)   
 ****************************************************************************/
function is_valid_discount_coupon($code ,$total){
	
  $CI =& get_instance();
  	
  $CI->db->where('code' , $code);
  $data = $CI->db->get('promo_discount_coupon')->row_array();

  $data['err_code'] = 0;
  
  if(empty($data['discount_coupon_id'])){
	 $data['err_code'] = 1;
	 return $data;
  }
  
  if($data['total'] > $total){
	 $data['err_code'] = 2;
	 return $data;
  }
  
  $CI->db->where('discount_coupon_id' ,$data['discount_coupon_id']);
  $count = $CI->db->count_all('order_total_item');
  
  if($data['use_count'] <= $count){
	 $data['err_code'] = 3;
	 return $data;
  }
	   
   return $data;
}


/*****************************************************
/* 名稱           : is_login_admin
 * 功能           : 判斷系統管理員是否登入
 * 參數           : t/f
 ****************************************************/
function is_login_admin(){

	if( !isset($_SESSION['admin_info']['admin_id']) || empty($_SESSION['admin_info']['admin_id']) ){
		return false;
	}
	
	return true;
}

/*****************************************************
/* 名稱           : is_login_store
 * 功能           : 判斷分店管理員是否登入
 * 參數           : t/f
 ****************************************************/
function is_login_store(){

	if( !isset($_SESSION['store_info']['branch_id']) || empty($_SESSION['store_info']['branch_id']) ){
		return false;
	}

	return true;
}

// ------------------------------------------------------------------------

/**
 * IsLoginCustomer
 *
 * 判斷會員是否登入
 *
 * @access	public
 * @return	t/f
 */
if ( ! function_exists('IsLoginCustomer'))
{
	function IsLoginCustomer()
	{
		$CI =& get_instance();

		if( empty($_SESSION['customer_info']['customer_id']) ){
			return false;
		}

		return true;
	}

}

// ------------------------------------------------------------------------

/**
 * IsStockEnough
 *
 * 驗證庫存量是否足夠
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('IsStockEnough'))
{
	function IsStockEnough($sku ,$entity)
	{
	   $CI =& get_instance();

	   $CI->db->where('sku' ,$sku);
	   $query = $CI->db->get('product_speci')->row_array();
	   
	   if($query['stock'] >= $entity){
	   	 return true;
	   }else{
	   	 return false;
	   } 
	}
}

// ------------------------------------------------------------------------

/**
 * IsReturnEntity
 *
 * 驗證訂單退貨數量是否正確
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('IsReturnEntity'))
{
	function IsReturnEntity($order_detail_id ,$entity)
	{
		$CI =& get_instance();

		$sql = "SELECT entity - IFNULL( (SELECT SUM(entity) FROM `return` WHERE order_detail_id = {$order_detail_id}) ,0 ) as entity
		          FROM order_detail WHERE order_detail_id = ".$order_detail_id;
		
		$query = $CI->db->query($sql)->row_array();
		
		if($query['entity'] < $entity){
			return false;
		}
		
		return true;
	}
}


/* End of file is_valid */