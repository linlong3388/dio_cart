<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 方法 : 資料傳送函數
 * 
 * @helpers dio_transfer
 * @author Dio
 *
 */

// ------------------------------------------------------------------------

/**
 * postJson
 *
 *  用curl傳送 json格式給遠端的 web service
 *  (已測試 嘉里大榮物流)
 *
 * @access	public
 * @param	$url  主機網址
 *          $data 陣列格式資料
 *          
 * @return	array
 */
if ( ! function_exists('postJson'))
{
	function postJson()
	{
		$url = "http://www.kerrytj.com/webquery/CargoStatus/SearchTrackingAPI.ashx";
		
		$data =  json_encode(array(
				'TrackingNumber' => '12345678901' ,
				'Flag' => '406458300177721'
		));
		
		$curl = curl_init( $url );
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_POSTFIELDS,$data);
		
		$curl_response = curl_exec($curl);
		
		curl_close($curl);
		
		return json_decode($curl_response, true);		
	}
}


/* End of file dio_transfer */