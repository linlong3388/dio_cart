<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 方法 : 促銷活動函數
 * 
 * @helpers dio_promo
 * @author Dio
 *
 */

// ------------------------------------------------------------------------

/**
 * isValidDiscountCoupon
 *
 * 優惠券驗證
 *
 * @access	public
 * @param	string
 * @return	array , err_code :
 *          0 成功
 *          1 無此資料
 *          2 未達條件 (購物金額不足)
 *          3 未達條件 (已超過使用次數)
 */
if ( ! function_exists('isValidDiscountCoupon'))
{
   function isValidDiscountCoupon($code ,$total){

	 $CI =& get_instance();
	 
	 $CI->db->where('code' , $code);
	 $data = $CI->db->get('promo_discount_coupon')->row_array();

	 $data['err_code'] = 0;

	 if(empty($data['discount_coupon_id'])){
	    $data['err_code'] = 1;
	    return $data;
	 }

	 if($data['total'] > $total){
	    $data['err_code'] = 2;
	    return $data;
	 }

	 $CI->db->where('discount_coupon_id' ,$data['discount_coupon_id']);
	 $count = $CI->db->count_all('order_total_item');

	 if($data['use_count'] <= $count){
	    $data['err_code'] = 3;
	    return $data;
	 }

	 return $data;
  }

}

// ------------------------------------------------------------------------

/**
 * getFeedbackGold
 *
 * 取得購物金
 *
 * @access	public
 * @param	string
 * @return	int
 */
if ( ! function_exists('getFeedbackGold'))
{
	function getFeedbackGold($customer_id){

	  $CI =& get_instance();

	  $CI->db->where('customer_id' ,$customer_id);
	  $CI->db->select_sum('feedback_gold');
	  $data = $CI->db->get('`order`')->row_array();
	 
	  return $data['feedback_gold'];
	}

}


/* End of file dio_promo */