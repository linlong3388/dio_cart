<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 方法 : 陣列函數
 * 
 * @helpers dio_array
 * @author Dio
 *
 */

// ------------------------------------------------------------------------

/**
 * delTwoDDupli
 *
 * 過濾二維陣列的重複元素
 * 如下，將會過濾掉重複的'Saab':
 * 
 *   delTwoDDupli($data,'name');
 * 
 *   $data = array
 *            (
 *              array( 'name'  => 'Volvo' ,
 * 		               'price' => 'US$9,800'),
 *              array( 'name'  => 'BMW' ,
 * 		               'price' => 'US$2,7000'),
 *              array( 'name'  => 'Saab',
 * 		               'price' => 'US$7,000'),
 *              array( 'name'  => 'Land Rover' ,
 *  		           'price' => 'US$8,900') ,
 *              array( 'name'  => 'Saab',
 * 		               'price' => 'US$7,000'),
 *            );
 *
 * @access	public
 * @param	array 
 * @param	string (指定欲過濾的元素名)
 * @return	array
 */
if ( ! function_exists('delTwoDDupli'))
{
	function delTwoDDupli($data,$fval)
	{
	$re_ary = array();
	$temp_ary = array();

	for($i=0 ; $i<COUNT($data) ; $i++){
		if(!in_array($data[$i][$fval], $temp_ary)){
			array_push($temp_ary, $data[$i][$fval]);
			$re_ary[] = $data[$i];
		}
	}

	return $re_ary;
	}
}

// ------------------------------------------------------------------------

/**
 * reSortArray
 *
 * 一維陣列重新排列，主要應用在下拉式搜尋框，排序欄位置頂
 *
 * @access	public
 * @param	string 
 * @param	array 
 * @return	array
 */
if ( ! function_exists('reSortArray'))
{
	function reSortArray($key ,$data)
	{
 	  if(!array_key_exists($key ,$data))
		return $data;
	  
	  else
		return array($key => $data[$key]) + $data;
	  
	}
}


// ------------------------------------------------------------------------

/**
 * reSortArrayMutil
 *
 * 一維陣列重新排列，主要應用在下拉式搜尋框，排序欄位置頂
 * 並提供多種搜尋功能
 *
 * 返回值有底下幾種情況 ，如:
 *     1) 傳入空值 返回 array() 全部資料
 *     2) 傳入鍵值 返回  單筆資料
 *     3) 傳入鍵值 排序後，返回 array() 全部資料
 *     4) 傳入空值 排序後，返回 array() 全部資料(搜尋用)
 *     5) 傳入鍵值 返回  單筆資料，追加一個 '*' 等於 '全部' 的值
 *
 * @access	public
 * @param	string
 * @param	string
 * @param	array
 * @return	mixed
 */
if ( ! function_exists('reSortArrayMutil'))
{
	function reSortArrayMutil($type=1 ,$key_id=0 ,$data ,$char="不拘")
	{
		
		switch ($type) {
			case 1:
				return $data;
				break;

			case 2:
				return $data[$key_id];
				break;

			case 3:
				$data = array($key_id => $data[$key_id]) + $data;
				return $data;
				break;

			case 4:
				if($key_id == "" || $key_id == "*"){ //全部
					$data = array('*' => $char) + $data;
				}else{ //條件
					$data = array($key_id => $data[$key_id]) + array('*' => $char) + $data;
				}
				return $data;
				break;
				
			case 5:
				
				$CI =& get_instance();
				
				$data = array('*' => $CI->lang->line('branch_all')) + $data;
				return reSortArrayMutil(2 ,$key_id ,$data); 
				break;
		}
	}
}

// ------------------------------------------------------------------------

/**
 * transKeyPairArray
 *
 * 將二維陣列轉成一維陣列的鍵/值配對，應用在編輯下拉式欄位
 *
 * 例如 : 將一個二維陣列
 * Array
 * (
 *     [0] => Array
 *         (
 *             [category_id] => 1
 *             [name] => aaa
 *         )
 *
 *     [1] => Array
 *         (
 *             [category_id] => 2
 *             [name] => bbb
 *         )
 *
 *     [2] => Array
 *        (
 *             [category_id] => 3
 *            [name] => ccc
 *         )
 *
 * )
 *
 * 轉成為 一為陣列
 *
 * Array
 * (
 *     [1] => aaa
 *     [2] => bbb
 *     [3] => ccc
 * )
 *
 * @access	public
 * @param	string
 * @param	array
 * @return	array
 */
if ( ! function_exists('transKeyPairArray'))
{
	function transKeyPairArray($data ,$key ,$val)
	{
		$temp = array();
		foreach ($data as $row){
			$temp[$row[$key]] = $row[$val];
		}

		return $temp;
	}
}



/* End of file dio_array */