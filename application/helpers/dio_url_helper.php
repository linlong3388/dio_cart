<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 方法 : 路由函數
 * 
 * @helpers dio_url
 * @author Dio
 *
 */

// ------------------------------------------------------------------------

/**
 * getCurrentFullUrl
 *
 * 取得當前完整的URL(含參數)
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getCurrentFullUrl'))
{
	function getCurrentFullUrl()
	{
		$CI =& get_instance();

		$url = $CI->config->site_url($CI->uri->uri_string());
		return $_SERVER['QUERY_STRING'] ? $url.'?'.$_SERVER['QUERY_STRING'] : $url;
	}
}

// ------------------------------------------------------------------------

/**
 * getUrlLastSection
 *
 * 取得URL最後一個區段(控制器)，並去除?後面的參數
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('getUrlLastSection'))
{
	function getUrlLastSection()
	{
 	  $url_name = end(explode('/', $_SERVER["REQUEST_URI"])) ;
	  $pos = strpos($url_name , '?');

	  if ($pos === false)
		return $url_name;
	  else
		return substr($url_name,0 ,$pos);
	}
}

// ------------------------------------------------------------------------

/**
 * getUrlLast2Section
 *
 * 取得URL最後二個區段(控制器)，並去除?後面的參數
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('getUrlLast2Section'))
{
	function getUrlLast2Section()
	{
	  $url_name = explode("/" ,$_SERVER["REQUEST_URI"]);
	  $url_1name = $url_name[count($url_name) - 1]; 
	  $url_2name = $url_name[count($url_name) - 2]; 

	  $pos = strpos($url_1name , '?');
	
	  if ($pos === false)
		return $url_2name.'/'.$url_1name;
	
	  else
		return $url_2name.'/'.substr($url_1name,0 ,$pos);
	}
}

// ------------------------------------------------------------------------

/**
 * getAdminURL
 *
 * 取得後台URL/頁面
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getAdminURL'))
{
	function getAdminURL($segment)
	{
		return base_url(DIO_ADMIN_PATH_URL.$segment);
	}
}

// ------------------------------------------------------------------------

/**
 * getStoreURL
 *
 * 取得分店URL/頁面
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getStoreURL'))
{
	function getStoreURL($segment)
	{
		return base_url(DIO_STORE_PATH_URL.$segment);
	}
}

// ------------------------------------------------------------------------

/**
 * getAdminImg
 *
 * 取得後台URL/圖片
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getAdminImg'))
{
	function getAdminImg($segment)
	{
		return base_url(DIO_PATH_PIMG.$segment);
	}
}

// ------------------------------------------------------------------------

/**
 * getUserFile
 *
 * 取得前台URL/檔案
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getUserFile'))
{
	function getUserFile($segment)
	{
		return base_url(DIO_PATH_FILE.$segment);
	}
}

// ------------------------------------------------------------------------

/**
 * getUserURL
 *
 * 取得前台URL/頁面
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getUserURL'))
{
	function getUserURL($segment)
	{
		return base_url(DIO_PATH_URL.$segment);
	}
}

// ------------------------------------------------------------------------

/**
 * getQueryStringParam
 *
 * 取得URL參數，由於$_SERVER['QUERY_STRING']
 *    移除URL重複&空白參數
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getQueryStringParam'))
{
	function getQueryStringParam()
	{
	  $vars = explode('&', $_SERVER['QUERY_STRING']);

	  $final = array();

	  if(!empty($vars)) {
		 foreach($vars as $var) {
			$parts = explode('=', $var);

			$key = $parts[0];
			$val = rawurldecode($parts[1]);

			if(!array_key_exists($key, $final) && !empty($val))
				$final[$key] = $val;
		}
	  }

	  return urldecode(http_build_query($final));
	}
}

// ------------------------------------------------------------------------

/**
 * getDefaultImageURL
 *
 * 若沒有圖片，取得預設圖片
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getDefaultImageURL'))
{
	function getDefaultImageURL($image ,$width=0 ,$height=0)
	{

		if( strlen($image) > 0){

			$image_path = DIO_PATH_PIMG.$image;
				
			if( file_exists( $image_path ) ){

				return base_url($image_path);
			}
				
		}

		return "http://placehold.it/{$width}x{$height}" ;

		//return empty($image) ? "http://placehold.it/{$width}x{$height}" :  base_url(DIO_PATH_PIMG.'/'.$image);
	}
}


// ------------------------------------------------------------------------

/**
 * getQueryStringParamURL
 *
 * 傳入完整的URL，過濾掉重複的URL參數後，返回完整的URL
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getQueryStringParamURL'))
{
	function getQueryStringParamURL($url)
	{
		$url_host  = explode('?', $url);

		parse_str($url_host[1] ,$final);

		return $url_host[0].'?'.urldecode(http_build_query($final));
	}
}

// ------------------------------------------------------------------------

/**
 * changeDownloadUrl
 *
 * 檔案下載的URL切換
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('changeDownloadUrl'))
{
	function changeDownloadUrl($url)
	{
		if( empty($url) ){
			return "";
		}

		if ( strpos($url, 'http') !== false ) {
			return $url;
		}

		return base_url(DIO_PATH_PIMG.$url);
	}
}

// ------------------------------------------------------------------------

/**
 * getLoginRedirectPage
 *
 * 取得登入後導向頁面
 *
 * @access	public
 * @return	array
 */
if ( ! function_exists('getLoginRedirectPage'))
{
	function getLoginRedirectPage()
	{
		if( !empty($_SESSION['page']['prev']) ){

			redirect($_SESSION['page']['prev']);
		}else{

			redirect('customer/info');
		}
		 
	}
}

/* End of file dio_url */