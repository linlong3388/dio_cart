<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * XOR encrypts a given string with a given key phrase.
 * (說明: 採用互斥邏輯運算進行(加/解)密的函式庫)
 *
 * @param     string    $InputString    Input string
 * @param     string    $KeyPhrase      Key phrase
 * @return    string    Encrypted string
 */
function XOREncryption($InputString, $KeyPhrase){

	$KeyPhraseLength = strlen($KeyPhrase);

	// Loop trough input string
	for ($i = 0; $i < strlen($InputString); $i++){
		// Get key phrase character position
		$rPos = $i % $KeyPhraseLength;

		// Magic happens here:
		$r = ord($InputString[$i]) ^ ord($KeyPhrase[$rPos]);

		// Replace characters
		$InputString[$i] = chr($r);
	}

	return $InputString;
}

// Helper functions, using base64 to
// create readable encrypted texts:
function XOREncrypt($InputString, $KeyPhrase){
	$InputString = XOREncryption($InputString, $KeyPhrase);
	$InputString = base64_encode($InputString);
	return $InputString;
}

function XORDecrypt($InputString, $KeyPhrase){
	$InputString = base64_decode($InputString);
	$InputString = XOREncryption($InputString, $KeyPhrase);
	return $InputString;
}

/* End of file dio_encrypt */