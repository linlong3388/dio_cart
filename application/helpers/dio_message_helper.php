<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * 方法 : 訊息函數
 * 
 * @helpers dio_message
 * @author Dio
 *
 */

// ------------------------------------------------------------------------

/**
 * dioRedirect
 *
 * 重新導向 ,防止頁面重複post
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('dioRedirect'))
{
	function dioRedirect($redirect = '' ,$message = '資料異動成功!')
	{

	  //$url = base_url('index.php').'/'.$redirect;
	  $url = getUserURL($redirect);

	  echo "<script>alert('".$message."');</script>";
	  echo "<script>window.location='{$url}'</script>";
	}
}

// ------------------------------------------------------------------------

/**
 * dioRedirectSelf
 *
 * 重新導向 ,防止頁面重複post
 * 由於get參數過多時不易傳遞，所以在本頁做reload
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('dioRedirectSelf'))
{
	function dioRedirectSelf($message = '資料異動成功!')
	{
	  echo "<script>alert('".$message."');</script>";
	  echo "<script>window.history.back()</script>";
	}
}


/* End of file dio_message */